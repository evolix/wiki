---
title: Howto Rsync
categories: sysadmin storage
...

* Documentation : <https://rsync.samba.org/documentation.html>

[rsync](https://rsync.samba.org/) est un logiciel libre pour synchroniser des fichiers depuis ou vers un serveur distant, ou encore localement. **rsync** permet notamment de réaliser des synchronisations incrémentales que nous utilisons pour réaliser des migrations de données et des sauvegardes.

## Installation

~~~
# apt install rsync

$ rsync --version
rsync  version 3.1.2  protocol version 31
~~~


## Utilisation de base

Voici une utilisation basique pour transférer un répertoire vers un serveur distant via SSH (la première commande fait un _dry-run_ pour vérifier que tout est Ok, la seconde envoie les données) :

~~~
$ rsync -aASv --dry-run --delete --numeric-ids /home example.com:/home
$ rsync -aASv --delete --numeric-ids --stats /home example.com:/home
~~~

Notons l'utilisation des **deux options fondamentales** pour lesquelles on doit se poser la question de leur utilisation à chaque commande **rsync** :

* `--delete` : Supprime les données « en trop » qui sont sur la destination mais pas sur la source (à utiliser dans 99.9% des cas et pourtant ce n'est pas le défaut !)
* `--numeric-ids` : Conserve les UID/GID originaux même si owner/group ont des UID/GID différents sur la destination (à utiliser en général, sauf dans les cas de migration de données d'un serveur vers un autre)


##  Utilisation

Copier un fichier `foo.txt` vers un serveur distant `example.com` :

~~~
$ rsync /tmp/foo.txt example.com:/tmp/
~~~

Copier tout un dossier vers un serveur distant, avec `-r` ou `--recursive` :

~~~
$ rsync --recursive /tmp/bar example.com:/tmp/
~~~

Copier un dossier depuis le serveur vers son poste :

~~~
$ rsync -r example.com:/tmp/bar /tmp/
~~~

La synchronisation peut se faire localement :

~~~
$ rsync -r /tmp/bar/ /tmp/bar2/
~~~

Utiliser le mode archive, avec `-a` ou `--archive`. Ce mode est équivalent à `-rlptgoD` et préserve la plupart des attributs :

~~~
$ rsync --archive /tmp/bar example.com:/tmp/
~~~

### Options

**rsync** propose de nombreuses options pour configurer le comportement avant/pendant/après les transferts.

Voici la liste des paramètres que nous utilisons souvent :

* `-l`, `--links` : Préserve les liens symboliques
* `-L`, `--copy-links` : Transforme les liens symboliques en fonction du répertoire de destination
* `-H`, `--hard-links` : Préserve les liens (hardlinks)
* `-o`, `--owner` : Préserve le propriétaire
* `-g`, `--group` : Préserve le groupe
* `-p`, `--perms` : Préserve les permissions
* `-A`, `--acls` : Préserve les ACLs (implique `-p`)
* `-t`, `--times` : Préserver les dates (autant que possible)
* `--numeric-ids` : Conserver les UID/GID originaux même si owner/group ont des UID/GID différents sur la destination (ceci n'est pas le défaut !)
* `-n`, `--dry-run` : Les actions ne sont pas réalisées, juste listées
* `-z`, `--compress` : Compresse les fichiers pour accélerer le transfert
* `-S`, `--sparse` : Traite efficacement les fichiers à trou (sparse files)
* `-v`, `--verbose` : Mode verbeux
* `-q`, `--quiet` : Pas de sortie en stdout
* `-h`, `--human-readable` : Les infos sont lisibles par un humain, conversion à l'unité de mesure la plus proche. Exemple 1249284o = 1.1M
* `-d`, `--dirs` : Copie les dossiers sans avoir besoin de lui indiquer qu'il faut le faire récursivement
* `--bwlimit=RATE` : Limite le transfert. Attention c'est en unité de 1024 octets. On peut mettre des suffixes. Exemple 1.5m (1.5Mo/s)
* `--partial` : Conserve les fichiers transférés partiellement. Idéal pour reprendre un précédent transfert interrompu
* `--stats` : Affiche des statistiques à la fin du transfert. Idéal si combiné avec `-h` pour avoir le résumé du transfert, vitesse moyenne, octets transférés, etc.
* `-4`, `--ipv4` : Utiliser une connexion IPv4
* `-6`, `--ipv6` : Utiliser une connexion IPv6
* `-P`, `--progress` : Suivre la progression du transfert de chaque fichier
* `-r`, `--recursive` : Mode récursif
* `-a`, `--archive` : Archive en preservant la plupart des attributs (équivalent à `-rlptgoD`)
* `--delete` : Supprimer les données distantes qui sont absentes depuis la source
* `--exclude=PATTERN` : Ignore les fichiers correspondants au pattern (peut être utilisée plusieurs fois)
* `--exclude-from=FILE` : Ignore les fichiers correspondants aux patterns situés dans un fichier
* `--delete-excluded` : Supprimer les données distantes qui sont exclus sur la source.
* `--ignore-errors` : Ignore les erreurs d'entrées/sorties sur les opérations de suppression
* `-e`, `--rsh=COMMAND` : Permet de changer le remote shell par défaut. Très utile pour spécifier un port différent pour SSH. Exemple : `-e "ssh -p 2222"`
* `-C`, `--cvs-exclude` : Ignore automatiquement les fichiers temporaires, à la manière de CVS
* `--chown=USER:GROUP` : Change le propriétaire USER:GROUP lors de la copie. Peux être combiner avec l'option -a.
* `-x`, `--one-file-system` : Rester sur le même système de fichier pour la source. Exemple : `rsync -x /var /mnt`, si `/var/log` est un point de montage, il ne sera pas copié dans `/mnt`.

## Serveur rsync

Activation via `/etc/default/rsync` :

~~~
RSYNC_ENABLE=true
~~~

Il faut définir au minimum des répertoires dans le fichier `/etc/rsyncd.conf` :

~~~
[rsync_shares]
    path = /home/rsync_shares
~~~

Le démon tourne par défaut sur le port TCP/873 :

~~~
873/tcp   open  rsync
~~~

Et l'on peut l'utiliser ainsi :

~~~
$ rsync -aASv --dry-run --delete --numeric-ids /home example.com::rsync_shares/
~~~


## FAQ

### file has vanished

Cela signifie que des fichiers ont été modifiés pendant le transfert.

### connection unexpectedly closed

Si l'on obtient une erreur ressemblant à :

~~~
Write failed: Broken pipe
rsync: connection unexpectedly closed (128109911 bytes received so far) [receiver]
rsync error: error in rsync protocol data stream (code 12) at io.c(601) [receiver=3.0.7]
rsync: connection unexpectedly closed (128109911 bytes received so far) [generator]
rsync error: unexplained error (code 255) at io.c(601) [generator=3.0.7]
~~~

Cela peut venir dans certains cas d'un routeur coupant agressivement les sessions en cas d'inactivité. Dans ce cas, une solution est d'ajouter l'option suivante en Rsync Over SSH : `-e "ssh -o ServerAliveInterval=5"`

### "aquota.user": Operation not permitted (1)

Les fichiers de quota (`aquota.user` et `aquota.group`) ne peuvent pas être directement synchronisé par rsync car ces fichiers sont gérés par le noyau et possédant des attributs étendu les rendant immuables.

Il est donc conseillé de les copier localement avec `cp` pour avoir une copie de ces fichiers sans attributs étendus et de copier ces fichiers plutôt que les originaux. Pensez aussi à exclure les fichiers des quotas originaux avec :

```
--exclude 'aquota.user' \ 
--exclude 'aquota.group'
```

