**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto PocketPC

Exemple de configuration avec un HTC Touch2 T3333 :

~~~
1. Dans "Config messag.", mettre l'adresse de messagerie (avec @example.com) et le mot de passe.
2. Passe en "Configuration manuelle"
3. Ne pas préciser de "Domaine:"
4. Mettre l'adresse du serveur
5. ATTENTION, bizarrement, il ne faut pas cocher la case "Ce serveur nécessite une connexion cryptée (SSL)"
6. Choisir ce que l'on veut synchroniser, et c'est terminé.
~~~
