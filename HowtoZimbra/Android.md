**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Bugs Android avec ActiveSync :

* pas de multi-agendas
* mauvaise gestion du statut privé lors d'une modification
* pas de suppression du mail de notification après une réponse à une invitation
* le push est moins réactif que l'iPhone (?!)
