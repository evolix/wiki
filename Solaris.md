**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Solaris/OpenIndiana

## Configuration Réseau

Désactivation de NWAM (NetWork Auto Magic), qui n'est pas pertinent sur un serveur (pas de changement fréquent d'IP ni de Wifi) :

~~~
# svcadm disable network/physical:nwam
# svcadm enable network/physical:default
~~~

Lister les interfaces physiques :

~~~
# dladm show-phys
LINK         MEDIA                STATE      SPEED  DUPLEX    DEVICE
e1000g0      Ethernet             unknown    1000   full      e1000g0
~~~

Lister les interfaces logiques :

~~~
# dladm show-link
LINK        CLASS     MTU    STATE    BRIDGE     OVER
e1000g0     phys      1500   up       --         --
~~~

Créer une interface logique :

~~~
# ipadm create-ip e1000g0
~~~

/!\ Sous OpenIndiana, l'argument à utiliser est encore _create-if_, il devrait être remplacé par _create-ip_ dans un future release pour s'aligner sur Solaris /!\

Ajouter une IP sur une interface logique :

~~~
# ipadm create-addr -T static -a 192.168.1.12/24 e1000g0/v4
~~~

Elle doit maintenant apparaitre dans les interfaces configurées :

~~~
# ipadm show-addr
ADDROBJ           TYPE     STATE        ADDR
lo0/v4            static   ok           127.0.0.1/8
e1000g0/v4        static   ok           192.168.1.12/24
lo0/v6            static   ok           ::1/128
~~~

Ajouter la route par défaut :

~~~
route -p add default 192.168.1.1
~~~

Les configurations faites avec les commande _dladm_ et _ipadm_ sont par défaut persistantes et survivent au reboot.

Configurer la résolution DNS :

~~~
cp /etc/nsswitch.dns /etc/nsswitch.conf
echo "nameserver 1.2.3.4" >> /etc/resolv.conf
~~~

## Gestion des périphériques

Sous Solaris/OpenIndiana, certains utilitaires habituellement utilisés pour lister les périphériques (block, usb, pci) ne sont pas disponibles.
Il sont remplacés par la commande _cfgadm_ qui remplace à peu prés _lspci_, _lsusb_, et _fdisk -l_ :

~~~
# cfgadm 
Ap_Id                          Type         Receptacle   Occupant     Condition
sata6/0::dsk/c3t0d0            disk         connected    configured   ok
sata6/1::dsk/c3t1d0            cd/dvd       connected    configured   ok
sata6/2                        sata-port    empty        unconfigured ok
sata6/3                        sata-port    empty        unconfigured ok
sata6/4                        sata-port    empty        unconfigured ok
[...]
usb2/1                         usb-input    connected    configured   ok
usb2/2                         unknown      empty        unconfigured ok
usb2/3                         unknown      empty        unconfigured ok
[...]
~~~

Les disques connectés ainsi que les ports libres sont bien visibles, pour les curieux ils sont mappés dans /dev/dsk/, mais leur path complet ne sera quasiment jamais utile.

## Utilisation de ZFS

Voir [wiki:HowtoZFS] pour les question d'utilisation générale.

#### iSCSI

Installer les paquet nécessaires :

~~~
# pkg install pkg:/storage-server
           Packages to install:  49
       Create boot environment:  No
Create backup boot environment: Yes
            Services to change:   4

DOWNLOAD                                  PKGS       FILES    XFER (MB)
Completed                                49/49   6848/6848  118.7/118.7

PHASE                                        ACTIONS
Install Phase                              9185/9185 

PHASE                                          ITEMS
Package State Update Phase                     49/49 
Image State Update Phase                         2/2 
~~~

Créer un volume pour recevoir les données (ici on créé un volume nommé "iscsi" de type block -- avec l'option -V -- dans le zpool "test", cf. [wiki:HowtoZFS]) :

~~~
# zfs create -V 90g test/iscsi
# zfs list
[...]
test                      93,3G  4,43G   427M  /export/test
test/iscsi                92,8G  97,3G  16,7K  -
~~~

Ajouter le volume aux LU(s) :

~~~
# sbdadm create-lu /dev/zvol/rdsk/test/iscsi 
Created the following LU:

              GUID                    DATA SIZE           SOURCE
--------------------------------  -------------------  ----------------
600144f0009a020000004f8eb38f0001  96636764160          /dev/zvol/rdsk/test/iscsi
~~~ 

Le rendre visible :

~~~
# stmfadm add-view 600144f0009a020000004f8eb38f0001
~~~

Tout de même s'assurer que le service _target_ iSCSI est actif :

~~~
svcs -a | grep -i iscsi
disabled       14:24:59 svc:/network/iscsi/target:default
online         13:12:57 svc:/network/iscsi/initiator:default
~~~

Si il ne l'est pas :

~~~
svcadm enable -r svc:/network/iscsi/target:default
~~~

Et enfin générer les targets :

~~~
# itadm create-target
Target iqn.1986-03.com.sun:02:21ad694e-4c6a-ec5a-9434-f3ffaf94c3da successfully created
# itadm list-target
TARGET NAME                                                  STATE    SESSIONS 
iqn.1986-03.com.sun:02:21ad694e-4c6a-ec5a-9434-f3ffaf94c3da  online   0
~~~

TODO : Gestion d'accès