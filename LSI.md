**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Télécharger le binaire 32bits suivant (on prendra soin d'installer les ia32-libs avant) : [<http://www.tomasek.cz/software/dellstuff/lsiutil-1.38.tar.bz2>]

Lancer le binaire et suivez ce qui suit :

~~~
machine:~> ./lsiutil

LSI Logic MPT Configuration Utility, Version 1.38, July 6, 2005

1 MPT Port found

Port Name         Chip Vendor/Type/Rev    MPT Rev  Firmware Rev
1.  /proc/mpt/ioc0    XXXXXX                      XXX          XXX

Select a device:  [1-1 or 0 to quit] 1

1.  Identify firmware, BIOS, and/or FCode
1.  Download firmware (update the FLASH)
1.  Download/erase BIOS and/or FCode (update the FLASH)
1.  Scan for devices
10.  Change IOC settings (interrupt coalescing)
13.  Change SAS IO Unit settings
16.  Display attached devices
20.  Diagnostics
21.  RAID actions
22.  Reset bus
23.  Reset target
30.  Beacon on
31.  Beacon off
97.  Reset SAS phy
98.  Reset SAS link
99.  Reset port

Main menu, select an option:  [1-99 or e for expert or 0 to quit] 21

1.  Show volumes
1.  Show physical disks
1.  Get volume state
23.  Replace physical disk
30.  Create volume
31.  Delete volume
32.  Change volume settings

RAID actions menu, select an option:  [1-99 or e for expert or 0 to quit] 32

Volume:  [0-1 or RETURN to quit] 0

Volume 0 Settings:  write caching disabled, auto configure, priority resync
Volume 0 draws from Hot Spare Pools:  

Enable write caching:  [Yes or No, default is No] Yes
Offline on SMART data:  [Yes or No, default is No]
Auto configuration:  [Yes or No, default is Yes]
Priority resync:  [Yes or No, default is Yes]
Hot Spare Pools (bitmask of pool numbers):  [00 to FF, default is 01]

RAID actions menu, select an option:  [1-99 or e for expert or 0 to quit] 0

Main menu, select an option:  [1-99 or e for expert or 0 to quit] 0

Port Name         Chip Vendor/Type/Rev    MPT Rev  Firmware Rev
1.  /proc/mpt/ioc0    LSI Logic SAS1068 B0      105      000a310

Select a device:  [1-1 or 0 to quit] 0
~~~

Rebootez la machine pour que les modifications prennent effet !