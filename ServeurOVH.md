---
categories: hebergeur
title: Serveur OVH
...

* Documentation : <https://help.ovhcloud.com/csm/fr-home>
* Travaux / status : <https://www.status-ovhcloud.com/>
* Espace client : <https://www.ovh.com/auth/>
* Vue des baies par datacenter : <http://vms.status-ovhcloud.com/>
* Latence réseau OVH : <https://smokeping.ovh.net/smokeping>
* Statut de cette page : test

[OVHcloud](https://www.ovhcloud.com/fr/) est un hébergeur créé en 1999 (5 ans avant Evolix ;).


## Infos sur un serveur dédié

Si l'on modifie le reverse DNS du serveur, il apparaîtra ainsi dans la liste « Bare Metal Cloud > Serveurs dédiés ».
…sauf si l'on modifie le "Nom" (accessible par le propriétaire administratif du serveur), c'est lui apparaîtra.
Si le nom et le reverse DNS ont été modifié, on retrouvera le nom original du serveur "nsNNNN.ip-NN-NN-NN.eu" dans l'URL !


## Partitionnement

Historiquement OVH ne permet pas de faire un partitionnement avancé.
Nous passons donc en mode « rescue » et nous utilisons des scripts disponibles sur <https://gitea.evolix.org/evolix/rescue-install>


## DNS

OVH ne propose pas deux serveurs DNS fiables. Nous préconisons l'installation de [Bind](HowtoBind) et l'utilisation du `/etc/resolv.conf` suivant :

~~~
nameserver 127.0.0.1
nameserver 213.186.33.99
~~~


## Additional IP (IP FailOver)

OVH propose l'utilisation d'adresses IP supplémentaires (à l'unité ou en bloc) sur un serveur (ou un vRack)
et potentiellement "transportables" sur un autre serveur, d'où leur ancien nom _IP FailOver_.

Pour en ajouter, il faut passer par « Commander des IP » et se laisser guider. Quelques notes :

* Si les adresses IP sont destinées au vRack ou une utilisation pas encore connue, on peut sélectionner le service « Parking ».
* Pour les blocs, on peut choisir entre un bloc /30 à un bloc /24

Pour une IPFO unique, on l'ajoutera au serveur via `/etc/network/interfaces` (attention, le masque est bien un /32) :

~~~
auto eth0:0
iface eth0:0 inet static
        address 192.0.43.2/32
~~~

On peut déléguer l'infogérance des IPFO en se rendant sur la page « Gestion des contacts » (accessible via le menu du compte en haut à droite du manager OVH), et en ajoutant un contact technique à la ligne concernant l'IP failover.

Pour utiliser les IPFO dans des VM, il faut aller dans l'onglet « Additional IP » sur la page « Network > IP » et l'on pourra ajouter une MAC virtuelle, qu'il faudra ensuite reporter dans la configuration libvirt de l'interface réseau de la VM. Cf <https://help.ovhcloud.com/csm/en-dedicated-servers-network-virtual-mac?id=kb_article_view&sysparm_article=KB0043799>

## Configuration réseau

Nous conseillons de supprimer les paquets `cloud-init` ( optionnelement `cloud-guest-utils cloud-image-utils cloud-utils` ) et `netplan.io` comme ceux-ci vont interferer avec la configuration via `/etc/network/interface`

### Configuration WAN

Il faut utiliser la gateway indiqué dans la console OVH

Dans le cas d'un KVM, il faut mettre `bridge_hw`, voir [HowtoKVM](/HowtoKVM#configuration-réseau) note 3.

### vRack

OVH propose l'option [vRack](https://www.ovhcloud.com/fr/network/vrack/) sur la plupart des serveurs dédiés.
Les serveurs avec cette option disposent d'une interface réseau physique dédiée (par exemple `eth1` dédiée au vRack).

C'est un réseau privé de niveau 2 permettant d'interconnecter directement ses serveurs dédiés, même si ils sont en Allemagne ou Canada.
On peut ainsi monter jusqu'à 4000 VLANs dans un vRack et cloisonner différents réseaux.
On peut également utiliser une plage d'adresses IP publiques sur un vRack permettant ainsi à des VMs écoutant uniquement sur le réseau vRack d'avoir une adresse IP publique.

### Configuration administrative d'un vRack

Le propriétaire administratif du serveur dédié doit créer un « Réseau Privé vRack » (nommée "pn-NNNNN").
Il pourra ensuite « attacher » ce « Réseau Privé vRack » à l'interface réseau du serveur dédié.

Si il y a besoin d'IP publiques, le propriétaire administratif du serveur dédié doit commande une IP ou un bloc d'adresses IP (anciennement IP FailOver, cf plus haut).
Dans le menu « Réseau Privé vRack > pn-NNNNN » il pourra ensuite sélectionner l'IP dans « Services éligibles » et « Ajouter » à « Votre vRack ».

Cf <https://help.ovhcloud.com/csm/fr-dedicated-servers-ip-block-vrack?id=kb_article_view&sysparm_article=KB0043347>

Attention, on conseille bien que ça soit le propriétaire administratif du serveur qui fasse ces opérations, et NON le contact technique.
Si c'est le contact technique qui effectue ces opérations, cela risque de poser problème en cas de changement de contact technique.

Malheureusement pour les « Réseau Privé vRack » ("pn-NNNNN"), la notion de contact technique n'est pas visible dans le manager OVH.
Donc par défaut, le "pn-NNNNN" ne sera pas visible par le contact technique.
MAIS on peut demander à OVH de le faire :

* le propriétaire administratif doit ouvrir un ticket qui demande de mettre un NIC en contact technique pour son "pn-NNNNN" et transmettre au contact technique le n° du ticket
* le contact technique peut ensuite ouvrir un ticket en demande l'accès au "pn-NNNNN" en citant le NIC propriétaire et le n° du ticket ci-dessus

#### Configuration technique d'un vRack

Nous prenons l'exemple de l'interface `eth1` dédiée au vRack.

Exemple de configuration  avec :

* eth1.100 pour du DRBD
* eth1.200 mappé sur br_lan pour un bridge LAN
* et eth1 mappé sur br_wan pour un bridge WAN pour les adresses IP publiques

/!\\ Si l'on veut que la machine physique dispose d'une des adresses IP publiques, il faut ajouter une route spécial (type multi-WAN).

~~~
# echo 2 vrack >> /etc/iproute2/rt_tables
~~~

`/etc/network/interfaces` :

~~~
auto eth1
iface eth1 inet manual

# LAN eth1.100 vRack VLAN 100 for DRBD
auto eth1.100
iface eth1.100 inet static
    address 192.168.0.10/24

# LAN br_lan vRack VLAN 200 for VMs LAN
auto eth1.200
iface eth1.200 inet manual

auto br_lan
iface br_lan inet static
    bridge_ports eth1.200
    address 10.0.0.10/24

# WAN br_wan vRack no VLAN
auto br_wan
iface br_wan inet static
    bridge_ports eth1
    address IP_VRACK/NETMASK_VRACK
    ## Need to work: echo 2 vrack >> /etc/iproute2/rt_tables
    post-up ip route add IP_VRACK/NETMASK_VRACK dev br_wan table vrack
    post-up ip route add default via IP_GW_VRACK dev br_wan table vrack
    post-up ip rule add from IP_VRACK/NETMASK_VRACK table vrack
    post-up ip rule add to IP_VRACK/NETMASK_VRACK table vrack
~~~

IP_GW_VRACK étant la dernière adresse IP du bloc (`HostMax` dans `ipcalc` ;)

Plus d'infos sur <https://help.ovhcloud.com/csm/fr-dedicated-servers-ip-block-vrack?id=kb_article_view&sysparm_article=KB0043347>


### retour de systemd-networkd à ifupdown

Les serveurs OVH sont désormais installés avec le réseau géré par `systemd-networkd`.

En l'état, nous reconfigurons cette partie pour revenir à `ifupdown` et son fichier `/etc/network/interfaces`.

Les instructions sont sur la page [HowtoDebian/Reseau#migrer-de-networkd-%C3%A0-ifup]()


### IPv6

L'IPv6 d'un serveur dédié OVH est notée dans le manager sous la forme d'un réseau /64, néanmoins la gateway se trouve en dehors de ce réseau. Il est donc nécessaire de configurer une route supplémentaire vers cette même gateway.

Voici la configuration conseillée dans `/etc/network/interfaces` :

~~~
iface eth0 inet6 static
        address 2001:1234:5:6789::1/64
        post-up /sbin/ip -6 route add 2001:1234:5:67ff:ff:ff:ff:ff dev eth0
        post-up /sbin/ip -6 route add default via 2001:1234:5:67ff:ff:ff:ff:ff
        pre-down /sbin/ip -6 route del default via 2001:1234:5:67ff:ff:ff:ff:ff
        pre-down /sbin/ip -6 route del 2001:1234:5:67ff:ff:ff:ff:ff
~~~

Et les paramètres à ajouter dans `/etc/sysctl.conf` :

~~~
net.ipv6.conf.all.autoconf = 0
net.ipv6.conf.all.accept_ra = 0
~~~

Puis exécuter `sysctl -p`

Voir la documentation OVH : <https://help.ovhcloud.com/csm/fr-dedicated-servers-network-ipv6?id=kb_article_view&sysparm_article=KB0043775>.

Si besoin de désactiver IPv6, voir <https://wiki.evolix.org/HowtoDebian/Reseau#d%C3%A9sactiver-ipv6>

#### Problème "Waiting for DAD... Timed out"

Certains serveurs OVH rencontrent des problèmes avec la détection de duplication d'adresse IPv6 (DAD), ce qui provoque l'échec de `networking.service`. Ce problème provient d'un trop faible interval (ou nombre de tentative) utilisé par défaut. Cela peut être corrigé en ajoutant `dad-interval 0.2` à la configuration de l'interface :

```
iface eth0 inet6 static
        address 2001:1234:5:6789::1/64
        post-up /sbin/ip -6 route add 2001:1234:5:67ff:ff:ff:ff:ff dev eth0
        post-up /sbin/ip -6 route add default via 2001:1234:5:67ff:ff:ff:ff:ff
        pre-down /sbin/ip -6 route del default via 2001:1234:5:67ff:ff:ff:ff:ff
        pre-down /sbin/ip -6 route del 2001:1234:5:67ff:ff:ff:ff:ff
        dad-interval 0.2
```

### Firewall Cisco ASA

Il est possible d'associer à un serveur dédié un firewall Cisco ASA, en mode transparent. Attention le mode transparent implique qu'il ne sera pas possible de monter des VPNs. OVH ne supporte pas encore le mode routé.
La configuration du firewall est disponible en HTTPs (client java) ou en CLI via ssh.


## Cloud Disk Array

[Cloud Disk Array (CDA)](https://www.ovh.com/fr/cloud-disk-array/) est une offre de stockage qui se repose sur Ceph.

Depuis l’interface OVH, on peut accéder à l’interface d’administration depuis : `Bare Metal Cloud` puis sur le côté gauche dans `Plateformes et services`. Le _cluster_ Ceph devrait être listé.

Une fois sur l’interface du _cluster_ Ceph, on a 4 onglets :

* « Sommaire » - donne les informations générales sur le cluster ;
* « Utilisateurs » - gérer les comptes utilisateurs et leurs permissions pour chaque pool ;
* « Contrôle d’accès IP » - gérer les plages d’IP autorisées ;
* « Pool » - créer et supprimer les pools.
    * Au moment où nous avons créé des _pools_, le nombre de réplication des objets était forcé à 3.

Note : s’il y a un pare-feu sur le serveur OVH qui sera un client du _cluster_ Ceph, il faut faire les autorisations réseaux suivantes sur le client :

* TCP sortie vers les IP des moniteurs sur le port 6789,
    * ces IP sont accessibles depuis le sommaire ;
* TCP sortie vers n’importe quelle IP (ou le réseau 10.0.0.0/8) sur les ports 6800:7300,
    * Les IP des OSD ne sont pas renseignées depuis l’interface du _cluster_ Ceph, donc il est nécessaire de faire une autorisation en sortie _très_ vaste.

Note : le _cluster_ Ceph sera au moins en version 14. Il est important que Ceph ⩾ 12 soit installé sur le client pour éviter les problèmes d’incompatibilité.


## Antispam OVH

La détection de spams se fait en temps réel sur les emails envoyés.

Lorsqu'une adresse IP est identifiée comme émettrice de spam, le port 25 est bloqué via VAC. On peut notamment vérifier le blocage sur l'interface d'admin beta : <https://www.ovh.com/manager/dedicated/login.html>

Le déblocage se fait via l'interface API : <https://api.ovh.com/console/#/ip>

Se connecter avec des identifiants OVH. Par la suite , il faut sélectionner "/ip" puis "/ip/{ip}/spam/{ipSpamming}/unblock" (icône POST en début de ligne), entrer l'IP de son serveur dans les zones "ip" et "ipSpamming" puis cliquer sur "Execute"

Vous pouvez utiliser le  lien <https://api.ovh.com/console/#/ip>  pour vérifier si l'IP a été bien débloqué ou pas.

Si l'IP est de nouveau détectée comme "spammeuse", elle sera bloquée de nouveau, le déblocage sera alors possible après 24h.
Si l'IP est détectée une nouvelle fois, elle sera bloquée pour 30 jours.

Note : via le nouveau manager, on peut le faire directement via <https://www.ovh.com/manager/dedicated/index.html#/configuration/ip>


## Utilisateur Unix

OVH crée un utilisateur `debian` à l'installation, nous conseillons de le supprimer.


## /root/.ssh/authorized_keys

OVH ajoute une ligne dans `/root/.ssh/authorized_keys` du type `Please login as the user "debian" rather than the user "root"`.
Comme nous savons configurer SSH ;) nous supprimons cette ligne.


## Vérifications du serveur

Outre un reboot du serveur en mode _rescue_, voici certaines vérifications possibles.

### Réseau

~~~
# ifconfig | grep error
          RX packets:48653845 errors:0 dropped:0 overruns:0 frame:0
          TX packets:68256134 errors:0 dropped:0 overruns:0 carrier:0
          RX packets:13277345 errors:0 dropped:0 overruns:0 frame:0
          TX packets:13277345 errors:0 dropped:0 overruns:0 carrier:0
~~~

### Disques / RAID

~~~
# tw_cli  info C0

Unit  UnitType  Status         %Cmpl  Stripe  Size(GB)  Cache  AVerify  IgnECC
------------------------------------------------------------------------------
u0    RAID-1    OK             -      -       698.637   ON     -        -

Port   Status           Unit   Size        Blocks        Serial
---------------------------------------------------------------
p0     OK               u0     698.63 GB   1465149168    5QD50X85
p1     OK               u0     698.63 GB   1465149168    5QD4N9L4

# smartctl -a -d 3ware,0 /dev/twe0 | egrep 'Serial|Error'
Serial Number:    5QD50X85
Error logging capability:        (0x01) Error logging supported.
  1 Raw_Read_Error_Rate     0x000f   113   100   006    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   084   060   030    Pre-fail  Always       -       260318814
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
200 Multi_Zone_Error_Rate   0x0000   100   253   000    Old_age   Offline      -       0
SMART Error Log Version: 1
No Errors Logged
# smartctl -a -d 3ware,1 /dev/twe0 | egrep 'Serial|Error'
Serial Number:    5QD4N9L4
Error logging capability:        (0x01) Error logging supported.
  1 Raw_Read_Error_Rate     0x000f   104   100   006    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   078   060   030    Pre-fail  Always       -       71534169
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
200 Multi_Zone_Error_Rate   0x0000   100   253   000    Old_age   Offline      -       0
SMART Error Log Version: 1
No Errors Logged
~~~

Il ne faut plus utiliser `megasasctl` pour interagir avec le contrôleur RAID, car ce logiciel n'est plus maintenu et peu sorti des faux négatifs.

### CPU/RAM

~~~
# grep -ri Oops /var/log/
# grep -ri threshold /var/log/
# grep -ri Segfault /var/log/
~~~


## Support OVH (à confirmer)

Le numéro actuel 1007 (+33 9 72 10 10 07) est efficace comparé aux années précédentes. Pour passer le menu et aller directement au support serveur : 3 1 2 pour un incident.
Pour des machines chez OVH Canada, il faut passer par leur support Canadien : +1 855-684-5463 (Fonctionne en français et anglais)

Déprécié : L'astuce est de joindre le 09.74.53.13.23 (non surtaxé) où un opérateur a simplement un accès à l'outil de tickets mais peut relancer les "vrais" techniciens par messagerie instantannée. En cas d'urgence, l'idéal est donc d'ouvrir un ticket, de relancer immédiatemment au 09.74.53.13.23 puis de prier.

Pour des renseignements avant de passer commande (stock, etc.) : sales@ovh.net

