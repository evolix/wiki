---
categories: securité
title: Howto Chiffrement avec LUKS
...

* Documentation : <https://gitlab.com/cryptsetup/cryptsetup/wikis>

LUKS (Linux Unified Key Setup) est le standard pour le chiffrement de disque sous Linux. Il permet le chiffrement de disque ou de partitions grâce au module noyau [DM-CRYPT](https://gitlab.com/cryptsetup/cryptsetup/wikis/DMCrypt) et l'outil [crypsetup](https://gitlab.com/cryptsetup/).

## Installation

~~~
# apt install cryptsetup

$ /usr/sbin/cryptsetup --version
cryptsetup 2.6.1 flags: UDEV BLKID KEYRING KERNEL_CAPI 

$ /sbin/modinfo dm_crypt
filename:       /lib/modules/6.1.0-20-amd64/kernel/drivers/md/dm-crypt.ko
license:        GPL
description:    device-mapper target for transparent encryption / decryption
author:         Jana Saout <jana@saout.de>
depends:        dm-mod
retpoline:      Y
intree:         Y
name:           dm_crypt
vermagic:       6.1.0-20-amd64 SMP preempt mod_unload modversions 
sig_id:         PKCS#7
signer:         Debian Secure Boot CA
sig_key:        32:A0:28:7F:84:1A:03:6F:A3:93:C1:E0:65:C4:3A:E6:B2:42:26:43
sig_hashalgo:   sha256
signature:      […]
~~~

## Création

On peut créer un volume chiffré LUKS en renseignant une passphrase ainsi :

~~~
# cryptsetup --verbose --verify-passphrase luksFormat /dev/sdz

WARNING!
========
This will overwrite data on /dev/sdz irrevocably.

Are you sure? (Type uppercase yes): YES
Enter LUKS passphrase:
Verify passphrase:
Command successful.

# cryptsetup luksOpen /dev/sdz crypt_sdz
Enter LUKS passphrase:
key slot 0 unlocked.
Command successful.

# mkfs.ext4 /dev/mapper/crypt_sdz
~~~

On peut aussi utiliser un fichier comme clé de chiffrement :

~~~
# dd if=/dev/random of=/root/.foo.key bs=1 count=256
# cryptsetup --verbose --key-size=256 luksFormat /dev/sdz /root/.foo.key
~~~

Enfin, il est aussi possible de faire du déchiffrement automatique avec d'autres outils, comme [Clevis](./HowtoClevis)

## Utilisation

Voir si une partition est de type LUKS :

~~~
# cryptsetup isLuks /dev/sdz
# echo $? # la commande n’affiche rien, il faut regarder son code de retour (0 = ok, c'est un volume LUKS)
0

# cryptsetup luksDump /dev/sdz
LUKS header information
Version:       	2
Epoch:         	3
Metadata area: 	16384 [bytes]
Keyslots area: 	16744448 [bytes]
UUID:          	5c0bd06f-5bf1-4ccc-aa66-a1c72b78a592
Label:         	(no label)
Subsystem:     	(no subsystem)
Flags:       	(no flags)
[…]
~~~

Déchiffrer/Ouvrir une partition LUKS :

~~~
## cryptsetup luksOpen <Volume_Chiffré> <Nom_Du_Volume_Déchiffré>
# cryptsetup luksOpen /dev/sdz sdz_crypt
~~~

Si la partition LUKS est protégée par un fichier :

~~~
# cryptsetup luksOpen --key-file /root/.foo.key /dev/sdz sdz_crypt
~~~

Informations sur la partition chiffrée :

~~~
# cryptsetup status sdz_crypt
/dev/mapper/sdz_crypt is active and is in use.
  type:    LUKS2
  cipher:  aes-xts-plain64
  keysize: 512 bits
[…]
~~~

Stopper le déchiffrement d'une partition LUKS :

~~~
# cryptsetup luksClose sdz_crypt
~~~

Pour « resizer » une partition LUKS (quand on a, par exemple, agrandi la partition qui supporte le volume chiffré) :

~~~
# cryptsetup resize sdz_crypt
~~~


## Configuration de /etc/crypttab

Le fichier `/etc/crypttab` permet de référencer les volumes chiffrés et leur point de montage d'un système. 

Ainsi, ils pourront être utilisés pour : 

* Demander la passphrase au démarrage de la machine (Attention, il faut donc être présent physiquement, donc le cas d'usage est souvent un ordinateur/station de travail)
* Déchiffrement avec un fichier de clé
* Demander la passphrase avec l'outil `cryptdisks_start` pour monter manuellement la partition
* Du déchiffrement automatique - Voir [HowtoClevis](./HowtoClevis)

~~~
# <target name>	<source device>		<key file>	<options>
sdz_crypt UUID=cd81744f-5254-5ff0-b649-3d0143827924 none luks,noauto
~~~

**IMPORTANT** : Il ne faut pas oublier l'option `noauto` pour ne pas que la passphrase soit demandé au démarrage.


La syntaxe pour utiliser un fichier de clé pour déchiffrer automatiquement au démarrage : 

~~~
# <target name>	<source device>		<key file>	<options>
sdz_crypt UUID=12345678-1234-5678-123456789abc /root/.foo.key luks
~~~



Quand l'option `noauto` est présente, on peut manuellement demander d'ouvrir le volume chiffré. Il faudra saisir la passphrase :

~~~
# cryptdisks_start sdz_crypt
Starting crypto disk...cryptsetup: 
sdz_crypt (starting)...Please unlock disk sdz_crypt: 
sdz_crypt (started)...done.

# mount /dev/mapper/sdz_crypt /mnt
~~~


## Gestion des passphrases

LUKS permet d'avoir plusieurs passphrases pour un même volume chiffré.

Pour ajouter une passphrase :

~~~
# cryptsetup luksAddKey /dev/sdz
Enter any LUKS passphrase:
key slot 1 unlocked.
Enter new passphrase for key slot:
Verify passphrase:
Command successful.
~~~

> *Note* : sur d'anciennes versions de *cryptsetup*, il fallait avoir la partition non déchiffrée pour pouvoir ajouter une passphrase : <http://bugs.debian.org/460409>

Pour supprimer une passphrase, on note son numéro avec :

~~~
# cryptsetup luksDump /dev/sdz
~~~

Puis on la supprime avec la commande ( inscrire la passphrase à supprimer ):

~~~
# cryptsetup luksRemoveKey  /dev/sdz
Enter LUKS passphrase to be deleted: 
~~~

Pour tester une _passphrase_ :

~~~
# cryptsetup luksOpen --test-passphrase /dev/sdz
# echo $? # la commande n’affiche rien en cas de succès
~~~

## Sauvegarde

Si l'entête du conteneur LUKS est corrompu, ceci rend la partition inutilisable.

On peut sauvegarde l'entête d'un conteneur LUKS ainsi :

~~~
# cryptsetup luksHeaderBackup --header-backup-file backup.txt /dev/sdz
~~~

Pour Restaurer l'entête :

~~~
# cryptsetup luksHeaderRestore --header-backup-file backup.txt /dev/sdz
~~~



## FAQ

### Command failed: Failed to setup dm-crypt key mapping

Si vous obtenez un message de ce type :

~~~
Command failed: Failed to setup dm-crypt key mapping.
Check kernel for support for the aes-cbc-essiv:sha256 cipher spec and verify that /dev/sda1 contains at least 133 sectors
~~~

Cela signifie probablement que le module noyau *Device Mapper* n'est pas chargé :

~~~
# modprobe dm-mod
~~~

### Notes sur les algorithmes de chiffrement

Pour utiliser un algorithme de chiffrement spécifique, il faut le préciser au moment de la création de la partition :

~~~
#  cryptsetup --verbose --cipher=aes-cbc-essiv:sha256 --verify-passphrase luksFormat /dev/sdz
~~~

Le chiffrement **aes-cbc-essiv** est le chiffrement par défaut de _cryptsetup_ pour les noyaux supérieurs au 2.6.10 car il corrige une vulnérabilité potentielle du chiffrement _aes-cbc-plain_. Une autre méthode de chiffrement utilisée avec l'algorithme AES (Rijndael) est le mode XTS, qui est réputé plus résistant aux attaques par watermarking, mais nécessite un module spécifique (judicieusement nommé _xts_) et une clef d'initialisation du double de la taille de la clef finale (voir <http://en.wikipedia.org/wiki/Disk_encryption_theory#XEX-based_tweaked-codebook_mode_with_ciphertext_stealing_.28XTS.29)>

Les autres algorithmes dignes d’intérêt sont _twofish_ et _serpent_, deux compétiteurs de l'AES face à Rijndael.

### Benchmark

On peut vérifier la capacité du processeur avec les différents algorithmes en utilisant `cryptsetup benchmark`. Exemple de sortie

~~~
# cryptsetup benchmark
# Tests are approximate using memory only (no storage IO).
PBKDF2-sha1       992969 iterations per second
PBKDF2-sha256     567411 iterations per second
PBKDF2-sha512     455111 iterations per second
PBKDF2-ripemd160  564965 iterations per second
PBKDF2-whirlpool  214169 iterations per second
#  Algorithm | Key |  Encryption |  Decryption
     aes-cbc   128b   564.0 MiB/s  2303.1 MiB/s
 serpent-cbc   128b    68.2 MiB/s   447.8 MiB/s
 twofish-cbc   128b   161.1 MiB/s   293.7 MiB/s
     aes-cbc   256b   421.8 MiB/s  1716.7 MiB/s
 serpent-cbc   256b    75.2 MiB/s   473.0 MiB/s
 twofish-cbc   256b   149.4 MiB/s   275.1 MiB/s
     aes-xts   256b  2025.6 MiB/s  2048.6 MiB/s
 serpent-xts   256b   465.4 MiB/s   438.9 MiB/s
 twofish-xts   256b   247.2 MiB/s   258.3 MiB/s
     aes-xts   512b  1503.3 MiB/s  1573.4 MiB/s
 serpent-xts   512b   491.9 MiB/s   396.5 MiB/s
 twofish-xts   512b   241.4 MiB/s   228.3 MiB/s
~~~

Cela permet aussi de choisir le chiffrement le plus performant, dans cet exemple *aes-xts* avec une clé de 256 bits est le plus performant.

