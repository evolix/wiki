# How to Proxmox

[Proxmox VE](https://www.proxmox.com/en/proxmox-virtual-environment/overview) est une solution open source de virtualisation. Il offre une API
et une interface web afin de permettre de facilement controllé des machines
virtuelles, des conteneurs le stockage, le réseau, la haute disponibilité
et bien d'autres choses.

## Installation

### Prérequis

* Système: Une installation propre de Debian 12
* Accès root: Assurez-vous d'avoir les privilèges root ou utilisez sudo pour les commandes
* Connexion Internet: Nécessaire pour télécharger les paquets Proxmox

### Étapes d'installation

Ajoutez une entrée dans `/etc/hosts` pour votre ip.
Cela signifie également qu'il faut supprimer l'adresse 127.0.1.1 qui pourrait être présente par défaut.
Par exemple, si votre adresse IP est 192.0.2.10 et votre nom d'hôte est prox4m1, alors votre fichier `/etc/hosts` pourrait ressembler à ceci :

~~~
127.0.0.1       localhost
192.0.2.10      prox4m1.proxmox.com prox4m1

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
~~~

###  Ajouter le dépôt et la clé Proxmox

Ajoutez le dépôt Proxmox VE au fichier `/etc/apt/sources.list` de Debian

~~~
# echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bookworm pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list

# wget https://enterprise.proxmox.com/debian/proxmox-release-bookworm.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg 
~~~

### Mettre à jour les dépôts

Mettez à jour la liste des paquets pour inclure les paquets de Proxmox

~~~
# apt update && apt full-upgrade
~~~

### Installer Proxmox VE kernel

Installez le kernel de Proxmox VE

~~~
# apt install proxmox-default-kernel
~~~

Redemarrez ensuite la machine pour boot sur le nouveau kernel

~~~
# systemctl reboot
~~~

Installer les paquets d'utilitaires

~~~
# apt install proxmox-ve postfix open-iscsi -y
~~~

### Suppression du kernel Debian

Proxmox gère son propre kernel, et une mise à jour pourrait créer des troubles si celui de Debian subsiste. Vous devez donc le supprimer du systeme

~~~
# apt remove linux-image-amd64 'linux-image-6.1*'
~~~

Mettez ensuite à jour la configuraton du grub

~~~
# update-grub
~~~

### Recommandé: supprimer le paquet os-prober

Le paquet os-prober scan toutes les partitions de l'hôte afin de creer plusieurs entrée sur le grub. Mais les partitions scannées peuvent inclure celles des machines virtuel, que vous ne voulez surtout pas ajouter au grub.

Si vous n'avez pas installé Proxmox en dual-boot avec un autre OS, supprimez donc le paquet:

~~~
# apt remove os-prober
~~~

### Configurer le réseau

Proxmox nécessite une configuration réseau adéquate. Assurez-vous que votre fichier `/etc/network/interfaces` soit correctement configuré avec un reseau statique (ou manuel).
En cas de mauvaise configuration, Proxmox pourrait ne pas démarrer et une réinstallation du systeme serait probable. 

Vous devez creer un bridge nommé `vmbr0` et y ajouter votre premiere interface, voici un exemple de configuration qui se trouve dans le fichier `/etc/network/interfaces` :

~~~
auto lo
iface lo inet loopback

iface eno1 inet manual

auto vmbr0
iface vmbr0 inet static
        address 192.0.2.10/24
        gateway 192.0.2.254
        bridge-ports eno1
        bridge-stp off
        bridge-fd 0
~~~

## Surveillance

Il est possible de surveiller un cluster proxmox en utilisant https://github.com/nbuchwitz/check_pve

## Utiliser DRBD 9 comme stockage dans PVE avec Linstor

`linstor-proxmox` est un plugin Proxmox qui permet de gérer un stockage DRBD, avec Linstor. Il permet de répliqué les volumes des VM et de conteneur dans tous les nœuds Proxmox où Linstor est installé.

Il permet aussi de migrer en direct les VM et les conteneurs, sans interruption de service, sans besoin d'avoir un SAN centralisé.
Linstor permet également d'utiliser toutes les fonctionnalités de haute disponibilité de Proxmox.

### Installation de `Proxmox VE Kernel Headers`

Avant d'installer le module kernel pour DRBD 9, il faut s'assurer de que les 'Kernel Headers' soit bien installés :

~~~
# apt install proxmox-default-headers
~~~ 

### Configuration de dépôt LINBIT public

LINBIT fourni un dépôt public à utiliser avec Proxmox VE. Ce dépôt fourni le plugin Linstor pour Proxmox, mais également toute la pile DRBD9, Linstor et ses utilitaires. On indique dans la variable `$PVERS` avec la version majeure de PVE sur lequel on veux ajouter le dépôt.

~~~
# wget -O /tmp/package-signing-pubkey.asc https://packages.linbit.com/package-signing-pubkey.asc
# gpg --yes -o /etc/apt/keyring/linbit-keyring.gpg --dearmor /tmp/package-signing-pubkey.asc
# PVERS=8 && echo "deb [signed-by=/etc/ap/keyring/linbit-keyring.gpg] \
http://packages.linbit.com/public/ proxmox-$PVERS drbd-9" > /etc/apt/sources.list.d/linbit.list
# apt update
~~~

### Installation du plugin Proxmox

~~~
# apt install drbd-dkms drbd-utils linstor-proxmox
~~~


### Initialisation du cluster

Avant d'initialisé le cluster Linstor vérifié les points suivants :

* Le module kernel DRBR9 est bien installé et chargé.
* Le paquet `drbd-utils` est bein installé
* Les outils LVM sont bien installé
* Les paquets `linstor-controller` ou `linstor-satellite` soit bien installé sur les nœuds appropriés.
* Le paquet `linstor-client` soit installé sur le noeud où `linstor-controller` est installé.

Après avoir fait ces vérifications, on peut démarrer le service `linstor-controller` sur l'hôte ou il est installé :

~~~
# systemctl enable --now linstor-controller
~~~

Ensuite on peut démarrer le service `linstor-satellite` sur les autres noeuds, ce service necessite une configuration supplémentaire à configurer vace la commande `systemctl edit linstor-satellite.service` :

~~~
[Service]
Type=notify
TimeoutStartSec=infinity
~~~

### Ajouter des nœuds au cluster Linstor

Après avoir initialisé le cluster, la prochaine étape et d'ajouter des noeuds au cluster :

~~~
# linstor node create foo 192.0.2.10
# linstor node create bar 192.0.2.11
~~~

`foo` est le nom que l'on donne au nœud, si on lui donne un hostname, sans mettre d'ip, Linstor va essayer de faire une resolution de nom sur le hostname.

### Lister les noeuds du cluster 

~~~
# linstor node list
~~~

Une fois qu'il y a des neouds ajouter, on peut démarré le service `linstor-satellite` sur chaque nœud (hors le nœud où se trouve le contrôleur) comme ceci :

~~~
# systemctl enable --now  linstor-satellite
~~~

### Spécifier le type de nœud Linstor

Lorsque vous créez un nœud Linstor, vous pouvez spécifier le type de nœud. Le type de nœud est un Label qui indique quel rôle a le serveur dans le cluster Linstor.
Le type de nœud peut être `controller`, `auxiliary`, `combined`, ou `satellite`.
Par exemple, si vous souhaitez de votre noeud Linstor soit à la fois controleur et satellite, vous pouvez le créer de cette manière :

~~~
# linstor node create foo 192.0.2.10 --node-type combined
~~~

L'option `--node-type` est optionnelle, si rien n'est spécifié, par défaut le noeud est de type `satellite`.
On peux changer le type d'un noeud avec la commande suivante :

~~~
# linstor node modify foo --node-type `combined|controller|satellite`
~~~

### Création d'un pool de stockage avec LVM

Un pool de stokage identifie un stockage utilisable par Linstor. Pour regrouper des pools de stockage de plusieurs nœuds, utilisez simplement le même nom sur chaque nœud.

Sur chaque hôte où Linstor est installé, il faut au préalable créer un Volume Group LVM, ou un zPool ZFS, avec de préférence le même nom sur chaque noeuds.

**Attention** : Lorsque on utilise Linstor avec LVM et DRBD, il faut ajouté ce filtre dans la configuration LVM, dans _/etc/lvm/lvm.conf_ :

~~~
global_filter = [ "r|^/dev/drbd|" ]
~~~

Création de Volume Group LVM :

~~~
# vgcreate vg_ssd /dev/nvme0n1
~~~

Après avoir créé le Volume Group sur tous les noeuds, vous pouvez créer les pools de stockage dans le Volume Group, depuis le noeuds défini commme controller comme ceci :

~~~
tic # linstor storage-pool create lvm tic pool_ssd vg_ssd
tic # linstor storage-pool create lvm tac pool_ssd vg_ssd
~~~

Pour lister les storage-pool :

~~~
# linstor storage-pool list
~~~

On créer le `resource-group` lié au `StoragePool` créer précédemment comme ceci :

~~~
# linstor resource-group create pool_ssd_resgroup --storage-pool pool_ssd --place-count 2
~~~

On défini l'option `--place-count` en fonction du nombre de noeud que contient Linstor. 

### Configuration du plugin Proxmox

La configuration de plugin Linstor pour Poxmox se fait dans `/etc/pve/storage.cfg`, ça devrais resembler à cela :

~~~
drbd: drbd_SSD
   content images,rootdir
   controller 192.0.2.10
   resourcegroup pool_ssd
~~~

* `drbd_SSD` : est le nom que l'on donne a notre pool de stockage DRBD configuré dans PVE, on peux mettre se qu'on souhaite.
* `resourcegroup` : on indique le storage-pool que l'on a créé dans Linstor.
* `controller` : on indique l'ip du noeud où se trouve le controlleur Linstor.

## Mise à jour d'une version majeure de Proxmox VE

Lors d'une nouvelle version de Debian, Proxmox mets à jours sa distribution et ses outils.

A chaque version majeure, proxmox publie une documentation d'upgrade avec l'URL sous cette forme :  https://pve.proxmox.com/wiki/Upgrade_from_X_to_X

### Exemple de mise à jour de Proxmox VE 7 vers Proxmox VE 8

Pour la mise à jour de Proxmox VE 7 vers Proxmox VE 8, il faut se référer à la documentation suivante, pour les cas particulier ou les problèmes éventuel : [https://pve.proxmox.com/wiki/Upgrade_from_7_to_8](https://pve.proxmox.com/wiki/Upgrade_from_7_to_8)

Voici les étapes à suivre lors de la mise à jour :

* Migré les VMs critique sur un des autres nœuds Proxmox et arrêter les VMs non critique.
* Vérifié que toutes les mises à jours mineure et de sécurité sont faites sur la version courante.
* Faire la mise à jour soit par SSH soit via la console, directement sur le nœud que l'on veux mettre à jour.
* Utiliser l'outil `pve7to8` qui fait un checklist avant la mise à jour comme ceci :

~~~
# root@proxmoxdev:~#  pve7to8 --full
~~~

On l'exécute avec l'option `--full` pour avoir toutes la checklist complète, il ne faut pas qu'il y est d'erreurs ou de warning, sinon il faut réglé les warnings remontées par l'outil avant de passer à l'étape suivante.

* On change les dépôts pour la nouvelle version de Debian :

~~~
# sed -i 's/bullseye/bookworm/g' /etc/apt/sources.list
~~~

Si on utilise le dépôt Proxmox `No-Subscription` il faut rajouter ce dépôt à `/etc/apt/sources.list` :

~~~
# Proxmox VE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve bookworm pve-no-subscription
~~~

* On peux commencer la mise à jour avec les commandes suivantes :

~~~
# apt update
# apt dist-upgrade
~~~

Durant le processus de mise à jour, des questions au niveau du remplacement de fichier de configuration peuvent être poser, voici les fichiers qui reviennent le plus souvent lors de la mise à jour de Proxmox VE 7 vers Proxmox VE 8 :

* `/etc/issue` -> Proxmox générera automatiquement ce fichier au démarrage, et il n'a que des effets cosmétiques sur la console de connexion.
L'utilisation de la valeur par défaut "Non" (conservez votre version actuellement installée) est sûre ici.

* `/etc/lvm/lvm.conf` -> Si vous n'avez pas apporté de modifications supplémentaires vous-même et que vous n'êtes pas sûr, il est suggéré de choisir "Oui" (installer la version du responsable du paquet) ici.


* Une fois la mise à jour terminé, on peux redémarrer l'hyperviseur.

* On peux re-jouer la commande `pve7to8` et vérifié les warnings.
* On peux se reconnecter à l'interface web, il faut supprimer le cache navigateur avec CTRL + SHIFT + R.