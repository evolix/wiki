**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto HDPARM

HDPARM est un outil pour visualiser et changer les paramètres d'un disque IDE (PATA et SATA).

## Commandes HDPARM utiles

Voir les caractéristiques d'un disque :

~~~
# hdparm -i /dev/hda

/dev/hda:

 Model=QUANTUM FIREBALL SE2.1A, FwRev=API.0D00, SerialNo=632804253921
 Config={ HardSect NotMFM HdSw>15uSec Fixed DTR>10Mbs }
 RawCHS=4092/16/63, TrkSize=32256, SectSize=512, ECCbytes=4
 BuffType=DualPortCache, BuffSize=80kB, MaxMultSect=16, MultSect=off
 CurCHS=4092/16/63, CurSects=4124736, LBA=yes, LBAsects=4124736
 IORDY=on/off, tPIO={min:120,w/IORDY:120}, tDMA={min:120,rec:120}
 PIO modes: pio0 pio1 pio2 pio3 pio4
 DMA modes: sdma0 sdma1 sdma2 mdma0 mdma1 mdma2 udma0 udma1 *udma2
 AdvancedPM=no
 Drive Supports : Reserved : ATA-1 ATA-2 ATA-3

# hdparm -i /dev/sda

/dev/sda:

 Model=INTEL SSDSA2M080G2GC, FwRev=2CV102HD, SerialNo=CVPO037003CR080JGN
 Config={ Fixed }
 RawCHS=16383/16/63, TrkSize=0, SectSize=0, ECCbytes=0
 BuffType=unknown, BuffSize=unknown, MaxMultSect=16, MultSect=1
 CurCHS=16383/16/63, CurSects=16514064, LBA=yes, LBAsects=156301488
 IORDY=on/off, tPIO={min:120,w/IORDY:120}, tDMA={min:120,rec:120}
 PIO modes:  pio0 pio3 pio4 
 DMA modes:  mdma0 mdma1 mdma2 
 UDMA modes: udma0 udma1 *udma2 udma3 udma4 udma5 udma6 
 AdvancedPM=no WriteCache=enabled
 Drive conforms to: ATA/ATAPI-7 T13 1532D revision 1:  ATA/ATAPI-2,3,4,5,6,7
* signifies the current active mode
~~~

Voir les paramètres d'un disque :

~~~
# hdparm /dev/sda

/dev/sda:
 multcount     =  1 (on)
 IO_support    =  1 (32-bit)
 readonly      =  0 (off)
 readahead     = 256 (on)
 geometry      # 9729/255/63, sectors 156301488, start = 0
~~~

Tester les performances d'accès au disque *sans* système de cache (cette opération doit
être faite sur un système inactif et répétée 2 ou 3 fois pour être significative) :

~~~
# hdparm -t /dev/sda

/dev/sda:
 Timing buffered disk reads: 384 MB in  3.01 seconds = 127.37 MB/sec

# hdparm -t /dev/sda

/dev/sda:
 Timing buffered disk reads: 386 MB in  3.01 seconds = 128.45 MB/sec

# hdparm -t /dev/sda

/dev/sda:
 Timing buffered disk reads: 388 MB in  3.01 seconds = 128.84 MB/sec
~~~

Tester les performances d'accès au disque *avec* système de cache (cette opération doit
être faite sur un système inactif et répétée 2 ou 3 fois pour être significative) :

~~~
# hdparm -T /dev/sda

/dev/sda:
 Timing cached reads:   12192 MB in  2.00 seconds = 6099.63 MB/sec

# hdparm -T /dev/sda

/dev/sda:
 Timing cached reads:   16332 MB in  2.00 seconds = 8172.65 MB/sec

# hdparm -T /dev/sda

/dev/sda:
 Timing cached reads:   15110 MB in  2.00 seconds = 7560.25 MB/sec
~~~

Activer le DMA :

~~~
# hdparm -d1 /dev/hda

/dev/hda:
 setting using_dma to 1 (on)
 using_dma    =  1 (on)
~~~

Activer le 32-bit I/O support :

~~~
# hdparm -c1 /dev/hda

/dev/hda:
 setting 32-bit I/O support flag to 1
 I/O support  =  1 (32-bit)
~~~

Désactiver le write cache :

~~~
# hdparm -W0 /dev/sda

/dev/sda:
 setting drive write-caching to 0 (off)
 write-caching =  0 (off)
~~~

## Optimisations d'un disque PATA

On observe les temps d'accès d'un disque :

~~~
# hdparm -tT /dev/hda

/dev/hda:
 Timing buffer-cache reads:   128 MB in  2.79 seconds = 45.88 MB/sec
 Timing buffered disk reads:  64 MB in 12.99 seconds =  4.93 MB/sec
~~~

On observe l'état de votre disque :

~~~
# hdparm  /dev/hda
/dev/hda:
 multcount    =  0 (off)
 I/O support  =  0 (default 16-bit)
 unmaskirq    =  0 (off)
 using_dma    =  0 (off)
 keepsettings =  0 (off)
 nowerr       =  0 (off)
 readonly     =  0 (off)
 readahead    =  8 (on)
 geometry     # 1023/64/63, sectors 4124736, start = 0
 busstate     =  1 (on)
~~~

On choisir d'activer le DMA et le 32 bit :

~~~
# hdparm -d1 -c1 /dev/hda

/dev/hda:
 setting 32-bit I/O support flag to 1
 setting using_dma to 1 (on)
 I/O support  =  1 (32-bit)
 using_dma    =  1 (on)
~~~

Puis on reteste notre disque :

~~~
# hdparm -tT /dev/hda

/dev/hda:
 Timing buffer-cache reads:   128 MB in  2.78 seconds = 46.04 MB/sec
 Timing buffered disk reads:  64 MB in  7.04 seconds =  9.09 MB/sec
~~~
