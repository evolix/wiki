**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto GANDI Hosting

Les serveurs proposés par GANDI Hosting sont des machines virtuelles Xen.
Ils permettent théoriquement l'ajout/suppression de mémoire et core CPU à chaud (mais il faut toujours se méfier, un redémarrage-surprise étant toujours possible).

## Spécificités Gandi

Gandi installe une clé publique dans _/root/.ssh/authorized_keys_ :

~~~
from="217.70.181.*" ssh-dss AAAAB3NzaC1kc3MAAACBAK9NzNMvvimq4+k3wf0cQDfiDDNpdtbOo/IpjKzI7MRwvvIuwdEKZ7Xp7J/sJM3uMuSK1vGpJXkJ3bxFL5e0Wk/GJBP7STFMqPVZZrMfL/uep9DT
/HssOYwtvZheP39PT29NMqXEmwUBrW2yojeol9BNCzxvX0D2oh+fMUmDfKxfAAAAFQC8ylRZUIdLgxGMlP4KRRX1rhd0mQAAAIBohLoFIe2zzWglQMobuKSqfhkTDA3KubgAUzkrA5aGZsPeiSChXOF97ilyKocH
PBDTLbMTyyu27JFAFKiLNB021RQOMtWtsDp6vGi3OQiys+MIUH1AcAid1Q7LijCIcSUQy/yaioOryNa0k7St10IRA3W/q/OyeGgiPz/BdoAdUwAAAIBDBij4WU5EcqjcW+JLuYPJ1Al3EkrA4SNktNNFtdTmfd0h
NKycQ8/wN+sNeLIx9uB4q7OQRLbKpNSW/mODA8japElhSySNWwUi35M+W2/9RbTTvzhD/NRrAmMFpLJd7gT26ITgjQ7G9fYi8VrvxT8092kWntQea9GikDNjKQvsHQ== mgt_gandi
~~~

Cela leur permet de prendre la main en _root_ en cas de nécessité. Attention, pour interdire cela par défaut tout (en gardant la possibilité de l'activer),
on a tendance à positionner _PermitRootLogin no_ dans la configuration du serveur SSH... sauf que Gandi force (au boot notamment) _PermitRootLogin  without-password_ !!!
À savoir...

Gandi installe cinq paquets spécifiques. À part un, on peut les désinstaller car ils servent uniquement à l'installation du système :

~~~
# aptitude purge gandi-hosting-agent gandi-hosting-agent-plugins-internal-debian gandi-hosting-agent-plugins-internal-unix gandi-hosting-agent-schemes
~~~

Note : il faut lancer la commande deux fois à cause d'un conflit lors de la suppression du paquet _gandi-hosting-agent-plugins-internal-unix_

Source : <http://groups.gandi.net/fr/post/gandi.fr.hebergement.expert/22663>

Le paquet _gandi-hosting-vm_ permet d'automatiser certaines actions demandées à partir du panel Gandi, comme le montage automatiques des disques
attachés au serveur ou l'ajout d'adresses IP supplémentaires.
Pour bénéficier des mises-à-jour de ce paquet, on gardera le dépôt _<http://mirrors.gandi.net/gandi/debian_> comme indiqué plus haut.

## Mise-à-jour du noyau

Il suffit d'aller sur le panel Gandi dans les options avancées de son "disque système" et de choisir la version souhaitée.

Il faut également installer les nouveaux modules via la procédure décrite sur <http://wiki.gandi.net/fr/iaas/references/server/kernel_modules>
On pourra lister exhaustivement les modules disponibles via la commande suivante exécutée sur le serveur :

~~~
$ GET <http://mirrors.gandi.net/kernel/> | grep modules | sed 's/^.*a href="\(.*\)".*/\1/'
~~~

Attention, pour les noyaux >= 2.6.32 il faut activer la sortie standard sur *hvc0* (options avancées du panel Gandi)
et s'assurer d'avoir dans la ligne suivante dans le fichier _/etc/inittab_ du serveur :

~~~   
1:2345:respawn:/sbin/getty 38400 hvc0
~~~

Ceci est indispensable pour avoir un mode console utilisable !

Note : il y a parfois des bugs avec les scripts Gandi, notamment dans le montage auto des différents disques :
       ils se montent parfois en tant que /srv/NOM.2 et il est nécessaire de nettoyer /srv/NOM...


## Reboot en cas de faille de sécurité sur le noyau

GANDI prévient et il suffit de stop puis start sa VM via l'interface web.

Voir <https://wiki.gandi.net/fr/iaas/references/server/kernel_changelog>

## Accès console de secours

Pour cela il faut l'activer dans l'interface d'admin, puis l'on pourra avoir un accès console via :

~~~
$ ssh <IP>@console.gandi.net
~~~

## IPv6

Depuis début janvier 2011, GANDI active désormais l'IPv6 par défaut : <http://wiki.gandi.net/fr/hosting/gandi-expert/manage-ipv6>

Pour le désactiver, l'une des méthodes est de lancer ces commandes :

~~~
# sysctl -w net.ipv6.conf.all.autoconf=0
# sysctl -w net.ipv6.conf.all.disable_ipv6=1
# ifconfig eth0 del 2001:4b98:..../64
~~~

Pour que cela reste en place, on ajoutera dans le fichier */etc/sysctl.conf* :

~~~
net.ipv6.conf.all.autoconf = 0
net.ipv6.conf.all.disable_ipv6 = 1
~~~

Pour l'utiliser sous contrôle, on utilisera des règles _ip6tables_. Cela nécessite d'avoir un noyau Linux >= 2.6.32 (voir plus haut) et d'ajuster ces régles.
Pour ne rien laisser par défaut :

~~~
# /sbin/ip6tables -P INPUT DROP
# /sbin/ip6tables -P OUTPUT DROP
# /sbin/ip6tables -P FORWARD DROP
~~~

## Gestion des disques

### Augmenter la taille d'un volume de données attaché

<http://wiki.gandi.net/en/iaas/references/disk/resize/classic>

#### Volume non système

- Cliquer sur augmenter dans l'interface (cela ne reboot pas la VM)

- Une fois l'opération terminée, faire à chaud (sans démontage de la partition) :

~~~
xvdg: detected capacity change from 5368709120 to 10737418240

# resize2fs /dev/xvdg
[...]
VFS: busy inodes on changed media or resized disk xvdg
~~~

ATTENTION : une fois cette opération réalisée, la nouvelle taille sera bien remontée par le système (df)
MAIS il sera tout de même nécessaire de redémarrer pour ne pas se heurter à des "Aucun espace disponible sur le périphérique"


#### Volume non système (DEPRECATED)

Pour les nouveaux volumes (depuis 2011), il suffit de lancer un redimensionnement à chaud de la partition.

Pour les anciens volumes, suivre la procédure suivante :

1. Modifier la taille du volume... mais ATTENTION, CELA PROVOQUE LE REDÉMARRAGE IMMÉDIAT DE LA MACHINE VIRTUELLE !!

2. Redimensionner votre système de fichiers à froid :

~~~
# umount /srv/foodata && e2fsck -f /dev/xvdb -C0
e2fsck 1.41.3 (12-Oct-2008)
Passe 1 : vérification des i-noeuds, des blocs et des tailles
Passe 2 : vérification de la structure des répertoires
Passe 3 : vérification de la connectivité des répertoires
Passe 4 : vérification des compteurs de référence
Passe 5 : vérification de l'information du sommaire de groupe
foodata : 12/327680 fichiers (0.0% non contigus), 55935/1310720 blocs
# umount /srv/foodata && resize2fs /dev/xvdb
resize2fs 1.41.3 (12-Oct-2008)
Resizing the filesystem on /dev/xvdb to 2621440 (4k) blocks.
Le système de fichiers /dev/xvdb a maintenant une taille de 2621440 blocs.
# mount /dev/xvdb /srv/foodata
~~~

Note : il est important de (re)démonter avant chaque opération, car les volumes peuvent se monter "tout seul" (merci GANDI...)

### Volume système (DEPRECATED??)

Cette opération est dangereuse ! Elle consiste notamment à changer la table de partitions à chaud.
Voir les étapes détaillées sur <http://wiki.gandi.net/en/hosting/manage-disk/resize-disk/expert>

Attention, lors du repartitionnement, on préfera utiliser le mode "cylindre" afin d'éviter un message du type "Partition 1 does not end on cylinder".

### Migrer des volumes d'un serveur vers la nouvelle infra de stockage

Pour diverses raisons (meilleure gestion, support des snapshots, etc.) il est conseillé de migrer vos disques vers la nouvelle infra de stockage Gandi.

Pour cela vous devrez :

* éteindre votre serveur,
* copier tous vos volumes associés à votre serveur (ils passeront donc avec la nouvelle infra),
* détacher les anciens volumes de votre serveur
* rattacher tous les volumes copiés à votre serveur
* marquer le nouveau volume système comme "bootable"
* redémarrer votre serveur

Note 1 : pensez bien à marquer votre volume "système" comme bootable

Note 2 : pensez à supprimer vos anciens volumes peu de temps après car vous les payez ;-)

### Créer un serveur de test à partir de snapshots de volumes

On peut désormais créer des snaphots de volume, on peut donc s'en servir pour créer à chaud un serveur "snapshot".

Pour cela vous devrez :

* créer des snapshots de chaque volume associé à votre serveur
* créer des volumes à partir de chaque snapshot créé
* créer un nouveau serveur avec un système quelconque
* éteindre ce nouveau serveur , puis détacher/supprimer le volume système de ce nouveau serveur
* attacher les volumes créés à partir des snapshots à ce nouveau serveur
* marquer le nouveau volume système comme "bootable"
* démarrer ce nouveau serveur

Note 1 : il pourra être utile d'éteindre les démons "cron" ou Postfix (SMTP) si vous copiez un serveur de production

Note 2 : pensez à supprimer vos anciens volumes peu de temps après car vous les payez ;-)

## Ajouter une adresse IP sur un serveur

*WARNING : on déconseille finalement l'ajout d'une 2ème adresse IP, cela fonctionne très mal => l'IP se déactive en permanence, il faut relancement manuellement un _sysctl -p /etc/sysctl.conf_...*

On peut ajouter une "interface" sur un serveur, elle s'ajoute automatiquement sur le serveur.
Mais la particuliarité est qu'une route par défaut est ajoutée pour chaque interface !
On doit donc gérer des paramètres dans le noyau Linux, Notamment désactiver
_rp_filter_ et _arp_filter_ pour toutes les interfaces, et gérer _arp_announce_.

Voici par exemple le fichier _sysctl.conf_ pour 2 interfaces réseau :

~~~
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.default.arp_announce = 0
net.ipv4.conf.lo.arp_announce = 0

net.ipv4.conf.eth1.arp_announce = 0

net.pv4.conf.all.arp_filter = 0
net.ipv4.conf.all.rp_filter = 0
net.ipv4.conf.default.rp_filter = 0
net.ipv4.conf.default.arp_filter = 0

net.ipv4.conf.eth0.rp_filter = 0
net.ipv4.conf.eth0.arp_filter = 0
net.ipv4.conf.eth1.rp_filter = 0
net.ipv4.conf.eth1.arp_filter = 0
~~~

Note : il est conseillé de mettre au moins 1 Mb/s (ou + ??) pour chaque interface.

## Support

Numéro de téléphone : 0170393748 (Choix 2 pour les VMs).