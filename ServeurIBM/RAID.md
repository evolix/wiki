**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Lors de l'installation de Debian, il se peut que la carte controleur RAID ne soit pas détectée correctement.

## Carte controleur LSI Logic MegaRAID SAS 9240-8i

Retour de lspic :

~~~
10:00.0 RAID bus controller: LSI Logic / Symbios Logic Device 0073 (rev 02)
	Subsystem: IBM Device 03b1
	Flags: bus master, fast devsel, latency 0, IRQ 24
	I/O ports at 2000 [size=256]
	Memory at 97b40000 (64-bit, non-prefetchable) [size=16K]
	Memory at 97b00000 (64-bit, non-prefetchable) [size=256K]
	Expansion ROM at 98000000 [disabled] [size=256K]
	Capabilities: [50] Power Management version 3
	Capabilities: [68] Express Endpoint, MSI 00
	Capabilities: [d0] Vital Product Data <?>
	Capabilities: [a8] Message Signalled Interrupts: Mask- 64bit+ Queue=0/0 Enable-
	Capabilities: [c0] MSI-X: Enable- Mask- TabSize=15
	Capabilities: [100] Advanced Error Reporting <?>
	Capabilities: [138] Power Budgeting <?>
	Kernel driver in use: megaraid_sas
~~~

Il faut prendre la dernière version du module sur le [site de LSI](http://www.lsi.com/channel/products/megaraid/sassata/9240-8i/index.html?part_number=MegaRAID+SAS+9240-8i+KIT&x=11&y=9) et extraire le fichier megaraid_sas.ko, et le copier dans _/lib/modules/2.6.26-2-amd64/kernel/drivers/scsi/megaraid/_

Ensuite on peut charger le nouveau module :

~~~
modprobe megaraid_sas
~~~

Enfin, une fois l'installation terminée, il faut à nouveau copier le module sur le système fraichement installé, puis regénérer l'image initrd en ajoutant le chargement de ce module :

~~~
chroot /target /bin/bash
~~~
~~~
echo "megaraid_sas" >> /etc/initramfs-tools/modules
~~~
~~~
update-initramfs -u
~~~
