---
title: Howto Custom ISO
...

Ce Howto a pour but de montrer comment on peut modifier le contenu d'un ISO. En effet, ces derniers sont très utilisées pour partager des systèmes d'exploitations. Par exemple, il y a Ubuntu, Lubuntu, BackBox, Gentoo, Debian Live, etc qui reprènnes ce principe.

# Pré-requis

Pour réaliser les tâches ci-dessous, nous aurons besoin de ces paquets :

~~~
 apt install squashfs-tools genisoimage debootstrap
~~~

# Déconstruction de l'ISO

Après avoir vu comment [https://wiki.evolix.org/HowtoDebian/liveCD](créer un liveCD) ou si l'on a téléchargé [http://ubuntu.univ-nantes.fr/ubuntu-cd/16.04.2/ubuntu-16.04.2-desktop-amd64.iso](un live CD d'installation d'Ubuntu), nous pouvons alors le monter dans un dossier pour qu'il soit accessible en lecture seule.

~~~
mount ubuntu-16.04.2-desktop-amd64.iso /mnt/
~~~

Nous copions son contenu dans un autre dossier dans lequel nous pourrons y apporter les modifications que l'on veut.

~~~
mkdir ~/custom
cp -R /mnt/ ~/custfom
~~~

## Modifier le menu

Lors du premier affichage, il est souvent demandé dans quel mode on souhaite lancer le système. Soit en mode live, en mode rescue ou en mode installation. Nous pouvons par exemple augmenter le temps d'affichage de ce menu. Toutes les modifications devront être réalisées depuis l'isolinux.

~~~
vi isolinux/stdmenu.cfg
menu timeoutrow 60
~~~

Nous vous invitons à lire [http://www.syslinux.org/wiki/](cette documentation) si vous souhaitez aller plus loin.

## Modifier le initrd

Initramfs est une partie du processus de boot et il comprend un system de fichier minimale pour décompresser le kernel et initialise le matériel.

Admettons que nous voulons maintenant modifier le comportement de l'image minimale qui permet entre autre de charger les modules pour le noyau Linux.

~~~
mkdir /tmp/initrd
cd /tmp/initrd
7z e -so ~/custom/casper/initrd.lz | cpio -id
~~~

_En fonction du type de compression, il faudra utiliser gunzip. Comme ceci ´gunzip -c´_

Par exemple on veut rattacher un module au noyau :

~~~
mkdir /tmp/initrd/etc/initramfs-tools/
echo "usbcore" > /tmp/initrd/etc/initramfs-tools/modules
~~~

Ou alors nous voulons éviter de charger le module principal qui permet de faire fonctionner les ports USB.

~~~
echo "blacklist usbcore" > /tmp/initrd/etc/modprobe.d/blacklist.conf
~~~

Une fois ces modifications apportées, nous la reconstruisons.

~~~
find . | cpio -o | gzip -c > ~/custfom/initrd-custom.img
~~~

Une fois l'avoir remis à sa place initiale dans le liveCD et après avoir démarré sur le live CD, une commande intéressante peut être exécuté pour vérifier que le module n'a pas été chargé.

~~~
 lsinitramfs /initrd.img |grep usbcore
~~~

## Modifier le squashfs d'un liveCD

Le Squashfs détient toute l'arborescence compressé d'un système. C'est très pratique lorsqu'il s'agit de partager rapidement son système et vu qu'il est compressé, il est donc léger.

Décompressons-le.

~~~
unsquashfs ~/custom/casper/filesystem.squashfs
~~~

Suite à cette manipulation, le dossier _ squashfs-root_ vient d'apparaitre. Avec facilitée, nous pouvons ajouter par exemple, l'utilisateur toto.

~~~
chroot squashfs-root/ adduser toto
~~~

Utiliser directement la commande _chroot_ permet de personnaliser très rapidement le futur OS. Il n'est donc pas nécessaire de le lier aux dossiers virtuel /proc ou /dev.

Après avoir fini de l'avoir librement personnalisé, nous reconstruisons le squashfs.

~~~
unlink ~/custom/casper/filesystem.squashfs
mksquashfs squashfs-root ~/custom/casper/filesystem.squashfs
~~~

__Attention!__ Il faut d'abord supprimer l'original au risque de dupliquer les fichiers qui ne seront pas pris en compte lors de la reconstruction de l'ISO.

# Reconstruction de l'ISO

Enfin, il nous reste plus qu'a recréer l'ISO.

~~~
genisoimage -D -r -V "NOM_IMAGE" -l -b ~/custom/isolinux/isolinux.bin -c ~/custom/isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../mon-custom.iso ~/custom
~~~

Les options signifient :

* -D est utilisé pour respecter le standard ISO9660.
* -r signifie que l'on utilise des liens symbolique, des inodes, etc
* -V est le nom du volume
* -l autorise l'usage de 31 caractères pour nommer le fichier
* -b indique le chemin du binaire utilisé pour ammorcer l'ISO
* -c est l'option requise pour lister les modes de démarrage
* -no-emul-boot pour éviter d'émuler un disque et executer directement l'image que nous lui avons fournis (isolinux.bin)
* -boot-load-size est présent car certains BIOS représente des problèmes s'ils n'ont pas un multiple de 4 secteurs pour lancer une émulation.
* -boot-info-table détient des données sur l'image CD dont des informations concernant la protection de copies.
* -o le fichier de sortie de l'image
* ~/custom est l'emplacement de l'arborescence système.

__Attention!__ l'emplacement de fichiers binaires tel que isolinux.bin ou boot.cat ne sont pas forcément a la même place.

Une fois reconstruit, on peut voir les informations sur l'ISO de la manière suivante.

~~~
dumpet -i custom.iso
~~~
