---
categories: network
title: Howto RIPE
...


Les démarches administratives pour obtenir un numéro d'AS et des adresses IP auprès du [RIPE (Réseaux IP Européens)](https://www.ripe.net/) relèvent souvent de fantasmes pour les non initiés.
Dans une optique de transparence, démystifions ces opérations.

## Comment devenir LIR

* S'inscrire en tant que LIR sur <https://www.ripe.net/lir-services/member-support/become-a-member>

* Signer les documents d'inscriptions

* Payer (environ 2000 EUR de frais d'accès, puis 1400 EUR par an)

* On obtient un certain nombre de goodies ;-) et d'infos, notamment un objet ORG-* maintenu par le RIPE, vous pouvez dès maintenant faire un whois dessus \o/

~~~    
$ whois ORG-FB42-RIPE

% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to 'ORG-FB42-RIPE'

organisation:   ORG-FB42-RIPE
org-name:       Foo BAR
org-type:       LIR
address:        Foo BAR
address:        42 avenue du Prado
address:        13000 MARSEILLE
address:        FR
e-mail:         info@example.com
mnt-ref:        RIPE-NCC-HM-MNT
mnt-by:         RIPE-NCC-HM-MNT
source:         RIPE # Filtered
~~~ 

 
## Création des premiers objets RIPE

Via <https://apps.db.ripe.net/startup/> on va créer un objet *mntner* et un premier objet *person*.

L'objet mntner créé servira à protéger tous les futurs objets (ils seront "mnt-by" cet objet).

    
    person nic-hdl: JD-RIPE
    
        * person: John DOE
        * address: Foo BAR
        * address: 42 avenue du Prado
        * address: 13000 MARSEILLE
        * address: FRANCE
        * phone: +33 4 12 34 56 78
        * e-mail: jdoe@example.com
        * nic-hdl: JD-RIPE
        * mnt-by: FB42-MNT
        * changed: jdoe@example.com 20110304
        * source: RIPE
    
    maintainer name: FOOBAR-MNT
    
        * mntner: FOOBAR-MNT
        * descr: Maintainer
        * admin-c: JD-RIPE
        * upd-to: jdoe@example.com
        * auth: MD5-PW $1$1234567890abcdefghijklmopqrstuvwxyz/
        * mnt-by: FOOBAR-MNT
        * referral-by: FOOBAR-MNT
        * notify: jdoe@example.com
        * changed: jdoe@example.com 20110304
        * remarks: Accepted the RIPE Database Terms and Conditions
        * source: RIPE
    

On peut ainsi ajouter/modifier des objets sur <https://www.db.ripe.net/fcgi-bin/webupdates.pl> avec le mot de passe de l'objet mntner.


## Demander une première Allocation

Via le [LIR Portal](https://lirportal.ripe.net/), on va demander une première allocation d'adresses IPv4 et IPv6.
Une allocation est une sorte de réserve d'adresses, dans laquelle on pourra assigner des blocs à des clients ou pour son propre usage.

Sur <https://lirportal.ripe.net/requestforms/> : *IPv4 First Allocation Request Form* et *IPv6 First Allocation Request Form*

* En IPv4, vous devriez obtenir une allocation d'un /22 (environ 1000 adresses IPv4).

* En IPv6, vous obtiendez un /32 (des milliards de milliards de blocs).

Pour l'IPv4, le RIPE peut vous poser la question : "Est-ce qu'un /21 vous _suffira_ pendant 6 mois ?" (et oui, la question est inverse de celle attendue)
Ils vous demandront également de vous justifier (vous pourriez recevoir un appel venant des Pays-Bas ;-)
Vous devez surtout justifier de votre intérêt global à devenir LIR et avoir vos propres adresses IP.
Les justifications précises (nombre d'équipements, etc.) se feront lors de la demande d'assignment.

Voici votre ALLOCATED PA IPv4 :
    
    $ whois 3.5.8.0/22

    % This is the RIPE Database query service.
    % The objects are in RPSL format.
    %
    % The RIPE Database is subject to Terms and Conditions.
    % See http://www.ripe.net/db/support/db-terms-conditions.pdf
    
    % Note: this output has been filtered.
    %       To receive output for a database update, use the "-B" flag.
    
    % Information related to '3.5.8.0 - 3.5.11.255'
    
    inetnum:         3.5.8.0 - 3.5.11.255
    netname:         FR-FOOBAR-20110329
    descr:           Foo BAR
    country:         FR
    org:             ORG-FB42-RIPE
    admin-c:         JD-RIPE
    tech-c:          JD-RIPE
    status:          ALLOCATED PA
    mnt-by:          RIPE-NCC-HM-MNT
    mnt-lower:       FOOBAR-MNT
    mnt-routes:      FOOBAR-MNT
    source:          RIPE # Filtered
    
    organisation:   ORG-FB42-RIPE
    org-name:       Foo BAR
    org-type:       LIR
    e-mail:         info@example.com
    address:        Foo BAR
    address:        42 avenue du Prado
    address:        13000 MARSEILLE
    address:        FR
    e-mail:         info@example.com
    mnt-ref:        RIPE-NCC-HM-MNT
    mnt-by:         RIPE-NCC-HM-MNT
    source:         RIPE # Filtered
    
    person:         John DOE
    address:        Foo BAR
    address:        42 avenue du Prado
    address:        13000 MARSEILLE
    address:        FRANCE
    e-mail:         jdoe@example.com
    nic-hdl:        JD-RIPE
    mnt-by:         FOOBAR-MNT
    source:         RIPE # Filtered
    

## Demander un Assignment

Un Assignment consiste à utiliser une partie de son Allocation. On assigne un ensemble d'adresses IP provenant de sa réserve d'adresses allouées
pour un usage précis : cela peut être interne (sa propre infra, des serveurs dédiés standalone, etc.) ou pour des clients (l'assignment sera alors à leur nom).

Le tout premier assignment IPv4 devra être approuvé pour le RIPE. Il se fait via une demande sur le LIR Portal
et devra être justifié très précisément (nombre d'équipements, marques, etc.).

Par la suite, si tout est bien géré (pas d'objets en erreur dans la base RIPE), les assignments suivants seront faits directement en créant
des objets *inetnum* dans la base RIPE. Pour être précis, il faudra attendre 6 mois : c'est le principe de l'Assignment Window (AW)
qui sera à 0 au départ puis évoluera à l'ensemble de votre Allocation (1024 pour un /22).

Voici un ASSIGNED PA IPv4 :

    
    $ whois 3.5.8.0

    % This is the RIPE Database query service.
    % The objects are in RPSL format.
    %
    % The RIPE Database is subject to Terms and Conditions.
    % See http://www.ripe.net/db/support/db-terms-conditions.pdf
    
    % Note: this output has been filtered.
    %       To receive output for a database update, use the "-B" flag.
    
    % Information related to '3.5.8.0 - 3.5.8.255'
    
    inetnum:         3.5.8.0 - 3.5.8.255
    netname:         FB-NET0
    descr:           Foo BAR infrastructure in Marseilles
    country:         FR
    admin-c:         JD-RIPE
    tech-c:          JD-RIPE
    status:          ASSIGNED PA
    mnt-by:          FOOBAR-MNT
    source:          RIPE # Filtered
    

Les assignments IPv6 ne nécessitent aucune approbation du RIPE, même pour les premiers.
On déclarera directement ses objets *inet6num* dans la base  RIPE.

Voici un ASSIGNED PA IPv6 :

    
    $ whois 2a01:4200::/48
    
    inet6num:        2a01:4200::/48
    netname:         FB6-NET0
    descr:           Foo BAR infrastructure in Marseilles
    country:         FR
    admin-c:         JD-RIPE
    tech-c:          JD-RIPE
    status:          ALLOCATED-BY-LIR
    mnt-by:          FOOBAR-MNT
    source:          RIPE # Filtered
    

## Demander un numéro d'AS (Autonomous System)

Si l'on va gérer l'annonce BGP de ses adresses IP obtenues, il est nécessaire d'obtenir un numéro d'AS (Autonomous System).
Cela se fait simplement via le LIR Portal : sur https://lirportal.ripe.net/requestforms/ *AS Number Request Form*.
Peu de justification sera nécessaire, la seule difficulté est que l'on demande de citer 2 numéros AS avec lesquels vous allez connecter :
vous devez citer ici les opérateurs avec lesquels vous avez OU vous comptez avoir un contrat de transit (a priori il n'y a jamais de vérification sur ce point).

Vous allez obtenir un numéro d'AS sur 32 bits :

    
    $ whois AS234567
    
    …
    
    % Information related to 'AS234567'
    
    aut-num:         AS234567
    as-name:         Foo BAR
    descr:           Foo BAR AS
    org:             ORG-FB42-RIPE
    import:          from ASTRANSIT1 accept ANY
    import:          from ASTRANSIT2 accept ANY
    export:          to ASTRANSIT1 announce AS234567
    export:          to ASTRANSIT2 announce AS234567
    admin-c:         JD-RIPE
    tech-c:          JD-RIPE
    mnt-by:          RIPE-NCC-END-MNT
    mnt-by:          FOOBAR-MNT
    source:          RIPE # Filtered
    

## Annoncer ses adresses IP en BGP

Avant d'annoncer ses adresses IP en BGP, il faut créer un objet *route* (ou *route6* en IPv6).

    
    $ whois 3.5.8.0/22
    
    …
    % Information related to '3.5.8.0/22AS234567'
    
    route:           3.5.8.0/22
    descr:           Foo
    origin:          AS234567
    mnt-by:          FOOBAR-MNT
    source:          RIPE # Filtered
    
    
    $ whois 2a01:4200::/32
    
    …
    % Information related to '2a01:4200::/32AS234567'
    
    route6:          2a01:4200::/32
    descr:           Foo
    origin:          AS234567
    mnt-by:          FOOBAR-MNT
    source:          RIPE # Filtered
    

Vous pouvez maintenant configurer vos routeurs BGP pour annoncer vos adresses IP.
Si vous aller gérer des débits de centaines de Gb/s, tournez vous Cisco, Juniper & co (et votre banquier).
Sinon privilégiez les solutions libres comme <http://trac.evolix.net/infogerance/wiki/HowtoQuagga> ou [HowtoOpenBSD/OpenBGPD]()

## Gérer les reverses DNS

<http://www.ripe.net/data-tools/dns/reverse-dns/how-to-set-up-reverse-delegation>

Pour gérer les reverses DNS d'adresses IPv4, il faut ajouter un champ _mnt-domains_ à l'objet *inetnum* (PA ALLOCATED)
puis ajouter un objet *domain* (domain: 8-11.5.3.in-addr.arpa) qui pointe vers vos serveurs DNS.

Attention, il faut préparer vos serveurs DNS avant d'ajouter l'objet *domain* !
Si vous utilisez <http://trac.evolix.net/infogerance/wiki/HowtoBind>, vous allez avoir des zones du type :

    
    zone "8.5.3.in-addr.arpa" {
            type master;
            file "/etc/bind/db.reverse.ripe8";
            allow-query { any; };
            allow-transfer { "foobar"; };
    };
    
    zone "0.0.2.4.1.0.a.2.ip6.arpa" {
        type master;
        file "/etc/bind/db.reverse.ip6.2a01-4200_32";
        allow-query { any; };
        allow-transfer { "foobar"; };
    };
    

Du fait du fonctionnement du reverse DNS, on est obligé de créer de multiples blocs multiples de /24 (ou /16… mais peu probable)

    
    $ whois 8.5.3.in-addr.arpa
    % This is the RIPE Database query service.
    % The objects are in RPSL format.
    %
    % The RIPE Database is subject to Terms and Conditions.
    % See http://www.ripe.net/db/support/db-terms-conditions.pdf
    
    % Note: this output has been filtered.
    %       To receive output for a database update, use the "-B" flag.
    
    % Information related to '8.5.3.in-addr.arpa'
    
    domain:          8.5.3.in-addr.arpa
    descr:           Reverse DNS powered by Foo BAR
    nserver:         ns0.example.com
    nserver:         ns1.example.com
    admin-c:         JD-RIPE
    tech-c:          JD-RIPE
    zone-c:          JD-RIPE
    mnt-by:          FOOBAR-MNT
    source:          RIPE # Filtered
    

## Ajouter un rôle pour gérer les demandes d'abuse

Il faut désormais créer un rôle pour gérer les demandes d'abuse.
On crée donc un objet *role* :

    
    role:           Foo BAR Abuse Department
    address:        Foo BAR
    address:        42 avenue du Prado
    address:        13000 MARSEILLE
    address:        FRANCE
    e-mail:         abuse@example.com
    abuse-mailbox:   abuse@example.com
    nic-hdl:        FAD42-RIPE
    mnt-by:         FOOBAR-MNT
    changed:        jdoe@example.com 20130531
    source:         RIPE
    

Il faut ensuite ajouter ce rôle via un champ _abuse-c_ sur votre objet *organisation*.
Pour cela vous devez passer par le LIR Portal : <https://lirportal.ripe.net/dbobjects/edit/organisation>
(votre objet étant _mnt-by: RIPE-NCC-HM-MNT_)

    
    $ whois ORG-FB42-RIPE
    
    …
    abuse-c:        FAD42-RIPE
    …
    


## Créer un bloc PI (adresses IP indépendantes)

En tant que LIR, on peut créer des blocs d'adresses IP indépendantes pour des clients, qui pourront faire à peu près ce qu'ils veulent de ce bloc
(notamment changer de fournisseur de transit, voire même de changer de LIR).

Malheureusement, ce n'est plus possible en IPv4 à cause de la pénurie des adresses. Cela reste néanmoins possible en IPv6.

Dans un premier temps, il faudra créer un objet *organisation* et *person*, au nom du client qui veut le bloc PI).

Ensuite, sur <https://lirportal.ripe.net/requestforms/> : *IPv6 Provider Independent (PI) Assignment Request Form*

Vous allez devoir justifier votre demande comme pour un Assignment classique (moins essentiel en IPv6).
Vous devrez notamment fournir des informations administratives sur le client et également le contrat que vous avez signé avec le client !
Vous devez d'ailleurs inclure dans ce contrat des mentions obligatoires fournies par le RIPE.

Une fois le bloc obtenu, il faudra principalement gérer les champs suivants : _mnt-routes:_ (pour gérer les routes auprès du RIPE) et _mnt-domains:_ (pour gérer les reverses DNS).

    
    $ whois <bloc obtenu>
    …
    status:         ASSIGNED PI
    mnt-by:         RIPE-NCC-END-MNT
    mnt-lower:      RIPE-NCC-END-MNT
    mnt-by:         FOOBAR-MNT
    mnt-routes:     ??
    mnt-domains:    ??
    source:         RIPE # Filtered
    

