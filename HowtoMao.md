# Howto Mao

Nous avons été initié au Mao à la [DebConf17](https://blog.evolix.com/evolix-a-la-debconf17/)

Le Mao est un jeu de cartes, on conseille d'être entre 4 à 12 joueurs.
Une partie de Mao dure en général de 40 minutes à plusieurs heures.

**ATTENTION, SI VOUS NE CONNAISSEZ PAS LE JEU DU MAO, NE LISEZ PAS LA SUITE CAR CELA GÂCHERAIT COMPLÈTEMENT VOTRE PREMIÈRE PARTIE !!**

Les règles du Mao sont assez bien décrites sur Wikipédia : <https://fr.wikipedia.org/wiki/Mao_(jeu)>

Nous décrivons ci-dessous notre adaptation des règles, dite « variante Evolix » très proche de la « variante DebConf ».

## Début de partie

On utilise des paquets de 52 cartes (sans les _jokers_) : un paquet pour 4 joueurs, deux paquets entre 5 et 8 joueurs, trois paquets entre 9 et 12 joueurs.

Un joueur bat les cartes et donne 5 cartes à chacun, puis annonce : « La Mao a de nombreuses règles, aucune ne doit être personnelle, et cela commence maintenant ! »

## Termes à utiliser

### En jouant

* « Je passe » ou « Passe »
* « Point d'ordre » / « Fin du point d'ordre »
* « Passe une [très [très [très [..]]]] bonne journée ! »
* « Merci [beaucoup [beaucoup [beaucoup [..]]]] ! »
* « C'est le blaireau ! »
* « Ma règle »
* « Denière carte »
* « Mao »

### En sanctionnant

* « Défaut de jouer »
* « Défaut de dire XXX »
* « Défaut de faire XXX »
* « Pas ton tour »
* « Parle »
* « Juron »
* « Mensonge »
* « Triche »
* « Dévoile son jeu »
* « Fausse pénalité »
* « Mensonge », « Tricheur », « Juron », « Prononce le nom de notre grand chef en vain »

## Nouvelles règles

Une nouvelle règle doit s'énoncer simplement, et n'avoir rien de personnel (notamment ne pas favoriser celui qui la dicte comme défavoriser une autre).
