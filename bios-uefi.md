---
title: BIOS VS UEFI
...

Le BIOS ou l'UEFI est le premier programme chargé au démarrage d'un ordinateur. Ils ont pour principale fonctions de détecter le matériel installés et de fournir ces informations au chargeur du boot (ou bootloader).

Ces deux techniques se différencies par leur conception logicielle et certaines de leurs fonctionnalités.

## BIOS

### Description

Le BIOS (Basic Input Output System) est le tout premier programme qui est exécuté lorsque vous allumez votre ordinateur. Il est stocké dans une mémoire morte (ROM) ou dans une mémoire flash (EEPROM) de la carte mère. L’objectif du BIOS est d’initialiser les composants matériels et d'y effectuer quelques tests puis ensuite de démarrer le système d’exploitation qui est stocké sur le disque dur (ex. : Windows, Debian, Mac OS X…).

### Avantages

  * Personnalisation des composants matériel (activation/désactivation des modules, périphériques, ...).
  * Selection de l'ordre de démarrage des périphériques.
  * Gestion de l'alimentation (mise en veille, overclocking, ...).
  * Surveillance du système : températures, vitesse de rotation des disques, ...

### Contraintes

  * Lenteur lors du chargement des périphériques.
  * Reconnaît les disques ayant des **partitions** moins de 2,2To.
  * Supporte nativement le MBR mais pas le GPT.
  * Écrit en langage assembleur.

## UEFI

### Description

L’UEFI (Unified Extensible Firmware Interface) est le remplaçant du BIOS. Le logiciel logé dans la mémoire flash de la carte mère permet de lancer des applications stockées sur le disque dur dans une partition spéciale appelée EFI System Partition (ESP). Parmi ces applications, on trouve généralement un chargeur de système d’exploitation, un utilitaire de tests de mémoire ou bien des outils de restauration.

Bien qu'il présente de nombreux avantages, il gère la rétro-compatibilité si l'on a besoin d'utiliser un disque anciennement utilisé par une machine disposant d'un BIOS. Le contraire n'est pas possible.

### Avantages

  * Détient tous les avantages du BIOS.
  * La gestion des disques de plus de 2,2To.
  * Un démarrage plus rapide de l'ordinateur.
  * Une meilleure prise en compte de la sécurité.
  * Écrit en langage C.

### Contraintes

  * Il faut que le disque dur ait une partition GPT (notons qu'il est extensible jusqu'à 9.4 Zo!).
  * Disponible que sur les machines 64 bits.

# Quelques thermes techniques

Pour éviter de se perdre facilement

  * UEFI legacy : prise en charge de la rétro-compatibilité avec le BIOS. On l'appelle aussi CSM (Compatibilité Support Module).
  * GRUB Legacy : désigne les anciennes versions de Grub inférieur à 1 (paquet grub-legacy dans Debian)
  * GRUB : désigne les versions  de Grub proches de 2. (paquet grub-pc dans Debian ou en fonction de l'architecture processeur : grub-efi-amd64/grub-efi-ia32).
  * BIOS UEFI : ça n'existe pas et c'est un abus de langage car on ne peut avoir qu'un seul BIOS ou UEFI.

Notons que le GRUB est le lanceur d'amorçage de systèmes d'exploitation. Il est appelé juste après le chargement du BIOS ou UEFI.

# Solutions alternatives

Parmis les logiciels libres nous avons ces outils disponibles :

 * [LibreBoot](https://libreboot.org/) (dérivé de la distribution [coreboot](https://www.coreboot.org/vendors.html))

# Webographie

  * [Comprendre simplement les différences entre le bios et l'uefi](https://lecrabeinfo.net/le-bios-et-luefi-pour-les-debutants.html)
  * [Explication des terminologies grub](https://www.debian-fr.org/t/bios-s-et-grub-s-explications-simples-si-possible/67811/2#post_3)
