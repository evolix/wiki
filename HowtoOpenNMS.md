**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto OpenNMS

OpenNMS est un outil de supervision de réseau Open Source. Il est développé en Java (eh oui...) et s’appuie sur le moteur applicatif Jetty et le SGBD PostgreSQL.

# Installation

<http://www.opennms.org/wiki/Installation:Debian>

Il faut d'abord ajouter des dépôts :

~~~
deb <http://debian.opennms.org> stable main
deb-src <http://debian.opennms.org> stable main
~~~

Et ensuite la clé GPG :

~~~
# wget -O /etc/apt/trusted.gpg.d/opennms.asc http://debian.opennms.org/OPENNMS-GPG-KEY
# dos2unix /etc/apt/trusted.gpg.d/opennms.asc
# chmod 644 /etc/apt/trusted.gpg.d/opennms.asc
~~~

L'installation de OpenNMS nécessite :

* Java
* PostgreSQL

Pour PostgreSQL, il faut modifier la méthode d'accès à la bdd dans le fichier _/etc/postgresql/$PGVERSION/main/pg_hba.conf_

~~~
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# OpenNMS change: configure local, IPv4 and IPv6 connections made from localhost to not require authentication

# "local" is for Unix domain socket connections only
local   all             all                                     trust # the default method is peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            trust # the default method is md5
# IPv6 local connections:
host    all             all             ::1/128                 trust # the default method is md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     postgres                                peer
#host    replication     postgres        127.0.0.1/32            md5
#host    replication     postgres        ::1/128                 md5
~~~

Cela demande un redémarrage de PostgreSQL :

~~~
#service postgresql restart
~~~

Enfin on installe *OpenNMS*

~~~
#aptitude install opennms
~~~

## Configuration

Détection de java par *OpenNMS* :

~~~
#/usr/share/opennms/bin/runjava -s
~~~

Création et configuration de la bdd :

~~~
#/usr/share/opennms/bin/install -dis
~~~

Il peut y avoir des problèmes avec les ports utilisés, on peut les modifier dans le fichier de configuration principal _/etc/opennms/opennms.properties_ mais aussi dans d'autres fichiers comme _/etc/opennms/service-configuration.xml_.

## Utilisation

On peut lancer le service :

~~~
#/usr/sbin/service opennms start
~~~

Maintenant pour utiliser OpenNMS il faut se rendre sur :
<http://ip:8980/opennms/>


