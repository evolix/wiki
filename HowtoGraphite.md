---
categories: databases
title: Howto Graphite
...

* Documentation : <http://graphite.readthedocs.io/en/latest/>

Graphite est un système de base de donnée en série temporelle (ou time series database (TSDB)). Il gère la collecte, le stockage et l'affichage des données.

Il est découpé en 3 composants principaux :

* carbon : Le daemon d'ingestion des données
* whisper : Le système de base de donnée (similaire au RRD)
* graphite-webapp : Une application web (Python/Django) pour l'afficage/génération de graphes

## Installation

~~~
# apt install graphite-web graphite-carbon python3-distutils
~~~

## Configuration

### Carbon

Le fichier le plus intéréssant est probablement `/etc/carbon/storage-schemas.conf` car il va définir notament les durées de rétention des données stoquées avec whisper.

Exemple si on souhaite appliquer la politique de rétention suivante à toute les données :

* 1 point toutes les 60 secondes pendant un jour
* 1 point toutes les 5 minutes pendant 7 jours
* 1 point toutes les 20 minutes pendant 4 semaines (~1mois)
* 1 point toutes les 1 heure pendant 2ans

~~~
[default]
pattern = .*
retentions = 60s:1d, 5m:7d, 20m:4w, 1h:2y
~~~

### WebApp

Le fichier de configuration de l'interface web se trouve dans `/etc/graphite/local_settings.py`.
Les choses intéréssantes à modifier sont :

* SECRET_KEY (pour les cookies de CSRF)
* TIME_ZONE
* DATABASES (si vous ne souhaitez pas utiliser sqlite comme base de donnée pour l'interface web (comptes de l'interface))

Après, pour que l'interface web de graphite puisse fonctionner, il faut initialiser la base de donnée et créer un premier utilisateur

~~~
# graphite-manage migrate
# graphite-manage migrate --run-syncdb
# graphite-manage createsuperuser
~~~

Il ne reste plus qu'a configurer le serveur web. Pour Apache2 on peut s'inspirer du fichier de conf fournit dans `/usr/share/graphite-web/apache2-graphite.conf`
Il faut penser à activer le module `wsgi` (avec `a2enmod wsgi` puis redémarrage d'Apache)

Il peut y avoir aussi un souci de permissions sur le dossier de wsgi, dans ce cas là, dans `/etc/apache2/mods-available/wsgi.conf`, il faut set la valeur suivante `WSGISocketPrefix /var/run/wsgi`

#### Problème de démarrage de l'interface web en Debian 12

Il y a actuellement un souci faisiant que l'application web python ne prenne pas en compte  `/etc/graphite/local_settings.py`. Avec les réglages par défaut, celle-ci n'arrivera pas à démarrer.
Un contourneent est d'ajouter une variable d'environnement pour forcer la bonne prise en compte de la configuration.

Dans `/etc/apache2/envvars`, on ajoutera la ligne suivante

```
export GRAPHITE_SETTINGS_MODULE=local_settings
```




## Utilisation



## Tips

### Redimensionnement suite à un changement des règles de rétention

Après un changent de la politique de rétention des données dans `/etc/carbon/storage-schemas.conf`, il faut redimensionnement les fichiers de données existant si on souhaite que la nouvelle politique s'y applique aussi.
Il existe une commande `whisper-auto-resize` qui après s'occupe de tout le redimensionnement

~~~
# whisper-auto-resize /var/lib/graphite/whisper/ /etc/carbon
# chown _graphite: -R /var/lib/graphite/whisper/
~~~

Par défaut, whisper-auto-resize ne fait que lister les fichiers qui seront modifiés et comment la rétention va changer.
Pour le faire, il suffit de rajouter l'argument `--confirm` pour confirmer de manière interactive à chaques fichiers. Ou avec `--doit` pour directement appliquer le redimensionnement.
Il existe aussi une option `--subdir` pour ne cibler qu'une parties données.

**ATTENTION** : L'opération va changer le propriétaire des fichiers. Il ne faut pas oublier de chown après (avec \_graphite sur Debian)
