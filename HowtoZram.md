# Howto zram

<http://en.wikipedia.org/wiki/Zram>

zRAM est un module du noyau Linux présent depuis le noyau 2.6.37.
Il permet de compresser la RAM afin d'éviter de swapper sur le disque.
C'est plus performant de compresser/décompresser dans la RAM que d'écrire sur un disque mécanique ou SSD.

A partir de Debian 10 il suffit d'installer le paquet `zram-tools`

~~~
# apt install zram-tools
~~~

Le paquet active automatiquement le module kernel zram et installe l'unité systemd `zramswap.service` pour qu'il soit activé au démarrage de la machine.

~~~
#  swapon -s
Filename				Type		Size	Used	Priority
/dev/vda7                              	partition	488444	0	-2
/dev/vda6                              	partition	488444	0	-3
/dev/zram0                             	partition	52428	0	100
/dev/zram1                             	partition	52428	0	100
/dev/zram2                             	partition	52428	0	100
/dev/zram3                             	partition	52428	0	100
/dev/zram4                             	partition	52428	0	100
~~~

### Configuration

Pour configurer la zram, il faut éditer le fichier `/etc/default/zramswap`, on peut modifier la priorité, le pourcentage de ram qui peut être utilisé (10% par défaut), ou une allocation statique de la RAM (notamment en MiB)

Pour visualiser les algorithmes de compressions :

~~~
# cat /sys/block/zram0/comp_algorithm
[lzo] lz4 lz4hc 
~~~

On peut définir la compression en zstd, qui est plus performant que lzo, comme ceci :

~~~
# echo zstd > /sys/block/zram0/comp_algorithm
~~~ 

/!\\ Il faut que le paquet zstd soit installé (disponible dans Debian 10) **et** que le noyau linux supporte ce type de compression dans l'API crypto (ce qui n'est pas le cas dans Debian 10).

Un outil natif nommé `zramctl` permet de gérer les périphériques de type zram. 

Exemple pour créer un périphérique (zram0 par défaut) avec une taille de 1G, l'algorithme de compression LZ4 et 4 threads de compression:

~~~
# zramctl -f -a lz4 -s 1G -t 4
~~~

L'outil `zramctl` permet aussi de voir des informations de manière détaillée :

~~~
zramctl --output-all
NAME       DISKSIZE DATA COMPR ALGORITHM STREAMS ZERO-PAGES TOTAL MEM-LIMIT MEM-USED MIGRATED MOUNTPOINT
/dev/zram4    51.2M   4K   78B lzo             5          0   12K        0B      12K       0B [SWAP]
/dev/zram3    51.2M   4K   78B lzo             5          0   12K        0B      12K       0B [SWAP]
/dev/zram2    51.2M   4K   78B lzo             5          0   12K        0B      12K       0B [SWAP]
/dev/zram1    51.2M   4K   78B lzo             5          0   12K        0B      12K       0B [SWAP]
/dev/zram0    51.2M   4K   78B lzo             5          0   12K        0B      12K       0B [SWAP]
~~~

### Désactivation de la zRAM

Si pour une raison quelconque on souhaite la désactiver :

~~~
# swapoff /dev/zram0 
~~~

On réinitialise les paramètres :

~~~
# echo 1 >/sys/block/zram0/reset
~~~

On désactive le module moyau :

~~~
# rmmod zram
~~~