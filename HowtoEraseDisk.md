---
title: Howto Erase Disk
...

# Shred

    shred -v /dev/sdd

On peut préciser le nombre de passe avec l’option `-n`. Plus d’informations dans le manuel : [`shred(1)`](https://manpages.debian.org/bookworm/coreutils/shred.1.en.html).

# OpenSSL AES

Pour effacer sdX très rapidement avec des données aléatoires (fournie par AES).

~~~
openssl enc -aes-256-ctr -pass pass:"$(dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64)" -nosalt < /dev/zero | pv -pterb > /dev/sdX
~~~
