# Howto Gitlab CI

Gitlab CI/CD permet de mettre en place des processus d'intégration continue pour
tester, *builder* et déployer vos projets Gitlab.

Cet outil est intégré directement dans Gitlab et est simple à mettre en place.

Les composants essentiels pour utiliser le CI sont un **Gitlab Runner** et
le fichier de configuration `.gitlab-ci.yml` propre à chaque projet.


## Gitlab Runner

Gitlab Runner est l'application avec laquelle s'exécute les scripts définis dans
le fichier de configuration `.gitlab-ci.yml`. Écrit en Go, Runner se déploie
sur n'importe quelle plateforme supportant le language.

Voir: [Installer Gitlab Runner](https://docs.gitlab.com/runner/install/)

### Installer un Runner avec Docker

L'une des façons les plus simples de mettre en place un Runner est avec Docker.
Il suffit de lancer le conteneur en utilisant la commande suivante:

```
$ docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

Ensuite il faut générer la configuration du runner en lançant la commande
suivante:

`$ docker exec -it gitlab-runner gitlab-runner register`

Les informations requises pour configurer un runner sont:

- L'URL du coordinateur CI (ex: https://gitlab.evolix/ci)
- Un token généré dans Gitlab pour ce runner
- Un nom descriptif pour ce runner
- [Optionel] Les tags des tâches que ce runner peut accepter

Un runner peut être spécifique à un projet particulier ou être disponible pour
tous les projets (partagé).

**Pour un runner spécifique**

Utiliser le token disponible dans la configuration du projet sous l'onglet *CI/CD*.

**Pour un runner partagé**

Utiliser le token disponible dans la configuration de Gitlab sous l'onglet
*Overview* puis *Runners*.

--

Une fois la configuration terminée, le runner sera enregistré auprès de votre
instance Gitlab, prêt à exécuter vos tâches !


## Fichier de configuration `.gitlab-ci.yml`

Ce fichier de configuration doit être situé à la racine de votre projet.

Il définit l'environnement, le *pipeline* et tout ce qui doit être exécuté pour
tester, *builder* et déployer le projet.

Vous pouvez consulter la
[documentation complète](https://docs.gitlab.com/ce/ci/yaml/README.html)
pour connaitre toutes les options disponibles.

### Example standard

```yaml
image: debian:jessie
services:
  - postgres

before_script:
  - bundle install

after_script:
  - rm secrets

stages:
  - build
  - test
  - deploy

job1:
  stage: build
  script:
    - execute-script-for-job1
  only:
    - master
  tags:
    - docker
```

### Exemple pour déployer un site web avec Rsync

```yaml
# Stages to execute
stages:
  - staging
  - deploy

# Global variables
variables:
  WWW_MOD: "D750,F640"

# Add SSH private key to deploy to prod/staging server
before_script:
- apt update -y && apt install -y rsync
- 'which ssh-agent || ( apt install openssh-client -y )'
- eval $(ssh-agent -s)
- ssh-add <(echo "$SSH_PRIVATE_KEY")
- mkdir -p ~/.ssh
- '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

# Deploy instruction for stage "staging"
# On master only
# Automatic
deploy_staging:
  stage: staging
  variables:
    WWW_USER: "..."
  script:
    - rsync -av --chmod=$WWW_MOD --delete --exclude .git $CI_PROJECT_DIR/www/ $WWW_USER@webserver.example.com:~/www/
  environment:
    name: staging
    url: https://staging.example.com
  only:
  - master

# Deploy instruction for stage "prod"
# On master only
# Only on manual action
deploy_prod:
  stage: deploy
  variables:
    WWW_USER: "..."
  script:
    - rsync -av --chmod=$WWW_MOD --delete --exclude .git $CI_PROJECT_DIR/www/ $WWW_USER@webserver.example.com:~/www/
  environment:
    name: production
    url: https://example.com
  when: manual
  only:
  - master
```
