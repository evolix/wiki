**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto Installer Système sur RaspberryPi

Ici sera expliqué comment installer un système sur RaspberryPi. La méthode reste assez similaire pour les différents systèmes, il est donc possible de garder une des procédures suivantes pour un système non listé ci-dessous.

### Raspbian

### Lakka

Matériel requis :

* Un RaspberryPi
* Un cable USB/MicroUSB (optionnel : un adaptateur secteur)
* Une manette de jeu (Ps3, Xbox, Freebox, ...). Le système n'a pas été testé avec un clavier
* Une carte SD (4Go minimum conseillé)
* Un cable HDMI
* Optionnel : un cable ethernet (afin de pouvoir transferer les jeux sur le réseau local*

Procédure à suivre :

* Télécharger le .zip de l'image système Lakka sur le site officiel Lakka.tv
* Utiliser la ligne de commande gunzip pour extraire le .img depuis le .zip ou utiliser un outil graphique dedié

~~~
$ ls -l /dev/sd*
~~~

* La commande ci-dessus va montrer les partitions accessibles ainsi que leur numéro. sda peut par exemple être le disque dur
* Monter la carte SD sur un ordinateur sous linux (possible sous autre OS mais non expliqué ici)

~~~
Executer à nouveau
$ ls -l /dev/sd*
Cela devrait afficher les mêmes partitions que précédemment avec la carte SD en plus

$ sudo dd if=Lakka-*.img of=/dev/sdX
Où Lakka-*.img est le chemin vers le .img extrait précedemment et sdX la carte SD (remplacer X par la lettre trouvée précedemment
~~~

* L'étape devrait prendre quelques petites minutes (même moins)
* Retirer en toute sécurité la carte SD et la placer dans le RaspberryPi
* Brancher le HDMI entre le Raspberry et l'écran
* Allumer l'écran
* Brancher le cable ethernet (optionnel)
* Brancher la manette USB sur l'un des ports USB
* Brancher l'alimentation du Raspberry (cable USB/microUSB)

 L'icône Lakka devrait apparaitre.
 Après quelques configurations automatiques, le menu Lakka devrait apparaitre.

* Si vous avez branché le cable ethernet, le Raspberry apparaitra sur vos autres ordinateurs dans le réseau local. Il est ensuite possible de remplir la carte SD de jeu dans le dossier ROMs s'y trouvant. Les images des jeux sont à placer, pas les .zip ou autre formats compressés.
* Aller dans "Load Content (Detect Core) afin d'avoir la liste des jeux envoyés par ethernet

Bon jeu!

### Debian

* Télécharger une image sur <https://raspi.debian.net/tested-images/>
* La copier sur une carte SD (ou sur le media de stockage souhaité sur lequel Debian devra être installé) : `xzcat raspi_<FILE>.img.xz | dd of=<SD-CARD_PATH> bs=64k oflag=dsync status=progress`
* Démarrer dessus, Debian s'installe automatiquement et est immédiatement utilisable sur ce support
* On peut se connecter en console par exemple avec minicom : `minicom -b 115200 -D /dev/ttyUSB0`
* L'identifiant par défaut est `root` sans mot de passe, on ne peut se connecter que localement ; s'il faut se connecter à distance, il est nécessaire de créer un second utilisateur ou de configurer un mot de passe à `root` et autoriser la connexion root dans `/etc/ssh/sshd_config`
* L'image est très minimale. Si besoin, on peut installer des paquets standards avec `tasksel install --new-install standard`
* Le hostname est défini par défaut sur `rpi{model}-{builddate}`, on peut vouloir le changer
* Le disque est partitionné par défaut avec une partition `/` qui prend tout l'espace disponible sur le support

Plus d'infos : <https://raspi.debian.net/>

### OpenBSD

Voir [/HowtoOpenBSD/Flashinstall]()