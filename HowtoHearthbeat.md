**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Hearthbeat

Documentation officielle : <http://www.linux-ha.org/wiki/Heartbeat>

## Tuer un Hearthbeat planté

Pour tuer un heathbeat planté, il faut

~~~
root@> ps aux |grep hearthbeat
root@> kill -9 #pid
root@> /etc/init.d/hearthbeat stop
root@> /etc/init.d/hearthbeat start
~~~

Cela permet de tuer le binaire, libérer les ressources utilisées par hearthbeat, et redémarrer le service.

## Hearthbeat 3
À partir de Debian Squeeze c'est Heartbeat 3 qui est inclus dans les packages, de nombreuses choses ont changées, entre autres, le fichier de configuration principal.
Installation : `aptitude install heartbeat cluster-glue`

### Mettre en place un failover sans crm/pacemaker
[[Image(heartbeat_simple.png, 33%)]]

Dans un premier temps il faut définir les nom d'hôtes dans /etc/hosts ou avoir un serveur DNS qui les résoud.

~~~
192.168.0.1 ha-1
192.168.0.2 ha-2
~~~

Il faut ensuite authentifier les nœuds, en placant un md5 commun aux 2 dans /etc/ha.d/authkeys. Par exemple avec ` md5sum <<< 'un_mot_de_passe'`

~~~
auth 1
1 md5 12499c1b5fdbf25aa4abc05176904ca7
~~~

Ensuite il faut s'assurer que ce fichier ne soit lisible que par le root, pour cela on fait un chmod 600 sur le fichier authkeys.

Puis, le fichier de configuration, le plus simple possible :
Ici celui de ha-1.

~~~
#
#       keepalive: how many seconds between heartbeats
#
keepalive 2
#
#       deadtime: seconds-to-declare-host-dead
#
deadtime 10
#
#       What UDP port to use for udp or ppp-udp communication?
#
udpport        694
bcast  eth0
mcast eth0 225.0.0.1 694 1 0
ucast eth0 192.168.0.2
#       What interfaces to heartbeat over?
udp     eth0
#
#       Tell what machines are in the cluster
#       node    nodename ...    -- must match uname -n
node    ha-1
node    ha-2
~~~

Sur ha-2 il faut faire le même fichier de configuration mais remplacer la ligne `ucast eth0 192.168.0.2` par `ucast eth0 192.168.0.1`
Enfin, il faut indiquer le maître et l'adresse IP virtuelle que l'on veut utiliser, ainsi éventuellement qu'un service tel que apache2 dans le fichier `/etc/ha.d/haresources`

~~~
ha-1 192.168.0.10 apache2
~~~

Puis démarrer le service sur les 2 machines `/etc/init.d/heartbeat start`[[BR]]

On peut ensuite vérifier le bon fonctionnement avec juste un `ifconfig`[[BR]]

Note : heartbeat utilise la méthode des `gratuitous arp` quand le maître est _down_, l'esclave indique que l'adresse IP virtuelle n'est plus associé à l'adresse MAC connu par le client, mais l'adresse MAC de l'esclave, celui-ci remet donc son cache à jour.

Si l'on souhaite que le master ne reprenne pas la main s'il est à nouveau up après un down, il faut mettre `auto_failback off` dans `ha.cf`

### Mettre en place un failover avec crm/pacemaker et apache en service
Pour utiliser _crm_ il faut l'activer dans le `ha.cf` en mettant ` crm yes `[[BR]]


Il y a plusieurs façon de configurer crm :

* Le shell crm, un outil en ligne de commande puissant, cachant la configuration de type xml, avec une aide en ligne et la complétion ;
* High availability web konsole, un interface web en AJAX. Encore en développement ;
* L'interface graphique heartbeat, typiquement un éditeur xml avancé avec des boutons pour définir des actions, et voir le status ;
* DRBD-MC, une console de management codé en Java, contient de nombreux assistants.

On utilisera ici le shell crm avec des _templates_.

~~~
crm
cib new test
configure
template
list templates
~~~

Les templates par défaut sont les suivants :

~~~
crm(live)configure template# list templates
virtual-ip   ocfs2        gfs2         filesystem   apache       clvm
gfs2-base
~~~

On veut utiliser le template apache :

~~~
new web apache
edit
~~~

Changer l'adresse IP virtuelle, l'id et le fichier de configuration apache.

~~~
ip 192.168.0.10
id web-1
configfile /etc/apache2/apache2.conf
~~~

Le fichier de template apache ressemble à ceci :

~~~
# Configuration: web

# Edit instructions:
#
# Add content only at the end of lines starting with '%%'.
# Only add content, don't remove or replace anything.
# The parameters following '%required' are not optional,
# unlike those following '%optional'.
# You may also add comments for future reference.

%name virtual-ip

# Virtual IP address
#
# This template generates a single primitive resource of type IPaddr

%pfx virtual-ip
%required

# Specify an IP address
# (for example: 192.168.1.101)

%% ip 192.168.0.10

%optional

# If your network has a mask different from its class mask, then
# specify it here either in CIDR format or as a dotted quad
# (for example: 24 or 255.255.255.0)

%% netmask

# Need LVS support? Set this to true then.

%% lvs_support

%name apache

# Apache web server
#
# This template generates a single primitive resource of type apache

%depends_on virtual-ip
%suggests filesystem

# NB:
# The apache RA monitor operation requires the status module to
# be loaded and access to its page (/server-status) allowed from
# localhost (127.0.0.1). Typically, the status module is not
# loaded by default. How to enable it depends on your
# distribution. For instance, on recent openSUSE or SLES
# releases, it is enough to add word "status" to the list in
# variable APACHE_MODULES in file /etc/sysconfig/apache2.

%pfx apache
%required

# Name the apache resource
# (for example: web-1)

%% id web-1

# The full pathname of the Apache configuration file

%% configfile /etc/apache2/apache2.conf

%optional

# Extra options to apply when starting apache. See man <httpd(8).>

%% options

# Files (one or more) which contain extra environment variables,
# such as /etc/apache2/envvars

%% envfiles

# Don't edit anything below this line.

%generate

primitive virtual-ip ocf:heartbeat:IPaddr
	params ip=%virtual-ip:ip
	opt cidr_netmask=%virtual-ip:netmask
	opt lvs_support=%virtual-ip:lvs_support


primitive apache ocf:heartbeat:apache
	params configfile=%apache:configfile
	opt options=%apache:options
	opt envfiles=%apache:envfiles

monitor apache 120s:60s

group %apache:id
	apache virtual-ip
~~~

~~~
show
apply
cd ..
show
commit
~~~

~~~
location web-1-pref web-1 100: ha-1
property stonith-enabled=false
commit
^d

~~~
Il faut ensuite attendre que la configuration se propage sur le deuxième nœud.
### Divers

En clonant des machines virtuelles, Heartbeat garde le même uuid qu'il a généré auparavant, ce qui pose quelques soucis, car il ne sait plus qui est qui ... et dans le doute il reboot !
Pour re-générer les uuid, il faut supprimer le fichier /var/lib/heartbeat/hb_uuid
Logs :

~~~
root@ha-2:~# grep -B 10 Reboot /var/log/syslog
May  9 11:24:18 client cib: [1923]: info: register_heartbeat_conn: Hostname: ha-2
May  9 11:24:18 client cib: [1923]: info: register_heartbeat_conn: UUID: 66a8d84d-3fc6-402e-aa46-d433a1d5281b
May  9 11:24:18 client cib: [1923]: info: ccm_connect: Registering with CCM...
May  9 11:24:18 client cib: [1923]: WARN: ccm_connect: CCM Activation failed
May  9 11:24:18 client cib: [1923]: WARN: ccm_connect: CCM Connection failed 1 times (30 max)
May  9 11:24:18 client heartbeat: [1887]: WARN: nodename ha-1 uuid changed to ha-2
May  9 11:24:18 client heartbeat: [1887]: debug: displaying uuid table
May  9 11:24:18 client heartbeat: [1887]: debug: uuid=66a8d84d-3fc6-402e-aa46-d433a1d5281b, name=ha-1
May  9 11:24:18 client heartbeat: [1887]: WARN: Managed /usr/lib/heartbeat/ccm process 1922 killed by signal 11 [SIGSEGV - Segmentation violation].
May  9 11:24:18 client heartbeat: [1887]: ERROR: Managed /usr/lib/heartbeat/ccm process 1922 dumped core
May  9 11:24:18 client heartbeat: [1887]: EMERG: Rebooting system.  Reason: /usr/lib/heartbeat/ccm
~~~
