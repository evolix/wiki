---
categories: web http
title: Howto Cache HTTP
...

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Documentation

* <http://fr.wikipedia.org/wiki/Cache-Control>
* <https://developer.mozilla.org/fr/docs/Web/HTTP/Caching>
* <https://web.dev/http-cache/>

## Exemples

En-têtes relatifs au cache HTTP :

~~~
Cache-Control:
Max-Age:
Age:
Pragma:
Expires:
Last-Modified:
~~~

Exemple de mise en cache en PHP :

~~~{.php}
$offset=3600*20; //Forte période 20h
//$offset=3600*3; //Période normale 3h

header("Cache-Control: public, max-age=$offset");
header("Expires: " .gmdate("D, d M Y H:i:s",time()+$offset). " GMT");
header("Last-Modified: " .gmdate("D, d M Y H:i:s",time()). " GMT");
header("X-generated-at: " .date(DATE_RFC822));
~~~

Exemple de non-mise en cache en PHP :

~~~{.php}
header("Pragma: no-cache");
header("Cache-Control: no-cache");
~~~

## Tester le comportement du cache

Cet outil en ligne permet d'interpéter les en-têtes HTTP renvoyées pour une URL : <https://redbot.org/>