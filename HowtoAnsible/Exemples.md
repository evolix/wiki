# Exemples Ansible

### Tester si une variable est vide

~~~{.yaml}
- debug:
    msg: "La variable n'a pas de description"
  when: var.description == none
~~~

### Installer plusieurs paquets

~~~{.yaml}
- name: Install packages
  apt:
    name: '{{ item }}'
    state: present
  with_items:
  - vim
  - vim-doc
~~~

### lien symbolique

~~~{.yaml}
- name: Create symlink foo -> bar
  file:
    src: "/usr/share/munin/plugins/bar"
    dest: "/etc/munin/plugins/foo"
    state: link
~~~

### répertoire

~~~{.yaml}
- name: Create directory
  file:
    path: /foo/bar
    state: directory
    owner: root
    group: root
    mode: "0750"
~~~
