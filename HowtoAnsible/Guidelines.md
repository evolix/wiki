**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Recommandations et politique Evolix pour l'écriture des _playbooks_

* séparer les _tasks_, _handlers_, _templates_, _files_ et _vars_ dans des répertoires et fichiers différents. Voir [ici](http://docs.ansible.com/ansible/playbooks_best_practices.html#directory-layout) pour la hierarchie à respecter ;
* utiliser `gather_facts: no` si le playbook ne les utilisent pas (gain de temps lors de l'exécution) ;
* toujours nommer les tâches ;
* s'assurer que chaque tâche est idopotente, notamment pour l'exécution de commande et les remplacement suivant des expressions rationnelles ;

Lire également les recommandations sur la [documentation officielle](http://docs.ansible.com/ansible/playbooks_best_practices.html).

Vidéo "Getting Started" (35 minutes en Anglais) : <https://ansible.wistia.com/medias/qrqfj371b6>