---
title: How to Guacamole
categories: vnc
...

* Documentation : <https://guacamole.apache.org/doc/gug/>

[Apache Guacamole](https://guacamole.apache.org/) est une application web permettant la connexion à des ordinateurs distants en utilisant les protocoles RDP, VNC et SSH.

## Installation

L'installation native de guacamole nécessite la compilation de guacd, pour cette raison, nous recommandons l'utilisation de docker.

### Pré-requis

* [Docker](https://wiki.evolix.org/HowtoDocker)
* [Docker compose](https://wiki.evolix.org/HowtoDockerCompose) (ou `apt install docker-compose-plugin` en utilisant les dépôts de docker)
* [PostgreSQL](https://wiki.evolix.org/HowtoPostgreSQL)

### Création de la base de donnée

```
# sudo -u postgres -- createdb guacamole
# docker run --rm docker.io/guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgresql > initdb.sql
# sudo -u postgres -- psql -d guacamole -f - < initdb.sql
# sudo -u postgres -- createuser -P guacamole
# sudo -u postgres -- psql -d guacamole -f - << EOF
GRANT SELECT,INSERT,UPDATE,DELETE ON ALL TABLES IN SCHEMA public TO guacamole;
GRANT SELECT,USAGE ON ALL SEQUENCES IN SCHEMA public TO guacamole;
EOF
```

Il faut aussi s'assurer que les containers du service puissent se connecter à PostgreSQL.

### Création du service

```
# POSTGRESQL_PASSWORD="${POSTGRESQL_PASSWORD:?}"
# mkdir /etc/guacamole
# cat > /etc/guacamole/compose.yml << EOF
services:
  guacd:
    image: docker.io/guacamole/guacd
    networks:
      - guacamole
  guacamole:
    image: docker.io/guacamole/guacamole
    depends_on:
      - guacd
    extra_hosts:
      - "host.docker.internal:host-gateway" 
    ports:
      - 8080:8080
    environment:
      GUACD_HOSTNAME: 'guacd'
      POSTGRESQL_HOSTNAME: 'host.docker.internal'
      POSTGRESQL_DATABASE: 'guacamole'
      POSTGRESQL_USER: 'guacamole'
      POSTGRESQL_PASSWORD: '${POSTGRESQL_PASSWORD}'
      # Active la fonctionnalité TOTP
      TOTP_ENABLED: 'true'
    networks:
      - guacamole

networks:
  guacamole:
    name: guacamole
    attachable: true
    driver: bridge
    driver_opts:
      com.docker.network.bridge.name: "guacamole0" 
    ipam:
      driver: default
      config:
        - subnet: 172.18.0.0/16
          ip_range: 172.18.5.0/24
          gateway: 172.18.0.1
EOF
# cd /etc/guacamole
# docker compose -f /etc/guacamole/compose.yml up -d
```

Il est ensuite possible de se connecté à guacamole en utilisant <http://127.0.0.1:8080/guacamole/> (remplacer `127.0.0.1` par l'IP ou le nom d'hôte du serveur) et en utilisant les identifiants `guacadmin`/`guacadmin`.

Il est fortement recommandé de remplacer cet utilisateur par un autre compte administrateur dès la première connexion.

## Administration

L'interface d'administration est accessible à un utilisateur administrateur en cliquant sur leur nom d'utilisateur et en sélectionnant `Settings`.

### Gestion des utilisateurs

La gestion des utilisateurs se fait à partir de l'onglet `Users` et la gestion des groupes utilisateurs se fait à partir de l'onglet `Groups`. Les droits peuvent se définir au niveau des utilisateurs individuels ou au niveau des groupes.

### Gestion des connexions

Les connexions sont à prédéfinir dans l'onglet `Connections` de l'interface administrateur en cliquant sur le bouton `New connection`.