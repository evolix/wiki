---
categories: web monitoring
title: Howto Smokeping
...

* Documentation : <https://oss.oetiker.ch/smokeping/doc/index.en.html>

## Installation

~~~
# apt install smokeping echoping

~~~
Normalement il est aussi installé _speedy-cgi-perl_ pour la gestion des cgi par apache.
À partir de maintenant smokeping est accessible sur <http://localhost/cgi-bin/smokeping.cgi>

Si cela ne fonctionne pas vérifier que vous avez dans le vhost par défaut.

~~~
ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/        
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Require all granted
~~~

## Configuration

### Mise en place d'alerte mail

Éditer le fichier `/etc/smokeping/config.d/alerts` et spécifier les bonnes informations :

~~~
*** Alerts ***
to = tech@evolix.net
from = smokealert@smokeping.evolix.net
~~~

Dans la suite du fichier de configuration se trouve les types d'alertes, exemple : 

~~~
+bigloss
type = loss
pattern = <100%,>50%,>50%
comment = host not responding
~~~

Permet de détecter quand un hôte ne ping plus du tout.

Vous pouvez ensuite dire à Smokeping d'envoyer une alerte en ajoutant l'alerte dans la liste des _Targets_ voulus.

~~~
# vim /etc/smokeping/config.d/Targets
+ EvolixLAN
menu = Evolix LAN
title = LAN d'evolix
alerts = bigloss

++ optiplex
host = optiplex.evolix.net

++sc420
host = sc420.evolix.net

++routeur
host = routeur.evolix.net
~~~

## Réinstallation depuis un backup

En cas de nécessité de réinstaller smokeping depuis un backup, il faut, sur le nouveau serveur, récupérer les dossiers suivants :

* /etc/apache2/
* /var/www/
* /etc/smokeping/
* /var/lib/smokeping/
* /usr/share/smokeping/
* /usr/lib/cgi-bin/smokeping.fcgi
