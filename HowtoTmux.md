---
title: Howto Tmux
categories: sysadmin
...

Tout comme [screen](https://wiki.evolix.org/HowtoScreen), Tmux permet de gérer des terminaux lancés en arrière plan. Il s'agit d'une alternative enrichie qui prend notamment en charge des splits plus complexes.

Il diffère de screen avec le concept de fenêtres ("windows") et panneaux ("panes"). Un panneau tmux est une fenêtre dans la terminologie de Screen. Une fenêtre Tmux est un agencement d'un ou plusieurs panneaux (par exemple, deux fenêtres côte à côte).

D'autres points de comparaison :

* Les fenêtres sont des entités indépendantes qui peuvent être attachées simultanément à plusieurs sessions et peuvent être déplacées librement entre les sessions d'un même serveur Tmux ;
* Plusieurs buffers ;
* Choix des raccourcis VI ou Emacs ;
* Sous licence BSD.

<http://man.openbsd.org/OpenBSD-current/man1/tmux.1>

Voir aussi : [HowtoXPanes](https://wiki.evolix.org/HowtoXpanes)


## Installation

~~~
# apt install tmux
~~~

## Utilisation de base

Lister les sessions lançées :

~~~
$ tmux ls
perso: 2 windows (created Mon Sep  5 09:34:52 2016) [315x78]
travail: 1 windows (created Fri Oct 21 16:05:10 2016) [315x78]
~~~

Pour lancer une nouvelle session :

~~~
$ tmux
~~~

Pour lancer une nouvelle session sous le nom "test" :

~~~
$ tmux new -s test
~~~

Pour lancer une nouvelle session avec mutt par exemple :

~~~
$ tmux new mutt
~~~

Pour se rattacher à la session Tmux 1 (voir `tmux ls`) :

~~~
$ tmux attach -t 1
~~~

À noter que plusieurs clients peuvent de rattacher à la même session simultanément (partage).

## Personnaliser le préfix

Par défaut, Tmux utilise la séquence `<Ctrl>+b` comme préfix pour les commandes internes.
Il est possible de modifier ce préfix dans la configuration, par exemple pour avoir `<Ctrl>-a` comme dans Screen :

~~~
set -g prefix C-a
~~~

Cette personnalisation étant fréquente, pour faciliter la documentation, nous indiquerons ici `<Prefix>` à la place de `<Ctrl>+b`.

Une fois attaché à une session Tmux, voici la liste des commandes de base utiles :

~~~
<Prefix>+d : sortir de tmux
<Prefix>+c : créer une nouvelle fenêtre
<Prefix>+n : se déplacer sur la fenêtre suivante
<Prefix>+w : lister les fenêtres disponibles
~~~

## Utilisation avancée

### panneaux

Une fois attaché à une session Tmux, voici la liste des commandes pour gérer les panneaux :

~~~
<Prefix>+x : fermer un panneau
<Prefix>+% : créer un panneau vertical
<Prefix>+" : créer un panneau horizontal
<Prefix>+z : passer le panneau courant en plein écran
<Prefix>+s : envoyer le panneau courant vers une autre fenêtre
<Prefix>+<flèche> : se déplacer entre les régions splitées
<Prefix>+} : déplacer un panneau vers la gauche
<Prefix>+{ : déplacer un panneau vers la droite
<Prefix>+, : renommer un panneau
~~~

### copier/coller

~~~
<Prefix>+[ : sélectionner la zone à copier
<Prefix>+] : coller
<Prefix>+= : se déplacer dans le buffer
~~~

### sélection et buffers

Par défaut Tmux capture les événements de souris et gère son propre bufefr interne.

On peut temporairement désactiver ce comportement en appuyant sur `Shift` pendant la sélection à la souris. Ça bascule alors sur le comportement de votre terminal.

Idem pour le "clic-milieu" pour coller ce qui est dans le buffer.

#### Supprimer les _buffers_

Tmux garde en mémoire les chaînes de charactères qui ont été copiées. Pour les supprimer :

~~~ { .bash }
while tmux delete-buffer 2> /dev/null
do
    continue
done
~~~

### clients attachés

Une fois connecté à une session, il est possible de lister les clients attachés :

~~~
$ tmux list-clients
/dev/pts/3: jdoe [193x42 rxvt-unicode-256color] (utf8)
/dev/pts/8: jdoe [181x41 xterm] (utf8)
~~~

Si on souhaite détacher proprement un des clients, 3 méthodes :

La méthode facile :

~~~
<Prefix>+D : (c'est bien un D majuscule) donne la liste des clients attachés, on choisi avec <Enter> celui ou ceux à détacher.
~~~

La méthode bas niveau :

~~~
$ tmux detach-client -t /dev/pts/8
~~~

La méthode "à la hache" :

~~~
$ tmux kill-client /dev/pts/8
~~~

Dans tous les cas on peut vérifier ensuite que le client a été détaché :

~~~
$ tmux list-clients
/dev/pts/3: jdoe [193x42 rxvt-unicode-256color] (utf8)
~~~

### Options globales de session

Masquer la barre d'état

~~~
<Prefix> :set -g status off
~~~


### Embrasser les 256 couleurs

Par défaut, tmux donne "screen" comme nom de terminal.
Si on souhaite libérer le pouvoir des 256 couleurs, il suffit de changer en "screen-256color" soit en entrant la commande, soit en dans son `.tmux.conf`

~~~
$ cat .tmux.conf 
set -g default-terminal "screen-256color"
~~~