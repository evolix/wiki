---
categories: openbsd network vpn security ipsec
title: Howto ISAKMPD
...

# Howto VPN IPsec sous OpenBSD

Il existe deux méthodes pour mettre en place un VPN IPsec sous OpenBSD. La première est de créer un fichier de configuration /etc/isakmpd/isakmpd.conf. La seconde est d'utiliser /etc/ipsec.conf avec sa syntaxe "pf-like" et de laisser ipsecctl s'occuper des politiques de flux et de l'établissement de la SA (Security association).

Nous utilisons principalement la deuxième.

## Méthode isakmpd via isakmpd.conf

### Recharger isakmpd

Pour recharger la configuration d'ISAKMPD, il suffit de lui envoyer un SIGHUP.
Ceci ne provoque pas coupure VPN pour les phases 1 (IPSEC) mais un rechargement de
toutes les phases 2 !

~~~
pkill -HUP isakmpd
~~~

Note : attention, on a déjà constaté un plantage peu après un rechargement.
Il conviendra de surveiller attentivement qu'ISAKMPD tourne bien.

### Redémarrer isakmpd

Attention cela interrompt tous les VPN pendant quelques instants.

~~~
# rcctl restart isakmpd
# ipsecctl -f /etc/ipsec.conf
~~~

### Debuger

~~~
# isakmpd -L -v -DA=90
# tcpdump -n -vs 1440 -r /var/run/isakmpd.pcap
~~~

## Méthode isakmpd via ipsec.conf

### Mise en place d'un VPN "simple" entre 2 machines OpenBSD

On dispose de :

* 2 passerelles VPN OpenBSD gw1 et gw2 (192.0.2.254 et 198.51.100.254)
* 2 LAN net1 et net2 correspondants (192.0.2.0/24 et 198.51.100.0/24)

Sur gw1 :

Vérifier que les sysctl suivants sont actifs :

~~~
# sysctl net.inet.esp.enable
net.inet.esp.enable=1
# sysctl net.inet.ah.enable
net.inet.ah.enable=1
# sysctl net.inet.ip.forwarding
net.inet.ip.forwarding=1
~~~

Dans /etc/pf.conf, positionner :

~~~
set skip on {lo0 enc0}

pass in on $ext_if proto esp from $addr_gw2 to ($ext_if)
pass in on $ext_if proto udp from $addr_gw2 to ($ext_if) port {isakmp, ipsec-nat-t}
pass in on $ext_if proto udp from $addr_gw2 port {isakmp, ipsec-nat-t} to ($ext_if)
~~~

Dans /etc/ipsec.conf, positionner :

~~~
local_network="192.0.2.0/24"

remote_ip="198.51.100.254"
remote_network="198.51.100.0/24"

ike dynamic esp from $local_network to $remote_network peer $remote_ip \
    main auth hmac-sha2-256 enc aes-256 group modp2048 lifetime 86400 \
    quick auth hmac-sha2-256 enc aes-256 group modp2048 lifetime 7200 \
    psk "PRE-SHARED KEY"
~~~

Le mot-clef "dynamic" permet d'activer le Dead Peer Detection (DPD), utilisé pour détecter la perte du pair. Certains VPNs peuvent ne pas rester stable s'il n'est pas activé.  
Le mot-clef "main" configure la phase 1, tandis que le mot-clef "quick" configure la phase 2.  
Adapter ensuite les protocoles de sécurité, les durées de vies des 2 phases, ainsi que la psk, qui doivent correspondre entre gw1 et gw2.

**Attention :** le DH group 20 (ecp384) ne semble pas fonctionner côté OpenBSD (le VPN ne monte pas, avec un timeout). Les autres groupes au dessus de DH group 14 n'ont pas été testés et pourraient également poser problème.

Tester la configuration :

~~~
# ipsecctl -n -f /etc/ipsec.conf
~~~

Activer et démarrer le service :

~~~
# rcctl enable ipsec
# rcctl enable isakmpd
# rcctl set isakmpd flags -KTv
# rcctl start isakmpd
# ipsecctl -f /etc/ipsec.conf
~~~

Puis effectuer les mêmes actions sur l'autre passerelle.

Les deux réseaux doivent alors pouvoir se pinger entre eux.

Pour couper un VPN, il faut ajouter l'option '-d' :

~~~
# ipsecctl -d -f /etc/ipsec.conf
~~~

### Monter des VPNs avec plusieurs pairs

Lorsque plusieurs VPNs sont montés, une bonne pratique est d'avoir un fichier par pair. Ainsi, les VPNs montés avec *FAI1* et *FAI2* pourront être indépendamment redémarrés :

~~~
# cat /etc/ipsec.conf
include "/etc/ipsec/fai1.conf"
include "/etc/ipsec/fai2.conf"

# ipsecctl -d -f /etc/ipsec/fai1.conf
# ipsecctl -f /etc/ipsec/fai1.conf

# ipsecctl -d -f /etc/ipsec/fai2.conf
# ipsecctl -f /etc/ipsec/fai2.conf
~~~

### Monter un VPN avec plusieurs réseaux

Dans le cas où plusieurs réseaux distants doivent être accessibles (plusieurs phases 2 doivent être montées), cette syntaxe peut être utilisée dans /etc/ipsec.conf :

~~~
local_network="192.0.2.0/24"

remote_ip="198.51.100.254"
remote_network="{198.51.100.0/24, 198.51.200.0/24}"

ike dynamic esp from $local_network to $remote_network peer $remote_ip \
    main auth hmac-sha2-256 enc aes-256 group modp2048 lifetime 86400 \
    quick auth hmac-sha2-256 enc aes-256 group modp2048 lifetime 7200 \
    psk "PRE-SHARED KEY"
~~~

### Obtenir l'équivalent isakmpd exécuté par ipsec

Il est possible de récupérer le nom des variables isakmpd générées par ipsecctl :

~~~
# ipsecctl -nvf /etc/ipsec.conf  # Commandes pour monter le VPN
# ipsecctl -nvdf /etc/ipsec.conf # Commandes pour couper le VPN
~~~

### Debug IPsec

L'option `DA=90` indique à isakmpd de logger tout ce qui se passe sur le VPN. Les logs seront alors très dense.

~~~
# cat /etc/rc.conf.local
isakmpd_flags="-K -DA=90"

# rcctl restart isakmpd
# ipsecctl -f /etc/ipsec.conf
~~~

Il est également possible modifier la configuration en cours sans redémarrer isakmpd à l'aide de la fifo :

~~~
# echo "D A 90" >/var/run/isakmpd.fifo
~~~

On pourra désactiver le debug ainsi :

~~~
# echo "D T" >/var/run/isakmpd.fifo
~~~

Autre possibilité :

~~~
# cat /etc/rc.conf.local
isakmpd_flags="-K Lv"

# rcctl restart isakmpd
# ipsecctl -f /etc/ipsec.conf

# cat /var/log/daemon
# tcpdump -n -vs 1440 -r /var/run/isakmpd.pcap
~~~

## Envoyer des commandes au daemon

Cela est valable pour les 2 méthodes et se fait en écrivant dans le fichier fifo /var/run/isakmpd.fifo.

La liste des commandes est disponible dans la page de man.

* Pour avoir l'état de tous les VPNs, avec le détails des phases 1 et 2 en cours :

~~~
# echo S >/var/run/isakmpd.fifo
# cat /var/run/isakmpd.result
~~~

* Pour stopper un VPN, phase 1 (main) ou 2 (quick) :

~~~
# echo "t main peer-XXXX" >/var/run/isakmpd.fifo
# echo "t quick XXXX" >/var/run/isakmpd.fifo
~~~

* Pour relancer un VPN  (phase 1 ou 2… ce n'est pas clair. cela semble fonctionner pour la phase 1… à voir pour la phase 2).

~~~
# echo "c XXXX" >/var/run/isakmpd.fifo
~~~

ou

~~~
# echo "c main peer-XXXX" >/var/run/isakmpd.fifo
# echo "c quick XXXX" >/var/run/isakmpd.fifo
~~~

*XXXX* doit être remplacé par le nom de la phase 1 ou 2 spécifié dans isakmpd.conf, ou celui généré par ipsecctl.

*Note générale : attention, il peut y avoir un délai dans l'exécution des commandes.*

## Consulter les logs

~~~
# tail -f /var/log/messages | grep isakmpd
~~~

## Voir les associations VPN

~~~
# ipsecctl -sa
~~~

## Voir le trafic qui passe à travers le VPN

Utile par exemple pour vérifier que le trafic arrive bien, et que sa réponse part également :

~~~
# tcpdump -envps 1500 -i enc0 -l | grep 192.0.2.123
~~~


## FAQ

Que veulent dire les messages suivants ?

~~~
isakmpd[7864]: dpd_handle_notify: bad R_U_THERE seqno 551411465 <= 2374875689
~~~

C'est un message relatif au DPD (Dead peer detection) qui est un mécanisme pour détecter des éventuels soucis avec VPN.
Voir <http://www.juniper.net/techpubs/software/erx/junose61/swconfig-routing-vol1/html/ipsec-config4.html#1329479>

~~~
isakmpd[7864]: attribute_unacceptable: AUTHENTICATION_METHOD: got PRE_SHARED, expected RSA_SIG
isakmpd[7864]: message_negotiate_sa: no compatible proposal found
isakmpd[7864]: dropped message from 192.0.2.1 port 500 due to notification type NO_PROPOSAL_CHOSEN
~~~

Il s'agit d'un routeur VPN distant qui essaye de se connecter, mais n'a pas de configuration correspondante.
Cela peut se produire typiquement si son adresse IP change, et qu'elle n'est plus présente dans la configuration d'ISAKMPD.

~~~
isakmpd[10418]: isakmpd: phase 1 done (as responder): initiator id 192.0.2.1, responder id 1.2.3.4, src: 1.2.3.4 dst: 192.0.2.1
isakmpd[10418]: dropped message from 192.0.2.1 port 500 due to notification type INVALID_ID_INFORMATION
isakmpd[10418]: message_validate_notify: protocol not supported
~~~

Cela signifie a priori que la configuration en face à un réseau "remote" incorrect (soit complètement faux, soit pas sous la forme ADRESSE RÉSEAU / MASQUE)

Voir <http://www.thegreenbow.com/support_flow.html?page=12105C&lang=fr>

~~~
isakmpd[80145]: dropped message from 192.0.2.1 port 500 due to notification type PAYLOAD_MALFORMED
isakmpd[80145]: message_parse_payloads: reserved field non-zero: b8
## OU
isakmpd[80145]: dropped message from 192.0.2.1 port 500 due to notification type INVALID_PAYLOAD_TYPE
isakmpd[80145]: message_parse_payloads: invalid next payload type <Unknown 55> in payload of type 5
~~~

C'est a priori à cause du paramètre d'authentification `hmac-sha2-512` utilisé qui n'est pas fonctionnel. Il faut préférer l'utilisation de `hmac-sha2-256`.