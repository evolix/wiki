---
title: Howto formatter une clé USB
...


> :warning: **Dangereux, assurez-vous d'être parfaitement sûr de travailler sur le bon périphérique de bloc !**

## Format FAT32 : compatibilité maximale

Après avoir utilisé une clé USB dans un format spécial (copie d'une image ISO de démarrage, formatage spécial, etc.) on peut souhaiter retrouver un formatage compatible avec le plus grand nombre d'OS.

Commencez par liste les disques reconnus avec `fdisk -l` ou `lsblk`
Admettons que notre support soit accessible sur `/dev/sdX`.

### Effacer toute trace de table de partitions


### Avec fdisk

Nous allons utiliser `fdisk` pour la partitionner.

~~~
$ fdisk /dev/sdX       # utilisation de fdisk(8)
> d                    # supprimer une partition
> n                    # créer une nouvelle partition
> p                    # choisir une partition primaire
> 1                    # faire de cette partition la première partition
>                      # valider deux fois pour les premiers et derniers secteurs
> t                    # modifier le type de partition
> b                    # indiquer le code hexadécimal de "W95 FAT32"
> w                    # enregistrer la partition et quitter

$ mkdosfs /dev/sdX1    # création d'un filesystem DOS sur la première partition
~~~

### Avec cfdisk

[cfdisk(8)](https://manpages.debian.org/buster/fdisk/cfdisk.8.en.html) est un outil user-friendly utilisant une interface curse permettant de partitionner tout type de périphériques blocs.
Pour les options avancés il faudra  utiliser [fdisk(8)](https://manpages.debian.org/buster/fdisk/fdisk.8.en.html) ou [parted(8)](https://manpages.debian.org/buster/parted/parted.8.en.html)

Exemple pour créer une table de partition dos avec une seule partition FAT32 de la taille maximum :

~~~
# cfdisk /dev/sdX
                      ┌ Select label type ───┐
                      │ gpt                  │
                      │ dos                  │
                      │ sgi                  │
                      │ sun                  │
                      └──────────────────────┘
On choisit dos

                           Disk: wiki.img
           Size: 1 GiB, 1073741824 bytes, 2097152 sectors
                 Label: dos, identifier: 0x24cff8c9

    Device       Boot     Start       End  Sectors   Size  Id Type
>>  Free space             2048   2097151  2095104  1023M            






     [   New  ]  [  Quit  ]  [  Help  ]  [  Write ]  [  Dump  ]

On choisit New

                           Disk: wiki.img
           Size: 1 GiB, 1073741824 bytes, 2097152 sectors
                 Label: dos, identifier: 0x24cff8c9

    Device       Boot     Start       End  Sectors   Size  Id Type
>>  Free space             2048   2097151  2095104  1023M            






 Partition size: 1023M

On laisse la taille max

                           Disk: wiki.img
           Size: 1 GiB, 1073741824 bytes, 2097152 sectors
                 Label: dos, identifier: 0x24cff8c9

    Device       Boot     Start       End  Sectors   Size  Id Type
>>  Free space             2048   2097151  2095104  1023M            






                       [ primary]  [extended]


                    0 primary, 0 extended, 4 free

On laisse en primaire

                           Disk: wiki.img
           Size: 1 GiB, 1073741824 bytes, 2097152 sectors
                 Label: dos, identifier: 0x24cff8c9

    Device       Boot     Start       End  Sectors   Size  Id Type
>>  wiki.img1              2048   2097151  2095104  1023M  83 Linux  



 ┌─────────────────────────────────────────────────────────────────┐
 │Partition type: Linux (83)                                       │
 └─────────────────────────────────────────────────────────────────┘
     [Bootable]  [ Delete ]  [ Resize ]  [  Quit  ]  [  Type  ]
     [  Help  ]  [  Write ]  [  Dump  ]

On change le type en FAT32

               ┌ Select partition type ──────────────┐
               │  0 Empty                            │
               │  1 FAT12                            │
               │  2 XENIX root                       │
               │  3 XENIX usr                        │
               │  4 FAT16 <32M                       │
               │  5 Extended                         │
               │  6 FAT16                            │
               │  7 HPFS/NTFS/exFAT                  │
               │  8 AIX                              │
               │  9 AIX bootable                     │
               │  a OS/2 Boot Manager                │
               │  b W95 FAT32                        │
               └───────────────────────────────────↓─┘

                           Disk: wiki.img
           Size: 1 GiB, 1073741824 bytes, 2097152 sectors
                 Label: dos, identifier: 0x24cff8c9

    Device       Boot   Start      End  Sectors   Size  Id Type
>>  wiki.img1            2048  2097151  2095104  1023M   b W95 FAT32 



 ┌─────────────────────────────────────────────────────────────────┐
 │Partition type: W95 FAT32 (b)                                    │
 └─────────────────────────────────────────────────────────────────┘
     [Bootable]  [ Delete ]  [ Resize ]  [  Quit  ]  [  Type  ]
     [  Help  ]  [  Write ]  [  Dump  ]

On sauvegarde et on quitte
~~~


