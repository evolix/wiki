---
title: Howto Fluentd
categories: log sysadmin 
...

* Site officiel : <https://www.fluentd.org/>
* Documentation : <https://docs.fluentd.org>
* Code source : <https://github.com/fluent/fluentd/>

[Fluentd](https://www.fluentd.org/) est un outil de collecte de données développé à l'origine par Treasure Data. Il est principalement écrit dans le langage de programmation Ruby.

## Principe

Fluentd est un outil Big Data pour des ensembles de données semi-structurés ou non structurés. Comme Apache Kafka, il analyse les journaux des événements et les journaux des applications. Selon Suonsyrjä et Mikkonen, "l'idée principale de Fluentd est d'être la couche unificatrice entre différents types d'entrées et de sorties de log".

## Installation

Sous Debian

On configure d'abord [apt](https://wiki.evolix.org/HowtoDebian/Packages) :

~~~
# cd /etc/apt/trusted.gpg.d
# wget https://artifacts.elastic.co/GPG-KEY-td-agent -O GPG-KEY-td-agent.asc
# chmod 644 GPG-KEY-td-agent.asc
# echo "deb http://packages.treasuredata.com/3/debian/stretch/ stretch contrib" > /etc/apt/sources.list.d/treasure-data.list
~~~

Puis on installe le paquet td-agent qui se veut être la version stable fournie par Treasure Data.

~~~
# apt update && apt install td-agent
~~~

## Utilisation de base

Lancer le démon :

~~~
# systemctl start td-agent.service
# systemctl status td-agent.service
~~~

Par défaut, le démon est configuré pour capturer les logs HTTP et les renvoyer vers stdout (/var/log/td-agent/td-agent.log).

On peut tester à l'aide la commande [curl](https://wiki.evolix.org/HowtocURL) :

~~~
$ curl -d 'json={"json":"message"}' http://localhost:8888/debug.test
~~~

## Configuration

Le fichier de configuration est disponible dans */etc/td-agent/td-agent.conf*

Après avoir édité ce fichier, vous devez redémarrer td-agent en utilisant systemctl:

~~~
# systemctl restart td-agent
~~~

## Logs

Par défaut, td-agent écrit ses logs dans le fichier */var/log/td-agent/td-agent.log*

## Liens

* [Post installation guide](https://docs.fluentd.org/v1.0/articles/post-installation-guide)
