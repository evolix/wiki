---
categories: network web dns
title: HowtoHappyDomain
...

* Documentation : <https://help.happydomain.org/fr/>
* Git : <https://github.com/happyDomain/happydomain>

[HappyDomain](https://www.happydomain.org) est une appli web libre developpé par une belle équipe de passionné(e)s. Son but est de "rendre la gestion de ces noms de domaine plus facile et intuitive, afin de donner à tous les moyens de son autonomie sur Internet." [plus d'infos ici](https://www.happydomain.org/fr/qui-sommes-nous/)

## Installation

### Avec le binaire :

Choisir le binaire adapté à l'architecture du serveur sur <https://get.happydomain.org/master/> et ...

~~~
wget https://get.happydomain.org/master/happydomain-linux-amd64
chmod +x happydomain-linux-amd64
~~~

### Avec docker/podman :

pour du Test:

~~~{.bash}
docker run -e HAPPYDOMAIN_NO_AUTH=1 -p 8081:8081 happydomain/happydomain
~~~

Avec persistence des données:

~~~{.bash}
docker volume create happydns_data
docker container run -v happydns_data:/data -e HAPPYDOMAIN_NO_AUTH=1 -p 8081:8081 happydomain/happydomain
~~~

> Ici happydomain va tourner dans le bridge docker par défaut, on peut aussi le mettre directement sur le réseau de l’hôte si besoin

## Configuration

On lancer `./happydomain-linux-amd64 -h` pour avoir une liste exhaustive des options disponible.

On peut mette ces options dans un fichier de configuration sous la forme `option=valeur`
Celui-ci sera cheché par happyDomain successivement à ces emplacements :

* ./happydomain.conf
* $XDG_CONFIG_HOME/happydomain/happydomain.conf
* /etc/happydomain.conf

> Il est aussi possible de specifier le fichier à utiliser en argument, par exemple `./happydomain-linux-amd64 /etc/happydomain.conf`

Le premier fichier trouvé sera utilisé, **Attention**, on ne peut pas séparer la configuration dans plusieurs fichiers.

On peut aussi directement specifier de la configuration par les variable d'environnement *ENV* ou avec des argument.

Voir <https://help.happydomain.org/fr/deploy/config/>

### Options importantes

* *bind* specifie le couple ip, port à utiliser, `127.0.0.1:8081` par exemple
* *dev* interface reseaux à sur laquel l'interface web va communiquer avec ses client (si besoin)
* *default-ns* dns à utiliser pour la resolution des noms de domaine à administrer
* *externalurl* url du service, utilisé pour envoyer les mails

**Mail**

## Editer des zones DNS

### RFC 2136 (Bind9, Knot etc)

Il faut avoir configuré une clef TSIG et ses ACL sur les zone qu'on veut administrer:
Pour chaque zone on doit pouvoir *update* des entrées et le avoir le droit de *transfer* (AXFR) la zone.
Voir [HowtoBind](/HowtoBind#mise-à-jour-dynamique)

## Plomberie

## FAQ