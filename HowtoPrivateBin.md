---
categories: web webapp
title: Howto PrivateBin
...

* Documentation : <https://privatebin.info>
* Code : <https://github.com/PrivateBin/PrivateBin>
* Licence : [Zlib/libpng, GPLv2, et autres](https://github.com/PrivateBin/PrivateBin/blob/master/LICENSE.md)
* Langage : PHP
* Ansible : <https://gitea.evolix.org/evolix/ansible-roles/src/branch/unstable/webapps/privatebin>

[PrivateBin](https://github.com/PrivateBin/PrivateBin) est une application web pour le partage sécurisé de secrets (mots de passe, etc.) ou de courts textes (code source, configuration, etc.).

## Installation

Nous installons la version **1.5.1** sous **Debian 11 (Bullseye)** en mode manuel.

PrivateBin s'appuie sur [PHP](HowtoPHP) et un serveur web (Apache, Nginx, etc.). Il est aussi possible d'utiliser un SGBD ([MariaDB/MySQL](HowtoMySQL), PostgreSQL, SQLite) pour les données bien que la configuration par défaut n'utilise que de simples fichiers (*flat files*). Dans la présente documentation, nous montrons comment installer avec Apache et sans dépendre d'un SGBD pour stocker les données.

On installe les dépendances de la manière suivante :

~~~
# apt install apache2 libapache2-mod-php libapache2-mpm-itk php-gd git
~~~

### Compte UNIX

Créer un compte UNIX *privatebin* :

~~~
# adduser --disabled-login --gecos 'PrivateBin App' privatebin
~~~

### PrivateBin

On clone le dépôt et on bâtit l'application :

~~~
# sudo -iu privatebin
$ git clone https://github.com/PrivateBin/PrivateBin.git
$ cd PrivateBin/
$ git checkout 1.5.1
~~~

### Apache

Pour accéder à l'application depuis Internet, on place un serveur web comme Apache devant. 
Il faut mettre le texte suivant dans le nouveau fichier `/etc/apache2/sites-available/privatebin.conf` :

~~~
<VirtualHost *:80 *:443>
    ServerName privatebin.evolix.org

    DocumentRoot /home/privatebin/PrivateBin
    
    <Directory /home/privatebin/PrivateBin>
        Options SymLinksIfOwnerMatch
        AllowOverride Options=All AuthConfig Limit FileInfo Indexes
        Require all granted
    </Directory>
    
    AssignUserID privatebin privatebin

    SSLEngine On
    SSLCertificateFile /etc/ssl/certs/privatebin.evolix.org.crt
    SSLCertificateKeyFile /etc/ssl/private/privatebin.evolix.org.key
    
    RewriteEngine On
    RewriteCond %{HTTPS} !=on
    RewriteCond %{HTTP:X-Forwarded-Proto} !=https
    RewriteRule ^/(.*) https://%{SERVER_NAME}/$1 [L,R=permanent]

</VirtualHost>
~~~

> **Note** : La partie SSL/TLS n'est pas développée, mais elle est essentielle au bon fonctionnement de PrivateBin. Vous pouvez par exemple générer et configurer un certificat [Let's Encrypt](HowtoLetsEncrypt) avec certbot. N'oubliez pas d'adapter les directives `SSLCertificateFile` et `SSLCertificateKeyFile` dans le vhost.  

PrivateBin fournit une configuration `.htaccess` qu'il est recommandé d'activer :

~~~
# sudo -iu privatebin
$ cd PrivateBin/
$ cp .htaccess.disabled .htaccess
$ exit
# a2enmod rewrite
# systemctl reload apache2.service
~~~

Il est aussi fortement recommandé de renforcer la sécurité de PrivateBin en déplaçant certains répertoires de l'application hors du `DocumentRoot` du vhost :

~~~
# sudo -iu privatebin
$ mkdir secret
$ cd PrivateBin/
$ mv {bin,cfg,doc,lib,tpl,tst,vendor} /home/privatebin/secret/
~~~

Avec votre éditeur de texte préféré, vous devez modifier le fichier `index.php` afin d'indiquer à PrivateBin le nouveau chemin relatif à utiliser :

~~~
define('PATH', '../secret/');
~~~

Finalement, on active le vhost, on vérifie sa syntaxe et si tout est beau on recharge la configuration d'Apache :

~~~
# a2ensite privatebin
# apache2ctl -t
# systemctl reload apache2.service
~~~

## Mises à jour

Les mises à jour peuvent être effectuées comme ceci :

~~~
# sudo -iu privatebin
$ cd PrivateBin/
$ git restore .
$ git fetch origin && git checkout <NOUV_VERSION>
$ cp -a /home/privatebin/secret /home/privatebin/secret.back
$ rm -rf /home/privatebin/secret/{bin,cfg,doc,lib,tpl,tst,vendor}
$ mv {bin,cfg,doc,lib,tpl,tst,vendor} /home/privatebin/secret/
~~~

Suite à la mise à jour du code via git et au déplacement des répertoires hors du `DocumentRoot` du vhost, vous devez remodifier le fichier `index.php` afin d'indiquer à PrivateBin le nouveau chemin relatif à utiliser ( `define('PATH', '../secret/');` ). Finalement, lorsque le bon fonctionnement de l'application est confirmé, vous pouvez supprimer le répertoire `/home/privatebin/secret.back`.

> **Note** : Ces commandes ne sont parfois pas suffisantes pour compléter une mise à jour. Vous devez systématiquement lire les [notes de versions](https://github.com/PrivateBin/PrivateBin/releases).


## Configuration

La configuration initiale de l'application peut être supplantée en faisant une copie du fichier `cfg/conf.sample.php` vers `cfg/conf.php`.

Les paramètres disponibles sont documentés dans [le wiki du projet](https://github.com/PrivateBin/PrivateBin/wiki/Configuration).

## Utilisation

Un navigateur Web moderne suffit pour utiliser PrivateBin.

Le principe est de saisir ou de copier-coller le texte qu'on souhaite partager secrètement dans la boîte `Éditer` puis de cliquer sur le bouton `Envoyer`. Une URL est alors générée pour accéder au document. C'est cette URL que vous devez partager avec votre destinataire, par exemple via courriel ou via Signal ou autre moyen de communication. Il est important de ne pas partager cette URL publiquement dans les médias sociaux ou les forums : elle ne doit être disponible que par vous et votre destinataire. 

Pour augmenter la sécurité de l'échange, il est possible d'ajouter un mot de passe et/ou de programmer la suppression automatique du document partagé immédiatement après sa première lecture par le destinataire.

