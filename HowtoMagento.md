---
categories: web ecommerce
title: Howto Magento
---

* Documentation : <https://magento.com/technical-resources>

[Magento](https://magento.com/) une plateforme e-commerce libre écrite en PHP.

## Pré-requis 

* [pré-requis pour Mangento 2.3](https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements-tech.html)
* [pré-requis pour Mangento 2.2](https://devdocs.magento.com/guides/v2.2/install-gde/system-requirements-tech.html)
* [pré-requis pour Mangento 2.1](https://devdocs.magento.com/guides/v2.1/install-gde/system-requirements-tech.html)
* [pré-requis pour Mangento 2.0](https://devdocs.magento.com/guides/v2.0/install-gde/system-requirements-tech.html)

## Protection

<https://support.hypernode.com/knowledgebase/how-to-protect-your-magento-store-against-brute-force/>

### Cloudflare

- Solution simple : <https://support.cloudflare.com/hc/en-us/articles/115004180727-Using-Cloudflare-with-your-Magento-1-online-store>
- Solution complexe : avec une extension Magento <https://www.cloudflare.com/integrations/magento/>
