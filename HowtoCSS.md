## Sélections

~~~
<p class='maclass'><span id='premier'>Un jour j'écrirai</span> <span id='monid'>mon texte</span>.</p>
~~~

Selon :

 - id : 

~~~
#monid { /* ... */ }
~~~

 - class :

~~~
.maclass { /* ... */ }
~~~

 - balise + id :

~~~
span#monid { /* ... */ }
~~~

 - le premier élément ayant comme parent :

~~~
p.maclass span:nth-child(1) { /* ... */ }
~~~

 - plusieurs éléments :

~~~
span#monid, #premier { /* ... */ }
~~~

 - tout les fils :

~~~
p * { /* ... */ }
~~~