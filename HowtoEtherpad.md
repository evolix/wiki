---
categories: web webapp
title: Howto Etherpad
...

* Documentation : <https://etherpad.org/doc/v1.8.18/>
* Code : <https://github.com/ether/etherpad-lite>
* Licence : Apache-2.0
* Langage : Javascript
* Ansible : à venir

[Etherpad](https://github.com/ether/etherpad-lite) est une application web de rédaction collaborative en temps réel développée en node.js.

## Installation

Nous installons la version **2.x** sous **Debian 12 (Bookworm)** en mode manuel. (Il y a aussi un mode d'installation pour [Docker](https://github.com/ether/etherpad-lite/blob/develop/doc/docker.md).)

Etherpad s'appuie sur [NodeJS](HowtoNodeJS) (avec pnmp) et peut être configurée avec différents SGBD ([MariaDB/MySQL](HowtoMySQL), PostgreSQL, SQLite) et proxy web ([Nginx](HowtoNginx), Apache, HAProxy, Varnish, etc). Dans la présente documentation, nous montrons comment faire avec MariaDB et Nginx.

Pour l'installation des dépendances, se référer à [NodeJS](HowtoNodeJS) pour l'installation de NodeJS 18.x, [MariaDB/MySQL](HowtoMySQL) et [Nginx](HowtoNginx)

Si pnmp n'a pas été activé, après l'installation de NodeJS, il faut jouer la commande suivante : 

```
# corepack enable pnpm
```

### Compte UNIX

Créer un compte UNIX *etherpad* :

~~~
# adduser --disabled-login --gecos 'etherpad App' etherpad
~~~

###  MariaDB

Créer (par exemple) la base de données *etherpad* et l'utilisateur Mariadb du même nom :

~~~
# mysqladmin create etherpad;
# mysql
MariaDB [(none)]> GRANT ALL PRIVILEGES ON etherpad.* TO 'etherpad'@'localhost' IDENTIFIED BY 'PASSWORD';
~~~

> **Note** : Pensez à conserver le mot de passe pour la suite.


### Etherpad

On clone le dépôt et on bâtit l'application :

~~~
# su -s/bin/bash - etherpad
$ git clone https://github.com/ether/etherpad-lite.git
$ cd etherpad-lite/
$ git checkout 2.0.3
$ src/bin/run.sh
~~~

> Le script run.sh s'occuper d'installer et de configurer tout et lance même etherpad en mode devel sur http://0.0.0.0:9001
> Une fois confirmé qu'Etherpad fonctionne, vous pouvez quitter en appuyant sur `Ctrl + C` sur le clavier.

On édite le fichier `settings.json` généré par `src/bin/run.sh`. Vous devrez nécessairement modifier au minimum les valeurs des variables relatives à la base de données et il est recommandé de faire passer la valeur de la variable `ip` de `0.0.0.0` à `127.0.0.1`.

~~~
{
  "ip": "127.0.0.1",

  "dbType" : "mysql",
  "dbSettings" : {
    "user":       "etherpad",
    "socketPath": "/run/mysqld/mysqld.sock",
    "password":   "PASSWORD",
    "database":   "etherpad",
    "charset":    "utf8mb4"
  },
}
~~~

Pour ajuster plus de paramètres, on peut s'appuyer sur la documentation très claire contenue dans le fichier `settings.json.template`.

### Unité systemd

Pour lancer l'application au démarrage du serveur, on met le texte suivant dans le nouveau fichier `/etc/systemd/system/etherpad.service`

~~~
[Unit]
Description=Etherpad - open source online editor for real-time collaborative editing.
Documentation=https://etherpad.org/doc/v2.0.3/
After=network.target
After=mariadb.service

[Service]
Type=simple
Environment=NODE_ENV=production
ExecStart=/usr/bin/pnpm --filter ep_etherpad-lite run prod
Restart=always
User=etherpad
Group=etherpad
WorkingDirectory=/home/etherpad/etherpad-lite

[Install]
WantedBy=multi-user.target
~~~

On active et on démarre l'unité en question :

~~~
# systemctl enable etherpad.service
# systemctl start etherpad.service
~~~

### Nginx

Pour accéder à l'application depuis Internet, on place nginx devant, qui agira comme serveur mandataire (*proxy*). 
Il faut mettre le texte suivant dans le nouveau fichier `/etc/nginx/sites-available/etherpad.conf` :

~~~
map $http_upgrade $connection_upgrade {
  default upgrade;
  ''      close;
}

server {
  listen 80;
  listen [::]:80;
  server_name etherpad.example.com;

  # Let's Encrypt
  include /etc/nginx/snippets/letsencrypt.conf;

  # Redirect all to https (port 443)
  location / { return 301 https://$host$request_uri; }
}

server {
  listen [::]:443 ssl http2;
  listen 443 ssl http2;
  server_name etherpad.example.com;

  ssl_certificate     /etc/letsencrypt/live/etherpad.example.com/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/etherpad.example.com/privkey.pem;

  location / {
    proxy_pass         http://127.0.0.1:9001;
    proxy_buffering    off; # be careful, this line doesn't override any proxy_buffering on set in a conf.d/file.conf
    proxy_set_header   Host $host;
    proxy_pass_header  Server;

    # Note you might want to pass these headers etc too.
    proxy_set_header    X-Real-IP $remote_addr; # https://nginx.org/en/docs/http/ngx_http_proxy_module.html
    proxy_set_header    X-Forwarded-For $remote_addr; # EP logs to show the actual remote IP
    proxy_set_header    X-Forwarded-Proto $scheme; # for EP to set secure cookie flag when https is used
    proxy_http_version  1.1; # recommended with keepalive connections

    # WebSocket proxying - from https://nginx.org/en/docs/http/websocket.html
    proxy_set_header  Upgrade $http_upgrade;
    proxy_set_header  Connection $connection_upgrade;
  }
}
~~~

> **Note** : La partie SSL/TLS n'est pas développée. Vous pouvez par exemple générer et configurer un certificat [Let's Encrypt](HowtoLetsEncrypt) avec certbot. N'oubliez donc pas de modifier les directives `ssl_` dans le vhost.  

On active le vhost, on vérifie sa syntaxe et si tout est beau on recharge la configuration de nginx :

~~~
# ln -s /etc/nginx/sites-available/etherpad.conf /etc/nginx/sites-enabled/etherpad.conf
# nginx -t
# systemctl reload nginx.service
~~~

Côté configuration d'etherpad, dans le fichier `settings.json`, il faut passer la directive `trustProxy` à **true** pour que etherpad accepte les IP clientes données dans les headers *X-Forwarded-For* pour les donner dans les logs

## Mises à jour

Les mises à jour peuvent être récupérées comme ceci :

~~~
# systemctl stop etherpad.service
# su -s/bin/bash - etherpad
$ cd etherpad-lite
$ git fetch origin && git checkout <NOUV_VERSION>
$ ./bin/installDeps.sh
$ exit
# systemctl start etherpad.service
~~~

> **Note** : Ces commandes ne sont parfois pas suffisantes. Vous devez systématiquement lire les [notes de versions](https://github.com/ether/etherpad-lite/releases).


## Configuration

Etherpad est configurable via le fichier `settings.json` ou des variables d'environnement. 

Il y a +200 [plugins](https://static.etherpad.org/) disponibles, dont environ 80 sont officiels.

## Utilisation de l'API

Son accès se fait via <https://pad.example.com/api/> où `la version` est indiqué.

Systématiquement, il y aura besoin de renseigner une clé qui se trouve sur le système de fichier :

~~~
$ cat etherpad-lite/APIKEY.txt
~~~ 

### Supprimer un pad

Admettons que nous avons le pad <https://pad.example.com/p/monsuperpad> où `monsuperpad` est le padID.

~~~
$ curl "https://pad.example.com/api/1.2.15/deletePad?apikey=${APIKEY}&padID=monsuperpad"
~~~

### Lister les pad

~~~
$ curl -s "https://pad.example.com/api/1.2.15/listAllPads?apikey=${APIKEY}"
~~~

## FAQ

### À propos des logs

On accède à la journalisation des événements de l'application via journald :

~~~
# systemctl status etherpad.service
# journalctl --unit=etherpad --no-pager
~~~
