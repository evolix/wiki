---
categories: databases nosql
title: Howto neo4j
---

* [Documentation officielle](https://neo4j.com/docs/operations-manual/current/)

Neo4j est une base de données de graphes pour Java.


## Installation

* [Documentation d'installation](https://neo4j.com/docs/operations-manual/current/installation/linux/debian/)
* [Documentation dépôt](https://debian.neo4j.com)
* [Versions disponibles](https://neo4j.com/developer/kb/neo4j-supported-versions/)
* [Dépendance Java](https://neo4j.com/docs/operations-manual/current/installation/requirements/#deployment-requirements-java)

Neo4j n'est pas disponible dans les dépôts Debian.

Configurer la clé et le dépôt du distributeur pour Apt :

~~~
wget -O - https://debian.neo4j.com/neotechnology.gpg.key | apt-key add -

# Debian 11 (Bullseye)
echo 'deb https://debian.neo4j.com stable 5' | tee /etc/apt/sources.list.d/neo4j.list

# Debian 10 (Buster)
echo 'deb https://debian.neo4j.com stable 4.4' | tee /etc/apt/sources.list.d/neo4j.list

# Debian 9 (Stretch)
echo 'deb https://debian.neo4j.com stable 3.5' | tee /etc/apt/sources.list.d/neo4j.list
~~~

Installer ensuite Neo4j :

~~~
apt update
apt install neo4j

systemctl status neo4j
~~~

S'assurer que la version de Java par défaut (`java -version`) [correspond bien à la de la version de Neo4j installée](https://neo4j.com/docs/operations-manual/current/installation/requirements/#deployment-requirements-java).

Si besoin d'une version Java différente de celle par défaut, il faudra modifier JAVACMD dans l'unité Systemd du service.

Puis, augmenter le nombre maximum de fichiers ouverts (la valeur 1024 par défaut est insuffisante : `ulimit -n`) :

~~~
# systemctl edit neo4j
[Service]
LimitNOFILE=60000  # minimum recommandé : 40000

systemctl daemon-reload
systemctl restart neo4j
~~~


## Configuration

https://neo4j.com/docs/operations-manual/current/configuration/neo4j-conf/

La configuration se trouve dans `/etc/neo4j/neo4j.conf`.

Pour [configurer l'interface web](https://neo4j.com/docs/operations-manual/current/configuration/connectors/) :

~~~
dbms.connector.http.enabled=true
dbms.connector.http.listen_address=0.0.0.0:7474
dbms.connector.http.advertised_address=test.com:7474
~~~


## Sauvegarde

* [Documentation officielle](https://neo4j.com/docs/operations-manual/5/backup-restore/)

### Versions Neo4j 5.X

Faire une sauvegarde « online » (appelée « backup ») :

~~~
neo4j-admin database backup <DATABASE> --to-path=/home/backup/neo4j/
~~~

Faire une sauvegarde « offline » (appelée « dump ») :

~~~
neo4j-admin database dump <DATABASE> --to-path=/home/backup/neo4j/neo4j-<DATABASE>-<timestamp>.dump
~~~


### Versions Neo4j 4.X

Faire une sauvegarde « online » (appelée « backup ») :

~~~
neo4j-admin backup --database=<DATABASE> --backup-dir=/home/backup/neo4j/
~~~

Faire une sauvegarde « offline » (appelée « dump ») :

~~~
neo4j-admin dump --database=<DATABASE> --to=/home/backup/neo4j/neo4j-<DATABASE>-<timestamp>.dump
~~~


### Versions Neo4j 3.X

Faire une sauvegarde :

~~~
neo4j-admin backup --backup-dir=/home/backup/neo4j/
~~~


### Anciennes versions (< Neo4j 4.0, 3.0 ?)

Note : *section non vérifiée*.

Pour réaliser un dump de la base de donnée :

~~~
/usr/bin/neo4j-shell -readonly -host 127.0.0.1 -port 1337 -c export-graphml -t -o neo4j.graphml
~~~

À noter que ça nécessite l'installation d'un plugin Neo4j pour faire une sauvegarde/restauration via GraphML : <https://github.com/jexp/neo4j-shell-tools>


## Monitoring

Mettre en place un check HTTP Nagios sur <http://localhost:7474/db/manage> (en attendant mieux).

