---
title: Howto Irssi
...

# Howto irssi

* Documentation : <https://irssi.org/documentation/>

## Installation

~~~
# apt install irssi
~~~

## Utilisation de base

~~~
/connect irc.freenode.net
/join #channel
/part
/query <nick>
/query
/part
~~~

## racourcis clavier

~~~
Echap + a = Se déplacer vers le dernier message le plus important
Ctrl + n = Se déplacer au salon suivant
Ctrl + p = Se déplacer au précédent salon
~~~

## Syntaxe Vim irssilog

~~~
$ mkdir -p ~/.vim/syntax
$ wget 'http://www.vim.org/scripts/download_script.php?src_id=10584' -O ~/.vim/syntax/irssilog.vim

vim 2016-08.log
:syntax on
:set syntax=irssilog
~~~

## Gérer le layout des windows

~~~
/WINDOW show x # créer une vue splité sur la fenêtre x
/WINDOW close # fermer la fenêtre courante
/WINDOW shrink|grow # rétrécir ou agrandir la fenêtre courante
/WINDOW balance # taille des fenêtres visible à l'identique
/WINDOW move x # déplace la fenêtre actuelle sur le numéro x
/WINDOW move UP # déplace la fenêtre sur celle du dessus
/WINDOW number x # change le numéro de la fenêtre active
/WINDOW show x # séparer la frame en deux avec la fenêtre x affiché : il faut que le mode 'sticky' soit désactivé pour cette fenêtre /WINDOW stick off
/WINDOW log ON <file> #activer les logs pour cette fenêtre <- afin de faire une surveillance sur le fichier ou autre possibilité
~~~

Ne pas hésiter à sauvegarder sa session actuelle (thème)

~~~
/SAVE
~~~

## Action selon touche pressé

~~~
/BIND ^[OP key F1
/BIND ^[11~ key F1
/BIND F1 /me a besoin d'aide !
~~~

## Envoyer une commande CTCP à un utilisateur

~~~
/CTCP user (PING|VERSION|USERINFO|CLIENTINFO|TIME)
~~~

## Loguer toutes les conversations

Log les channels mais aussi les conversations privés :

~~~
/LOG START ~/irclogs/irssi-%Y-%m-%d
/SET autolog ON
~~~

## Highlight de son pseudo

~~~
/highlight mon-pseudo
~~~

Pour désactiver tout highlight personnalisé:

~~~
/dehighlight 1
~~~

Le chiffre 1 représente l'ordre à laquelle on ajoute les filtres.

## Levels

Des "levels" sont attribués aux messages en fonction de leur type. On peut en obtenir la liste avec la commande :

~~~
/help levels
~~~

## Ignorer des messages

La commande ignore permet de ne pas afficher certains messages. On peut les ignorer en fonction d'un mask, d'un channel et d'un level. Attention, les messages ne seront plus affichés (à l'exception du level NO_ACT) !

~~~
/ignore $mask|$channel $LEVEL
~~~

La commande prend plusieurs options pour affiner son comportement (pour les découvrir : `/help ignore`)

Exemples simples :

~~~
# Ignorer tous les messages du level NICKS (changement de pseudo) :
/ignore * NICKS
# De manière plus large, ignorer tous les messages d'information des niveaux suivants :
/ignore * JOINS PARTS QUITS NICKS
# Ou, pour seulement les ignorer dans la zone d'activité de la barre de statut, on a le level spécial NO_ACT (attention, le level NO_ACT est buggé dans les vieilles versions d'Irssi !) :
/ignore * NO_ACT -JOINS -PARTS -QUITS -NICKS

# Lister ce qui est ignoré :
/ignore
# Retirer une entrée de la liste ("désignorer") :
/unignore $i
~~~

Les options suivantes permettent aussi de gérer ce qui s'affiche dans la barre d'activité :

~~~
# Pour que certains niveaux ne déclenchent pas d'activité :
/set activity_hide_level QUITS JOINS PARTS NICKS
# Pour certains channels ne déclenchent pas d'activité :
/set activity_hide_targets #channel1 #channel2
~~~


## Scripts sur irssi

Créer ou copier les scripts dans ~/.irssi/scripts/

~~~
/SCRIPT load <chemin>
/SCRIPT unload <nom script>
~~~


## Notifications

Pour avoir des notifications avec irssi, on pourra utiliser le plugin [fnotify](https://scripts.irssi.org/scripts/fnotify.pl) qui va écrire tous les messages nous étant destinés dans ~/.irrsi/fnotify. On peut aussi le limiter à seulement une fenêtre (/WINDOW log), et le combiner aussi au plugin [highlite](https://scripts.irssi.org/scripts/highlite.pl).

On pourra ensuite parser ce fichier et envoyer des notifications. Voici un exemple de script avec irssi qui tourne dans un screen sur un serveur distant :

~~~{.bash}
#!/bin/bash

if [ -n "$IRSSI_SCREEN_SRV" ]; then
        ssh -q $IRSSI_SCREEN_SRV ": > .irssi/fnotify;tail -f .irssi/fnotify" > >(\
        while read heading message; do
                notify-send "${heading}" "${message}" -t 5000
        done) & \
        FNOTIFY_PID=$!
        x-terminal-emulator -e "ssh -t $IRSSI_SCREEN_SRV screen -r -D"
        kill $FNOTIFY_PID 2&> /dev/null
else
        notify-send 'Error' 'You must set IRSSI_SCREEN_SRV !' -t 10000
fi
~~~

Recherche :

~~~
/lastlog <pattern>
/lastlog -hilight
/lastlog -clear
~~~

## Rejoindre des canaux par défaut

~~~
/channel add -auto #canal1 nom_serveur
/channel add -auto #canal2 nom_serveur
~~~
