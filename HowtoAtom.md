* Site officiel : <https://atom.io/>
* Documentation : <https://atom.io/docs>

## Installation

Atom est disponible sous forme de paquets Debian, mais il n'y a pas de dépôt disponible. Il faut donc l'installer et le mettre à jour manuellement :

~~~
# wget https://atom.io/download/deb -O /tmp/atom-amd64.deb && dpkg -i /tmp/atom-amd64.deb
~~~

## Astuces

### Raccourcis :

~~~
Crtl+Maj+f : Rechercher un mot clé dans tout le projet.
~~~

### Modifier l'interface

  * Cacher/Afficher la barre d'outil :

~~~
shift+ctrl+p puis chercher "menubar" pour sélectionner "Window: Toggle Menu Bar"
~~~