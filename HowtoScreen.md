---
title: Howto Screen
categories: sysadmin
...

Screen permet de gérer des terminaux virtuels, notamment lancés en arrière plan.
Il permet ainsi de lancer facilement des process dans un terminal et de les laisser tourner sans rester connecté.

<https://www.gnu.org/software/screen/manual/screen.html>

## Installation

~~~
# apt install screen
~~~

## Utilisation de base

Lister les screens lançés :

~~~
$ screen -ls
There are screens on:
        30742.pts-6.server  (19/09/2016 18:24:12)   (Detached)
        4288.pts-2.server   (21/05/2015 18:07:39)   (Detached)
2 Sockets in /var/run/screen/S-jdoe.
~~~

Pour lancer un nouveau screen (l'option *-S foo* est facultative, elle permet juste de nommer son screen) :

~~~
$ screen -S foo
~~~

Pour se rattacher à un screen existant avec le numéro 30742 (voir screen -ls) :

~~~
$ screen -rd 30742
~~~

Pour se rattacher à un screen existant et utilisé en partage (permettant de voir à plusieurs un terminal) :

~~~
$ screen -x 30742
~~~

Une fois connecté à un screen, voici la liste des commandes de base utiles :

~~~
Ctrl+a+d : sortir du screen
Ctrl+a+a : faire un Ctrl+a
<Ctrl>+a+<Echap> OU <Ctrl>+a+[ : remonter dans le buffer d'un screen
<Ctrl>+a+s : freezer le screen courant
<Ctrl>+a+q : defreezer le screen courant (utile quand on freeze par erreur...)
~~~

## Utilisation avancée

Supprimer les screen "morts" :

~~~
$ screen -wipe
~~~

Lancer une commande un screen en le laissant détaché :

~~~
$ screen -S title -dm bash -c "sleep 600; date"
~~~

Une fois connecté à un screen, voici la liste des commandes pour gérer les onglets :

~~~
<Ctrl>+a+c : ouvrir un nouvel onglet
<Ctrl>+a+A (ou T ??) : renommer l'onglet courant
<Ctrl>+a+n : passer a l'onglet suivant
<Ctrl>+a+p : revenir a l'onglet precedent
<Ctrl>+a+w : afficher le statut des onglets
<Ctrl>+a+<N> : aller au Nième onglet
<Ctrl>+a+a+<X> : action sur une fenêtre screen interne (screen dans un screen)
~~~

Pour découper un screen verticalement :

~~~
<Ctrl>+a+S : spliter un screen verticalement
<Ctrl>+a+X : détruire la region splitée
<Ctrl>+a+<Tab> : se déplacer entre les régions splitées
~~~

Gestion des copier/coller :

~~~
<Ctrl>+a+<Echap> OU <Ctrl>+a+[ : se déplacer dans le screen (active le mode copie)
M : pour se positionner en milieu d'écran
w : se déplacer un mot vers l'avant
b : se déplacer un mot en arrière
g : se déplacer en début de buffer
G : se déplacer en fin de buffer
<espace>+<espace> : sélectionner la zone à copier
<Ctrl>+a+] : coller
~~~

## screen avec port série

~~~
$ screen /dev/ttyS0 38400
~~~

## FAQ

### Erreur *Cannot open your terminal '/dev/pts/N' - please check.*

On ne peut pas lancer/attacher screen après avoir fait un *sudo* ou *su*.
La solution propre est d'utiliser SSH à la place (rappel, rien n'empêche de faire un ssh sur localhost).
Le contournement temporaire est de faire un *chown 666 /dev/pts/N*

### Redimensionner terminal après `screen -x`

Lorsqu’on s’attache à une session où un autre client est déjà attaché avec `screen -x`, la taille du terminal (lignes et colonnes) sera celle du premier client qui s’est attaché à la session. On peut redimensionner la fenêtre en avec `<Ctrl> a + F`.

Ça changera la taille pour tout les clients attachés.

### Afficher une barre comme le fait Tmux

On peut faire afficher une barre en bas (ou en haut) de la fenêtre comme le fait Tmux par défaut. Il faut utiliser la commande `hardstatus` (ou `caption`)

1. Passer en mode commande : `C-a :`.
2. Entrer la commande : `hardstatus alwayslastline "[ %H / %S ] %-w%n %t*%+w"`

Ça affichera une barre d’état en bas de la fenêtre. La barre ressemblera à ceci :

~~~
[ hostname / sessionname ] 0 irssi*  1 mutt  2 bash  3 bash
~~~

Exemple plus complexe : `hardstatus alwayslastline "[ %H / %S ]%=%-w>> %n %t <<%+w%=%0c"`

Affichera :

~~~
[ hostname / sessionname ]              0 irssi  1 mutt  >> 2 bash <<  3 bash              HH:MM
~~~

### Remonter une notification lors d’une alerte dans une autre fenêtre

Dans ce cas, on utilise Screen dans un émulateur de terminal dans une session X. Nous avons deux fenêtres dans notre session Screen : `0 irc` et `1 bash`. On est sur la fenêtre Screen `1 bash`. Lorsque la fenêtre X n’est pas _focus_, une notification dans la fenêtre `0 irc` ne sera pas remontée par Screen à l’émulateur de terminal puis au gestionnaire de fenêtres. Pour que Screen remonte la notification quelque soit la fenêtre visible, il faut ajouter '^G' (deux caractères) à la variable `bell_msg` :

~~~
bell_mgs 'Bell in window %n^G'
~~~

Plus d’informations : [Screen User's Manual: Bell](https://www.gnu.org/software/screen/manual/html_node/Bell.html).
