---
categories: wip
title: Howto Vim
...

* Documentation : <hhttps://www.vim.org/docs.php>

[Vim](https://www.vim.org/) est un éditeur de texte libre recommandé pour l'administration système.
Vim est une couche de Vi apportant de nombreuses possibilités de personnalisation.
Merci et RIP à [Bram Moolenaar](https://fr.wikipedia.org/wiki/Bram_Moolenaar) qui est l'initiateur de ce logiciel.

## Installation

~~~{.bash}
# apt install vim
~~~

Pour éviter de se tromper d'éditeur, nous pouvons le définir par défaut :

~~~{.bash}
# update-alternatives --config editor
~~~

Sur nos serveurs, nous appliquons la configuration personnalisée suivante

~~~
# vim /etc/vim/vimrc.local

let g:skip_defaults_vim = 1
set autoread
~~~

Nous vous recommandons de suivre en quelques minutes le tutoriel pour apprendre les bases :

~~~{.bash}
$ vimtutor
~~~

Il faut savoir qu'il y a deux vues distinctes que l'on peut apercevoir en bas de l'interface ; le mode "-- VISUEL --" et le mode "-- INSERTION --". Avant de lancer les commandes que nous allons présenter plus bas, il faut d'abord savoir que le mode insertion s'active avec la touche `i` et pour revenir en mode visuel, il suffit d'appuyer sur la touche `Echap`.

En cas de besoin pour obtenir de l'aide :

~~~
:help user-manual
~~~

##  Premiers pas

Commençons par connaître une liste de raccourcis "vitales" :

~~~
a = "passer en mode «Insert» à droite du curseur"
:q! = "quitter sans sauvegarder"
:wq ou :x = "sauvegarder et quitter"

dd = "supprimer (et couper) la ligne actuelle"
v = "sélection du texte à partir du curseur"
V = "sélection du texte par ligne"
y ou Y = "copier la ligne actuelle"
p = "coller la ligne précédemment sélectionné"
u = "Annule la modification"
Ctrl+r = "Reviens sur annulation de la précédente modification"

zz = "recentre le texte à se trouve le curseur"
XG = "place le curseur à la ligne X"
gg = "Début du fichier"
G = "fin du fichier"

/mot = "Recherche le terme 'mot' vers le bas"
/mot\c = "Recherche le terme 'mot' vers le bas et sans prendre en compte la casse"
?mot = "Recherche le terme 'mot' vers le haut"
~~~


## Commandes avancées

La barbe commence à pousser ? C'est que vous êtes sur le bon chemin jeune padawan. Voici une seconde liste d'astuces pour éditer encore plus vite.

~~~

i = "passer en mode «Insert» - à gauche du curseur"
I = "passer en mode «Insert» - en début de phrase"
A = "passer en mode «Insert» - en fin de phrase"

x ou . = "effacer le caractère à droite"
X = "effacer le caractère à gauche"
rX = "remplace un caractère par X"
R = "Remplace les mots après le curseur"
U = Restaure la ligne à son état d’origine
cw = "change word - change le mot après l'emplacement du curseur"
:s/ancien/nouveau/g = "remplace le mot ancien par le mot nouveau"
:%s/aa/bb/g = "remplace toutes les récurrences dans un fichier"
ce = "édit le mot positionné à côté du curseur."
c$ = "édit tout le reste de ligne."
cG ="édit tout le reste du document après sur curseur"
~ = "inverser la casse d'un caractère"

e = "place le curseur à la fin d'un mot"
0 ou ^ = "place le curseur en début de ligne"
w = "fin du mot"
$ = "fin de ligne"
% = "Trouve le caractère fermant de '( [ ou {' 

:e <fichier> = "Après avoir sauvegardé son travail, on peut éditer directement un autre fichier"

ZZ = Équivalent à :wq
~~~

## Options

~~~
:set hlsearch = "active l'option de surbrillance lors de la recherche d'un mot"
:syntax on = "coloration syntaxique "
:color desert = "change la couleur pour *desert*"
~~~

Pour désactiver les options, il faudra ajouter le préfixe "no". Voici un exemple :

~~~
:set nolist = "Afficher les caractères invisibles"
~~~


### Afficher les caractères invisibles

Pour afficher les caractères invisibles :

~~~
:set list
~~~

Pour customiser les caractères utilisés (respectivement : tabulations, espaces insécables, espaces en fin de ligne retours à la ligne), par exemple :

~~~
:set listchars=tab:→\ ,nbsp:␣,trail:␣,eol:↩
~~~

On notera le `\ ` pour `tab`, car vim attend deux caractères.

Pour les désactiver :

~~~
:set nolist
~~~




## Usages spécifiques 

  * Incrémenter / Décrémenter un nombre :

~~~
Ctrl+a = "Incrémenter"
Ctrl+x = "Décrémenter"
~~~

  * Commenter du code selon la procédure suivante :

~~~
Ctrl+v = "Mode «Visual» activé"
<fleches_directionneles> = "Choisir la zone concernée"
I = "Insérer en début de ligne - active le mode «Insert»"
<char> = "Caractère du commentaire en question - selon langage"
Echap = "Quitter le mode «Insert»"
~~~

   * Pour dé-commenter, suivre la liste des commandes suivantes :

~~~
Ctrl+v = "Mode «Visual» activé"
<flèches directionnelles> = "Choisir la zone concernée"
x = "Supprimer le caractère"
~~~

  * Ajouter/enlever de la tabulation :

~~~
Ctrl+v = "Mode «Visual» activé"
<flèches directionnelles> = "Choisir la zone concernée"
tab+> = "Déplace la sélection vers la droite et inversement avec la touche <"
~~~

  * Revenir 10 minutes plus tôt :

~~~
:earlier 10m
~~~

  * Taper des caractères UTF8

~~~
Ctrl+V u XXXX = "où `XXXX` est le code du caractère UTF8"
~~~

  * Taper des caractères ASCII

~~~
Ctrl+v NNN = "où `NNN` est le code du caractère UTF8"
~~~

  * Rechercher et remplacer dans portion d'un fichier (méthode sed)

Admettons que nous avons éditons un fichier d'une dizaine de ligne où les mots anciens doivent être remplacés par nouveaux. Ce changement doit uniquement s'opérer entre la ligne 2 et 6 :

~~~
:2,6s/anciens/nouveaux/g
~~~

  * Scripting

Exécuter automatiquement des commandes :

~~~{.bash}
$ vim ":retab" "+wq"
~~~

Enregistrer la sortie d'un script dans un fichier :

~~~{.bash}
$ vim -s mon-script.txt FICHIER.txt
~~~

  * Quitter Vim avec un code de sortie > 0

~~~
:cq
~~~

Cas d'usage : annuler un commit Git en cours de rédaction, cf. [/HowtoGit](/HowtoGit#quitter-l%C3%A9diteur-de-texte-sans-que-git-nex%C3%A9cute-laction-li%C3%A9e)

  * Dans tout le fichier, si la ligne contient `todo`, supprimer la ligne

~~~
:% g/todo/ d
~~~

  * De la ligne actuelle jusqu'à la fin du fichier, si la ligne n'est pas vide
    et ne commence pas par `backend`, indenter la ligne

~~~
:.,$ v/^$/ v/^backend/ s/^/    /
~~~

## Webografie

* [Documentation sur le site officiel](http://www.vim.org/docs.php)
* [Vim casts](http://vimcasts.org/)
* [VimAwesome - liste de nombreux plugins](http://vimawesome.com/)
* [Wallpaper movement commands](https://bitbucket.org/tednaleid/vim-shortcut-wallpaper/src)
* [Mémento des touches de racourcis](http://vimsheet.com)

## FAQ

Fonction "undo" même après fermeture, reboot… : <https://vi.stackexchange.com/questions/6/how-can-i-use-the-undofile#53>

