## TL;PL

Si vous n'avez pas le temps de lire la documentation d'OpenStack, leur _[cheat sheet](https://docs.openstack.org/ocata/user-guide/cli-cheat-sheet.html)_ est un bon début.

## Serveur

### Création d'un serveur

NOTE : Toutes les commandes de cette section sont executées dans le shell `openstack`. Vous pouvez prefixer ces commandes avec `openstack `.

Pour créer un serveur, il faut d'abord savoir quelles ressources il doit utiliser (`flavor`) ainsi que l'image sur laquelle il va booter. On peut obtenir une liste des options disponibles avec les commandes suivantes :

~~~
flavor list --sort-column Name
image list
~~~

Pour exemple on créé une machine en Debian 11 de type `d2-2` nomée `foo` :

~~~
server create --flavor d2-2 --image Debian\\ 11 foo
~~~

### Arrêt d'un serveur

Si l'on souhaite arrêter un serveur tout en gardant les modifications effectuées sur son image pour, par exemple, relancer le serveur plus tard ou l'utiliser comme modèle pour plusieurs autres serveurs, il faut :

~~~
server image create --name foo-bullseye foo
image show foo-bullseye
server delete foo
server list
~~~

Pour relancer le serveur, il suffit de créer un nouveau serveur en choisissant l'image `foo-bullseye`.
