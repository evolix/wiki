---
categories: web
title: Howto Squid
...

* Documentation : <http://www.squid-cache.org/Versions/v3/3.5/cfgman/>
* Rôle Ansible : <https://forge.evolix.org/projects/ansible-roles/repository/show/squid>

[Squid](http://www.squid-cache.org/) est serveur proxy HTTP/HTTPS.

## Installation

~~~
# apt install squid squidclient

$ /usr/sbin/squid -v | head -3
Squid Cache: Version 3.5.23
Service Name: squid
Debian linux

# systemctl status squid
● squid.service - LSB: Squid HTTP Proxy version 3.x
   Loaded: loaded (/etc/init.d/squid; generated; vendor preset: enabled)
     Docs: man:systemd-sysv-generator(8)
 Main PID: 20892 (squid)
   CGroup: /system.slice/squid.service
           ├─20890 /usr/sbin/squid -YC -f /etc/squid/squid.conf
           ├─20892 (squid-1) -YC -f /etc/squid/squid.conf
           ├─20893 (logfile-daemon) /var/log/squid/access.log
           └─20894 (pinger)
~~~

> *Note* : Pour Debian 8, il faut installer ainsi :
>
> ~~~
> # apt install squid3 squidclient
> ~~~


## Configuration

La configuration par défaut est dans le fichier `/etc/squid/squid.conf`.

Nous utilisons une configuration alternative en spécifiant via `/etc/default/squid` :

~~~
CONFIG=/etc/squid/evolinux-defaults.conf
SQUID_ARGS="-YC -f $CONFIG"
~~~

`/etc/squid/evolinux-defaults.conf`:

~~~
http_port 127.0.0.1:3128
coredump_dir /var/spool/squid
max_filedescriptors 4096

acl SSL_ports port 443
acl Safe_ports port 80          # http
acl Safe_ports port 21          # ftp
acl Safe_ports port 443         # https
acl Safe_ports port 70          # gopher
acl Safe_ports port 210         # wais
acl Safe_ports port 1025-65535  # unregistered ports
acl Safe_ports port 280         # http-mgmt
acl Safe_ports port 488         # gss-http
acl Safe_ports port 591         # filemaker
acl Safe_ports port 777         # multiling http
acl CONNECT method CONNECT
acl Whitelist_domains dstdom_regex -i "/etc/squid/evolinux-whitelist-defaults.conf"
acl Whitelist_domains dstdom_regex -i "/etc/squid/evolinux-whitelist-custom.conf"
include /etc/squid/evolinux-acl.conf

http_access deny !Safe_ports
http_access deny CONNECT !SSL_ports
http_access allow localhost manager
http_access deny manager
include /etc/squid/evolinux-httpaccess.conf
http_access allow localhost
http_access deny all

refresh_pattern ^ftp:           1440    20%     10080
refresh_pattern ^gopher:        1440    0%      1440
refresh_pattern -i (/cgi-bin/|\?) 0     0%      0
refresh_pattern .               0       20%     4320

logformat combined %>a %[ui %[un [%tl] "%rm %ru HTTP/%rv" %>Hs %<st "%{Referer}>h" "%{User-Agent}>h" %Ss:%Sh
access_log /var/log/squid/access.log combined

include /etc/squid/evolinux-custom.conf
~~~

### Configuration en mode Proxy local

Pour sécuriser un serveur, on peut installer un proxy HTTP en local.
Cela permet d'interdire ou de garder des traces des requêtes HTTP faites vers l'extérieur

`/etc/squid/evolinux-custom.conf` :

~~~
http_port 8888 transparent
cache deny all
ignore_expect_100 on
tcp_outgoing_address 192.0.2.42
~~~

Le fichier `/etc/squid/evolinux-whitelist-defaults.conf` contiendra les noms de domaine autorisés en sortie :

~~~
^mirror.evolix.org$
^security.debian.org$
^pub.evolix.org$
~~~

> *Note* : la liste des noms de domaine autorisés en sortie peut-être trouvée sur <https://forge.evolix.org/projects/ansible-roles/repository/revisions/stable/entry/squid/files/evolinux-whitelist-defaults.conf>


Le fichier `/etc/squid/evolinux-acl.conf` contient les adresses IPv4 locales :

~~~
acl LOCAL src 192.0.2.42/32
~~~

`/etc/squid/evolinux-httpaccess.conf` :

~~~
http_access deny !Whitelist_domains
http_access allow LOCAL
~~~

Il reste évidemment à rediriger les requêtes HTTP en sortie vers Squid avec IPTables :

~~~
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -m owner --uid-owner proxy -j ACCEPT
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -d  192.0.2.42 -j ACCEPT
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -d 127.0.0.1 -j ACCEPT
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -j REDIRECT --to-port 8888
~~~

### Configuration en mode Proxy pour réseau local

`/etc/squid/evolinux-custom.conf` :

~~~
http_port 3128 transparent
error_directory /usr/share/squid3/errors/custom
email_err_data on
cache_mgr proxy@example.com
negative_ttl 0
cache deny badreq
coredump_dir /var/spool/squid3
cache_dir ufs /var/spool/squid3 800 16 256
cache_mem 128 MB
maximum_object_size_in_memory 512 KB
maximum_object_size 64 MB
~~~

Le fichier `/etc/squid/evolinux-whitelist-defaults.conf` contiendra les noms de domaine autorisés en sortie :

~~~
^mirror.evolix.org$
^security.debian.org$
^pub.evolix.org$
~~~

> *Note* : la liste des noms de domaine autorisés en sortie peut-être trouvée sur https://forge.evolix.org/project/ansibles-roles/repository...


Le fichier `/etc/squid/evolinux-acl.conf` contient les adresses IPv4 locales :

~~~
acl badreq http_status 400
acl LOCAL src 192.0.2.42/32
~~~

`/etc/squid/evolinux-httpaccess.conf` :

~~~
http_access deny !Whitelist_domains
http_access allow LOCAL
~~~

#### rate-limiting

Voici une configuration qui définit 3 *delay_pools* et qui en utilise un des trois pour l'acl _localnet_ :

~~~
## rate-limiting

delay_pools 3

# Débit non limité
delay_class 1 2
delay_parameters 1 -1/-1 -1/-1
# Débit global non limité
# Debit d'un poste limité à 1310720 bytes/s, soit environ 10Mb/s
delay_class 2 2
delay_parameters 2 -1/-1 1310720/1310720
# Débit global limité à 120000 bytes/s, soit 960 Kbit/s
# Débit d'un poste limité à 40000 bytes/s, soit 320 Kbit/s
delay_class 3 2
delay_parameters 3 120000/120000 40000/40000

delay_access 1 deny all
delay_access 2 allow localnet
delay_access 2 deny all
delay_access 3 deny all
~~~

## Administration

Vérification de la syntaxe de la configuration actuelle :

~~~
# squid -k parse
~~~

## Monitoring

### Log2mail

Afin de détecter des oublis ou des éventuelles attaques, il sera intéressant d'ajouter la configuration suivante au logiciel _log2mail_ : 

~~~
file = /var/log/squid/access.log
  pattern = "TCP_DENIED/403"
  mailto = ADRESSE-MAIL-ALERTE
  template = /etc/log2mail/mail
~~~

On n'oubliera pas d'ajouter l'utilisateur _log2mail_ dans le groupe _proxy_ pour qu'il puisse lire ce fichier (sinon _log2mail_ plante !!) :

~~~
# adduser log2mail proxy
Adding user `log2mail' to group `proxy' ...
Adding user log2mail to group proxy
Done.
~~~


## FAQ

### Infos à propos de Squid

Pour avoir des informations à propos de Squid :

~~~
$ squidclient -h 127.0.0.1 -p 3128 cache_object://localhost/
$ squidclient -h 127.0.0.1 -p 3128 mgr:info
~~~

### WARNING! Your cache is running out of filedescriptor

En utilisation intensive, si vous obtenez un message '''WARNING! Your cache is running out of filedescriptors''',
c'est qu'il est nécessaire d'augmenter le maximum de fichiers ouverts... mais ce problème est normalement résolu
depuis Debian Lenny où l'on trouve un '''ulimit -n 65535''' dans le script d'init (c'était à ajouter en ''Etch'').


### Résolution DNS

Squid "cache" les nameserver de /etc/resolv.conf car il gère son propre resolver : <http://wiki.squid-cache.org/Features/Dnsserver>  
Voir aussi <https://www.jethrocarr.com/2014/07/05/funny-tasting-squid-resolver/>  
En conséquence, si vous changez votre /etc/resolv.conf vous devez recharger Squid !

### Récupération des UID des requêtes sortantes

On peut le faire avec iptables :

~~~
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -j LOG --log-uid
~~~

Extrait de logs :

~~~
Jun 29 15:08:30 foo kernel: [14743572.575111] IN= OUT=eth0 SRC=192.0.2.1 DST=192.0.2.2 LEN=52 TOS=0x00 PREC=0x00 TTL=64 ID=51425 DF PROTO=TCP SPT=53175 DPT=80 WINDOW=29200 RES=0x00 SYN URGP=0 UID=1137 GID=1071
~~~

### Arrêt rapide de squid

Il suffit de jouer sur la directive `shutdown_lifetime`. Par défaut, la valeur est à 30 secondes.
Mais on peut ajuster la valeur comme on le souhaite.

Pour un arrêt immédiat. On passera la valeur à 0.

~~~
shutdown_lifetime 0 seconds
~~~

### Request entities

Si l'on a ce type d'erreur :

~~~
HTTP/1.1 411 Length Required
~~~

Squid bloque les requêtes GET et HEAD avec des *request entities* par défaut. Pour les autoriser, on peut suivre la procédure suivante.

~~~
# echo 'request_entities on' >> /etc/squid/evolinux-custom.conf
# systemctl reload squid.service
~~~
