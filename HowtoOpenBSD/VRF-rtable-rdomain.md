---
categories: openbsd network
title: HowToOpenBSD/VRF (rtable/rdomain)
---

L'idée d'utiliser les VRFs (Virtual Routing and Forwarding) est d'aller plus loin dans l'isolation des flux. Depuis OpenBSD 4.9 (IPv4) et OpenBSD 5.5 (IPv6) il est possible d'utiliser des rtable pour ajouter une séparation logique, créer plusieurs tables de routage. Nous allons donc voir comment créer des tables de routage suplémentaires et comment permettre la communication entre celles-ci (VRF leaking) grâce à l'utilisation de PF.

## Pour créer une nouvelle table de routage (rtable).

~~~
# route -T 1 add default 10.0.0.42
~~~

En ajoutant une route dans une table inexistante, celle-ci sera créée.

## Pour attribuer une interface à une table de routage.

~~~
# ifconfig vlan10 rdomain 1
~~~

En attribuant une interface à une rtable, tout le trafic passant par cette interface sera "forwardé" en se basant sur les informations contenues dans celle-ci. L'ensemble d'une ou plusieurs interfaces affiliées à une rtable est appelé domaine de routage (rdomain).

## Une fois cela fait, on peut tester une commande en utilisant une table de routage spécifique.

Avec ping par exemple

~~~
# ping -V 1 10.0.0.42
~~~

Certaines commandes n'ont pas d'options spécifiques, on peut alors
utiliser la commande route avec le paramètre -T suivi du numéro de
rtable et de exec

~~~
# route -T 1 exec ssh 10.0.0.42
~~~

## Attribuons une autre interface dans une autre table de routage.

Attribuons vlan20 au rdomain 2

~~~
# ifconfig vlan20 rdomain 2
~~~

Nous avons donc maintenant em0 dans la table de routage par default, l'interface vlan10 dans la rdomain 1 et vlan20 dans la rdomain 2

Chaque rtable dispose donc de ses propres informations (Routes,ARP), il est donc possible par exemple de diposer de deux interfaces avec la même adresse IP.

~~~
# ifconfig vlan10 10.0.0.10 netmask 255.255.255.0
# ifconfig vlan20 10.0.0.10 netmask 255.255.255.0
~~~

~~~
# ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 32768
        priority: 0
        groups: lo
        inet6 ::1 prefixlen 128
        inet6 fe80::1%lo0 prefixlen 64 scopeid 0x4
        inet 127.0.0.1 netmask 0xff000000
em0: flags=28843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST,NOINET6> mtu 1500
        lladdr 52:54:00:03:56:0b
        priority: 0
        groups: egress
        media: Ethernet autoselect (autoselect)
        inet 192.168.4.241 netmask 0xffffff00 broadcast 192.168.4.255
em1: flags=28843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST,NOINET6> mtu 1500
        lladdr 52:54:00:93:0f:aa
        priority: 0
        media: Ethernet autoselect (100baseTX full-duplex)
        status: active
enc0: flags=20000<NOINET6>
        priority: 0
        groups: enc
        status: active
pflog0: flags=20141<UP,RUNNING,PROMISC,NOINET6> mtu 33144
        priority: 0
        groups: pflog
vlan10: flags=28843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST,NOINET6> rdomain 1 mtu 1500
        lladdr 52:54:00:93:0f:aa
        priority: 0
        vlan: 10 parent interface: em1
        groups: vlan
        status: active
        inet 10.0.0.10 netmask 0xffffff00 broadcast 10.0.0.255
vlan20: flags=28843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST,NOINET6> rdomain 2 mtu 1500
        lladdr 52:54:00:93:0f:aa
        priority: 0
        vlan: 20 parent interface: em1
        groups: vlan
        status: active
        inet 10.0.0.10 netmask 0xffffff00 broadcast 10.0.0.255
~~~

## Nous allons maintenant mettre des IPs alias sur em0.

~~~
ifconfig em0 inet alias 192.168.4.242 netmask 255.255.255.0
ifconfig em0 inet alias 192.168.4.243 netmask 255.255.255.0
~~~

~~~
# ifconfig em0
em0: flags=28843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST,NOINET6> mtu 1500
        lladdr 52:54:00:03:56:0b
        priority: 0
        media: Ethernet autoselect (autoselect)
        inet 192.168.4.241 netmask 0xffffff00 broadcast 192.168.4.255
        inet 192.168.4.242 netmask 0xffffff00 broadcast 192.168.4.255
        inet 192.168.4.243 netmask 0xffffff00 broadcast 192.168.4.255
~~~

## Afin de permettre la communication entre différents rdomains (VRF leaking) on peut utiliser PF.

Ici l'idée est de pouvoir communiquer avec les deux machines dans les vlans 10 et 20 depuis le réseau 192.168.4.0/24. En utilisant des règles PF on va pouvoir utiliser l'alias IP 192.168.4.243 pour communiquer aves 10.0.0.11 de la rtable1 et l'alias IP 192.168.4.242 pour communiquer aves 10.0.0.11 de la rtable2.

~~~
# cat /etc/pf.conf
set skip on lo

block return    # block stateless traffic
pass            # establish keep-state

match in on em0 to 192.168.4.243 rdr-to 10.0.0.11 rtable 1
match in on em0 to 192.168.4.242 rdr-to 10.0.0.11 rtable 2

match out on em0 received-on vlan10 nat-to 192.168.4.243
match out on em0 received-on vlan20 nat-to 192.168.4.242

match in on vlan10 to !vlan10:network rtable 0
match in on vlan20 to !vlan20:network rtable 0
~~~
