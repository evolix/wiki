---
categories: openbsd network
title: FTP derrière un firewall OpenBSD
---

Le protocole FTP n'est pas très compatible avec les pare-feux à
états. Si on souhaite héberger un serveur ftp derrière un pare-feu
OpenBSD, il est nécessaire d'avoir un proxy ftp qui va autoriser les
bons ports dynamiquement.

Pour mettre en place ce proxy, il faut rajouter dans pf.conf les
règles suivantes :

~~~
anchor "ftp-proxy/*"
pass in quick proto tcp to any port ftp divert-to 127.0.0.1 port 8021
~~~

On active et on démarre le proxy

~~~
# rcctl enable ftpproxy
# rcctl start ftpproxy
~~~

Puis on fait entrer en vigueur les règles précédemment rajoutées dans
pf.conf

~~~
# pfctl -f /etc/pf.conf
~~~

Dans le cas où le firewall fait du NAT, il faut lancer ftpproxy avec des flags un peu différents :

~~~
# rcctl set ftpproxy flags -R 192.0.2.42 -p 21 -b 198.51.100.42
~~~

Avec 192.0.2.42 l'ip privée du nat et 198.51.100.42 l'ip publique du firewall. On mettra alors les règles pf suivantes :

~~~
anchor "ftp-proxy/*"
pass in on egress inet proto tcp to 198.51.100.42 port 21
~~~


Si besoin, on peut se référer à la man page
[ftp-proxy(8)](https://man.openbsd.org/ftp-proxy).

Si besoin de voir les règles présentes dans l'*anchor*

~~~
# pfctl -a 'ftp-proxy/*' -sr
~~~

## FAQ

### `client limit (100) reached`

Si vous trouvez le message d'erreur suivant dans `/var/log/messages`, vous pouvez augmenter le nombre maximum de connexions FTP simultanées avec l'option `-m`.

~~~
client limit (100) reached, refusing connection 192.0.2.42
~~~

Vous pouvez changer l'option `-m` comme ceci :

~~~
# rcctl set ftpproxy flags $(rcctl get ftpproxy flags) -m 200
# rcctl restart ftpproxy
~~~
