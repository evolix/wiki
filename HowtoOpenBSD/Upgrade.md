---
categories: system kernel openbsd sysadmin
title: Howto Upgrade OpenBSD
...

* OpenBSD <https://www.openbsd.org/>

# Mise à jour du système

## Avant de mettre à jour - Prérequis

Avant de lancer la mise à jour, il faut vérifier les choses suivantes :

### Cas d'un équipement redondant

**Si le serveur à mettre à jour est master d'un groupe [CARP(4)](https://man.openbsd.org/carp)**

1. On commence par s'assurer que le serveur est bien synchronisé avec son backup  

~~~
# /usr/share/scripts/sync.sh "Synchronisation pre-upgrade" 
~~~

2.  On passe le serveur en BACKUP  

~~~
# ifconfig -g carp carpdemote 50
~~~

> *Note* : Pour constater le changement d'état en direct on peut effectuer un tail -f /var/log/messages

3. On déplace les fichiers de configuration carp dans un lieu sûr  

~~~
# mv /etc/hostname.carp* /root/
~~~

4. On commente l'ensemble des lignes du fichier /etc/rc.conf.local pour éviter que les services ne démarrent avant que la redondance ne soit correctement reconfigurée.

~~~
# sed -i 's/^/#/' /etc/rc.conf.local
~~~

**Si le serveur à mettre à jour est backup d'un groupe [CARP(4)](https://man.openbsd.org/carp)**

1. On déplace les fichiers de configuration carp dans un lieu sûr  

~~~
# mv /etc/hostname.carp* /root/
~~~

2. On commente l'ensemble des lignes du fichier /etc/rc.conf.local pour éviter que les services ne démarrent avant que la redondance ne soit correctement reconfigurée.

~~~
# sed -i 's/^/#/' /etc/rc.conf.local
~~~

### Toutes versions

- La partition /usr doit avoir une taille totale de 1.1Go minimum dont au moins 500Mo disponibles.

### 6.6 vers 6.7

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade67.html#ConfigChanges) !

- Vérifier l'absence de l'utilisateur système « named »

~~~
# userdel named
# groupdel named
# rm -rf /var/named
~~~

### 6.7 vers 6.8

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade68.html#ConfigChanges) !

### 6.8 vers 6.9

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade69.html#ConfigChanges) !

- [pf.conf(5)](https://man.openbsd.org/pf.conf.5). La syntaxe des options de routage de PF (route-to, reply-to, dup-to) a changé. Si vous utilisez ces fonctionnalités et ne disposez pas d'accès à la console, pensez à adapter /etc/pf.conf avant la mise à jour ; la syntaxe précédente sera rejetée par [pfctl(8)](https://man.openbsd.org/pfctl.8).

### 6.9 vers 7.0

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade70.html#ConfigChanges) !

- [snmpd.conf(5)](https://man.openbsd.org/snmpd.conf.5). La configuration de SNMP par défaut a changée. Le protocole par défaut est maintenant SNMPv3, et les communautées public et private n'existent plus par défaut. Si vous souhaitez utiliser les protocoles SNMPv1 ou SNMPv2c, pensez à adapter /etc/snmpd.conf.

### 7.0 vers 7.1

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade71.html#ConfigChanges) !

### 7.1 vers 7.2

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade72.html#ConfigChanges) !

- [openssl(1)](https://man.openbsd.org/man1/openssl.1). LibreSSL est passé de la version 3.5.2 à la version 3.6.0. Certaines suites de chiffrement ne sont plus acceptées. Par exemple pour NRPE, selon la version utilisée sur le serveur, il peut être nécessaire de modifier l'option `ssl_cipher_list` dans la configuration client :

~~~
# vim /etc/nrpe.cfg

ssl_cipher_list=ALL:!MD5:@STRENGTH:@SECLEVEL=0
~~~

### 7.2 vers 7.3

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade73.html#ConfigChanges) !

- Nous avons constaté que le [pluging exec de collectd](/HowtoCollectd#plugin-exec) fait planter le firewall aléatoirement à partir de cette version. Nous ne savons pas si c'est valable avec tous types de scripts mais l'avons constaté avec plusieurs. Nous conseillons donc la désactivation de ce plugin.

### 7.3 vers 7.4

Prendre connaissance de la page détaillant [les changements de configuration](https://www.openbsd.org/faq/upgrade74.html#ConfigChanges) !

- [pfsync(4)](https://man.openbsd.org/pfsync.4). pfsync a été ré-écrit. Si les interfaces `/etc/hostname.pfsyncX` sont configurées de la façon suivante :

~~~
up
syncdev $interface
~~~

Alors il faut changer l'ordre pour les configurer avant de les activer :

~~~
syncdev $interface
up
~~~

### 7.4 vers 7.5

Aucun changement de configuration pour cette version.

### 7.5 vers 7.6

Aucun changement de configuration pour cette version.

## Mettre à jour le système

Lancer la mise à jour via [sysupgrade(8)](http://man.openbsd.org/sysupgrade)

~~~
# sysupgrade
~~~

> *Note* : Une fois les sets récupérés, le serveur va redémarrer sur le kernel bsd.rd et effectuer la mise à jour automatiquement.
 
Pendant la mise à jour automatique, le serveur sera pinguable mais non accessible en SSH, il ne faut donc pas entièrement se fier au ping pour la reprise en main une fois l'upgrade terminée.
 
***Une fois le serveur de nouveau disponible en SSH***

**Merger les configurations**

~~~
# sysmerge
~~~

**Mettre à jour les firmwares**

~~~
# fw_update
~~~

> *Note* : Cette action est déjà effectuée par [sysupgrade(8)](http://man.openbsd.org/sysupgrade) 

**Mettre à jour les paquets**

~~~
# pkg_add -u
~~~

**Appliquer les éventuels patchs de sécurité**

~~~
# syspatch
~~~

## Une fois la mise à jour effectuée

Une fois la mise à jour effectuée, il faut si besoin modifier certaines configurations et supprimer certains fichiers devenus potentiellement obsolètes.

### 6.6 vers 6.7

Fichiers à supprimer

~~~
# rm -rf /usr/libdata/perl5/*/Storable \
        /usr/libdata/perl5/*/arybase.pm \
        /usr/libdata/perl5/*/auto/arybase \
        /usr/libdata/perl5/B/Debug.pm \
        /usr/libdata/perl5/Locale/{Codes,Country,Currency,Language,Script}* \
        /usr/libdata/perl5/Math/BigInt/CalcEmu.pm \
        /usr/libdata/perl5/unicore/To/_PerlWB.pl \
        /usr/libdata/perl5/unicore/lib/GCB/EB.pl \
        /usr/libdata/perl5/unicore/lib/GCB/GAZ.pl \
        /usr/share/man/man3p/B::Debug.3p \
        /usr/share/man/man3p/Locale::{Codes*,Country,Currency,Language,Script}.3p \
        /usr/share/man/man3p/Math::BigInt::CalcEmu.3p \
        /usr/share/man/man3p/arybase.3p
# rm -f /usr/sbin/{dig,host,nslookup}
~~~

### 6.7 vers 6.8

Fichiers à supprimer

~~~
# rm -f /usr/lib/libperl.a
# rm /usr/X11R6/lib/libxkbui.* \
     /usr/X11R6/lib/pkgconfig/xkbui.pc \
     /usr/X11R6/include/X11/extensions/XKBui.h
~~~

### 6.8 vers 6.9

Fichiers à supprimer

~~~
# rm -f /usr/bin/podselect \
        /usr/lib/libperl.so.20.0 \
        /usr/libdata/perl5/*/CORE/dquote_inline.h \
        /usr/libdata/perl5/*/Tie \
        /usr/libdata/perl5/*/auto/Tie \
        /usr/libdata/perl5/Pod/Find.pm \
        /usr/libdata/perl5/Pod/InputObjects.pm \
        /usr/libdata/perl5/Pod/ParseUtils.pm \
        /usr/libdata/perl5/Pod/Parser.pm \
        /usr/libdata/perl5/Pod/PlainText.pm \
        /usr/libdata/perl5/Pod/Select.pm \
        /usr/libdata/perl5/pod/perlce.pod \
        /usr/libdata/perl5/unicore/Heavy.pl \
        /usr/libdata/perl5/unicore/lib/Lb/EB.pl \
        /usr/libdata/perl5/unicore/lib/Perl/_PerlNon.pl \
        /usr/libdata/perl5/unicore/lib/Sc/Armn.pl \
        /usr/libdata/perl5/utf8_heavy.pl \
        /usr/share/man/man1/podselect.1 \
        /usr/share/man/man3p/Pod::Find.3p \
        /usr/share/man/man3p/Pod::InputObjects.3p \
        /usr/share/man/man3p/Pod::ParseUtils.3p \
        /usr/share/man/man3p/Pod::Parser.3p \
        /usr/share/man/man3p/Pod::PlainText.3p \
        /usr/share/man/man3p/Pod::Select.3p
~~~

### 6.9 vers 7.0

Fichiers à supprimer

~~~
# rm -f /usr/X11R6/lib/libdmx.* \
        /usr/X11R6/include/X11/extensions/dmxext.h \
        /usr/X11R6/lib/pkgconfig/dmx.pc \
        /usr/X11R6/man/man3/DMX*.3
~~~

### 7.0 vers 7.1

Aucun fichier à supprimer pour cette version.

### 7.1 vers 7.2

Fichiers à supprimer

~~~
# userdel _switchd
# groupdel _switchd
# rm /etc/rc.d/switchd \
     /usr/sbin/switchctl \
     /usr/sbin/switchd \
     /usr/share/man/man4/switch.4 \
     /usr/share/man/man5/switchd.conf.5 \
     /usr/share/man/man8/switchctl.8 \
     /usr/share/man/man8/switchd.8
~~~

### 7.2 vers 7.3

Aucun fichier à supprimer pour cette version.

### 7.3 vers 7.4

Aucun fichier à supprimer pour cette version.

### 7.4 vers 7.5

Aucun fichier à supprimer pour cette version.

### 7.5 vers 7.6

Aucun fichier à supprimer pour cette version.

> *Note* : On peut également utiliser le paquet [sysclean](https://github.com/semarie/sysclean) pour supprimer d'éventuels autres fichiers devenus obsolètes.

### Cas d'un équipement redondant

**Si le serveur mis à jour était membre d'un groupe [CARP(4)](https://man.openbsd.org/carp)**

On replace les fichiers de configuration carp

~~~
# mv /root/hostname.carp* /etc/
~~~

On décommente les lignes commentées plus haut dans /etc/rc.conf.local
    
~~~
# sed -i 's/^#//' /etc/rc.conf.local
~~~

### Toutes versions

Une fois l'étape post-upgrade correspondant à la version mise à jour traitée, on peut effectuer le dernier reboot :

~~~
# reboot
~~~

## Liens

* OpenBSD <https://www.openbsd.org/>
* Version stable <https://www.openbsd.org/72.html>
* Guide upgrade <https://www.openbsd.org/faq/upgrade72.html>
