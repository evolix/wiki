---
categories: openbsd network firewall
title: Howto PFSYNC sous OpenBSD
---

* Documentation : <https://www.openbsd.org/faq/pf/carp.html#pfsyncintro>

[Pfsync](https://man.openbsd.org/pfsync.4) est un protocole permettant le transfert et la synchronisation de la table d'états utilisée par PacketFilter entre plusieurs firewalls. Son fonctionnement par défaut met en place une diffusion multicast des changements de table sur une interface donnée, permettant aux noeuds du même cluster de mettre à jour leur table en conséquence. Généralement utilisé de concert avec [CARP](/HowtoOpenBSD/CARP) cela permet un fail-over transparent entre plusieurs firewalls.

> **Attention, Pfsync ne dispose d'aucun mécanisme d'authentification, ce qui expose les noeuds de votre cluster au spoofing de packets, et à la création d'états falsifiés permettant à un tier d'outre-passer totalement le jeu de règles pf. Ce protocole est à mettre en place uniquement dans un réseau de confiance.**

## Configuration

La configuration de pfsync consiste simplement à créer une pseudo-interface puis à lier celle-ci avec l'interface physique de notre choix. Dans un souci de sécurité il est important de ne pas utiliser une interface existante afin d'isoler les trafics.

Dans un premier temps, il est possible de choisir entre une synchronisation par diffusion multicast ou une synchronisation peer-to-peer (p2p). Par défaut pfsync diffuse tous les paquets de synchronisation vers l'addresse multicast ```224.0.0.240```.

Note : la diffusion peer-to-peer permet la protection éventuelle du trafic pfsync via ipsec.

### Configuration à la volée

Afin de commencer à émettre les paquets de synchronisation sur une interface :

~~~
# ifconfig pfsync0 create
# ifconfig pfsync0 syncdev <interface>
~~~

Pour arrêter l'émission et supprimer l'interface :

~~~
# ifconfig pfsync0 -syncdev <interface>
# ifconfig pfsync0 destroy
~~~

Pour passer en mode peer-to-peer :

~~~
# ifconfig pfsync0 syncpeer <ip_address>
~~~

Pour retourner en mode multicast :

~~~
# ifconfig pfsync0 -syncpeer
~~~

### Configuration persistente

Pour le mode multicast, créer `/etc/hostname.pfsync0` :

~~~
syncdev <interface>
up
~~~

Pour une configuration peer-to-peer :

~~~
syncpeer <ip_address>
syncdev <interface>
up
~~~

Note : la ligne `up` doit obligatoirement être en dernier pour que la configuration fonctionne.

## Configuration PacketFilter

Afin d'autoriser le trafic pfsync, ajouter la règle dans `/etc/pf.conf` :

~~~
pass quick on pfsync0 proto pfsync
~~~

## Commandes

Afficher les statistiques du protocole pfsync :

~~~
# netstat -sp pfsync
pfsync:
        89586993 packets received (IPv4)
        0 packets received (IPv6)
                0 packets discarded for bad interface
                0 packets discarded for bad ttl
                0 packets shorter than header
                0 packets discarded for bad version
                0 packets discarded for bad HMAC
                0 packets discarded for bad action
                0 packets discarded for short packet
                1 state discarded for bad values
                116211 stale states
                36304837 failed state lookup/inserts
        5465049553 packets sent (IPv4)
        0 packets sent (IPv6)
                0 send failed due to mbuf memory error
                712946959 send error
~~~
