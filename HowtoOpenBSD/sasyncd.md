---
categories: openbsd network vpn ipsec isakmpd iked
title: Howto sasyncd
---

Documentation :

* <http://man.openbsd.org/man8/sasyncd.8>
* <http://man.openbsd.org/man5/sasyncd.conf.5>

SAsyncd est un démon permettant de synchroniser les SA (Security Association) d'IPsec entre 2 firewalls redondés via [CARP](/HowtoOpenBSD/CARP). Il est compatible avec les 2 démons [isakmpd](/HowtoISAKMPD) et [iked](/HowtoIKED)

## Configuration de sasyncd

SAsyncd se configure dans `/etc/sasyncd.conf` :

~~~
listen on 192.0.2.1

# IP addresses or hostnames of sasyncd(8) peers.
peer 192.0.2.2

# Track master/slave state on this carp(4) interface.
interface carp0
group carp

control isakmpd

# Shared AES key, 16/24/32 bytes.
sharedkey 0xXXXX
~~~

Avec les paramètres suivants :

* `listen on` : IP sur laquelle sasyncd doit écouter ; le port TCP peut aussi être précisé en ajoutant `port` (exemple : "listen on X.X.X.X port Y") (défaut : 500)
* `peer` : définit le paire sasyncd ; peut être défini plusieurs fois
* `interface` : interface CARP sur laquelle sasyncd doit suivre l'état master/backup
* `group` : groupe CARP sur lequel sasyncd doit incrémenter le compteur *carpdemote* lorsqu'il détecte un changement d'état de l'interface définit précédemment (défaut : carp)
* `control` : démon que sasyncd doit contrôler, parmi `isakmpd`, `iked`, `all`, ou `none` (défaut : isakmpd).
* `sharedkey` : clé AES partagée pour chiffrer les échanges entre les peer ; le paramètre est obligatoire et doit avoir une longueur de 16, 24 ou 32 octets.

Pour les paramètres `listen on` et `peer`, il est conseillé d'utiliser le même réseau que celui utilisé pour `pfsync`.

La clé partagée peut être générée avec la commande `openssl rand -hex 32`. Ne pas oublier d'indiquer "0x" au début.

Vu que le fichier de configuration contient un secret partagé, il ne doit être lisible que par l'utilisateur `root` (le démarrage échouera sinon) :

~~~
# chown root /etc/sasyncd.conf
# chmod 0600 /etc/sasyncd.conf
~~~

Il faut autoriser le port utilisé, dans `/etc/pf.conf` :

~~~
pass quick on $pfsync_if proto tcp from $peer to port 500
~~~

Puis on peut activer et démarrer sasyncd :

~~~
# rcctl enable sasyncd
# rcctl start sasyncd
~~~

**Note** : en plus du temps de bascule de l'interface CARP, il faut prévoir une 10aine de secondes de temps de bascule de sasyncd, pouvant monter jusqu'à une 60aine de secondes.

## Configuration IPsec

Le démon IPsec utilisé, isakmpd ou iked, doit être démarré avec le paramètre `-S` :

~~~
# rcctl set isakmpd flags -S
# rcctl set iked flags -S
~~~

Penser à redémarrer le démon concerné après la modification. Un reboot complet du serveur est souvent nécessaire pour que tout fonctionne correctement.

Pour isakmpd :

~~~
     -S      This option is used for setups using sasyncd(8) and carp(4) to
             provide redundancy.  isakmpd starts in passive mode and will not
             initiate any connections or process any incoming traffic until
             sasyncd has determined that the host is the carp master.
             Additionally, isakmpd will not delete SAs on shutdown by sending
             delete messages to all peers.
~~~

Pour iked :

~~~
     -S      Start iked in passive mode.  See the set passive option in
             iked.conf(5) for more information.

     set passive
           Set iked(8) to global passive mode.  In passive mode no packets are
           sent to peers and no connections are initiated by iked(8), even for
           active policies.  This option is used for setups using sasyncd(8)
           and carp(4) to provide redundancy.  iked(8) will run in passive
           mode until sasyncd has determined that the host is the master and
           can switch to active mode.
~~~

Ainsi, sasyncd définit si l'hôte est master ou backup d'après l'état de l'interface CARP, puis passe le démon défini en mode actif si l'interface est master. L'hôte master suit tous les changements locaux des SA et les synchronise à tous les paires sasyncd passifs.

Deux autres paramètres pour isakmpd ou iked ont également leur importance : `local` et `srcid`.

Le paramètre `local` indique quelle IP locale sera utilisée pour établir le tunnel ; il faut donc utiliser l'IP WAN CARP partagée entre les 2 hôtes sasyncd. Le paramètre `srcid` est un identifiant utilisé lors de l'établissement du tunnel et envoyé au pair IPsec ; par défaut, il correspond au FQDN de l'hôte et sera donc différent entre les 2 hôtes sasyncd. Il faut donc lui aussi le configurer pour utiliser l'IP WAN CARP partagée.

Pour isakmpd, via ipsec.conf :

~~~
[…]
local_ip_carp="198.51.100.254"

ike esp from $local_network to $remote_network local $local_ip_carp peer $remote_ip \
    main […] \
    quick […] \
    srcid $local_ip_carp \
    psk […]
~~~

Pour iked, via iked.conf :

~~~
[…]
local_ip_carp="198.51.100.254"

ikev2 active esp \
    from $local_network to $remote_network \
    local $local_ip_carp peer $remote_ip \
    ikesa […] \
    childsa […] \
    srcid $local_ip_carp \
    ikelifetime XXX lifetime XXX \
    psk […]
~~~