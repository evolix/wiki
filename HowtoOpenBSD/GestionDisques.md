---
categories: openbsd network
title: Howto Gestion des disques sous OpenBSD
---

La gestion des disques sous OpenBSD est parfois déroutante, surtout si
l'on vient du monde GNU/Linux. Pour une introduction, lire la [FAQ](https://www.openbsd.org/faq/faq14.html#intro)

## Ajout d'un disque

### Disque formaté pour OpenBSD (FFS)

Si l'on ajoute un nouveau disque sur un système OpenBSD, voici les différentes étapes pour l'initialiser (outre l'installation matérielle ;-).

Prenons l'exemple avec disque `sd1` ajouté. Il faut d'abord initialiser sa table des partitions :

~~~
# fdisk -i sd1
Do you wish to write new MBR and partition table? [n] y
Writing MBR at offset 0.
# fdisk sd1
Disk: sd1       geometry: 60734/255/63 [975699968 Sectors]
Offset: 0       Signature: 0xAA55
            Starting         Ending         LBA Info:
 #: id      C   H   S -      C   H   S [       start:        size ]
-------------------------------------------------------------------------------
 0: 00      0   0   0 -      0   0   0 [           0:           0 ] unused
 1: 00      0   0   0 -      0   0   0 [           0:           0 ] unused
 2: 00      0   0   0 -      0   0   0 [           0:           0 ] unused
*3: A6      0   1   1 -  60733 254  63 [          63:   975691647 ] OpenBSD
~~~

On gère ensuite la partie *disklabel* :

~~~
# disklabel -E sd1
# Inside MBR partition 3: type A6 start 63 size 975691647
Treating sectors 63-975691710 as the OpenBSD portion of the disk.
You can use the 'b' command to change this.

Initial label editor (enter '?' for help at any prompt)
> a
partition: [a]
offset: [63]
size: [975691647]
FS type: [4.2BSD]

> p
OpenBSD area: 63-975691710; size: 975691647; free: 0
#                size           offset  fstype [fsize bsize  cpg]
  a:        975691647               63  4.2BSD   2048 16384    1
  c:        975699968                0  unused

> q
Write new label?: [y]
~~~

On peut enfin créer le système de fichiers et le monter :

~~~
# newfs sd1a
newfs: reduced number of fragments per cylinder group from 103664 to 103656 to enlarge last cylinder group
/dev/rsd1a: 476411.9MB in 975691644 sectors of 512 bytes
2354 cylinder groups of 202.45MB, 12957 blocks, 25984 inodes each
super-block backups (for fsck -b #) at:
 32, 414656, 829280, 1243904, ......
# mount /dev/sd1a /mnt/
~~~

### Disque en FAT32

Afin qu'il soit lisible par d'autres systèmes (par exemple un disque USB pour des sauvegardes), voici les différentes étapes pour
initialiser un disques en FAT32. Prenons l'exemple avec disque `sd1` ajouté. Il faut d'abord initialiser sa table des partitions :

~~~
# fdisk -i sd1

        -----------------------------------------------------
        ------ ATTENTION - UPDATING MASTER BOOT RECORD ------
        -----------------------------------------------------

Do you wish to write new MBR and partition table? [n] y
~~~

On indique ensuite que la partition est de type FAT32 :

~~~
# fdisk -e sd1
Enter 'help' for information
fdisk: 1> p
Disk: sd1       geometry: 60801/255/63 [976773168 Sectors]
Offset: 0       Signature: 0xAA55
            Starting         Ending         LBA Info:
 #: id      C   H   S -      C   H   S [       start:        size ]
-------------------------------------------------------------------------------
 0: 00      0   0   0 -      0   0   0 [           0:           0 ] unused      
 1: 00      0   0   0 -      0   0   0 [           0:           0 ] unused      
 2: 00      0   0   0 -      0   0   0 [           0:           0 ] unused      
*3: A6      0   1   1 -  60800 254  63 [          63:   976768002 ] OpenBSD     
fdisk: 1> e 3
            Starting         Ending         LBA Info:
 #: id      C   H   S -      C   H   S [       start:        size ]
-------------------------------------------------------------------------------
*3: A6      0   1   1 -  60800 254  63 [          63:   976768002 ] OpenBSD
Partition id ('0' to disable)  [0 - FF]: [A6] (? for help) ?
Choose from the following Partition id values:
00 unused         20 Willowsoft     67 Novell         AB MacOS X boot
01 DOS FAT-12     24 NEC DOS        68 Novell         AF MacOS X HFS+
02 XENIX /        38 Theos          69 Novell         B7 BSDI filesy*
03 XENIX /usr     39 Plan 9         70 DiskSecure     B8 BSDI swap   
04 DOS FAT-16     40 VENIX 286      75 PCIX           BF Solaris     
05 Extended DOS   41 Lin/Minux DR   80 Minix (old)    C0 CTOS        
06 DOS > 32MB     42 LinuxSwap DR   81 Minix (new)    C1 DRDOSs FAT12
07 HPFS/QNX/AUX   43 Linux DR       82 Linux swap     C4 DRDOSs < 32M
08 AIX fs         4D QNX 4.2 Pri    83 Linux files*   C6 DRDOSs >=32M
09 AIX/Coherent   4E QNX 4.2 Sec    84 OS/2 hidden    C7 HPFS Disbled
0A OS/2 Bootmgr   4F QNX 4.2 Ter    85 Linux ext.     DB CPM/C.DOS/C*
0B Win95 FAT-32   50 DM             86 NT FAT VS      DE Dell Maint  
0C Win95 FAT32L   51 DM             87 NTFS VS        E1 SpeedStor   
0E DOS FAT-16     52 CP/M or SysV   8E Linux LVM      E3 SpeedStor   
0F Extended LBA   53 DM             93 Amoeba FS      E4 SpeedStor   
10 OPUS           54 Ontrack        94 Amoeba BBT     EB BeOS/i386   
11 OS/2 hidden    55 EZ-Drive       99 Mylex          EE EFI GPT     
12 Compaq Diag.   56 Golden Bow     9F BSDI           EF EFI Sys     
14 OS/2 hidden    5C Priam          A0 NotebookSave   F1 SpeedStor   
16 OS/2 hidden    61 SpeedStor      A5 FreeBSD        F2 DOS 3.3+ Sec
17 OS/2 hidden    63 ISC, HURD, *   A6 OpenBSD        F4 SpeedStor   
18 AST swap       64 NetWare 2.xx   A7 NEXTSTEP       FF Xenix BBT   
19 Willowtech     65 NetWare 3.xx   A8 MacOS X     
1C ThinkPad Rec   66 NetWare 386    A9 NetBSD      
Partition id ('0' to disable)  [0 - FF]: [A6] (? for help) 0B
Do you wish to edit in CHS mode? [n]
offset: [63]
size: [976768002]
fdisk:*1> w
Writing MBR at offset 0.
fdisk: 1> q
~~~

La partition est ainsi accessible en `sd1i` comme l'indique disklabel :

~~~
# disklabel sd1
disklabel: warning, DOS partition table with no valid OpenBSD partition
# /dev/rsd1c:
type: SCSI
disk: SCSI disk
label: HM500JI
flags:
bytes/sector: 512
sectors/track: 63
tracks/cylinder: 255
sectors/cylinder: 16065
cylinders: 60801
total sectors: 976773168
rpm: 3600
interleave: 1
trackskew: 0
cylinderskew: 0
headswitch: 0           # microseconds
track-to-track seek: 0  # microseconds
drivedata: 0

16 partitions:
#                size           offset  fstype [fsize bsize  cpg]
  c:        976773168                0  unused      0     0
  i:        976768002               63   MSDOS
~~~

Enfin, on la formate ensuite en FAT32 :

~~~
# newfs_msdos sd1i
/dev/rsd1i: 974863936 sectors in 121857992 FAT32 clusters (4096 bytes/cluster)
bps=512 spc=8 res=32 nft=2 mid=0xf8 spt=63 hds=255 hid=63 bsec=976768002 bspf=952016 rdcl=2 infs=1 bkbs=2
~~~

## Monter une partition FFS sous Linux

~~~
# mount -t ufs -o ufstype=44bsd -o ro /dev/sdb4 usb/
~~~

Pour le support en RW, c'est encore expérimental d'après le message :

~~~
kernel: [1816288.453987] ufs was compiled with read-only support, can't be mounted as read-write
~~~

## Créer un swap sur un fichier image

Exemple avec une swap de 1Go.

~~~
# dd if=/dev/zero of=/var/swap bs=1k count=1048576
# chmod 600 /var/swap
# swapctl -a /var/swap
## Verifier
# swapctl -l
# echo "/var/swap /var/swap swap sw 0 0" >> /etc/fstab
~~~

## Agrandir une partition (exemple avec /home)

On peut agrandir une partition si elle est placée en dernier dans le schéma de partitionnement et s'il y a de la place libre non partitionnée sur le disque.

Pour afficher les partitions sur un disque on utilise `disklabel`, qui affiche la taille en Gio :

~~~
# disklabel -pg sd0

# /dev/rsd0c:
type: SCSI
disk: SCSI disk
label: PERC H330 Adp   
duid: efdfad9f52e8f066
flags:
bytes/sector: 512
sectors/track: 4
tracks/cylinder: 23
sectors/cylinder: 92
cylinders: 21222266
total sectors: 1952448512 # total bytes: 931.0G
boundstart: 4
boundend: 1952448472
drivedata: 0 

16 partitions:
#                size           offset  fstype [fsize bsize   cpg]
  a:             1.0G                4  4.2BSD   2048 16384 12958 # /
  b:             2.0G          2097216    swap                    # none
  c:           931.0G                0  unused                    
  d:            10.0G          6291616  4.2BSD   2048 16384 12958 # /var
  e:             5.0G         27263168  4.2BSD   2048 16384 12958 # /usr
  f:             2.0G         37748960  4.2BSD   2048 16384 12958 # /tmp
  g:            10.0G         41943328  4.2BSD   2048 16384 12958 # /home
~~~

Sous OpenBSD la partition `c:` représente le disque complet, si on soustrait les tailles des autres partitions, on voit qu'il reste 901Gio de libre sur ce disque.

On veut augmenter la taille de la partition `/home` qui est placée à la fin du disque.

* On s'assure que la partition `/home` n'est pas utilisée (se connecter directement en tant que root temporairement), puis on la démonte :

~~~
# fstat -f /home
USER     CMD          PID   FD MOUNT        INUM  MODE         R/W    SZ|DV

# umount /home
~~~

* On entre en mode édition de la partition de la manière suivante :

~~~
# disklabel -E sd0
~~~

* On affiche les partitions avec la commande `p`

~~~
Label editor (enter '?' for help at any prompt)
sd0> p
OpenBSD area: 4-1952448472; size: 1952448468; free: 1889533572
#                size           offset  fstype [fsize bsize   cpg]
  a:          2097212                4  4.2BSD   2048 16384 12958 # /
  b:          4194388          2097216    swap                    # none
  c:       1952448512                0  unused                    
  d:         20971552          6291616  4.2BSD   2048 16384 12958 # /var
  e:         10485792         27263168  4.2BSD   2048 16384 12958 # /usr
  f:          4194368         37748960  4.2BSD   2048 16384 12958 # /tmp
  g:         20971584         41943328  4.2BSD   2048 16384 12958 # /home
~~~

* On change la taille de la partition avec l'option `c` sur la partition `g` (/home), et on indique la taille finale souhaitée :

~~~
sd0> c g
Partition g is currently 20971584 sectors in size, and can have a maximum
size of 1910505144 sectors.
size: [20971584] 100G
~~~

* On écrit les changements sur le disque et on quitte : 

~~~
sd0*> w
sd0> q
No label changes.
~~~

* Ensuite on étend la partition `g` du disque `sd0` que l'on vient d'augmenter :

~~~
# growfs sd0g

We strongly recommend you to make a backup before growing the Filesystem

 Did you backup your data (Yes/No) ? Yes
new filesystem size is: 52428805 frags
Warning: 313940 sector(s) cannot be allocated.
growfs: 102246.7MB (209401280 sectors) block size 16384, fragment size 2048
	using 505 cylinder groups of 202.47MB, 12958 blks, 25984 inodes.
	with soft updates
super-block backups (for fsck -b #) at:
 21147488, 21562144, 21976800, 22391456, 22806112, 23220768, 23635424, 24050080, 24464736, 24879392, 25294048,
....
~~~

* On fait un fsck sur la partition pour s'assurer qu'elle soit propre et sans erreur :

~~~
# fsck /dev/sd0g
** /dev/rsd0g
** Last Mounted on /home
** Phase 1 - Check Blocks and Sizes
** Phase 2 - Check Pathnames
** Phase 3 - Check Connectivity
** Phase 4 - Check Reference Counts
** Phase 5 - Check Cyl groups
376 files, 748 used, 51521360 free (192 frags, 6440146 blocks, 0.0% fragmentation)

MARK FILE SYSTEM CLEAN? [Fyn?] y

***** FILE SYSTEM WAS MODIFIED *****
~~~

* On remonte la partition `/home` et on vérifie que l'augmentation de la taille a bien été prise en compte  :

~~~
# mount /home

# df -h
Filesystem     Size    Used   Avail Capacity  Mounted on
/dev/sd0a     1005M    117M    838M    12%    /
/dev/sd0f      2.0G   24.0K    1.9G     0%    /tmp
/dev/sd0e      4.9G    1.8G    2.8G    39%    /usr
/dev/sd0d      9.8G    7.7G    1.6G    82%    /var
/dev/sd0g     98.3G    1.5M   93.4G     0%    /home
~~~