---
categories: openbsd raid encryption
title: HowTo Encryption sur OpenBSD
---

* Documentation : <http://man.openbsd.org/bioctl.8>

Sur OpenBSD, l’encryption se fait avec le RAID logiciel et utilise donc [bioctl(8)](http://man.openbsd.org/bioctl.8).

## Préparation du disque

Écrire des données aléatoires au disque (Cela prend du temps)

~~~
# dd if=/dev/urandom of=/dev/rsd0c bs=1m
~~~

Initialiser le disk avec [fdisk(8)](http://man.openbsd.org/fdisk.8)
et créer une partition RAID avec [disklabel(8)](http://man.openbsd.org/disklabel.8)

~~~
# fdisk -iy -g -b 960 sd0
# disklabel -E sd0
Label editor (enter '?' for help at any prompt)
> a a
offset: [64]
size: [39825135] *
FS type: [4.2BSD] RAID
> w
> q
No label changes.
~~~

## Encryption du disque

Construire le disque encrypté sur la partition “a”.

~~~
# bioctl -c C -l sd0a softraid0
New passphrase:
Re-type passphrase:
sd1 at scsibus2 targ 1 lun 0: <OPENBSD, SR CRYPTO, 005> SCSI2 0/direct fixed
sd1: 19445MB, 512 bytes/sector, 39824607 sectors
softraid0: CRYPTO volume attached as sd1
~~~

Seulement sd0 est créé par défaut

~~~
# cd /dev && sh MAKEDEV sd1
~~~

Le premier mégaoctet est utilisé par bioctl pour le RAID, donc si
notre disque a été utilisé avec softraid précédemment, on utilise
[dd(1)](http://man.openbsd.org/dd.1) pour l’effacer

~~~
# dd if=/dev/zero of=/dev/rsd1c bs=1m count=1
~~~

Suivez l’installation standard en utilisant votre nouveau volume encryptée (dans notre cas sd1)