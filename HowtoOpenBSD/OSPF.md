---
categories: openbsd network
title: Howto OSPF sous OpenBSD
---

## Documentation

* <http://man.openbsd.org/ospfd.8>
* <http://man.openbsd.org/ospfd.conf.5>
* <http://man.openbsd.org/ospf6d.8>
* <http://man.openbsd.org/ospf6d.conf.5>

OSPF est un protocole de routage interne à un AS (IGP, Interior Gateway Protocol) grâce auquel chaque routeur annonce ses réseaux locaux aux autres routeurs, leur permettant à tous d'obtenir une vue complète de la topologie du réseau.

### Liens utiles

* <http://www.openbsd.org/papers/linuxtag06-network.pdf>

## Configuration

OSPFD est installé par défaut sous OpenBSD. En IPv4, il se configure dans `/etc/ospfd.conf`. En IPv6, on passera par `/etc/ospf6d.conf`.

Pour activer OSPFD, on modifiera `/etc/rc.conf.local` :

~~~
# rcctl enable ospfd ospf6d
~~~

### Exemple de configuration

Prenons 3 routeurs, placés tous les trois dans la zone 0.0.0.0, dite de backbone.
Ces 3 routeurs ont un lien entre eux via l'interface em0, et 2 d'entre eux ont une interface externe CARP partagée pour le réseau 192.0.0.0/24.

~~~
        130.0.0.1/24
        +---em1---+
        |         |
        |         |
        |         |
        +---em0---+
             .1

        10.0.0.0/24

     .2              .3
+---em0---+     +---em0---+
|         |     |         |
|         |     |         |
| BACKUP  |     | MASTER  |
+---------+     +---------+
     |               |
     +-----carp0-----+
        192.0.0.1/24
~~~

Voici le `ospfd.conf` du routeur ayant l'adresse IP 10.0.0.1 :

~~~
# global configuration
router-id 10.0.0.1
redistribute connected
fib-update yes
rfc1583compat no
spf-delay msec 1000
spf-holdtime msec 5000

# area
area 0.0.0.0 {
        auth-type crypt
        auth-md 1 "PASS"
        auth-md-keyid 1

        interface em0 {
                metric 10
                retransmit-interval 5
                router-dead-time 40
                hello-interval 10
                router-priority 1
                transmit-delay 1
        }
}
~~~

Voici le `ospfd.conf` du routeur ayant l'adresse IP 10.0.0.2 :

~~~
# global configuration
router-id 10.0.0.2
fib-update yes
rfc1583compat no
spf-delay msec 1000
spf-holdtime msec 5000

# area
area 0.0.0.0 {
        auth-type crypt
        auth-md 1 "PASS"
        auth-md-keyid 1

        interface em0 {
                metric 10
                retransmit-interval 5
                router-dead-time 40
                hello-interval 10
                router-priority 1
                transmit-delay 1
        }

        interface carp0 {
                metric 10
                passive
        }
}
~~~

Et voici le `ospfd.conf` du routeur ayant l'adresse IP 10.0.0.3 :

~~~
# global configuration
router-id 10.0.0.3
fib-update yes
rfc1583compat no
spf-delay msec 1000
spf-holdtime msec 5000

# area
area 0.0.0.0 {
        auth-type crypt
        auth-md 1 "PASS"
        auth-md-keyid 1

        interface em0 {
                metric 10
                retransmit-interval 5
                router-dead-time 40
                hello-interval 10
                router-priority 1
                transmit-delay 1
        }

        interface carp0 {
                metric 10
                passive
        }
}
~~~

**Attention** : les fichiers de configurations doivent être en `chmod 600` pour que le démon puisse démarrer.

L'authentification n'est possible que sous IPv4, on passera par IPsec pour IPv6.

Ici, le 3ème routeur sera le DR (router-id le plus élevé) et le 2ème sera le BDR. Cela signifie que chaque routeur enverra ses routes aux DR et BDR, avant que le DR ne les redistribue à tout le monde.
Si on veut garder les adresses IP des machines comme router-id tout en ne désignant pas celui qui a le router-id le plus élevé comme DR, on peut changer l'option router-priority.

Les interfaces em0 sont donc celles sur lesquels les paquets OSPF sont envoyés afin d'établir des adjacences.
Toutes les interfaces externes sont annoncées en tant que passives pour qu'aucun paquet ne soit envoyé en dehors de notre réseau. Les interfaces carp n'ont pas besoin d'être explicitées comme passives car OSPFD les verra toujours comme telles.

Si l'on veut simplement propager tous les réseaux directement connectés au routeur, il suffit de spécifier `redistribute connected`. Avec cette configuration, le routeur 10.0.0.1 propage le réseau de son interface em1 sans avoir à la précisier. Attention toutefois, ce paramètre ne fonctionne pas avant OpenBSD 6.1.

Du côté CARP, dans le cas où le routeur 10.0.0.3 est master et 10.0.0.2 est backup, alors OSPFD verra l'état CARP et seul le master enverra le réseau 192.0.0.0/24 aux autres routeurs.

Les paramètres globaux `fib-update`, `rfc1583compat`, `spf-delay`, `spf-holdtime` et les paramètres d'interfaces `metric`, `retransmit-interval`, `router-dead-time`, `hello-interval`, `router-priority` et `transmit-delay` sont laissés à leur valeur par défaut et n'ont donc pas obligatoirement besoin d'être donnés.

### OSPF pour IPv6

La configuration est la même entre IPv4 et IPv6 (en dehors de l'authentification qui n'est possible qu'en IPv4 comme indiqué plus haut). Les seules différences viennent du daemon, des commandes, et du fichier de configuration qui contiennent tous `6` en plus :

* Daemon : `ospfd` en IPv4, `ospf6d` en IPv6
* Contrôler le daemon : `ospfctl` en IPv4, `ospf6ctl` en IPv6
* Fichier de configuration : `ospfd.conf` en IPv4, `ospf6d.conf` en IPv6

#### Bug rencontré en IPv6

Un bug a été rencontré dans le fonctionnement des interfaces CARP avec ospf6d. Lors de l'annonce d'une même interface CARP sur 2 peers (l'un master, l'autre backup), une métrique de 65535 est annoncée aux voisins par le peer avec l'interface backup, contre une métrique beaucoup plus faible (par exemple 10) par celui avec l'interface master. Ainsi, seule la route avec la métrique la plus faible est gardée dans la table de routage du voisin.

Cependant, nous avons remarqué qu'en IPv6, les 2 peers annoncent leur route en tant que status backup avec une métrique de 65535, même si le status du peer est master.

Cela se traduit par les voisins gardant la route des 2 peers :

~~~
# ospf6ctl show rib detail 

                   Network Routing Table (Area 0.0.0.0)

Destination        Nexthop         Adv Router      Path type    Cost           
2001:db8:1::/48    fe80::14%em0    192.0.2.14      Intra-Area   65545  
2001:db8:1::/48    fe80::15%em0    192.0.2.15      Intra-Area   65545  
~~~

Pour contourner le problème, il suffit d'ajouter le paramètre `depend on carpX` pour chaque interface CARP (qui n'est pas nécessaire en IPv4, et n'est pas censé être nécessaire puisque OSPF est censé savoir que c'est une interface CARP) :

~~~
# global configuration
router-id 192.0.2.14

# area
area 0.0.0.0 {

        interface ix0

        interface carp0 {
            passive
            depend on carp0
        }

        interface carp1 {
            passive
            depend on carp1
        }
}
~~~

Le fonctionnement normal est alors rétabli : le backup annonce son interface avec la métrique maximale de 65535, le master l'annonce avec une métrique normale de 10, et les voisins ne gardent que la route vers le master.

### Utilisation

Voir un résumé des infos d'OSPFD :

~~~
# ospfctl show
Router ID: 10.0.0.1
Uptime: 00:01:10
RFC1583 compatibility flag is disabled
SPF delay is 1000 msec(s), hold time between two SPFs is 5000 msec(s)
Number of external LSA(s) 0 (Checksum sum 0x0)
Number of areas attached to this router: 1

Area ID: 0.0.0.0
  Number of interfaces in this area: 2
  Number of fully adjacent neighbors in this area: 2
  SPF algorithm executed 4 time(s)
  Number LSA(s) 4 (Checksum sum 0x1ea2c)
~~~

Voir un résumé des infos sur les voisins :

~~~
# ospfctl show neighbor
ID          Pri State        DeadTime Address     Iface     Uptime
10.0.0.2    1   FULL/BCKUP   00:00:31 10.0.0.2    em0       00:00:41
10.0.0.3    1   FULL/DR      00:00:31 10.0.0.3    em0       00:00:43
~~~

Voir la FIB (Forwarding Information Base) complète (attention : affiche toutes les routes apprises, même par BGP) :

~~~
# ospfctl show fib
flags: * # valid, O OSPF, C # Connected, S Static
Flags  Prio Destination          Nexthop          
*C        4 10.0.0.0/26          link#6
*O       32 192.0.0.0/24         10.0.0.3
*C        4 130.0.0.0/24         link#5
*C        0 127.0.0.0/8          link#0
*S        8 127.0.0.0/8          127.0.0.1
*         1 127.0.0.1/32         127.0.0.1
*S        8 224.0.0.0/4          127.0.0.1
~~~

En particulier la ligne *\*O       32 192.0.0.0/24         10.0.0.3* indiquant que l'on passe par le master pour joindre le réseau 192.0.0.0/24 obtenu par OSPF.

Voir seulement les routes OSPF de la FIB :

~~~
# ospfctl show fib ospf
flags: * # valid, O OSPF, C # Connected, S Static
Flags  Prio Destination          Nexthop       
*O       32 192.0.0.0/24         10.0.0.3
~~~

Voir les routes OSPF, statiques et directement connectées :

~~~
# ospfctl show fib ospf static connected
flags: * # valid, O OSPF, C # Connected, S Static
Flags  Prio Destination          Nexthop       
*C        4 10.0.0.0/26          link#6
*O       32 192.0.0.0/24         10.0.0.3
*C        4 130.0.0.0/24         link#5
*C        0 127.0.0.0/8          link#0
*S        8 127.0.0.0/8          127.0.0.1
*S        8 224.0.0.0/4          127.0.0.1
~~~

Il est également possible d'utiliser le mot clef `interface` ou de spécifier directement une adresse de destination.

Voir l'état des interfaces :

Sur 10.0.0.1, sans carp :

~~~
# ospfctl show interfaces
Interface   Address            State  HelloTimer Linkstate  Uptime    nc  ac
em1         130.0.0.1/24       DOWN   -          active     00:00:00   0   0
em0         10.0.0.1/24        OTHER  00:00:08   active     00:18:06   2   2
~~~

Sur 10.0.0.2, avec un carp backup :

~~~
# ospfctl show interfaces
Interface   Address            State  HelloTimer Linkstate  Uptime    nc  ac
carp0       192.0.0.1/24       DOWN   -          backup     00:00:00   0   0
em0         10.0.0.2/24        BCKUP  00:00:08   active     00:18:06   2   2
~~~

Sur 10.0.0.3, avec un carp master :

~~~
# ospfctl show interfaces
Interface   Address            State  HelloTimer Linkstate  Uptime    nc  ac
carp0       192.0.0.1/24       DOWN   -          master     00:00:00   0   0
em0         10.0.0.3/24        DR     00:00:08   active     00:18:06   2   2
~~~

En remplaçant ospfctl par ospf6ctl pour vérifier les paramètres IPv6.

Pour que les adjacences se fassent, penser à ajouter la règle dans pf.conf afin que le trafic des multicast soit autorisé.

En IPv4, ils communiquent de leurs adresses ip em0 vers les multicast
224.0.0.5 et 224.0.0.6 :

~~~
pass quick on em0 proto ospf from 10.0.0.0/24 to {224.0.0.5 224.0.0.6}
~~~

En IPv6, ils communiquent de leurs adresses ip link-local em0 vers les
multicast ff02::5 et ff02::6 :

~~~
pass quick on em0 proto ospf from fe80::/64 to {ff02::5 ff02::6}
~~~

## FAQ

### Après modificiation d'une configuration, la nouvelle route n'est pas prise en compte

Il se peut que la RIB (Routing Information Base, table de routage du logiciel, d'OSPFD en l'occurence) et la FIB (Forwarding Information Base, table de routage du kernel du routeur) ne soient pas synchronisés.
La RIB contient les meilleures routes apprises par OSPF uniquement, tandis que la FIB est la table de routage utilisée par le routeur pour transférer un paquet. Après une modification de la configuration d'OSPF, si la nouvelle route n'est pas prise en compte par la FIB, il faut forcer la synchronisation.

La bonne route est vue par OSPF :

~~~
# ospfctl show rib
Destination          Nexthop           Path Type    Type      Cost    Uptime  
192.0.2.0/24         198.51.100.1      Intra-Area   Network   20      3d00h38m
~~~

La mauvaise route est vue dans la FIB :

~~~
# route -n get 192.0.2.1
     gateway: 198.51.100.15
~~~

On resynchronise :

~~~
# ospfctl fib reload
~~~

Puis on refait les vérifications.