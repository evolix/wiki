--- 
title: HowtoOpenBSD/httpd
categories: 
...

* Manpage httpd <https://man.openbsd.org/httpd.8>
* Manpage httpd.conf <https://man.openbsd.org/httpd.conf.5>

_httpd(8)_ est le démon http présent dans le système de base sous _OpenBSD_. Léger et intuitif, il sera suffisant pour les cas les plus simples.

## Utilisation de base

Vérifier la configuration :

~~~
# httpd -n
~~~
> *Note* : on peut spécifier un fichier de configuration alternatif en utilisant l'argument "-f"

Lancer en mode debug :

~~~
# httpd -dvvv
~~~

Lancer le démon avec rcctl(8): 

~~~
# rcctl enable httpd
# rcctl start httpd
~~~

Stopper le démon :

~~~
# rcctl stop httpd
~~~

Pour définir des arguments au démarrage du démon, on peut les
rajouter a rc.conf(8).

## Configuration simple

Exemple d'une configuration minimale :

~~~
server "default" {
    listen on * port 80
    root "/htdocs"
}
~~~

**Note** : le serveur chroot(8) dans `/var/www` par défaut. Ce paramètre peut être écrasé, par exemple par `/home/www` :

~~~
chroot "/home/www"

server "default" {
[…]
}
~~~

## Configuration avancée

Voici une configuration pour servir un site Wordpress en HTTP/HTTPS :

~~~
types { include "/usr/share/misc/mime.types" }

server "example.com" {
        listen on * port 80
	alias "www.example.com"
        block return 302 "https://$SERVER_NAME$REQUEST_URI"
}

server "example.com" {
        listen on * tls port 443
	alias "www.example.com"
        root "/example/www/"
        directory index index.php

	location "/.*" { block } 
	location "/upload/*.php" { block } 
	location "/files/*.php" { block } 

        location "/*.php*" {
                fastcgi socket "/run/php-fpm-7.2.sock"
        }

	tls {
                certificate "/etc/ssl/example.crt"
                key "/etc/ssl/private/example.key"
        }

	log access "example.com.log"
        log style combined
}

~~~
