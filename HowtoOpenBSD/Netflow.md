---
categories: openbsd network
title: Howto Netflow sous OpenBSD
---

* Documentation : <http://man.openbsd.org/pflow>

[Netflow](https://en.wikipedia.org/wiki/NetFlow) est un protocole développé par CISCO pour collecter des informations sur le trafic réseau.
Cette page ne concerne que la configuration OpenBSD pour enregistrer les flows, pour pouvoir les exploiter, voir notre [Howto nfdump](../Howtonfdump).

## Configuration

Les paquets Netflow sont envoyés en UDP vers un démon chargé de collecter les informations.

Sous OpenBSD, cela s'active via :

~~~
# ifconfig pflow0 create
# ifconfig pflow0 flowsrc 192.0.32.10 flowdst 192.0.32.1:9995 pflowproto 10
~~~

> *Note* : on utilise ici Netflow v10 (IPFIX)


## Marquer les paquets à collecter

Il faut ensuite marquer les paquets que l'on veut collecter avec PF.

Pour collecter tout le trafic, on positionnera dans son `/etc/pf.conf` :

~~~
set state-defaults pflow
~~~

Pour marquer uniquement certains paquets il faut ajouter l'état _(pflow)_ au niveau des règles concernées, du type :

~~~
set skip on lo
pass keep state (pflow)
~~~


## FAQ

### Peut-on avoir des netflows sans activer les états PF ?

Non, les _Netflow_ s'appuyent justement sur les états PF, si vous ne les activez pas vous n'aurez pas de flow.

### Je n'obtiens pas la date de début du flow et sa durée

Cela ne fonctionnait pas avec des anciennes versions d'OpenBSD mais c'est bien OK depuis OpenBSD 6.0.

### Pics de paquets/octets

J'obtiens des pics sur mes courbes de paquets/octets, est-ce normal ? La réponse est « oui », car OpenBSD n'envoie un flow qu'à la fin d'un état et l'on peut donc avoir de très nombreux paquets/octets sur un laps de temps très court si c'est une longue connexion qui se termine juste à ce moment là. C'est le principe de capture d'un flow VS la capture de chaque paquet (ce qui est quasiment impossible, on ne capture donc qu'un échantillon des paquets).
