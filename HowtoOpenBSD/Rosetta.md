---
categories: openbsd
title: Rosetta : équivalence des commandes entre OpenBSD et Debian
---

Cette page indique à l'aide d'un tableau l'équivalence des différentes commandes entre Debian et OpenBSD. Seules les commandes de bases sont données ; pour les options et arguments possibles, voir les manuels correspondants.

## Réseau

Voir [/HowtoOpenBSD/Reseau]()

|Commande                   |Debian                |OpenBSD                  |
|---                        |---                   |---                      |
|Voir les IP des interfaces | `ifconfig` ou `ip a` | `ifconfig`              |
|Voir les ports en écoute   | `netstat` ou `ss`    | `fstat | grep internet` |

## Gestion des paquets

Voir [/HowtoOpenBSD/Packages]()

|Commande                       |Debian                       |OpenBSD                  |
|---                            |---                          |---                      |
|Rechercher un paquet           | `apt search <keywork>`      | `pkg_info -Q <keyword>` |
|Installer un paquet            | `apt install <pkg>`         | `pkg_add <pkg>`         |
|Désinstaller un paquet         | `apt remove <pkg>`          | `pkg_remove <pkg>`      |
|Lister les paquets installés   | `dpkg -l`                   | `pkg_info`              |
|Mettre à jour tous les paquets | `apt update && apt upgrade` | `pkg_add -u`            |

## Gestion système

|Commande                             |Debian                       |OpenBSD               |
|---                                  |---                          |---                   |
|Lister les fichiers ouverts          | `lsof <path>`               | `fstat -f <path>`    |
|Exécuter un programme périodiquement | `watch <command>`           | `gnuwatch <command>` |

Les crons réguliers sous Debian sont des dossiers dans lesquels les scripts sont exécutés. Sous OpenBSD, c'est un seul fichier contenant les commandes (ou chemin vers des scripts) à exécuter :

|Type de cron |Debian               |OpenBSD               |
|---          |---                  |---                   |
|Quotidien    | `/etc/cron.daily/`  | `/etc/daily.local`   |
|Hebdomadaire | `/etc/cron.weekly/` | `/etc/weekly.local`  |
|Mensuel      | `/etc/cron.monthly` | `/etc/monthly.local` |

## Gestion utilisateur

|Commande                                               |Debian                       |OpenBSD            |
|---                                                    |---                          |---                |
| Editer les hashs des mot de passes                    | `vipw -s`                   |          `vipw`   |