---
categories: sysadmin openbsd
title: Howto Cron sous OpenBSD
...

* man 5 crontab : <https://man.openbsd.org/man5/crontab.5>
* man 8 daily <https://man.openbsd.org/man8/daily.8>

Le service **cron** (diminutif de *crontab*, pour **c**h**ron**o **tab**le) est le planificateur de tâches standard sur les systèmes UNIX/Linux. Il permet le déclenchement de commandes à des dates récurrentes (à la minute près).

## Installation

Le service **cron** fait partie du système de base sous OpenBSD, et le service tourne déjà :

~~~
# rcctl check cron
cron(ok)
~~~

## Configuration

La configuration se trouve dans la crontab de l'utilisateur `root`, visible via la commande `crontab -l` :

~~~
SHELL=/bin/sh
PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/share/scripts"
HOME=/var/log
#
#minute	hour	mday	month	wday	[flags] command
#
# rotate log files every hour, if necessary
0	*	*	*	*	/usr/bin/newsyslog
# send log file notifications, if necessary
#1-59	*	*	*	*	/usr/bin/newsyslog -m
#
# do daily/weekly/monthly maintenance
30	1	*	*	*	/bin/sh /etc/daily
30	3	*	*	6	/bin/sh /etc/weekly
30	5	1	*	*	/bin/sh /etc/monthly
#~	*	*	*	*	/usr/libexec/spamd-setup

#~	*	*	*	*	-ns rpki-client -v && bgpctl reload
~~~

On trouve notamment des dates fixées pour les actions définies dans les fichiers `/etc/{daily,weekly,monthly}`.

## Utilisation

### /etc/{daily,weekly,monthly} et /etc/{daily,weekly,monthly}.local

Les trois fichiers `/etc/daily`, `/etc/weekly`, et `/etc/monthly` sont des scripts shell lancés périodiquement par cron, qui s'occupent des maintenances de base. Leur sortie, si elle existe, en envoyé par mail à l'utilisateur root.

Ces scripts ne doivent pas être modifiés. Ils exécutent chacun d'eux une version locale où peuvent être ajoutés des chemins vers les scripts à exécuter périodiquement : `/etc/daily.local`, `/etc/weekly.local`, et `/etc/monthly.local`. Les fichiers `*.local` sont exécutés en premier.

Pour lancer un script en tant que _root_ à une date fixée par le système, il suffit de le rendre exécutable et de le mentionner dans l'un des trois fichiers `/etc/{daily,weekly,monthly}.local` en fonction de la fréquence souhaitée. La sortie de l'ensemble des scripts pour chaque fréquence sera envoyée dans le même mail. Ainsi, on peut formater le mail à l'aide de `next_part` :

~~~
next_part "Checking /etc git status:"
/usr/local/bin/git --git-dir=/etc/.git --work-tree=/etc status --short
next_part "Evocheck output:"
sh /usr/share/scripts/evocheck.sh --verbose --cron
~~~

Dans le mail envoyé, le texte "Checking /etc git status:" ne sera indiqué seulement si le script ou la commande qui suit renvoie une sortie.

Pour une fréquence personnalisée, on peut utiliser la crontab de l'utilisateur qui doit lancer la commande, avec la commande `crontab -e -u <user>`. Par exemple :

~~~
# crontab -e -u _munin
*/5 * * * * /usr/local/bin/munin-cron > /dev/null
~~~

Les crons de chaque utilisateur sont stockés dans les fichiers `/var/cron/tabs/<user>`
