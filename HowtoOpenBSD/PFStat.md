---
categories: openbsd network firewall
title: Howto PFStat
---

## Installation

~~~
# pkg_add pfstat

# rcctl enable pfstatd
# rcctl start pfstatd  
pfstatd(ok)
~~~

## Configuration

Exemple de `pfstat.conf` pour afficher la bande passante utilisée:

~~~
collect 1 = interface "bnx0" pass bytes in ipv4 diff
collect 2 = interface "bnx0" pass bytes out ipv4 diff

image "/var/www/htdocs/pfstats/bnx0.jpg" {
        from 1 days to now
        width 980 height 300
        left
                graph 1 bps "in" "bits/s" color 0 192 0 filled,
                graph 2 bps "out" "bits/s" color 0 0 255
}

image "/var/www/htdocs/pfstats/bnx0-week.jpg" {
        from 1 weeks to now
        width 980 height 300
        left
                graph 1 bps "in" "bits/s" color 0 192 0 filled,
                graph 2 bps "out" "bits/s" color 0 0 255
}

image "/var/www/htdocs/pfstats/bnx0-month.jpg" {
        from 1 months to now
        width 980 height 300
        left
                graph 1 bps "in" "bits/s" color 0 192 0 filled,
                graph 2 bps "out" "bits/s" color 0 0 255
}
~~~

On lance ensuite les crons suivants :

~~~
# crontab -l

*   * * * *    /usr/local/bin/pfstat -q # Met a jour la base RRD
*/5 * * * *  /usr/local/bin/pfstat -p # Trace les graphs
~~~


Une petite page HTML pour rendre la consultation plus facile :

~~~{.html}
<html>
<h1>PFStats</h1>
  <h2>bnx0</h2>
    <h3>Today :</h3>
    <img src="bnx0.jpg"/>
    <h3>Last week :</h3>
    <img src="bnx0-week.jpg"/>
    <h3>Last month :</h3>
    <img src="bnx0-month.jpg"/>
</html>
~~~

## Lien utile 

<https://chown.me/blog/faire-des-graphes-a-partir-des-infos-de-pf.html>
