---
categories: openbsd system
title: Howto OpenBSD packages
...

* pkg_add(1) : <https://man.openbsd.org/pkg_add>
* FAQ : <http://www.openbsd.org/faq/faq15.html>
* liste de miroirs : <http://www.openbsd.org/ftp.html>

## Gestion des paquets

Les paquets se gèrent avec des commandes `pkg_XXX`.

Installer/désinstaller un paquet :

* Installer un paquet : `pkg_add <package>`
* Désinstaller un paquet : `pkg_delete <package>`

Utilisation plus avancée :

* Mettre à jour tous les paquets installés : `pkg_add -u`
* Désinstaller toutes les dépendances qui ne sont plus utilisées : `pkg_delete -a`
* Voir les paquets installés : `pkg_info`
* Avoir des infos sur un paquet (version disponible/installée, description) : `pkg_info <package>`
* Chercher un paquet contenant \<regexp\> dans son nom : `pkg_info -Q <regexp>`

## Gestion du miroir

Depuis OpenBSD 6.1, le miroir utilisé se configure dans `/etc/installurl`.

Par défaut, nous utilisons notre miroir http://mirror.evolix.org/openbsd/, qui supporte un certain nombre de versions antérieures.

Si on veut installer un paquet sous une version non supportée, on aura une erreur :

~~~
# pkg_add ncdu
https://ftp.evolix.org/openbsd/5.0/packages/amd64/: no such dir
Can't find ncdu
~~~

On peut dans ce cas ponctuellement modifier le miroir utilisé avec la variable `PKG_PATH`. On peut trouver plusieurs miroirs disposant de la version dont nous avons besoin ici : <http://www.openbsd.org/ftp.html>

~~~
# export PKG_PATH=http://ftp.eu.openbsd.org/pub/OpenBSD/$(uname -r)/packages/$(machine -a)/ 
# pkg_add ncdu                                                                              
ncdu-1.14.2: ok
~~~