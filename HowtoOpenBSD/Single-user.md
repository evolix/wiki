---
title: Howto OpenBSD/Single-user
categories: openbsd debug
---

## Démarrer OpenBSD en single-user mode

Dans le cas d'un problème de configuration empêchant le démarrage normal du serveur, on peut démarrer en mode single-user afin de corriger la configuration problématique.

Pendant le démarrage, au prompt `boot>` :

~~~
boot> boot -s
~~~

Une fois démarré, il faut définir la variable `TERM` :

~~~
# export TERM=xterm
~~~

Si on veut utiliser des outils comme vim, il faut monter la partition `/usr` :

~~~
# mount /usr
~~~

Si on veut modifier une configuration dans la partition `/`, il faut la remonter en read-write :

~~~
# mount -u -o rw /
~~~

Il suffit ensuite de faire la modification nécessaire, puis de redémarrer normalement.