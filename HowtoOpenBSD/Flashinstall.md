---
title: Howto OpenBSD/Flashinstall
categories: openbsd
---

## Introduction

L'idée ici est d'installer OpenBSD sur une mémoire flash type clé USB ou carte SD avec /var en read only afin de préserver le support.

## minicom

S'il n'y a pas d'interface vidéo et qu'il faut utiliser le port console, on utilise `minicom` :

~~~
pu port             /dev/ttyUSB0
pu baudrate         115200
pu rtscts           No 
pu xonxoff          Yes
~~~

On peut aussi lancer la commande directement : `$ minicom -b 115200 -D /dev/ttyUSB0`

## Mise en place

**1. Démarrer sur le CD d'installation d'OpenBSD**

<https://www.openbsd.org/faq/faq4.html#Download>

Pour un Raspberry Pi, télécharger une image pour l'architecture ARM64

On peut copier minirootXX.img sur la carte SD avec dd :

~~~
# dd if=minirootXX.img of=/dev/sdX bs=1M && sync
~~~

* Dans le cas d'utilisation du port série, au prompt `boot>` on indique au système que c'est une console série

~~~
boot> stty com0 115200
boot> set tty com0
~~~

Cela peut également être précisé dans le fichier de configuration `/etc/boot.conf`, avec la partition montée sur une autre machine :

~~~
# echo "stty com0 115200" > /mnt/etc/boot.conf
# echo "set tty com0" >> /mnt/etc/boot.conf
~~~

Puis activer la tty00 dans `/etc/ttys` : 

~~~
tty00   "/usr/libexec/getty std.115200" vt220    on secure  
~~~

* Dans le cas d'un Raspberry Pi, si l'on veut démarrer sur l'interface HDMI, il faudra remplacer ces commandes par :

~~~
boot> set tty fb0

# echo "set tty fb0" > /mnt/etc/boot.conf
~~~

**Note** : étant donné que l'installeur OpenBSD démarre entièrement en RAM, il est parfaitement possible de booter l'installeur sur une carte SD, puis d'installer sur cette même carte SD le système durant l'installation.

Puis on peut passer à l'installation

**2. Installer OpenBSD sur la Clé USB ou carte SD par exemple**

Pour un support de 8G, on utilise ce type de partitionnement :

**Note** : pour un Raspberry Pi, il est important de garder la partition MSDOS automatiquement créée pour pouvoir booter ensuite sur la carte SD.

~~~
/ 1G (a)
swap 512M (b)
/var 1536M (d) ou /var 512M (d) pour un Raspberry Pi 3B+ n'ayant que 1Go de RAM
/usr 2G (e)
/home lereste (f)
~~~

Pour un support de 16G et plus, on utilise le même partitionnement mais avec `5G` pour `/usr`, et `2G` pour la `swap`

**3. Une fois la machine redémarrée, activer le port console :**

* On configure la console série de manière définitive :

~~~
# echo "stty com0 115200" >> /etc/boot.conf
# echo "set tty com0" > /etc/boot.conf
~~~

* Dans le cas d'un Raspberry Pi, si l'on veut démarrer sur l'interface HDMI, il faudra remplacer ces commandes par :

~~~
# echo "set tty fb0" > /mnt/etc/boot.conf
~~~

* Sur les équipements ayant une carte vidéo comme les Raspberry Pi, si on veut utiliser le port console en mode multi-user, il faut l'activer :

~~~
# vim /etc/ttys
~~~

Modifier la ligne `tty00` pour activer le tty et définir son type :

~~~
tty00   "/usr/libexec/getty std.115200" vt220   on  secure
~~~

**Note** : si le seul accès courant est une console série et que la configuration n'a pas été faite, alors on peut redémarrer l'équipement et démarrer en [single-user](/HowtoOpenBSD/Single-user), où l'accès est actif par défaut, pour appliquer la configuration.

**4. Création des partitions mfs :**

~~~
# mkdir /var_persistent
# vi /etc/fstab
~~~

On modifie le fichier /etc/fstab afin qu'il ressemble à cela :

~~~
XXXXXXXXXXXXXXXX.b none swap sw
XXXXXXXXXXXXXXXX.a / ffs rw,softdep,noatime 1 1
XXXXXXXXXXXXXXXX.f /home ffs rw,softdep,noatime,nodev,nosuid 1 2
XXXXXXXXXXXXXXXX.e /usr ffs rw,softdep,noatime,nodev 1 2
XXXXXXXXXXXXXXXX.d /var_persistent ffs ro,softdep,noatime,nodev,nosuid 1 2
swap    /var    mfs   rw,nodev,nosuid,-P=/var_persistent,-s1536m   0 0
swap    /tmp    mfs   rw,noexec,nodev,nosuid,-s64m   0 0
~~~

Remplacer -`s1536m` par `-s512m` pour un Raspberry Pi 3B+

Ici on utilise mfs qui le gros avantage d'avoir l'option -P qui permet de recupérer le contenu d'un dossier !

On redémarre…

~~~
# Reboot
~~~

**5. Une fois la machine de nouveau disponible**

~~~
pkg_add rsync
~~~

**6. On rajoute le rsync de synchronisation vers la partition persistante**

Lors d'un shutdown propre :

~~~
# cat /etc/rc.shutdown
mount -u -w /var_persistent
sync
sleep 1
echo "Syncing /var (MFS) to Flash, please be patient..."
/usr/local/bin/rsync -aqH --delete --delete-excluded --exclude='run/*' --exclude='cache/*' --exclude='cron/tabs/*.sock' /var/ /var_persistent/
~~~

En cron une fois par semaine :

~~~
# Sync /var vers /var_persistent une fois par semaine
30      4       *       *       6       /bin/sh /etc/rc.shutdown >/dev/null && /sbin/mount -u -r /var_persistent
~~~

**7. On redémarre pour tester !**

## Upgrade du système

Lors d'une mise à jour du système, il faut remettre /var à la place de /var_persistent :

* Exécuter manuellement le cron pour synchroniser /var et /var_persistent
* Ouvrir `/etc/fstab`
* Commenter la ligne `swap    /var    mfs   rw,nodev,nosuid,-P=/var_persistent,-s1536m   0 0`.
* Remplacer `/var_persistent` par `/var`, et remplacer l'option `ro` par `rw`
* Commenter les lignes ajoutées dans `/etc/rc.shutdown`
* Redémarrer
* Faire [l'upgrade](/HowtoOpenBSD/Upgrade)
* Revenir sur le fichier `/etc/fstab` comme avant
* Redémarrer
* Décommenter les lignes dans `/etc/rc.shutdown`

## Liens utiles

* <https://sites.google.com/site/bsdstuff/mfs>
* <https://www.mimar.rs/en/sysadmin/2015/how-to-increase-openbsds-resilience-to-power-outages/>
* <http://wiki.gcu.info/doku.php?id=openbsd:install_soekris>
* <http://man.openbsd.org/newfs.8>
