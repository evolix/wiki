---
title: Howto vmm
categories: virtualisation openbsd
---

Documentation :

* <http://man.openbsd.org/OpenBSD-current/man4/vmm.4>
* <http://man.openbsd.org/OpenBSD-current/man8/vmd.8>
* <http://man.openbsd.org/OpenBSD-current/man8/vmctl.8>
* <http://man.openbsd.org/OpenBSD-current/man5/vm.conf.5>

OpenBSD intègre un système de virtualisation (écrit de zéro). Celui-ci est encore en cours de développement, cette documentation est donc susceptible de se retrouver obsolète par moment.

Vmd est le démon responsable de l'exécution des VMs sur un hôte et s'interface avec vmm (virtual machine monitor) intégré au noyau. Il est démarré en même temps que la machine et est contrôlé avec vmctl.

# Support matériel

Le processeur doit supporter la virtualisation, qui doit être activée dans le bios/efi.

Cas où la virtualisation n'est pas activée dans le bios :

~~~
$ dmesg | grep vm
vmm disabled by firmware
vmm at mainbus0 not configured
~~~

Cas où elle est activée et supportée :

~~~
$ dmesg | grep vm
vmm0 at mainbus0: VMX/EPT
~~~

# Installation d'une machine

On active et démarre vmd :

~~~
# rcctl enable vmd
# rcctl start vmd
~~~

Vmd utilise le fichier de conf */etc/vm.conf*, mais on préfère faire l'installation sans celui-ci, et ne l'utiliser qu'une fois la machine prête à être utilisée.

Création du disque :

~~~
# vmctl create -s 4G "/home/vm/OpenBSD.img"
~~~

Installation de la machine :

~~~
# wget https://ftp.openbsd.org/pub/OpenBSD/X.X/amd64/installXX.fs -O /home/vm/installXX.fs
# vmctl start -m 512M -b /bsd.rd -d /home/vm/OpenBSD.img -d /home/vm/installXX.fs -c OpenBSD
~~~

La machine démarrera sur le /bsd.rd de l'hôte, avec OpenBSD.img en premier disque, installXX.fs en second disque, 512M de RAM, et l'hôte se connectera automatiquement à la console de la VM.

Cela utilise cu(1) donc si on veut quitter : `<entrée> puis ~.` ou `<entrée> puis ~^D` (ne coupera pas la connexion SSH si l'hôte n'est pas local).

~~~
Connected to /dev/ttyp0 (speed 115200)

(I)nstall, (U)pgrade, (A)utoinstall or (S)hell?
~~~

Une fois installée, on éteint la VM et on la configure dans */etc/vm.conf* :

~~~
vm "OpenBSD" {
    disk "/home/vm/OpenBSD.img"
    memory 512M

    interface { switch "network" }
    interfaces 2
}

switch "network" {
    interface bridge0
}
~~~

Deux interfaces seront créées, dont l'une sera dans le bridge0 qui doit exister :

~~~
# echo "up" > /etc/hostname.bridge0 && sh /etc/netstart bridge0
~~~

# Utilisation

* Lister les VMs : `vmctl status`
* Démarrer une VM : `vmctl start <ID|name>`
* Se connecter à la console d'une VM : `vmctl console <ID|name>`
* Éteindre une VM : `vmctl stop <ID|name>`
* Mettre en pause/sortir de pause une VM : `vmctl (un)pause <ID|name>`.
* Recharger la configuration depuis */etc/vm.conf* : `vmctl reload`. Les VMs démarrées mais non présentes dans la configuration ne seront pas éteintes. Celles éteintes et présentes dans la configuration seront démarrées si elles n'ont pas l'option `disable`.

Toutes les actions sur les VMs peuvent être faites à partir de leur ID ou de leur nom.

# FAQ

## config_setvm: can't open tap tap: No such file or directory

Si on démarre des machines totalisant plus de 4 interfaces réseaux, vmd s'arrête et le message `config_setvm: can't open tap tap: No such file or directory` est visible dans les logs.

Cela est dû au fait que seules 4 interfaces tap sont créées par défaut, donc vmd n'arrive pas à attribuer les interfaces nécessaires aux VMs. Il faut les créer :

~~~
# cd /dev; sh MAKEDEV tap4
~~~

## Réseau Wi-Fi

Pour le réseau, en wifi il faut faire du NAT car on ne peut pas bridger l'interface wifi à cause d'une limitation dans le standard 802.11.

Pour faire cela, le mieux est de passer par une interface vether0.... à compléter.
