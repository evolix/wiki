---
categories: openbsd network
title: Howto Réseau sous OpenBSD
---

OpenBSD est particulièrement pratique comme équipement réseau car
celui-ci intègre toute une panoplie d'outil réseau dans le système de
base que certaines applications tierces (via les ports/packages)
viennent compléter.

[La FAQ6 est consacrée au réseau](https://www.openbsd.org/faq/faq6.html)

La commande
[ifconfig](http://www.openbsd.org/cgi-bin/man.cgi?query=ifconfig) est
l'outil principal pour gérer le réseau.

# Configuration

Configuration manuelle d'une interface :

~~~
# ifconfig vr0 192.0.32.10/24
# ifconfig vr0 inet6 2a01:4:3:2::1/64
~~~

Suppression d'une adresse d'une interface :

~~~
# ifconfig vr0 192.0.32.10/24 delete
# ifconfig vr0 inet6 2a01:4:3:2::1/64 delete
~~~

Configuration automatique :

~~~
# echo "inet 192.0.32.10/24" > /etc/hostname.vr0
# echo "inet6 2a01:4:3:2::1 64" >> /etc/hostname.vr0
~~~

Démarrer une interface par son fichier de configuration :

~~~
# sh /etc/netstart vr0
~~~

# Alias

Ajouter un alias :

~~~
# ifconfig vr0 inet alias 192.0.32.11/24
~~~

Supprimer un alias :

~~~
# ifconfig vr0 192.0.32.11/24 delete
~~~

Configuration automatique :

~~~
# echo "inet alias 192.0.32.11/24" > /etc/hostname.vr0
~~~

# DHCP

Le daemon [dhcpleased](http://man.openbsd.org/OpenBSD-7.0/dhcpleased.8) contrôle la configuration DHCP.

Configurer une interface en DHCP :

~~~
# cat /etc/hostname.vr0
autoconf
~~~

Dans le cas où on veut refuser des paramètres donnés par le serveur DHCP, il faut configurer [dhcpleased.conf](http://man.openbsd.org/OpenBSD-7.0/dhcpleased.conf.5). Par exemple, pour refuser la configuration DNS ainsi que la route par défaut sur l'interface `vr0` :

~~~
# cat /etc/dhcpleased.conf
interface vr0 {
    ignore dns
    ignore routes
}
~~~

On redémarre ensuite dhcpleased :

~~~
# rcctl restart dhcpleased
~~~

# Manipulation de la table de routage

Afficher la table de routage :

~~~
# route -n show
~~~

Afficher que la table de routage pour l'IPv6

~~~
# route -n show -inet6
~~~

Ajouter une route par défaut :

~~~
# route add default 192.0.32.254
~~~

Modifier la route par défaut :

~~~
# route change default 192.0.32.254
~~~

Ajouter une route pour une adresse :

~~~
# route add -host 192.0.33.42 192.0.32.253
~~~

Ajouter une route pour un réseau :

~~~
# route add 192.0.33.0/24 192.0.32.252
~~~

Pour supprimer une route :

~~~
# route delete 192.0.33.0/24
~~~

Voir la route utilisée pour joindre une IP :

~~~
# route get 8.8.8.8
~~~

# VLAN

<https://man.openbsd.org/vlan.4>

Exemple avec `/etc/hostname.vio0` :

~~~
up
~~~

et son interface VLAN correspondant `/etc/hostname.vlan0` :

~~~
parent vio0 vnetid 42
inet xxx
inet6 xxx
~~~

# PPPoE

Via configuration :

~~~
# cat /etc/hostname.bge0
description "Lien vers ADSL"
up
# cat /etc/hostname.pppoe0
inet 0.0.0.0 255.255.255.255 NONE \
        pppoedev bge0 authproto chap \
        authname 'LOGIN' authkey 'PASS' up
dest 0.0.0.1
inet6 <adresse IPv6> <masque>
~~~

Manuellement :

~~~
# ifconfig bge0 up
# ifconfig pppoe0  0.0.0.0 255.255.255.255 \
 pppoedev bge0 authproto chap authname 'LOGIN' authkey 'PASS' up
# ifconfig pppoe0 0.0.0.0 0.0.0.1
# ifconfig pppoe0 inet6  <adresse IPv6>/<masque>
~~~

Pour stopper :

~~~
# ifconfig pppoe0 down
# ifconfig bge0 down
~~~

Rajouter des routes statiques vers PPPoE :

~~~
# route add 1.2.3.0/24 -ifp pppoe0 0.0.0.1
~~~

Gestion de la route _default_ avec PPPoE (attention, on ne peut pas se servir du fichier `/etc/mygate`) :

~~~
# rm /etc/mygate
# echo '!route add default -ifp pppoe0 0.0.0.1'  >> /etc/hostname.pppoe0
# echo '!route add -inet6 default fe80::%pppoe0' >> /etc/hostname.pppoe0
~~~

# IPv6

## Router Advertisement daemon pour un routeur

Activation de *rad* pour annoncer un préfixe et un routeur.

`/etc/rad.conf` :

~~~
interface re0 {
  prefix 2001:db8::/32
  mtu 1500
}
~~~

On peut éventuellement spécifier des options DNS :

~~~
interface re0 {
  prefix 2001:db8::/32
  dns {
    nameserver 2001:db8::1
    nameserver 2001:db8::2
    search example.com
  }
  mtu 1500
}
~~~

Puis on l'active et on le lance :

~~~
# rcctl enable rad
# rcctl start rad
~~~

## Autoconfiguration pour un client

Activation de l'autoconfiguration IPv6 :

~~~
# ifconfig em0 inet6 autoconf
~~~

Le client aura besoin que le routeur ait configuré *rad* pour recevoir sa configuration IPv6.

# Bridge

Pour faire un bridge, il faut créer un fichier `/etc/hostname.bridge0` et ajouter les interfaces à bridger. Il ne faudra pas oublier de « up » les interfaces.

/etc/hostname.bridge0

~~~
add em1
add em2
stp em1
stp em2
up
~~~

STP permet d'activer le spanning tree.

`/etc/hostname.em1`

~~~
up
~~~

`/etc/hostname.em2`

~~~
up
~~~

# STP

Un rappel sur le fonctionnement :

<https://en.wikipedia.org/wiki/Spanning_Tree_Protocol>
<http://www.cisco.com/image/gif/paws/10556/spanning_tree1.swf>

Un simple ifconfig permet d'afficher le status.


~~~
bridge0: flags=41<UP,RUNNING>
        groups: bridge
        priority 32768 hellotime 2 fwddelay 15 maxage 20 holdcnt 6 proto rstp
        em2 flags=eb<LEARNING,DISCOVER,STP,AUTOEDGE,PTP,AUTOPTP>
                port 3 ifpriority 128 ifcost 200000 discarding role alternate
        em1 flags=eb<LEARNING,DISCOVER,STP,AUTOEDGE,PTP,AUTOPTP>
                port 2 ifpriority 128 ifcost 65535 forwarding role root

~~~

On voit ici que le port em2 est bloqué, et le port em1 est le port pour contacter le bridge root.

On pourra changer la priorité d'un bridge pour forcer le passage par lui. Par défaut ils ont une valeur identique de 32768.


~~~
ifconfig bridge0 spanpriority 16384
~~~

Par défaut OpenBSD est en RSTP, la bascule prend ~6. En STP, la bascule prend ~30s.

~~~
bridge0: flags=41<UP,RUNNING>
        groups: bridge
        priority 0 hellotime 2 fwddelay 15 maxage 20 holdcnt 6 proto rstp
        designated: id 90:e2:ba:28:b2:01 priority 0
        em2 flags=eb<LEARNING,DISCOVER,STP,AUTOEDGE,PTP,AUTOPTP>
                port 3 ifpriority 128 ifcost 200000 forwarding role designated
        em1 flags=eb<LEARNING,DISCOVER,STP,AUTOEDGE,PTP,AUTOPTP>
                port 2 ifpriority 128 ifcost 65535 forwarding role designated

~~~


/!\\ Attention, toucher au maxage et fwddelay peut causer une boucle. On déconseille de toucher à ces valeurs.

On peut aussi changer le maxage et le fwddelay pour une bascule plus rapide. (Minimum de 6s).


~~~
ifconfig bridge0 maxage 6
ifconfig bridge0 fwddelay 6
~~~

Ainsi en théorie bascule durera entre 6 et 12s.

# Debug / résolutions de problèmes

Voir des erreurs réseau :

~~~
# netstat -i
~~~

Voir des stats :

~~~
# netstat -s
~~~

Optimiser les buffers réseau :

~~~
# sysctl net.inet.udp.recvspace=262144
# sysctl net.inet.udp.sendspace=262144
~~~

Rappel des solutions "classiques" : soucis avec le MTU, ECN à
désactiver, Window Scaling à ajuster, Autoneg à désactiver

Voir en direct la quantité de trafic qui passe sur l'interface em0 :

~~~
netstat -b -n -w1 -h -I em0
~~~

Voir la quantité de trafic écoulé sur l'interface em0 

~~~
netstat -hI em0 # en nombre de paquets
netstat -bhI em0 # en quantité de data
~~~