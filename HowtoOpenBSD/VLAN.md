---
categories: openbsd network
title: Howto VLAN sous OpenBSD
---

* Documentation : <http://man.openbsd.org/vlan.4>

*IEEE 802.1Q/1AD* encapsulation/decapsulation pseudo-device

## Création manuelle

Création de 2 VLANs, 1 et 7, rattachés à l'interface bnx0

~~~
# ifconfig vlan1 create
# ifconfig vlan7 create
# ifconfig vlan1 vnetid 1 parent bnx0
# ifconfig vlan7 vnetid 7 parent bnx0
~~~

On configure ensuite l'interface de la même manière qu'une interface classique :

~~~
# ifconfig vlan1 10.10.10.1/24
# ifconfig vlan7 10.20.30.1/24
~~~

## Création définitive

~~~
# cat /etc/hostname.bnx0
up

# cat /etc/hostname.vlan1
vnetid 1 parent bnx0
up
description "VLAN1-Net1"
inet 10.10.10.1/24

# cat /etc/hostname.vlan7
vnetid 7 parent bnx0
up
description "VLAN7-Net1"
inet 10.20.30.1/24
~~~
