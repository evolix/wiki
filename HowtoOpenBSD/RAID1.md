---
categories: openbsd raid
title: HowTo RAID1 logiciel sur OpenBSD
---

* Documentation : <http://man.openbsd.org/bioctl.8>

Sur OpenBSD, le RAID logiciel se fait avec [bioctl(8)](http://man.openbsd.org/bioctl.8). Cet outil, qui est intégré dans le système de base, permet de faire plusieurs types de RAID (appelés "*disciplines*"). Les différentes possibilités de *disciplines* sont :

* 0 ([RAID 0](https://fr.wikipedia.org/wiki/RAID_(informatique)#RAID_0_:_volume_agr.C3.A9g.C3.A9_par_bandes))
* 1 ([RAID 1](https://fr.wikipedia.org/wiki/RAID_(informatique)#RAID_1_:_Disques_en_miroir))
* 5 ([RAID 5](https://fr.wikipedia.org/wiki/RAID_(informatique)#RAID_5_:_volume_agr.C3.A9g.C3.A9_par_bandes_.C3.A0_parit.C3.A9_r.C3.A9partie))
* C qui est en réalité du chiffrement
* c (faire attention à la casse typographie par rapport à la discipline
  précédente) qui permet de concaténer plusieurs disques en un seul
  volume logique.

## Installation avec du RAID1

*1) Démarrer sur le CD d'installation d'OpenBSD*

*2) Entrer en mode (S)hell*

*3) Créer le dev associé au deuxième disque (par défaut seul le premier disque est présent dans dev sous OpenBSD)*

~~~
# cd /dev
# sh MAKEDEV sd1
# dd if=/dev/zero of=/dev/sd0c bs=1m count=1
# dd if=/dev/zero of=/dev/sd1c bs=1m count=1
# fdisk -iy sd0
# fdisk -iy sd1
~~~

> **Note** : Le premier mégaoctet est utilisé par bioctl pour le RAID, donc
si notre disque a été utilisé avec softraid précédemment, on utilise
dd(1) pour l'effacer (`dd if=/dev/zero of=/dev/rsd0c bs=1M count=1`).

*4) On crée ensuite une large partition sur les disques (Notez bien le FS type: RAID)*

~~~
# disklabel -E sd0
Label editor (enter '?' for help at any prompt)
> a a
offset: [64]
size: [20964761]
FS type: [4.2BSD] RAID

# disklabel sd0 > structure
# disklabel -R sd1 structure
~~~

*5) On crée ensuite le Volume RAID1*

~~~
# bioctl -c 1 -l sd0a,sd1a softraid0
~~~

*6) On Lance l'install*

~~~
# install
~~~

À l'étape de préparation du disque, choisir notre nouveau volume RAID (dans notre cas sd2)

~~~
Available disks are: sd0 sd1 sd2.

Which one is the root disk? (or 'done') [sd0] sd2
~~~

Et voilà !

## Pour vérifier l'état du RAID

~~~
# bioctl sd2
Volume      Status               Size Device
softraid0 0 Online         2146656256 sd2     RAID1
          0 Online         2146656256 0:0.0   noencl <sd0a>
          1 Online         2146656256 0:1.0   noencl <sd1a>
~~~

On peut connaître les détails du disque (modèle et numéro de série) en rejouant la commande sur celui concerné :

~~~
# bioctl sd0 
sd0: <ATA, XXXXXX, YYYY>, serial ZZZZZZ
# bioctl sd1 
sd1: <ATA, XXXXXX, YYYY>, serial ZZZZZZ
~~~

## Simuler un disque mort

~~~
# bioctl -O /dev/sd0a sd2
~~~

En rejouant la commande bioctl sd2, vous devriez voir le disque sd0 Offline.

## Reconstruction du RAID

On reconstruit ici le RAID pour la partition sd0a.

0. On remet un nouveau disque
1. D'abord préparer le disque

~~~
# dd if=/dev/zero of=/dev/sd0c bs=1m count=1
# disklabel -E sd0
Label editor (enter '?' for help at any prompt)
> a a
offset: [64]
size: [20964761]
FS type: [4.2BSD] RAID
~~~

2. Puis lancer la reconstruction

~~~
# bioctl -R /dev/sd0a sd2
~~~

Maintenant allons prendre un café… ou deux… ou trois…
