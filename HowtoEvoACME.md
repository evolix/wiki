---
categories: web security
title: Howto EvoACME
...

* Code source : <https://gitea.evolix.org/evolix/evoacme>
* Rôle Ansible : <https://gitea.evolix.org/evolix/ansible-roles/src/branch/stable/evoacme/>

# Howto EvoACME

EvoACME est un ensemble de scripts qui facilite la création de certificats Let's Encrypt dans le contexte d'un serveur web Evolix (Apache + Evoadmin-web).

## Installation

Il existe un rôle Ansible qui facilite l'installation. Il est aussi possible de l'installer manuellement.

### certbot

EvoAcme utilise Certbot, le client Let's Encrypt créé par l'EFF.

Consultez notre [documentation de Let's Encrypt](/HowtoLetsEncrypt) pour l'installation de certbot.

Certbot installe son propre script dans la crontab, mais nous allons le remplacer

~~~
# mv /etc/cron.d/certbot /etc/cron.d/certbot.disabled
~~~

### evoacme

Configuration pour evoacme (`/etc/default/evoacme`) :

~~~{.bash}
SSL_KEY_DIR=/etc/ssl/private
ACME_DIR=/var/lib/letsencrypt
CSR_DIR=/etc/ssl/requests
CRT_DIR=/etc/letsencrypt
LOG_DIR=/var/log/evoacme
SSL_MINDAY=30
EMAIL=security@example.com
~~~

Tâche quotidienne (`/etc/cron.daily/evoacme`) :

~~~{.bash}
#!/bin/sh
#
# Run evoacme script on every configured cert
#
# Author: Victor Laborie <vlaborie@evolix.fr>
# Licence: AGPLv3
#

[ -f /etc/default/evoacme ] && . /etc/default/evoacme
CRT_DIR="${CRT_DIR:-'/etc/letsencrypt'}"

export QUIET=1

find "${CRT_DIR}" \
    -maxdepth 1 \
    -mindepth 1 \
    -type d \
    ! -path "*accounts" \
    ! -path "*archive" \
    ! -path "*csr" \
    ! -path "*hooks" \
    ! -path "*keys" \
    ! -path "*live" \
    ! -path "*renewal" \
    -printf "%f\n" \
        | xargs --max-args=1 --no-run-if-empty evoacme
~~~

### OpenSSL

On crée une configuration personnalisée pour OpenSSL dans `/etc/letsencrypt/openssl.cnf` :

~~~{.ini}
[req]
default_bits = 2048
encrypt_key = yes
distinguished_name = req_dn
prompt = no
[req_dn]
C = FR
ST = France
L = Marseille
O = Foo
OU = Security
emailAddress = security@example.com
~~~

### Intégration au serveur web

Cette phase là est à gérer avec l'installation de certbot.

## Scripts

Il y a 3 scripts à installer dans `/usr/local/sbin` :
* `vhost-domains`
* `make-csr`
* `evoacme`

## Exécution

Sur un serveur web typique il y a 3 étapes.

### Déterminer la liste des domaines

`vhost-domains example` permet d'extraire du VHost `example` la liste des domaines utilisés.

Il cherche les fichiers `/etc/apache2/sites-enabled/example.conf`, `/etc/nginx/sites-enabled/example.conf` et `/etc/nginx/sites-enabled/example` et utilise le premier trouvé.

~~~
# vhost-domains example
www.example.com
example.com
~~~

### Créer un CSR

À partir de la liste des domaines du VHost, on crée un CSR :

~~~
# make-csr example www.example.com example.com
~~~

Le CSR sera alors créé dans `/etc/ssl/requests/example.csr`

### Créer un certificat

~~~
# VERBOSE=1 evoacme example
~~~

Il va chercher un CSR à l'emplacement convenu : `/etc/ssl/requests/example.csr` et se connecter à Let's Encrypt pour créer un certificat.

La variable d'environnement `DRY_RUN=1` permet un mode « dry-run » en faisant l'ensemble du process sur l'API de production, mais sans générer le certificat final (attention, cela impacte quand même le quota d'utilisation de Let's Encrypt).

La variable d'environnement `TEST=1` permet de générer un certificat non valide mais sans décompter le quota d'utilisation de Let's Encrypt. En effet les actions sont limitées et pendant les tests de mise en place il vaut mieux ne utiliser l'API de production. À enlever bien sûr pour générer le certificat final.

La variable `VERBOSE=1` permet d'afficher de nombreux messages utiles pour comprendre ce qui se passe, surtout en cas d'erreur.

Le certificat sera généré dans le dossier ` /etc/letsencrypt/example/<TIMESTAMP>/` et un fichier de configuration est généré (ou mis à jour) dans `/etc/nginx/ssl/example.conf` (pour Nginx) :

~~~
# cat /etc/nginx/ssl/example.conf
ssl_certificate /etc/letsencrypt/example/live/fullchain.pem;
ssl_certificate_key /etc/ssl/private/example.key;
~~~

Il faut s'assurer que le fichier `/etc/nginx/ssl/example.conf` est bien inclus dans le fichier du VHost afin que le bon certificat soit utilisé.

~~~
server {
    […]
    include /etc/nginx/ssl/example.conf;
    […]
}
~~~

## FAQ

### Rewrite

Si vous avez une re-écriture http vers https, cela ne pose de problème (Let's Encrypt n'exige pas d'avoir un certificat valide pour sa vérification) MAIS il faut s'assurer de conserver l'URL.

Par exemple la ré-écriture suivante ne convient **pas** :

~~~
RewriteRule ^/(.*) https://example.com/ [L,R]
~~~

il faut avoir :

~~~
RewriteRule ^/(.*) https://example.com/$1 [L,R]
~~~

### Renommage des VirtualHosts

Attention, EvoACME s'appuie donc sur le nom du fichier de VirtualHost dans `/etc/apache2/sites-enabled/` ...attention en cas de renommage il faudra renommer également le .csr ! Notamment pour `000-default` si on le renomme en `zzz-default` par exemple.

### Nom des fichiers dans /etc/ssl/private

Attention, EvoACME va créer une clé privée dans /etc/ssl/private en fonction du nom du VirtualHost : il faut s'assurer que cela ne va pas écraser une clé existante.
