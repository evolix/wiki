**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Bogofilter

## Installation

~~~
# aptitude install bogofilter
~~~

## Apprentissage

Avec une Maildir pleine de spams :

~~~
$ bogofilter -s -v -B Maildir/.verified-spams/ -o 0.99,0.01
~~~

Avec une Maildir pleine de hams :

~~~
$ bogofilter -n -v -B Maildir/.verified-hams/ -o 0.99,0.01
~~~

Mettre des termes à ignorer dans un fichier ignore.txt puis :

~~~
$ cd ~/.bogofilter
$ bogoutil -l ./ignorelist.db < ignore.txt
~~~

## Correction de l'apprentissage

On repèrera tous les hams avec l'entête _X-Bogosity: Spam, tests=bogofilter, spamicity=1.000000_ que l'on mettra dans une Maildir :

~~~
$ bogofilter -Sn -v -B Maildir/.bogofilter-nonspam/ 
~~~

Quant aux spams notés pas assez généreusement par Bogofilter :

~~~
$ bogofilter -Ns -v -B ~/Maildir/.bogofilter-spam/ -o 0.99,0.01
~~~
