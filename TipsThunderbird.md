---
title: Tips Thunderbird
...

Quelques petits conseils astuces pour Thunderbird

## Désactiver l'ajout des adresses des messages sortants

* Édition > Paramètres > Rédaction
* Aller sur la section Adressage
* Décocher "Ajouter les adresses des messages sortants dans :"

Cela évitera par exemple de conserver des adresses avec erronés et surtout de remplir votre carnet d'adresse automatiquement


## Voir ou cacher des dossiers d'une boîte mail

* Clic droit sur le compte > S'abonner
  * Ou bien : clic gauche sur le compte > Fichiers > S'abonner
* Cocher ou décocher les dossiers souhaités

