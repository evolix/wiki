---
categories: web orchestration ansible
toc: yes
title: How to AWX
...

# How to AWX

* Documentation: [docs.ansible.com/ansible-tower/latest/html/userguide/overview.html](https://docs.ansible.com/ansible-tower/latest/html/userguide/overview.html)
* Repository: [github.com/ansible/awx](https://github.com/ansible/awx)

[AWX](https://www.ansible.com/community/awx-project) est une interface web et REST pour l'outil d'automatisation [ansible](Howtoansible).

## Installation

À partir de 18.0 il est recommandé de l'installer à travers [AWX Operator](https://github.com/ansible/awx-operator) qui dépend de kubernetes. Il est aussi possible d'utiliser une image docker.

### Kubernetes

Documentation: <https://ansible.readthedocs.io/projects/awx-operator/en/latest/installation/basic-install.html>

Déploiement de AWX Operator sur un cluster Kubernetes :

* On utilise kustomize pour assembler et appliquer les configurations yaml Kubernetes.

* Il faut une storage class fonctionnelle dans le cluster.
 
Remarques :

* On peut aussi utiliser un chart helm pour l'opérateur uniquement : https://ansible.readthedocs.io/projects/awx-operator/en/latest/installation/helm-install-on-existing-cluster.html

* Si vous n'avez pas de cluster Kubernetes, on peut facilement en créer un cluster *mono-neud* avec k3s par exemple voir [HowtoK3s](/HowtoK3s.md)

Nous allons créer un dossier awx contenant la configuration pour l'opérateur, une instance et traefik (ingress) :

~~~{.bash}
$ tree -L 3 awx
awx
├── kustomization.yaml
├── awx-instances
│   ├── demo
│   │   ├── awx-instance.yaml
│   │   ├── kustomization.yaml
│   │   └── traefik-ingressroute.yaml
│   └── kustomization.yaml
└── awx-operator
    └── kustomization.yaml
~~~

* un fichier kustomize qui va assembler tout `awx/kustomization.yaml`

~~~{.yaml}
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ./awx-operator
  - ./awx-instances
~~~

* L'opérateur `awx/awx-operator/kustomization.yaml ` qui va être créé dans le namespace `awx`

~~~{.yaml}
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  # Prendre le dernier release tag : https://github.com/ansible/awx-operator/releases
  - github.com/ansible/awx-operator/config/default?ref=2.10.0
# Set the image tags to match the git version from above
images:
  # le tag de l'image doit etre le meme que celui du release tag ci-dessus
  - name: quay.io/ansible/awx-operator
    newTag: 2.10.0

# Specifier le namespace de awx
namespace: awx
~~~

* Une instance...

~~~{.yaml}
# awx/awx-instances/demo/kustomization.yaml 
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - awx-instance.yaml
  - traefik-ingressroute.yaml

# awx/awx-instances/demo/awx-instance.yaml 
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx-demo
spec:
  service_type: clusterip

# awx/awx-instances/demo/traefik-ingressroute.yaml
apiVersion: traefik.io/v1alpha1
kind: IngressRoute
metadata:
  name: demo-instance-awx
spec:
  entryPoints:
    - websecure
  routes:                           
  - kind: Rule
    match: Host(`awx.chez-vous.org`)
    services:
    - kind: Service
      name: awx-demo-service
      port: 80
  tls:
    certResolver: letsencrypt
~~~

> on peut aussi configurer directement les paramètres de l'insgress dans le spec de l'instance, voir documentation : https://ansible.readthedocs.io/projects/awx-operator/en/latest/user-guide/network-and-tls-configuration.html

Lancer le Déploiement :

~~~{.bash}
$ kubectl apply -k awx
namespace/awx created
customresourcedefinition.apiextensions.k8s.io/awxbackups.awx.ansible.com created
customresourcedefinition.apiextensions.k8s.io/awxrestores.awx.ansible.com created
customresourcedefinition.apiextensions.k8s.io/awxs.awx.ansible.com created
serviceaccount/awx-operator-controller-manager created
role.rbac.authorization.k8s.io/awx-operator-awx-manager-role created
role.rbac.authorization.k8s.io/awx-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/awx-operator-metrics-reader created
clusterrole.rbac.authorization.k8s.io/awx-operator-proxy-role created
rolebinding.rbac.authorization.k8s.io/awx-operator-awx-manager-rolebinding created
rolebinding.rbac.authorization.k8s.io/awx-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/awx-operator-proxy-rolebinding created
configmap/awx-operator-awx-manager-config created
service/awx-operator-controller-manager-metrics-service created
deployment.apps/awx-operator-controller-manager created
~~~

* Voir ou en est l'installation

~~~{.bash}
$ kubectl -n awx logs -f deployments/awx-operator-controller-manager
# suivre le lancement d'instance(s)
$ kubectl logs -n awx -f deployments/awx-operator-controller-manager -c awx-manager
# Récuperer le mot de passe d'une instance
$ kubectl get secret -n awx awx-demo-admin-password -o jsonpath="{.data.password}" | base64 --decode ; echo
~~~

Si tout se passe bien, on

### Docker (Déprécié)

Copier le dépôt sur le serveur a partir d'une version stable: https://github.com/ansible/awx/releases/tag

Il faudra installer les paquets suivants:

 - `python3`
 - `python3-venv`
 - `python3-setuptools-scm`
 - `python3-psycopg2`
 - `ansible`
 - `docker-compose`

Le dépôt devra appartenir a l'usager exécutant, il faut s'assurer que les programmes dans le dossier tools ont bien un droit d’exécution. (Faire attention a noexec qui peut remonter jusqu'au conteneur dans certains cas)

Une fois les prérequis installée et les permissions bien appliquée, on peut continuer avec l'installation:

La documentation complète se situe sur [Github](https://github.com/ansible/awx/blob/devel/tools/docker-compose/README.md)

``
$ make docker-compose-build
$ make docker-compose
``

Suite a la complétion, on peut lancer dans un autre terminal:

``
$ docker exec tools_awx_1 make clean-ui ui-devel
``

On peut ensuite accéder a l'interface sur `https://localhost:8043/#/home`


### CLI

Une interface de ligne de commande peut aussi être installé directement sur l'hôte

```
$ pip3 install awxkit
```

### Doc

La documentation peut être installé en local

```
$ pip3 install sphinx sphinxcontrib-autoprogram
$ cd awxkit/awxkit/cli/docs
$ TOWER_HOST=https://awx.example.org TOWER_USERNAME=example TOWER_PASSWORD=secret make clean html
$ cd build/html/ && python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ..
```

## Configuration

### Créer un nouvel admin

```
$ docker exec -ti tools_awx_1 awx-manage createsuperuser
```

### Utiliser un postgres externe

Il faut aller indiquer le postgres désiré dans les variables de `tools/docker-compose/inventory`