* Statut de cette page : en test

VNC (Virtual Network Computing) est un protocole permettant la visualisation et le contrôle de bureau à distance.

Il se compose d'une partie serveur, se trouvant sur le poste dont on veut visualiser ou contrôler le bureau, et d'une partie client, sur le poste qui souhaite y accéder.

**Attention : Le lancement d'un serveur VNC peut présenter un risque grave de sécurité et donner un accès distant complet à la machine.**


# Installation

**Partie serveur :**

Sur Linux, on peut utiliser le serveur VNC `x11vnc`, qui permet de se connecter directement à X11 :

~~~
# apt install x11vnc
~~~

**Partie client :**

On utilise (par exemple) le client `xtightvncviewer` :

~~~
# apt install xtightvncviewer
~~~


# Utilisation

## Se connecter directement au bureau d'un poste

**[TODO: avec mot de passe]**

Pré-requis : 

* Le port 5900 doit être ouvert sur le poste qui lance le serveur VNC.
* Le poste qui lance le serveur VNC elle doit se trouver sur un réseau accessible au poste client VNC.

**Sur le poste serveur, dont on veut accéder au bureau :**

~~~
$ x11vnc -display :0 -viewonly
~~~

Options :

* `-display` : le display X11 que l'on veut partager.
* `-viewonly` : **important**, pour désactiver le contrôle à distance.
* `-forever` : pour rester à l'écoute après la déconnexion d'un client.
* `-shared` : pour autoriser plusieurs clients.
* `-rfbport` : le port d'écoute (**5900 par défaut**).

**Sur le poste client :**

~~~
$ vncviewer <IP_SERVEUR_VNC>:<DISPLAY>
ou bien :
$ vncviewer <IP_SERVEUR_VNC>::<PORT>
~~~

Noter l'utilisation de `::` pour distinguer le port du display.


## Se connecter au bureau d'un poste se trouvant derrière une passerelle

Si le poste est derrière une passerelle sur laquelle on a la main, il faut, en plus des instruction précédentes :

1. Configurer une redirection du port 5900 (par défaut) sur de la passerelle vers l'IP privée du poste serveur VNC.
2. Se connecter au serveur VNC en utilisant l'IP de la passerelle  :

~~~
$ vncviewer <IP_PASSERELLE>::<PORT>
~~~


## Se connecter au bureau d'un poste en passant par un serveur de rebond

Si le poste est derrière une passerelle sur laquelle on n'a pas la main, utiliser une redirection SSH inverse de port en passant par un serveur de rebond.

**Sur le serveur de rebond :**

* Activer `GatewayPorts yes` dans `/etc/ssh/sshd_config` et recharger `ssh`.
* Autoriser l'accès du port 5900 (par défaut) à l'IP du ou des clients VNC.

**Sur le poste serveur, dont on veut accéder au bureau :**

~~~
$ x11vnc -display :0 -viewonly
$ ssh -R <PORT>:127.0.0.1:<PORT> <SERVEUR_REBOND>
~~~

La dernière commande `ssh` redirige les connexions sur `SERVEUR_REBOND:PORT` vers le port local `PORT` (`127.0.0.1`).

**Sur le poste client :**

~~~
$ vncviewer <SERVEUR_REBOND>::<PORT>
~~~
