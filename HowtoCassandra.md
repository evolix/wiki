---
categories: databases
title: Howto Cassandra
---

* [Documentation Officielle](https://cassandra.apache.org/doc/latest/index.html)
* Statut de cette page : WIP

[Cassandra](https://cassandra.apache.org/) est une base de donnée NoSQL distribuée créée pour gérer une grande quantité de donnée sur beaucoup de serveurs.

## Installation

Nous utilisons les dépôts de Cassandra.

Ajouter le dépôt dans un fichier `/etc/apt/sources.list.d/cassandra.sources` :

```
Types: deb
URIs: https://debian.cassandra.apache.org
Suites: 50x
Components: main
Signed-by: /etc/apt/keyrings/apache-cassandra.asc
Enabled: yes
```

Puis récupérer la clé PGP :

```
# wget -O /etc/apt/keyrings/apache-cassandra.asc https://downloads.apache.org/cassandra/KEYS
# chmod 0644 /etc/apt/keyrings/apache-cassandra.asc
```

Puis :

```
# apt update
# apt install cassandra

# nodetool version
ReleaseVersion: 5.0
```

Afin de pouvoir configurer Cassandra, il faut supprimer le cluster créé par défaut à l'installation du paquet :

```
# systemctl stop cassandra
# rm -rf /var/lib/cassandra/*
```

Afin d'éviter un mauvais état détecté par systemd, nous utilisons une unité systemd `/etc/systemd/system/cassandra.service` réimplémentant correctement ce qui est fait par `/etc/init.d/cassandra` :

```
[Unit]
Description=Cassandra distributed storage system for structured data
Wants=network-online.target
After=network-online.target time-sync.target nss-lookup.target ntp.service mdadm.service
Before=multi-user.target
ConditionPathExists=%E/cassandra/cassandra.yaml
ConditionPathExists=%E/cassandra/cassandra-env.sh
ConditionPathExists=/usr/share/cassandra/apache-cassandra.jar

[Service]
Environment=CASSANDRA_HEAPDUMP_DIR=%S/cassandra
Environment=CASSANDRA_LOG_DIR=%L/cassandra
EnvironmentFile=%E/default/cassandra
LimitMEMLOCK=infinity
LimitNOFILE=100000
ConfigurationDirectory=cassandra
RuntimeDirectory=cassandra
LogsDirectory=cassandra
User=cassandra

Type=exec
ExecStart=/usr/sbin/cassandra -H ${CASSANDRA_HEAPDUMP_DIR} -E %S/cassandra/hs_err_%p.log -f

# Code de sortie de Java en cas de SIGTERM
SuccessExitStatus=143

# Commence par tenter d'éteindre proprement la JVM
KillMode=mixed

PrivateTmp=yes

# Dupliqué de /var/log/cassandra.log
StandardOutput=null

[Install]
WantedBy=multi-user.target
```

## Ports utilisés

Cassandra utilise les ports suivants :

- 7000/tcp: Communication entre les nœuds d'un cluster
- 7001/tcp: Communication chiffrée entre les nœuds d'un cluster (non utilisé par défaut)
- 7199/tcp: port pour monitoring JMX
- 9042/tcp: Communication entre les clients de Cassandra et le cluster
- 9142/tcp: Communication chiffrée entre les clients de Cassandra et le cluster

Les autorisations réseau nécessaires sont donc :

- Autoriser la communication sur le port 7000/tcp (ou 7001/tcp) entre les nœuds du cluster
- Autoriser la communication sur le port 9042/tcp (ou 9142/tcp) entre les clients Cassandra et les nœuds du cluster

## Configuration

> Les exemples vont être pour une machine 192.0.2.3 joignant un cluster appelé "Exemple" avec 192.0.2.1 et 192.0.2.2 comme points de contact du cluster.

La configuration se fait principalement dans `/etc/cassandra/cassandra.yaml`.

### Configuration du cluster

Le nom du cluster (principalement utile pour éviter l'ajout par erreur d'une machine d'un cluster "Foo" dans le cluster "Bar") est configuré par l'entrée `cluster_name`.

```
cluster_name: "Exemple"
```

Les serveurs de départ du cluster (nécessaire pour que les serveurs quels serveurs fassent partie du cluster) sont configurés à partir de `seed_provider`, cette configuration prend un nom de classe java et les paramètres de cette classe. La classe par défaut est `org.apache.cassandra.locator.SimpleSeedProvider` qui prend pour paramètres `seeds` qui contient les machines servant comme point de contacte, au format `<IP>:<Port>` et séparé par des virgules. Par exemple :

```
seed_provider:
  - class_name: org.apache.cassandra.locator.SimpleSeedProvider
    parameters:
      - seeds: "192.0.2.1:7000,192.0.2.2:7000
```

Finalement, chaque serveur doit être configuré pour écouter les autres nœuds du cluster sur une interface en particulier avec `listen_address`. **Cette configuration ne doit pas être à 0.0.0.0** !

```
listen_address: "192.0.2.3"
```

Pour écouter les clients Cassandra sur d'autres interfaces, il faut d'utiliser la configuration `rpc_address` qui peut être mise à 0.0.0.0 mais il faut alors configurer `broadcast_rpc_address` qui ne peut pas être 0.0.0.0.

### Sécurité

Par défaut Cassandra n'utilise pas d'authentification ni d'autorisation (donc tout le monde peut se connecter à un cluster Cassandra et faire ce qu'il veut dessus).

#### Activer l'authentification par mot de passe

Avant d'activer l'authentification par mot de passe, il faut augmenter le facteur de réplication du keyspace `system_auth`, la valeur recommandée est 3 à 5 par DC. Avec un seul DC, cela peut se faire par le biais de la commande CQL suivante (utiliser `cqlsh` pour l'exécutée) :

```
cqlsh> ALTER KEYSPACE system_auth WITH replication = {'class': 'SimpleReplicationStrategy', 'replication_factor': 3 };
```

Il faut ensuite modifier la configuration `authenticator` de `org.apache.cassandra.auth.AllowAllAuthenticator` à `org.apache.cassandra.auth.PasswordAuthenticator` sur un des nœuds du cluster et redémarré ce nœud.

Après cela le nœud en question utilise l'authentification par mot de passe. Il faut ensuite se connecter avec le rôle par défaut (`cassandra`/`cassandra`) et le remplacer avec un autre compte `SUPERUSER` :

```
# cqlsh -u cassandra -p cassandra
cqlsh> CREATE ROLE <compte> WITH SUPERUSER = TRUE AND LOGIN = true AND LOGIN = '<mot_de_passe>';
cqlsh> ALTER ROLE cassandra WITH SUPERUSER = false AND LOGIN = false;
```

Finalement modifier la configuration `authenticator` de `org.apache.cassandra.auth.AllowAllAuthenticator` à `org.apache.cassandra.auth.PasswordAuthenticator` sur les autres nœuds et les redémarrés.

#### Activer l'autorisation

Afin de pouvoir contrôler les accès aux ressources, il faut modifier la configuration `authorizer` de `org.apache.cassandra.auth.AllowAllAuthorizer` à une autre valeur puis redémarrer chaque nœud (à modifier dans la configuration de chaque nœud un à un). La seule autre valeur fournie par Cassandra est `org.apache.cassandra.auth.CassandraAuthorizer`.

## Gestion des rôles et des droits

> Voir la partie précédente pour activer l'authentification par mot de passe et la gestion des droits.

Cassandra utilise un système de rôles pour la gestion des droits, chaque rôle peuvent avoir des droits spécifiques ou hérité de droits d'un autre rôle. Certains rôles peuvent être utilisés pour se connecter au cluster et d'autres peuvent n'être utiles que pour grouper des droits.

### Lister les rôles

L'ensemble des rôles (qu'ils soient utilisables pour la connexion ou nom) peuvent être listés via la commande `LIST ROLES` :

```
cqlsh> LIST ROLES;
role                      | super | login | options | datacenters
---------------------------+-------+-------+---------+-------------
                   example | False |  True |        {} |         ALL
                 cassandra | False | False |        {} |         ALL
              exampleadmin |  True |  True |        {} |         ALL
```

Les rôles utilisables à la connexion peuvent être listés via la commande `LIST USERS` :

```
cqlsh> LIST USERS
 name         | super | datacenters
--------------+-------+-------------
      example | False |         ALL
 exampleadmin |  True |         ALL
```

### Créé un rôle utilisateur et un keyspace

```
cqlsh> CREATE KEYSPACE <keyspace> WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 3};
cqlsh> CREATE ROLE <utilisateur> WITH LOGIN = true AND PASSWORD = '<mot_de_passe>';
cqlsh> GRANT ALL ON KEYSPACE <keyspace> TO <utilisateur>;
```

(Il est aussi possible d'utiliser `hash_password` et `HASHED PASSWORD = '<...>'` pour éviter d'utiliser le mot de passe en clair.)

### Lister les permissions d'un rôle

```
cqlsh> LIST ALL PERMISSIONS OF <role>;
```

## Monitoring

### Nagios

Nous utilisons le plugin [check\_cassandra](https://github.com/civiccc/cassandra-nagios/blob/master/plugins/check_cassandra.pl) pour surveiller Cassandra. Ce plugin surveille à la fois que Cassandra est démarré, qu'il répond et que le cluster est dans un bon état. Ce check est présent sur chaques membres du cluster.

> Cassandra prend beaucoup de temps à redémarré, ce qui peut être détecté par le check.

## Maintenance

* Obtenir l'état du cluster :

  ```
  nodetool status
  ```

  Cela liste les noeuds du cluster, le "datacenter" dans lequel ces noeuds se trouvent et leur états du point de vue du noeud sur lequelvous avez exécuter la commande.

* Obtenir la distribution des temps de latence en lecture/écriture au niveau du coordinateur (sur tous les noeuds) :

  ```
  nodetool proxyhistograms
  ```

* Obtenir la distribution des temps de latences des requêtes locales à un noeud :

  ```
  nodetool tablehistograms <keyspace> <table>
  ```

* Obtenir le chemin de stockage des données :

  ```
  nodetool datapaths ["--"] [keyspace[.table]...]
  ```
* Les logs se trouvent dans `/var/log/cassandra/`, le fichier `system.log` contient les logs applicatifs et le fichier `gc.log` contient les logs du garbage collector java, qui peuvent être utils pour analysé un problème de lenteur.

