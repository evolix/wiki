---
title: Howto dwm
categories: window manager desktop suckless
---

**Site officiel** : <https://dwm.suckless.org/>

`dwm` est un gestionnaire de fenêtres très simple. Sa simplicité de conception permet de relativement facilement modifier le code source pour ajouter ou modifier des fonctionnalités. Il est écrit en C.

## Installation

### Sous Debian

On peut installer le paquet `dwm` directement. Comme quasiment tous les logiciels de la communauté _suckless_, `dwm` ne peut être configuré qu’en le compilant soi-même. Installer le paquet permet toutefois d’avoir un idée du logiciel brut, pour essayer, et d’installer ses dépendances, comme `dmenu`.

~~~
# apt install dwm
~~~

#### Compiler `dwm`

Par défaut, `make install` installe `dwm` sous `/usr/local`. Il faut modifier la variable `PREFIX` dans le fichier `config.mk` si on souhaite changer la destination.

~~~
# apt install libx11-dev libxft-dev libxinerama-dev
# cd /usr/local/src
# git clone git://git.suckless.org/dwm
# cd dwm
# cp config.def.h config.h
# vi config.h
# make clean install
~~~

## Barre d'état

`dwm` s’appuye sur le nom de la fenêtre racine (_root window_) et affiche le texte correspondant dans sa barre d’état. Pour changer le texte de la barre, on peut utiliser la commande `xsetroot` :

~~~
$ xsetroot -name 'hello there'
~~~

Le script _shell_ suivant permet d’afficher la date et l’heure dans la barre :

~~~
$ while xsetroot -name "$(date +'%F | %R')"
do
    sleep 20
done
~~~

## _Patchs_

De nombreux _patchs_ sont disponibles sur le [site officiel](https://dwm.suckless.org/patches/).

### Bépo

Si on utilise un clavier bépo, les raccourcis `MOD-[1-9]` ne fonctionneront pas. Pour corriger ça :

~~~
diff --git a/config.def.h b/config.def.h
index 9efa774..9a12476 100644
--- a/config.def.h
+++ b/config.def.h
@@ -81,6 +81,8 @@ static const Key keys[] = {
        { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
        { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
        { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
+       { MODKEY,                       XK_asterisk,view,          {.ui = ~0 } },
+       { MODKEY|ShiftMask,             XK_asterisk,tag,           {.ui = ~0 } },
        { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
        { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
        { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
@@ -94,6 +96,15 @@ static const Key keys[] = {
        TAGKEYS(                        XK_7,                      6)
        TAGKEYS(                        XK_8,                      7)
        TAGKEYS(                        XK_9,                      8)
+       TAGKEYS(                        XK_quotedbl,               0)
+       TAGKEYS(                        XK_guillemotleft,          1)
+       TAGKEYS(                        XK_guillemotright,         2)
+       TAGKEYS(                        XK_parenleft,              3)
+       TAGKEYS(                        XK_parenright,             4)
+       TAGKEYS(                        XK_at,                     5)
+       TAGKEYS(                        XK_plus,                   6)
+       TAGKEYS(                        XK_minus,                  7)
+       TAGKEYS(                        XK_slash,                  8)
        { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
 };
 
~~~

Les valeurs `XK_quotedbl`, `XK_guillemotleft` cie peuvent être obtenues avec la commande `$ xev -event keyboard`.

> On peut utiliser la même méthode (en changeant les `XK_`) pour azerty.
