---
categories: network
title: Howto IPMI
...

* <https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface>
* <http://buttersideup.com/docs/howto/IPMI_on_Debian.html>

## Installation

~~~
# apt install openipmi ipmitool
~~~

Vérifions que les modules kernel sont activés :

~~~
# modprobe ipmi_si
# modprobe ipmi_devintf
~~~

Assurons-nous qu'ils le sont au démarrage de la machine :

~~~
# cat /etc/modules
ipmi_si
ipmi_devintf
~~~

## Réseau IPMI

* Pour pouvoir accéder à l'IPMI via le réseau, il faut brancher son port Ethernet puis configurer son réseau :

~~~
# ipmitool lan print
# ipmitool lan set help

# ipmitool -I open lan set 1 ipsrc static
# ipmitool -I open lan set 1 ipaddr 192.0.2.5
# ipmitool -I open lan set 1 netmask 255.255.0.0
~~~

> Note : pour une machine sans port dédié à l'IPMI, le premier port réseau de la machine est partagé avec l'IPMI, avec sa propre adresse MAC. Dans ce cas et si le port du switch est en mode trunk avec un VLAN dédié réseau IPMI, il faut le configurer :

~~~
# ipmitool -I open lan set 1 vlan id <off|<id>>
~~~



* Si le mot de passe a été perdu, on peut modifier celui de l'utilisateur par défaut `root` :

~~~
# ipmitool user set password 2
~~~

Le mot de passe sera démandé à l'utilisateur. Attention, il faut mettre un mot de passe suffisamment complexe, sinon une erreur `Insufficient privilege level` apparaît.

* Pour accéder à l'interface IPMI d'une autre machine :

~~~
# ipmitool -I lan -H 192.0.2.6 -U root shell
~~~

## Diagnostic matériel

~~~
# ipmitool sel list
# ipmitool sdr list
# ipmitool sensor list
# ipmitool chassis status
~~~

## Entrées IPMI System Event Log

Visualiser les logs :

~~~
# ipmi-sel
~~~

Nettoyer le log (attention pas de retour en arrière possible) :

~~~
# ipmi-sel --clear
~~~

## Consommation électrique instantanée

~~~
# ipmitool dcmi power reading
~~~

## Afficher la valeur des registres d'un type particulier

Lister la valeur des sondes du type « Power supply » :

~~~
# ipmitool sdr type "Power Supply"
PS Redundancy    | 77h | ok  |  7.1 | Fully Redundant
Status           | 85h | ok  | 10.1 | Presence detected
Status           | 86h | ok  | 10.2 | Presence detected, Power Supply AC lost
~~~

Lister les types de sondes :

~~~
# ipmitool sdr type
Sensor Types:
        Temperature               (0x01)   Voltage                   (0x02)
        Current                   (0x03)   Fan                       (0x04)
        Physical Security         (0x05)   Platform Security         (0x06)
        Processor                 (0x07)   Power Supply              (0x08)
        Power Unit                (0x09)   Cooling Device            (0x0a)
        Other                     (0x0b)   Memory                    (0x0c)
        Drive Slot / Bay          (0x0d)   POST Memory Resize        (0x0e)
        System Firmwares          (0x0f)   Event Logging Disabled    (0x10)
        Watchdog1                 (0x11)   System Event              (0x12)
        Critical Interrupt        (0x13)   Button                    (0x14)
        Module / Board            (0x15)   Microcontroller           (0x16)
        Add-in Card               (0x17)   Chassis                   (0x18)
        Chip Set                  (0x19)   Other FRU                 (0x1a)
        Cable / Interconnect      (0x1b)   Terminator                (0x1c)
        System Boot Initiated     (0x1d)   Boot Error                (0x1e)
        OS Boot                   (0x1f)   OS Critical Stop          (0x20)
        Slot / Connector          (0x21)   System ACPI Power State   (0x22)
        Watchdog2                 (0x23)   Platform Alert            (0x24)
        Entity Presence           (0x25)   Monitor ASIC              (0x26)
        LAN                       (0x27)   Management Subsys Health  (0x28)
        Battery                   (0x29)   Session Audit             (0x2a)
        Version Change            (0x2b)   FRU State                 (0x2c)
~~~

## Gestion de l'alimentation

On peut éteindre, démarrer, redémarrer, ou visualiser l'état de l'alimentation d'un serveur :

~~~
# ipmitool power
chassis power Commands: status, on, off, cycle, reset, diag, soft
~~~

* Visualiser : `status`
* Démarrer : `on`
* Éteindre : `off`
* Redémarrer : `cycle` (off puis on), `reset` (hard reset), `soft` (soft redémarrage de l'OS via ACPI)
* Pulse a diagnostic interrupt (NMI) directly to the processor(s) : `diag`

## Redémarrer l'interface IPMI

C'est nécessaire après avoir configuré l'IP pour HP ILO pour qu'elle soit prise en compte :

~~~
# ipmitool mc reset warm # Pour recharger (suffisant pour iLO, attendre 10s)
# ipmitool mc reset cold # Pour redémarrer
~~~

## Serial Over LAN (SOL)

Il est possible d'accéder au port série d'un serveur via la connexion ethernet de l'IPMI. Pour ce faire, il suffit d'activer cette fonctionnalité dans l'interface IPMI.

Pour un serveur avec iDRAC9, avec l'IP 192.0.2.1, il suffit d'exécuter la commande racadm suivante :

~~~
$ ssh root@192.0.2.1 set iDRAC.IPMILan.Enable 1
~~~

Via l'interface web, cette option se trouve dans ces menus :

* en anglais : `iDRAC Settings -> Connectivity -> Network -> IPMI Settings -> Enable IPMI Over LAN : Enabled`
* en français : `Paramètre iDRAC -> Connectivité -> Réseau -> Paramètres IPMI -> Activer IPMI sur le LAN : Activé`

Il faut aussi s'assurer que le port UDP 623 est ouvert en sortie.
Si ce n'est pas le cas, voici un exemple avec `iptables` d'autorisation du port 623 sur le sous-réseau `192.0.2.0/24` où se trouve l'adresse IP de l'IPMI `192.0.2.1` :

~~~
/sbin/iptables -I OUTPUT -p udp --dport 623 --sport 1024:65535 -s 192.0.2.0/24 -j ACCEPT
/sbin/iptables -I INPUT -p udp --sport 623 --dport 1024:65535 -s 192.0.2.0/24 -j ACCEPT
~~~

Avant de tenter d'accéder au port série, on peut tester la connexion à l'IPMI du serveur (ici 192.0.2.1) via le port 631 avec les identifiants utilisés pour accéder à l'interface web :

~~~
$ ipmitool -C 3 -I lanplus -H 192.0.2.1 -U root power status
Password:
Chassis Power is on
~~~

On peut maintenant essayer de se connecter au port Serial Over LAN avec la commande suivante :

~~~
$ ipmitool -C 3 -I lanplus -H 192.0.2.1 -U root sol activate
~~~

Notez que par ce moyen, il est possible d'accéder au BIOS de la machine, puis son bootloader et finalement à l'OS ; mais pour avoir accès à chacun de ces composants, vous avez probalement besoin de les configurer respectivement pour qu'ils interagissent sur le port série choisi. Par exemple pour activer l'interaction sur le port série de GRUB et de Debian sur un serveur Dell R640, il fait définir les variables suivantes dans `/etc/default/grub` puis lancer `update-grub`.

~~~
GRUB_CMDLINE_LINUX="console=tty0 console=ttyS0,115200n8"
GRUB_TERMINAL="console serial"
~~~

## FAQ

### La connexion à l'interface web m'affiche une erreur 400

Sous certaines versions d'iDRAC, une [vérification du Host Header est activée](https://www.dell.com/support/kbdoc/fr-fr/000193619/http-https-fqdn-connection-failures-on-idrac9-firmware-version-5-10-00-00). Si on utilise un tunnel SSH pour se connecter, l'IP 127.0.0.1 utilisée pour la connexion à travers le tunnel ne correspond pas à l'IP de l'hôte, et la connexion est bloquée.

On peut vérifier ce paramètre, et le désactiver :

Pour iDRAC9 :

~~~
$ ssh root@192.0.2.10
racadm>>get idrac.webserver.HostHeaderCheck
HostHeaderCheck=Enabled
racadm>>set idrac.webserver.HostHeaderCheck 0
~~~

Pour iDRAC8 :

~~~
$ ssh root@192.0.2.10
/admin1-> racadm get idrac.webserver.HostHeaderCheck
[Key=idrac.Embedded.1#WebServer.1]
HostHeaderCheck=Enabled
/admin1-> racadm set idrac.webserver.HostHeaderCheck 0
[Key=idrac.Embedded.1#WebServer.1]
Object value modified successfully
~~~

### Pas d'option HTML5 pour la console : mettre à jour iDRAC 8

Un serveur avec iDRAC 8 ne peut pas être mis à jour vers iDRAC 9. Cependant, la version d'iDRAC 8 peut, elle, être mise à jour.

Les anciennes versions d'iDRAC 8 ne proposent pas l'accès à la console virtuelle en HTML5, seulement en java. Il faut donc la mettre à jour pour avoir l'option HTML5 :

* Chercher son serveur sur la page de support Dell : <https://www.dell.com/support/home/fr-fr?app=drivers>
* Sous `Pilotes et téléchargements`, sélectionner la catégorie `iDRAC avec Lifecycle Controller`
* Télécharger la version d'iDRAC la plus récente, en format `exe` (nom du type [`iDRAC-with-Lifecycle-Controller_Firmware_J3JTJ_WN64_2.85.85.85_A00.EXE`](https://dl.dell.com/FOLDER10762018M/1/iDRAC-with-Lifecycle-Controller_Firmware_J3JTJ_WN64_2.85.85.85_A00.EXE))
* Décompresser le fichier téléchargé (dans un dossier à part, car les fichiers sont mis directement à l'endroit où il est décompressé) : `unzip iDRAC-with-Lifecycle-Controller_Firmware_J3JTJ_WN64_2.85.85.85_A00.EXE`
* Depuis l'iDRAC, sous `iDRAC Settings`, cliquer sur `Update and Rollback`
* Depuis les fichiers décompréssés, fournir le fichier `payload/firmimg.d7`, puis lancer la mise à jour