**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Glassfish

<http://glassfish.java.net/documentation.html>

## Installation Glassfish 2

Installer le JDK Java 6 :

~~~
# aptitude install sun-java6-jdk
~~~

Créer un utilisateur glassfish et l'utiliser :

~~~
# useradd -m -d /srv/glassfish glassfish
# su glassfish
~~~

Télécharger la dernière version du logiciel (exemple pour Glassfish 2.1.1) :

~~~
$ wget <http://download.java.net/javaee5/v2.1.1_branch/promoted/Linux/glassfish-installer-v2.1.1-b31g-linux.jar>
~~~

Exécuter le JAR :

~~~
$ java -Xmx256m -jar glassfish-installer-v2.1.1-b31g-linux.jar
~~~

Se déplacer dans le répertoire `glassfish` et positionner les droits sur les binaires :

~~~
$ cd glassfish
$ chmod -R +x lib/ant/bin/*
~~~

Exécuter _ant_ (on fait ici une installation en mode _cluster_) :

~~~
$ lib/ant/bin/ant -f setup-cluster.xml 
~~~

### Démarrage

Exécuter la commande suivante, et patienter le temps du chargement :

~~~
$ ./bin/asadmin start-domain
~~~

L'interface d'admin est alors normalement disponible sur le port TCP/4848.

Note : attention, bien ajuster le fichier _/etc/hosts_ sous peine d'obtenir une erreur du type :
`[...] Unable to determine local hostname from InetAddress.getLocalHost().getHostName() [...] `

### Enregistrer le mot de passe admin

~~~
$ ./bin/asadmin login
~~~

Puis saisir l'identifiant et le mot de passe du compte admin.
Ces infos seront sauvées dans `$HOME/.asadminpass`.

Note : attention, il faut avoir démarrer (start-domain) avant de pouvoir faire cette commande

### Pour modifier le mot de passe admin

* Modifier dans l'interface d'admin de Glassfish
* Supprimer `$HOME/.asadminpass`
* relancer asadmin start-domain et suivre les instructions...

### Notes

* Ports à voir
* Script d'init.d à positionner
* Pour utiliser l'admin, éviter Firefox (pages blanches)

## Installation Glassfish 3 ou 4

v3.0 : <http://glassfish.java.net/downloads/3.1-final.html>

v3.1 : <http://glassfish.java.net/downloads/v3-final.html>

v4 : <https://glassfish.java.net/download.html>

~~~
$ wget <http://download.java.net/glassfish/3.1/release/glassfish-3.1.zip>
$ unzip glassfish-3.1.zip
$ cd glassfishv3
$ ./bin/asadmin
Use "exit" to exit and "help" for online help.
asadmin> version
Version string could not be obtained from Server [localhost:4848] for some reason.
(Turn debugging on e.g. by setting AS_DEBUG=true in your environment, to see the details).
Using locally retrieved version string from version class.
Version = GlassFish Server Open Source Edition 3.1 (build 43)
Command version executed successfully.
asadmin> exit
Command multimode executed successfully.
~~~

### 4.1

~~~
# adduser --disabled-password glassfish
# sudo -iu glassfish
$ wget <http://download.java.net/glassfish/4.1/release/glassfish-4.1.zip>
$ unzip glassfish-4.1.zip
~~~


## Logs

GlassFish gère lui-même la rotation de ses logs, mais ne les compresse pas.

On pourra trouver des informations de configuration des logs dans :

~~~
GLASSFISH_FOLDER/glassfish/domains/<DOMAIN>/config//domain.xml
ou
GLASSFISH_FOLDER/glassfish/domains/<DOMAIN>/config/logging.properties
~~~

Il peuvent être tournés toutes les heures et perturber un logrotate configurer uniquement pour de la compression.
On préférera soit la mise en place d'un cron de compression, soit la désactivation de la rotation des logs par Glassfish et l'utilisation de logrotate seul pour la rotation et la compression.


