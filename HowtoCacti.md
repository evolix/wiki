---
categories: web network
title: Howto Cacti
...

* Documentation : <https://docs.cacti.net/>

## Installation

~~~
apt install cacti
~~~

## FAQ

### Réinitialiser un mot de passe avec MySQL ?
Cela se passe dans la table user_auth et c'est encodé en MD5 (version 0.8.7) :

~~~
$ echo -n mynewpass | md5sum
4ef4d3b296aab9434da77e423cff3b8f
mysql> update cacti.user_auth set password='4ef4d3b296aab9434da77e423cff3b8f' where id=42;
~~~

### Générer correctement les graphes
Sous Debian Lenny, pour que Cacti puisse générer correctement tous les graphes (surtout les previews), il faut indiquer dans les paramètres Cacti la bonne de version de RRDTool à utiliser :
    Aller dans "Settings" > "RRDTool Utility Version" et sélectionner la version 1.2.x

### Problème de courbe limités à 100Mb/s

Si la courbe est limité, il faut d'abord s'assurer de deux choses :

* La courbe doit être en 64bits

> Vérification : onglet console, cliquer à gauche dans "Data Sources" puis choisir le graph limité. **Output Type ID** doit être à "In/Out Bits (64-bit Counters)"

* SNMPv2 doit être activé sur la machine

> Vérification : onglet console, cliquer à gauche sur "Devices" puis sur la machine, **SNMP Version** doit être à "Version 2"

Après avoir fait ces modifications, puisque les limitations sont écrites dans le fichier rrd, il faut les modifier manuellement :

Trouver le fichier rrd correspondant : onglet console, cliquer à gauche dans "Data Sources" puis choisir le graph limité. Le fichier est à l'emplacement indiqué par **Data Source Path**, dans /usr/share/cacti/site/rra/.

Voir les limitations actuelles :

~~~
# rrdtool tune <fichier_rrd>.rrd
DS[traffic_in] typ: COUNTER     hbt: 600        min: 0.0000     max: 10000000.0000
DS[traffic_out] typ: COUNTER    hbt: 600        min: 0.0000     max: 10000000.0000
~~~

Augmenter les limitations :

~~~
# rrdtool tune <fichier_rrd>.rrd -a traffic_in:1000000000
# rrdtool tune <fichier_rrd>.rrd -a traffic_out:1000000000
~~~

### Avoir le 95e percentile avec des débits supérieurs à 100Mb/s

Par défaut, les graphs pour le 95e percentile sont créés en 32bits. Pour pouvoir afficher un débit supérieur à 100Mb/s, il faut les transformer en 64bits :

Onglet console, cliquer à gauche sur "Data Queries", choisir "SNMP - Interface Statistics" puis "In/Out Bits with 95th Percentile".  
Il faut passer les data sources de **IfInOctets (Bytes In)** et **IfOutOctets (Bytes Out)** à **IfHCInOctets (Bytes In - 64-Bit Counters)** et **IfHCOutOctets (Bytes Out - 64-Bit Counters)**.