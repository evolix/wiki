# Howto clusterssh

~~~
$ cssh server1.example.com server2.example.com:2222
~~~

## Inventaire

Il est possible d'utiliser un fichier d'inventaire pour clusterssh, avec des groupes de serveurs :

~~~
$ cat inventory
group1 192.168.2.1 192.168.2.2 192.168.2.3
www www1.example.com www2.example.com
db db1.example.com db2.example.com
~~~

On peut alors ouvrir facilement une connexion sur chaque serveur d'un groupe

~~~
$ cssh -c inventory group1
~~~

## Exécution de commande

On peut indiquer une commande à exécuter directement sur tous les serveurs cibles. Exemple pour redémarrer Postfix sur un ensemble de serveurs :

~~~
$ cssh mx1.example.com mx2.example.com -a "systemctl reload postfix"
~~~

## Config spécifique

~~~
$ cat ~/.clusterssh/config

terminal_reserve_top=32
~~~

## Options SSH

~~~
$ cssh -o "-o IdentityFile=<chemin-clé>" <serveurs…>
~~~
