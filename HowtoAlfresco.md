**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto Alfresco Community

### Installation

Installer Tomcat 6 (voir [wiki:HowtoTomcat])

Installer MySQL (voir [wiki:HowtoMySQL])

Installer JDBC :

~~~
# aptitude install libmysql-java
# cd /usr/share/tomcat6/lib/
# ln -s ../../java/mysql-connector-java.jar mysql.jar
~~~

Si cela n'est pas fait, vous aurez les erreurs très explicites :

~~~
Caused by: org.hibernate.HibernateException: Hibernate Dialect must be explicitly set)
~~~

Alfresco a aussi besoin d'OpenOffice et ImageMagick :

~~~
# aptitude install openoffice.org-headless imagemagick
~~~

Egalement l'outil _swftools_ non disponible sous Debian Lenny, on va donc se servir chez les voisins :

~~~
wget <https://launchpad.net/ubuntu/+source/swftools/0.8.1-2.1ubuntu1/+build/924585/+files/swftools_0.8.1-2.1ubuntu1_amd64.deb>
dpkg -i swftools_0.8.1-2.1ubuntu1_amd64.deb
~~~

Il faut aussi préparer un répertoire pour le "contentstore" (stockage brut des documents), un autre pour les documents supprimés (à purger de temps en temps), un pour les index _Lucene_ et un autre pour les sauvegardes de ces index. Par exemple :

~~~
# mkdir /srv/contentstore /srv/contentstore.deleted /srv/indexes /srv/indexes.backup
# chown tomcat6:tomcat6 /srv/contentstore /srv/contentstore.deleted /srv/indexes /srv/indexes.backup
~~~

Pour configurer Alfresco, le plus simple est de créer un fichier _alfresco-global.properties_ (à mettre par exemple dans _/var/lib/tomcat6/shared/classes/_) :

~~~
db.name=NOM BDD
db.username=USER BDD
db.password=PASS BDD
db.host=127.0.0.1
db.port=3306

db.driver=org.gjt.mm.mysql.Driver
db.url=jdbc:mysql://${db.host}:${db.port}/${db.name}
hibernate.dialect=org.hibernate.dialect.MySQLInnoDBDialect

ooo.exe=/usr/bin/soffice
img.root=/usr
swf.exe=/usr/bin/pdf2swf

dir.root=/srv
dir.contentstore=${dir.root}/contentstore
dir.contentstore.deleted=${dir.root}/contentstore.deleted
dir.auditcontentstore=${dir.root}/audit.contentstore
dir.indexes=${dir.root}/indexes
dir.indexes.backup=${dir.root}/indexes.backup
~~~

Pour une configuration avancée, on utilisera le répertoire _/var/lib/tomcat6/shared/classes/alfresco/extension/_.
Par exemple pour customiser les logs d'Alfresco, on créera un fichier _myalfresco-log4j.properties_ :

~~~
log4j.appender.File2=org.apache.log4j.DailyRollingFileAppender
log4j.appender.File2.File=/var/log/tomcat6/alfresco.log
log4j.appender.File2.Append=true
log4j.appender.File2.DatePattern='.'yyyy-MM-dd
log4j.appender.File2.layout=org.apache.log4j.PatternLayout
log4j.appender.File2.layout.ConversionPattern=%d{ABSOLUTE} %-5p [%c] %m%n

log4j.rootLogger=error, Console, File2
~~~

Note : penser à créer le /var/log/tomcat6/alfresco.log avec les bons droits

Télécharger alfresco-community-war-3.3.zip sur <http://sourceforge.net/projects/alfresco/files/Alfresco/3.3%20Community%20Edition/alfresco-community-war-3.3.zip/download>
et déployer les fichiers WAR (alfresco.war, share.war si besoin) dans le répertoire _/var/lib/tomcat6/webapps/_, puis :

~~~
# /etc/init.d/tomcat6 restart
~~~

### Administration JMX

<http://wiki.alfresco.com/wiki/JMX>

On peut utiliser l'outil _jconsole_ avec les paramètres suivants :

~~~
Remote process = service:jmx:rmi:///jndi/rmi://localhost:50500/alfresco/jmxrmi
Username = controlRole
Password = change_asap
~~~

Note : mots de passe à changer via les fichiers _alfresco-jmxrmi.*_

