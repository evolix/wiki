---
title: Howto NewRelic
---

# Installer l'agent Servers "sysmond"

Doc officielle : <https://docs.newrelic.com/docs/servers/new-relic-servers-linux/installation-configuration/servers-installation-ubuntu-debian>

~~~
# echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' > /etc/apt/sources.list.d/newrelic.list
# wget -O /etc/apt/trusted.gpg.d/newrelic.asc https://download.newrelic.com/548C16BF.gpg
# dos2unix /etc/apt/trusted.gpg.d/newrelic.asc
# chmod 644 /etc/apt/trusted.gpg.d/newrelic.asc
# apt update
# apt install newrelic-sysmond
# nrsysmond-config --set license_key=<votre clé de licence>
# systemctl start newrelic-sysmond
~~~


# Installer l'agent PHP

Doc officielle : <https://docs.newrelic.com/docs/agents/php-agent>

~~~
# echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' > /etc/apt/sources.list.d/newrelic.list
# wget -O /etc/apt/trusted.gpg.d/newrelic.asc https://download.newrelic.com/548C16BF.gpg
# dos2unix /etc/apt/trusted.gpg.d/newrelic.asc
# chmod 644 /etc/apt/trusted.gpg.d/newrelic.asc
# apt update
# apt install newrelic-php5
# newrelic-install install
# rm /etc/php5/conf.d/newrelic.ini
# systemctl restart <votre serveur web>
~~~

Debconf vous demandera la clé de license à mettre. On pourra aussi la mettre à la main dans `/etc/php5/mods-available/newrelic.ini`.

# Divers

## Désactiver les requêtes à AWS

Il arrive que NewRelic fasse des requêtes à l'adresse `http://169.254.169.254/2009-04-04/meta-data/instance-id` qui est une API utilisée par AWS pour mettre à disposition des méta-données.

Lorsqu'on n'est pas chez AWS ça n'a aucune utilité. On peut désactiver cette fonctionnalité dans la configuration avec `utilization.detect_aws: false`.

Documentation : <https://docs.newrelic.com/docs/agents/ruby-agent/configuration/ruby-agent-configuration#utilization> (valable pour les agents Ruby et autres)

Dans le cas de newrelic-daemon :

~~~
# ps auwx | grep newrelic
root      6925  0.0  0.0 188284  7660 ?        Ssl  Apr27   0:00 /usr/bin/newrelic-daemon --agent --pidfile /var/run/newrelic-daemon.pid --logfile /var/log/newrelic/newrelic-daemon.log --port /tmp/.newrelic.sock --tls --define utilization.detect_aws=true --define utilization.detect_docker=true
root      6936  0.1  0.0 1467440 21840 ?       Sl   Apr27   1:20 /usr/bin/newrelic-daemon --agent --pidfile /var/run/newrelic-daemon.pid --logfile /var/log/newrelic/newrelic-daemon.log --port /tmp/.newrelic.sock --tls --define utilization.detect_aws=true --define utilization.detect_docker=true -no-pidfile
~~~

S'il n'y a que la config d'exemple, alors c'est le module PHP qui fournit sa commande. Elle est gérée dans `/etc/php5/mods-available/newrelic.ini`. Sinon, on peut copier la config d'exemple (`# cp /etc/newrelic/newrelic.cfg.template /etc/newrelic/newrelic.cfg`).

On doit ajuster les variables `utilization.detect_aws` (idem pour docker).

Si on gère le démon via le script d'init (donc pas via PHP), il faut relancer le démon :

~~~
# /etc/init.d/newrelic-daemon restart
~~~

