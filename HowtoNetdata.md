---
categories: web monitoring
title: Howto Netdata
...

  * [Documentation générale](https://learn.netdata.cloud/docs/agent) 

## Installation

~~~
# apt install curl gnupg apt-transport-https debian-archive-keyring
# cd /etc/apt/trusted.gpg.d
# wget https://packagecloud.io/netdata/netdata/gpgkey -O netdata.asc
# chmod 644 netdata.asc
# echo "deb https://packagecloud.io/netdata/netdata/debian/ $(lsb_release -c -s) main" > /etc/apt/sources.list.d/netdata_netdata.list
# apt update
# apt install netdata
~~~

## Accès

L'accès par défaut se fait sur le port 19999. Ou bien on peut configurer un reverse proxy Apache ou Nginx.

### Accès via Apache

~~~
    RewriteEngine On
    ProxyRequests Off
    ProxyPreserveHost On

    <Proxy *>
        Require all granted
    </Proxy>

    # Local Netdata server accessed with '/netdata/', at localhost:19999
    ProxyPass "/netdata/" "http://localhost:19999/" connectiontimeout=5 timeout=30 keepalive=on
    ProxyPassReverse "/netdata/" "http://localhost:19999/"

    # if the user did not give the trailing /, add it
    # for HTTP (if the virtualhost is HTTP, use this)
    RewriteRule ^/netdata$ http://%{HTTP_HOST}/netdata/ [L,R=301]
    # for HTTPS (if the virtualhost is HTTPS, use this)
    #RewriteRule ^/netdata$ https://%{HTTP_HOST}/netdata/ [L,R=301]
~~~
