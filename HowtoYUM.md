**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto YUM

Installation :

~~~
# yum install <package>
~~~

Mise-à-jour :

~~~
# yum update
# yum upgrade
~~~

Vider le cache :

~~~
# yum clean all
Loaded plugins: rhnplugin
Cleaning up Everything
~~~

Installer un paquet en désactivant un repo (pour prendre le paquet que dans le dépot souhaité)

~~~
yum repolist
yum install nom_du_paquet --disablerepo=epel
~~~