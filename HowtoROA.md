---
categories: network
title: Howto ROA
...

Une ROA (Route Origin Authorisations) est un objet signé cryptographiquement qui déclare quel AS à le droit d'annoncer quels préfixes depuis BGP. Elles dépendent d'une certification de ressources (RPKI), délivrée par un RIR (Regional Internet Registry) à un LIR (Local Internet Registry) et certifiant les ressources qui lui appartiennent. Un routeur BGP peut s'en servir pour vérifier l'origine d'un préfixe.

# Créer une ROA avec le RIPE

Le RIPE propose une documentation complète (en anglais) expliquant comment mettre en place une ROA : <https://www.ripe.net/manage-ips-and-asns/resource-management/certification/resource-certification-roa-management>

Le certificat (RPKI) est attribué par le RIPE avec une validité de 18 mois, puis automatiquement renouvelé tous les 12 mois. Il sera également mis à jour si les ressources qui nous appartiennent changent. La [RFC 7935](https://tools.ietf.org/html/rfc7935) décrit les paramètres utilisés pour la RPKI.

Pour créer une ROA, il suffit de se servir de l'outil disponible sur <http://my.ripe.net/#/rpki>. Sur cette page est affichée une liste des actuelles annonces BGP de l'AS. L'outil nous propose ensuite de créer les ROA qui correspondent à ces annonces très simplement en les cochant, puis en validant. Il faut donc vérifier avant de valider que les annonces BGP listées sont bien légitimes.

## Portée d'une ROA

En validant simplement les ROA proposées par l'outil, celles-ci seront spécifiques aux annonces BGP et il faudra en créer de nouvelles si l'AS veut faire de nouvelles annonces BGP.

Il est également possible de créer des ROA ayant une portée plus large, mais présentant l'inconvénient d'être moins sécurisé.

Par exemple, l'AS65536 détient le bloc IPv6 2001:DB8::/32, et annonce par BGP les blocs 2001:DB8::/48, 2001:DB8:1::/48 et 2001:DB8:4::/46.
L'outil proposera alors de créer les ROA 2001:DB8::/48 max-length /48, 2001:DB8:1::/48 max-length /48, et 2001:DB8:4::/46 max-length /46.

On pourrait vouloir créer une seule ROA 2001:DB8::/45 max-length /48 qui englobe les 3 ROA ci-dessus, le max-length signifiant que l'on peut annoncer tous les préfixes faisant partie de 2001:DB8::/45, jusqu'à une précision de /48. Cependant, cette ROA englobe également les préfixes 2001:DB8:2::/48 et 2001:DB8:3::/48 que l'AS65536 n'annonce pas, et qui auraient alors un statut valide si quelqu'un venait à les annoncer par BGP en se faisant passer pour cet AS.

Il faut donc garder des ROA aussi précises que possible pour profiter au mieux de cette fonctionnalité.

## Statuts des ROA

Une annonce BGP peut avoir 3 statuts ROA :

* ROA valid : l'annonce BGP est couverte par une ROA, le préfixe est annoncé par un AS autorisé et le max-length est respecté.
* ROA invalid : le préfixe est annoncé par un AS non autorisé, ou l'annonce est trop spécifique (le max-length n'est pas respecté).
* ROA unknown : le préfixe annoncé n'a aucune ROA.

Depuis l'interface web, il est possible de mettre en place des alertes par mail si un statut ROA invalid ou unknown se présente avec vérification toutes les 24h, et nous permettant d'agir en conséquence : un nouveau préfixe annoncé par l'AS avec oubli de créer une ROA, ou une fausse annonce (tentative de hijack) ?

# Influencer le routage avec les ROA

Le principal intérêt des ROA est de les utiliser afin de modifier les décisions de routage. Il faut utiliser pour cela le [RPKI Validator](https://www.ripe.net/manage-ips-and-asns/resource-management/certification/tools-and-resources) qui permet de récupérer la base de données contenant le statut de toutes les ROA. Le routeur (qui doit être compatible RPKI) peut ensuite être configuré pour associer chaque route de sa table de routage avec son statut ROA et lui appliquer une caractéristique particulière : par exemple une localpref à 90 pour les ROA invalides (voire un refus de la route), 100 pour les ROA inconnues et 110 pour les valides.

Il est nécessaire que le routeur soit compatible RPKI : [Juniper, Cisco et Nokia](https://www.ripe.net/manage-ips-and-asns/resource-management/certification/router-configuration), ainsi que les solutions libres telles que [Quagga ou Bird](http://rtrlib.realmv6.org/) ou encore [OpenBGPD](http://man.openbsd.org/man8/rpki-client.8) proposent des solutions.

## Configuration sous OpenBGPD

[OpenBGPD](HowtoOpenBSD/OpenBGPD) dispose de [**rpki-client**](http://man.openbsd.org/man8/rpki-client.8), disponible depuis OpenBSD 6.7.

Pour s'en servir, un cron et une configuration dans `/etc/bgpd.conf` sont nécessaires :

Dans la crontab de root :

~~~
~     *       *       *       *       -ns rpki-client -v && bgpctl reload
~~~

Par défaut, rpki-client va écrire un fichier contenant toutes les ROA valides dans `/var/db/rpki-client/openbgpd`. À partir de ce fichier, BGP saurra si un préfixe reçu est valide, invalide ou inconnu.

Dans la configuration de BGP, on inclut ce fichier et on indique que faire avec les routes invalides :

~~~
include "/var/db/rpki-client/openbgpd"
deny quick from ebgp ovs invalid
~~~

### Vérifications

Une fois configuré et exécuté une première fois, le statut des routes peut être vérifié depuis le routeur :

* Voir les routes reçues ayant un statut invalide, valide, ou inconnu :

~~~
# bgpctl show rib ovs invalid
# bgpctl show rib ovs valid
# bgpctl show rib ovs not-found
~~~

* Voir les routes reçues et utilisées ayant un statut invalide, valide, ou inconnu :

~~~
# bgpctl show rib selected ovs invalid
# bgpctl show rib selected ovs valid
# bgpctl show rib selected ovs not-found
~~~