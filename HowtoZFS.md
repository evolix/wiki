**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto ZFS

Les exemples donnés ici n'ont été testés sur OpenIndiana et Solaris, bien qu'ils soient probablement applicables à FreeBSD voir Linux, ils peuvent présenter de grosses différences dans le support de fonctionnalités spécifiques ou du montage automatique !

ZFS est le système de fichiers par défaut de Solaris et OpenIndiana, il regroupe à lui seul les outils de différents niveaux habituellement utilisés pour gérer le stockage tels LVM pour le découpage logique, mdadm pour la gestion du raid, et ext3/4 pour le filesystem en lui même.

Tout commence avec le zpool, qui est la première unité de gestion de disques avec ZFS, un zpool peut être composé d'un ou plusieurs disques, avec ou sans gestion de parité, et même avec ou sans cache séparé.
Viennent ensuite les volumes, qui sont découpés dans un zpool, et qui constituent un équivalent des partitions. Attention, par défaut un zpool est parfaitement utilisable comme un gros volume, mais rien n’empêche de le (re)découper par la suite, de la même manière un volume peut très bien être créé en tant que block device afin d'être partagé en iSCSI par exemple.

||*zpool*||*zfs*||
||RAID||Ext3/4||
||LVM||img||

## Zpool(s)

### Création

Créer un zpool simple (sur un disque nouvellement ajouté par exemple) :

~~~
# cfgadm 
Ap_Id                          Type         Receptacle   Occupant     Condition
sata6/0::dsk/c3t0d0            disk         connected    configured   ok
sata6/1::dsk/c3t1d0            cd/dvd       connected    configured   ok
sata6/2::dsk/c3t2d0            disk         connected    configured   ok
[...]
~~~

~~~
# zpool create test1 c3t2d0
# zpool list
NAME    SIZE  ALLOC   FREE  CAP  DEDUP  HEALTH  ALTROOT
rpool  7,94G  3,25G  4,69G  40%  1.00x  ONLINE  -
test1  49,8G   114K  49,7G   0%  1.00x  ONLINE  -
~~~

Créer un zpool avec plusieurs disques (équivalent d'un RAID0) :

~~~
# zpool create test2 c3t3d0 c3t4d0
'test2' successfully created, but with no redundancy; failure of one
device will cause loss of the pool
# zpool list
NAME    SIZE  ALLOC   FREE  CAP  DEDUP  HEALTH  ALTROOT
rpool  7,94G  3,24G  4,69G  40%  1.00x  ONLINE  -
test1  49,8G  86,5K  49,7G   0%  1.00x  ONLINE  -
test2  99,5G   127K  99,5G   0%  1.00x  ONLINE  -
~~~

Créer un zpool en RAID-Z (équivalent d'un RAID5) :

~~~
# zpool destroy test2
# zpool destroy test1
# zpool create test raidz c3t2d0 c3t3d0 c3t4d0
# zpool list
NAME    SIZE  ALLOC   FREE  CAP  DEDUP  HEALTH  ALTROOT
rpool  7,94G  3,26G  4,68G  41%  1.00x  ONLINE  -
test    149G   253K   149G   0%  1.00x  ONLINE  -
~~~

Dans ce cas, la parité est conservée une seule fois, mais il est possible de choisir d'utiliser un parité double ou triple en ayant un minimum de 4 ou 5 disques et en remplaçant _raidz_ par respectivement _raidz2_ ou _raidz3_.

Pour obtenir les détails sur l'état d'un zpool (qu'il s'agisse d'un raidz, ou d'un mirroir, ou encore d'un stripping sans parité) :

~~~
# zpool status test
  pool: test
 state: ONLINE
  scan: none requested
config:

        NAME        STATE     READ WRITE CKSUM
        test        ONLINE       0     0     0
          raidz1-0  ONLINE       0     0     0
            c3t2d0  ONLINE       0     0     0
            c3t3d0  ONLINE       0     0     0
            c3t4d0  ONLINE       0     0     0

errors: No known data errors
~~~

Les zpool créés localement sont montés par défaut sur la racine, pour changer ce comportement, il faut préciser le point de montage par défaut avec l'option _-m_ après le _create_ :

~~~
# zpool create -m /export/test test raidz  c3t2d0 c3t3d0 c3t4d0
# df -h
[...]
test                    98G    34K        98G     1%    /export/test
~~~

Il est à noter que les zpools sont également persistants, et sont (re)montés automatiquement au démarrage.

### Ajout de disques à un zpool

À compléter

### Désignation des disques de spare dans un zpool

~~~
# zpool add test spare c3t5d0
# zpool status test
  pool: test
 state: ONLINE
  scan: none requested
config:

        NAME        STATE     READ WRITE CKSUM
        test        ONLINE       0     0     0
          raidz1-0  ONLINE       0     0     0
            c3t2d0  ONLINE       0     0     0
            c3t3d0  ONLINE       0     0     0
            c3t4d0  ONLINE       0     0     0
        spares
          c3t5d0    AVAIL   

errors: No known data errors
~~~

Dès lors qu'un disque du pool est en défaut, tout se déroule automatiquement :

~~~
# zpool status test
  pool: test
 state: ONLINE
status: One or more devices is currently being resilvered.  The pool will
        continue to function, possibly in a degraded state.
action: Wait for the resilver to complete.
  scan: resilver in progress since Fri Apr 20 17:11:19 2012
    154M scanned out of 309M at 3,28M/s, 0h0m to go
    51,2M resilvered, 49,86% done
config:

        NAME          STATE     READ WRITE CKSUM
        test          ONLINE       0     0     0
          raidz1-0    ONLINE       0     0     0
            c3t2d0    ONLINE       0     0     0
            spare-1   ONLINE       0     0     0
              c3t3d0  UNAVAIL      0     0     0  corrupted data
              c3t5d0  ONLINE       0     0     0  (resilvering)
            c3t4d0    ONLINE       0     0     0
        spares
          c3t5d0      INUSE     currently in use

errors: No known data errors
~~~

### Remplacement d'un disque

Dans le cas précédent, un disque était en défaut, il a été remplacé par un disque de spare, mais il ne faut pas tarder à remplacer le disque défectueux afin d'éviter toute défaillance en cascade.

Le disque c3t3d0, vient d'être remplacé par un disque neuf :

~~~
# zpool replace test c3t3d0
# zpool status test
  pool: test
 state: ONLINE
  scan: resilvered 103M in 0h0m with 0 errors on Fri Apr 20 17:23:08 2012
config:

        NAME        STATE     READ WRITE CKSUM
        test        ONLINE       0     0     0
          raidz1-0  ONLINE       0     0     0
            c3t2d0  ONLINE       0     0     0
            c3t3d0  ONLINE       0     0     0
            c3t4d0  ONLINE       0     0     0
        spares
          c3t5d0    AVAIL   

errors: No known data errors
~~~

Si le disque n'avait pas été remplacé physiquement, mais qu'un autre disque était utilisable sur le serveur une autre solution aurait été de le remplacer par ce dernier :

~~~
# zpool replace test c3t3d0 c3t6d0
# zpool status test
  pool: test
 state: ONLINE
  scan: resilvered 103M in 0h1m with 0 errors on Fri Apr 20 20:33:44 2012
config:

        NAME        STATE     READ WRITE CKSUM
        test        ONLINE       0     0     0
          raidz1-0  ONLINE       0     0     0
            c3t2d0  ONLINE       0     0     0
            c3t6d0  ONLINE       0     0     0
            c3t4d0  ONLINE       0     0     0
        spares
          c3t5d0    AVAIL   

errors: No known data errors
~~~

Les deux commandes précédentes affichent un état de la reconstruction tant que l'opération n'est pas terminée.

### Réparation d'un zpool en défaut

À compléter

### Utilisation de disques de cache

À compléter

## Volumes ZFS

### Création

Créer un filesystem ZFS dans le zpool _test_ :

~~~
zfs create test/zfs
~~~

Créer un volume de type "block" (pour le partager en iSCSI, ou pour utiliser un autre système de fichiers dessus) dans le zpool _test_ :

~~~
# zfs create -V 90g test/iscsi
# zfs list
[...]
test                      93,3G  4,43G   427M  /export/test
test/iscsi                92,8G  97,3G  16,7K  -
~~~

### Partage en réseau

#### NFS

À compléter

#### SMB

À compléter

### Synchronisation de volumes/pools

Sychroniser filer1:zpool/vol1 vers filer2:zpool/vol2:

~~~
# Premiere synchro seulement:
filer1# zfs snapshot -r zpool/vol1@sync1  # -r = recursif
filer1# zfs send -R zpool/vol1@sync1 | ssh filer2 'zfs receive -F zpool/vol2'

# synchro incrémentale suivante:
filer1# zfs snapshot -r zpool/vol1@sync2
filer1# zfs send -R -I @sync1 zpool/vol1@sync2 | ssh filer2 'zfs receive -F zpool/vol2'
~~~

Attention: receive -F écrase la destination, et send -R envoie *tout* (points de montages, destruction/creation de sous volumes, snapshots, ...)

