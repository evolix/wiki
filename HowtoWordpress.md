---
categories: web webapp saas
title: Howto WordPress
...

* Documentation : <https://wordpress.org/support/>

[WordPress](https://wordpress.org/) est une application web libre de gestion de contenu, historiquement utilisée pour faire un blog.
C'est l'application web la plus utilisée au monde avec plus de 30% des sites web qui l'utilise.

## Installation

Nous préconisons d'utiliser WordPress avec [Apache-ITK](HowtoApache) et [MariaDB](HowtoMySQL).

Pour [Apache-ITK](HowtoApache) on désactive la gestion des droits pour faciliter les mises à jour :

~~~
AssignUserID example example
~~~

Et également la configuration suivante pour le *DocumentRoot* :

~~~
<Directory /home/example/www/>
    Options +SymLinksIfOwnerMatch
    AllowOverride AuthConfig Limit FileInfo Options Indexes
    Require all granted
</Directory>
~~~

Nous préconisons aussi de désactiver l'accès à `xmlrpc.php` sauf si vous en avez vraiment besoin :

~~~
<Files "xmlrpc.php">
Require all denied
</Files>
~~~

Nous utilisons l'outil [WP-CLI](https://wp-cli.org/fr/) :

~~~{.bash}
$ curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
$ php wp-cli.phar --info
$ php wp-cli.phar core download --path=$HOME/www
$ php wp-cli.phar core config --dbname=example --dbuser=example --dbpass=PASSWORD --dbhost=127.0.0.1 --path=$HOME/www
$ php wp-cli.phar core is-installed --path=$HOME/www
$ php wp-cli.phar core install --url=blog.example.com --title="TITRE" --admin_user="admin" --admin_password=PASSWORD --admin_email="blog@example.com" --skip-email --path="$HOME/www"
~~~

## Mise à jour

Les mises à jour sont très importantes car WordPress est très utilisé dans le monde, et il y a donc de nombreuses attaques sur les versions non à jour.

Par défaut, si les droits le permettent, le core de WordPress se met à jour automatiquement pour les versions mineures. Depuis la version 5.6, c'est même le cas pour les versions majeures. Plus d'informations sur <https://developer.wordpress.org/advanced-administration/upgrade/upgrading/#configuring-automatic-background-updates>.

Sinon, vous pouvez faire les mises à jour directement depuis l'interface d'admin ou alors avec [WP-CLI](https://wp-cli.org/fr/) :

~~~{.bash}
$ php wp-cli.phar cli update
$ php wp-cli.phar --info
$ php wp-cli.phar core download --force --path="$HOME/www"
$ php wp-cli.phar core check-update --path="$HOME/www"
$ php wp-cli.phar core update --path="$HOME/www"
$ php wp-cli.phar core verify-checksums --path="$HOME/www"
~~~

Attention, il faut parfois faire aussi une mise à jour de la base de données par l'interface web.

### Mise à jour par l'interface d'admin via SSH

**Cette méthode n'est plus recommandée. Préférez la méthode par défaut (`FS_METHOD` = `direct` et pas de séparation des droits par itk) quand possible.**

Si les droits ne le permettent pas, vous pouvez activer les mises à jour via l'interface d'admin avec SSH.

Tutoriel détaillé : <http://kbeezie.com/secure-wordpress-ssh2/>

**Important** : Installer le plugin WordPress [ssh-sftp-updater-support](https://wordpress.org/plugins/ssh-sftp-updater-support). Sa présence résoud bon nombre de problèmes. 

1. Vérifier que le module PHP `ssh2` est installé.
1. Activer `allow_url_fopen` dans la configuration du vhost.
1. Assurez-vous que l'option `define('FS_METHOD','direct')` n'est pas définie, on peut la forcer à `ssh2` si nécessaire dans le fichier `/home/<vhost>/www/wp-config.php`
1. Vérifier la correspondance des droits avec le module ITK d'Apache (directive `AssignUserID`) et l'accès à la clé privé SSH.

~~~
# vhost=siteweb
# install -d -o www-${vhost} -g $vhost -m 750 /home/${vhost}/sshkeys
# ssh-keygen -f /home/${vhost}/sshkeys/wordpress
# chown ${vhost}: /home/${vhost}/sshkeys/wordpress*
# cat /home/${vhost}/sshkeys/wordpress.pub >> /home/${vhost}/.ssh/authorized_keys
# sudo -u ${vhost} ssh -i /home/${vhost}/sshkeys/wordpress ${vhost}@127.0.0.1
~~~

Modifier le fichier `wp-config.php` en conséquence:

~~~
define( 'FS_METHOD', 'ssh2' );
define( 'FTP_USER', '$vhost' );
define( 'FTP_HOST', '127.0.0.1' );
define( 'FTP_PUBKEY', '/home/$vhost/sshkeys/wordpress.pub' );
define( 'FTP_PRIKEY', '/home/$vhost/sshkeys/wordpress' );
~~~

Il reste plus qu'a se rendre sur le backoffice du site à la page qui liste les modules et en mettre un à jour pour tester.

**Wordpress 4.3 et 4.8**, la mise à jour via ssh est cassée et il faudra appliquer ce fix : <https://web.archive.org/web/20190630102647/https://www.lekernelpanique.fr/2015/09/08/wordpress-4-3-casse-la-mise-a-jour-via-sshsftp/>


## Administration

### Utilisateurs

Créer un utilisateur admin :

~~~
$ php $HOME/wp-cli/wp-cli.phar user create admintest john@example.com --role=administrator
Success: Created user 3.
Password: XXXXXXX
~~~

Si temporaire, ne pas oublier de le supprimer :

~~~
$ php $HOME/wp-cli/wp-cli.phar user list
+----+------------+---------------+----------------------+----------------------+---------------+
| ID | user_login | display_name  | user_email           | user_registered      | roles         |
+----+------------+---------------+----------------------+----------------------+---------------+
| X  | admintest  | admintest     | johndoe@example.com  | 2016-10-17 17:40:26  | administrator |

$ php $HOME/wp-cli/wp-cli.phar user delete X
~~~

### Version

Si l'on a accès au code PHP, la version actuelle de WordPress se trouve dans le fichier `wp-includes/version.php`.


## plugins

On peut vérifier la liste des plugins installés sur Wordpress :

~~~{ .sql }
SELECT * FROM wp_options WHERE option_name = 'active_plugins';
~~~

### W3 Total Cache

Le plugin W3 Total Cache améliore les performances d'un « site/blog » propulsé par Wordpress ainsi que la rapidité du chargement des pages pour une meilleure expérience utilisateur et un meilleur référencement !

Pour cela W3 Total Cache met en cache différents éléments : renforcement du cache navigateur, cache des pages, des objets et des requêtes SQL. Il propose aussi différentes fonctions tel que le html/css/js minify et la gestion de CDN.

La configuration se passe dans le tableau de bord de Worpdress, dans l'onglet « Performance ».
Cliquer sur le bouton « Compatiblity Check », pour vérifier que votre serveur dispose de tous les modules nécessaire.

### Jetpack Protect

Le plugin [Jetpack Protect](https://jetpack.com/protect/) permet d'effectuer des scans réguliers et d'avoir des règles de « WAF » via une option payante (environ 15 EUR par mois).
Attention, Jetpack utilise `xmlrpc.php` donc il faut le garder actif ou ajouter les [adresses IP Jetpack en liste blanche](https://jetpack.com/support/how-to-add-jetpack-ips-allowlist/).
Pour une protection efficace, les règles « WAF » peuvent être mises dans un `.htaccess` :

~~~
php_value auto_prepend_file "/home/FOO/www/wp-content/jetpack-waf/bootstrap.php"
~~~

## Sécurité

### Mise à jour


Nous préconisez que les mises à jour automatiques soient activées, même pour les versions majeures.
Normalement c'est le comportement par défaut depuis WordPress 5.6 mais vérifiez bien que c'est le cas et si besoin
activez « Enable automatic updates for all new versions of WordPress. »

### Restriction de l'admin par IP

Nous préconisons de restreindre l'accès à l'interface d'admin par IP :

~~~
<LocationMatch "^/wp-(admin|login)">
Require ip 192.0.2.42
</LocationMatch>
~~~

et nous préconisons aussi de bloquer l'exécution de PHP dans wp-contents/uploads (attention, cela risque de casser certains plugins « sales ») :

~~~
<Directory /home/FOO/www/wp-content/uploads/>
    <FilesMatch "\.php$">
      Require all denied
    </FilesMatch>
</Directory>
~~~

### Fail2Ban

Si l'on ne peut pas restreindre l'accès à l'interface d'admin, nous préconisons d'utiliser [HowtoFail2Ban]().

Par ordre de préférence, nous conseillons d'utiliser :

- [Wordpress avec plugin Fail2Ban](HowtoFail2Ban#wordpress-avec-plugin-fail2ban)
- [Wordpress avec plugin simple](HowtoFail2Ban#wordpress-avec-plugin-simple)
- [Wordpress sans plugin](HowtoFail2Ban#wordpress-sans-plugin)

### WPScan

[WPScan](https://wpscan.org/) est un outil de scan de vulnérabilités en boite noire dédié à WordPress. Il va détecter la version de WordPress, lister d'éventuelles vulénrabilités connues pour cette version, répertorier les thèmes et plugins utilisés avec leur failles et pointer des défauts de configuration de l'installation.

Pour l'installer, vous pouvez suivre les instructions de [la documentation](https://github.com/wpscanteam/wpscan#install) à savoir :

* Installer ruby/gem en vous appuyant sur [Rbenv](https://wiki.evolix.org/HowtoRbenv)
* En tant qu'utilisateur, faire `gem install wpscan`

Nous conseillons de l'installer sur une machine distante, et pas directement sur le serveur qui héberge WordPress.

Avant la première utilisation, et de manière régulière, il faut mettre à jour WPScan, cela lui permet de récupérer la liste des dernières vulnérabilités. Vous pouvez le faire avec la commande suivante :

~~~
$ ruby wpscan.rb --update
~~~

Pour « scanner » un site, l'opération est simple, il suffit juste de donner l'adresse du site à WPScan. Vous optiendrez alors un compte rendu complet de la situation.

~~~
$ ruby wpscan.rb --url example.net
~~~


## FAQ

### Gestion des redirections

Lister les redirections déjà présentes :

~~~{.bash}
$ php $HOME/wp-cli/wp-cli.phar rewrite list --format=csv
~~~

Lister selon url donné (ex: SERVERNAME/fr_FR) :

~~~{.bash}
$ php $HOME/wp-cli/wp-cli.phar --url=blog.example.com rewrite list --format=csv --match=fr_FR
~~~

### Forcer les droits

Nous conseillons de désactiver la gestion des droits grâce à Apache-ITK (cf Installation).

Si vous ne faites pas cela, sachez que WordPress surcharge l'umask défini par Apache/PHP et écrit par défaut les fichiers et dossiers en 750.

Il faut rajouter ces 2 lignes dans `wp-config.php` pour avoir des droits corrects :

~~~
define( 'FS_CHMOD_DIR', ( 0770 & ~ umask() ) );
define( 'FS_CHMOD_FILE', ( 0660 & ~ umask() ) );
~~~

### Problème de "Live preview" sur les thèmes

S’il y a un problème pour generer un "live preview" d'un thèmes, ou si on a une erreur php de ce type dans les logs :

PHP Fatal error:  Call to undefined function submit_button() in $HOME/www/wp-content/plugins/ssh-sftp-updater-support/sftp.php on line 249

Il faut ajouter cette option dans le fichier wp-config.php :

~~~
define( 'FS_METHOD', 'direct' );
~~~

Il faut placer cette option avant toutes les autres, en début de fichier.

> **Note** : Attention, ne pas faire ceci si apache tourne avec un autre utilisateur (tel que le module ITK).

### Erreur lors de mise a jour : Impossible de localiser le dossier racine de WordPress (wp-content)

Il faut ajouter ce hack à la fin du fichier *wp-config.php* :

~~~
if(is_admin()) {
add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
define( 'FS_CHMOD_DIR', 0751 );
}
~~~

> **Note** : Attention, ne pas faire ceci si apache tourne avec un autre utilisateur (tel que le module ITK).

### Erreur lors de la publication d'une page

Si vous rencontrez l'erreur 404 suivante dans les logs lors de la publication d'une page :

~~~
POST /wp-json/wp/v2/pages/xx?_locale=user HTTP/1.1" 404
~~~

Cela vient probablement des permaliens. Il suffit de les mettre à jour dans la partie Réglages > permaliens du panneau d'administration en cliquant sur "Enregistrer les modifications".

### WordPress multisite

<https://codex.wordpress.org/Nginx#WordPress_Multisite_subdomains_rules>

### HTTP -> HTTPS

Si l'on veut passer en HTTPS un site précédemment en HTTP, outre la configuration Apache / redirection automatique, il faut aussi remplacer
dans la configuration toutes les URLs en HTTPS. Pour faire cela, on peut faire une dump de la table `wp_options` puis faire des remplacements avec un éditeur :

~~~
$ mysqldump wp_options --no-create-db --replace --extended-insert=FALSE --where="option_value LIKE '%http:%'" > https.sql
~~~

> **Note** : Attention, gardez bien le dump original pour restaurer les lignes originales en cas de besoin

### reverse-proxy et HTTPS

Si l'on a un reverse-proxy devant Wordpress et une communication HTTP entre le reverse-proxy et le serveur final,
il faut indiquer à Wordpress qu'on est bien en HTTPS, cela peut se faire via la configuration Apache par exemple :

~~~
SetEnv HTTPS on
~~~

### PHP Fatal error:  Uncaught Error: Call to undefined function is_admin() 

Si vous obtenez `Call to undefined function is_admin()` par exemple avec WP-CLI :

~~~
PHP Fatal error:  Uncaught Error: Call to undefined function is_admin() in phar:///home/stage3eme/wp-cli.phar/vendor/wp-cli/wp-cli/php/WP_CLI/Runner.php(1334) : eval()'d code:89
~~~

c'est que vous avez probablement une vieille fonction `is_admin()` qui traîne dans `wp-config.php` ou autre.

### Réinitialisation d'un mot de passe en ligne de commande

On se plaçant dans le dossier ou est installé roundcube on peut réinitialiser le mot de passe d'un utilisateur et même de l'administrateur, en ligne de commande :

~~~
$ php wp-cli.phar user reset-password --show-password $user
~~~

### Traduction

Par exemple pour le thème "barletta" :

~~~
$ cd wp-content/themes/barletta/languages
$ cp barletta.pot fr_FR.po
$ vim fr_FR.po
$ msgfmt fr_FR.po -o fr_FR.mo
~~~



