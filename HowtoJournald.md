---
categories: system sysadmin
title: Howto systemd-djournald
...

# Howto journald

## `systemd-journald`

* Manpage : <https://www.freedesktop.org/software/systemd/man/latest/systemd-journald.html>

`journald` est le composant de `systemd` qui collecte et stocke les logs. Il remplace les solutions de gestion de logs traditionnelles comme `syslog`.

Les logs sont stockés sous forme binaire dans le dossier `/var/log/journal/`. Si ce dossier n'existe pas, `journald` ne gardera des logs qu'en mémoire de manière temporaire, ils ne serons plus écrit sur le disque.

## `journalctl`

* Manpage : <https://www.freedesktop.org/software/systemd/man/latest/journalctl.html>

`journalctl` est l'utilitaire permettant d'interagir avec les logs collectés par `journald`. Il sert affiché, rechercher, filtrer et d'analyser les logs systèmes ; ce qui avec `syslog` était fait avec `grep`, `sort`, `awk`, etc.

### Commandes de base

Voici quelques commandes de base que l'on pourra combiner :

* `journalctl -u <unité>` : logs d'une unité
* `journalctl -b` : logs depuis le dernier boot
* `journalctl -b-1` : logs depuis l'avant-dernier boot, etc.
* `journalctl --since "2024-02-29 13:37:00"` : logs depuis une certaine date
* `journalctl --until "2024-02-29 14:37:00"` : logs jusqu'à une certaine date
* `journalctl -f` : sortie dynamique des derniers logs similaire à `tail -f`
