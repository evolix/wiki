**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto VMware

Nous parlons ici de VMware ESXi utilisé notamment avec le vSphere Client.

* Version 4.0
* Version 4.1 : support "officiel" de Debian GNU/Linux 4 et 5 (32 et 64 bits)
* Version 5.0
* Version 6.0
* Version 7.0

VMware ESXi/vSphere est gratuit dans sa version de base, et l'on peut s'affranchir de ses limitations via différentes licences.

### Limitations de VMware ESXi/vSphere

Dans la version 4.1 gratuite :

* Permet de gérer un seul ESXi dans une même fenêtre (et donc évidemment pas de Vmotion = migration à chaud)
* Pas de limite de mémoire physique
* Limite de 6 coeurs pour un processeur physique (?)
* Limite de 4 coeurs pour une machine virtuelle (?)
* Pas d'accès à l'API Vmware (?) et donc Veam Backup inutilisable (?)
* Pas de possibilité de cloner à chaud une machine (?)

Dans la version 5.0 gratuite :

* Permet de gérer un seul ESXi dans une même fenêtre ;
* Limité à 32 Go de mémoire physique par CPU ;
* Limité à 8 vCPU pour une machine virtuelle.

Dans les version 5.0 payantes :

* Il faut avoir autant de licences que de processeurs physiques

<http://www.vmware.com/products/vsphere/buy/editions_comparison.html>

<http://www.vmware.com/products/vsphere/pricing.html>


## SSH

Pour activer l'accès à la console virtuelle de ESX en SSH, il suffit de s'authentifier sur la machine physique (touche F2) et d'aller dans le menu Troubleshooting Options, puis d'activer SSH.
Il est également possible d'activer SSH depuis vphere dans configuration / profil de sécurité

## VMDK

Les fichiers VMDK sont des disques virtuels pour VMware.

<http://sanbarrow.com/vmdk-handbook.html>

Note préalable : pour naviguer dans un "datastore" il est préférable d'utiliser WinSCP plutôt que le "Navigateur de datastore" intégré qui ne nous montre pas exactement la réalité.

Par exemple, avec une machine virtuelle Linux sur un ESXi 4.1, on a :

* Un fichier foo.vmdk (de moins d'1 Ko) contenant des infos sur le disque virtuel :

~~~
# Disk DescriptorFile
version=1
encoding="UTF-8"
CID=28c888c9
parentCID=ffffffff
isNativeSnapshot="no"
createType="vmfs"

# Extent description
RW 167772160 VMFS "foo-flat.vmdk"

# The Disk Data Base 
#DDB

ddb.adapterType = "lsilogic"
ddb.geometry.sectors = "63"
ddb.geometry.heads = "255"
ddb.geometry.cylinders = "10443"
ddb.uuid = "60 00 C2 9f 1c 11 be 03-20 bd 8d c6 d7 c6 47 e6"
ddb.longContentID = "458235d4353cf09526fd53f628c888c9"
ddb.virtualHWVersion = "7"
~~~

* Un fichier foo-flat.vmdk d'une taille de 80 Go.

Lors d'une copie d'un disque virtuel, il faut copier ces 2 fichiers.

## Performances

Quelques "astuces" :

* Attention, dans vSphere les graphes de performance disque sont notés en Kbps MAIS il s'agit en fait de Ko/s !!
* Les performances en utilisant SCP via SSH sont déplorables (environ 9 Mo/s). Via <http://www.veeam.com/vmware-esxi-fastscp.html>

## Réseau

* Les adresses MAC des interfaces réseau sont générées automatiquement. ATTENTION, un mode manuel est possible (et même conseillé si l'on souhaite basculer de machine sans utiliser Vmotion) MAIS l'on devra mettre dès le départ une adresse MAC manuellement pour pouvoir la repréciser sur un autre ESXi. En effet, les plages d'adresses MAC automatiques et manuelles sont différentes. 
* Les cartes réseau Intel ne sont parfois pas reconnues par défaut (!!). Il faut installer un driver. Pour cela, on récupère le modèle de la carte via `lspci` + `lspci -n` et le site <http://pci-ids.ucw.cz>
   Ensuite, il faut aller télécharger le driver sur <http://downloads.vmware.com/d/#drivers_tools> on récupère alors un driver au format .zip
   On doit enstuie passer l'ESXi en mode maintenance puis exécuter en SSH :

~~~
# esxupdate --bundle=INT-intel-lad-ddk-igb-3.1.17-offline_bundle-452075.zip update
Encountered error MaintenanceModeError:
The error data is:
   Message     - The following VIBs require this host to be in maintenance mode:
                 cross_vmware-esx-drivers-net-igb_400.3.1.17-1vmw.2.17.249663.
                 Please put the host in maintenance mode to proceed.
   Errno       - 18
   Description - Maintenance mode is not enabled or could not be determined.
 # esxupdate --bundle=INT-intel-lad-ddk-igb-3.1.17-offline_bundle-452075.zip update
Unpacking cross_vmware-esx-drivers-net-igb_400.3.1.1.. ########################################################################### [100%]

Removing packages :vmware-esx-drivers-net-igb          ########################################################################### [100%]

Installing packages :cross_vmware-esx-drivers-net-ig.. ########################################################################### [100%]

Running [/usr/sbin/vmkmod-install.sh]...
ok.
The update completed successfully, but the system needs to be rebooted for the
changes to be effective.
~~~

## Mises-à-jour

Par exemple, il faut télécharger un fichier .zip : update-from-esx4.1-4.1_update02.zip

Puis l'appliquer via SSH (en mode maintenance), puis rebooter :

~~~
# esxupdate --bundle=update-from-esx4.1-4.1_update02.zip update
Unpacking cross_qlogic-fchba-provider_410.1.3.7-454520 ########################################################################### [100%]

Unpacking cross_oem-vmware-esx-drivers-scsi-3w-9xxx_.. ########################################################################### [100%]

Unpacking cross_oem-vmware-esx-drivers-net-vxge_400... ########################################################################### [100%]

Unpacking cross_vmwprovider_4x.1.0.1-2.11.502767       ########################################################################### [100%]

Unpacking cross_swmgmt_4x.1.0.1-1.4.348481             ########################################################################### [100%]

Unpacking cross_kmodule_4x.1.0.1-1.4.348481            ########################################################################### [100%]

Unpacking cross_omc_1.1.0-2.11                         ########################################################################### [100%]

Unpacking cross_hdr_4x.1.0.1-1.4.348481                ########################################################################### [100%]

Removing packages :qlogic-fchba-provider               ########################################################################### [100%]

Installing packages :cross_oem-vmware-esx-drivers-sc.. ########################################################################### [100%]

Installing packages :cross_qlogic-fchba-provider_410.. ########################################################################### [100%]

Running [cim-install.sh]...
ok.
Running [vmkmod-install.sh]...
ok.
Running [/sbin/esxcfg-secpolicy -p /etc/vmware/secpolicy]...
ok.
The update completed successfully, but the system needs to be rebooted for the changes to be effective.
~~~

## iSCSI

### Configuration réseau pour iSCSI

Doc officielle pour ESXi 4.1 : <http://www.vmware.com/pdf/vsphere4/r41/vsp_41_iscsi_san_cfg.pdf>

Pour ajouter un stockage iSCSI via des cartes réseau dédiés, il faut configurer ces interfaces réseau.

Via vSphere, cela se fait via : _Configuration> Mise en réseau> Ajouter gestion réseau..._

On ajoute une connexion VMkernel, on sélectionne la bonne carte (vmnicN) et l'on entre son adresse IP / masque.

Si l'on a plusieurs interfaces dédiées pour l'iSCSI, on les ajoutera dans des vSwitchs séparés (ce n'est pas obligé
mais c'est plus propre...), et on aura donc autant de Ports VMKernel (interfaces vmkN) qu'il y a de cartes réseau.

Des manipulations sont apparemment nécessaires en CLI, par exemple pour 2 cartes réseau dédiées au iSCSI, on créera
deux vSwitchs vides (chacun associé à une carte physique), puis :

~~~
# esxcfg-vswitch -A "iSCSI1" vSwitch1
# esxcfg-vswitch -A "iSCSI2" vSwitch2
# esxcfg-vmknic -a "iSCSI1" -i 192.0.43.1 -n 255.255.255.0 -m <MTU>
# esxcfg-vmknic -a "iSCSI2" -i 192.0.43.2 -n 255.255.255.0 -m <MTU>
~~~

Avec MTU = 1500 ou 9000 si l'infra supporte les Jumbo Frames. Attention,
concernant les Jumbo Frames, il faut vraiment que tous les équipements
réseau de niveau 2 le supporte, à savoir le SAN, le switch, etc.

À voir si nécessaire mais on peut aussi passer le MTU des vSwitchs à 9000 :

~~~
# esxcfg-vswitch -m 9000 vSwitchN
# esxcfg-vswitch -l
~~~

Enfin, dans le cas où l'on passe par de l'iSCSI 100% logiciel via VMware,
on aura donc une interface (adaptateur de stockage) vmhbaNN.
En CLI, on doit apparemment ajouter explicitement les interfaces ainsi :

~~~
# esxcli swiscsi nic add -n vmk1 -d vmhbaNN
# esxcli swiscsi nic add -n vmk2 -d vmhbaNN
# esxcli swiscsi nic list -d vmhbaNN
~~~

Dans le cas d'iSCSI géré en partie par la carte (iSCSI matériel ou "offload"),
on verra a priori plusieurs interfaces vmhbaNN.

Enfin, une fois tout en place, on pourra valider les "sessions" iSCSI via :

~~~
# esxcli swiscsi session list -d vmhba34
~~~

### Ajouter un disque iSCSI

Aller dans ''Configuration> Adaptateurs de stockage> iSCSI Software Adapter" et sélectionner le vmhbaNN correspondant.

Cliquer sur _Propriétés..._ puis l'onglet _Découverte dynamique_. On peut ainsi _Ajouter..._ un serveur cible,
en entrant son adresse IP : il devrait ainsi découvrir tous les périphériques iSCSI annoncés par le SAN.

Si plusieurs chemins sont disponibles vers les périphériques, on aura accès au menu _Gérer les chemins..._
permettant de choisir le type de sélection de chemin : Fixed (Fixe), Most Recently Used (récemment utilisé) ou Round Robin (circulaire).

~~~
Fixed (VMware) The host always uses the preferred path to the disk when that path is available.
If the host cannot access the disk through the preferred path, it tries the
alternative paths. The default policy for active-active storage devices is Fixed.
Most Recently Used (VMware)
The host uses a path to the disk until the path becomes unavailable. When the
path becomes unavailable, the host selects one of the alternative paths. The host
does not revert back to the original path when that path becomes available
again. There is no preferred path setting with the MRU policy. MRU is the
default policy for active-passive storage devices and is required for those
devices.
Round Robin (VMware) The host uses an automatic path selection algorithm rotating through all
available paths. This implements load balancing across all the available
physical paths.
Load balancing is the process of spreading server I/O requests across all
available host paths. The goal is to optimize performance in terms of
throughput (I/O per second, megabytes per second, or response times).
~~~

Le meilleur choix en général est Round Robin (circulaire).

Enfin, dans _Configuration> Stockage_ on ira sur _Ajouter stockage..._ puis l'on configurera le stockage,
à savoir _Disque/LUN_, choix du périphérique, etc. On aura ainsi un Datastore (Banque de données) utilisable.

### Activer vMotion

La fonctionnalité vMotion permet notamment de déplacer des VM d'un hyperviseur à l'autre sans downtime.

Afin d'activer vMotion, depuis le client vsphere :

1 - Cliquer sur l'hôte concerné[[BR]]
2 - Cliquer sur l'onglet configuration[[BR]]
3 - Dans la catégorie "Matériel", cliquer sur "Mise en réseau"[[BR]]
4 - Cliquer sur Propriétés pour le vSwitch où un VMkernel a été configuré[[BR]]
5 - Dans la boîte de dialogue, séléctionner le vmkernel dans l'onglet "Ports" et cliquer sur "Modifier"[[BR]]
6 - Cocher la case vMotion et cliquer sur "OK"[[BR]]

## SCSI

Si un diqsque SCSI géré par VMware a été étendu, pour que l'hôte Linux puisse precevoir son agrandissement, sans avoir a le redémarrer, on peux forcer le scan de ce périphérique avec les commandes suivantes :

~~~
# ls /sys/class/scsi_device/
# echo 1 > /sys/class/scsi_device/0\:0\:0\:0/device/rescan
#
# #On peut aussi utiliser son identifiant sdX
# echo 1 > /sys/class/block/sdb/device/rescan   
~~~

## Vmware CLI

<https://pubs.vmware.com/vsphere-50/index.jsp?topic=%2Fcom.vmware.vcli.ref.doc_50%2Fvcli-right.html>

Accessible via connexion SSH sur le serveur.

Connaître la version de VMware :

~~~
~ # vmware -vl
VMware ESXi 5.0.0 build-469512
VMware ESXi 5.0.0 GA
~~~


Lister les machines virtuelles :

~~~
~ # esxcli vm process list
~~~

## Installation des VMwareTools

Note : à partir de VMWare 6.0 et/ou Debian 7, il est recommendé d'installer le paquet Debian open-vm-tools (présent dans les dépôts officiels).

Se connecter à VSPHERE et sélectionner la machine cible, puis clic droit, et "installer/Mettre à niveau les outils VMware"

Installer les dépendances suivantes :

~~~
# aptitude install build-essential gcc-4.3 make linux-headers-`uname -r`
~~~

Puis, en SSH sur la machine :

~~~
# mount /dev/cdrom /mnt
# cd /tmp
# tar xvf /mnt/VMwareTools-4.0.0-256968.tar.gz
# ./vmware-tools-distrib/vmware-install.pl

Creating a new VMware Tools installer database using the tar4 format.

Installing VMware Tools.

In which directory do you want to install the binary files? 
[/usr/bin] 

What is the directory that contains the init directories (rc0.d/ to rc6.d/)? 
[/etc] 

What is the directory that contains the init scripts? 
[/etc/init.d] 

In which directory do you want to install the daemon files? 
[/usr/sbin] 

In which directory do you want to install the library files? 
[/usr/lib/vmware-tools] 

The path "/usr/lib/vmware-tools" does not exist currently. This program is 
going to create it, including needed parent directories. Is this what you want?
[yes] 

In which directory do you want to install the documentation files? 
[/usr/share/doc/vmware-tools] 

The path "/usr/share/doc/vmware-tools" does not exist currently. This program 
is going to create it, including needed parent directories. Is this what you 
want? [yes] 

The installation of VMware Tools 4.0.0 build-256968 for Linux completed 
successfully. You can decide to remove this software from your system at any 
time by invoking the following command: "/usr/bin/vmware-uninstall-tools.pl".

Before running VMware Tools for the first time, you need to configure it by 
invoking the following command: "/usr/bin/vmware-config-tools.pl". Do you want 
this program to invoke the command for you now? [yes] 

Initializing...



Stopping VMware Tools services in the virtual machine:
   Guest operating system daemon:                                      done
   Unmounting HGFS shares:                                             done
   Guest filesystem driver:                                            done
 Adding system startup for /etc/init.d/vmware-tools ...
   /etc/rc0.d/S36vmware-tools -> ../init.d/vmware-tools
   /etc/rc6.d/S36vmware-tools -> ../init.d/vmware-tools
   /etc/rcS.d/S38vmware-tools -> ../init.d/vmware-tools
The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - vmmemctl module loads perfectly into
the running kernel.

Could not create the '/mnt/hgfs' directory.
The filesystem driver (vmhgfs module) is used only for the shared folder 
feature. The rest of the software provided by VMware Tools is designed to work 
independently of this feature.

If you wish to have the shared folders feature, you can install the driver by 
running vmware-config-tools.pl again after making sure that gcc, binutils, make
and the kernel sources for your running kernel are installed on your machine. 
These packages are available on your distribution's installation CD.
[ Press Enter key to continue ] 

The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - vmxnet module loads perfectly into 
the running kernel.

The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - vmblock module loads perfectly into 
the running kernel.

The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - vmci module loads perfectly into the
running kernel.

The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - vsock module loads perfectly into 
the running kernel.

The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - vmxnet3 module loads perfectly into 
the running kernel.

The bld-2.6.26-2-x86_64amd64-Debian5.0.1 - pvscsi module loads perfectly into 
the running kernel.

No X install found.

Creating a new initrd boot image for the kernel.
update-initramfs: Generating /boot/initrd.img-2.6.26-2-amd64
   Checking acpi hot plug                                              done
Starting VMware Tools services in the virtual machine:
   Switching to guest configuration:                                   done
   Paravirtual SCSI module:                                            done
   Guest memory manager:                                               done
   VM communication interface:                                         done
   VM communication interface socket family:                           done
   Guest operating system daemon:                                      done

The configuration of VMware Tools 4.0.0 build-256968 for Linux for this running
kernel completed successfully.

You must restart your X session before any mouse or graphics changes take 
effect.

You can now run VMware Tools by invoking the following command: 
"/usr/bin/vmware-toolbox" during an X server session.

To enable advanced X features (e.g., guest resolution fit, drag and drop, and 
file and text copy/paste), you will need to do one (or more) of the following:
1. Manually start /usr/bin/vmware-user
2. Log out and log back into your desktop session; and,
3. Restart your X session.

Enjoy,

--the VMware team

Found VMware Tools CDROM mounted at /mnt. Ejecting device /dev/hda ...

# reboot
~~~

## vCLI VMware vSphere CLI

C'est un outil qui permet d'administrer un ESX en CLI.

### Installation

Le télécharger sur le site de VMware : <http://communities.vmware.com/community/vmtn/server/vsphere/automationtools/vsphere_cli>

Décompresser l'archive tar.gz, créer un fichier /etc/tmp-release avec à l'intérieur « ubuntu » pour faire croire que l'on est sur une Ubuntu. (L'installeur ne gère que Red Hat, Suse et Ubuntu).

~~~
$ tar zxvf vmware[...].tar.gz
$ cd vmware-vsphere-cli-distrib
# echo ubuntu > /tmp/temp-release
# sudo aptitude install libarchive-zip-perl libclass-methodmaker-perl libdata-dump-perl
$ sudo ./vmware-install.pl 
~~~

Si erreur du type :

~~~
<http_proxy> not set. please set environment variable '<http_proxy'> e.g. export 
<http_proxy=http://myproxy.mydomain.com:0000> . 

ftp_proxy not set. please set environment variable 'ftp_proxy' e.g. export 
ftp_proxy=<http://myproxy.mydomain.com:0000> 
~~~

Modifier dans le script

~~~
       if ( !( $ftpproxy && $<httpproxy))> {
          uninstall_file($gInstallerMainDB);
          exit 1;
       }
~~~

par

~~~
       if ( !( $ftpproxy && $<httpproxy))> {
          #uninstall_file($gInstallerMainDB);
          #exit 1;
       }
~~~

### Configuration

Le but est de créer un fichier de configuration pour s'authentifier sans passer à chaque fois l'adresse IP, le login et le mot de passe. Pour cela il suffit de créer un fichier texte avec les informations suivantes :

~~~
VI_SERVER = 192.168.0.10
VI_USERNAME = root
VI_PASSWORD = UberPass
VI_PROTOCOL = <https>
VI_PORTNUMBER = 443
~~~

Ensuite on pourra spécifier ce fichier lors de l'exécution des commandes, comme esxcli.

~~~
esxcli --config vmware.cfg network ip interface list
~~~

### top / stats


Stats I/O (iotop like) : 

~~~
TERM=xterm esxtop
puis v
puis L 36 Entrée
~~~


### Étendre une partition VMFS

1. Récupérez l’ID et secteurs de la partition VMFS. Ici, c’est l’ID 3, début au secteur 10229760 et fin au secteur 860050190.


~~~
$ partedUtil getptbl /vmfs/devices/disks/vml.0200010000600508b1001c504c1f73b6b6947016164c4f47494341
gpt
 109406 255 63 1757614684
 1 64 8191 C12A7328F81F11D2BA4B00A0C93EC93B systemPartition 128
 5 8224 520191 EBD0A0A2B9E5443387C068B6B72699C7 linuxNative 0
 6 520224 1032191 EBD0A0A2B9E5443387C068B6B72699C7 linuxNative 0
 7 1032224 1257471 9D27538040AD11DBBF97000C2911D1B8 vmkDiagnostic 0
 8 1257504 1843199 EBD0A0A2B9E5443387C068B6B72699C7 linuxNative 0
 2 1843200 10229759 EBD0A0A2B9E5443387C068B6B72699C7 linuxNative 0
 3 10229760 860050190 AA31E02A400F11DB9590000C2911D1B8 vmfs 0
~~~


2. Récupérez le dernier secteur du disque dur, c’est en effet à cette dernière adresse que l’on va étendre notre partition.

~~~

$ partedUtil getUsableSectors /vmfs/devices/disks/vm.0200010000600508b1001c504c1f73b6b6947016164c4f47494341
1757614650
~~~


3. Redimensionner la partition (ID 3, secteur 10229760 à 1757614650).


~~~
partedUtil resize /vmfs/devices/disks/vml.0200010000600508b1001c504c1f73b6b6947016164c4f47494341 3 10229760 1757614650
~~~


4. Étendre le système de fichier (partition d’ID 3).


~~~
vmkfstools –growfs /vmfs/devices/disks/vml.0200010000600508b1001c504c1f73b6b6947016164c4f47494341:3 /vmfs/devices/disks/vml.0200010000600508b1001c504c1f73b6b6947016164c4f47494341:3
~~~


Et voilà, et en plus c’est à chaud, il suffira de cliquer sur ré-analyser dans le gestionnaire de disques (de vSphere) pour voir apparaître la nouvelle taille !

### Liste des commandes

<http://pubs.vmware.com/vsphere-50/topic/com.vmware.ICbase/PDF/vsphere-esxi-vcenter-server-50-command-line-interface-getting-started-guide.pdf>

## Vcenter server appliance

Il s'agit d'une VM permettant l'administration d'un ou plusieurs hyperviseurs. On peut ainsi gérer un cluster de serveurs VMware.

Cette VM s'installe de la façon suivante :

Télécharger : <https://my.vmware.com/web/vmware/details?downloadGroup=VC60U2&productId=491&rPId=11406>

1. Monter ou extraire le fichier ISO

2. Ouvrir le dossier vcsa et installer le plugin

3. À la racine ouvrir le fichier vcsa-setup.html

4. Activer le plug-in au sein du navigateur et cliquer sur Install.

5. Continuer le processus d'installation

Exemple de configuration :

~~~
Hostname : vcenter
root / foo
SSO : administrator / bar
nom de domaine SSO : vsphere.local
nom de site SSO : default
nom du système : vcenter.example.com
IP : 192.168.0.1
SSH activé
~~~


Résumé d'installation


~~~
Informations du serveur cible :     192.168.0.1
Nom :     vcenter
Type d'installation :     Installer
Type de déploiement :     Instance intégrée de Platform Services Controller
Configuration du déploiement :     Minuscule (jusqu'à 10 hôtes, 100 machines virtuelles)
Banque de données :     datastore1
Mode Disque :     thick
Mappage de réseau :     Network 1 to VM Network
Allocation d'IP :     IPv4 , static
Nom d'hôte     
Synchronisation de l'heure :     ntp.evolix.net
Base de données :     embedded
Propriétés :     
SSH activé = True
Programme d'amélioration du produit = Désactivé
Nom d'utilisateur SSO = administrator
Nom de domaine SSO = vsphere.local
Nom du site SSO = default
Adresse IP du réseau 1 = 192.168.0.1
Nom d'hôte = vcenter.example.com
Masque de réseau du réseau 1 = 255.255.255.0
Passerelle par défaut = 192.168.0.1
DNS = 192.168.0.254
~~~

Une fois l'installation terminé, pour accéder au portail vcenter : vcenter.example.com

Identifiants : administrator@vsphere.local / bar

Une fois sur le portail, pour ajouter des hôtes vmware, aller dans « Hôtes et Clusters » puis cliquer sur action et nouveau centre de données.

Une fois le centre de données crée cliquer dessus puis sur action et Ajouter hôte, autant de fois que nécéssaire.

On peut ensuite créer un cluster en cliquant sur action puis Nouveau cluster.

## Direct Ressource Scheduler (DRS)

Disponible à partir de VMware vSphere 6.7, le DRS est la méthode par laquelle VMware permet l'automatisation de la répartition des ressources entre plusieurs hyperviseurs, dans le but d'équilibrer la charge sur ces hyperviseurs. Il est défini par cluster vSphere et peut avoir 3 modes (assumant qu'il soit actif):

* automatique : les VM sont réparties automatiquement au démarrage et peuvent être migrées automatiquement entre hyperviseurs pour répartir la charge,
* semi-automatique : les VM sont réparties automatiquement au démarrage. Si VMware pense qu'une migration devrait être faite pour répartir la charge, une recommandation est donné à l'utilisateur qui peut décider de l'appliquer ou non,
* manuelle : VMware ne fait que donner des recommandations de répartition de charge.

Si le DRS est activé globalement, il est possible de le désactiver pour certaines VM uniquement (ou inversement : le désactiver partout sauf pour certaines) :

* Depuis le cluster, onglet "Configure", sous "Configuration", cliquer sur "VM Overrides" > "Add..."
* Sélectionner les VM pour lesquelles on veut appliquer une modification particulière
* Sélectionner l'option à modifier pour ces VM : cocher Override pour "DRS automation level", et choisir le niveau voulu
* Confirmer

### Définition de règle d'affinités VM/Hyperviseur

Il est possible de définir des affinités entre VMs et hyperviseurs pour dire à VMware de préférer faire tourner un groupe de VM sur un groupe d'hyperviseurs ou d'éviter de faire tourner un groupe de VM sur un groupe d'hyperviseurs. Pour ce faire il faut :

* Définir un groupe de VM:
    * Depuis le cluster, onglet "Configure", sous "Configuration", cliquer sur "VM/Host Group" > "Add..."
    * Donner un nom à la règle et sélectionner le type "VM Group"
    * Ajouter les membres du groupe en cliquant sur "Add..." et en sélectionnant les machines voulues
    * Confirmer
* Définir un groupe d'hyperviseurs:
    * Depuis le cluster, onglet "Configure", sous "Configuration", cliquer sur "VM/Host Group" > "Add..."
    * Donner un nom au groupe et sélectionner le type "Host Group"
    * Ajouter les membres du groupe en cliquant sur "Add..." et en sélectionnant les hyperviseurs voulus
    * Confirmer
* Définir la règle d'affinité
    * Depuis le cluster, onglet "Configure", sous "Configuration", cliquer sur "VM/Host Rules" > "Add..."
    * Donner un nom à la règle et sélectionner le type "Virtual Machines to Hosts"
    * Sélectionner les groupes de VM et d'hyperviseurs sur lesquels appliquer la règle
    * Sélectionner le type de règle (le type décrit ce à quoi il correspond)
    * Confirmer

### Définition de règles d'affinités VM/VM

Il est possible de définir des affinités entre VMs pour dire à VMware qu'elles devraient tourner sur des hyperviseurs différent (autant que possible) ou sur le même hyperviseur. Pour ce faire il faut :

* Depuis le cluster, onglet "Configure", sous "Configuration", cliquer sur "VM/Host Rules" > "Add..."
* Donner un nom à la règle
* Sélectionner le type correspondant à ce qui est voulu entre "Keep Virtual Machines Together" (faire tourner sur un même hyperviseur) et "Separate Virtual Machines" (faire tourner sur des hyperviseurs différents)
* Ajouter les VMs auxquelles la règle s'applique en cliquant sur "Add..." et en sélectionnant les machines voulues
* Confirmer

## Storage DRS

Disponible à partir de VMware vSphere 6.7, le SDRS est la méthode par laquelle VMware permet l'automatisation de la répartition des ressources entre plusieurs Datastores, dans le but d'équilibrer l'espace disponible. Pour l'utiliser il faut définir un "Datastore Cluster" (c'est fait au niveau du datacenter) avec les datastores sur lesquels répartir la charge. Il est possible de choisir les seuils de migration et si les migrations devraient aussi se faire pour des questions de performance.

Le SDRS peut avoir deux modes :

* Automatique : VMware fait la répartition automatiquement
* Manuelle : VMware donne des recommandations pouvant être appliquées

> Note : Ajouter des datastores déjà utilisés peut ajouter des Overrides qu'il peut être nécessaire de supprimer (depuis le du "Datastore cluster", dans l'onglet "Configure", sous "Configuration", cliquer sur "VM Overrides")

> Note : Si des disques sont dans le mode "Independent - Persistent" ou "Independent - Non-Persistent" alors SDRS ne peut pas fonctionner pour la machine.

### Définir des règles d'affinité entre VM

Il est possible de dire à VMware qu'un groupe de VM devrait avoir leurs données dans des datastore différents autant que possible. Pour ce faire il faut :

* Depuis le "datastore Cluster", onglet "Configure", sous "Configuration", cliquer sur "Rules" > "Add..."
* Donner un nom à la règle et sélectionner le type "VM anti-affinity"
* Sélectionner les VM sur lesquelles la règle s'applique en cliquant sur "Add..." et en sélectionnant les VM voulues
* Confirmer 

# FAQ

## `This operation would violate a virtual machine affinity/anti-affinity rule.`

Lors d'un (re)démarrage d'une VM il se peut que vous obteniez un message d'erreur du type « This operation would violate a virtual machine affinity/anti » et que le machine ne démarre pas. 

Ça signifie qu'une règle d'affinité empèche son démarrage. Par exemple si vous avez un groupe de 3 VM qui on comme rêgle de chacune tourner sur des hôtes différents mais que vous avez seulement 2 hôte alors vsphere vous empéchera de demarer une 3eme VM car tous les hyperviseurs ont déjà une VM de ce groupe en cours d'execution.

Dans ce cas là on peut temporairement désactiver la règle d'affinité pour que vsphere nous laisse démarrer la machine. Il est possible de rétablir la règle d'affinité, une fois la machine lancée, sans générer d'erreur ou de message supplémentaire.

Pour se faire on clic sur :

1. Le cluster où se trouve la machine puis : “Configure” > “Configuration“ > “VM/Host Rules”
2. On selectione la règle qui pose problème puis : “Edit...”
3. Dans la popup on décoche “Enable rule.”

On foit désactiver vous pourrez démarrer la VM en question.