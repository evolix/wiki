**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Mercurial

<http://mercurial.selenic.com/guide/>

Sous Debian, pour installer Mercurial :

~~~
# aptitude install mercurial
~~~

## Utilisation

Configurer son client :

~~~
$ cat ~/.hgrc
[ui]
username = John Doe <jdoe@example.com>
verbose = True
~~~

Créer un dépôt :

~~~
$ mkdir -p foo && cd foo
$ hg init
~~~

Cloner un dépôt :

~~~
$ hg clone <http://hg.example.com/repo1>
destination directory: repo1
requesting all changes
adding changesets
adding manifests
adding file changes
added 40 changesets with 85 changes to 19 files
updating working directory
15 files updated, 0 files merged, 0 files removed, 0 files unresolved
~~~

Ajouter un nouveau fichier au dépôt :

~~~
$ hg add monfichier
~~~

Valider les modifications apportées au dépôt :

~~~
$ hg commit
Mon message de commit
HG: Enter commit message.  Lines beginning with 'HG:' are removed.
HG: --
HG: user: user@host
HG: branch 'default'
HG: changed monfichier
~~~

Envoyer les modifications sur le dépôt :

~~~
$ hg push
pushing to <http://hg.example.com/repo1>
http authorization required
realm: Password protected
user: user@host
password:
searching for changes
adding changesets
adding manifests
adding file changes
added 1 changesets with 1 changes to 1 files
~~~

Récupérer la dernière version d'un dépôt :

~~~
$ hg pull
searching for changes
no changes found
~~~

Placer le _working directory_ dans une version particulière :

~~~
$ hg update 19
9 files updated, 0 files merged, 4 files removed, 0 files unresolved
~~~

## HGWEB

HGWEB sert à naviguer dans les dépôts par une interface web, mais surtout
à utiliser Mercurial via HTTP.

~~~
# aptitude install libapache2-mod-wsgi
# mkdir /home/hg
# cp /usr/share/doc/mercurial-common/examples/hgweb.wsgi /home/hg/
# sed -i 's@/path/to/repo/or/config@/home/hg/hgweb.config@' /home/hg/hgweb.wsgi

# cat /home/hg/hgweb.config
[paths]
test = /home/hg/test
coin = /home/hg/coin

[web]
#allow_push = *
baseurl = /
push_ssl = false
# chown -R www-data:www-data /home/hg
~~~

Il reste maintenant à activer un VirtualHost.
Par exemple avec un authentification LDAP :

~~~{.apache}
<VirtualHost *:80>

  ServerName hg.example.com
  DocumentRoot /home/hg/www

  #WSGIDaemonProcess hg user=hg group=hg processes=2 maximum-requests=5000
  WSGIScriptAlias / /home/hg/hgweb.wsgi

 <Directory />
    Require all granted
    Options ExecCGI FollowSymlinks
  </Directory>

  <Location />
      AuthType Basic
      AuthName "Reserved access"
      AuthBasicProvider ldap
      AuthzLDAPAuthoritative on
      AuthLDAPURL "ldap://ldap.example.com:389/ou=people,dc=example,dc=com?uid?sub?(objectClass=*)"
      Require ldap-group cn=hg,ou=group,dc=example,dc=com
      <LimitExcept GET>
          Require ldap-group cn=hg-push,ou=group,dc=example,dc=com
      </LimitExcept>
  </Location>

</VirtualHost>
~~~

Il reste à gérer les droits du répertoire /home/hg devant appartenir à l'utilisateur web
(par exemple via ACL ou Apache-ITK).

## FAQ

* Lorsque je push en SSH, j'obtiens :

~~~
remote: abandon : There is no Mercurial repository here (.hg not found) !
abandon : no suitable response from remote hg !
~~~

Il faut mettre un double slash (//) après le hostname. Voir notamment <http://jehaisleprintemps.net/blog/fr/2009/05/10/mercurial-et-ssh-le-piege-eviter/>

* Lorsque je push en HTTP, j'obtiens :

~~~
ssl required
~~~

Il faut passer en HTTPS. Si vraiment, on veut le faire en HTTP non sécurisé, il faut le forcer au niveau du serveur via _push_ssl = false_
Voir <http://mercurial.selenic.com/wiki/FAQ#FAQ.2FCommonProblems.I_get_an_.22ssl_required.22_error_message_when_trying_to_push_changes>
