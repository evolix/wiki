**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Oracle Database 11Gr2

## Télécharger Oracle DB

Rendez-vous sur le site d'[Oracle](http://www.oracle.com/technetwork/database/enterprise-edition/downloads/index.html). Vous devez avoir un compte, une fois connecté, récupérez l'adresse du lien qui contient un token, et vous pouvez le télécharger avec wget en copiant/collant l'URL, exemple de lien (plus valable évidemment) : <http://download.oracle.com/otn/linux/oracle11g/R2/linux.x64_11gR2_database_2of2.zip?AuthParam=1323247397_1e374bb8429455952fa3a9acf468afc4>

## Installer les pré-requis

Pour Stretch, mettre dans son /etc/apt/sources.list :

~~~
deb [arch=amd64,i386] http://mirror.evolix.org/debian/ stretch main
deb [arch=amd64,i386] http://mirror.evolix.org/debian/ stretch-updates main
deb [arch=amd64,i386] http://security.debian.org/debian-security stretch/updates main
~~~

Puis :

~~~
# dpkg --add-architecture i386
# aptitude update
# aptitude install build-essential gcc-multilib lib32z1 libaio1 libstdc++5 rpm xauth x11-utils
~~~


### Quelques hacks moches ...

~~~
# ln -s /usr/bin/awk /bin/awk
# ln -s /usr/bin/basename /bin/basename
# ln -s /usr/bin/rpm /bin/rpm
~~~

Sous Stretch, il faudra « linker » quelques librairies utilisées lors de l'installation pour compiler des outils Oracle.

~~~
# mkdir /usr/lib64
# chmod 755 /usr/lib64
# ln -s /usr/lib/x86_64-linux-gnu/libpthread_nonshared.a /usr/lib64/
# ln -s /usr/lib/x86_64-linux-gnu/libc_nonshared.a /usr/lib64/
# ln -s /lib/x86_64-linux-gnu/libgcc_s.so.1 /usr/lib64/
# ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6 /usr/lib64/
~~~

## Installation

/!\ Attention il faut au moins 1Go de libre sur /tmp et 15Go de swap (peut-être ignoré) ainsi que 13Go pour la base oracle ! /opt devrait donc faire plus de 15Go !

Il faut installer un compte Unix "oracle" :

~~~
# addgroup oracle
Adding group `oracle' (GID 1001) ...
Done.
# adduser --system --home /opt/oracle --ingroup oracle oracle
Adding system user `oracle' (UID 107) ...
Adding new user `oracle' (UID 107) with group `oracle' ...
Creating home directory `/opt/oracle' ...
# passwd oracle
~~~

L'installation d'Oracle DB se fait via un GUI, pour cela il faut activer le forwading X11 sur votre serveur SSH et autoriser le compte "oracle" à se connecter en SSH (par clé de préférence).
On peut ainsi se connecter en SSH ainsi (avec compression + algorithme de chiffrement léger histoire d'améliorer un peu la vitesse d'affichage des fenêtres) :

~~~
$ ssh -c arcfour,blowfish-cbc -YC oracle@server.example.com
~~~

Enfin on lance l'installeur : 

~~~
# /opt/oracle/install/runInstaller
~~~

Dans la partie pré-requis cela va échouer pour pratiquement tout ... « fixez » ce qui est possible en cliquant sur Fix ! On vous demandera alors d'exécuter un script : 

~~~
# bash /tmp/CVU_11.2.0.1.0_oracle/orarun.sh /tmp/CVU_11.2.0.1.0_oracle/fixup.response /tmp/CVU_11.2.0.1.0_oracle/fixup.enable /tmp/CVU_11.2.0.1.0_oracle
~~~

Cela va modifier sysctl avec les valeurs recommandés.

En Wheezy il a y des erreurs lors de la compilation des outils.

Si vous avez : 

~~~
/usr/bin/ld: note: 'B_DestroyKeyObject' is defined in DSO /u01/app/oracle/product/11.2.0/dbhome_1/lib/libnnz11.so so try adding it to the linker command line
/u01/app/oracle/product/11.2.0/dbhome_1/lib/libnnz11.so: could not read symbols: Invalid operation
collect2: error: ld returned 1 exit status
~~~

Il faut lancer le make à la main, récupérer la dernière ligne qui s'affiche et la corriger en ajoutant `-lnmemso -lcore11 -lnnz11`. Enfin cliquer sur « retry install ».

~~~
cd $ORACLE_HOME/sysman/lib
make -f ins_emagent.mk "agent"
~~~

Avec Oracle 12 si vous avez :

~~~
/usr/bin/ld: /opt/oracle/app/oracle/product/12.1.0/dbhome_1//rdbms/lib/houzi.o: undefined reference to symbol 'ztcsh'
/usr/bin/ld: note: 'ztcsh' is defined in DSO /opt/oracle/app/oracle/product/12.1.0/dbhome_1//lib/libnnz12.so so try adding it to the linker command line
/opt/oracle/app/oracle/product/12.1.0/dbhome_1//lib/libnnz12.so: could not read symbols: Invalid operation
collect2: error: ld returned 1 exit status
~~~

Il faut éditer le fichier /opt/oracle/app/oracle/product/12.1.0/dbhome_1//lib/ldflags et ajouter -lons

Si vous avez : 

~~~
/usr/bin/ld: /opt/oracle/app/oracle/product/12.1.0/dbhome_1//rdbms/lib/houzi.o: undefined reference to symbol 'ztcsh'
/usr/bin/ld: note: 'ztcsh' is defined in DSO /opt/oracle/app/oracle/product/12.1.0/dbhome_1//lib/libnnz12.so so try adding it to the linker command line
/opt/oracle/app/oracle/product/12.1.0/dbhome_1//lib/libnnz12.so: could not read symbols: Invalid operation
collect2: error: ld returned 1 exit status
~~~

Il faut éditer /opt/oracle/app/oracle/product/12.1.0/dbhome_1//lib/sysliblist et mettre -lnnz12

Puis relancer l'installation avec « Retry ».

L'installation devrait se finir, mais il reste encore des scripts à exécuter en root !

~~~
# bash /opt/oracle/app/oraInventory/orainstRoot.sh
# bash /opt/oracle/app/oracle/product/11.2.0/dbhome_1/root.sh
~~~

Et voilà !

Vous pouvez ensuite vous connectez en xxx ou avec la magnifique interface web du type : <https://mysuperserver:1158/em.> Pour se connecter en admin le login est SYS, le mot de passe est celui que vous avez renseigné pendant l'installation et il faut cliquer sur, « connect as SYSDBA ».

Rajouter cette ligne dans le .profile de l'utilisateur oracle :

~~~
ORACLE_HOME=/opt/oracle/app/oracle/product/11.2.0/dbhome_1
ORACLE_HOME_LISTNER=$ORACLE_HOME
export ORACLE_HOME
export ORACLE_HOME_LISTNER
PATH=/usr/sbin:/opt/oracle/app/oracle/product/11.2.0/dbhome_1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/X11R
6/bin; export PATH
export ORACLE_UNQNAME=oracle
export ORACLE_SID=oracle
~~~

## Gestion

Lancement de ??? :

~~~
sudo -u oracle lsnrctl start
~~~

Lancement du serveur d'application et de l'interface web :

~~~
sudo -u oracle emctl start dbconsole

~~~
Note : N'est plus dispo en Oracle 12. C'est EM Express à la placé, lancé par défaut sur le port 5500. <https://localhost:5500/em/login>

Lancement du SGBD :

~~~
sudo -u oracle dbstart
~~~

## Erreurs bizarre

~~~
sqlplus "/ as sysdba"

SQL*Plus: Release 11.2.0.1.0 Production on Mon Oct 15 18:01:54 2012

Copyright (c) 1982, 2009, Oracle.  All rights reserved.

ERROR:
ORA-01031: insufficient privileges

~~~

Assurez-vous d'avoir 

~~~
#define SS_DBA_GRP "dba"
#define SS_OPER_GRP "dba
~~~

dans le fichier $ORACLE_HOME/rdbms/lib/config.c et « relinkez » le binnaire oracle.

`$ORACLE_HOME/bin/relink all`

### dbstart ne démarre pas la DB

Essayez

~~~
sqlplus "/ as sysdba"
alter system register;
startup;
~~~

Vérifiez aussi que db_name soit bien mis dans $ORACLE_HOME/dbs/init.ora

### Mot de passe perdu ?

<http://rolfje.wordpress.com/2007/01/16/lost-oracle-sys-and-system-password/>


## Sources

<http://wiki.debian.org/DataBase/Oracle>
<http://howto.landure.fr/gnu-linux/debian-4-0-etch/installer-un-serveur-oracle-sur-debian-5-0-lenny>
<http://www.debian-administration.org/articles/656>

# Howto Oracle Database 12R2 sous Docker

## Avant propos

Oracle DB n'étant officiellement supporté sous Debian, pour nous en faciliter l'installation et la maintenance nous allons l'installer dans un conteneur Docker.

## Installer Docker

Avant de commencer nous allons avoir besoin de l'ensemble des outils docker sur la machine.

Vous pouvez vous référer à notre [HowtoDocker](https://wiki.evolix.org/HowtoDocker)

## Pré-requis

Récupérer le dépot Oracle docker-images sur Github :

~~~
wget https://github.com/oracle/docker-images/archive/master.zip
~~~

## Télécharger Oracle DB

Télécharger la version 12R2 via le site d'Oracle : [http://www.oracle.com/technetwork/database/enterprise-edition/downloads/index.html]()

## Installation

Décomprésser le dépot Oracle :

~~~
# unzip master.zip
~~~

Placer le fichier "linuxx64_12201_database.zip" correspondant à la version 12R2 que vous avez récupéré via le site d'Oracle dans le sous dossier docker-images-master/OracleDatabase/SingleInstance/dockerfiles/12.2.0.1/ du dépôt Oracle :

~~~
# mv linuxx64_12201_database.zip docker-images-master/OracleDatabase/SingleInstance/dockerfiles/12.2.0.1/
~~~

Lancer l'installation :

~~~
# ./buildDockerImage.sh 

Usage: buildDockerImage.sh -v [version] [-e | -s | -x] [-i] [-o] [Docker build option]
Builds a Docker Image for Oracle Database.
  
Parameters:
   -v: version to build
       Choose one of: 11.2.0.2  12.1.0.2  12.2.0.1  
   -e: creates image based on 'Enterprise Edition'
   -s: creates image based on 'Standard Edition 2'
   -x: creates image based on 'Express Edition'
   -i: ignores the MD5 checksums
   -o: passes on Docker build option

* select one edition only: -e, -s, or -x

LICENSE UPL 1.0

Copyright (c) 2014-2017 Oracle and/or its affiliates. All rights reserved.

# ./buildDockerImage.sh -v 12.2.0.1 -e
~~~

Une fois l'installation terminée, on peut lancer notre image docker :

~~~
# docker run --name oracle -p 1521:1521 -p 5500:5500 -v /path-to-your-persistent-oradata:/opt/oracle/oradata oracle/database:12.2.0.1-ee
~~~

--name oracle → On donne le nom de notre choix  
-p 1521:1521 → On redirige le port 1521 local vers le port 1521 du conteneur  
-p 5500:5500 → On redirige le port 5500 local vers le port 5500 du conteneur  
-v /path/to/your/persistent/oradata:/opt/oracle/oradata → On souhaite avoir une base de données persistente,  /opt/oracle/oradata du conteneur sera donc relié à notre dossier local /path/to/your/persistent/oradata

