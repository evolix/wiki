ii - « irc it » ou « irc improved » est un client IRC basé seulement aux lancement en ligne de commande et à la manipulation à partir de fichiers FIFO. C'est un projet *suckless*

# Installation

~~~
# apt install ii
~~~


# Utilisation

~~~
$ # ii -i <repertoire> -s <servername> -p <port> -k <environment> -n <nickname> -f <realname>
$ ii -i ~/.irc/ -s localhost -n evouser

~~~
Une fois lancé, le programme ne rend pas la main, donc on pourra préférer le mettre en tâche de fond.

Pour rejoindre un chan irc, il suffit d'indiquer la commande dans le fichier fifo `in` :

~~~
$ echo "/j #channel" > ~/.irc/localhost/in
~~~


Exemple d'utilisation :

~~~
$ ls ~/.irc/
localhost
$ ls -l ~/.irc/localhost/
prwx------ 1 evouser evolix    0 Feb 10 09:33 in
-rw-r--r-- 1 evouser evolix 4762 Feb 10 09:33 out
$ echo "/j #channel" > ~/.irc/localhost/in
$ ls -l ~/.irc/localhost
prwx------ 1 evouser evolix    0 Feb 10 09:33 in
-rw-r--r-- 1 evouser evolix 5132 Feb 10 09:36 out
drwx------ 2 evouser evolix 4096 Feb 10 09:36 #channel
$ ls -l ~/.irc/locahost/\#channel
prwx------ 1 evouser evolix  0 Feb 10 09:36 in
-rw-r--r-- 1 evouser evolix 74 Feb 10 09:36 out
$ echo "coucou je parle grâce à ii !" > ~/.irc/localhost/\#channel/in
$ tail -f ~/.irc/localhost/\#channel/out
2017-02-10 09:37 -!- evouser(~evouser@evouser.evolix.net) has joined #channel
2017-02-10 09:39 <evouser> coucou je parle grâce à ii !
~~~


# Astuce

## Modification de out en FIFO

Par défaut out est un fichier texte. Pour savoir lorsque nouveaux messages et être certains de récupérer la nouvelle ligne (dans script - pour un bot par exemple), il faut rendre le fichier 'out' en un fichier FIFO => donc pas de logs possible.

~~~{.bash}
$ rm "~/.irc/localhost/\#channel/out"
$ mkfifo "~/.irc/localhost/\#channel/out"
$ cat "~/.irc/localhost/\#channel/out"
2017-02-10 11:50 <user> evouser: coucou evouser !
$
~~~

## irc avec mot de passe

Pour se connecter à un serveur IRC qui nécessite mot de passe, il faut passer le mot de passe en variable d'environnement (pour que le mot de passe ne soit pas visible par un autre utilisateur connecté sur la machine).

~~~
$ IIPASS='motdepasse' ii -i ~/.irc/ -s localhost -k IIPASS -n evouser
~~~

# Le petit frère sic

Avec **sic**, pas de répertoire et de hiérarchie de server/#channel/fichier, tout se passe avec STDIN et STDOUT, donc très pratique pour une automatisation simplifié dans un script ou pour quelqu'un adepte de minimalisme :

~~~
$ sic -h localhost -p 6667 -n evouser 
localhost: 03/16/17 11:58 >< NOTICE (*): *** Looking up your hostname
localhost: 03/16/17 11:58 >< NOTICE (*): *** Checking Ident
localhost: 03/16/17 11:58 >< NOTICE (*): *** No Ident response
localhost: 03/16/17 11:58 >< NOTICE (*): *** Found your hostname
localhost: 03/16/17 11:58 >< 001 (evouser): Welcome to the irc Internet Relay Chat Network evouser
...
~~~

Les lignes qui commence par le caractère « : » sont interprétées comme des commandes. Il y a 4 commandes interne au client :

- `:j #some-chan` pour joindre le canal « some-chan » ;
- `:l #some-chan` pour quitter le canal « some-chan » ;
- `:m #some-chan bla bla bla` ou `:m some-user bla bla bla` pour envoyer le message « bla bla bla » à un canal ou un utilisateur ;
- `:s #some-chan` ou `:s #some-user` pour définir un canal ou un utilisateur par défaut ;
- `:command` pour envoyer une commande spécifique au serveur, par exemple : `:ns identify mynickname flyYouFools`

Exemple d'utilisation :

~~~
$ sic -h irc.freenode.net -n evouser
helix.oftc.net: 2018-10-31 15:49 >< NOTICE (AUTH): *** Looking up your hostname...
helix.oftc.net: 2018-10-31 15:49 >< NOTICE (AUTH): *** Checking Ident
helix.oftc.net: 2018-10-31 15:49 >< NOTICE (AUTH): *** No Ident response
helix.oftc.net: 2018-10-31 15:49 >< NOTICE (AUTH): *** Found your hostname
[…]
helix.oftc.net: 2018-10-31 15:49 >< 376 (evouser): End of /MOTD command.
evouser     : 2018-10-31 15:49 >< MODE (evouser): +i
:s someuser
Install Gentoo?
someuser    : 2018-10-31 15:51 <evouser> Install Gentoo?
evouser     : 2018-10-31 15:51 <someuser> Nah, plan9 is bae!
:j #evocanal
evouser     : 2018-10-31 15:53 >< JOIN (): #evocanal
helix.oftc.net: 2018-10-31 15:53 >< MODE (#evocanal +nt):
helix.oftc.net: 2018-10-31 15:53 >< 353 (evouser = #evocanal): @evouser
helix.oftc.net: 2018-10-31 15:53 >< 366 (evouser #evocanal): End of /NAMES list.
:s #evocanal
hello!
#evocanal   : 2018-10-31 15:53 <evouser> hello!
:m evo2 join me on #evocanal
someuser    : 2018-10-31 15:54 <evouser> join me on #evocanal
someuser    : 2018-10-31 15:54 >< JOIN (): #evocanal
~~~