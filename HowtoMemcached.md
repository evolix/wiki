---
categories: web
title: Howto Memcached
...

* Documentation : <https://github.com/memcached/memcached/wiki>

[Memcached](http://memcached.org/) est un serveur noSQL clé-valeur non persistent (il stocke tout en mémoire, et toutes les données sont donc perdues après un redémarrage). Son utilisation typique est de mettre en cache des résultats issus de base de données, des réponses d'API, des rendus de pages, etc.

## Installation

~~~
# apt install memcached
~~~

Pour utiliser Memcached avec PHP / Ruby / Python : 

~~~
# apt install php5-memcache php5-memcached ruby-remcached python-pymemcache
~~~

## Configuration

La configuration se passe dans le fichier `memcached.conf` :

~~~
-d
# Mode verbose (pour du debug)
logfile /var/log/memcached.log
-v
# -vv
# Taille mémoire (en Mo)
-m 64
# Adresses d'écoute (à supprimer pour ouvrir de partout)
-l 127.0.0.1
-p 11211
-u nobody
# Nombre max de connexions
-c 1024
~~~

## Exemple de script en PHP

<http://www.php.net/manual/en/book.memcache.php>

~~~
<?php

$memcache = memcache_connect("localhost", 11213);
$myValue = $memcache->get('my_value');
    if ($myValue == FALSE) {
        echo 'Tha value is not in memcache!';
        // Adding the value in memcache for 60s
        $memcache->add('my_value', "Hello World!", false, 60);
    } else {
        echo 'The value is in memcache and is: ' . $myValue . '';
    }
?>
~~~

## Test avec telnet

~~~
$ telnet localhost 11211
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.

set foo 0 100 3
bar
STORED

get foo
VALUE bar 0 3
END

stats
STAT pid 8861
(etc)
~~~

## Test avec netcat

En non-interactif, utile pour mettre dans un script :

~~~
echo -e "set check_memcached 0 100 5\r\ncheck\r\nquit" | nc localhost 11211
echo -e "get check_memcached\r\nquit" | nc localhost 11211
~~~

## Administration

Un outil d'administration pratique est [phpmemcacheadmin](https://github.com/elijaa/phpmemcacheadmin)

Afficher la liste des clés avec memcdump (fourni via le paquet libmemcached-tools) : 

~~~
$ memcdump --servers=localhost
~~~

## Sessions PHP avec Memcached

_Memcached_ peut notamment être utilisé pour stocker les sessions PHP. Par exemple :

~~~
session.save_handler = memcached
session.save_path = "127.0.0.1:11211/"
~~~

...mais les [développeurs de Memcached le déconseillent](http://dormando.livejournal.com/495593.html) : en effet, _Memcached_ est fait pour du cache et non du stockage.


## Instances

Si besoin, plusieurs instances de _Memcached_ peuvent être lancées sur le serveur, sur des ports TCP différents.

On utilise [systemd](HowtoSystemd) pour gérer les instances, il faut créer un template `/etc/systemd/system/memcached@.service` :

~~~{.ini}
[Unit]
Description=memcached daemon
After=network.target

[Service]
ExecStart=/usr/share/memcached/scripts/systemd-memcached-wrapper /etc/%p_%i.conf

[Install]
WantedBy=multi-user.target
~~~

Ne plus démarrer le service _Memcached_ par défaut dans le target multi-user (~ runlevel 3) :

~~~
# rm /etc/systemd/system/multi-user.target.wants/memcached.service
~~~

Ensuite pour chaque instance avec un fichier de configuration `/etc/memcached_$nominstance.conf` et un lien symbolique vers le template dans le target multi-user :

~~~
# cd /etc/systemd/system/multi-user.target.wants/
# ln -s /etc/systemd/system/memcached@.service memcached@$nominstance.service
~~~

Le fichier `/etc/memcached_$nominstance.conf` doit être dupliqué en prenant soin d'adapter les options utiles comme _logfile_ et _-p_.

Puis

~~~
# systemctl daemon-reload
~~~

On peut ensuite manipuler chaque instance avec systemctl, par exemple :

~~~
# systemctl <start|stop|restart> memcached@$nominstance
~~~

## Réplication

_Memcached_ n'intègre pas de fonction de réplication/sharding/clustering. Un ancien patch existe pour une réplication master-master : [Repcached](http://repcached.lab.klab.org/) mais c'est ancien et non maintenu : nous déconseillons de l'utiliser en production.


## Monitoring

~~~
$ telnet localhost 11211
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
stats
STAT pid 11575
STAT uptime 417529
STAT time 1339577389
STAT version 1.2.2
STAT pointer_size 64
STAT rusage_user 246.070000
STAT rusage_system 849.540000
STAT curr_items 91905
STAT total_items 15555673
STAT bytes 360080271
STAT curr_connections 33
STAT total_connections 7626105
STAT connection_structures 376
STAT cmd_get 8342525
STAT cmd_set 16858359
STAT get_hits 1700473
STAT get_misses 6642052
STAT evictions 412025
STAT bytes_read 39016879520
STAT bytes_written 3810238557
STAT limit_maxbytes 402653184
STAT threads 1
~~~

### Nagios


Vérification simple :

~~~
$ /usr/lib/nagios/plugins/check_tcp -H 127.0.0.1 -p 11211
~~~

Vérification plus avancée avec :

~~~
# apt install libcache-memcached-perl
$ wget https://raw.githubusercontent.com/willixix/WL-NagiosPlugins/master/check_memcached.pl
$ perl check_memcached.pl -H 127.0.0.1
MEMCACHE OK: memcached 1.4.21 on 127.0.0.1:11211, up 2 minutes 37 seconds
~~~

### Munin

Le plugin Munin pour _Memcached_ est fourni en standard :

~~~
# apt install munin-plugins-extra libcache-memcached-perl
# cd /etc/munin/plugins
# ln -s /usr/share/munin/plugins/memcached_ memcached_bytes
# ln -s /usr/share/munin/plugins/memcached_ memcached_counters
# ln -s /usr/share/munin/plugins/memcached_ memcached_rates
# /etc/init.d/munin-node restart
~~~

On peut ensuite tester le plugin :

~~~
# munin-run memcached_bytes
# munin-run memcached_counters
# munin-run memcached_rates
~~~

Des plugins pour Munin existent pour relever diverses informations sur l'utilisation de memcache, comme la mémoire utilisée, le nombre et type de requêtes faites, le nombre de clés dans la base, etc…

Un plugin intéressant et assez complet : <https://github.com/mhwest13/Memcached-Munin-Plugin>
