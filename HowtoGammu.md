# Installation

~~~
# apt install gammu
~~~

Permet de manipuler et agir sur son téléphone cellulaire ou modem gsm.

[https://wammu.eu/docs/manual/index.html](https://wammu.eu/docs/manual/index.html)

# Mode client

## Configuration

Exemple avec un Huawei E173 (Modem USB) :

~~~{.bash}
$ gammu-config
~~~

Ouvrira un dialog interactif vous permettant de configurer un fichier par défaut pour le modem en question.
Pour l'exemple le port est : /dev/ttyUSB0
et la conection : at19200
Ces éléments sont propre au device.

Cela va créer un .gammurc sur le /home de votre utilisateur présent, et qui récapitule exactement les mêmes choses sélectionné dans le dialog.

Exemple :

~~~{.ini}
[gammu]

port = /dev/ttyUSB0
model = 
connection = at19200
synchronizetime = yes
logfile = 
logformat = nothing
use_locking = 
gammuloc = 
~~~

## Utilisation

Envoyer un SMS :

~~~{.bash}
echo "Hello Evolix!" | gammu sendsms TEXT "12345678"
~~~

# Daemon

## Installation

~~~
# apt install gammu-smsd
~~~

## Configuration

Exemple dans /etc/gammu-smsd :

~~~{.ini}
[gammu]
port = /dev/ttyUSB0
connection = at19200

[smsd]
PIN=1234 #Possiblement nécessaire si téléphone
service = files
logfile = /var/log/gammu-smsd
debuglevel = 1
RunOnReceive = /usr/local/bin/RunOnReceive.sh
#Pour certains modèle, la synchro et réception trop fréquente retourne de nombreuses erreurs -> donc augmentation des délais - à affiner
#http://stackoverflow.com/questions/29365148/gammu-stops-receiving-sms-aftar-a-while
ReceiveFrequency = 60
StatusFrequency = 60
CommTimeout = 60
SendTimeout = 60
LoopSleep = 20
CheckSecurity = 0
CheckBattery = 0

# Les répertoires (boites) où se trouveront les sms
inboxpath = /var/spool/gammu/inbox/
outboxpath = /var/spool/gammu/outbox/
sentsmspath = /var/spool/gammu/sent/
errorsmspath = /var/spool/gammu/error/
~~~

*path représente la SMSDir du daemon, comme pour une Maildir avec messages envoyés, ou ceux déjà reçu.

La directive **RunOnReceive** permet de lancer un script externe automatiquement lors d'une réception d'un nouveau message (existe aussi en **RunOnSend**).
message
## Utilisation

Ajout d'un nouveau message dans la queue :

[https://wammu.eu/docs/manual/smsd/inject.html](https://wammu.eu/docs/manual/smsd/inject.html)
~~~{.bash}
$ echo "Hello Evolix!" | gammu-smsd-inject TEXT 12345678
~~~

Dans les logs gammu-smsd on pourra lire lors de l'envoie :

~~~{.bash}

~~~

... lors de la réception :

~~~{.bash}
Tue 2017/02/14 15:30:12 gammu-smsd[22917]: Read 1 messages
Tue 2017/02/14 15:30:12 gammu-smsd[22917]: Received IN20170214_152534_00_+3312345678_00.txt
Tue 2017/02/14 15:30:12 gammu-smsd[22933]: Starting run on receive: /usr/local/bin/RunOnReceive.sh IN20170214_152534_00_+3312345678_00.txt
~~~

## RunOnReceive

[https://wammu.eu/docs/manual/smsd/run.html](https://wammu.eu/docs/manual/smsd/run.html)

Exemple **RunOnReceive.sh** :

~~~{.bash}
#!/bin/sh

echo "${SMS_1_NUMBER}: ${SMS_1_TEXT}" > /tmp/gammu-receive-fifo

exit 0
~~~

où */tmp/gammu-receive-fifo* est une FIFO, où un programme externe (notification, ou autre) viendra lire, et aura automatiquement le contenu lors d'une nouvelle écriture.