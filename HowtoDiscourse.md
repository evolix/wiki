**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowToDiscourse

La methode d'installation officielle de Discourse se fait avec docker. Voici les etapes à suivre pour l'installer :

~~~
# ln -s /usr/bin/aptitude /usr/bin/apt-get
~~~

~~~
# wget -qO- <https://get.docker.io/> | sh
~~~

~~~
# mkdir /var/discourse
~~~

~~~
# git clone <https://github.com/discourse/discourse_docker.git> /var/discourse
~~~

~~~
# cd /var/discourse
~~~

~~~
# cp samples/standalone.yml containers/app.yml
~~~

~~~
# vim containers/app.yml
~~~

Partie importante à modifier

~~~
...
env:
  LANG: en_US.UTF-8
  ## TODO: How many concurrent web requests are supported?
  ## With 2GB we recommend 3-4 workers, with 1GB only 2
  UNICORN_WORKERS: 3
  ##
  ## TODO: List of comma delimited emails that will be made admin and developer
  ## on initial signup example 'user1@example.com,user2@example.com'
  DISCOURSE_DEVELOPER_EMAILS: 'exemple@evolix.net'
  ##
  ## TODO: The domain name this Discourse instance will respond to
  DISCOURSE_HOSTNAME: 'exemple.evolix.net'
  ##
  ## TODO: The mailserver this Discourse instance will use
  DISCOURSE_SMTP_ADDRESS: 31.XXX.XX.XX         # (mandatory)
  DISCOURSE_SMTP_PORT: 25                        # (optional)
  #DISCOURSE_SMTP_USER_NAME: user@example.com      # (optional)
  #DISCOURSE_SMTP_PASSWORD: pa$$word               # (optional)
  DISCOURSE_SMTP_ENABLE_START_TLS: false
  ##
  ## The CDN address for this Discourse instance (configured to pull)
  #DISCOURSE_CDN_URL: //discourse-cdn.example.com
...
~~~

Il est important d'indiquer l'IP publique de la machine pour l'envoi de mails, et de rajouter dans la conf postfix le prefix 172.16.0.0/12 dans la directive mynetworks.

On peut ensuite faire tourner Discourse avec un utilisateur simple

~~~
# chown -R foo:foo /var/discourse/
~~~

~~~
# usermod -aG docker foo
~~~

~~~
$ ./launcher bootstrap app
~~~

~~~
$ ./launcher start app
~~~

Modifier la conf postfix en ajoutant 172.16.0.0/12 à la variable mynetworks afin que le conteneur de discourse puisse envoyer des mails

Maintenance : 

Lors de certaines MAJ via discourse il peut être necessaire une fois la MAJ terminée de reconstruire le conteneur (au cours de cette opération le conteneur sera stoppé et relancé)

~~~
$ ./launcher rebuild app
~~~

En cas d'erreur sur l'application il est possible de rentrer dans le conteneur pour debug

~~~
$ ./launcher ssh app
~~~