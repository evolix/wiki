**/!\\ Page en construction.**

# Filebeat

## Description

**Todo : à compléter.**


## Installation

### Via Ansible

Evolix met à votre disposition dans son dépôt public [ansible-roles](https://gitea.evolix.org/evolix/ansible-roles>) un rôle Ansible pour installer automatiquement Filebeat !

Vous pouvez ajouter ce rôle dans votre playbook pour automatiser l'installation de Filebeat.


### Via Apt

Elastic met à disposition un dépôt à ajouter dans les sources d'Apt.

La procédure à suivre se trouve à l'adresse : <https://www.elastic.co/guide/en/beats/filebeat/current/setup-repositories.html#_apt>


### Manuelle

Téléchargez avec `wget` le fichier `.deb` de la version souhaitée sur l'URL <https://www.elastic.co/fr/downloads/beats/filebeat>. Puis exécutez :

```
# Installer
dpkg -i filebeat-$version-amd64.deb
# Activer le service
systemctl --now enable filebeat

```

## Configuration

Le fichier de configuration de Filebeat est `/etc/filebeat/filebeat.yml`.


### Droits sur les logs

On peut définir les permissions des logs de Filebeat dans la variable `logging.files` de `/etc/filebeat/filebeat.yml`.

Cependant, on ne peut pas définir le propriétaire et le groupe des fichers de log générés par Filebeat. Il les créé avec le propriétaire et le groupe root.
Lorsqu'on les change avec la commande `chgrp`, ils repassent en root peu de temps après.

La solution est de donner l'accès au répertoire à l'utilisateur qui doit avoir accès aux logs, puis de mettre le `suid` ou le `suid` sur le répertoire pour forcer le propriétaire le groupe des fichiers contenus dans le répertoire.

Avec le `suid` :

~~~
chown $user $filebeat_log_dir
chmod u+s $filebeat_log_dir
~~~

Avec le `sgid` :

~~~
chgrp $user $filebeat_log_dir
chmod g+s $filebeat_log_dir
~~~


### Droits d'administration

Pour donner les droits de gestion et d'administration à l'utilisateur $fb avec sudo :

```
vim /etc/sudoers.d/filebeat
# Ajouter (en adaptant $fb) :
Cmnd_Alias FILEBEAT_CMD = sudoedit /etc/filebeat/*, /bin/systemctl restart filebeat, /bin/systemctl stop filebeat, /bin/systemctl start filebeat, /usr/bin/filebeat
$fd ALL=NOPASSWD: FILEBEAT_CMD
```

Pour donner un accès en lecture à la configuration de Filebeat à l'utilisateur $fb :

```
addgroup $fb adm
chgrp adm /etc/filebeat/filebeat.yml
chmod g+r /etc/filebeat/filebeat.yml
```

