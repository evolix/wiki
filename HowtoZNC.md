---
title: Howto ZNC ; installer un bouncer IRC
...

Documentation officielle : <http://znc.in>

## Installation

~~~
# apt install -t jessie-backports znc
~~~

La version de ZNC fournie dans Jessie (et avant) est très ancienne
et pourrait [contenir des bugs et des problèmes de
sécurité](http://wiki.znc.in/Installation#Debian). N'ayant aucune
dépendance externe au projet, il est acceptable de l'installer avec
un paquet "backport".

ZNC ne doit surtout pas fonctionner avec l'utilisateur root. On
crée donc un utilisateur dédié.

~~~
# useradd --create-home -d /var/lib/znc --system --shell /sbin/nologin --comment "Account to run ZNC daemon" --user-group znc
~~~

## Configuration

~~~
# sudo -u znc znc --datadir /var/lib/znc --makeconf
~~~

On utilise directement le dossier `/var/lib/znc` ça simplifie la
gestion par la suite.

La configuration est interactive. D'abord la partie générale.

~~~
[ .. ] Checking for list of available modules...
[ >> ] ok
[ ** ]
[ ** ] -- Global settings --
[ ** ]
[ ?? ] Listen on port (1025 to 65534): 6667
[ !! ] WARNING: Some web browsers reject port 6667. If you intend to
[ !! ] use ZNC's web interface, you might want to use another port.
[ ?? ] Proceed with port 6667 anyway? (yes/no) [yes]:
[ ?? ] Listen using SSL (yes/no) [no]:
[ ?? ] Listen using both IPv4 and IPv6 (yes/no) [yes]:
[ .. ] Verifying the listener...
[ >> ] ok
[ ** ] Unable to locate pem file: [/var/lib/znc/znc.pem], creating it
[ .. ] Writing Pem file [/var/lib/znc/znc.pem]...
[ >> ] ok
[ ** ] Enabled global modules [webadmin]
[ ** ]
~~~

Ensuite la création d'un utilisateur de départ (avec des droits de
gestion)

~~~
[ ** ] -- Admin user settings --
[ ** ]
[ ?? ] Username (alphanumeric): toto
[ ?? ] Enter password:
[ ?? ] Confirm password:
[ ?? ] Nick [toto]:
[ ?? ] Alternate nick [toto_]:
[ ?? ] Ident [toto]:
[ ?? ] Real name [Got ZNC?]: Toto Tata
[ ?? ] Bind host (optional):
[ ** ] Enabled user modules [chansaver, controlpanel]
[ ** ]
~~~

On peut ensuite configurer un premier réseau.

~~~
[ ?? ] Set up a network? (yes/no) [yes]:
[ ** ]
[ ** ] -- Network settings --
[ ** ]
[ ?? ] Name [freenode]:
[ ?? ] Server host [chat.freenode.net]:
[ ?? ] Server uses SSL? (yes/no) [yes]:
[ ?? ] Server port (1 to 65535) [6697]:
[ ?? ] Server password (probably empty):
[ ?? ] Initial channels:
[ ** ] Enabled network modules [simple_away]
[ ** ]
[ .. ] Writing config [/var/lib/znc/configs/znc.conf]...
[ >> ] ok
[ ** ]
[ ** ] To connect to this ZNC you need to connect to it as your IRC server
[ ** ] using the port that you supplied.  You have to supply your login info
[ ** ] as the IRC server password like this: user/network:pass.
[ ** ]
[ ** ] Try something like this in your IRC client...
[ ** ] /server <znc_server_ip> 6667 jlecour:<pass>
[ ** ]
[ ** ] To manage settings, users and networks, point your web browser to
[ ** ] http://<znc_server_ip>:6667/
[ ** ]
~~~

L'outil propose de lancer directement ZNC. Il vaut mieux ne pas le
faire tout de suite

~~~
[ ?? ] Launch ZNC now? (yes/no) [yes]: no
~~~

Le résultat se trouve dans `/var/lib/znc/configs/znc.conf`. Il ne
faut pas modifier ce fichier manuellement pendant que znc fonctionne.
On peut utiliser l'interface web, ou bien couper znc, changer la
config manuellement et le relancer.

*Note: la page de manuel complète pour la configuration est disponible
sur <http://wiki.znc.in/Configuration>.*

## Gestion du démon

Le paquet ne fourni pas de script d'init ni d'unité systemd.

### manuellement

Il est possible de gérer ZNC très basiquement avec un cron :

~~~
*/10 * * * * /usr/local/bin/znc --datadir /var/lib/znc >/dev/null 2>&1
~~~

Pour un lancement ponctuel, au 1er plan :

~~~
# /usr/local/bin/znc --datadir /var/lib/znc --foreground
~~~

### avec systemd

Pour utiliser une unit systemd, créez un fichier
`/etc/systemd/system/znc.service` :

~~~
[Unit]
Description=ZNC, an advanced IRC bouncer
After=network-online.target

[Service]
ExecStart=/usr/bin/znc -f --datadir=/var/lib/znc
User=znc

[Install]
WantedBy=multi-user.target
~~~

Pour activer l'unité et démarrer le service :

~~~
# systemctl start znc.service
# systemctl enable znc.service
~~~

## Connecter un client IRC

Une fois démarré, votre bouncer est connecté avec votre identité
aux serveurs et canaux configurés. Pour l'utiliser il faut connecter
votre client IRC au bouncer.

Selon que votre client IRC propose des champs séparés ou pas :

* **username** = `znc_username@clientid/network`, **password** = `znc_password`
* **password** = `znc_username@clientid/network:znc_password`

La partie `clientid` est facultative et libre. En l'utilisant, vous
pourrez alors identifier vos clients connectés dans la sortie de
la commande `/msg *status listclients`.

La partie `network` correspond au nom du réseau que vous avez donné
lors de la configuration de ZNC ; par exemple `Freenode`.

Il ne faut pas confondre vos identifiants pour ZNC (utilisés ici)
et ceux que ZNC doit utiliser pour se connecter avec votre identité
aux réseaux IRC. Ceux là sont également configurés lors de la mise
en place de ZNC et peuvent être modifiés par l'interface web ou
dans le fichier de config.
