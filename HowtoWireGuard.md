---
title: Howto Wireguard
categories: sysadmin network vpn
...

* Site officiel WireGuard : <https://www.wireguard.com/>
* OpenBSD Manpage <https://man.openbsd.org/wg>
* Debian Manpage <https://manpages.debian.org/stable/wireguard-tools/wg.8.en.html>

[WireGuard](https://www.wireguard.com/) est une solution de communication Open source qui implémente la technique de réseau privé virtuel (VPN) pour créer des connexions point à point sécurisées. C'est une solution simple, moderne et rapide qui utilise une cryptographie moderne. Il vise à être plus rapide, plus simple et plus léger que IPsec. Il est également considérablement plus performant que OpenVPN. Initialement publié pour le noyau Linux, il est désormais multiplateforme (Windows, macOS, BSD, iOS, Android) et largement déployable. Il est déjà considéré comme la solution VPN la plus sécurisée et la plus simple à utiliser.

## Installation

Sous Debian (Kernel >= 5.6) et OpenBSD (>= 6.8), Wireguard fait partie du Kernel. On utilise toutefois les outils fournis par WireGuard pour faciliter l'utilisation.

### Debian

~~~
# apt install wireguard

$ wg --version
wireguard-tools v1.0.20210223 - https://git.zx2c4.com/wireguard-tools/
~~~

### OpenBSD

~~~
# pkg_add wireguard-tools

$ wg --version
wireguard-tools v1.0.20210914 - https://git.zx2c4.com/wireguard-tools/
~~~

### Autres systèmes

WireGuard est également disponibles sous Android, macOS, iOS, Windows, et de nombreux autres : <https://www.wireguard.com/install/>

## Configuration du serveur

Autoriser le routage via `sysctl` :

~~~
## Sous Debian
# echo "net.ipv4.conf.all.forwarding = 1" >> /etc/sysctl.d/wireguard.conf
# echo "net.ipv6.conf.all.forwarding = 1" >> /etc/sysctl.d/wireguard.conf
# sysctl -p /etc/sysctl.d/wireguard.conf

## Sous OpenBSD
# echo "net.inet.ip.forwarding=1" >> /etc/sysctl.conf
# echo "net.inet6.ip6.forwarding=1" >> /etc/sysctl.conf
# sysctl -p /etc/sysctl.conf
~~~

Générer une clé privée pour le serveur et sa clé publique :

~~~
# mkdir -p /etc/wireguard/keys/
# wg genkey | tee /etc/wireguard/keys/serveur_priv | wg pubkey > /etc/wireguard/keys/serveur_pub
# find /etc/wireguard/ -type d -exec chmod 700 {} \; && find /etc/wireguard/ -type f -exec chmod 600 {} \;
~~~

Créer un fichier de configuration `/etc/wireguard/wg0.conf`, pour l'interface `wg0` : 

~~~
[Interface]
Address = 192.0.2.1/24, 2001:db8::1/64
ListenPort = 51820
PrivateKey = ***** # Contenu de /etc/wireguard/keys/serveur_priv
~~~

Avec :

* `Address` : l'IP dans le réseau WireGuard attribuée au serveur
* `ListenPort` : le port d'écoute de WireGuard, 51820 est celui par défaut
* `PrivateKey` : la valeur de la clé privée du serveur, générée plus tôt

Ces paramètres sont également possibles :

* `DNS` : serveurs DNS à utiliser sur ce tunnel VPN
* `MTU` : MTU à utiliser sur le lien ; automatiquement déterminé si non spécifié
* `PreUp`, `PostUp`, `PreDown`, `PostDown` : commandes ou scripts qui seront exécutés par bash avant ou après avoir activé ou désactivé l'interface VPN
* `SaveConfig` : si true, la configuration est mise en mémoire (et protégée) tout le temps que l'interface est active ; signifie que si le fichier de configuration est modifié pendant que le VPN est UP, alors lorsqu'il sera coupé, la configuration sera remise comme elle était au moment de son démarrage. On est donc obligé de stopper le VPN, faire notre modification, puis le démarrer à nouveau. Valeur par défaut : false.

Il suffit ensuite de démarrer notre VPN avec `wg-quick` :

~~~
# wg-quick up wg0
OU
# systemctl start wg-quick@wg0.service
~~~

On peut voir la configuration en cours avec `wg` : 

~~~
# wg
interface: wg0
  public key: *****
  private key: (hidden)
  listening port: 51820
~~~

Enfin, on peut activer le démarrage automatique de notre interface `wg0` : 

~~~
## Sous Debian
# systemctl enable wg-quick@wg0.service

## Sous OpenBSD
# echo "!/usr/local/bin/wg-quick up wg0" > /etc/hostname.em0
~~~

## Déclaration des clients côté serveur

Générer une clé privée pour un client et sa clé publique, puis une clé pré-partagée :

~~~
# CLIENT=<nom_client>
# wg genkey | tee /etc/wireguard/keys/${CLIENT}_priv | wg pubkey > /etc/wireguard/keys/${CLIENT}_pub
# wg genpsk > /etc/wireguard/keys/${CLIENT}_psk
# find /etc/wireguard/ -type f -exec chmod 600 {} \;
~~~

Dans le fichier de configuration `/etc/wireguard/wg0.conf`, sous le bloc `[Interface]`, on crée un bloc `[Peer]` par client : 

~~~
[Peer] # Client 1
PublicKey = ***** # Contenu de /etc/wireguard/keys/${CLIENT}_pub
PresharedKey = ***** # Contenu de /etc/wireguard/keys/${CLIENT}_psk
Endpoint = <IP_publique_client>:51820 # Facultatif
AllowedIPs = 192.0.2.0/24, 2001:db8::/64
~~~

Avec :

* `AllowedIPs` :
    * Utilisé comme table de routage en sortie : pour joindre ces réseaux, je passe par ce peer sur le VPN.
    * Utilisé comme ACL en entrée : ce peer sera autorisé à me joindre s'il a une de ces IPs sources.

* `Endpoint` :
    * L'IP publique du client et le port utilisé pour WireGuard. Cette option est facultative : si elle n'est pas présente du côté du serveur, elle doit être présente du côté du client et le serveur ajustera automatiquement le paramètre selon l'IP distante observée.

Puis on redémarre l'interface `wg0` :

~~~
# wg-quick down wg0 && wg-quick up wg0
OU
# systemctl reload wg-quick@wg0.service
~~~

Et on peut vérifier que la déclaration du peer est bonne :

~~~
# wg
interface: wg0
  public key: *****
  private key: (hidden)
  listening port: 51820

peer: *****
  allowed ips: 192.0.2.0/24, 2001:db8::/64
~~~

Quand le client aura monté sa connexion WireGuard, son adresse IP sera visible dans une valeur `endpoint`.

## Configuration du client

Il faut donner au client les paramètres générés côté serveur :

~~~
[Interface]
PrivateKey = ***** # Contenu de /etc/wireguard/keys/${CLIENT}_priv
Address = 192.0.2.2/24, 2001:db8::2/64

[Peer]
PublicKey = ***** # Contenu de /etc/wireguard/keys/serveur_pub
PresharedKey = ***** # Contenu de /etc/wireguard/keys/${CLIENT}_psk
Endpoint = <server_ip>:51820
AllowedIPs = 0.0.0.0/0, ::/0
~~~

Il peut ensuite utiliser sa configuration de la même façon que le serveur, avec `wg-quick` ou `systemctl`.

Pour avoir un historique et notamment une liste des IP utilisées, il peut être utile de garder la configuration client sur le serveur, par exemple dans `/etc/wireguard/configs/${CLIENT}.conf` :

~~~
# mkdir /etc/wireguard/configs/
# vim /etc/wireguard/configs/${CLIENT}.conf
# find /etc/wireguard/ -type d -exec chmod 700 {} \; && find /etc/wireguard/ -type f -exec chmod 600 {} \;
~~~

## Débogage

Sous OpenBSD :

En cas de besoin, il est possible d'activer ou désactiver le débogage.

~~~
# ifconfig wg0 debug
# ifconfig wg0 -debug
~~~

## Limitations

Le but de WireGuard est d'être léger, simple et performant. Ainsi il ne fait que créer un tunnel VPN et ne gère pas tout ce qui peut être autour.

WireGuard n'a pas de notion de configuration poussée par le serveur (contrairement à OpenVPN par exemple). Par exemple l'administrateur d'un serveur WireGuard ne peut pas pousser aux autres serveurs/clients WireGuard de routes. Ainsi, si le client veut pouvoir joindre un serveur donné via son VPN, c'est à lui de l'indiquer dans sa configuration. Si tous les clients veulent pouvoir joindre une nouvelle IP via le serveur, alors tous les clients doivent modifier leur configuration.