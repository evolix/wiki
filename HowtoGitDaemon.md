**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowToGitDeamon

## Prérequis

Installation du service git-daemon :

~~~
# aptitude install git-daemon-sysvinit
~~~

Configuration du démon Git /etc/default/git-daemon :

~~~
cat > /etc/default/git-daemon <<GD
GIT_DAEMON_ENABLE=true
GIT_DAEMON_USER=gitdaemon
GIT_DAEMON_OPTIONS="--interpolated-path=/home/%H/repositories/%D"
GD
~~~

Éditer le fichier /etc/init.d/git-daemon et commenter la ligne suivante (34) :

~~~
#DAEMON_ARGS="$DAEMON_ARGS --base-path=$GIT_DAEMON_BASE_PATH $GIT_DAEMON_DIRECTORY"
~~~

#### Pensez à ouvrir le port 9418 entrant sur le firewall !

## Configuration

Choix de l'utilisateur $GIT :

* $GIT : utilisateur propriétaire des dépots
* dépôts présent dans /home/$GIT/repositories
* accès git:// depuis $GIT@votre-domaine.tld

~~~
GIT='git'
~~~

Crée un lien symbolique car git-daemon accède aux dépôts via $GIT.votre-domaine.tld  :

~~~
ln -s /home/$GIT/ /home/$GIT.$(hostname -d)
~~~

Donne l'accès en lecture au dépôts à l'utilisateur gitdaemon :

~~~
addgroup gitdaemon $GIT
~~~

Redémarrage du démon git :

~~~
service git-daemon restart
~~~
