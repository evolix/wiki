---
title: Howto Mercure
categories: web
...

* Documentation : <https://mercure.rocks/docs>

[Mercure](https://mercure.rocks/) est un logiciel libre de communication en temps réel écrit en Go.

## Installation

Note: l’installation a été faite pour Mercure v0.15 sur Debian 11 et Mercure v0.16 sur Debian 12.

### Télécharger Mercure

Récupérer une archive depuis le [dépôt sur GitHub](https://github.com/dunglas/mercure/releases) puis vérifier sa somme de contrôle. Attention, l’archive n’a pas de répertoire à sa racine, donc il vaut mieux se mettre dans un répertoire avant de l’extraire. Par exemple :

~~~
# mkdir mercure-tmp
# cd mercure-tmp
# wget https://github.com/dunglas/mercure/releases/download/<VERSION>/mercure_Linux_x86_64.tar.gz
# sha256sum mercure_Linux_x86_64.tar.gz
# tar -xz -f mercure_Linux_x86_64.tar.gz
# chown -R root: .
~~~

L’archive devrait contenir les fichiers suivants.

~~~
-rw-r--r-- root:root Caddyfile
-rw-r--r-- root:root COPYRIGHT
-rw-r--r-- root:root dev.Caddyfile
-rw-r--r-- root:root LICENSE
-rwxr-xr-x root:root mercure
-rw-r--r-- root:root README.md
~~~

### Installer les fichiers

~~~
# mkdir -p /usr/local/share/doc/mercure
# cp -a COPYRIGHT LICENSE README.md /usr/local/share/doc/mercure/
# mkdir /etc/mercure
# cp -a *Caddyfile* /etc/mercure/
# cp -a mercure /usr/local/sbin/
~~~

### Créer une unité systemd

1. Créer le fichier `/etc/systemd/system/mercure.service` avec le contenu suivant.

~~~
[Unit]
Description=Mercure Hub (allow to push data updates to web browsers and other HTTP clients)
Documentation=https://mercure.rocks/docs

[Service]
WorkingDirectory=/var/lib/mercure
ExecStart=/usr/local/sbin/mercure run --config /etc/mercure/Caddyfile
ExecReload=/usr/local/sbin/mercure reload --config /etc/mercure/Caddyfile
Restart=on-failure

CapabilityBoundingSet=
NoNewPrivileges=yes

UMask=0027

RuntimeDirectory=mercure
StateDirectory=mercure
ConfigurationDirectory=mercure
LogsDirectory=mercure

ProtectSystem=strict
ProtectHome=yes
PrivateTmp=yes
PrivateDevices=yes
PrivateUsers=yes
ProtectKernelTunables=yes
ProtectKernelModules=yes
ProtectControlGroups=yes
RestrictNamespaces=
LockPersonality=yes
RestrictRealtime=yes
RemoveIPC=yes
SystemCallFilter=@system-service
SystemCallFilter=~@privileged
SystemCallErrorNumber=EPERM
SystemCallArchitectures=native

PrivateNetwork=no
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6

[Install]
WantedBy=multi-user.target
~~~

2. Recharger la configuration de systemd :`# systemctl daemon-reload`.
3. Activer et démarrer l’unité : `# systemctl enable --now mercure.service`.

## Configuration

La configuration du service est dans le fichier `/etc/mercure/Caddyfile`. Voici un exemple de configuration (adapter le texte en majuscule entre `<>`) :

~~~
{
    auto_https off
    admin unix//run/mercure/admin.sock
}

<DOMAIN>:<PORT>

log {
    format filter {
        wrap console
        fields {
            uri query {
                replace authorization REDACTED
            }
        }
    }
    output file /var/log/mercure/access.log
}

tls <PATH_TO_FULLCHAIN> <PATH_TO_PRIVKEY>

route {
    encode zstd gzip

    mercure {
        transport_url bolt:///var/lib/mercure/mercure.db
        publisher_jwt <PUBLISHER_KEY> <PUBLISHER_KEY_ALG>
        subscriber_jwt <SUBSCRIBER_KEY> <SUBSCRIBER_KEY_ALG>
        subscriptions
    }

    header / Content-Type "text/html; charset=utf-8"
    respond / `<!DOCTYPE html>
    <html lang=en>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <title>Welcome to Mercure</title>
    <h1>Welcome to Mercure</h1>
    <p>The URL of your hub is <code>/.well-known/mercure</code>.
    Read the documentation on <a href="https://mercure.rocks">Mercure.rocks, real-time apps made easy</a>.`

    respond /healthz 200
    respond "Not Found" 404
}
~~~

Note: Mercure embarque le serveur web [Caddy](https://caddyserver.com/).

Le plus simple pour les valeurs de `PUBLISHER_KEY_ALG` et `SUBSCRIBER_KEY_ALG` est d'avoir HS256, puis mettre [une chaîne de caractère aléatoire](/TipsShell#mot-de-passe) pour `PUBLISHER_KEY` et `SUBSCRIBER_KEY`.

## Mise à jour

Tout comme l’installation, [télécharger Mercure](#télécharger-mercure) et [installer les fichiers](#installer-les-fichiers) **sans réécrire complètement le fichier `/etc/mercure/Caddyfile`**. Pour finir, redémarrer le service : `# systemctl restart mercure.service`
