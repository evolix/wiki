---
title: Howto Jitsi
category: Communication
...

- Documentation : [https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart)
- Code : [https://github.com/jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet)
- Licence : [Apache License 2.0](https://github.com/jitsi/jitsi-meet/blob/master/LICENSE)
- Language : Java, Javascript et Lua (Prosody)
- Rôle Ansible : (à venir)

**Jitsi Meet** est une application de visionconférence riche en fonctionnalités et compatible avec les principaux navigateurs Web. Lorsqu'il n'y a que deux participants dans une conférence, la communication audio-vidéo se fait en pair-à-pair via WebRTC. Dès qu'il y a un troisième participant, on bascule en mode client-serveur.

## Installation

### Pré-requis

Il est important que `/tmp` et `/usr/share/jitsi` soient en sur des partitions montées en `read-write` et en `exec`.

Si `/tmp` n'est pas "exec", l'audio/video ne marchera pas.
Si `/usr/share/jitsi` n'est pas accessible en écriture, jicofo se plaindra de ne pas pouvoir acquérir de lock.

### Requis

Un nom de domaine configuré sur l'adresse IP de la machine qui hébergera les composants serveur de Jitsi Meet.

L'ouverture dans le pare-feu du serveur des ports `TCP/443` et `UDP/10000`.

### Paquets Debian

Nous installons Jitsi Meet depuis les paquets Debian officiels (branch stable) sur un serveur Debian 11 (Bullseye).

Les principaux composants de Jitsi Meet sont : 

* [Meet](https://github.com/jitsi/jitsi-meet) : application Javascript chargée côté client dans le navigateur ou l'appli mobile des participants
* [Jicofo](https://github.com/jitsi/jicofo) : JItsi COnference FOcus, gère notamment les sessions Jingle (extension média de XMPP) des participants (Java)
* [Videobridge ou JVB](https://github.com/jitsi/jitsi-videobridge) : routeur audio-vidéo compatible WebRTC (Java)
* [Prosody](https://prosody.im/) : serveur de messagerie instantanée compatible XMPP (Lua)
* Nginx ou Apache : serveur web et proxy

Pour obtenir ces composants et leurs dépendances, il faut ajouter le dépôt de Jitsi (et sa clé gpg) aux sources de apt :

~~~
# curl https://download.jitsi.org/jitsi-key.gpg.key | gpg --dearmor > /etc/apt/keyrings/jitsi.gpg
# chmod 644 /etc/apt/keyrings/jitsi.gpg
# echo 'deb [signed-by=/etc/apt/keyrings/jitsi.gpg] https://download.jitsi.org stable/' | tee /etc/apt/sources.list.d/jitsi.list > /dev/null
# chmod 644 /etc/apt/sources.list.d/jitsi.list 

# curl https://prosody.im/files/prosody-debian-packages.key | gpg --dearmor > /etc/apt/keyrings/prosody.gpg
# chmod 644 /etc/apt/keyrings/prosody.gpg
# echo 'deb [signed-by=/etc/apt/keyrings/prosody.gpg] http://packages.prosody.im/debian $(lsb_release -sc) main' | tee /etc/apt/sources.list.d/prosody.list > /dev/null
# chmod 644 /etc/apt/sources.list.d/prosody.list 

~~~

Ensuite, recharger la liste des dépôts du système et installer le paquet `apt-transport-https`.

~~~
# apt update
# apt install apt-transport-https
~~~

Lors de l'installation du paquet `jitsi-meet`, celui-ci vérifie si un serveur web, Apache ou Nginx est présent et configurera un vhost.
Si aucun serveur web n'est présent sur la machine, il installera et configurera Nginx.

~~~
# apt install jitsi-meet
~~~

Suite à l'installation, on peut vérifier que les principaux services systemd sont fonctionnels comme ceci :

~~~
# systemctl status jitsi-videobridge2.service
● jitsi-videobridge2.service - Jitsi Videobridge
     Loaded: loaded (/lib/systemd/system/jitsi-videobridge2.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2023-01-08 22:55:09 CET; 27min ago
   Main PID: 7052 (java)
      Tasks: 85 (limit: 65000)
     Memory: 453.6M
        CPU: 49.299s
     CGroup: /system.slice/jitsi-videobridge2.service
             └─7052 java -Xmx3072m -XX:+UseG1GC -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp -Djdk.tls.ephemeralDHKeySize=2048 -Dconfig.file=/etc/jitsi/videobridge/jvb.conf -Dnet.java.sip.communicator.SC_HOME_DIR_LOCATION=/etc/jit>

Jan 08 22:55:09 jitsi01 systemd[1]: Starting Jitsi Videobridge...
Jan 08 22:55:09 jitsi01 systemd[1]: Started Jitsi Videobridge.
~~~

Même chose pour `jicofo.service`, `prosody.service` et `nginx.service`.

### Générer un certificat SSL Let's Encrypt

**Il faut bien vérifier que le nom de domaine configurer pour Jitsi pointe bien vers le serveur au niveau des DNS.**

À l'installation initiale, Jitsi va proposer de créer un certificat Let's Encrypt pour le domaine choisi, mais il est aussi possible de le faire plus tard.

Si on choisi de le faire plus tard, on peut exécuter un script fournit par Jitsi :

~~~
# /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
~~~

Par défaut Jitsi utilise [acme.sh](/HowtoAcme.sh) pour la gestion des certificats. Le renouvellement est configuré dans la crontab de `root`.

## Mises à jour

La mise à jour des paquets se fait de la manière habituelle :

~~~
# apt update
# apt list --upgradable
# apt upgrade
~~~

IMPORTANT : avant de se lancer dans la mise à jour des paquets, il est une bonne pratique de lire les [notes de version](https://github.com/jitsi/jitsi-meet/releases).

## Configuration

### Tableau des principaux fichiers

|Fichier(s)          |Chemin                                              | 
|--------------------|----------------------------------------------------|
|Conf. Meet          | /etc/jitsi/meet/meet.exemple.org-config.js         |
|SSL pour Meet       | /etc/jitsi/meet/meet.exemple.org.crt et .key       |
|Conf. Jicofo        | /etc/jiti/jicofo/jicofo.conf                       |
|Conf. Videobridge   | /etc/jitsi/videobridge/jvb.conf                    |
|Conf. Videobridge   | /etc/jitsi/videobridge/sip-communicator.properties |
|Conf. Prosody       | /etc/prosody/conf.d/meet.exemple.org.cfg.lua       |
|SSL pour Prosody    | /etc/prosody/certs/*                               |
|Conf. Nginx (vhost) | /etc/nginx/sites-enabled/meet.exemple.org.conf     |

### Serveur TURN (coturn) sur le port TCP/443

Le pare-feu de plusieurs entreprises empêchera les participants de communiquer en sortie sur le port `UDP/10000`. 
Une [solution de rechange](https://jitsi.github.io/handbook/docs/devops-guide/turn#use-turn-server-on-port-443) peut être configurée pour remédier à ce problème.

Cette solution de rechange via `TCP/443` et en passant par nginx, coturn et un nom de domaine secondaire (exemple : `turn.meet.exemple.org`) est moins performante que la communication directe via `UDP/10000` et n'a pas vocation à la remplacer.

### Serveur STUN (coturn) sur le port UDP/3478

La configuration initiale de Jitsi Meet fait appel à un serveur STUN externe (`meet-jit-si-turnrelay.jitsi.net`) qui écoute sur le port `UDP/443` (oui `UDP`, pas `TCP`). 

Si on désire plutôt utiliser son propre serveur, il est possible de le faire avec coturn, qui est déjà installé par défaut et écoute sur le port `UDP/3478`.

## Utilisation

Lancer un navigateur web (ou l'appli mobile ou de bureau) et entrer le nom de domaine, ou l'adresse ip, que l'on a renseigné dans la configuration de Jitsi.

Par défaut Jitsi génère un certificat auto-signé, il faut accepter celui-ci.


## Maintenance

### Conférences actives

Il est possible d'avoir un aperçu des conférences actives grâce à la commande suivante :

~~~
# tail -f /var/log/jitsi/jvb.log | grep "conf_name="
~~~

Ou bien en faisant une requête locale à VideoBridge :

~~~
$ curl -s http://localhost:8080/debug | jq . | grep name
~~~


## FAQ

### API applis mobiles et bureau

Si on désire que l'instance Jitsi soit fonctionnelle avec l'application mobile ou de bureau (Electron), il faut autoriser l'accès à l'API, ce qui est le cas dans le vhost généré automatiquement lors de l'installation. Si vous avez vhost nginx personnalisé, vous devrez vous assurer d'avoir ces lignes :

~~~
location /external_api.js {
    alias /usr/share/jitsi-meet/libs/external_api.min.js;
}
~~~

### Entêtes bloquantes

Il faut aussi vérifié que les Headers suivants ne soit pas activé dans nginx, car cela bloquera l'accès à l'instance par l'application :

~~~
Content-Security-Policy "frame-ancestors 'none'";
X-Frame-Options "DENY";
~~~

