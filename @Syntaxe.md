Pour relancer un service : /etc/init.d/foo restart OU systemctl restart foo ? → [Sondage](https://framadate.org/t1vYfIBlDV9maiZm)


Proposition de convention :
Ne pas utiliser de titre "niveau 1" (H1) dans les pages, car Gitit rajoute déjà le "title" en H1 automatiquement.
