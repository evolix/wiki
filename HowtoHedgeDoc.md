---
categories: web webapp
title: Howto HedgeDoc
...

* Documentation : <https://docs.hedgedoc.org>

[HedgeDoc](https://github.com/hedgedoc/hedgedoc) est une application web libre de rédaction collaborative en temps réel, proposant notamment la syntaxe Markdown. Son [code](https://github.com/hedgedoc/hedgedoc) s'appuye sur [NodeJS](/HowtoNodeJS).

## Installation

Nous installons la version **1.9.6** sous **Debian 11 (Bullseye)** en mode manuel. (Il y a aussi un mode d'installation pour [Docker et Docker Compose](https://docs.hedgedoc.org/setup/docker/).)

HedgeDoc s'appuie sur [NodeJS](HowtoNodeJS), [Yarn](HowtoYarn), [node-gyp](https://github.com/nodejs/node-gyp) et peut être configuré avec différents SGBD ([PostgreSQL](HowtoPostgreSQL), MySQL, mariaDB, SQLite) et proxy web ([Nginx](HowtoNginx), Apache). Dans la présente documentation, nous montrons comment faire avec PostgreSQL et Nginx.

On installe les dépendances de la manière suivante :

~~~
# apt install apt-transport-https postgresql nginx git wget
# echo "deb https://deb.nodesource.com/node_16.x nodistro main" > /etc/apt/sources.list.d/nodesource.list
# wget https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key -O /etc/apt/trusted.gpg.d/nodesource.asc
# chmod 644 /etc/apt/trusted.gpg.d/nodesource.asc
# apt update && apt install nodejs
# npm install --global yarn
# npm install --global node-gyp
~~~

### Compte UNIX

Créer un compte UNIX *hedgedoc* :

~~~
# adduser --disabled-login --gecos 'HedgeDoc App' hedgedoc
~~~

###  PostgreSQL

Créer la base de données et l'utilisateur PostgreSQL :

~~~
# sudo -u postgres createuser hedgedoc -P
# sudo -u postgres createdb hedgedoc
# sudo -u postgres psql
# psql=# grant all privileges on database hedgedoc to hedgedoc;
# exit
~~~

> **Note** : Pensez à conserver le mot de passe pour la suite.


### HedgeDoc

On clone le code et on bâtit l'application :

~~~
# sudo -iu hedgedoc
$ git clone https://github.com/hedgedoc/hedgedoc.git
$ cd hedgedoc/
$ git checkout 1.9.6
$ bin/setup
$ yarn install --frozen-lockfile
$ yarn build
~~~

> IMPORTANT : si votre machine dispose de < de 2 Go de RAM, vous ne pourrez bâtir le *frontend* (`yarn build`). Une alternative est d'installer à partir du [paquet tar.gz précompilé](https://github.com/hedgedoc/hedgedoc/releases/download/1.9.6/hedgedoc-1.9.6.tar.gz) par l'équipe de développement.

On édite le fichier `config.json` généré par `bin/setup`. Vous devrez nécessairement modifier au minimum les valeurs des variables suivantes : `domain` (sous `"production":`), `username`, `password` et `database` (sous `"production":"db":`).

~~~
{
    "test": {
        "db": {
            "dialect": "sqlite",
            "storage": ":memory:"
        },
        "linkifyHeaderStyle": "gfm"
    },
    "development": {
        "loglevel": "debug",
        "db": {
            "dialect": "sqlite",
            "storage": "./db.hedgedoc.sqlite"
        },
        "domain": "localhost",
        "urlAddPort": true
    },
    "production": {
        "domain": "hedgedoc.evolix.org",
        "loglevel": "info",
        "protocolUseSSL": "true",
        "urlAddPort": false,
        "hsts": {
            "enable": true,
            "maxAgeSeconds": 31536000,
            "includeSubdomains": true,
            "preload": true
        },
        "csp": {
            "enable": true,
            "directives": {
            },
            "upgradeInsecureRequests": "auto",
            "addDefaults": true
        },
        "cookiePolicy": "lax",
        "db": {
            "username": "hedgedoc",
            "password": "MDP_POSTGRES_NOTE_PLUS_HAUT",
            "database": "hedgedoc",
            "host": "localhost",
            "port": "5432",
            "dialect": "postgres"
        }
    }
}
~~~


### Unité systemd

Pour lancer l'application au démarrage du serveur, on met le texte suivant dans le nouveau fichier `/etc/systemd/system/hedgedoc.service`

~~~
[Unit]
Description=HedgeDoc - The best platform to write and share markdown.
Documentation=https://docs.hedgedoc.org/
After=network.target
# Uncomment if you use MariaDB/MySQL
# After=mysql.service
# Uncomment if you use PostgreSQL
After=postgresql.service

[Service]
Type=exec
Environment=NODE_ENV=production
Restart=always
RestartSec=2s
ExecStart=/usr/bin/yarn start --production
CapabilityBoundingSet=
NoNewPrivileges=true
PrivateDevices=true
RemoveIPC=true
LockPersonality=true
ProtectControlGroups=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectKernelLogs=true
ProtectClock=true
ProtectHostname=true
ProtectProc=noaccess
RestrictRealtime=true
RestrictSUIDSGID=true
RestrictNamespaces=true
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6
ProtectSystem=strict
PrivateTmp=true
SystemCallArchitectures=native
SystemCallFilter=@system-service

# You may have to adjust these settings
User=hedgedoc
Group=hedgedoc
WorkingDirectory=/home/hedgedoc/hedgedoc

# Example: local storage for uploads and SQLite
# ReadWritePaths=/opt/hedgedoc/public/uploads /opt/hedgedoc/db

[Install]
WantedBy=multi-user.target
~~~

On active et on démarre l'unité en question :

~~~
# systemctl enable hedgedoc.service
# systemctl start hedgedoc.service
~~~

### Nginx

Pour accéder à l'application depuis Internet, on place nginx devant, qui agira comme serveur mandataire (*proxy*). 
Il faut mettre le texte suivant dans le nouveau fichier `/etc/nginx/sites-available/hedgedoc.conf` :

~~~
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

server {
    listen 80;
    listen [::]:80;
    server_name hedgedoc.evolix.org;

    # Let's Encrypt
    include /etc/nginx/snippets/letsencrypt.conf;

    # Redirect all to https (port 443)
    location / { return 301 https://$host$request_uri; }
}

server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    server_name hedgedoc.evolix.org;

    ssl_certificate     /etc/letsencrypt/live/hedgedoc.evolix.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/hedgedoc.evolix.org/privkey.pem;

    location / {
        proxy_pass http://127.0.0.1:3000;
        proxy_set_header Host $host; 
        proxy_set_header X-Real-IP $remote_addr; 
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; 
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /socket.io/ {
        proxy_pass http://127.0.0.1:3000;
        proxy_set_header Host $host; 
        proxy_set_header X-Real-IP $remote_addr; 
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; 
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }
}
~~~

> **Note** : La partie SSL/TLS n'est pas développée. Vous pouvez par exemple générer et configurer un certificat [Let's Encrypt](HowtoLetsEncrypt) avec certbot. N'oubliez donc pas de modifier les directives `ssl_` dans le vhost.  

On active le vhost, on vérifie sa syntaxe et si tout est beau on recharge la configuration de nginx :

~~~
# ln -s /etc/nginx/sites-available/hedgedoc.conf /etc/nginx/sites-enabled/hedgedoc.conf
# nginx -t
# systemctl reload nginx.service
~~~

## Mises à jour

Les mises à jour peuvent être récupérées comme ceci :

~~~
# systemctl stop hedgedoc.service
# sudo -iu hedgedoc
$ cd hedgedoc
$ git fetch origin && git checkout <NOUV_VERSION>
$ bin/setup
$ yarn install --frozen-lockfile
$ yarn build
$ exit
# systemctl start hedgedoc.service
~~~

> **Note** : Ces commandes ne sont parfois pas suffisantes. Vous devez systématiquement lire les [notes de versions](https://github.com/hedgedoc/hedgedoc/releases).


## Configuration

HedgeDoc est configurable via le fichier `config.json` ou des variables d'environnement. 
L'application est compatible avec divers modes d'athentification (local, ldap, oauth2, saml, etc.) et divers stockages externes (s3, minio, lutim, etc.).
La documentation officielle fournit la liste de toutes les [options de configuration](https://docs.hedgedoc.org/configuration/).


## Utilisation

On peut utiliser en principe n'importe quel navigateur Web pour se connecter à HedgeDoc.
La syntaxe markdown compatible avec l'application fait l'objet d'[une page](https://docs.hedgedoc.org/references/hfm/) de la documentation officielle.
En gros, le standard [CommonMark](https://spec.commonmark.org/) est suivi avec quelques ajouts.

## FAQ

### À propos des logs

On accède à la journalisation des événements de l'application via journald :

~~~
# systemctl status hedgedoc.service
# journalctl --unit=hedgedoc
~~~

### Comment supprimer une note invitée ?

[HedgeDoc ne permet pas de supprimer de note créée en mode "invité" à l'heure actuelle](https://github.com/hedgedoc/hedgedoc/issues/2856). Pour la supprimer, il faut donc modifier directement la base de données.

Il faut par contre noter que l'ID de note utilisé dans l'URL n'est pas représenté dans la base de données. Il faut donc se baser sur d'autres données pour trouver la note (par exemple le titre de la note).