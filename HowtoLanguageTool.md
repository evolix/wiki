---
title: Howto LanguageTool
...


- Documentation : [https://dev.languagetool.org/]()
- Code source : [https://github.com/languagetool-org/languagetool]()
- Status de cette page : unstable / bullseye

[LanguageTool](https://languagetool.org/) est un correcteur multilingue de grammaire et de style.

# Serveur

Par défaut LanguageTool utilise le Cloud de son éditeur pour analyser votre grammaire, avec ces [conditions d'utilisation](https://languagetool.org/legal/privacy). Pour éviter de dépendre d'un service tiers, il est possible d'installer un serveur LanguageTool afin d'y accéder localement ou à distance.

## Installation

~~~(sh)
# curl -Lo /tmp/languagetool.zip https://languagetool.org/download/LanguageTool-stable.zip
# bsdtar -xvf /tmp/languagetool.zip -C /opt -'s|LanguageTool-[0-9\.]*|languagetool|'
# cat <<EOF > /etc/systemd/system/languagetool.service
[Unit]
Description=Languagetool HTTP server

[Service]
Type=simple
Restart=always
# XXX "--config /dev/null" is a workaronud from https://forum.languagetool.org/t/self-hosted-languagetool-server-and-chrome-add-on/9519/4
ExecStart=/usr/bin/java -cp /opt/languagetool/languagetool-server.jar org.languagetool.server.HTTPServer --port 8081 --allow-origin --config /dev/null
ExecReload=/bin/kill -HUP $MAINPID
# Successfull exit on SIGTERM for most java process, could also use `KillSignal=SIGQUIT`
SuccessExitStatus=143
DynamicUser=yes

[Install]
WantedBy=multi-user.target
EOF
# systemctl daemon-reload
# systemctl enable --now languagetool.service
~~~

# Extension Firefox

## Installation

L'extension Firefox LanguageTool s'installe à partir de cette page [https://addons.mozilla.org/en-US/firefox/addon/languagetool/]().

## Utilisation d'un serveur LanguageTool local

Afin que les données de votre correcteur d'orthographe ne passent par le Cloud de LanguageTool, il faut que vous installiez le serveur LanguageTool en local. Et ensuite configurer l'extension depuis Firefox : accédez aux préférences de l'extension, tout en bas de cette page, cliquez sur `Advanced settings (only for professional users)` puis configurez `LanguageTool server:` à `Local server (localhost)`

# TODO Extension LibreOffice
