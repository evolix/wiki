---
title: Howto Custom Live CD
...


* Documentation : <https://debian-live.alioth.debian.org/live-manual/unstable/manual/html/live-manual.en.html>

Ce Howto permet de créer un live CD personnalisé de Debian. Cela peut être utile dans le cas où vous devez utiliser une version plus récente d’un paquet lors de l’installation (à cause de périphérique non détectés par exemple).
Voir même pour les plus aguerri de créer votre propre distribution.

## Préparation

Nous aurons besoin des outils suivants pour la suite.

~~~{.bash}
sudo apt install live-build live-manual live-tools
~~~

On créé le répertoire dans lequel on va travailler.

~~~{.bash}
mkdir ~/livecd && cd ~/livecd
~~~

## Personnalisation du live CD

La commande suivante permettra de créer notamment le répertoire _config/_ qui contiendra toute la configuration d'un système de base pour le live CD :

~~~{.bash}
lb config
~~~

Nous proposons par exemple quelques options dont la définition des dépôts, de la version du système et de l'architecture processeur supporté, etc.

~~~{.bash}

lb config noauto \
--mode "debian" \
--system "live" \
--architectures "amd64" \
--distribution "stretch" \
--bootappend-live "boot=live components quiet noswap" \
--archive-areas "main contrib non-free" \
--security "true" \
--updates "true" \
--backports "false" \
--binary-images "iso-hybrid" \
--apt-indices "true" \
--apt-recommends "false" \
--apt-secure "true" \
--apt-source-archives "true" \
--linux-package "linux-image" \
--bootloader "syslinux" \
--debian-installer "live" \
--debian-installer-gui "false" \
--iso-application "handylinux" \
--parent-mirror-bootstrap "http://mirror.evolix.org/debian/" \
--parent-mirror-chroot "http://mirror.evolix.org/debian/" \
--parent-mirror-binary "http://mirror.evolix.org/debian/" \
--memtest "none" \
--clean \
--debug \
--verbose \
--source "false" \
"${@}"
~~~

Ces paramètres seront retranscrit dans les fichiers _config/binary_, _config/common_ et _config/build_.

*Nous conseillons de créer un fichier exécutable pour simplifier les prochaines modifications.*

## Ajout de paquets supplémentaires

Si les paquets à intégrer au live CD ne sont pas présents par défaut alors il suffit de les renseigner dans le fichier _config/package-lists/live.list.chroot_.

~~~
live-boot
live-config
live-config-systemd
debootstrap
vim
tmux
mutt
postfix
mailutils
~~~

## Ajout de fichiers et dossiers personnalisés

Le dossier _config/includes.chroot_ reprend l'architecture d'un système Linux. C'est à dire que l'on à pas besoin d'utiliser deboostrap ou chroot, toutes créations présent dans ce dossier seront automatiquement importé dans un dossier séparé lors de sa construction.

Par exemple si l'on veut importer la clé publique d'un utilisateur quelconque, on devra exécuter ces deux commandes :

~~~{.bash}
mkdir config/includes.chroot/root/.ssh
echo "ssh-rsa AAAAEAAAAD[...]ZDEF utilisateur@domaine.com" > config/includes.chroot/root/.ssh/authorized_keys
~~~

## Personnalisation approfondis

Dans le cas d'un besoin spécifique, il est possible d'utiliser un script personnalisé qui seront à inclure dans le dossier _config/hooks_. Par exemple, nous avons besoin de préconfigurer un agent mail pour une futur envoi de mail au démarrage du système. Voici à quoi cela peut ressembler :


~~~{.bash}
#!/bin/sh

set -e

sed -i 's/^relayhost.*/relayhost = pele.evolix.net/' /etc/postfix/main.cf
sed -i 's/^mydestination.*/mydestination = evolix.net, localhost, localhost.localdomain, localhost/' /etc/postfix/main.cf

systemctl restart postfix
~~~

_l'option `set -e` permet de stopper la construction de l'ISO si le constructeur rencontre une erreur. Dans un le cas d'un fichier absent par exemple._

## Modifier le menu du boot


Pour cela, il faut créer le dossier suivant si ce n'est pas déjà fait.

~~~
mkdir config/includes.binary/isolinux/
~~~

Au minimum, nous modifions le menu présent dans le fichier _config/includes.binary/isolinux/live.cfg_ pour faire booter directement notre Debian live.

~~~
default live
 
label live
  menu label ^Debian Live CD (Jessie x86)
  linux /live/vmlinuz
  initrd /live/initrd.img
  append boot=live config quiet splash
~~~

Pour modifier l'image d'arrière plan, il suffit de remplacer le fichier splash.png.

Pour réduire le temps de boot, on peut définir un faible timeout en modifiant le fichier _config/includes.binary/isolinux/isolinux.cfg_

~~~
include menu.cfg
default vesamenu.c32
prompt 0
timeout 10
~~~

## Création du live CD

Il suffit simplement d’exécuter la commande ci-dessous.

~~~
lb build
~~~

au bout de 15 minutes, une image ISO du live CD sera créée et se nommera live-image-amd64.hybrid.iso.

Si l'on souhaite y apporter des modifications alors on devra exécuter ces deux commandes pour repartir sur une bonne base.

~~~
lb clean
lb build
~~~

Pour partager le dossier de construction via git, le dossier en lui même est relativement lourd. Il est possible de lancer ces deux commandes qui vont permettre de réduire son poids:

~~~
lb clean --all
rm -r config/hooks/cache/*
rm -r config/hooks/chroot/*
~~~

## Gravure

Une fois l’image prête, pour la graver :

~~~
wodim -dev /dev/cdrom binary.iso
~~~

## Astuces

### Accélérer la construction via le cache

Nous recommandons d'installer et configurer un cache local avec le paquet `apt-cacher-ng`.

~~~
apt install apt-cacher-ng
/etc/init.d/apt-cacher-ng start
export http_proxy=http://localhost:3142/
lb config --apt-http-proxy http://127.0.0.1:3142/
~~~

La dernière commande va mettre à jour le fichier config/common.

### Supprimer totalement le cache 

Si l'on fait de grande modification, comme la volonté de changer de version, alors il faut supprimer les fichiers ci-dessous :

~~~
rm -f config/binary config/bootstrap config/build config/chroot config/common config/source
rm -Rf config/includes config/includes.bootstrap config/includes.source
~~~
