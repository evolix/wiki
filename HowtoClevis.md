---
categories: clevis chiffrement
title: Howto Clevis
...

* Github / Documentation : <https://github.com/latchset/clevis>

[Clevis](https://github.com/latchset/clevis) est un logiciel qui permet l'automatisation de déchiffrement. Il peut être utilisé pour déchiffrer automatiquement des fichiers ou déverrouiller des partitions chiffrées avec [LUKS](./HowtoLUKS)

Pour pouvoir déchiffrer automatiquement un élément, il a besoin d'être associé avec un “PIN”. Dans la terminologie de Clevis, un PIN est un plugin qui implémente une méthode de déchiffrement automatique. Ça peut être [Tang](./HowtoTang) pour faire ça via le réseau ou un TPM 2.0 (ou une clé FIDO2) ou une combinaison de plusieurs PINs en s'appuyant sur [le partage de clé secrète de Shamir](https://fr.wikipedia.org/wiki/Partage_de_cl%C3%A9_secr%C3%A8te_de_Shamir)


## Installation

~~~
# apt install clevis clevis-luks clevis-systemd

$ clevis --help

Usage: clevis COMMAND [OPTIONS]

  clevis decrypt               Decrypts using the policy defined at encryption time
  clevis encrypt sss           Encrypts using a Shamir's Secret Sharing policy
  clevis encrypt tang          Encrypts using a Tang binding server policy
  clevis luks bind             Binds a LUKS device using the specified policy
  clevis luks common functions 
  clevis luks edit             Edit a binding from a clevis-bound slot in a LUKS device
  clevis luks list             Lists pins bound to a LUKSv1 or LUKSv2 device
  clevis luks pass             Returns the LUKS passphrase used for binding a particular slot.
  clevis luks regen            Regenerate clevis binding
  clevis luks report           Report tang keys' rotations
  clevis luks unbind           Unbinds a pin bound to a LUKS volume
  clevis luks unlock           Unlocks a LUKS volume
~~~

> **Remarque** : il y a d'autres paquets installables en fonction des cas et des besoins. Par exemple  `clevis-tpm2` est nécessaire uniquement si on souhaite se servir du TPM de la machine. De même, pour le déchiffrement de la racine au démarrage, il faut le paquet `clevis-initramfs` pour que cette opération se fasse au boot.

## Configuration

### Partitions chiffrées avec LUKS

#### Binding d'un volume avec tang

Un binding est un lien entre un volume chiffré et un PIN pour Clevis. Il peut y avoir plusieurs bindings pour un volume donné.

Exemple : Création d'un binding d'une partition chiffrée `/dev/sdz` avec le serveur tang à l'addresse `http://192.0.2.10:42`

```
# clevis luks bind -d /dev/sdz tang '{"url": "http://192.0.2.10:42"}'
Enter existing LUKS password: 
The advertisement contains the following signing keys:

X4rKb0DNu7rpmrjHF0SbeQfMQ-mUoVHdUiiNOmiFx1c

Do you wish to trust these keys? [ynYN] y
```

> **Note** : Voir [la commande tang-show-keys dans le HowtoTang](./HowtoTang#afficher-lales-clé-actuelle) pour vérifier la clé affichée.

> **IMPORTANT** : Il faut bien s'assurer d'utiliser une IP et non un FQDN, notamment pour la partition racine. En effet, la résolution DNS ne semble pas être faite. Ça ne marchera donc pas.

#### Binding d'un volume avec un TPM 2.0

A Compléter

#### Liste des bindings

Il peut y avoir plusieurs bindings pour un volume LUKS
On peut utiliser la commande `clevis luks list` prévue à cet effet pour lister les bindings et leurs paramètres.


Exemple pour lister les bindings de `/dev/sdz`

```
# clevis luks list  -d /dev/sdz
1: tang '{"url":"http://192.0.2.10:42"}'
2: tang '{"url":"http://192.0.2.29:42"}'
```

#### Retrait d'un binding

> **REMARQUE IMPORTANTE** : Avant de retirer un binding, assurez-vous que les méthodes restantes (mot de passe, autre bindings avec clevis) puisse vous permettre de continuer de déverouiller le volume

Comme il peut y avoir plusieurs bindings, il faut récupérer son numéro de slot avec la commande [`clevis luks list`](#liste-des-bindings)


Exemple : Retirer le binding dans avec le serveur tang sur `http://192.0.2.29:42` (slot numéro 2) pour `/dev/sdz` 

```
# clevis luks list  -d /dev/sdz
1: tang '{"url":"http://192.0.2.10:42"}'
2: tang '{"url":"http://192.0.2.29:42"}'

# clevis luks unbind -d /dev/sdz -s 2
The unbind operation will wipe a slot. This operation is unrecoverable.
Do you wish to erase LUKS slot 1 on /dev/sdz? [ynYN] y
Enter any remaining passphrase: 

# clevis luks list  -d /dev/sdz
2: tang '{"url":"http://192.0.2.10:42"}'
```

#### Rotation de clés de tang

Après une rotation de clé d'un serveur [tang](./HowtoTang#rotation-de-clés), il faut mettre à jour le binding du slot

Exemple : Mise à jour du binding de `/dev/sdz` présent au *slot 1* : 

```
# clevis luks report -d /dev/sdz -s 1
The following keys are not in the current advertisement and were probably rotated:
  6cSAW9rr6KH4mKga3urSJatIHYHOibA28AKJQlVfIxQ
  aH0R-mkpdt7y72zoFEGCxZyeNMWZreoblk1H3zjIx2g
Do you want to regenerate the binding with "clevis luks regen -q -d /dev/sdz -s 1"? [ynYN] y
Regenerating binding (device /dev/sdz, slot 1):
Pin: tang, Config: '{"url":"http://192.0.2.10:42"}'
Binding regenerated successfully
```

> *Note* : Si les anciennes clés sont toujours disponibles, *Clevis* Ne demandera pas si on fait confiance à la nouvelle clé, car elle sera signée par l'ancienne déjà connue.


#### Ouvrir un volume bindé manuellement

Utiliser la commande habituelle `cryptsetup luksOpen` demandera une phrase de passe du volume. Ça n'utilisera pas les pins configurés avec Clevis
Il faut demander à Clevis de faire l'opération de déverrouillage s'il a des pins disponibles.

Pour cela on utilisera la commande `clevis luks unlock`

```
# clevis luks unlock -d /dev/sdz -n mon_volume_srv
## => On peut en suite utiliser le volume via /dev/mapper/mon_volume_srv
```

#### Ouvrir un volume bindé (non-root) automatiquement au démarrage

Il s'agit d'un disque qui ne supporte pas la racine système (/). On peut donc demander à systemd de faire cette opération automatiquement
Assurez-vous d'avoir le paquet `clevis-systemd` bien installé comme noté dans les pré-requis.

Il faut ensuite rajouter les options suivantes pour les volumes : 
* Options `auto,_netdev` dans `/etc/crypttab` pour le volume LUKS
* Options `auto,nofail,_netdev`  dans `/etc/fstab` pour monter la partition si vous avez directement formaté le volume en ext4


Exemple : 

```
# cat /etc/crypttab 
# <target name>	<source device>		<key file>	<options>
mon_volume_srv UUID="fe716a5a-28d0-450b-a241-2abe961b7968" none luks,discard,auto,_netdev


# cat /etc/fstab 
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/dev/mapper/mon_volume_srv /srv ext4 defaults,auto,nofail,_netdev  0 2

```

Si un ou plusieurs services ne doivent démarrer que lorsque la partition chiffrée est montée, alors on peut ajouter un "override" systemd. Exemple pour une partition `/srv` (il faut bien que `systemctl list-units` liste `srv.mount`) : 

```
[Unit]
After=srv.mount
```


#### Ouvrir un volume bindé (root/racine) automatiquement au démarrage

Il s'agit du cas particulier où on dévérouille la racine système (/). Cette opération va se produire durant la phase de démarrage du système, via les outils présents dans l'initramfs
Ainsi, assurez-vous d'avoir le paquet `clevis-initramfs` bien installé.


Dans le cas de l'utilisation de tang, le réseau est nécessaire. Par défaut, clévis fait un appel *DHCP*, pour obtenir une IP et ainsi pouvoir faire l'appel au serveur tang. 

En l'absence de service DHCP, il est donc nécessaire de donner les paramètres réseau au démarrage dans la ligne de commande (*cmdline*) donnée au kernel via le paramètre suivant : `ip=<IP>::<GATEWAY>:<NETMASK>:<HOSTNAME>:<IFACE>`

Exemple : `ip=192.0.2.42::192.0.2.254:255.255.255.0:foo:ens42` 


## Plomberie

## FAQ
