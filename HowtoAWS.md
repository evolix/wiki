---
categories: cloud amazon aws ec2 s3 awscli
title: Howto AWS
...

* Documentation officielle : <https://docs.aws.amazon.com/>
* Guide non officiel : [The Open Guide to Amazon Web Services](https://github.com/open-guides/og-aws)

[AWS](https://aws.amazon.com/) est un service de _cloud computing_.

# Services

Cette section présente différents services proposés par AWS.

## Instance Elastic Compute Cloud (EC2)

Une instance EC2 (Elastic Compute Cloud) est une machine virtuelle ou physique. Voir [la section dédiée](#Serveurs-EC2).

## Security Group

Un Security Group est équivalent à un pare-feu pour un ensemble d'instances (EC2, ElastiCache ou autres) qui y sont attachés.

# Serveurs EC2

* <http://aws.amazon.com/documentation/ec2/>
* <https://console.aws.amazon.com/ec2/>
* <http://alestic.com/2011/02/ec2-change-type>

## Présentation

Amazon Elastic Compute Cloud (Amazon EC2) est la partie système pure (*compute*).

## Opération sur les instances EC2

### Installation de l'API d'Amazon EC2

l'API Tools d'Amazon EC2 permet de gérer ses instances, AMI, etc… en ligne de commande.

Voici la procédure à appliquer pour l'installation des outils :

* Télécharger l'archive depuis cette page : <http://aws.amazon.com/developertools/351>
* Générer un couple certificat/clé privée depuis l'interface d'Amazon, qui permettra de s'authentifier sur le service :
   "Account" > "Identifiants de sécurité" > Onglet "Certificats X.509" > "Créer un nouveau certificat" et télécharger le certificat et la clé publique
* Placer le certificat et la clé privée à la racine du répertoire de l'API, une fois celui-ci décompressé
* Indiquer ensuite diverses informations dans les variables d'environnement, qu'on pourra placer dans son _.profile_ :

~~~
export EC2_HOME=~/ec2
export EC2_PRIVATE_KEY=pk-YOURKEYNAME.pem
export EC2_CERT=cert-YOURKEYNAME.pem
export JAVA_HOME=/usr/lib/jvm/java-6-sun-1.6.0.22/jre/
# accessoirement :
export EC2_URL=<https://ec2.eu-west-1.amazonaws.com>
~~~

* On peut tester que l'installation de l'API est correcte en listant par exemple les AMI disponible :

~~~
bin/ec2-describe-images -a

~~~
NOTE : pour exécuter les commandes EC2, il faut se trouver à la racine du répertoire d'installation.

### AMI Debian

Nous préférons utiliser les AMI préparées par l'équipe Debian. Leurs identifiants sont disponibles sur le [Wiki Debian](https://wiki.debian.org/Cloud/AmazonEC2Image), en particulier celles pour [Stretch](https://wiki.debian.org/Cloud/AmazonEC2Image/Stretch).

### Stocker une AMI sur un EBS

Voici la procédure à suivre :

* Démarrer une instance de l'AMI :

~~~
ec2-run-instances --instance-type m1.large --kernel aki-9800e5f1 ami-xxxxxx
~~~

* Créer un nouveau volume, de même taille que l'AMI, puis l'attacher à l'instance :

~~~
ec2-create-volume --size 10
ec2-attach-volume vol-xxxxxx --instance i-xxxxxx --device /dev/sdf
~~~

* Depuis l'instance, copier l'intégralité de l'AMI sur le volume :

~~~
# Un peu violent mais ça semble marcher.
# Un meilleure méthode serait peut-être de faire un « unbundle » de l'AMI et de la copier sur /dev/sdf.

# Éteindre un maximum de services pour limiter les écritures sur le disque
dd if=/dev/sda1 of=/dev/sdf
fsck /dev/sdf
~~~

* Détacher le volume le volume, et terminer l'instance :

~~~
ec2-detach-volume vol-xxxxxx --instance i-xxxxxx
ec2-terminate-instances i-xxxxxx
~~~

* Faire une snapshot du volume :

~~~
ec2-create-snapshot vol-xxxxxx
~~~

* Enregistrer le snapshot en tant qu'AMI :

~~~
ec2-register --name <name> -a x86_64 --root-device-name /dev/sda1 --block-device-mapping /dev/sda1=snap-xxxxxx
~~~

* Démarrer enfin une instance de cette nouvelle AMI :

~~~
ec2-run-instances --instance-type m1.large --kernel aki-9800e5f1 ami-xxxxxx
ec2-associate-address <IP> -i i-xxxxxx
~~~

### Partager une AMI Amazon EC2

En tant qu'utilisateur du service EC2 d'Amazon, on peut être amené à vouloir partager ses AMI (Amazon Machine Image) avec d'autres utilisateurs (amis, clients, etc) qui utilisent donc un autre compte Amazon. La procédure pour cela est très simple, et directement intégré à l'interface web :

* En premier lieu, il est nécessaire de connaître le Account Number du compte qui aura accès à notre AMI (il ne s'agit de l'email utilisé pour s'identifier). On trouvera cela dans Your account > Personal Information, en haut à droite de la page.
* Ensuite, il suffit de faire un clic droit sur notre AMI dans la vue correspondante, de sélectionner Edit permissions et d'ajouter un ou plusieurs Account Number dans les champs prévus à cet effet.
* Sur l'autre compte, l'AMI sera alors accessible dans la catégorie Private images.

### Gérer plusieurs sous-comptes sur Amazon AWS avec IAM

* <https://console.aws.amazon.com/iam/home>
* <http://aws.amazon.com/iam/>

IAM est un module qui permet de créer des sous-comptes (et même des groupes) et de leur donner des permissions spécifiques.
On peut ainsi créer plusieurs sous-comptes ayant un accès total, ou uniquement des permissions spécifiques (accès à certains modules : EC2, S3, etc. en read-only ou full access).

Ces utilisateurs peuvent avoir un access key (pour accéder à l'API Amazon AWS) et/ou un mot de passe (pour l'accès web).

Pour l'accès web, cela se passe sur une URL spécifique qui est : <https://account-identifier.signin.aws.amazon.com/console/ec2> 

### Migrer une instance EBS d'un compte à un autre

La migration de l'instance ne se fait pas correctement, il est nécessaire de migrer le snapshot puis de recréer l'instance sur le nouveau compte.
Vous devez au préalable snapshoter le volume de l'instance, et partager cette snapshot avec l'ID du nouveau compte (cf. « Partager une AMI Amazon EC2 », en adaptant aux snapshots).

Voici la procédure à suivre, avec $EC2_CERT et $EC2_PRIVATE_KEY contenant les clés du nouveau compte :

* Tout d'abord on ne peux pas avoir une instance qui utilise une snapshot partagée, il faut donc créer un volume à partir de cette snapshot, puis snapshoter ce volume :

~~~
bin/ec2-create-volume --size 10 --availability-zone us-east-1b --snapshot snap-xxxxxxxx
bin/ec2-create-snapshot vol-xxxxxxxx
~~~

* Créez ensuite l'instance à partir de la snapshot :

~~~
bin/ec2-register --name <NAME> -a x86_64 --root-device-name /dev/sda1 --block-device-mapping /dev/sda1=snap-xxxxxxxx
~~~

* Démarrez la nouvelle instance et assignez lui une nouvelle IP :

~~~
bin/ec2-run-instances --instance-type m1.large --kernel aki-9800e5f1 ami-xxxxxxxx
bin/ec2-associate-address <IP> -i i-xxxxxxxx
~~~

#### Installation des modules noyau

* Récupérez les modules pour votre noyau :

~~~
wget <http://ec2-downloads.s3.amazonaws.com/ec2-modules-2.6.18-xenU-ec2-v1.0-x86_64.tgz>
~~~

* Décompressez l'archive à la racine du système :

~~~
tar xzf /tmp/ec2-modules-2.6.18-xenU-ec2-v1.0-x86_64.tgz -C /
~~~

L'archive fournit le fichier vmlinuz, les modules .ko, et le /boot/System.map.

### Failed to execute /init Kernel panic

En cas de kernel panic suite à un reboot.


~~~
[    0.967754] Freeing unused kernel memory: 688k freed
[    0.971012] Failed to execute /init
[    0.973212] Kernel panic - not syncing: No init found.  Try passing init= option to kernel. See Linux Documentation/init.txt for guidance.
[    0.980516] Pid: 1, comm: swapper/0 Not tainted 3.2.0-4-amd64 #1 Debian 3.2.65-1+deb7u2
[    0.985497] Call Trace:
~~~

L'initramfs est corrompue. Pour corriger ceci :

* L'arrêter ;[[BR]]
* Détacher son EBS ;[[BR]]
* Attacher son EBS sur une machine OK en disque secondaire (xvdf).[[BR]]


~~~
# mount /dev/xvdf /mnt/
# mount -t proc none /mnt/proc
# mount -o bind /dev /mnt/dev
# mount -o bind /dev/pts /mnt/dev/pts
# mount -t sysfs sys /mnt/sys
# chroot /mnt/ /bin/bash
# dpkg-reconfigure linux-image-3.2.0-4-amd64
# exit
# umount /mnt/dev/pts
# umount /mnt/dev
# umount /mnt/proc
# umount /mnt/sys
# umount /mnt
~~~

* Détacher l'ebs de la machine de rescue ;[[BR]]
* Attacher l'ebs sur la machine planté (xvda) ;[[BR]]
* Démarrer la VM.

Pour éviter que l'initramfs soit corrompue, lors d'un upgrade kernel, il faut s'assurer que /var/tmp soit bien en exec :

~~~
mount -oremount,exec /var/tmp
dpkg-reconfigure linux-image-3.2.0-4-amd64
~~~

### Agrandir la partition montée sur `/`

Lorsqu'on fait un `lsblk`, on voit que la partition montée sur `/` est la première. On pourrait alors se dire qu'il n'est pas possible d'agrandir la partition.

~~~
NAME     MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
xvda     202:0    0   20G  0 disk
├─xvda1  202:1    0  7.9G  0 part /
├─xvda14 202:14   0    3M  0 part
└─xvda15 202:15   0  124M  0 part /boot/efi
~~~

En réalité, la partition 1 est en dernier ! On peut le vérifier avec les secteurs de début et de fin des partitions.

~~~
# fdisk -l /dev/xvda
[…]
Device       Start      End  Sectors  Size Type
/dev/xvda1  262144 16775167 16513024  7.9G Linux filesystem
/dev/xvda14   2048     8191     6144    3M BIOS boot
/dev/xvda15   8192   262143   253952  124M EFI System
[…]
~~~

Dans ce cas, on peut agrandir la partition 1 sans problème.

## Configuration SSH

**ATTENTION** Par défaut, le serveur SSH est configuré globalement avec le paramètre : `PasswordAuthentication no`. Cette ligne doit être commentée, si on utilise pas d'authentification par clé SSH.

Il est recommander de s'authentifier avec une clé SSH. Pour ajouter une clé :

1. taper _key pairs_ dans la barre de recherche, _Key pairs_ devrait apparaître en tant que fonctionnalité EC2 ;
1. on tombe sur la liste des clés SSH, cliquer sur _Actions_ puis _Import key pair_.

Lors de la création d'une instance EC2, il sera possible de choisir la clé qui sera utilisée.

Pour ajouter une clé avec Ansible :

~~~ { .yaml }
- name: Create key pair
  amazon.aws.ec2_key:
    aws_access_key: "{{ aws_access_key }}"
    aws_secret_key: "{{ aws_secret_key }}"
    region: "{{ ec2_region }}"
    name: "{{ item.value.name }}"
    key_material: "{{ item.value.ssh_key }}"
    state: present
  with_dict: "{{ users }}"
~~~

Le dictionnaire `users` est de la forme :

~~~ { .yaml }
users:
  user1:
    name: user1
    ssh_key: 'ssh-ed25519 …'
  user2:
    name: user2
    ssh_key: 'ssh-ed25519 …'
~~~

## Screenshot via AWS

Via l'interface web AWS, on a accès à un screenshot de l'écran actuel de la VM (en read-only).

Attention ! Il faut se méfier de ce screenshot qui peut ne pas réfléter l'état réel de la machine... exemple typique au démarrage d'une machine, vous verrez l'écran du "BIOS QEMU" avec "Loading initial ramdisk ..."
pourtant la VM n'est pas forcément dans cet état, il est probable qu'en fait elle est en cours de boot ou pire, que son boot a échoué pour une raison ou une autre POURTANT VOUS NE VERREZ PAS CELA SUR LE SCREENSHOT !!

## ELB Elastic Load Balancer

« L'équilibrage de charge » fonctionne sur le principe suivant.

- On crée un nouveau « Équilibreur de charge d'application » ;
- On choisi « l'écouteur » (HTTP ou HTTPS) (équivalent frontend) ;
- Si on a choisi HTTPS, on choisit un certificat (que l'on peut créer via Amazon) ;
- On rattache un groupe de sécurité (règles pare-feu) ;
- On crée un nouveau groupe cible qui contient la liste des backend avec le choix du protocole, HTTP, HTTPS ou TCP. On pourra spécifier une URI a surveiller ;
- On met les machines que l'on veut dans ce groupe.

Une fois que LB est en place, on pourra récupérer son nom de domaine pour le rattacher via CNAME au nom de domaine du site concerné.  
Exemple de nom : example-1270308724.eu-central-1.elb.amazonaws.com  
Exemple de CNAME : www.example.com CNAME example-1270308724.eu-central-1.elb.amazonaws.com.

Sur les frontaux web il faudra penser à mettre en place le mod remoteip pour Apache ou `set_real_ip_from` pour Nginx.

## SMTP

Les instances EC2 sont limitées en envoi de mail par le port 25. Il est possible de demander la levée de ces limites : <https://aws.amazon.com/fr/premiumsupport/knowledge-center/ec2-port-25-throttle/>

## Meta-données

Une instance EC2 peut accéder à un ensemble de méta-données sur elle-même.
Elles sont accessible en HTTP par nom d'hôte (`instance-data`) ou adresse IP (`169.254.169.254`).

~~~
$ wget -q -O - http://instance-data/latest/meta-data/instance-id
i-0a534900aa4c582d7
~~~

La liste des informations accessibles :

* ami-id
* ami-launch-index
* ami-manifest-path
* block-device-mapping/
* events/
* hostname
* identity-credentials/
* instance-action
* instance-id
* instance-type
* local-hostname
* local-ipv4
* mac
* metrics/
* network/
* placement/
* profile
* public-hostname
* public-ipv4
* public-keys/
* reservation-id
* security-groups

Il est également possible d'y accéder via un outil plus haut niveau, généralement préinstallés sur les images d'Amzon, ou disponibles via le paquet `cloud-guest-utils`.

~~~
$ ec2metadata
ami-id: ami-0539351fee4a5a3b1
ami-launch-index: 0
ami-manifest-path: (unknown)
ancestor-ami-ids: unavailable
availability-zone: ap-southeast-1a
block-device-mapping: ami
ebs1
root
instance-action: none
instance-id: i-0a534900aa4c582d7
instance-type: t2.medium
local-hostname: ip-172-27-22-121.ap-southeast-1.compute.internal
local-ipv4: 172.27.22.121
kernel-id: unavailable
mac: unavailable
profile: default-hvm
product-codes: unavailable
public-hostname: ec2-18-164-130-102.ap-southeast-1.compute.amazonaws.com
public-ipv4: 18.164.130.102
public-keys: ['ssh-rsa […]']
ramdisk-id: unavailable
reserveration-id: unavailable
security-groups: foo-bar
user-data: unavailable
~~~

Il est possible de restreindre la commande :

~~~
$ ec2metadata --instance-id
i-0a534900aa4c582d7
~~~

## FAQ

### Test accessibilité réseau

Pour vérifier l'accès à Amazon : <http://ec2-reachability.amazonaws.com/> et vérifier l'affichage de l'icône "vert".

# AWS CLI

Amazon propose un outil en ligne de commande pour interagir avec ses outils AWS.

* Documentation : https://docs.aws.amazon.com/cli/latest/index.html

## Installation

~~~
# apt install awscli
~~~

> On peut aussi installer `awscli` avec `pip`.

## Configuration

La commande `configure` de stocker dans un fichier les valeurs par défaut pour 4 options courantes.

~~~
$ aws configure
AWS Access Key ID [None]: 
AWS Secret Access Key [None]: 
Default region name [None]: 
Default output format [None]: 
~~~

Si vous renseignez vos identifiants, ils seront stockés dans `~/.aws/credentials`. Vérifiez bien les permissions d'accès à ce fichier car ces identifiants donnent accès à l'ensemble des actions possibles sur AWS.

Il est possible de créer un ou plusieurs profils pour pré-enregistrer des options par compte. Il faudra ensuitre indiquer le nom du profil à utiliser au lancement des commandes.

~~~
$ aws --profile XXX configure
[…]
$ aws --profile XXX [options] <command> <subcommand>
~~~

L'option `--output` permet d'indiquer si on souhaite une sortie en `text`, `xml` ou `json`.

Lors des appels à des commandes, il peut être nécessaire de fournir l'identifiant de l'instance, le nom de la région… Si la commande est exécutée depuis une instance AWS, certaines de ces infos sont dynamiquement accessibles depuis [des API de méta-données](#meta-données).

# Amazon S3

* Présentation : <https://aws.amazon.com/s3/>
* Documentation : <https://aws.amazon.com/documentation/s3/>

## Présentation

Amazon Simple Storage Service (Amazon S3) fournit un espace de
stockage via une interface REST (donc web). On peut y accéder via
l'espace client d'Amazon ou via des clients.

C'est de l'object storage. Donc chaque fichier qu'on met dedans est un
objet. Cet objet contient les données et les meta-données. On ne peut
pas créer de dossier mais les noms de objet peuvent contenir des slash
("/") et ainsi on peut créer une arborescence virtuelle. En réalité
tous les objets sont stockés au même niveau. On stocke les objets dans
un *bucket*.

Pour plus de détails voir la [page wikipedia en
anglais](https://en.wikipedia.org/wiki/Object_storage).

## Clients

* L'outil cli d'amazon <https://aws.amazon.com/cli/>
* S3cmd <https://github.com/s3tools/s3cmd>
* Minio Client (mc) <https://github.com/minio/mc>
* Rclone <https://rclone.org/s3/>
