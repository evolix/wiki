**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Serveur DELL

Serveurs DELL PowerEdge.

## Disques SSD

<http://www.dell.com/learn/fr/fr/frbsdt1/help-me-choose/hmc-hard-drive-12g?c=fr>

### SSD DELL Value (SATA MLC)

Bullshit DELL : "Les disques durs SSD SATA 2,5 pouces Dell Value sont adaptés aux clients dotés de charges de travail mixtes, telles qu'applications d'entreposage de données, de bureau virtuel et HPC. Ils sont disponibles avec des capacités de 100 Go et de 200 Go, et ils ont été conçus pour être compatibles avec les serveurs Dell et pour s'y intégrer facilement."

<http://aaltonen.co/2011/12/30/dells-value-mlc-ssd-specifications/>

Il s'agit de disques Samsung : <http://www.storagereview.com/samsung_ssd_sm825_enterprise_ssd_review>

<http://www.samsung.com/us/business/oem-solutions/pdfs/SM825_Product%20Overview.pdf>

<http://www.samsung.com/us/business/oem-solutions/pdfs/SSD-FAQs-2012.pdf>

### SSD DELL Mainstream (SAS SLC)

Bullshit DELL : "Les disques durs SSD SAS 2,5 pouces Dell Mainstream sont une bonne solution pour les clients qui gèrent des bases de données OLTP (Online Transactional Processing), des systèmes d'aide à la décision pour les entreprises et des applications de mise en mémoire cache des données, des utilisations susceptibles de traiter des charges de travail en écriture extrêmement intensives. Ces disques sont disponibles avec des capacités de 200 Go et de 400 Go. Ils ont été conçus pour être compatibles et s'intégrer facilement avec les systèmes de stockage d'entreprise de niveau 0 (existants ou nouveaux) notamment les serveurs, les systèmes de stockage à connexion directe et les solutions de stockage en réseau."

Il s'agit de disques Sandisk / Pliant : <http://www.sandisk.com/products/ssd/sas/> / <http://www.storagesearch.com/plianttech.html>

### SSD Fusion-IO

<http://www.fusionio.com/>


## Mise à jour des firmwares

Pour mettre à jour les différents firmwares (SATA, BIOS, iDRAC) des serveurs Dell, la méthode la plus commode est de le faire via l'iDRAC.

### Par l'iDRAC

Une fois connecté à l'iDRAC, allez dans le menu : Maintenance -> System Update -> Manual Update. Mettre le champ "Location Type" à "HTTP", renseignez l'addresse IP d'un serveur HTTP dans le "HTTP Address", vérifier maintenant la connexion au serveur HTTP et sa bonne configuration en cliquant sur "Test network connexion". 

Un serveur HTTP bien configuré pour servir des firmwares DELL a besoin :

- de retourner un code 200 sur la racine du serveur (par exemple en ayant fait `touch index.html ` dans la racine du serveur web)
- d'un [fichier "catalogue" de Dell](https://www.dell.com/support/kbdoc/en-us/000132986/dell-emc-catalog-links-for-poweredge-servers) nommé Catalog.xml ou Catalog.xml.gz ou catalog.xml ou Catalog.xml.gz ou Catalog.xml ou catalog.xml
- des firmwares en question, au bon emplacement ; pour être sûr de la hiérarchie dans laquelle doit se trouver chacun des firmwares, on peut suivre les logs du serveur HTTP lors d'une demande de téléchargement dû dit firmware dans l'iDRAC

Il est aussi possible de téléverser directement un fichier `.EXE` sur l'iDRAC en utilisant le "Location Type" "Local", mais c'est plus fragile que d'utiliser le client HTTP de l'iDRAC.

### Par clé USB

Pour mettre à jour le BIOS, il faut télécharger la dernière version disponible sur la page de l'appareil sur le site de Dell. 

Prendre le .exe bios exécutables Ex : <http://www.dell.com/support/drivers/fr/fr/frbsdt1/DriverDetails/Product/poweredge-1950?driverId=G50YP&osCode=WNET&fileId=3000127357&languageCode=fr&categoryId=BI>. Puis télécharger FreeDos sur <http://chtaube.eu/computers/freedos/bootable-usb/#download>. Prendre la version correspondant à la taille de la clé usb qui sera utilisée. Décompresser l'archive, par exemple : bunzip2 FreeDOS-1.1-memstick-2-256.img.bz2. Utiliser dd pour copier l'image sur la clé comme suit : dd i=/FreeDOS-1.1-memstick-2-256.img of=/dev/sdx bs=512k. Placer le .exe téléchargé précédemment sur la racine de la clé USB.

Grâce au boot menu de la machine, boot sur la clé. Lorsqu'il le sera demandé, sélectionner de boot sur FreeDOS sans drivers.
Lorsque le prompt DOS sera affiché, entrer le nom du .exe et suivre les instructions à l'écran.
