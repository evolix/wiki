# Howto Supervisor

C'est un système qui permet à ses utilisateurs de surveiller et de contrôler un certain nombre de processus. il est destiné à être utilisé pour contrôler les processus liés à un projet ou à un client, et est conçu pour démarrer comme tout autre programme au démarrage.

<http://supervisord.org/>

~~~
# apt install supervisor
~~~

## Configuration

Fichier /etc/supervisor/supervisord.conf

Par exemple pour écouter sur le port TCP/9001 (sans authentification) :

~~~
[inet_http_server]
port = 127.0.0.1:9001
~~~

On peut ensuite faire :

~~~
$ supervisorctl -s http://127.0.0.1:9001 status
~~~

### Exemple

Si l'on souhaite surveiller le process /home/bar/foo

~~~
# cat /etc/supervisor/conf.d/foo.conf
[program:foo]
user=bar
command=/home/bar/foo
autostart=true
autorestart=true
stderr_logfile=/home/bar/log/foo.error.log
stdout_logfile=/home/bar/log/foo.log
~~~

~~~
# supervisorctl reread 
# supervisorctl update
~~~

~~~
# supervisorctl
foo                    RUNNING   pid 42042, uptime 0:42:42
supervisor> stop foo
foo: stopped
supervisor> start foo
foo: started
supervisor> help

default commands (type help <topic>):
=====================================
add    exit      open  reload  restart   start   tail   
avail  fg        pid   remove  shutdown  status  update 
clear  maintail  quit  reread  signal    stop    version
~~~

## Modifier sa verbosité 

On peut indiquer que l'on veut seulement les informations liées aux erreurs :

~~~
[supervisord]
loglevel=error
~~~

## En cas de soucis

On vérifie si le process tourne encore

~~~
# ps -ef | grep supervisord
~~~

Si c'est le cas on peut le `kill` (et `kill -9` si ça n'a pas suffit).

Si ce n'est plus le cas, il se peut qu'il soit nécessaire de supprimer
la socket

~~~
# unlink /var/run/supervisor.sock
~~~
