# Howto date

## Commande date

### Avoir la date actuelle

~~~
$ date
samedi 2 avril 2005, 16:59:42 (UTC+0200)
$ date +"%d-%m-%Y %H:%M:%S"
02-04-2005 16:59:42
$ date +"%d-%m-%y %r"
02-04-05 04:59:42 PM
$ date +%s
1112453982
$ date -R
Sat, 02 Apr 2005 16:59:42 +0200
$ TZ=America/Montral date
samedi 2 avril 2005, 09:59:42 (UTC-0500)
~~~

Notes :

* `date` : donne la date locale
* `date -u` : donne la date UTC
* `TZ=America/Montral date` : donne la date en fonction de la variable TZ
* `date -R` : donne la date au format RFC822

### Changer la date

~~~
# date -s "01/19/2005 04:27:00"
~~~

### Convertir une date sous Debian

On utilise `date -d` puis l'on peut avoir les mêmes options d'affichage que vu plus haut :

À partir d'un timestamp :

~~~
$ date -d @1112453982
samedi 2 avril 2005, 16:59:42 (UTC+0200)
$ date -d @1112453982 +"%d-%m-%Y %H:%M"
02-04-2005 16:59
~~~

À partir d'une date à différents formats :

~~~
$ date -d 20010930
dimanche 30 septembre 2001, 00:00:00 (UTC+0200)
$ date -d "2001-09-30 13:37:42"
dimanche 30 septembre 2001, 13:37:42 (UTC+0200)
$ date -d "next Thursday"
jeudi 7 septembre 2017, 00:00:00 (UTC+0200)
date -d "next Month"
mardi 3 octobre 2017, 23:11:39 (UTC+0200)
~~~

On peut aussi faire un `date -d "DATE +1day/month/etc.` :

~~~
$ date -d "1970-01-01 GMT+2 + 1220367600 seconds"
~~~

D'où les astuces pour connaître la date dans 20 jours par exemple :

~~~
$ date -d "$(date +%Y-%m-%d) +20 days"
samedi 23 septembre 2017, 23:12:51 (UTC+0200)
$ date -d "+20 days"
samedi 23 septembre 2017, 23:12:51 (UTC+0200)
~~~

### Convertir une date sous OpenBSD

L'option `-d` n'existe pas pour `date` sous OpenBSD.

Il faut utiliser l'option `-j` pour indiquer que l'on veut parser une date, et l'option `-f` pour indiquer le format d'entrée. On peut ensuite indiquer le format de sortie souhaité comme sous Debian avec `+"%…"`. Il n'est par contre pas possible de faire des calculs de date.

~~~
$ TZ=:Zulu date -jf "%Y%m%d%H%M%SZ" "20210214150230Z" +"%d-%m-%Y %H:%M"
14-02-2021 15:02
~~~