---
categories: sysadmin Zimbra zimbra Nextcloud nextcloud sync synchro synchronisation vdirsyncer
title: Howto Vdirsyncer
...

* Documentation: <https://vdirsyncer.pimutils.org/en/stable/>

Vdirsyncer est un outils de synchronisation de calendriers et de carnets d'adresses avec une gestion de droits et de priorités en cas de conflits.


## Installation

Installer les paquets sur votre poste :

~~~
# apt install vdirsyncer libpam-systemd
~~~

Sous Bullseye, une [incompatibilité avec python-click](https://bugs.debian.org/1008502) rend le paquet inutilisable, mais les versions compatibles peuvent être installé depuis [notre dépôt backports](/HowtoDebian/Backports).

~~~
# apt install python3-click/bullseye-backports python3-click-threading/bullseye-backports
~~~

Avec votre compte Unix, créer le répertoire de configuration de Vdirsyncer :

~~~
$ mkdir -vp ~/.config/vdirsyncer/
~~~

## Configuration

Pour synchroniser un calendrier et un carnet d'adresse d'un serveur Zimbra vers un serveur Nextcloud, il faut spécifier dans la configuration les identifiants et les noms des calendriers et carnets d'adresses des deux instances.

Renseigner les variables shell suivantes :

~~~
ZIMBRA_URL=
ZIMBRA_USER=
ZIMBRA_MDP=
ZIMBRA_NOM_CALENDRIER=Calendar
ZIMBRA_NOM_CONTACT=Contacts
NEXTCLOUD_URL=
NEXTCLOUD_USER=
NEXTCLOUD_MDP=
NEXTCLOUD_NOM_CALENDRIER=personal
NEXTCLOUD_NOM_CONTACT=contacts
~~~

Puis, créer le fichier de configuration :

~~~
$ cat > ~/.config/vdirsyncer/config <<EOF
[general]
# Emplacement où se trouve la base des métadonnées
status_path = "~/.local/state/vdirsyncer/status/"

#### Calendar

[pair pull2push_calendar]
a = "remote_zimbra"
b = "remote_nextcloud"
conflict_resolution = "a wins"
collections = null

[storage remote_zimbra]
type = "caldav"
# On précise le calendrier :
url = "$ZIMBRA_URL/dav/$ZIMBRA_USER/$ZIMBRA_NOM_CALENDRIER"
username = "$ZIMBRA_USER"
password = "$ZIMBRA_MDP"
# Pas d'écriture sur cette instance :
read_only = true

[storage remote_nextcloud]
type = "caldav"
# On précise le calendrier :
url = "$NEXTCLOUD_URL/remote.php/dav/calendars/$NEXTCLOUD_USER/$NEXTCLOUD_NOM_CALENDRIER/"
username = "$NEXTCLOUD_USER"
password = "$NEXTCLOUD_MDP"

#### Contacts

[pair pull2push_contacts]
a = "remote_zimbra_contacts"
b = "remote_nextcloud_contacts"
conflict_resolution = "a wins"
collections = null

[storage remote_zimbra_contacts]
type = "carddav"
# On précise le nom du carnet
url = "$ZIMBRA_URL/dav/$ZIMBRA_USER/$ZIMBRA_NOM_CONTACT/"
username = "$ZIMBRA_USER"
password = "$ZIMBRA_MDP"
# Pas d'écriture sur cette instance :
read_only = true

[storage remote_nextcloud_contacts]
type = "carddav"
# On précise le nom du carnet
url = "$NEXTCLOUD_URL/remote.php/dav/addressbooks/users/$NEXTCLOUD_USER/$NEXTCLOUD_NOM_CONTACT/"
username = "$NEXTCLOUD_USER"
password = "$NEXTCLOUD_MDP"
EOF
~~~

## Tester

Vérifier la syntaxe de la configuration (**qu'il faut lancer à chaque modification du fichier de configuration**) :

~~~
vdirsyncer discover
~~~

La sortie devrait être :

~~~
Discovering collections for pair pull2push_calendar
remote_zimbra:
  - "Calendar"
remote_nextcloud:
  - "personal" ("Personal")
Saved for pull2push_calendar: collections = null
Discovering collections for pair pull2push_contacts
remote_zimbra_contacts:
  - "Contacts"
remote_nextcloud_contacts:
  - "contacts" ("Contacts")
Saved for pull2push_contacts: collections = null
~~~

On obtient 2 paires `pull2push_calendar` et `pull2push_contacts`

## Importation manuelle

Lancer la copie d'une seule paire :

~~~
vdirsyncer sync pull2push_contacts
~~~

## Importation automatique

Lancer le timer systemd fournit par la paquet `vdirsyncer` avec un compte [utilisateur](https://wiki.evolix.org/HowtoSystemd#systemd-par-utilisateur) :

~~~
# loginctl enable-linger foo
# su - foo
$ systemctl --user enable vdirsyncer.timer
$ systemctl --user start vdirsyncer.timer
~~~

Ainsi, la commande `vdirsyncer sync` sera lancée toutes les 5 minutes.
