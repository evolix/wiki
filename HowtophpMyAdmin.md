# Howto phpMyAdmin

~~~
# apt install phpmyadmin
~~~

> Note : Pour Debian 10 (Buster), phpMyAdmin est seulement disponible dans les [backports](/HowtoDebian/Backports). Voir la section [ci-dessous](#utiliser-la-version-des-backports) pour les utiliser.

Pour empêcher un accès trop simple, on va désactiver la configuration globale :

~~~
# a2disconf phpmyadmin
Conf phpmyadmin disabled.
To activate the new configuration, you need to run:
  service apache2 reload
~~~

> Note : Pour Debian 7, `a2disconf` n'existe pas, il faut supprimer le lien symbolique `/etc/apache2/conf.d/phpmyadmin.conf`

On pourra ensuite ajouter la configuration phpMyAdmin au sein d'un VirtualHost précis, et notre astuce est de renvoyer `Alias /phpmyadmin vers un chemin standard (pour prendre le pas sur la configuration standard) et d'utiliser un _Alias_ non standard (exemple _/pma-42_). On pourra aussi ajouter en plus une restriction par IP :

~~~
    Alias /phpmyadmin /var/www
    Alias /pma-42 /usr/share/phpmyadmin/
    Include /etc/phpmyadmin/apache.conf
    <Directory /usr/share/phpmyadmin/>
        Require all denied
        Require ip 192.0.2.42
        Require ip 2001:db8::42
    </Directory>
~~~

Note : on veille à ne pas toucher au fichier `/etc/phpmyadmin/apache.conf` qui contient des instructions importantes pour la sécurité. Le garder intact facilite aussi les futures mises à jour du package.


## Configuration

Par défaut nous n'utilisons pas de configuration avancée, on s'assure donc de désactiver les directives pour s'appuyer sur une base de données :

~~~
# rm /etc/phpmyadmin/config-db.php
~~~

### Configuration avancée

Il est préférable de modifier la configuration via l'interface web en ajoutant /setup/ à l'URL de phpMyAdmin

Cela va demander un identifiant que l'on pourra configurer via /etc/phpmyadmin/htpasswd.setup

Attention, pour sauvegarder sa configuration, il est nécessaire de lancer la commande pma-configure avant :

~~~
# pma-configure 
Unsecuring phpMyAdmin installation...
Setup script can now write to the configuration file.

Do not forget to run /usr/sbin/pma-secure after configuring,
~~~

Une fois les changements sauvegardés dans l'interface /setup/ (par exemple on pourra ajouter plusieurs serveurs MySQL) on lancera la commande pma-secure :

~~~
# pma-secure 
Securing phpMyAdmin installation...
Setup script won't be able to write configuration.
~~~

### Configuration pour utilisation derrière un reverse-proxy

Si l'on utilise phpMyAdmin derrière un reverse-proxy, il faudra probablement forcer la variable de configuration PmaAbsoluteUri
Par exemple si le serveur HTTP "caché" tourne sur le port TCP/8080, mais que celui-ci n'est pas accessible directement, on devra forcer
PmaAbsoluteUri pour utiliser une URL sans 8080. Cela se passe dans le fichier de configuration /etc/phpmyadmin/config.inc.php :


~~~
$cfg['PmaAbsoluteUri'] = '<http://example.com/pma-42';>
~~~

## Configuration de phpMyAdmin pour la connexion  à un serveur distant

Si on souhaite utiliser phpMyAdmin pour se connecter a des bases mysql sur un serveur distant, il faut ajouter un serveur dans la configuration de phpMyAdmin.

Il faut modifier le fichier */etc/phpmyadmin/config.inc.php* et ajouter cette configuration pour un nouveau serveur :

~~~
/* Authentication type */
$cfg['Servers'][$i]['auth_type'] = 'cookie';
/* Server parameters */
$cfg['Servers'][$i]['host'] = 'IP_DU_SERVEUR';
$cfg['Servers'][$i]['connect_type'] = 'tcp';
$cfg['Servers'][$i]['compress'] = false;
~~~

## FAQ

### Erreur « Le fichier ne peut pas être lu »

Lors de l'upload d'un fichier (pour un import par ex) : ` Le fichier ne peut être lu.`

Vérifier si _upload_tmp_dir_ est bien dans la liste et contenu dans _open_basedir_, et que le répertoire ait les bon droits.


### Erreurs : mysqli_real_connect() et La connexion au controluser tel que défini dans votre configuration a échoué.

Il faut que le fichier */etc/phpmyadmin/config-db.php* ne tente PAS de se connecter à la base phpmyadmin, il doit continir une configuration de se style :

~~~
<?php
$dbuser='';
$dbpass='';
$basepath='';
$dbname='';
$dbserver='';
$dbport='';
$dbtype='mysql';
~~~


### Erreur de la fonction count() avec php7.3 ou php7.2 sur une Debian 9 / 10

Si l'on a ce type d'erreur :

**Warning in ./libraries/sql.lib.php#613
count(): Parameter must be an array or an object that implements Countable**

Le patch est a appliquer dans le fichier `/usr/share/phpmyadmin/libraries/sql.lib.php` il faut modifier la ligne 613 

>
> ~~~
> || (count($analyzed_sql_results['select_expr'] == 1)
> ~~~
>
> par ceci :
>
> ~~~
> || ((count($analyzed_sql_results['select_expr']) == 1)
> ~~~

Le bug est présent sur phpMyAdmin 4.6, c'est corrigé en phpMyAdmin 4.8.


### Page blanche pour la version PhpMyAdmin de bullseye-backports en PHP 8.1

Vérifier si la dépendance `php-mbstring` est bien installée.

## Utiliser la version des backports

Puisque Bullseye (Debian 11) a été publiée, la version backports de Buster (Debian 10) ne bougera pas plus que celle de Bullseye, donc les dépendances à tirer des backports ne devraient pas changer.

Vérifier que les backports sont bien dans `/etc/apt/sources.list` :

~~~
deb http://mirror.evolix.org/debian buster-backports main
~~~

Mettre-à-jour `phpmyadmin` :

~~~
apt update
apt install -t buster-backports phpmyadmin
~~~
