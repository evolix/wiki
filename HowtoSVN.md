**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto SVN

## Procédure de mise en place d'un dépôt

On crée le répertoire qui contiendra les dépôts SVN :

~~~
mkdir /home/svn/
~~~

On crée également un groupe _svn_ qui contiendra les utilisateurs qui auront le droits d'accéder au dépôt :

~~~
addgroup -q svn
~~~

On peut ensuite créer un premier dépôt nommé _foo_ :

~~~
svnadmin create /home/svn/foo
~~~

Il est maintenant nécessaire d'adapter les droits :

~~~
chgrp -R svn /home/svn/
chmod 750 /home/svn/

chmod -R g+rwX /home/svn/foo/
chmod -R g+s /home/svn/foo/db/
~~~

Enfin, on peut ajouter les utilisateurs ayant accès au svn dans le groupe svn :

~~~
adduser -q jdoe svn
~~~

Il est nécessaire que leur umask soit positionné à la valeur 007, on l'indiquant par exemple dans leur ~/.profile.

~~~
echo umask 007 >> ~jdoe/.profile
~~~

## Authentification sans compte UNIX

Éditer le fichier svnserve.conf à votre convenance, typiquement, droits d'écriture en anonyme ou pas, etc. Mais surtout, indiquez le fait qu'on utilise une base de données des utilisateurs et mot de passes dans un fichier passwd.

~~~
[general]
anon-access = none
auth-access = write
password-db = passwd
realm = Nom du repo
~~~

Ajouter les utilisateurs et leur mot de passes dans le fichier conf/passwd :

~~~
[users]
# harry = harryssecret
# sally = sallyssecret
toto = tata
~~~

## SVN+HTTP

Installation et activation des modules requis :

~~~
# aptitude install libapache2-svn
# a2enmod dav_svn
~~~

Configuration du VHost :

~~~
        <Location /svn>
                DAV svn
                SVNParentPath /home/svn
                AuthType Basic
                AuthName "SVN"
                AuthUserFile /home/svn/.htpasswd
                AuthzSVNAccessFILE /home/svn/.authz
                Require valid-user
        </Location>
~~~

Dans le cas de plusieurs dépôts, on vérifiera de bien utiliser la directive "SVNParentPath".
Pour un seul dépôt, la directive "SVNPath" peut être utilisée, et dans ce cas doit pointer directement sur le dépôt en question.

## SVN+HTTP et authentification LDAP

Même pré-requis que précédemment, avec en plus :

~~~
# a2enmod authnz_ldap
~~~

Et la configuration suivante :

~~~
        <Location /svn>
                DAV svn
                SVNParentPath /home/svn
                AuthType Basic
                AuthName "SVN"
                AuthBasicProvider ldap
                AuthzLDAPAuthoritative on
                AuthLDAPURL "ldap://127.0.0.1:389/dc=example,dc=com?uid?sub?(objectClass=*)"
                AuthLDAPGroupAttribute memberUid # Chercher l'attribut memberUid au lieu de uniqueMember
                AuthLDAPGroupAttributeIsDN off # Ne pas chercher un DN complet (uid=user,dc=example,dc=com) dans l'attribut memberUid mais uniquement l'uid
                Require ldap-group cn=svn,ou=group,dc=example,dc=com
        </Location>
~~~

## Autoriser la modification des révisions

Par défaut c'est désactivé. On peut l'activer en activant le hook `pre-revprop-change`. Pour cela il suffit de renommer en supprimant l'extension tmpl, et en s'assurant qu'il y ait les droits d'exécution.

Exemple de message d'erreur si ce n'est pas autorisé :

~~~
Repository has not been enabled to accept revision propchanges;
ask the administrator to create a pre-revprop-change hook
~~~

Si l’on veut restreindre la modification des révisions a un utilisateur en particulier, il faut ajouter ceci dans le hook `pre-revprop-change` :

~~~
if [ ! "$USER" = "user" ]; then
  echo "Changing revision properties is only allowed to user." >&2
  exit 1
fi
~~~

## FAQ

Lorsque l'on obtient un message du type :

~~~
$ svn checkout svn+ssh://SERVEUR/svn/sendsms2mobyt
svn: Erreur de base Berkeley pour le système de fichiers '/svn/sendsms2mobyt/db' en ouvrant l'environnement :

svn: DB_VERSION_MISMATCH: Database environment version mismatch
svn: bdb: Program version 4.6 doesn't match environment version 4.4
~~~

... c'est que la version du dépôt est obsolète. Il faut la mettre à jour sur le serveur SVN :

~~~
$ svnadmin recover sendsms2mobyt/
Verrou du dépôt acquis.
Patientez; le rétablissement du dépôt peut être long...

Fin du rétablissement.
La dernière révision du dépôt est 12
~~~

Si l'on veut commiter en svn+ssh:// avec un login différent du checkout :

On peut forcer l'utiliser via le $HOME/subversion/config

~~~
[tunnels]
ssh = $SVN_SSH ssh -l <user>
~~~
