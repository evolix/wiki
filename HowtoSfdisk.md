**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto sfdisk

Dumper une table de partitions :

~~~
# sfdisk -d /dev/sdz > partitions
~~~

Restaurer une table de partitions :

~~~
# sfdisk /dev/sdz < partitions
~~~

Si le disque est en cours d'utilisation mais que l'on a même pas peur :

~~~
# sfdisk --force /dev/sdz < partitions
~~~

## Redimensionner une partition avec sfdisk

sfdisk est pratique car il gère directement les secteurs : secteur de début et taille des partitions en secteur.

0. Par précaution, on démonte la partition ciblée et on dump le volume actuel

~~~
# sfdisk -d /dev/sdz > partitions.veryold
~~~


1. Bien noter de combien de secteurs on augmente le volume :
   on pourra notamment comparer le nombre de secteurs du volume (avec fidsk par exemple)
   avant augmentation, puis après augmentation. Notons une augmentation de N secteurs.

2. On dumpe la partition

~~~
# sfdisk -d /dev/sdz > partitions
# cp partitions partitions.new
~~~

3. On édite le fichier partitions.new, en ajoutant +N à la dernière partition ET - si besoin - à la partition extended correspondant

4. On réinjecte et on resize :

~~~
# sfdisk /dev/sdz < partitions.new
# resize2fs /dev/sdz8
~~~

