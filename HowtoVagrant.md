---
title: Howto Vagrant
categories: virtualisation dev
---

* Documentation : <https://www.vagrantup.com/docs/>

[Vagrant](https://www.vagrantup.com) est un logiciel qui permet de configurer et démarrer des environnements virtuels légers, portables et reproductibles.

À travers son fichier de configuration nommé **Vagrantfile**, il permet d'orchestrer l'installation et la configuration de VMs ou de conteneurs variés (Libvirt, LXC, VirtualBox, VMWare, Docker, Amazon EC2...).


## Installation

~~~
apt install vagrant
~~~

Des paquets Debian officiels (maintenus par Hashicorp) sont aussi disponibles pour des versions plus récentes, mais sans dépôt : <https://www.vagrantup.com/downloads.html>


## Quickstart

Prérequis : `libvirt`, DHCP

Créer un fichier `Vagrantfile` dans un répertoire dédié :

~~~
Vagrant.configure("2") do |config|
  config.vm.provider :libvirt do |libvirt|
    libvirt.memory = 1024
    libvirt.cpus = 2
    libvirt.keymap = "fr"
  end

  config.vm.box = "debian/bookworm64"

  config.vm.define :test_vagrant do |node|
    node.vm.hostname = "test-vagrant"
  end

  config.vm.synced_folder "./", "/vagrant", disabled: true
end
~~~

Lancer la box :

~~~
$ vagrant up test_vagrant
~~~

Se connecter à la box :

~~~
$ vagrant ssh test_vagrant
vagrant@test-vagrant:~$ 
~~~

Stopper la box :

~~~
$ vagrant halt test_vagrant
~~~


## Fonctionnement

### Boxes

Vagrant utilise des [boxes](https://developer.hashicorp.com/vagrant/docs/boxes) pour créer des VMs ou des conteneurs.

Ce sont des images de systèmes pré-installés.

On peut trouver des boxes dans le [catalogue public de son éditeur HashCorp](https://vagrantcloud.com/boxes/search).

Les boxes sont nommées par fournisseurs, par exemple :

* `debian/bullseye64`
* `debian/bookworm64`

On peut aussi les récupérer en spécifiant une URL plutôt à la place du nom de la box.


### Providers

Vagrant permet de lancer et configurer des boxes dans VirtualBox, Libvirt, LXC, VMware, Docker, Amazon EC2 et autres.

Ces providers peuvent être locaux ou situés sur des serveurs distants.


#### Libvirt

Les boxes et les images `QCOW2` du provider `libvirt` sont stockées par défaut dans `/var/lib/libvirt/images/`.

Par défaut, le volume de stockage est une image au format `QCOW2`, ce qui éviter d'avoir à réserver de l'espace disque.


## Syntaxe de Vagrant

On configure un environnement Vagrant, constitué d'une ou plusieurs boxes, avec un fichier `Vagrantfile`.

On peut avoir autant d'environnements qu'on souhaite, tant qu'ils sont dans des répertoires distincts.

Le `Vagrantfile` peut notamment être partagé dans le dépôt d'un projet et permet d'avoir un environnement de test reproductible pour tous les usagers du dépôt.

La syntaxe est en Ruby et décrit le type de machine à démarrer, leurs spécifications et leur configuration initiale (provision).


### Vérifier la validité d'un `Vagrantfile`

Se positionner dans le répertoire où se trouve le `Vagrantfile` :

~~~
$ vagrant validate
~~~


### Exemple de `Vagrantfile` avec VirtualBox et provisionnement Ansible

~~~{.ruby}
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure('2') do |config|
  # Image de base de la machine virtuelle
  config.vm.box = "debian/bookworm64"

  # Désactiver le partage et la synchronisation via NFS (activé par défaut)
  config.vm.synced_folder "./", "/vagrant", disabled: true

  # Spécification du provider et caractéristiques des VM.
  config.vm.provider :virtualbox do |v|
    v.memory = 1024
    v.cpus = 2
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--ioapic", "on"]
  end

  # Définition de la machine virtuelle qui sera lancée.
  config.vm.define :default do |default|
    default.vm.hostname = "default"
    default.vm.network :private_network, ip: "192.168.33.33"
    default.vm.provision :ansible do |ansible|
          ansible.limit = "default"
          ansible.playbook = "provisioning/vagrant.yml"
          ansible.raw_arguments = ["-b"]
    end
  end

  # Configuration permettant de distribuer l'image dans le Vagrant Atlas
  config.push.define "atlas" do |push|
    push.app = "evolix/evolinux"
    push.vcs = false
  end

end
~~~


#### Providers

##### Libvirt

Le [provider libvirt](https://github.com/vagrant-libvirt/vagrant-libvirt/blob/master/README.md) permet d'utiliser les services supportés par l'API [libvirt](https://libvirt.org/), notamment [KVM](HowtoKVM).

Voici un exemple d'utilisation d'un serveur KVM distant :

~~~{.ruby}
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure('2') do |config|
    config.vm.provider :libvirt do |libvirt, override|
        libvirt.host = "kvm-host.example.com"
        libvirt.connect_via_ssh = 'yes'
        libvirt.memory = 1024
        libvirt.cpus = 2
        libvirt.cpu_mode = "host-passthrough"
        libvirt.random :model => 'random'
        override.ssh.proxy_command = "ssh kvm-host.example.com nc -N %h %p"
    end
end
~~~


##### Virtualbox

Le provider originel et par défaut de Vagrant est [Virtualbox](https://www.virtualbox.org/).

Il a l'avantage de pouvoir tourner sur Linux, Windows et Mac OS :

~~~{.ruby}
# -*- mode: ruby -*-
# vi: set ft=ruby :

config.vm.provider :virtualbox do |v|
    v.memory = 1024
    v.cpus = 2
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--ioapic", "on"]
  end
~~~


## Utilisation du CLI

### Gestion des boxes

Ajouter une nouvelle box Debian Bookworm pour `libvirt` aux boxes disponibles localement :

~~~
$ vagrant box add --provider=libvirt debian/bookworm64
~~~

Lister les boxes disponibles localement :

~~~
$ vagrant box list
debian/bookworm64 (libvirt, 12.20230723.1)
debian/bullseye64 (libvirt, 11.20210829.1)
~~~

Lister les boxes locales à mettre à jour :

~~~
$ vagrant box outdated --global
* 'debian/bullseye64' for 'libvirt' is outdated! Current: 11.20210829.1. Latest: 11.20230615.1
* 'debian/bookworm64' for 'libvirt' (v12.20230723.1) is up to date
~~~

Mettre-à-jour toutes les box :

~~~
$ vagrant box update
~~~

Note : pour la commandes suivantes, l'option `--provider` n'est nécessaire que s'il y a des boxes du même nom sur plusieurs providers.

Mettre-à-jour une box en particulier :

~~~
$ vagrant box update [--provider=libvirt] --box debian/bullseye64
~~~

Supprimer une box :

~~~
$ vagrant box remove [--provider=libvirt] debian/bullseye64
~~~

Pour supprimer les boxes qui ne sont plus à jour, il faut d'abord faire un `vagrant destroy [<VM_ID|NAME>]` (voir plus bas [la gestion des VMs](#gestion-des-environnements-et-des-vms)) sur les VMs qui les utilisent.

Puis (ajouter `-n` pour un dry-run) :

~~~
$ vagrant box prune [--provider=libvirt]
~~~

Si on lance la commande sans supprimer les VMs au préalable, elle demandera une confirmation pour les boxes qui sont encore utilisées.


### Gestion des environnements et des VMs

Notes : 

* Pour simplifier, on utilise le terme « VM » mais en fonction du provider ce peut aussi être un conteneur.
* **La plupart de ces commandes doivent être jouées dans le répertoire de l'environnement concerné.**
* Un environnement peut contenir plusieurs VMs. Dans ce cas, il faut préciser le nom ou l'id de l'instance dans la commande.

Lister toutes les VMs Vagrant présentes sur l'hôte (utile pour retrouver les répertoires, les ids...) :

~~~
$ vagrant global-status
~~~

Indiquer le statut de la ou les VMs :

~~~
$ vagrant status [<VM_ID|NAME>]
~~~

Créer une instance de VM par défaut avec la box `debian/bookworm64` :

~~~
$ vagrant init debian/bookworm64
~~~

Cette commande crée un fichier `Vagrantfile` (à modifier) qui contient la configuration de la VM.

Alternativement, on peut créer soi-même le `Vagrantfile` pour configurer plusieurs VMs dans le même environnement ou configurer plus finement la VM.

Démarrer ou redémarrer la VM :

~~~
vagrant up|reload [<VM_ID|NAME>]
~~~

Éteindre la VM :

~~~
$ vagrant halt [<VM_ID|NAME>]
~~~

Sortir de veille ou mettre en veille la VM :

~~~
$ vagrant resume|suspend [<VM_ID|NAME>]
~~~

Supprimer la VM (extinction + suppression des ressources) :

~~~
$ vagrant destroy [<VM_ID|NAME>]
~~~

Attention : cette commande est différente de `virsh destroy`, qui ne fait qu'éteindre une VM sans supprimer les ressources ! `vagrant destroy` équivaut plutôt à `virsh destroy && virsh undefine`.


### Snapshots des environnements / VMs

> **Pré-requis** : le paquet `libguestfs-tools` doit être installé.

La commande `vagrant package` permet de créer une "box" d'une VM (allumée ou éteinte).

Depuis le dossier contenant le `Vagrantfile` :

~~~{.bash}
$ vagrant package jessie --output test-jessie-1.box
~~~

Le fichier box est une archive tar contenant un fichier .img et des métadonnées.

> **Remarque** : Vagrant enlève les clefs SSH de l’hôte


### Interaction avec les environnements / VMs

Lister les ports mappés de l'invité vers l'hôte (fonctionnalité non supportée par le provider `libvirt`) :

~~~
$ vagrant port
    22 (guest) => 2222 (host)
    80 (guest) => 8080 (host)
~~~

Se connecter en SSH à une VM :

~~~
$ vagrant ssh
~~~

On peut aussi demander à Vagrant de produire une configuration SSH à ajouter dans notre `.ssh/config` :

~~~
$ vagrant ssh-config
~~~

Uploader un fichier dans la VM :

~~~
$ vagrant upload source [destination] [<VM_ID|NAME>]
~~~


### Provisionnement des environnements / VMs

*Documentation à venir*

~~~
vagrant provision
~~~


## FAQ

### Désactiver le partage NFS

Pour éviter que Vagrant ne crée par défaut un partage NFS, le `Vagrantfile` doit contenir :

~~~
config.vm.synced_folder "./", "/vagrant", disabled: true
~~~

Sinon, au lancement, on rencontre l'erreur :

~~~
It appears your machine doesn't support NFS, or there is not an adapter to enable NFS on this machine for Vagrant. Please verify that `nfsd` is installed on your machine, and try again. If you're on Windows, NFS isn't supported. If the problem persists, please contact Vagrant support.
~~~


### Configurer localement le provider par défaut

Si le `Vagrantfile` de l'environnement est partagé entre plusieurs utilisateurs, ceux-ci peuvent souhaiter utiliser différents providers.

Pour cela, on peut créer un fichier `~/.Vagrantfile` local, configurant le provider par défaut de l'utilisateur. Par exemple, pour utiliser par défaut le provider `libvirt` :

~~~
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt"
end
~~~

Ensuite, il faut mettre dans le `Vagrantfile` de chaque environnement cet en-tête :

~~~
# Load ~/.Vagrantfile if exist, permit local config provider
vagrantfile = File.join("#{Dir.home}", '.Vagrantfile')
load File.expand_path(vagrantfile) if File.exists?(vagrantfile)
~~~

Si `~/.Vagrantfile` n'existe pas et qu'aucun provider n'est défini, le provider par défaut du système sera utilisé.


### Synchroniser un répertoire entre l'hôte et l'invité 

<https://developer.hashicorp.com/vagrant/docs/synced-folders>


#### Avec Rsync

*Documentation à venir*

<https://developer.hashicorp.com/vagrant/docs/synced-folders/rsync>

Synchroniser automatiquement le répertoire :

~~~
vagrant rsync-auto
~~~

Voir la [documentation](https://developer.hashicorp.com/vagrant/docs/synced-folders/rsync).

### Répertoire avec Plusieurs `Vagrantfiles`

On peut utiliser la variable d'environnement [`VAGRANT_VAGRANTFILE`](https://developer.hashicorp.com/vagrant/docs/other/environmental-variables#vagrant_vagrantfile) pour spécifier le nom (et non le chemin) du `Vagrantfile` à utiliser.
