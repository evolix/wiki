**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto LFTP

LFTP est le couteau suisse du FTP.

~~~
$ lftp login@ftp.example.com
~~~

ou en FTP anonyme :

~~~
$ lftp ftp.example.com
~~~

ou en SFTP :

~~~
$ lftp sftp://login@ftp.example.com
~~~

On peut ensuite utiliser les commandes :

* `ls` : lister le contenu d'un répertoire distant
* `cd` : se déplacer dans les répertoires distants
* `put <fichier>` : envoyer un document local vers le répertoire distant courant
* `get <fichier>` : récupérer un document distant dans son répertoire local
* `mput <pattern>` : comme put mais on peut utiliser une expresssion régulière pour envoyer plusieurs fichiers en même temps
* `mget <pattern>` : comme get mais on peut utiliser une expresssion régulière pour recevoir plusieurs fichiers en même temps

Pour télécharger un répertoire complet :

~~~
> cd <répertoire>
> lcd <répertoire local>
> mirror
~~~

~~~
debug 5 <<< verbeux
set cache:enable off <<< pour éviter de cacher le ls et autres commandes
set ftp:passive-mode off
set ftp:passive-mode on
~~~

Il est possible de transmettre des commandes à LFTP depuis la ligne de commande. Exemple pour envoyer un fichier, attendre que le transfer soit terminé et quitter LFTP :

~~~
$ lftp -e "PUT foo.txt; wait; exit" ftp.example.com/path
~~~
