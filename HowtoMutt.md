---
categories: mail
title: Howto Mutt
... 

* Documentation : <http://www.mutt.org/doc/manual/>
* Documentation en local sur Debian : `/usr/share/doc/mutt/html/manual.html`

[Mutt](http://www.mutt.org/) est un client email en mode console.

«  All mail clients suck. This one just sucks less. » — me, circa 1995 


## Installation

~~~
# apt install mutt

$ mutt -version
NeoMutt 20170113 (1.7.2)
...
~~~

## Configuration

La configuration générale se trouve dans `/etc/Muttrc` (et des fichiers inclus dans `/etc/Muttrc.d/`).

La configuration par utilisateur se trouve dans `~/.muttrc`.
Voici un exemple de configuration : <https://github.com/gcolpart/dotfiles/blob/master/.muttrc>

Pour visualiser les contenus en "text/html" nous suggèrons d'installer `lynx` et de créer un fichier `~/.mailcap` :

~~~
text/html; lynx -force_html -dump %s; copiousoutput
~~~

## Utilisation basique

On peut lancer Mutt en lecture seule avec `-R` et en précisant une Maildir/mbox avec `-f

~~~
$ mutt
$ mutt -R
$ mutt -R -f Maildir/.Sent/
~~~

Voici les commandes indispensables à connaître pour utiliser Mutt :

~~~
? : aide
q : quitter Mutt en appliquant de changements (sauf si en lecture seule)
x : quitter Mutt sans appliquer de changements

# Lecture d'une boîte
o : trier la vue courant par date, taille ou threads etc.
$ : appliquer les changements sans quitter Mutt (statuts lu, répondu, flag, etc.)
l : appliquer un filtre à la vue courante
T : taguer tous les emails correspondants à un filtre
; : appliquer une commande à tous les emails tagués

# Lecture de l'email courant
r : répondre
g : répondre à tous
d : effacer l'email
h : "voir les en-têtes du mail" (switch)
s : déplacer l'email dans une autre boîte
t : taguer l' email
v : "Regarder pièces-jointes"

# Envoi d'email
m : rédiger un nouvel email
Echap + e : ré-éditer l'email courant pour envoi
a : ajouter une pièce jointe (juste avant l'envoi)
D : supprimer une pièce jointe (juste avant l'envoi)
s : modifier le sujet (juste avant l'envoi)
Echap + f : modifier l'expéditeur (juste avant l'envoi)
q : quitter et mettre l'email en Brouillon (ou pas)
y : valider l'envoi (nonnnnn)
~~~

La puissance de Mutt repose notamment sur les filtres pour voir/taguer les emails.

Voici les filtres indispensables à connaître pour utiliser Mutt :

~~~
~A : tout voir
~U : voir que les non-lus
~d<3d : voir les emails de moins de 3 jours
~d 3/1/20-17/3/21 : voir les emails entre le 3 mars 2020 et le 17 mars 2021
~f foo@example : dont l'expéditeur contient foo@example
~C foo@example : dont le destinataire (To ou Cc) contient foo@example
~s foo : dont le sujet contient foo
~s ( foo | bar ) : dont le sujet contient foo ou bar
~h foo : dont les entêtes de l'emails contiennent foo (cela peut être long)
~b foo : dont le contenu de l'email contient foo (cela peut être très long)
! filtre : l'inverse d'un filtre
~~~

*Note :* les filtres sont évidemment cumulatifs

## Commandes avancées

~~~
c : "Ouvre une autre boîte"
y : "Naviguer dans les boîtes"
w : "Appliquer un flag sur le mail"
W : "Supprimer un flag sur le mail"
<Tab> | j : "Sélectionner le mail non lu suivant"
s : "Sauvegarder le mail"
D : "Sélectionner tous les mails à supprimer"
> : "Défiler la liste des mails vers le bas"
< : "Défiler la liste des mails vers le haut"
q : "Quitter l'écran"
i : "Revenir à l'index"
T : "taguer tous les mails correspondant à la recherche"
^T : "dé-taguer tous les mails correspondant à la recherche"
= : "Aller au premier email reçu"
* : "Aller au dernier mail reçu"
~~~

## Motifs de recherche

Les actions de type recherche, tag… savent utiliser des motifs pour sélectionner les messages. On peut utiliser un ou plusieurs motifs :

~~~
~A              tous les messages
~b EXPR         messages qui contiennent EXPR dans le corps
~B EXPR         messages qui contiennent EXPR dans tout le message
~c USER         messages en cc: à USER
~C EXPR         messages avec to: EXPR ou bien cc: EXPR
~D              messages supprimés
~d [MIN]-[MAX]  messages dont ``date-sent'' ets dans la plage de date
~E              messages expirés
~e EXPR         messages qui contiennent EXPR dans le champ ``Sender''
~F              messages marqués d'un drapeau
~f USER         messages provenant de USER
~g              messages signés avec PGP
~G              messages chiffrés avec PGP
~h EXPR         messages qui contiennent EXPR dans un des entêtes
~k              messages qui contiennent des éléments PGP
~i ID           messages dont ID est trouvé dans le champ ``Message-ID''
~L EXPR         messages envoyés ou reçu de EXPR
~l              messages adressés à une liste de diffusion
~m [MIN]-[MAX]  messages dans la plage MIN à MAX *)
~n [MIN]-[MAX]  messages avec un score compris entre MIN et MAX *)
~N              nouveaux messages
~O              anciens messages
~p              messages qui vous sont adressés (utilise $alternates)
~P              messages que vous avez écrit (utilise $alternates)
~Q              messages qui ont été répondus
~R              messages lus
~r [MIN]-[MAX]  messages dont le champ ``date-received'' est dans la plage de date
~S              superseded messages
~s SUBJECT      messages ayant SUBJECT dans le champ ``Subject''
~T              messages marqués
~t USER         messages adressés à USER
~U              messages non lus
~v              message dans une discussion condensée
~x EXPR         messages contenant EXPR dans le champ `References'
~y EXPR         messages contenant EXPR dans le champ `X-Label'
~z [MIN]-[MAX]  messages dont la taille est comprise entre MIN et MAX *)
~=              messages dupliqués (voir $duplicate_threads)
~~~

Où `EXPR`, `USER`, `ID`, et `SUBJECT` sont des expressions régulières.

> Les formes `<[MAX]`, `>[MIN]`, `[MIN]-` et `-[MAX]` sont aussi autorisées.

> Les recherches ne sont pas sensibles à la casse si elles ne contiennent pas de majuscule.

On peut combiner les motifs, par exemple, si on veut rechercher les e-mails dont le sujet commence par `Cron ` _et_ envoyés par `user@host.name` : `~s '^Cron ' ~f 'user@host.name'`. Combiner des motifs fait un ET logique en eux par défaut. Si on veut faire un OU logique : `~s '^Cron ' | ~s '\[Info\] '`. Voir la documentation pour plus d'informations : [Nesting and Boolean Operators](http://www.mutt.org/doc/manual/#complex-patterns).

## GPG

Voici les options de configuration de base :

~~~
# Use GPGME
set crypt_use_gpgme = yes
set crypt_replysign = yes
set crypt_replyencrypt = yes
set crypt_replysignencrypted = yes
set crypt_verify_sig = yes
set crypt_autosign = yes

# Definir la clé de chiffrement :
set pgp_sign_as = 0x53415200
~~~

Après avoir terminé la rédaction d'un mail, voici la suite de commande à suivre pour un mutt configuré en FR :

~~~
p : Activer le mode de chiffrement
c : Pour chiffrer le mail (sinon, plusieurs modes son proposés)
y : Pour envoyer le mail
o : Pour valider la sélection de la clé de chiffrement.
~~~

## Quelques séquences utiles

### Jeter un œil sur une série de mails avant de les déplacer en masse :

* Dans la liste des messages ;
* `l` pour filtrer la liste ;
* `~s '\[evomaintenance\]'` le sujet doit contenir `[evomaintenance]` ;
* une fois les mails parcourus ;
* `T` pour taguer tous les mails (la recherche est normalement rappelée, il suffit de valider) ;
* `;s` pour indiquer qu'on veut déplacer tous les mails tagués ;
* saisie du dossier cible, par exemple `=.evomaintenance/`.

## Astuces de configuration

Stocker ses mails dans Maildir et utiliser un dossier brouillon et une copie d'envoi.

~~~{.bash}
set folder="~/Maildir"
set spoolfile = ~/Maildir
set record="+.Sent"
set postponed="+.Drafts"
~~~

Garder un historique des commandes et motifs de recherche :

~~~{.bash}
set history=1024
set save_history=2048
set history_file="~/.mutt/history"
~~~

Trier par thread par défaut, avec le thread entier qui suit la date du dernier message :

~~~{.bash}
set sort=threads # default sort messages by thread
set sort_browser=reverse-date # showing mbox list default to newest first
set sort_aux=last-date-received # showing threads with youngest message last
~~~

Format de date plus complet :

~~~{.bash}
set date_format="%Y-%m-%d %H:%M"
set index_format="%2C | %Z [%d] %-30.30F (%-4.4c) %s"
~~~

Si on a beaucoup de mails dans la boîte et qu'on veut le "numéro" du mail sur sur plus de 2 chiffres, il suffit d'indiquer `%3C`, `%4C`…

Avec `%d`, la date est affichée (en utilisant le format indiqué pour `date_format` mais dans le fuseau horaire de l'expéditeur. Pour l'avoir dans le fuseau horaire local, il suffit de changer pour `%D`.

Toute les options de format d'affichage de l'index (et des dates) sont "consultables dans la documentation":http://www.mutt.org/doc/manual/#index-format

Désactiver le protocole CMS :

Ajouter cette directive dans /etc/Muttrc :

~~~{.bash}
set crypt_use_gpgme=no
~~~

## FAQ

### J'ai perdu ma console

<https://gcolpart.evolix.net/blog21/capture-inputoutput-of-a-process-with-gdb/>

### Supprimer les doublons d'une boîte

_Killer-feature_ de Mutt, on peut taguer les e-mails en double : `T` puis `~=`.

### Imprimer depuis mutt

~~~
# apt install muttprint
~~~

Puis pour générer un fichier Postscript, mettre dans sa configuration :

~~~
set print_command="muttprint --printer TO_FILE:/var/tmp/muttprint.ps %s"
~~~

### Désactiver a recherche de nouveaux mails (sauf à l'ouverture)

~~~
set check_new=no
~~~

### Cache

~~~
set header_cache="/var/tmp/mutt-jdoe-cache-dir/"
#set message_cachedir = "/path/to/folder"
~~~

### RFC5321.MailFrom et RFC5322.From

Pour jouer à envoyer des emails avec un RFC5321.MailFrom (enveloppe) et RFC5322.From (entête) différents, il faut :

~~~
set envelope_from_address="enveloppe@example.com"
set use_envelope_from=yes
set from="entete@example.com"
~~~

> Note : les paramètres RFC5321.MailFrom sont ensuite envoyés à `sendmail -f` en général, donc c'est lui le responsable que ça marche comme attendu.



