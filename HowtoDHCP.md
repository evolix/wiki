**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Failover DHCP master/master 

Ce mode permet à deux serveurs DHCP de fonctionner simultanément sur un même LAN.

Les leases dynamiques sont alors partagés entre les 2 serveurs et synchronisés via un échange réseau entre les serveurs. En cas de panne de l'un deux, l'autre prend alors à sa charge l'ensemble des leases.

### Sous Debian

Il est nécessaire d'ajouter une section "failover" sur chacun des serveurs. Exemple :

Master :

~~~
failover peer "dhcp-failover" {
        primary;
        address 192.0.2.1;
        peer address 192.0.2.2;
        port 647;
        peer port 647;
        max-response-delay 30;
        max-unacked-updates 10;
        load balance max seconds 3;
        split 128;
        mclt 1800;
}
~~~

Slave :

~~~
failover peer "dhcp-failover" {
        secondary;
        address 192.0.2.2;
        peer address 192.0.2.1;
        port 647;
        peer port 647;
        max-response-delay 30;
        max-unacked-updates 10;
        load balance max seconds 3;
}
~~~

Il est également nécessaire d'englober la directive "range" et les sections "host" dans une section "pool", et l'on déclarera celle-ci en mode failover. Exemple :

~~~
subnet 192.0.2.0 netmask 255.255.0.0 {
  option routers 192.2.0.254;

  pool {
    failover peer "dhcp-failover";
    range 192.0.2.100 192.0.2.200;

    host host1 {
          hardware ethernet 08:00:1f:82:7a:72;
          fixed-address 192.0.2.4
    }

    [...]
}
~~~

### Sous OpenBSD

Il ne faut pas modifier la configuration DHCP en elle-même, mais ajouter des options au daemon. Voir le manuel [dhcpd(8)](http://man.openbsd.org/dhcpd#SYNCHRONISATION).

Dans `/etc/rc.conf.local`, il faut ajouter les flags `-y` (interface d'écoute en unicast et multicast ou IP unicast, ne doit être défini qu'une seule fois) et `-Y` (interface d'envoi en multicast ou IP unicast, peut être défini plusieurs fois).

Par exemple pour accepter tous les messages de synchronisation en multicast et unicast à la réception sur l'interface `em0`, et envoyer des messages de multicast sur `em0`, tout en répondant aux requêtes DHCP sur les interfaces `em0` et `em2` uniquement :

~~~
dhcpd_flags=-y em0 -Y em0 em0 em2
~~~

Pour accepter tous les messages de synchronisation à destination de l'IP 192.0.2.5, et envoyer des messages sur l'IP 192.0.2.6, tout en répondant aux requêtes DHCP sur les interfaces `em1` et `em2` uniquement :

~~~
dhcpd_flags=-y 192.0.2.5 -Y 192.0.2.6 em1 em2
~~~

Si le fichier `/var/db/dhcpd.key` existe, alors son contenu sera utilisé comme clé pré-partagée entre les différents serveurs DHCP qui doivent tous avoir exactement le même fichier, afin d'authentifier les messages de synchronisation.

## Leases, adresses IP et adresses MAC

Les leases sont les distributions d'IP aux clients.

Le serveur DHCP les stocke dans `/var/db/dhcpd.leases`.


### Lister et compter les IP attribuées par DHCPD

~~~
awk -F" " '/lease/{print$2}' /var/db/dhcpd.leases | sort -V | uniq
awk -F" " '/lease/{print$2}' /var/db/dhcpd.leases | sort -V | uniq | wc -l
~~~


### Lister les adresses MAC par nombre de leases

~~~
grep hardware /var/db/dhcpd.leases | sort | uniq -c | sort -hr
~~~

Attention, le nombre de leases n'est pas égal au nombre d'IPs attribuées !

## Monitoring

Il est possible de monitorer l'utilisation des pool DHCP à l'aide d'un script, adapté selon l'outil de monitoring et l'OS hôte.

Le script indiquera le taux d'utilisation de chacun des pools configurés dans le serveur DHCP. Attention : les baux statiques ne sont pas pris en compte, seuls les baux dynamiques le sont. Les pools dynamiques ne doivent pas inclure les IPs attribuées par des baux statiques : si un bail statique a une IP comprise dans le pool dynamique, alors il ne sera pas compté dans le taux d'utilisation.

Le script change légèrement selon s'il doit tourner sous Debian ou OpenBSD :

* Sous Debian, la variable $CONFFILE sera généralement à `/etc/dhcp/dhcpd.conf` alors que sous OpenBSD, elle sera à `/etc/dhcpd.conf`.
* Sous Debian, la variable $LEASEFILE sera généralement à `/var/lib/dhcp/dhcpd.leases` alors que sous OpenBSD, elle sera à `/var/db/dhcpd.leases`.
* Sous Debian, on a un `push (@activeleases, $lease);` dans un `elsif ($line =~ /binding state active/) {`, alors que sous OpenBSD, le même push est dans un `elsif ($line !~ /abandoned/) {`.

### NRPE

Le script pour Debian est présent dans [ansible-roles](https://gitea.evolix.org/evolix/ansible-roles/src/branch/unstable/nagios-nrpe/files/plugins/check_dhcp_pool), celui pour OpenBSD est présent dans [EvoBSD](https://gitea.evolix.org/evolix/EvoBSD/src/branch/dev/roles/nagios-nrpe/files/plugins_bsd/check_dhcp_pool).

### Munin

Le script pour Debian est présent dans [ansible-roles](https://gitea.evolix.org/evolix/ansible-roles/src/branch/unstable/munin/files/plugins/dhcp_pool). Munin n'est généralement pas utilisé sous OpenBSD, et le script n'existe alors pas dans EvoBSD mais peut être adapté avec les indications ci-dessus au besoin.

Voir ensuite [HowtoMunin]() pour la configuration.

### Grafana

Le script pour OpenBSD est présent dans [EvoBSD](https://gitea.evolix.org/evolix/EvoBSD/src/branch/dev/roles/collectd/files/dhcp_pool.pl). Celui pour Debian n'existe pas encore dans ansible-roles mais peut être adaptée au besoin avec les indications ci-dessus.

Voir ensuite [HowtoCollectd](), [HowtoInfluxDB]() et [HowtoGrafana]() pour la configuration.

## FAQ

### Abandonned leases

Si le serveur DHCP n'arrive plus à attribuer d'adresses IP et log `no free leases on subnet`, il se peut que le liste des baux DHCP attribués par le serveur (`/var/db/dhcpd.leases`) soit plein d'IP `abandonned`.

Cela signifie que le serveur considère cette IP comme déjà utilisée et ne l'attribuera, pas à moins qu'il soit à court d'IP disponibles, auquel cas il revérifiera quand même que l'IP n'est pas toujours utilisée, ce qui pourrait tout de même empêcher son attribution.

