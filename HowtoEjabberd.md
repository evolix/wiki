**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Ejabberd

Ejabberd est un serveur Jabber écrit en Erlang.

<https://www.ejabberd.im/>

## Installation

Ejabberd est packagé dans Debian :

~~~
# apt install ejabberd
~~~

Lors de l'installation, indiquer :

* hostname : le domaine principal (fqdn) que le serveur devra gérer ;
* compte admin : un compte Jabber (sans la partie @domaine) qui sera admin du serveur (permet l'accès à l'interface web + accès à diverses commandes admin depuis un client Jabber.

## DNS

Enregistrements à rajouter dans la zone DNS :

~~~
_xmpp-client._tcp       IN      SRV 0 0 5222 im
_xmpp-server._tcp       IN      SRV 0 0 5269 im
im                      IN      A   192.0.2.42
~~~

Note : _xmpp-server_ est facultatif, mais recommandé pour communiquer avec d'autres serveurs XMPP.

## Configuration

La configuration se fait désormais via le fichier _/etc/ejabberd/ejabberd.yml_.

Pour configurer plusieurs hosts :

~~~
hosts:
  - "im.example.com"
  - "example.com"
~~~

### Authentification (à mettre à jour)

Ejabberd supporte plusieurs backends externes pour l'authentification (MySQL, LDAP, PAM, script externe…).

Pour utiliser un annuaire LDAP :

* commenter `{auth_method, internal}.` pour ne faire _que_ de l'authentification sur LDAP. Pour utiliser plusieurs méthodes d'authentification, il est possible de passer une liste comme ceci :

~~~
{auth_method, [ldap, internal]}.
~~~

* Décommenter/modifier les directives suivantes :

~~~
%%
%% Authentication using LDAP
%%
{auth_method, ldap}.

%% List of LDAP servers:
{ldap_servers, ["localhost"]}.

%% Encryption of connection to LDAP servers (LDAPS):
{ldap_encrypt, none}.
%%{ldap_encrypt, tls}.

%% Port connect to LDAP server:
{ldap_port, 389}.
%%{ldap_port, 636}.

%% LDAP manager:
%%{ldap_rootdn, "dc=example,dc=com"}.

%% Password to LDAP manager:
%%{ldap_password, "******"}.

%% Search base of LDAP directory:
{ldap_base, "dc=example,dc=com"}.

%% LDAP attribute that holds user ID:
{ldap_uids, [{"uid", "%u"}]}.

%% LDAP filter:
{ldap_filter, "(objectClass=shadowAccount)"}.
~~~

### Optimisation

Par défaut, le support du multi-cœur n'est pas forcément désactivé. Pour l'activer si le processeur le supporte, éditer le fichier _/etc/default/ejabberd_ :

~~~
SMP=auto
~~~

## Administration

~~~
# ejabberdctl status
The node ejabberd@node is started with status: started
ejabberd 14.07 is running in that node

# ejabberdctl register jdoe im.example.com <password>

# ejabberdctl connected_users
jdoe@im.example.com/4767678256447397329306554

# ejabberdctl registered_users im.example.com
jdoe

# ejabberdctl kick jdoe im.example.com
# ejabberdctl unregister jdoe im.example.com
~~~

## Sauvegarde / migration

~~~
# ejabberdctl dump /tmp/ejabber.dump && mv /tmp/ejabber.dump /home/backup/
~~~

La restauration se fait avec `ejabberdctl load`

Note : on peut s'en servir pour migrer des données, en reformatant le fichier de dump qui n'est complexe à comprendre.
Par exemple pour migrer le répertoire des utilisateurs (roster) :

~~~
{tables,[{roster,[{record_name,roster},
                  {attributes,[usj,us,jid,name,subscription,ask,groups,
                               askmessage,xs]}]}]}.
{roster,{"jdoe","im.example.com",
         {"bob","gmail.com",[]}},
        {"jdoe","im.example.com"},
        {"bob","gmail.com",[]},
        [],both,none,[],<<>>,[]}.
[...]
~~~
