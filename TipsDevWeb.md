---
categories: web développement astuce
title: Tips développement web
...

Voici quelques conseils que nous recommandons fortement aux développeurs web.

## Scripts en crontab

* Un script en crontab devrait renvoyer ses erreurs sur _stderr_
* Un script en crontab ne doit renvoyer sur _stderr_ qu'en cas d'erreur
* Un script en crontab ne doit pas rediriger _stderr_ dans un fichier (exemples à ne pas reproduire: `2> cron_err.log` ou `> cron_err.log 2>&1`)
* Un script en crontab ne doit rien renvoyer sur _stdout_
* Un script en crontab devrait être lancé avec une option `-quiet` ou `-cron`, permettant de
   le lancer en mode interactif sans cette option et d'obtenir des informations (sur _stdout_)
* Un script en crontab ne devrait pas faire de requête en curl/wget sur _localhost_ ... surtout si
   le script demande beaucoup de ressources ! **L'utilisation d'un langage CLI est conseillé.**
   *[PHP]* Dans le cas de PHP, il faut par exemple utiliser PHP CLI (qui a souvent des limitations moins restrictives qu'en mode web)
* Un script doit être lancé dans lancé dans la crontab de l'utilisateur, JAMAIS EN ROOT
* Si un script est lancé dans la crontab de root (pour une mauvaise raison donc), il ne doit être accessible que par root (en écriture ET lecture)
* Si un script lancé dans la crontab de root a des droits Unix 777, merci de vous pendre.
* Un script en crontab ne devrait pas être lancé à la minute 0, et surtout pas à 00h00. Ces heures sont maudites par tout sysadmin.

Pour gérer sa crontab, voir [HowtoCrontab]()

## Gestion des droits

* masque # 027 en mode FTP/SFTP VS masque 077 en mode web
* Si besoin des droits d'écriture pour le web : 

~~~
$ chmod g+w
~~~

* Un fichier avec les droits 777 (ou 666) provoque la fonte de la banquise, dégage du CO2 et tue des bébés chats.
   (même le site Internet du diable lui-même n'a pas de fichier en 666.)

### Détection des droits incorrects

* Détecter les fichiers non lisibles par Apache (lecture sur le groupe) : 

~~~
$ find ./ -type f ! -perm /g=r -exec ls -l {} \;
~~~

* Détecter les répertoires non lisibles par Apache (lecture/exécution sur le groupe) : 

~~~
$ find ./ -type d \( ! -perm /g=r -o ! -perm /g=x \) -exec ls -ld {} \;
~~~

* Détecter les fichiers/répertoires accessibles en écriture par Apache (écriture sur le groupe) : 

~~~
$ find ./ -perm /g=w
~~~

* Détecter les fichiers/répertoires accessibles en écriture par tous : 

~~~
$ find ./ -perm -007 -o -type f -perm -006
~~~

### Wordpress

<http://codex.wordpress.org/Changing_File_Permissions#Permission_Scheme_for_WordPress>

Les droits d'écriture ne sont pas requis dans un cas général, mais voici les changements de droits souvent requis :

* Autoriser les uploads : 

~~~
$ chmod -R g+w wp-content/uploads/
~~~

* Gérer des règles d'écritures via Wordpress : 

~~~
$ chmod 660 .htaccess
~~~

* Pour éditer des thèmes via l'éditeur en ligne de Wordpress : 

~~~
$ chmod -R g+w sur les thèmes que l'on veut éditer
~~~

* Si répertoire de cache : 

~~~
$ chmod -R g+w cache/
~~~

* À voir selon les plugins installés (qui doivent documenter les changements de droits nécessaires)

## Envoi d'email

Il est important de se pré-occuper d'utiliser un expéditeur valide pour les envois d'emails, sachant qu'il y a 2 expéditeurs :

* l'expéditeur d'enveloppe (appelé aussi *Return-Path*) : c'est cette adresse qui recevra les erreurs NPAI (N'habite Pas à l'Adresse Indiquée), etc.)
* l'expéditeur d'entête : c'est cette adresse qui sera visible par le destinataire final

Voici nos recommandations :

1. Utiliser une adresse d'expéditeur d'enveloppe identique à celle d'expéditeur d'entête
2. Utiliser une adresse personnalisée du type facteur@example.com avec un domaine que vous gérez bien sûr
3. Avoir un démon OpenDKIM sur le serveur d'envoi configuré avec votre domaine, si vous êtes client Evolix ouvrez un ticket pour vérifier que c'est bien le cas
4. Avoir un enregistrement SPF qui autorise l'envoi depuis le serveur d'envoi, et un enregistrement DMARC pour renforcer les vérifications SPF et/ou DKIM
5. Traiter réactivement (automatiquement si possible) les erreurs du type NPAI (N'habite Pas à l'Adresse Indiquée) en arrêtant immédiatemment l'envoi vers les adresses concernées

Pour plus de détails, voir <https://wiki.evolix.org/HowtoPostfix#d%C3%A9livrabilit%C3%A9>

### Comment faire pour régler les expéditeurs d'enveloppe et d'entête

Cela dépend des langages. En PHP, la fonction *mail()* ne permet PAS de régler l'expéditeur d'enveloppe, on conseille d'utiliser la bibliothèque [PHPMailer](https://github.com/PHPMailer/PHPMailer). Il peut également être possible de forcer l'expéditeur d'enveloppe dans la configuration PHP via la directive *sendmail_path* forcée à `"/usr/sbin/sendmail -t -i -f facteur@example.com"` par exemple, mais c'est moins pratique.

## Manipulation de mots de passe

Depuis PHP >=5.5.0, le langage s'est doté de fonctions haut niveau pour la manipulation de mot de passe. Ainsi, on a `password_hash()` qui va s'occuper du hachage du mot de passe (algo, sel, difficulté) et  `password_verify()` pour la vérification.

Si on souhaite générer un mot de passe en CLI, on peut utiliser `mkpasswd -m sha-512` en CLI pour générer des hashés. (Ou tout autre outil cli similaire)

## Performances

### Compression GZIP

Vérification de la compression GZIP :

~~~
$ curl -# -I -H "Accept-Encoding: gzip" http://example.com/ | grep -i ^Content-Encoding
Content-Encoding: gzip

$ curl -# -H "Accept-Encoding: gzip" http://example.com/ | file -
/dev/stdin: gzip compressed data, from Unix
~~~


