---
categories: databases
title: Howto SQLite
---

* Documentation : <http://www.sqlite.org/docs.html>

[SQLite](https://www.sqlite.org/) est un moteur de base de données dont le stockage se fait dans un simple fichier. Cela permet une utilisation simple (en ligne de commande, PHP, C, Ruby, etc.) et légère (il n'y a pas de démon). SQLite est utilisé dans des applications web légères et des logiciels connus comme Firefox.

SQLite utilise le langage de requêtes SQL et implémente la plupart de la spécification SQL-92. De nombreuses fonctionnalités sont donc compatibles avec MySQL et PostgreSQL mais des différences significatives existent. La documentation propose une aide à décision d'[utiliser ou non SQLite](http://www.sqlite.org/whentouse.html).

## Installation

~~~
# apt install sqlite3

$ sqlite3 --version
3.16.2 2017-01-06 16:32:41 a65a62893ca8319e89e48b8a38cf8a59c69a8209
~~~

## Utilisation

* Documentation CLI : <http://www.sqlite.org/cli.html>

Créer un fichier SQLite :

~~~
$ sqlite3 foo.db
SQLite version 3.16.2 2017-01-06 16:32:41
Enter ".help" for usage hints.
sqlite> .tables
sqlite> .exit

$ file foo.db
foo.db: empty
~~~

Créer une table :

~~~
sqlite> create table foo (i int);

sqlite> .tables
foo

sqlite> .schema foo
CREATE TABLE foo (i int);
~~~

Actions sur une table :

~~~
sqlite> INSERT INTO foo VALUES (42);
sqlite> SELECT * FROM foo;
42 
sqlite> UPDATE foo SET i=43 WHERE i=42;
sqlite> DELETE FROM foo WHERE i=43;
sqlite> DROP TABLE foo;
~~~

## PHP et SQLite

Assurez vous d'avoir l'extension sqlite3 installée et active. 
Sur un système Debian `apt install php-sqlite3` récupérera l'extension si elle n'est pas déjà présente.

* <http://php.net/manual/fr/book.sqlite3.php>

~~~
$db = new SQLite3('foo.db');

$db->exec('CREATE TABLE foo (i int)');
$db->exec('INSERT INTO foo VALUES (42)');
$result = $db->query('select * from foo');
~~~

Il est aussi possible de manipuler une base SQLite avec PDO

~~~
try{    
    $pdo = new PDO('sqlite:foo.db');
} catch (PDOException $e){
     exit('PDO Error');
}

~~~

