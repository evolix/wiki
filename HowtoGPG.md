---
categories: security
title: Howto GnuPG
...

* Documentation : <https://gnupg.org/documentation/index.html>

[GnuPG](https://gnupg.org/) est une implémentation complète et libre du standard OpenPGP défini par la [RFC4880](https://www.ietf.org/rfc/rfc4880.txt).  
GnuPG permet le chiffrement et la signature de vos données ainsi que de vos communication.

## Installation

GnuPG 2 est maintenant la version par défaut dans Debian :

~~~
# apt install gnupg
~~~


## Générer une clé GPG

* Guide de A à Z écrit par Daniel Kahn Gillmor, mainteneur GPG dans Debian : <https://riseup.net/en/security/message-security/openpgp/best-practices>  
* Guide de création de clé GPG par Debian : <https://keyring.debian.org/creating-key.html>  

Si vous ne souhaitez pas lire ces guides d'un bout à l'autre, il est possible de [télécharger ce fichier](https://raw.githubusercontent.com/ioerror/duraconf/master/configs/gnupg/gpg.conf) et le mettre dans `~/.gnupg/gpg.conf`. Il regroupe l'ensemble des pratiques recommandées par le guide.

Une fois que vous vous êtes assuré d'avoir une configuration adéquate, il vous faut générer votre clef.  
La génération d'une clé peut être très longue si vous avez un faible réservoir d'entropie, vous pouvez générer de l'entropie en bougeant la souris et en tapant sur le clavier, ou en installant le logiciel `haveged`.  
Une autre astuce est de disposer d'une clé matériel RNG.

On génère par la suite notre clef, on conseille la plus longue possible (4096) :

~~~
$ gpg --gen-key
$ gpg --full-generate-key # Pour pouvoir choisir, notamment, la taille de la clé.
~~~

Une fois que vous avez créé votre clef, vous pouvez afficher son fingerprint :

~~~
$ gpg --list-secret-keys

> Key fingerprint = 677F 54F1 FA86 81AD 8EC0  BCE6 7AEA C4EC 6AAA 0A97
~~~

## Importer une signature

~~~
$ gpg --import fichier.asc
~~~

## Lister les signatures d'une clé

~~~
$ gpg --list-signatures <IDClé>
~~~

## Signer un fichier

~~~
$ gpg --output doc.sig --sign doc
$ gpg --output doc.sig --clearsign doc
~~~

L'option `--clearsign` permet de ne pas compresser le fichier et de conserver le format initial, pratique pour du texte.

## Créer une sous-clef pour signer

Il est conseillé de ne pas garder la clef principale sur la machine. Avoir une sous-clef accessible pour l’usage régulier est cependant pratique.

~~~
$ gpg --edit-key $identifiant_long_de_la_clef
[…]
gpg> addkey
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
  (14) Existing key from card
Your selection? 4
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 4096
[…]
gpg> save
~~~

La sous-clef de signature peut maintenant être exportée pour être utilisée sur une autre machine connectée à Internet.

~~~
gpg --armor --export-secret-subkeys $identifiant_long_de_la_clef > Psubkey.asc
~~~

La clef primaire peut aussi être exportée pour être stockée en lieu sûr.

~~~
gpg --armor --export-secret-keys $identifiant_long_de_la_clef > PMkey.asc
~~~

La clef primaire, maintenant stockée en lieu sûr, peut être supprimée du trousseau local, puis la sous-clef privée importée de nouveau.

~~~
gpg --delete-secret-key $identifiant_long_de_la_clef
gpg --import Psubkey.asc
~~~

## Configuration

### Agent GPG

L'agent GPG garde en mémoire la passphrase de vos clés mais fait aussi office d'agent SSH, ce qui vous permet d'utiliser GPG pour vos connecter sur vos serveurs !

~~~
# apt install gnupg-agent pinentry-qt
~~~

`.gnupg/gpg-agent.conf` :

~~~
enable-ssh-support
pinentry-program /usr/bin/pinentry-qt
log-file $HOME/.gnupg/gpg-agent.log
~~~

Fichier `.bashrc` (ou `.zshrc`) :

~~~
pgrep -u $USER gpg-agent > /dev/null || gpg-agent --daemon
export GPG_TTY=$(tty)
export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
gpg-connect-agent updatestartuptty /bye
~~~

### Smartcard

GPG et l'agent GPG peuvent être utilisés via des cartes a puces (smartcard) ce qui permet de sécuriser vos clés GPG sur des cartes ou des tokens USB dont elles ne peuvent être extraites, l'utilisation de Smartcard nécessite les paquets suivants : 

~~~
# apt install opensc pcscd scdaemon
~~~

La carte à puce (ou le token USB) est ensuite accessible via la commande suivante :

~~~
$ gpg --card-edit
~~~

#### Quel choix de smartcard ?

Evolix recommande la [Nitrokey Start](https://shop.nitrokey.com/shop/product/nitrokey-start-6) qui est un token USB Open Source et Open Hardware !

### Signer à distance (Agent forwarding)

Il est possible d’utiliser GnuPG sur un serveur distant sans y stocker de clef privée, en transférant l’agent GPG par la connexion SSH.

* Documentation : <https://wiki.gnupg.org/AgentForwarding>

Sur la machine locale (qui a accès à la clef privée), récupérer le nom de la `<extra_socket_locale>`.

~~~
$ gpgconf --list-dir agent-extra-socket
~~~

Sur le serveur distant, récupérer le nom de la `<socket_distante>`.

~~~
$ gpgconf --list-dir agent-socket
~~~

Le transfert distant de socket peut se faire avec l’option `-R` de `ssh`, ou simplement en ajoutant la ligne `RemoteForward <socket_distante> <extra_socket_locale>` à ~/.ssh/config.

~~~
Host tunnel-gpg
HostName serveur.example.net
RemoteForward <socket_distante> <extra_socket_locale>
~~~

Sur l’hôte distant, la configuration suivante (dans `/etc/ssh/sshd_config.d/gpg-agent.conf` par exemple) permet de supprimer automatiquement les sockets bloquées).

~~~
StreamLocalBindUnlink yes
~~~

Pour pouvoir utiliser gpg sur l’hôte distant sans clef privée, il faut tout de même que la clef publique soit présente dans le trousseau.

Sur la machine locale, exporter la clef publique.

~~~
$ gpg --armor --export $identifiant_long_de_la_clef > clef.asc
$ rsync -avP clef.asc tunnel-gpg:.
~~~

Sur la machine distante, importer la clef publique et augmenter son niveau de confiance.

~~~
$ gpg --import clef.asc
$ gpg --edit-key $identifiant_long_de_la_clef
gpg> trust
[…]
Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y
[…]
gpg> save
~~~

Une fois connecté à l’hôte, l’utilisation de gpg sur l’hôte distant va maintenant s’appuyer sur le l’agent GPG de la machine locale : si la phrase secrète n’est pas déjà en cache, ou si son utilisation nécessite une carte à puce, elle sera demandée.

**Remarque** : GPG sur l’hôte distant essayera de démarrer l’agent GPG s’il n’est pas déjà démarré. Il effacera alors la socket transférée pour configurer la sienne. Pour éviter cela, il est possible de passer l’option `--no-autostart` à la commande`gpg` distante.

~~~
$ echo no-use-agent >> ~/.gnupg/gpg.conf
~~~

**Remarque** : il est nécessaire d’avoir une configuration graphique pour pinentry sur l’hôte local (ça ne fonctionne pas avec pinentry-tty par exemple).

## Organiser une KSP

Une KSP (Key Signing Party) est un évènement où l'on vérifie l'identité des personnes afin de signer leurs clés et étendre la toile de confiance PGP.  
Site qui détaille la toile de confiance : <https://pgp.cs.uu.nl/plot/>  
Voir les statistiques d'une clé : <https://pgp.cs.uu.nl/stats/IDCLE.html> ou IDCLE est remplacé par votre identifiant de clé, exemple <https://pgp.cs.uu.nl/stats/8b962943fc243f3c.html>.

Il y a plusieurs manières d'organiser une KSP, mais la méthode la plus utilisé est celle du [FOSDEM](https://github.com/FOSDEM/keysigning).

### Héberger un serveur de clés

On utilise [ksp-tools](https://github.com/formorer/ksp-tools) écrit en perl.

~~~
# apt install libhttp-daemon-perl libcgi-pm-perl liblog-loglite-perl libproc-reliable-perl libtypes-datetime-perl
# adduser --disabled-password ksp
# sudo -iu ksp
$ mkdir keys gpg
$ git clone https://github.com/formorer/ksp-tools.git
~~~

On crée une unité systemd pour lancer le démon perl.  
/etc/systemd/system/kspd.service

~~~
[Unit]
Description=KSP server.
After=network.target

[Service]
User=ksp
ExecStart=/usr/bin/perl /home/ksp/ksp-tools/bin/kspkeyserver.pl
Type=forking

[Install]
WantedBy=default.target
~~~

### Générer la liste des clés

On utilise un script du [FOSDEM](https://github.com/FOSDEM/keysigning) écrit en shell.

~~~
# sudo -iu ksp
$ git clone https://github.com/FOSDEM/keysigning.git fosdem-tools
$ cd ~/fosdem-tools/
$ ln -s ~/keys/votreVHOST/keys .
$ bin/generate-keylist.sh
~~~
