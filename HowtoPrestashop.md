---
categories: web
title: Howto PrestaShop
...

* Documentation : <https://www.prestashop.com/fr/ressources/documentation>

[PrestaShop](https://www.prestashop.com/) est une application web pour créer une boutique en ligne. Le code source est en PHP, il est disponible sur [disponible sur Github](https://github.com/PrestaShop/PrestaShop).

## Prérequis

<http://doc.prestashop.com/display/PS17/Ce+dont+vous+avez+besoin#Cedontvousavezbesoin-Instructionsdeconfigurationrapides>

* Serveur Apache ou Nginx
* PHP 5.6 ou plus.
* Extensions PHP indispensables : PDO_MySQL, cURL, SimpleXML, mcrypt, GD, OpenSSL, DOM, SOAP, Zip, fileinfo, Intl.
* MySQL 5.0 ou plus.


Réglages PHP :

~~~
allow_url_fopen on
allow_url_include = Off
register_globals Off
memory_limit >= 256M
upload_max_filesize >= "16M"
extension = php_mysql.dll
extension = php_gd2.dll
~~~

Réglages Apache : 

* mod_rewrite actif
* mod_security désactivé
* mod_auth_basic désactivé.
* AllowOverride AuthConfig Limit FileInfo Indexes Options 

Bonus : 

* Possibilité d'avoir des cron
* Memcached


## Installation

Télécharger la dernière version _stable_ de PrestaShop par archive ou par dépôt git.

~~~
git clone https://github.com/PrestaShop/PrestaShop.git
~~~

Selon la version, peut avoir un problème dans le fichier `config/autoload.php` : commenter la ligne qui gène si optionnel.

* Utiliser la ligne de commande pour installer prestashop:

~~~
$ php './install-dev/index_cli.php' --language=fr --timezone='localhost' --base_uri='/' --domain='{{ host }}' \
 --db_server='{{ db_host }}' --db_user='{{ db_user }}' --db_password='{{ db_pwd }}' --db_name='{{ db_name }}' \
 --db_clear=0 --db_create=0 --name='{{ site_title }}' --firstname='{{ firstname }}' --lastname='{{ lastname }}' \
 --password='{{ admin_pwd }}' --email='{{ admin_email }}' --newsletter=0
~~~

* supprimer le repertoire `install-dev`
* S'assurer des droits pour le groupe <user> afin que l'instance du serveur web lancé en tant que www-<user> puisse écrire dans les répertoires.

~~~
$ chmod -R g+w admin-dev/autoupgrade/ app/logs/ app/Resources/translations/ cache/ config/ download/ img/ log/ mails/ modules/ themes/ translations/ var/ upload/

~~~

* Après installation :

~~~
$ mv admin/ admin$RANDOM/
~~~

## Optimisation

Utiliser [Memcached](HowtoMemcached)

## Modules

### Soucis connu avec gamification

Le module gamification fait appel à une ip externe, qui, si ne répond pas, empêche une connexion à l'interface admin (la connexion sera très lente, voir impossible).
Le mieux est de désactiver le module (temporairement du moins) en renommant le répertoire du module:

~~~{.bash}
$ mv ~/www/modules/gamification{,.old}
~~~
