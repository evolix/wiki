---
title: Howto PortKnocking
...

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto PortKnocking

## Explication

Le Port Knocking, littéralement « taper à la porte », permet d'ouvrir un port seulement si on a été identifier en se connectant auparavant sur un ou plusieurs ports.

## Exemple de script

~~~
#!/bin/bash
export IF_RESEAU="eth4"

start_fw()
{
        #Autoriser seulement tout en sortie.
	/sbin/iptables -P INPUT DROP
	/sbin/iptables -P FORWARD DROP
	/sbin/iptables -P OUTPUT ACCEPT
        #Autoriser Boucle local.
	/sbin/iptables -A INPUT -i lo -j ACCEPT
	/sbin/iptables -A OUTPUT -o lo -j ACCEPT
        #Autoriser en entrée seulement si la connexion précédente à déjà été établie
	/sbin/iptables -A INPUT  -i $IF_RESEAU --match state --state ESTABLISHED -j ACCEPT
        #Autoriser l'ICMP
	/sbin/iptables -A INPUT -p icmp -j ACCEPT
        
        #Port Knocking pour ouvrir le port SSH
        /sbin/iptables -A INPUT -m recent --rcheck --seconds 5 --name SSH -m state --state NEW -p tcp --dport ssh -j ACCEPT
        /sbin/iptables -A INPUT -i $IF_RESEAU -m state --state NEW -m tcp -p tcp --dport 1600 -m recent --name SSH --set -j DROP
	
        # Chargement du module de gestion des connexion state (autorisation des connexions deja etablies a passer le firewall)
	/sbin/modprobe ip_conntrack
}
stop_fw() 
{

        # Vidage des règles pour toutes les tables :
        iptables -F
        iptables -X
    
        # On remet la politique par défaut à ACCEPT dans les trois tables par défaut
        iptables -P INPUT ACCEPT
        iptables -P OUTPUT ACCEPT
        iptables -P FORWARD ACCEPT

}

case "$1" in
	start)
		start_fw
		echo "firewall started"
		;;

	stop)
		stop_fw 
		echo "firewall stopped"
		;;

	restart)
		stop_fw
		echo "firewall stopped"
		start_fw
		echo "firewall restarted"
		;;
*)
		echo "usage: $0 [start|stop|restart]" >&2
		exit 1

esac
exit 0

~~~

Il faut retenir ces deux lignes, qui permettent d'activer le port knocking. On doit « taper » sur le port 1600, pour autoriser une connexion SSH, dans les 5 secondes qui suivent.

~~~
/sbin/iptables -A INPUT -m recent --rcheck --seconds 5 --name SSH -m state --state NEW -p tcp --dport ssh -j ACCEPT
/sbin/iptables -A INPUT -i $IF_RESEAU -m state --state NEW -m tcp -p tcp --dport 1600 -m recent --name SSH --set -j DROP
~~~


Ensuite pour pouvoir se connecter, il faut « taper » au port 1600, puis dans les 5 secondes se connecter en SSH. On utilise pour cela _netcat_ : 

~~~
$ nc -w 1 host 1600 < /dev/null > /dev/null 2>&1
$ ssh host
~~~

## Automatisation

L'idéal est de mettre en place un alias par exemple : 

~~~
alias sshHostParticulier="nc -w 1 192.168.0.1 1600 < /dev/null > /dev/null 2>&1; ssh root@192.168.0.1"
~~~

## Utilisation de knockd

knockd est un démon qui permet d'écouter sur une liste de ports donnés pour une séquence de « knocks » et d'exécuter une commande si la séquence reçue correspond à celle configurée.

Voir <http://www.zeroflux.org/projects/knock.>

### Installation

knockd est présent dans les paquets Debian :

~~~
# aptitude install knockd
~~~

### Configuration

Exemple de configuration pour ouvrir le port SSH :

~~~
[opencloseSSH]
        sequence      = 2222:tcp,3333:tcp,4444:tcp
        seq_timeout   = 15
        tcpflags      = syn
        start_command = /usr/sbin/iptables -A INPUT -s %IP% -p tcp --dport 22 -j ACCEPT
        cmd_timeout   = 86400
        stop_command  = /usr/sbin/iptables -D INPUT -s %IP% -p tcp --dport 22 -j ACCEPT
~~~

Dans l'exemple ci-dessus, il est nécessaire d'envoyer un paquet TCP avec le flag SYN successivement sur les ports TCP/2222, TCP/3333, TCP/4444 dans un délai de 15 secondes maximum pour que la commande spécifiée dans la directive `start_command` soit exécutée.

Ici on autorise l'adresse IP qui a initiée la séquence à se connecter sur le port 22. Cette autorisation est valable 1 jour (`cmd_timeout`), au delà, l'accès est à nouveau fermé. Les directives `cmd_timeout` et `stop_command` sont optionnelles.
