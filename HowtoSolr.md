# Howto Solr

Solr est un logiciel de moteur de recherche s'appuyant sur la bibliothèque de recherche Lucene et conçue sous licence libre. Il communique avec le client à l'aide d'une interface de programmation en XML et JSON, généralement via le protocole HTTP. 

Ressources :

  * <http://lucene.apache.org/solr/>
  * <http://wiki.apache.org/solr/>

## Installation

On installe le paquet 'solr-tomcat' :

~~~
# apt install solr-tomcat
~~~

## Configuration

Par défaut, les données sont dans */var/lib/solr*.
Si l'on souhaite les stocker ailleurs (par exemple _/srv/solr_), on gèrera un lien symbolique :
`var/lib/solr -> /srv/solr`

La configuration se trouve dans le répertoire _/etc/solr/_

## Multi cores

Pour lancer plusieurs *cores* sur le même Solr, commenter la configuration par défaut et rajouter vos *cores* dans /etc/solr/solr.xml :

~~~
  <--
  <cores adminPath="/admin/cores" defaultCoreName="collection1">
    <core name="collection1" instanceDir="." />
  </cores>
  -->
  <cores defaultCoreName="core1" adminPath="/admin/cores">
    <core instanceDir="core1/" name="core1"/>
    <core instanceDir="core2/" name="core2"/>
    <core instanceDir="core3/" name="core3"/>
  </cores>
~~~

Puis créer le dossiers de configuration et de donnes par *core* :  

~~~
mkdir -m 0755 /etc/solr/core1
mkdir -m 0777 /usr/share/solr/core1
mkdir -m 0770 /var/lib/solr/core1
chown tomcat8: /var/lib/solr/core1
~~~

Ensuite faire les liens symboliques nécessaires par *core* :

~~~
ln -s /etc/solr/conf /etc/solr/core1/conf
ln -s /etc/solr/core1 /usr/share/solr/core1/conf
ln -s /var/lib/solr/core1 /usr/share/solr/core1/data
~~~

Enfin, redémarrer tomcat :

~~~
systemctl restart tomcat8
~~~

## Trop de logs ?

Si c'est trop verbeux dans catalina.out (ou catalina.date.log), il faut changer le niveau de verbosité dans son interface d'admin. Exemple avec un tunnel :

~~~
ssh -L 8080::8080 unemachine-solr
~~~

