**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Plugin Redmine Git Hosting + Gitolite
 
#### Redmine et Gitolite doivent se situer sur le même serveur !

## Prérequis

Installation de gitolite 3 (sous Wheezy nécessite les backports) :

~~~
# aptitude install gitolite3
~~~

Installation des dépendances Redmine Git Hosting :

~~~
# aptitude install build-essential libssh2-1 libssh2-1-dev cmake libgpg-error-dev
~~~

Mise en place de [wiki:HowtoGitDaemon git-daemon] et [wiki:HowtoGitWeb gitweb] (optionnel).

## I. Installation Gitolite

Choix de l'utilisateur $GITOLITE :

* $GITOLITE : utilisateur propriétaire de l'application
* accès SSH depuis $GITOLITE@votre-domaine.tld

~~~
GITOLITE='git'
~~~

Création de l'utilisateur système $GITOLITE ainsi que son groupe.

~~~
useradd $GITOLITE -d "/home/$GITOLITE" -c "Gitolite $GITOLITE" -s "/bin/bash" -m
~~~

Initialisation de Gitolite :

~~~
cp /home/$REDMINE/.ssh/redmine_gitolite_admin_id_rsa.pub /home/$GITOLITE/
chown $GITOLITE: /home/$GITOLITE/redmine_gitolite_admin_id_rsa.pub
su - $GITOLITE -c "gitolite setup -pk redmine_gitolite_admin_id_rsa.pub"
rm /home/$GITOLITE/redmine_gitolite_admin_id_rsa.pub
~~~

Configuration gitolite3 :

~~~
chmod 750 -R /home/$GITOLITE/repositories /home/$GITOLITE/projects.list
vi /home/$GITOLITE/.gitolite.rc
#changer UMASK de 0077 à 0027
#changer GIT_CONFIG_KEYS de '' à '.*'
#décommenter la ligne : LOCAL_CODE                =>  "$ENV{HOME}/local"
~~~

Interdit l'utilisateur $GITOLITE de se connecter avec un mot de passe et l'autorise à se connecter si l'accès SSH est limité :

~~~
cat >> /etc/ssh/sshd_config <<SSH
Match User $GITOLITE
    PasswordAuthentication no
SSH
sed -ie "/^AllowUsers/s/.*/& $GITOLITE/" /etc/ssh/sshd_config
sed -ie "/^AllowGroups/s/.*/& $GITOLITE/" /etc/ssh/sshd_config
service ssh reload
~~~

### II. Installation du plugin redmine Git Hosting -User = $REDMINE)

Connection en tant que l'utilisateur $REDMINE :

~~~
su - $REDMINE
~~~

Génération de la clé SSH :

~~~
ssh-keygen -N '' -f /home/$REDMINE/.ssh/redmine_gitolite_admin_id_rsa
~~~

#### Cette clé aura accès à tous les dépots de Gitolite !
Ajout de localhost dans les know_hosts de l'utilisateur $REDMINE (vous devez accepter le fingerprint !) :

~~~
ssh localhost
~~~

Clonage des plugins dans redmine/plugins :

~~~
git clone <https://github.com/jbox-web/redmine_bootstrap_kit.git> -b v0.2.x ~/redmine/plugins/redmine_bootstrap_kit
git clone <https://github.com/jbox-web/redmine_git_hosting.git> -b v1.2.x ~/redmine/plugins/redmine_git_hosting
~~~

Ajout du scm Xitolite dans Redmine :

~~~
echo "  scm_xitolite_command: /usr/bin/git" >> ~/redmine/config/configuration.yml
~~~

### III. Configuration de l'accès sudo (User = root) 

*A faire même si l'utilisateur $GITOLITE est le même que $REDMINE*

Autorise $REDMINE à sudo vers $Gitolite :

~~~
cat > /etc/sudoers.d/$REDMINE\_$GITOLITE <<SUDO
Defaults:$REDMINE !requiretty
$REDMINE ALL=($GITOLITE) NOPASSWD:ALL
SUDO
chmod 440 /etc/sudoers.d/$REDMINE\_$GITOLITE
~~~

#### Si le plugin est installé après Redmine et non en même temps

Exécuter les taches [wiki:HowToRedmine-Source/Plugins post-installation].

### Configuration du plugin

* Rendez vous dans Administration -> Redmine Git Hosting Plugin -> SSH et remplacer :
* Nom d'utilisateur Gitolite par la valeur de $GITOLITE
* L'emplacement des clés privées et publiques SSH de Gitolite par le dossier .ssh de l'utilisateur $REDMINE
* Rendez vous dans Administration -> Redmine Git Hosting Plugin -> Accès et remplacer :
* Nom de domaine du serveur HTTP et HTTPS par le nom de domaine de votre serveur
* Nom de domaine du serveur SSH par $GITOLITE.votredomain.tld
* Rendez vous dans Administration -> Redmine Git Hosting Plugin -> Stockage et remplacer :
* Répertoire de stockage des hooks non-core par hooks
* Rendez vous dans Administration -> Redmine Git Hosting Plugin -> Hooks et remplacer :
* Url des hooks par <https://votre-domaine.tld>
* Cliquer en haut à droit sur installer les hooks !
* Rendez vous dans Administration -> Redmine Git Hosting Plugin -> Test de la configuration et vérifier que tous les indicateurs sont au vert