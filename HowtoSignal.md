# Howto Signal

## signal-desktop

<https://signal.org/fr/download/linux/>

## signalbackup-tools

<https://github.com/bepaald/signalbackup-tools>

~~~
# apt install libsqlite3-dev
$ git clone https://github.com/bepaald/signalbackup-tools.git
$ ./BUILDSCRIPT.bash --config without_dbus
~~~

~~~
$ ./signalbackup-tools ~/tmp/signal.backup --dumpmedia medias/
signalbackup-tools (./signalbackup-tools) source version 20241220.161546 (SQlite: 3.40.1, OpenSSL: OpenSSL 3.0.15 3 Sep 2024)
Please provide passphrase for input file '~/tmp/signal.backup': ***** ***** ********** ***** *****
BACKUPFILE VERSION: 1
BACKUPFILE SIZE: 9135859762
COUNTER: 3483898945
Reading backup file: 100.0%... done!
Database version: 260
[Warning]: Foreign key constraint violated.
-----------------------------
| table    | parent  | fkid |
-----------------------------
| reaction | message | 1    |
-----------------------------
Dumping media to dir 'medias/'
Saving attachments... done.

$ ./signalbackup-tools ~/tmp/signal.backup --deleteattachments --output signal-no_medias.backup
...
Done! Wrote 866511882 bytes.

$ ./signalbackup-tools ~/tmp/signal.backup --deleteattachments --onlyolderthan "2024-01-01 00:00:00" --output signal-no_medias.backup
...
Done! Wrote 2209229805 bytes.
~~~

