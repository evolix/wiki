---
title: Howto HAProxy - les checks
category: web HA
---

Cet article se focalise sur la manière dont HAProxy fait ses checks et dont on peut les configurer.

Pour une documentation plus générale, consultez la page [/HowtoHAProxy]().

Liens externes utiles :

* <https://www.haproxy.com/blog/haproxy-configuration-basics-load-balance-your-servers>
* <https://www.haproxy.com/blog/the-four-essential-sections-of-an-haproxy-configuration>
* <https://www.haproxy.com/documentation/haproxy-configuration-tutorials/service-reliability/health-checks/>

## Pas de check

~~~
backend foo
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Dans cet exemple, les 2 serveurs sont réputés être disponibles en tout temps.
La répartition des requêtes dépend de l'option `balance`.

## Check actif

Doc : <http://docs.haproxy.org/2.6/configuration.html#check>

### check simple

~~~
backend foo
    default-server check
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Dans cet exemple, un check actif est exécuté à intervale régulier.
Le check est fait sur la plus haute couche de transport configurée : TCP par défaut, ou SSL/TLS si l'option `ssl` (ou `check-ssl`) est présente.

### check HTTP

Doc : <http://docs.haproxy.org/2.6/configuration.html#option%20httpchk>

~~~
backend foo
    option httpchk
    default-server check
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Dans cet exemple, le check est fait sur la couche applicative HTTP.
Le check est une requête `OPTIONS /` sur le port de proxification (ici `80`).

Le check est en succès si la partie transport (connexion TCP et/ou SSL/TLS) **et** la partie applicative (réponse HTTP correcte) est OK.

~~~
backend foo
    option httpchk
    default-server check port 81
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple montre comment spécifier un port spécifique (`81`) pour le check.

~~~
backend foo
    option httpchk GET /healthz
    default-server check
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple montre comment modifier la requête HTTP envoyée (`GET /healthz`) par le check.

Il existe aussi de nombreuses options pour personnaliser le comportement du check ; status attendu, contenu attendu, mode de connexion…

### Fréquence des checks

Doc : <http://docs.haproxy.org/2.6/configuration.html#inter>

~~~
backend foo
    option httpchk
    default-server check inter 1s
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Dans cet exemple on voit l'option `inter <delay>` qui permet de personnaliser la fréquence des checks (défaut: `2` secondes).


~~~
backend foo
    option httpchk
    default-server check inter 1s fastinter 500 downinter 1m
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple montre que le check est exécuté chaque seconde pour un serveur `UP`, mais toutes les 500 millisecondes lorsque l'état est en transition (après un échec quand il est `UP`, ou après un succès quand il est `DOWN`), et toutes les minutes lorsqu'il est `DOWN`.

### Seuils des checks

Doc :

* <http://docs.haproxy.org/2.6/configuration.html#fall>
* <http://docs.haproxy.org/2.6/configuration.html#rise>

~~~
backend foo
    option httpchk
    default-server check fall 5 rise 10
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple montre comment personnaliser le nombre d'échecs consécutifs (`fall 5`) avant de passer en statut `DOWN` (défaut: `3`), et le nombre de succès (`rise 10`) avant de passer en statut `UP` (défaut: `2`).

Note: Si pendant une transition d'état le service revient (ne serait-ce qu'une fois) à l'état précédent, le compteur est remis à zéro. Il faut en tenir compte si on a des situation de « flap » pour ne pas se retrouver bloqué avec un service ne changerait jamais d'état à cause de compteurs de `fall` ou `rise` qui seraient constamment remis à zéro et n'atteindraient jamais leur valeurs.

### Timeouts

Doc : <http://docs.haproxy.org/2.6/configuration.html#timeout%20check>

Pour les checks actifs, le timeout par défaut est la valeur minimale entre la fréquence du check (`inter`) et le délai de connextion (`timeout connect`). Cela permet de ne pas avoir plusieurs checks identiques en cours en même temps.

~~~
backend foo
    option httpchk
    timeout check 100
    default-server check inter 1000
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple montre comment personnaliser le timeout du check à `100 millisecondes` tout en conservant une fréquence de check à `1 seconde`.


### Checks "agent"

Doc : <http://docs.haproxy.org/2.6/configuration.html#agent-check>

Il est possible d'utiliser des checks actifs plus avancés, appelés `agent-check`.
Les capacités de personnalisation sont similaires.

La différence est surtout dans le fait qu'on interroge un agent spécifique sur les serveurs distants (et pas l'application proxifiée elle-même) et qu'on s'attend à une information à la fois simple (pas de HTTP) et plus riche (possibilité d'instruire HAProxy sur des états plus riches : statut, poids…).

Il est important de noter qu'un changement d'état ordonné par un check "agent" prend effet immédiatement, sans tenir compte des valeurs de `fall` et `rise`.

### Dépendance d'état

Il est possible de définir une dépendance d'état pour un serveur au sein d'un backend, sur un serveur dans un autre backend.

L'utilitité est de ne faire le check actif qu'une fois, mais d'appliquer l'état sur plusieurs serveurs.

Ça fonctionne également si le changement d'état se fait manuellement (via l'interface web, ou l'API).


~~~
backend foo
    server srv1 192.0.2.1:80 check
    server srv2 192.0.2.2:80 check

backend bar
    server srv1 192.0.2.1:80 track foo/srv1
    server srv2 192.0.2.2:80 track foo/srv2
~~~

Dans l'interface web, les colonnes liées aux checks indiqueront alors `via foo/srv1` et `via foo/srv2` au lieu des infos détailéles des checks.

## Checks passifs

En plus des checks actifs, il est possible de surveiller le flux de données de manière passive, grace à l'utilisation de la directive `observe`.

Doc :

* <http://docs.haproxy.org/2.6/configuration.html#observe>
* <http://docs.haproxy.org/2.6/configuration.html#on-error>

### observe layer4

~~~
backend foo
    default-server check observe layer4 inter 1m
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple montre comment activer la surveillance passive au niveau de la couche transport.
Ici, la fréquence des checks actifs est réglée à 1 minute (indépendamment de l'intensité du trafic réel), mais si des erreurs de connexion constatées sur le trafic géré, alors on va passer un serveur `DOWN` plus rapidement qu'avec les checks actifs.

L'option `on-error` permet de définir le comportement lorsqu'une telle erreur est détectée. Par défaut c'est `on error fail-check` qui est équivalent à un échec de check actif (et provoque donc le passage en mode `fastinter`).

### observe layer7

~~~
backend foo
    option httpchk
    default-server check observe layer7 inter 1m
    server srv1 192.0.2.1:80
    server srv2 192.0.2.2:80
~~~

Cet exemple est quasi identique au précédent, sauf que la surveillance passive du trafic se fait au niveau de la couche applicative (HTTP ici), en plus de la couche transport. Des entêtes imparsables, ou un code HTTP (autre que `100` à `499`, `501` ou `505`).

## Divers

### Espacer les checks

Si on a de nombreux checks sur un même HOST:PORT (par exemple des backends multiles qui envoient sur les mêmes serveurs finaux) il peut y avoir un phénomène de martelement.

Il est possible d'introduire un petit délai aléatoire dans le lancement des checks avec l'option [spead-checks](https://docs.haproxy.org/2.6/configuration.html#spread-checks).  
La valeur est en pourcentage, comprise entre `0` et `50`. Il est courrant de choisir une valeur entre `2` et `5`.