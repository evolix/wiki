**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Fetchmail

~~~
# apt install fetchmail
~~~

Config via /etc/fetchmailrc :


~~~
set postmaster "postmaster"
set no spambounce
set no syslog
set logfile /var/log/fetchmail.log
set properties ""
set daemon 60
set no bouncemail

defaults
# Retrieve only new messages
#no fetchall
# Retrieve all messages
#fetchall
smtpaddress mail.example.com
# on vire le Deliver-To: pour eviter les boucles foireuses
dropdelivered

poll pop.example.com with proto POP3

user 'jdoe@example.com' there with password 'PASS' is 'john@example.org' here
~~~