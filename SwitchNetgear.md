**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Switch Netgear GS724T (24 ports Gigabit)

<http://kb.netgear.com/app/products/model/a_id/2451> 

Il n'est pas vraiment manageable. On y accède par une interface web. Par défaut son adresse est 192.168.0.239 et son mot de passe password

## Switch Netgear FSM7352S (48 ports Fast)

Voici la config minicom (à mettre dans /etc/minicom/minirc.switch) :

~~~
pu port             /dev/cua01
pu baudrate         9600
pu bits             8
pu parity           N
pu stopbits         1
pu rtscts           No
~~~

Quelques commandes utiles :

~~~
> en
(FSM7352S) #show vlan

VLAN ID VLAN Name                        VLAN Type
------- -------------------------------- ---------
1       Default                           Default  
2       Foobar                            Static   

(FSM7352S) #show vlan 1

VLAN ID: 1     
VLAN Name: Default
VLAN Type: Default

Interface   Current   Configured   Tagging 
----------  --------  -----------  --------
1/0/1       Include   Include      Untagged  
1/0/2       Include   Include      Untagged  
1/0/3       Include   Include      Untagged  
1/0/4       Include   Include      Untagged  
1/0/5       Include   Include      Untagged  
1/0/6       Include   Include      Untagged  
[...]


(FSM7352S) #conf
(FSM7352S) (Config)#interface 1/0/48
(FSM7352S) (Interface 1/0/48)#vlan pvid 2
(FSM7352S) (Interface 1/0/48)#vlan participation include 2
~~~
