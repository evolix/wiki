- Documentation : [https://www.dell.com/support/kbdoc/en-uk/000177280/how-to-use-the-poweredge-raid-controller-perc-command-line-interface-cli-utility-to-manage-your-raid-controller]()
- Status de cette page : unstable / bullseye

[PERCCLI](https://www.dell.com/support/kbdoc/en-uk/000177280/how-to-use-the-poweredge-raid-controller-perc-command-line-interface-cli-utility-to-manage-your-raid-controller) est un utilitaire en ligne de commande permettant de gérer les contrôleurs RAID de Dell.

Dans notre usage PERCCLI remplace [MegaCLI](InfosMegaCLI). PERCCLI est en réalité un rebranding du logiciel [StorCLI](https://docs.broadcom.com/doc/12352476) édité par Broadcom.

# Installation

## PERCCLI

Version à installer par défaut.

On télécharge `perccli` et on l'installe dans `/usr/local/sbin/perccli` :

~~~(sh)
# curl -A notcurl -OL https://dl.dell.com/FOLDER09770976M/1/PERCCLI_7.2313.0_A14_Linux.tar.gz
# bsdtar  -xOf PERCCLI_7.2313.0_A14_Linux.tar.gz PERCCLI_7.2313.0_A14_Linux/perccli-007.2313.0000.0000-1.noarch.rpm |
  bsdtar -C /usr/local/sbin/ -'s|\./opt/MegaRAID/perccli/perccli64|perccli|' -xf- ./opt/MegaRAID/perccli/perccli64
~~~

## PERCCLI2 

Version à installer uniquement pour les adaptateurs Dell PERC H965i.

On télécharge `perccli2` et on l'installe dans `/usr/local/sbin/perccli2` :

~~~(sh)
# curl -A notcurl -OL  https://dl.dell.com/FOLDER09970165M/1/perccli2_8.4.0.22_linux.tar.gz
# bsdtar -xOf perccli2_8.4.0.22_linux.tar.gz perccli2_8.4.0.22_linux/perccli2-008.0004.0000.0022-1.x86_64.rpm | bsdtar -C /usr/local/sbin/ -'s|\./opt/MegaRAID/perccli2/perccli2|perccli2|' -xf- ./opt/MegaRAID/perccli2/perccli2
~~~


# Pierre de rosette

| PERCCLI                            | MegaCLI                                 | Description       |
|------------------------------------+-----------------------------------------+-------------------|
| `perccli /call show all`           | `megacli -adpallinfo -aALL`             | Adaptateurs       |
| `perccli /call/eall/sall show all` | `megacli -pdlist -aALL`                 | Disques physiques |
| `perccli /call/vall show all`      | `megacli -LDInfo -Lall -aALL`           | Disques virtuels  |
| `perccli /call show events`        | `megacli -AdpEventLog -GetEvents -aALL` | Logs              |
| `perccli /call show alilog`        | `megacli -AdpAlILog -aALL`              | NVRAM logs        |

[Documentation de Broadcom](https://www.broadcom.com/support/knowledgebase/1211161499760/lsi-command-line-interface-cross-reference-megacli-vs-twcli-vs-s) traduisant les commandes `MegaCLI` en equivalant
`StorCLI` (équivalant à `PERCCLI`).

# Explications

## Syntaxe de la ligne de commande

La commande `perccli` permet d'agir sur différents éléments des cartes RAID. Ci-dessous se trouve la liste des premiers arguments supporté par la commande associée à leurs éléments, `X` correspond à l'index (commençant à 0) de cet élément :

- `/cX` : un contrôleur
- `/eX` : un berceau (*enclosure*)
- `/sX` : un emplacement (*slot*)
- `/vX` : un disque virtuel
- `/dX` : un groupe de disques
- `/fall` : la configuration externe (*foreign configuration*)
- `/pX` : un disque physique
- `/lnX` : une ligne (*Lane*) 
- `/bbu` : la batterie (*battery backup unit*)
- `/cv` : le cache (*cache vault*)

Par exemple pour avoir l'état du premier controleur (son index est donc 0) il faut faire `perccli /c0 show`.

Pour avoir une sortie en JSON d'une commande il suffit de la suffixer avec l'argument `J`. Par exemple pour avoir l'état du la batterie du premier contrôleur en JSON il faut exécuter `perccli /c0/bbu show J`.

## Exemple de sortie de la commande

```
# perccli show
CLI Version = 007.2313.0000.0000 Mar 07, 2023
Operating system = Linux 6.1.0-16-amd64
Status Code = 0
Status = Success
Description = None

Number of Controllers = 1
Host Name = parbackup4bis
Operating System  = Linux 6.1.0-16-amd64

System Overview :
===============

-------------------------------------------------------------------------
Ctl Model         Ports PDs DGs DNOpt VDs VNOpt BBU sPR DS EHS ASOs Hlth
-------------------------------------------------------------------------
  0 PERCH755Front    16   4   1     0   1     0 Opt On  -  N      0 Opt
-------------------------------------------------------------------------

Ctl=Controller Index|DGs=Drive groups|VDs=Virtual drives|Fld=Failed
PDs=Physical drives|DNOpt=Array NotOptimal|VNOpt=VD NotOptimal|Opt=Optimal
Msng=Missing|Dgd=Degraded|NdAtn=Need Attention|Unkwn=Unknown
sPR=Scheduled Patrol Read|DS=DimmerSwitch|EHS=Emergency Spare Drive
Y=Yes|N=No|ASOs=Advanced Software Options|BBU=Battery backup unit/CV
Hlth=Health|Safe=Safe-mode boot|CertProv-Certificate Provision mode
Chrg=Charging | MsngCbl=Cable Failure
```

## Abréviations de la sortie standard

De nombreuses abréviations sont utilisées dans la sortie standard, en voici une **liste non exhaustive** :

- ASOs : Advanced Software Options
- AWB : Always WriteBack
- AftrLD : Identify Freespace After LD
- AftrVD : Identify Freespace After VD
- Alms : Alarm count
- Arr : Array Index
- B : Blocked
- BBU : Battery backup unit/CV
- BT : Background Task Active
- C : Cached IO
- CBShld : Copyback Shielded
- CFShld : Configured shielded
- CO : Cache Offload
- CR : CacheCade(Read)
- CW : CacheCade(Read/Write)
- Cac : CacheCade
- CertProv : Certificate Provision mode
- Chrg : Charging
- Cl : Cluster
- Cmp : Compatible Profile
- Consist : Consistent
- Cpybck : CopyBack
- Ctl : Controller Index
- Curr : Current Profile
- DG : Drive Group
- DGs : Drive groups
- DHS : Dedicated Hot Spare
- DID : Device ID
- DNOpt : Array NotOptimal
- DS3 : Dimmer Switch 3
- DS : DimmerSwitch
- Dflt : Default Profile
- Dgd : Degraded
- Dgrd : Degraded
- EHS : Emergency Spare Drive
- EID : Enclosure Device ID
- F : Foreign
- FC : Flush Commands
- FP : Fast Path
- FSpace : Free Space Present
- FUA : Force Unit Access
- Fld : Failed
- FreSpc : Freespace available before expansion
- Frgn : Foreign
- GHS : Global Hotspare
- HA : High Availability
- HD : Hidden
- HSPShld : Hotspare shielded
- Hlth : Health
- IOPs : IO per second
- InDwdCnt : Invalid Dword Count
- Intf : Interface
- InvalDwdCnt : Invalid Dword count
- LosOfDwrdSynCnt : Loss of Dword synchronization count
- LsDwSyCnt : Loss of Dword system Count
- MD : Max Disks
- MaxAHCIDev : Maximum AHCI Devices
- MaxLD : Maximum Logical Drives
- MaxPCIeDev : Maximum PCIe Devices
- MaxPhyDrv : Maximum Physical Drives
- Med : Media Type
- Msng : Missing
- MsngCbl : Cable Failure
- MsngCbl : Missing Cable
- N : No
- NR : No Read Ahead
- NdAtn : Need Attention
- NoArrExp  :  Without Array Expansion
- OCE : Online Capacity Expansion
- OfLn : OffLine
- Offln : Offline
- Onln : Online
- Opt : Optimal
- Optl : Optimal
- Optm : Optimal Profile
- PD : Physical drive count
- PDC : Drive Cache
- PDC : PD Cache
- PDs : Physical drives
- PI : Protection Info
- PNO : Phy Number
- PS : Power Supply Count
- Pdgd : Partially degraded
- Pend : Pending Profile
- PhyNo : Phy Number
- PhyResetPrbCnt : Phy reset problem count
- ProdID : Product ID
- ProfID : Profile ID
- R : Read Ahead Always
- RC : Read Commands
- RF : Reduced Feature Set
- RO : Read Only
- RW : Read Write
- Rbld : Rebuild
- Re : Recovery
- Read RT : Average Read Response Time
- Rec : Recovery
- RepInProg : Repair in progress
- RnDiErCnt : Running Disparity Error Count
- Row : Row Index
- RungDispartyErrCnt : Running Disparity error count
- SED : Self Encrypting Drive
- SIM : SIM Count
- SR : Sectors Read
- SS : Safe Store
- SSHA : Single server High Availability
- SW-FUA : Sectors written with FUA
- SW : Sectors written
- Safe : Safe-mode boot
- SeSz : Sector Size
- SlotBP : Bays On Port
- Sntze : Sanitize
- Sp : Spun
- Span : Span Index
- T : TransitioTRANS=TransportReady
- TSs : Temperature Sensor count
- Temperature : Temperature in Degree Celsius
- Total RT : Average Total Response Time
- Type : Drive Type
- UBUnsp : UBad Unsupported
- UBad : Unconfigured Bad
- UGShld : UGood shielded
- UGUnsp : UGood Unsupported
- UGood : Unconfigured Good
- Unkwn : Unknown
- VD : Virtual Drive
- VDs : Virtual drives
- WB : WriteBack
- WBSup : Write Back Support
- WC-FUA : Write Commands with FUA
- WT : WriteThrough
- Write RT : Average Write Response Time
- X : Not Available/Not Installed
- Xfer Rate : Transfer Rate
- Y : Yes
- dflt : Default
- sCC : Scheduled
- sPR : Scheduled Patrol Read
- us : micro seconds


# Utilisation

## Avoir un aperçu des contrôleurs RAID d'un système

~~~
# perccli /c0/bbu show all 
CLI Version = 007.2313.0000.0000 Mar 07, 2023
Operating system = Linux 5.10.0-24-amd64
Status Code = 0
Status = Success
Description = None

Number of Controllers = 1
Host Name = foo
Operating System  = Linux 5.10.0-24-amd64

System Overview :
===============

---------------------------------------------------------------------------
Ctl Model           Ports PDs DGs DNOpt VDs VNOpt BBU sPR DS EHS ASOs Hlth 
---------------------------------------------------------------------------
  0 PERCH750Adapter     8  10   4     0   4     0 Opt On  -  N      0 Opt  
---------------------------------------------------------------------------

Ctl=Controller Index|DGs=Drive groups|VDs=Virtual drives|Fld=Failed
PDs=Physical drives|DNOpt=Array NotOptimal|VNOpt=VD NotOptimal|Opt=Optimal
Msng=Missing|Dgd=Degraded|NdAtn=Need Attention|Unkwn=Unknown
sPR=Scheduled Patrol Read|DS=DimmerSwitch|EHS=Emergency Spare Drive
Y=Yes|N=No|ASOs=Advanced Software Options|BBU=Battery backup unit/CV
Hlth=Health|Safe=Safe-mode boot|CertProv-Certificate Provision mode
Chrg=Charging | MsngCbl=Cable Failure
~~~

# Mode d'emploi

## Actions sur les disques physiques

### Localiser un disque

Faire clignoter un disque :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" start locate
~~~

Arrêter le clignotement d'un disque :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" stop locate
~~~

### Ajouter un disque dans un volume RAID (Disk Group)

Il faudra probablement initialiser le nouveau disque (par exemple si il est en état "UGood" Unconfigured Good) :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" start initialization
~~~

On peut suivre l'initialisation (qui peut durer plusieurs dizaines de minutes) ainsi :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" show initialization
~~~

Il faut ensuite ajouter le disque en "hotspare" dans le bon volume RAID (DG pour Disk Group) :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" add hotsparedrive DGs=N
~~~

Et l'on pourra suivre le « rebuild » (cf plus bas).


### Passer un disque en OFFLINE

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" set offline
~~~

### Passer un disque en ONLINE

**!!! ATTENTION !!! ATTENTION !!! ATTENTION !!!**

L’erreur fatale à ne pas commettre lorsque vous constatez un disque en état
Firmware state: Failed est de le forcer à revenir Online. En effet, cela
force à repasser en mode RAID sans reconstruction et vous pouvez dire adieu
à votre système de fichiers. NE FAITES DONC PAS :

~~~ 
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" set online
~~~ 

### Passer un disque en missing

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" set online
~~~

Ça le sort du RAID.

### Contrôler la rotation d'un disque rotatif

Stopper la rotation d’un disque, dans le but de le retirer :

~~~ 
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" spindown
~~~ 

Activer la rotation d’un disque :

~~~ 
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" spinup
~~~ 

### Lancer la reconstruction d'un disque

~~~ 
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" start rebuild
~~~ 

### Contrôler la reconstruction d'un disque

~~~ 
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" show rebuild
~~~ 

### JBOD

a priori on ne veut PAS utiliser cette fonctionnalité, le mode JBOD (Just a Bunch Of Disks) signifie que le disque est disponible sans RAID.
On peut mettre plusieurs disques en JBOD a priori : ça ressemble à du RAID0 en moins bien. Et dans les cas où l'on veut utiliser directement le disque on utiliser le mode « PassThru ».
Bref, on n'utilise pas sauf exception.

Expliquons néanmoins comment on manipule ce mode.

Il faut initialiser le disque et le mettre en JBOD :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" start initialization
perccli "/c${controler}/e${enclosure}/s${slot}" set jbod
~~~

Cela devrait faire apparaître un nouveau device disponible sur le système (cc "lsblk").

Si l'on veut supprimer ce disque JBOD :

~~~
controler=0 ; enclosure=252 ; slot=0
perccli "/c${controler}/e${enclosure}/s${slot}" del jbod
~~~


## Actions sur les disques virtuels

### Extensions à chaud d'un disque virtuel

~~~ 
controler=0 ; virtual=239 ; size=1337
perccli "/c${controler}/v${virtual}" expand "Size=$size"
~~~ 

### Suivre l'extension d'un disque virtuel

~~~ 
controler=0 ; virtual=239
perccli "/c${controler}/v${virtual}" show expansion
~~~ 

## Actions sur les controleurs disques

### Accéder aux logs d'un controller

~~~ 
controler=0
perccli "/c${controler}" show alilog
~~~ 

## FAQ

### Failure 255 Operation not allowed

Si en jouant des commandes on obtient :

~~~
------------------------------------------------
Drive      Status  ErrCd ErrMsg
------------------------------------------------
/cX/eX/sX Failure   255 Operation not allowed.
~~~

Cela veut a priori dire que l'élément n'est pas prêt pour faire cette opération : soit il faut faire d'autres opérations au préalable, soit l'opération est impossible (c'est-à-dire qu'elle doit se faire autrement).

### insert

Si l'on veut ajouter un disque dans un volume RAID, on pourrait penser qu'il faut utiliser "insert".
Mais si l'on fait cela on obtient "Operation not allowed" :

~~~
# perccli /cX/eX/sX insert dg=X array=X row=X
Status = Failure
Description = Insert Drive Failed.

------------------------------------------------
Drive      Status  ErrCd ErrMsg
------------------------------------------------
/cX/eX/sX Failure   255 Operation not allowed.
~~~

On passe par "add hotsparedrive" cf plus haut.


