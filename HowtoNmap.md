---
categories: network
title: Howto Nmap
---


* Documentation Nmap : <https://nmap.org/docs.html>


[Nmap](https://nmap.org/) est un outil libre très populaire de scan de ports & de réseaux. Il est couplé d'un puissant moteur d'exécution de scripts qui permet notamment, suite à un scan, d'identifier la machine et les services détectés.


## Installation

~~~
# apt install nmap

$ nmap -V

Nmap version 7.40 ( https://nmap.org )
Platform: x86_64-pc-linux-gnu
Compiled with: liblua-5.3.3 openssl-1.1.0c libpcre-8.39 libpcap-1.8.1 nmap-libdnet-1.12 ipv6
Compiled without:
Available nsock engines: epoll poll select
~~~

## Exemples d'utilisation

### Détecter rapidement les machines présentes sur un réseau

Pour lister/trouver les machines actives sur un réseau, on peut faire un scan par ping :
~~~
$ nmap -sP 192.0.2.0/24
$ nmap -6 -sP 2001:0DB8::/32
~~~

### Générer un rapport

Nmap est distribué une feuille de style xsl utilisable sur la sortie xml. Dans le paquetage Debian de Nmap, la feuille de style se trouve dans `/usr/share/nmap/nmap.xsl`. Par défaut, elle ne semble pas bien prise en compte, mais on peut notamment l'héberger nous même ou utiliser celle en ligne sur nmap.org et spécifier l'url avec l'option --stylesheet

~~~
# nmap -sS -oX /var/www/scans/.xml --stylesheet https://nmap.example.org/nmap.xsl 192.0.2.42
~~~

### Est-ce qu'un port UDP est ouvert ?

Vu que la plupart des services UDP ne répondent pas lors de la réception de paquets malformés, ce qui inclut la sonde `nmap` par défaut, il faut se baser sur la fonctionnalité [nmap-service-probes](https://github.com/jwilkins/nmap/blob/c1229d8332e436f0f883898b6d9a4d07fd2da42b/nmap-service-probes#L2882-L2884). Ça s'active avec l'option `-V` qui va essayer de découvrir la version du service UDP en utilisant diverses sondes, ce qui prend un certain temps.

Donc si avec l'option `-V`, le champ `VERSION` est rempli (ici avec `OpenVPN`) c'est qu'un flux UDP vers ce service est possible, si ce n'est pas le cas, on ne peut déduire si un flux et possible ou pas.

~~~(sh)
# nmap -sUV -p 1194 opened.example.com closed.example.com
Starting Nmap 7.93 ( https://nmap.org ) at 2024-06-05 10:34 CEST
Nmap scan report for opened.example.com (192.0.2.42)
Host is up (0.028s latency).

PORT     STATE SERVICE VERSION
1194/udp open  openvpn OpenVPN

Nmap scan report for closed.example.com (192.0.2.84)
Host is up (0.028s latency).

PORT     STATE         SERVICE VERSION
1194/udp open|filtered openvpn

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 2 IP addresses (2 hosts up) scanned in 104.29 seconds
~~~

### S'assurer qu'on peut obtenir l'heure d'un serveur NTP

~~~(sh)
# nmap -sU -p 123 --script ntp-info pool.ntp.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-06-07 15:26 CEST
Nmap scan report for pool.ntp.org (82.65.248.56)
Host is up (0.015s latency).
Other addresses for pool.ntp.org (not scanned): 91.194.60.128 5.39.80.51 95.81.173.74
rDNS record for 82.65.248.56: 82-65-248-56.subs.proxad.net

PORT    STATE SERVICE
123/udp open  ntp
| ntp-info:
|_  receive time stamp: 2024-06-07T13:26:56

Nmap done: 1 IP address (1 host up) scanned in 10.34 seconds
~~~
