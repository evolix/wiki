---
categories: system sysadmin systemd
title: Howto systemd-networkd
...

* Manpages : <https://manpages.debian.org/bookworm/systemd/systemd-networkd.8.en.html>

`systemd-networkd` est un composant de [systemd](/HowtoSystemd.md) dédié à la configuration du réseau. Il permet de configurer les interfaces physiques ainsi que de créé et configuré des interfaces vitruelles (bridge, vlan, bonding, tunnels…).

## Installation

Aucune action nécessaire, `systemd-networkd` (et les outils en cli comme `networkctl`) sont présent dans le packet systemd, déjà présent par défaut sur Debian.

```
# networkctl --version
systemd 252 (252.12-1~deb12u1)
+PAM +AUDIT +SELINUX +APPARMOR +IMA +SMACK +SECCOMP +GCRYPT -GNUTLS +OPENSSL +ACL +BLKID +CURL +ELFUTILS +FIDO2 +IDN2 -IDN +IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 -PWQUALITY +P11KIT +QRENCODE +TPM2 +BZIP2 +LZ4 +XZ +ZLIB +ZSTD -BPF_FRAMEWORK -XKBCOMMON +UTMP +SYSVINIT default-hierarchy=unified

# networkctl 
WARNING: systemd-networkd is not running, output will be incomplete.

IDX LINK      TYPE     OPERATIONAL SETUP    
  1 lo        loopback -           unmanaged
  2 enp0s31f6 ether    -           unmanaged
  3 wwan0     wwan     -           unmanaged
  4 wlp4s0    wlan     -           unmanaged

4 links listed.
```

## Configuration

Il existe un fichier de configuration pour des options "globales" (`/etc/systemd/networkd.conf`), mais la configuration des réseaux et des liens se fait dans des fichiers dans `/etc/systemd/network/`.

Il existe trois types de configuration : 

* Fichiers en `*.network` > [Configuration de réseaux](/HowtoSystemdNetworkd/ConfigurationReseau)
* Fichiers en `* .netdev` > [Configuration d'interfaces virtuelles (bridge, bond, ...)](/HowtoSystemdNetworkd/ConfigurationInterfacesVirtuelles)
* Fichiers en `*.link` > Configuration d'une interface à sa connexion (par udev) - En général, on n'en a pas besoin <https://manpages.debian.org/bookworm/udev/systemd.link.5.en.html>

### Exemples - IP statiques

Exemple de configuration. Une IP (v4 et v6) statique pour une interface réseau donnée, identifiée par son addresse MAC : 

```
# cat /etc/systemd/network/wan.network
[Match]
MACAddress=52:54:00:00:00:00

[Network]
DHCP=no
Address=192.0.2.33/24
Gateway=192.0.2.254
Address=2001:db8::33/48
Gateway=2001:db8::ffff
```

Résultat 

```
# neworkctl
# networkctl 
IDX LINK TYPE     OPERATIONAL SETUP     
  1 lo   loopback carrier     unmanaged
  2 ens3 ether    routable    configured

2 links listed.


# networkctl status 
●        State: routable                       
  Online state: online                         
       Address: 192.0.2.33 on ens3
                2001:db8::33 on ens3
                fe80::5054:ff:3615:3615 on ens3
       Gateway: 192.0.2.254 on ens3
                2001:db8::ffff on ens3

Aug 03 14:42:32 hostname systemd-networkd[304]: lo: Link UP
Aug 03 14:42:32 hostname systemd-networkd[304]: lo: Gained carrier
Aug 03 14:42:32 hostname systemd-networkd[304]: Enumeration completed
Aug 03 14:42:32 hostname systemd-networkd[304]: ens3: Configuring with /etc/systemd/network/wan.network.
Aug 03 14:42:32 hostname systemd[1]: Started systemd-networkd.service - Network Configuration.
Aug 03 14:42:32 hostname systemd-networkd[304]: ens3: Link UP
Aug 03 14:42:32 hostname systemd[1]: Starting systemd-networkd-wait-online.service - Wait for Network to be Configured...
Aug 03 14:42:33 hostname systemd-networkd[304]: ens3: Gained carrier
Aug 03 14:42:34 hostname systemd-networkd[304]: ens3: Gained IPv6LL
Aug 03 14:42:46 hostname systemd[1]: Finished systemd-networkd-wait-online.service - Wait for Network to be Configured.


# networkctl status ens3
● 2: ens3                                                                      
                     Link File: /usr/lib/systemd/network/99-default.link
                  Network File: /etc/systemd/network/wan.network
                         State: routable (configured)
                  Online state: online                                         
                          Type: ether
                          Path: pci-0000:00:03.0
                        Driver: virtio_net
                        Vendor: Red Hat, Inc.
                         Model: Virtio network device
             Alternative Names: enp0s3
              Hardware Address: 52:54:00:00:00:00
                           MTU: 1500 (min: 68, max: 65535)
                         QDisc: fq_codel
  IPv6 Address Generation Mode: eui64
      Number of Queues (Tx/Rx): 1/1
              Auto negotiation: no
                       Address: 192.0.2.33
                                2001:db8::33
                                fe80::5054:ff:3615:3615
                       Gateway: 192.0.2.254
                                2001:db8::ffff
             Activation Policy: up
           Required For Online: yes

Aug 03 14:41:32 hostname systemd-networkd[304]: ens3: Configuring with /etc/systemd/network/wan.network.
Aug 03 14:41:32 hostname systemd-networkd[304]: ens3: Link UP
Aug 03 14:41:33 hostname systemd-networkd[304]: ens3: Gained carrier
Aug 03 14:41:34 hostname systemd-networkd[304]: ens3: Gained IPv6LL
```


## Administration

## Plomberie

## FAQ