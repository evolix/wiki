---
categories: cloud haute-disponibilite
title: HowTo Pacemaker
...

* Documentation : <https://www.clusterlabs.org/pacemaker/doc/>

**Pacemaker** est un gestionnaire de ressources en cluster permettant de gérer la haute disponibilité de ressources tournants sur plusieurs serveurs.

Pacemaker utilise [Corosync](https://corosync.github.io/corosync/) pour gérer la communication et les décisions entre les serveurs du cluster.

## Configuration d'un nouveau cluster

### Installation

~~~
# apt install pacemaker pacemaker-cli-utils
# pacemakerd --version
Pacemaker 2.0.5
Written by Andrew Beekhof
# pcs --version
0.10.8
~~~

Il faut ensuite définir un mot de passe pour l'utilisateur `hacluster`.

~~~
# passwd hacluster
~~~

Finalement nettoyer toute trace d'un cluster (Debian démarre le service pacemakerd par défaut, ce qui empêche la mise en place sans cette étape).

~~~
# pcs cluster destroy
~~~

### Définition du cluster

> Il faut en plus que les machines puissent se connecter en root entre elles.

Depuis l'une des machines du cluster (n'importe laquelle), configuré la connexion entre les serveurs du cluster.

> Cette commande peut être exécutée en plusieurs fois si le mot de passe de `hacluster` est différent sur les différents hôtes.

~~~
host1# pcs host auth <host1> [addr=<ip_host1>] <host2> [addr=<ip_host2>] [...]
~~~

Puis définir le cluster :

~~~
host1# pcs cluster setup <nom_du_cluster> <host1> [addr=<ip_host1>] <host2> [addr=<ip_host2>] [...]
host1# pcs cluster auth
~~~

Et finalement démarré le cluster (et faire en sorte que les services démarrent automatiquement au démarrage de la machine) :

~~~
host1# pcs cluster start --all
host1# pcs cluster enable --all
~~~

## Administration

### Activer ou désactiver le Fencing

Le **fencing** est géré via la configuration du `stonith` (Shoot The Other Node In The Head), dans le cas où il est activé, si une majorité des nodes d'un cluster n'arrivent pas à contacter un autre node alors le cluster éteindra automatiquement ce dernier node (généralement électriquement) d'une manière configurée, afin de protéger contre le split-brain.

Il est fortement recommandé de garder le fencing actif sur un cluster en production.

~~~
pcs property set stonith-enabled=<true|false>
~~~

### Configuré le Fencing

~~~
# cib_path=</chemin/vers/cib/temporaire>
# pcs cluster cib "${cib_path:?}"
# pcs -f "${cib_path:?}" stonith create <nom_du_stonith> <materiel_utilisé_pour_le_stonith> <configuration>
# pcs -f "${cib_path:?}" property set stonith-enabled=true
# pcs cluster cib-push "${cib_path:?}"
~~~

Si le nom des nodes pour le cluster est différent des noms connus par le matériel utilisé, il faut définir le paramètre `pcmk_host_map` avec le format suivant : `<nom_du_node1_pour_pacemaker>:<nom_du_node1_pour_stonith>;<nom_du_node2_pour_pacemaker>:<nom_du_node2_pour_stonith>` (si le stonith utilise plusieurs noms/ports pour un node, ils sont à séparer par des virgules).

Pour obtenir la liste des matériels supportés, utilisé la commande suivante :

~~~
pcs stonith list
~~~

Pour connaître les options de configurations, utilisé la commande suivante :

~~~
pcs stonith describe <nom_du_matériel>
~~~

Il y a aussi des configurations communes documentées dans la page man `pacemaker-fenced(7)`.

### Définir la politique en cas de perte de quorum

Pour définir comment le cluster réagit en cas de perte du quorum, faire la commande suivante :

~~~
# pcs property set no-quorum-policy=<stop|freeze>
~~~

Valeurs :

* **stop**: Les services restant sont désactivés
* **freeze**: Rien n'est fait tant que le quorum n'est pas retrouvé

### Ajouter une ressource (service mis en cluster, ou état surveillé)

~~~
# pcs resource create <nom_de_ressource> <plugin_de_resource> <configuration> [clone|promotable]
~~~

> La configuration `clone` signifie que la ressource peut se trouver active sur plusieurs machines en même temps, cela créé une ressource "locale" `<nom_de_ressource>_clone` qui est à utiliser pour les restrictions. `promotable` signifie que la ressource a une gestion interne de primaire/secondaire.

Les plugins de ressources disponibles à la définition sont obtenables avec les commandes suivantes (le format dans la commande de création de ressource est `<standard>:<provider>:<agent>` :

~~~
# pcs resource standards
# pcs resource providers <standard>
# pcs resource agents <standard>:<provider>
~~~

Les configurations disponibles pour un plugin de ressource sont disponbiles avec la commande suivante :

~~~
# pcs resource describe <standard>:<provider>:<agent>
~~~

> Il est fortement recommandé d'utiliser les plugins du standard `ocf` si possible.

> Si un plugin gère le lancement d'un service système, il ne faut pas qu'il soit en `enable` dans systemd.

### Définition de restrictions entre resources

> Le score est un nombre ou `-INFINITY` ou `INFINITY` définissant le niveau d'importance de la restriction.

Pour définir une restriction de colocalisation de deux ressources (une ressource devant être sur un même serveur qu'un autre, ou le contraire) :

> Il est préférable de créer un groupe de ressource quand possible.

~~~
# pcs constraint colocation add <resource_contrainte> with <resource_contraignante> <score>
~~~

Pour définir une restriction de localisation d'une ressource par rapport à une condition :

~~~
# pcs constraint location <resource> rule score=<score> <condition>
~~~

Pour définir une restriction sur l'ordre de démarrage de ressources (`ressource1` doit être démarré avant `ressource2`) :

~~~
# pcs constraint order <ressource1> then <ressource2>
~~~

### Définition d'un groupe de ressources (étant donc contraint à être démarré sur le même node)

~~~
# cib_path=</chemin/cib/temporaire>
# group_name=<nom_du_groupe>
# pcs cluster cib "${cib_path:?}"
# pcs -f "${cib_path:?}" resource create --group "${group_name:?}" <...> # définition de la première ressource
# pcs -f "${cib_path:?}" resource create --group "${group_name:?}" <...> # définition de la seconde ressource
[...]
# pcs cluster cib-push "${cib_path:?}"
~~~
