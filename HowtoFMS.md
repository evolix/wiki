**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto FMS : Flash Media Server

Par défaut FMS écoute sur les ports TCP 1935, 80 et 8080.
Pour qu'il n'écoute que sur le port 1935.

Pour changer cela, changer dans le conf/fms.ini :

~~~
ADAPTOR.HOSTPORT = :1935
~~~

Ainsi que dans conf/_defaultRoot_/Adaptor.xml :

~~~
               <HttpProxy enable="false" maxbuf="16384">
                        <Host port="80">${HTTPPROXY.HOST}</Host>
                </HttpProxy>
~~~