La gamme ThinkPad est une gamme professionnelle d'ordinateurs portables, commercialisée depuis 2005 par Lenovo, antérieurement par IBM.

# X1

## Problème de charge de la batterie

Les `X1` rencontrent parfois des problèmes de charge de batterie : le niveau ne monte plus, voire baisse (lentement).

Débrancher et rebranche d'adaptateur secteur ne résout pas le problème.

La solution est de démonter le capot inférieur du portable. Il comporte 5 vis (cruciformes classiques), qui rentrent fichées sur le capot au dévissage (bien pratique, car cela évite de les perdre).

Attention, un clips central est situé au niveau du PAD, et peut laisser croire qu'on a oublié de retirer une vis. Il faut forcer légèrement dessus en introduisant un stylo par exemple pour faire levier dessus.

Il n'est pas nécessaire d'enlever la batterie (qui comporte plus de vis que le capot !).

Il suffit de débrancher/rebrancher le connecter de la batterie.

Il n'est pas facile à débrancher. Le côté le plus éloigné de la batterie nécessite d'être basculé vers le haut pour pouvoir le sortir de son logement.

Vérifier au redémarrage du PC que la batterie se charge bien de nouveau.
