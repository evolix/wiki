---
categories: monitoring
title: Howto Icinga
---

* Documentation : <https://docs.icinga.com/>

[Icinga](https://www.icinga.com/) est un logiciel libre de surveillance système et d'équipements réseaux. 
Au départ (en 2009), c'est un fork de [Nagios](https://www.nagios.com/) avec pour objectifs d'avoir une meilleure évolutivité et une interface web plus moderne. La version 1 d'Icinga a une configuration compatible avec Nagios, ce qui n'est pas le cas de la version 2 qui a été complètement ré-écrite.

Après avoir utilisé Nagios pendant plus de 10 ans, nous utilisons Icinga depuis plus de 5 ans.

_Note :_ Cette documentation concerne Icinga dans sa version 2, appelé `icinga2` dans certains cas.

## Installation

Depuis Debian 9 (Stretch), les différents composants/modules d'icinga (Icinga, l'interface web…) sont présents dans les dépôts officiels de Debian.

Il y a plusieurs paquets, pour chacun des composants : 

* icinga2 : Le daemon icinga2
* icingadb : Le daemon qui synchronise l'état d'icinga exposé dans une base Redis vers une base SQL (MariaBD ou PostgreSQL) pour l'interface web
* icingaweb2 : L'interface web de visualisation d'icinga
* icingadb-web : Le module de consultation de l'état du monitoring utilisant icingadb comme source.

Il est possible d'utiliser `icinga2` seul pour ces capacités de monitoring.

Pour bénéficier de l'inteface web, en plus des paquets `icingadb`, `icingaweb2` et `icingadb-web`, il faut aussi avoir les dépendances/services suivants :

* Une base [Redis](HowtoRedis), 
* Une base de données SQL : [MariaDB/MySQL](HowtoMySQL) ou [PostgreSQL](HowtoPostgreSQL)
* Un service web de votre choix avec PHP (nous recommendons [Apache](HowtoApache))

~~~
# apt install icinga2 icingaweb2 icingadb-web icingadb icingacli

# /usr/sbin/icinga2 --version
icinga2 - The Icinga 2 network monitoring daemon (version: r2.13.6-1)

Copyright (c) 2012-2024 Icinga GmbH (https://icinga.com/)
License GPLv2+: GNU GPL version 2 or later <https://gnu.org/licenses/gpl2.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

System information:
  Platform: Debian GNU/Linux
  Platform version: 12 (bookworm)
  Kernel: Linux
  Kernel version: 6.1.0-26-cloud-amd64
  Architecture: x86_64

Build information:
  Compiler: GNU 12.2.0
  Build host: x86-conova-01
  OpenSSL version: OpenSSL 3.0.14 4 Jun 2024

Application information:

General paths:
  Config directory: /etc/icinga2
  Data directory: /var/lib/icinga2
  Log directory: /var/log/icinga2
  Cache directory: /var/cache/icinga2
  Spool directory: /var/spool/icinga2
  Run directory: /run/icinga2

Old paths (deprecated):
  Installation root: /usr
  Sysconf directory: /etc
  Run directory (base): /run
  Local state directory: /var

Internal paths:
  Package data directory: /usr/share/icinga2
  State path: /var/lib/icinga2/icinga2.state
  Modified attributes path: /var/lib/icinga2/modified-attributes.conf
  Objects path: /var/cache/icinga2/icinga2.debug
  Vars path: /var/cache/icinga2/icinga2.vars
  PID path: /run/icinga2/icinga2.pid
~~~

À savoir, par défaut après l'installation, icinga2 démarre et monitore par défaut l'hôte sur lequel il a été installé.
Il est donc possible que vous receviez déjà des notifications par mail sur le compte de root !

> *Remarque* : Il existe aussi un dépôt par les développeurs d'icinga si on souhaite avoir la toute dernière version. 
Les détails de configuration sur : <https://packages.icinga.com/debian/>

## Configuration

### Icinga2

La configuration du daemon icinga2 se situe dans le dossier `/etc/icinga2`.

On y retrouve la structure de dossiers suivante :

~~~
/etc/icinga2
├── conf.d              // <- Ensemble des fichiers de configuration (généralement ignoré en mode multi-noeud/cluster)
│   └── *.conf
├── constants.conf      // <- Fichier de configuration de constantes (comme le `NodeName` ou `ZoneName`)
├── features-available  // <- Configuration des fonctionnalité disponibles 
│   └── *.conf
├── features-enabled    // <- Configuration des fonctionnalité qui sont activées
│   └── *.conf
├── icinga2.conf        // <- Fichier de configuration initial
├── pki                 // <- Fichiers de la pki
├── scripts
│   ├── mail-host-notification.sh
│   └── mail-service-notification.sh
├── zones.conf          // <- Fichier de déclaration des zones (cas d'une configuration multi-noeud/cluster)
└── zones.d             // <- Dossier contenant les configuration des zones déclarées
    └── README
~~~

Dans icinga2, il y a la notion de *feature* qu'on peut traduire par *fonctionnalité*. 
Il s'agit de la même mécanique qu'on peut observer avec [Apache2](HowtoApache) et les dossiers *mods-enabled* et *mods-available*, qui permet d'activer et de configurer des fonctionnalités ou modules du daemon icinga2.

Voici une liste de fonctionnalités habituellement activées :

* checker : C'est le module qui effectue les checks des services 
* notification : C'est le module qui envoie les notitifcations suite à des évènements
* api : Permet la communication inter-noeuds dans le cas d'une configuration distribuée
* icingadb : Permet de configurer la base Redis pour icingadb
* ido-mysql (déprécié) : Permet de configurer le module IDO qui exporte les informations de monitoring (état, historique) dans une base MySQL

Dans la configuration d'icinga, les éléments de configuration possible sont des objets. Il existe divers types d'objets avec des relations spécifiques entre eux.
Ainsi, une machine sera décrite avec un objet **Host**, et les services surveillés de cette machine seront des objets de type **Service** qui seront rattachés à l'objet **Host**.

Voir la [description détaillée de tous les objets](http://docs.icinga.org/icinga2/latest/doc/module/icinga2/chapter/object-types).

Le nom des fichiers contenant les objets n'est pas important. Une bonne pratique est de rassembler les objets de même types dans un même fichier (exemple :`/etc/icinga2/conf.d/hosts.conf` et `/etc/icinga2/conf.d/services.conf` ) ou encore de rassembler les objets ayant des relations logiques ensemble (exemple :`/etc/icinga2/conf.d/dc_mrs1.conf` et `/etc/icinga2/conf.d/dc_pa2.conf`)

#### Architecture multi-nœuds & Zones

Icnga peut fonctionner seul pour faire du monitoring. 
Mais il est aussi possible de l'utiliser en cluster avec d'autres nœuds.

Il y a plusieurs utilités à cela : 

* Fonctionnement en haute-disponibilité
* Répartition de charge
* Cloisonnement / Gestion des flux réseaux

Dans cette situation, les nœuds icinga peuvent avoir différents roles dans une architecture donnée : 

* nœud **master** : Serveur central
* nœud **satellite** : Serveur dans une zone différente rattaché au nœud master et ayant des nœud agent comme enfants.
* nœud **agent/client** : Serveur chargé de lancer des commandes locales (semblable à un serveur *nrpe*)

La configuration est alors découpée en zones. Les nœuds icinga font partie d'une zone, et les zones sont configuées sous forme de hierarchie.
Plusieurs nœud peuvent faire partie d'une même zone, ils fonctionneront en mode répartition de charge / haute disponibilité.

La zone principale avec le(s) nœud(s) master et des zones secondaires pour les nœuds satellite. 
Cette notion de zone permet par exemple d'avoir des nœuds satellite répartis dans plusieurs pays. 

#### Amorce d'une infrastructure multi-nœuds - Configuration d'un nœud icinga master

Si on souhaite mettre en place une infrastructure multi-nœuds, il faut commencer par un nœud master.

Le plus simple est d'utiliser l'assistant de configuration :
Il se chargera d'activer l'API et de créer une autorité de certification x509 (PKI) pour l'authentification avec les autres nœuds.

~~~
# icinga2 node wizard
Welcome to the Icinga 2 Setup Wizard!
We'll guide you through all required configuration details.


Please specify if this is a satellite setup ('n' installs a master setup) [Y/n]: n
Starting the Master setup routine...
Please specify the common name (CN) [icinga2-master]:  
Checking for existing certificates for common name 'icinga2-master'...
Certificates not yet generated. Running 'api setup' now.
information/cli: Generating new CA.
[…]
Done.

Now restart your Icinga 2 daemon to finish the installation!

# systemctl restart icinga2
~~~

### Configuration de l'interface web

L'interface web permet de visualiser l'état du monitoring, consulter l'historique et déclencher des commandes.

Initialement, celle-ci fonctionnait de concert avec la *feature* “IDO” (*Icinga Data Output*) d'Icinga. 
Cette fonctionnalité poussait l'intégralité des informations (configuration, état actuel, historique…) dans une base MySQL ou PostgreSQL pour l'affichage par l'interface web. 
Bien que les modules IDO soient toujours présents, ils sont considérés comme dépréciées et seront retirés dans une future version.

Pour une nouvelle installation, il est donc préférable de configurer icingadb à place du module ido-mysql/ido-postgresql.

> *Remarque* : `icingadb` est packagé dans Debian depuis la version 12 (Bookworm) !

Pour une ancienne installation, il est conseillé d'envisager la migration. Si l'on souhaite conserver les informations d'historique, il existe
[une procédure dans la documentation d'icingadb](https://icinga.com/docs/icinga-db/latest/doc/06-Migration/)


icingadb peut être décomposée en 3 parties :

* La *feature*/fonctionnalité *icingadb* pour le daemon icinga - Elle pousse dans une base Redis toutes les informations de configuration, d'état et évènements
* Le daemon icingadb (paquet `icingadb`) : Synchronise dans la base SQL tous les les informations conservées dans le temps et la configuration
* Le module web icingadb, qui permet la consultation des informations en récupérant les informations depuis Redis et la base SQL


Après mise en place de [Redis](HowtoRedis), on peut configurer et activer la *feature* icingadb dans icinga2

~~~
# vim /etc/icinga2/features-available/icingadb.conf
object IcingaDB "icingadb" {
  host     = "127.0.0.1"
  port     = 6379
  password = "PASSWORD"
}

# icinga2 feature enable icingadb
# systemctl reload icinga2
~~~

Après rechargement de la configuration d'icinga, celui-ci va commencer à pousser les informations de monitoring dans Redis.
On va en suite préparer deux bases MySQL. Une servira pour icingadb, et l'autre pour l'interface web elle même (comptes utilisateurs, permissions...)

~~~
## On crée une base icingaweb, ainsi qu'un compte icingaweb ayant les droits d'accès
# mysqladmin create icingaweb
# mysql
> CREATE USER 'icingaweb'@'localhost' IDENTIFIED BY 'PASSWORD';
> GRANT ALL ON icingaweb.* TO 'icingaweb'@'localhost';
> GRANT SELECT ON `icingadb`.* TO `icingaweb`@`localhost`;

## On crée une base icingadb, ainsi qu'un compte icingadb ayant les droits d'accès
# mysqladmin create icingadb
# mysql
> CREATE USER 'icingadb'@'localhost' IDENTIFIED BY 'PASSWORD';
> GRANT ALL ON icingadb.* TO 'icingadb'@'localhost';

## On charge le schéma de base de donnée nécessaire pour icingadb
# mysql -o icingadb < /usr/share/icingadb/schema/mysql/schema.sql
~~~

L'étape suivante est de configurer le daemon `icingadb` dans `/etc/icingadb/config.yml`

~~~
# vim /etc/icingadb/config.yml
# This is the configuration file for Icinga DB.

# Connection configuration for the database to which Icinga DB synchronizes monitoring data.
# This is also the database used in Icinga DB Web to view and work with the data.
# In high availability setups, all Icinga DB instances must write to the same database.
database:
  # Database type. Either 'mysql' for MySQL or 'pgsql' for PostgreSQL.
  # Defaults to 'mysql'.
  type: mysql

  # Database host or absolute Unix socket path.
  host: localhost

  # Database port. By default, the MySQL or PostgreSQL port, depending on the database type.
  port: 3306

  # Database name.
  database: icingadb

  # Database user.
  user: icingadb

  # Database password.
  password: PASSWORD

# Connection configuration for the Redis server where Icinga 2 writes its configuration, state and history items.
# This is the same connection as configured in the 'icingadb' feature of the corresponding Icinga 2 node.
# High availability setups require a dedicated Redis server per Icinga 2 node and
# therefore a dedicated Icinga DB instance that connects to it.
redis:
  # Redis host or absolute Unix socket path.
  host: localhost

  # Redis port.
  # Defaults to '6380' since the Redis server provided by the 'icingadb-redis' package listens on that port.
  port: 6379

  # Redis password.
  password: PASSWORD
~~~

On peut ensuite activer et démarrer icingadb

~~~
# systemctl enable --now icingadb
# systemctl status icingadb
● icingadb.service - Icinga DB
     Loaded: loaded (/lib/systemd/system/icingadb.service; enabled; preset: enabled)
     Active: active (running) since Thu 2024-11-07 17:15:51 CET; 4 days ago
       Docs: https://icinga.com/docs/icingadb/latest/
   Main PID: 2116 (icingadb)
      Tasks: 12 (limit: 9508)
     Memory: 146.5M
        CPU: 1h 44min 49ms
     CGroup: /system.slice/icingadb.service
             └─2116 /usr/sbin/icingadb --config /etc/icingadb/config.yml

~~~

> *Remarque* : Si vous avez dans le log du daemon icingadb le message suivant : `Waiting for Icinga 2 to write into Redis, please make sure you have started Icinga 2 and the Icinga DB feature is enabled`
> C'est que vous avez peut-être : 
>
> * Oublié de recharger icinga2
> * Pas activé la *feature* icingadb
> * Que vous n'utilisez pas la même pas Redis des deux côtés !

Si tout s'est bien passé, dans les logs du daemon icingadb, on observera les messages suivants : 

~~~
Nov 07 15:03:54 HOSTNAME icingadb[1338925]: Starting history sync
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: heartbeat: Received Icinga heartbeat
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: Taking over
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: Starting config sync
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: Starting overdue sync
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: Starting initial state sync
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 246 items of type checkcommand
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 3 items of type zone
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: Starting state runtime updates sync
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 1276 items of type checkcommand customvar
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Finished initial state sync in 37.863918ms
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 5 items of type checkcommand envvar
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 1 items of type endpoint
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 489 items of type customvar flat
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 472 items of type customvar
Nov 07 15:03:55 HOSTNAME icingadb[1338925]: config-sync: Inserting 3152 items of type checkcommand argument
Nov 07 15:03:56 HOSTNAME icingadb[1338925]: Starting config runtime updates sync
Nov 07 15:03:56 HOSTNAME icingadb[1338925]: Starting history retention
Nov 07 15:03:56 HOSTNAME icingadb[1338925]: config-sync: Finished config sync in 510.793117ms
~~~

Pour permettre à l'interface web de piloter Icinga (Forcer l'exécution d'un check, acquitter des alertes…) il activer l'API (si ce n'est pas déjà le cas) et créer un accès.
On va créer un accès sans restrictions, donc veillez à avoir un mot de passe fort !

~~~
# icinga2 feature enable api
# cat /etc/icinga2/conf.d/api_users.conf 
object ApiUser "USER" {
  password = "PASSWORD"

  permissions = [ "*" ]
}
~~~

> *Remarque* : Il semble que le module icingadb de l'interface web supporte seulement l'API d'icinga pour piloter celui-ci. 
> L'ancien module *monitoring* proposait aussi un mode "command" qui permettait de se passer de l'API.


On peut terminer par la configuration de l'interface web.

Si on utilise Apache, le paquet `icingaweb2` arrive directement avec de la configuration chargée par défaut.
Ainsi le chemin `/icingaweb` depuis n'importe quel vhost renvoie vers l'interface web d'Icinga.

>*Note* :Si on souhaite, changer l'URL, ou gérer de manière plus fine depuis un vhost, on peut alors désactiver cette configuration `a2disconf icingaweb2`
> Et s'inspirer de son contenu (depuis `/etc/apache2/conf-available/icingaweb2.conf`) pour intégrer icingaweb au vhost de son choix.

En se rendant sur l'interface web directement, il sera proposé d'utiliser l'assistant de configuration
Mais d'abord, il faut générer un jeton (ou token) avec la commande `icingacli setup token create` pour être autorisé a faire le processus de configuraiton.

La prochaine étape de l'assistant demande quelles fonctions activer : il faut au moins activer le module **icingadb** (sinon l'interface web perd son intérêt)

L'assistant lance ensuite une série de tests. Il est important de s'assurer que tous les tests sont OK (il est normal que les tests PostgreSQL s'affichent en jaune, car on utilise MySQL).

Pour le paramétrage de l'authentification à l'interface web, on peut utiliser la base de données crée à l'occasion ou avoir d'autre backend (LDAP, etc.).

L'assistant va ensuite avoir besoin des paramètres d'accès à la base de données SQL et au redis utilisés par icingadb.
On pourra réutiliser les paramètres configurés dans `/etc/icingadb/config.yml`

En fin de procédure, l'interface web est désormais accessible via <http://monitoring.example.org/icingaweb2/>

### Configuration de l'interface web avec le module IDO (déprécié)

Ce module est déprécié, mais voici quelques pointeurs pour la configuration.

On suivra la même procédure que pour la configuration de l'interface web avec quelques différences.

Le module à activer est le module **monitoring** qui est l'interface de visualisation pour IDO
Pour les accès à la base SQL utilisée par la fonctionalités IDO, on peut ré-utiliser les identifiants utilisés dans `/etc/icinga2/features-available/ido-mysql.conf` (ou `/etc/icinga2/features-available/ido-postgreql.conf`)


### Configuration client/agent

Pour surveiller un équipement, il faut installer Icinga (nœud agent/client) ou s'appuyer sur d'autres protocoles (NRPE, SSH, SNMP).

#### Surveiller un équipement avec Icinga (nœud agent/client)

Quand on installe Icinga sur un équipement, le daemon est lancé et se met à surveiller l'hôte local.
On va configurer de manière minimale pour juste le rattacher au nœud master et c'est le master qui va décider comment utiliser la machine.

On utilise l'assistant de configuration (_note_: les autres commandes `icinga2 node <paramètre>` sont dépréciées) :

~~~
# icinga2 node wizard
Welcome to the Icinga 2 Setup Wizard!
We'll guide you through all required configuration details.

Please specify if this is a satellite setup ('n' installs a master setup) [Y/n]: y
Starting the Node setup routine...
Please specifiy the common name (CN) [icinga2-client1]:
Please specify the master endpoint(s) this node should connect to:
Master Common Name (CN from your master setup): icinga2-master
Do you want to establish a connection to the master from this node? [Y/n]: y
Please fill out the master connection information:
Master endpoint host (Your master's IP address or FQDN): 192.0.2.1
Master endpoint port [5665]:
Add more master endpoints? [y/N]: n
Please specify the master connection for CSR auto-signing (defaults to master endpoint host):
Host [192.0.2.1]:
Port [5665]:
information/base: Writing private key to '/etc/icinga2/pki/icinga2-client1.key'.
information/base: Writing X509 certificate to '/etc/icinga2/pki/icinga2-client1.crt'.
information/cli: Fetching public certificate from master (192.0.2.1, 5665):

Certificate information:

 Subject:     CN = icinga2-master
 Issuer:      CN = Icinga CA
 Valid From:  Aug 15 21:03:54 2016 GMT
 Valid Until: Aug 12 21:03:54 2031 GMT
 Fingerprint: 13 37 FF 42 00 13 00 DE DB EE F0 00 16 45 2A 14 F5 00 CA FE

Is this information correct? [y/N]: y
information/cli: Received trusted master certificate.

Please specify the request ticket generated on your Icinga 2 master.
 (Hint: # icinga2 pki ticket --cn 'icinga2-sclient1'): ed347c9f5c4c1a08f811bf2032944d01c48979a
information/cli: Requesting certificate with ticket 'ed347c9f5c4c1a08f811bf2032944d01c48979a'.

information/cli: Writing signed certificate to file '/etc/icinga2/pki/icinga2-client1.crt'.
information/cli: Writing CA certificate to file '/etc/icinga2/pki/ca.crt'.
Please specify the API bind host/port (optional):
Bind Host []:
Bind Port []:
Accept config from master? [y/N]: y
Accept commands from master? [y/N]: y
~~~

> *Note* : Pour contrôler l'empreinte du certificat de la machine à laquelle on se connecte, on peut utiliser la commande `openssl x509 -noout -in /etc/icinga2/pki/icinga2-master.crt -fingerprint -sha1`

À noter qu'à l'étape où l'assistant demande le ticket généré sur le master, il faut exécuter la commande suivante sur le master et copier l'output dans l'assistant :

~~~
# icinga2 pki ticket --cn '<nom du client>'
~~~

Après l'exécution de l'assistant, l'instance Icinga est réglée comme appartenant à sa propre *zone*. Cette *zone* est fille du master renseigner lors de l'assistant. Si client reconnaît maintenant le master, il est nécessaire de configurer le master pour qu'il reconnaisse le client. On va donc modifier le fichier *zones.conf* sur le master y ajouter la *zone* et le *endpoint* du nouveau client :

~~~
[…]

object Endpoint "icinga2-client1" {
  host = "192.0.2.10"
}

object Zone "icinga2-client1" {
  endpoints = [ "icinga2-client1" ]
  parent = "master"
}
~~~

Et voilà, en théorie le master et le client peuvent se parler ! On peut maintenant configurer les checks via le master.

> *Note* : nous n'avons pas encore configuré de test sur le client, il est normal qu'il n'apparaisse pas sur l'interface web.


#### mode top down command endpoint (exécution de commande)

<https://docs.icinga.com/icinga2/latest/doc/module/icinga2/chapter/distributed-monitoring#distributed-monitoring-top-down-command-endpoint>

C'est la méthode la plus simple, similaire à NRPE : le nœud master souhaitant effectuer un check, il lance une commande au travers de l'agent local icinga2 installé. Le seul pré-requis est que les objets de type **CheckCommand** définissant les commandes à exécuter doivent être présent sur le master **et** localement (i.e. là où la commande est exécutée).



#### mode top down config sync (checks envoyés vers le master)

<https://docs.icinga.com/icinga2/latest/doc/module/icinga2/chapter/distributed-monitoring#distributed-monitoring-top-down-config-sync>

La configuration des machines locales est faite sur le serveur central. Lorsque l'on redémarre le daemon, le serveur central s'assure que les machines locales ont la bonne configuration. Cette méthode permet de centraliser l'ensemble de notre configuration.



#### Cas A - Client avec configuration centrale sur le master

Dans ce premier cas, les fichiers de configuration sont présents et modifiés sur le master, qui les synchronise sur le(s) client(s).
Nous allons donc ajouter nos tests sur le master dans *zones.d*, le dossier prévu à cet effet. On commence par y créer un nouveau dossier correspondant à la zone (donc le client appartenant à celle-ci) que l'on souhaite configurer :

~~~
# mkdir /etc/icinga2/zones.d/icinga2-client1
~~~

Dans ce dossier, on crée un fichier nommé *hosts.conf* qui va nous servir à définir un nouvel objet de type *Host*. Cet objet va nous permettre par la suite de lier d'autres types d'objets à notre nouveau client :

~~~
object Host "icinga2-client1" {
  import "generic-host"

  check_command = "hostalive"
  address = "192.0.2.10"
}
~~~

On peut maintenant ajouter de nouveaux tests dans *services.conf*, des objets de type *Service* :

~~~
object Service "disk" {
  import "generic-service"

  host_name = "icinga2-sclient1"
  check_command = "disk"
}

object Service "ssh" {
  import "generic-service"

  host_name = "icinga2-sclient1"
  check_command = "ssh"
}
~~~

Pour que la synchronisation se fasse entre le master et le client, on redémarre Icinga sur le master :

~~~
# systemctl reload icinga2
~~~

> *Note* : Si, pour une raison ou une autre, la configuration venait à être invalide lors du reload, celui-ci va être avorté. Mais Icinga va continuer de fonctionner avec l'ancienne configuration valide chargée avant. En effet, il contrôle la configuration avant de demander au daemon de s'arrêter. Pour contrôler la syntaxe de la configuration, on peut d'ailleurs utiliser la commande `icinga2 daemon --validate`

Si tout s'est passé correctement, vous devriez voir apparaître une nouvelle machine dans l'interface web, accompagné de deux nouveaux services : *disk* et *ssh*.

À noter que cette méthode de configuration est assez fastidieuse étant donné que l'on doit déclarer chaque services pour chaque machines. Ainsi, on tendra à privilégier l'utilisation de gabarits que l'on va ensuite appliquer sur les machines en fonctions de règles. Ces règles peuvent être basées sur :

* Des conventions de nommage des machines (i.e. foo-mail -> correspond à \*-mail -> Application de services propres à un serveur mail (SMTP, IMAP, mailq…))
* L'appartenance à des groupes
* La présence d'attributs personnalisés sur l'hôte

Ainsi, à place de définir 1000 services ssh pour 1000 machine linux, on va définir une fois le service ssh et l'appliquer aux 1000 machines. Dans l'exemple suivant, on va ainsi appliquer le service ssh aux machines dont on connait l'adresse et dont l'attribut vars.os a pour valeur "linux".

~~~
apply Service "ssh" {
  import "generic-service"

  check_command = "ssh"
  assign where host.address && host.vars.os == "Linux"
}
~~~

#### Cas B - Configuration en pont d'éxécution de commandes

Dans ce cas, toute la configuration va rester sur le nœud master (et donc dans le dossier `/etc/icinga2/zone.d/master/`). C'est le master qui va planifier l'exécution des checks.

Ainsi, on peut définir dans un fichier `icinga2-client2.conf` :

~~~
object Host "icinga2-client2" {
  import "generic-host"

  check_command = "hostalive"
  address = "192.0.2.20"
}

object Service "disk" {
  import "generic-service"

  command_endpoint = "icinga2-client2"
  host_name = "icinga2-sclient2"
  check_command = "disk"
}

object Service "ssh" {
  import "generic-service"

  command_endpoint = "icinga2-client2"
  host_name = "icinga2-sclient2"
  check_command = "ssh"
}
~~~

La définition de l'hôte et des services est quasi identique au cas précédent. Cependant, il faut ajouter l'attribut **command_endpoint** aux objets de type *Service* pour definir l'*Endpoint* qui doit être commandé pour exécuter la commande.


#### Surveiller un équipement sans Icinga (NRPE, SSH, etc.)

Comme annoncé plus haut, il est aussi possible de surveiller une machine qui n'aurait pas Icinga par d'autres moyens comme NRPE ou SSH. La commande de l'exécution des checks devra alors être prise en charge par un nœud du cluster Icinga. Idéalement, un *master* ou un *satellite*. Ainsi, la configuration proposée doit être entreposée dans le dossier de la zone qui aura la charge de la surveillance de la machine.

~~~
object Host "no-icinga2-host" {
  import "generic-host"

  check_command = "hostalive"
  address = "192.0.2.10"
}
~~~

##### Utilisation de NRPE

Lors d'une migration de *Nagios* vers *Icinga*, il peut être intéressant de supporter tous les checks fait par NRPE pour fluidifier la transition vers la nouvelle plateforme.

Pour cela, il faut d'abord récupérer le client **NRPE** :

~~~
# apt install nagios-nrpe-plugin
~~~

En suite, il suffit juste de créer les services dans la configuration du master pour paramétrer les checks voulu. L'exemple suivant crée un service *swap* pour la machine *no-icinga2-host* décrite plus haut qui exécutera la commande *check_swap* par NRPE.

~~~
object Host "no-icinga2-host" {
  import "generic-host"

  check_command = "hostalive"
  address = "192.0.2.30"
}

object Service "swap" {
  import "generic-service"

  host_name = "no-icinga2-host"

  check_command = "nrpe"
  vars.nrpe_command = "check_swap"
}
~~~

##### Utilisation de SSH

Dans certaines situations, l'utilisation de NRPE ou Icinga n'est pas possible (restrictions firewall, NAT…). Dans ces cas-là, on peut aussi exécuter des commandes via une connexion SSH.

Quelques pré-requis avant :

* L'utilisateur du daemon icinga2 doit avoir une clé ssh pour s'authentifier. Celle-ci doit être autorisée par l'hôte distant.
* Le daemon doit avoir l'empreinte du serveur ssh dans son known_hosts. (Sinon, il refusera de se connecter)
* Les scripts/plugins/commandes à exécuter doivent être présents sur la machine et exécutables par l'utilisateur utilisé par icinga2

Avec ces pré-requis, il suffit alors de définir une commande de check important l'objet *by_ssh* fournit avec icinga2. Ensuite, il suffit d'utiliser cette commande lors de la définition des services

~~~
object CheckCommand "by_ssh_swap" {
  import "by_ssh"

  vars.by_ssh_command = "/usr/lib/nagios/plugins/check_swap -w $by_ssh_swap_warn$ -c $by_ssh_swap_crit$"
  vars.by_ssh_swap_warn = "75%"
  vars.by_ssh_swap_crit = "50%"
}

object Service "swap" {
  import "generic-service"

  host_name = "no-icinga2-host"
  check_command = "by_ssh_swap"
  vars.by_ssh_logname = "icinga"
}
~~~

> *Cool fact* : on peut même faire du NRPE par SSH (dans le cas d'une machine en NAT ou avec un firewall assez restrictif). Exemple :

~~~
object CheckCommand "nrpe_via_ssh" {
  import "by_ssh"

  vars.by_ssh_command = "/usr/lib/nagios/plugins/check_nrpe -H $host.vars.internalIP$ $service.vars.nrpe_command$"
  vars.by_ssh_logname = "monitoring"

}

apply Service "ssh-nrpe-load" {
  import "generic-service"

  check_command = "nrpe_via_ssh"
  vars.nrpe_command = "-c check_load"
  vars.notification_period = "worktime"

  assign where host.vars.service_base && host.check_command == "nrpe_via_ssh"
}
~~~


## Configuration avancée

### Zone globale

Une bonne pratique est de créer une zone globale. Elle va ainsi permettre de pousser des fichiers de configuration sur tous les nœuds icinga2 du cluster. C'est pratique pour déployer des éléments de configuration générique et/ou globaux. Ainsi, si on appelle notre zone globale *global-templates*, il suffit de créer un dossier `/etc/icinga2/zone.d/global-templates` et d'ajouter le bloc suivant au fichier `/etc/icinga2/zone.conf` pour **chaque** installations d'icinga2 :

~~~
object Zone "global-templates" {
  global = true
}
~~~

### Envoi de notifications

Contrôler le bon fonctionnement de services, c'est bien. Alerter en cas de défaillance c'est mieux !

On peut définir un objet notification qui sera appliqué a nos services pour envoyer des mails. Il utilisera le script d'envoi d'emails fournit avec icinga2

~~~
apply Notification "mail" to Service {
  import "mail-service-notification"

  users = ["foo"]
  states = [ Critical, Unknown ]
  period = "9to5"

  assign where true
}
~~~

L'objet **Notification** 'mail' est définit pour envoyer une notification par courriel à l'utilisateur 'foo' lorsqu'un service est en état *Critical* ou *Unknown* sur la période *9to5* fournit dans la configuration par défaut.

~~~
object User "foo" {
  import "generic-user"

  display_name = "Foo"
  email = "foo@example.net"
}
~~~

### Timeperiods

On peut écrire de nouveaux objets **Timeperiod** pour décrire par exemple les heures ouvrées et envoyer des notifications uniquement durant cette période. 
Dans cet exemple, c'est défini du lundi au vendredi, de 9h à 12h et de 14h à 18h.

~~~
object TimePeriod "wortime" {
  import "legacy-timeperiod"

  display_name = "Heures Ouvrées"
  ranges = {
    "monday"    = "09:00-12:00,14:00-18:00"
    "tuesday"   = "09:00-12:00,14:00-18:00"
    "wednesday" = "09:00-12:00,14:00-18:00"
    "thursday"  = "09:00-12:00,14:00-18:00"
    "friday"    = "09:00-12:00,14:00-18:00"
  }
}
~~~

Il est aussi possible, par jeu d'inclusion/exclusion, de construire des éléments plus complexes.

Exemple pour avoir un objet **Timeperiod** `no-worktime` pour décrire les heures non-ouvrées, en prenant en compte les jours fériés en France : 

~~~
object TimePeriod "no-worktime" {
  import "everytime"
  excludes = [ "worktime" ]
  includes = [ "holidays-fr" ]
}

object TimePeriod "everytime" {
  import "legacy-timeperiod"
  display_name = "24/7"
  ranges = {
    "monday"    = "00:00-24:00"
    "tuesday"   = "00:00-24:00"
    "wednesday" = "00:00-24:00"
    "thursday"  = "00:00-24:00"
    "friday"    = "00:00-24:00"
    "saturday"  = "00:00-24:00"
    "sunday"    = "00:00-24:00"
  }
}

object TimePeriod "holidays-fr" {
  import "legacy-timeperiod"

  display_name = "French holidays"

  ranges = {
    "january 1" = "00:00-24:00"                 // Jour de l'an
    "may 1" = "00:00-24:00"                     // Fête du travail
    "may 8" = "00:00-24:00"                     // Fête de la victoire 1945
    "july 14" = "00:00-24:00"                   // Fête nationale
    "august 15" = "00:00-24:00"                 // Assomption
    "november 1" = "00:00-24:00"                // Toussaint
    "november 11" = "00:00-24:00"               // Armistice 1918
    "december 25" = "00:00-24:00"               // Noël

    // Specifique 2025
    "april 21"  = "00:00-24:00" // Lundi de Paques
    "may 29" = "00:00-24:00"    // Jeudi de l'Ascension
    "june 9"  = "00:00-24:00"   // Lundi de Pentecote

  }

}
~~~

### Contrôle de santé du cluster

Une autre bonne pratique est de rajouter des checks pour s'assurer du bon fonctionnement de notre cluster. Il existe deux commandes utilisables qui sont complémentaires :

* cluster : Elle s'assure que tout les *endpoints* de la zone soient actifs et que les zones directement connectés fonctionnent correctement
* cluster-zone : Elle s'assure que la zone donnée en paramètres est bien connecté au cluster

Idéalement, il faut donc utiliser **cluster** pour des zones de plus d'une machine, et **cluster-zone** pour s'assurer que chaque zone filles fonctionnent bien. Dans notre cas on va utiliser des checks *cluster-health* depuis le master pour s'assurer que nos deux endpoints *icinga2-client1* et *icinga2-client2* soient bien connectés.

Exemple avec *cluster-zone* :

~~~
# cat /etc/icinga2/zone.d/icinga2-master/cluster.conf

apply Service "cluster-health" {
  check_command = "cluster-zone"

  display_name = "cluster-health-" + host.name
  vars.cluster_zone = host.name

  assign where host.vars.client_endpoint
}
~~~

### Haute disponibilité

Dans certaines situations, on va vouloir mettre plusieurs instances Icinga dans une même zone à fin de les faire travailler ensemble et ainsi répartir la charge et avoir une tolérance de panne. Les modules d'Icinga sont capables de gérer la répartition entre chaque membre actifs d'une même zone.

Modules avec des fonctions de haute disponibilité :

* checker : Il va répartir l'exécution des checks entre chaque machine de la zone
* notification : De même, il peut répartir l'envoi des notifications entre chaque machine. C'est aussi désactivable avec le paramètre *enable_ha* - A ce moment-là chaque nœuds avec la fonctionnalité de notification active enverra une notification
* ido-{mysql,postgresql} : Là, les nœuds de la zone vont s'accorder pour qu'une seule instance écrive dans la base de données MySQL. Si l'instance en charge d'écrire est down, une autre prendra la relève. Désactivable avec le paramètre *enable_ha*

Pour ajouter un nouveau nœud une *zone* master, il suffit de l'installer normalement comme expliqué plus haut, puis d'ajuster le fichier `zone.conf` pour faire en sorte que les deux nœuds appartiennent bien à la même zone.


### Gestion de la PKI - Génération manuelle des certificats

Dans certains cas, on peut être amené à générer manuellement le certificat TLS pour l'authentification d'un nouveau nœud.

La commande suivante sert à générer une CSR, pour une machine "icinga2-client2". Elle peut être directement lancée sur la machine désignée ou depuis le master du cluster (si la machine monitorée n'a pas encore été provisionnée par exemple).

~~~
# icinga2 pki new-cert --cn icinga2-client2 \
  --key icinga2-client2.key \
  --csr icinga2-client2.csr
~~~

En suite, depuis le master, on génère le certificat avec la CSR.

~~~
# icinga2 pki sign-csr --csr icinga2-client2.csr --cert icinga2-client2
~~~

Il ne reste plus qu'a rapatrier les fichiers `/etc/icinga2/pki/icinga2-client2.*` sur la machine désignée.


## Accéder à l'API en Python

Documentation de l'API : https://icinga.com/docs/icinga-2/latest/doc/12-icinga2-api/


### Récupérer le résultat d'un check

Par exemple, pour faire en Python l'équivalent d'un curl qui renvoie le `load` de `test-www00` :

~~~bash
$ USER=
$ PASSWORD=
$ HOST=
$ PORT=5665
$ SERVICE="test-www00!load"
$ curl -s -u "${USER}:${PASS}" "https://${HOST}:${PORT}/v1/objects/services/${SERVICE}" | jq '.results[0].attrs.last_check_result'
~~~

Faire la requête avec la fonction `get()` du module `requests` :

~~~python
user =
password =
host =
service = "test-www00!load"

url = f'https://{host}:5665/v1/objects/services/{service}'

import requests
response = requests.get(url, auth=(user, pass))
~~~

La réponse étant au format JSON, on la convertit en dictionnaire avec le module `json`.

~~~python
import json
response_dict = json.loads(response.text)
~~~

Puis, on l'affiche avec la fonction `pprint()` du module pretty print (`pprint`) :

~~~python
from pprint import pprint
check_result = response_dict['results'][0]['attrs']['last_check_result']
pprint(check_result)
~~~


### Désactiver la vérification du certificat (auto-signé…)

~~~python
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

response = requests.get(url, auth=(user, pass), verify=False)

~~~

Sinon on aura le warning suivant :

~~~
/usr/lib/python3/dist-packages/urllib3/connectionpool.py:1053: InsecureRequestWarning: Unverified HTTPS request is being made to host… Adding certificate verification is strongly advised.
~~~


## Accéder à l'API en Perl

Sur une debian Jessie :

~~~
# apt install libconfig-inifiles-perl
# cpan install REST::Client
~~~

[Exemples en Perl](https://docs.icinga.com/icinga2/latest/doc/module/icinga2/chapter/icinga2-api#icinga2-api-clients-programmatic-examples-perl)


## FAQ

### Que veut-dire « Icinga » ?

C'est un mot zulu signifiant « il recherche » ou « il examine ».

### plugin Vim pour la syntaxe de la configuration d'Icinga

Tous les fichiers de configuration utilisent la même syntaxe. Il existe un plugin vim pour cette dernière :

~~~
# apt -t jessie-backports install vim-icinga2
~~~

### Existe-t-il un log des évènements ?

Par défaut, il n'y en a pas. Néanmoins, il existe un module *compatlog*, activable avec `icinga2 feature enable compatlog` qui permet de générer un log des évènements dans `/var/log/icinga2/compat/icinga.log`

### Comment ajouter des plugins sur l'interface web ?

L'interface icingaweb2 est elle aussi extensible via des plugins. Mais comme la grande partie d'entre eux n'est pas packagée, il faut les installer soi-même. Quelques modules bien connus sont référencés [sur le site d'icinga](https://www.icinga.com/products/icinga-web-2-modules/)

Ainsi, on veut installer le plugin [generictts](https://github.com/Icinga/icingaweb2-module-generictts), on va faire : 

~~~
# mkdir -p /usr/local/share/icingaweb2/modules/
# cd /usr/local/share/icingaweb2/modules/
# git clone https://github.com/Icinga/icingaweb2-module-generictts.git generictts

# ln -s /usr/local/share/icingaweb2/modules/generictts /etc/icingaweb2/enabledModules/generictts
~~~

Vous n'avez plus qu'a vous rendre dans l'interface pour voir que le plugin est bien activé.


### Erreur `mux_client_request_session` Session open refused by peer

Sur l'interface web, on peut voir l'erreur suivante :

~~~
Remote command execution failed: mux_client_request_session: session request failed: Session open refused by peer
~~~

Il est possible que le `MaxSession` d'OpenSSH soit atteint. Voir la [page dédiée à OpenSSH](/HowtoOpenSSH#ssh-refuse-douvrir-une-session-ou-nombre-maximum-de-sessions-ssh-atteint) pour le changer.

### Comment ajouter un dashlet (onglet) dans icinga-web

Pour créer des nouvelles vues dans Icinga web à partir d'un filtre, appelé "dashlet" dans Inciga, vous pouvez suivre [cet article](https://icinga.com/blog/2021/01/27/creating-dashboards-based-on-custom-filters/) du blog d'Icinga. 


### Erreur `icingaweb[756299]: Unable to locate monitoring module` dans les logs

Si vos logs système sont saturés par le message suivant : `icingaweb[756299]: Unable to locate monitoring module`.

Rien de grave. Même si vous êtes passés à `icingadb` pour la visuation côté interface web, le module essaye de charger des composants PHP de l'ancien module de visualisation (`monitoring`), pour rester compatibles avec certains hooks. 
Installer le paquet `icingaweb2-module-monitoring` est suffisant pour que ce message n'apparaisse plus.

Inutile d'activer le module dans l'interface d'icingaweb


### Icingaweb / icingadb > Ordre des éléments (version Debian 12 (Bookworm))

Avec icingadb (dans sa version distribuée dans Debian 12), l'ordre par défaut n'est pas exactement comme dans l'ancien module (monitoring).
En effet, dans l'ancien module, le tri par défaut prends en compte la sévérité **et** la date de dernier changement d'état.
Le problème a été remonté à Debian en [#1095139](https://bugs.debian.org/1095139) dans le but de le voir corrigé lors d’une prochaine mise à jour de la version stable.

Sauf que dans la version actuelle, la date de changement d'état n'est pas prise en compte. Seul la sévérité de l'alerte est prise en compte.

Ce commportement a été corrigé dans le projet source ([ici](https://github.com/Icinga/icingadb-web/commit/acc11a395ddd9eb1c06591ab4ea108f75addf9f7) et [là](https://github.com/Icinga/icingadb-web/commit/c503f1438273d046428db537e46b787ff6f6d073)).

Il est possible d'appliquer le patch suivant qui reprends ces modifications pour récupérer le tri : 

```
# cat /tmp/icinga2.patch
diff -Npaur a/modules/icingadb/application/controllers/HostController.php b/modules/icingadb/application/controllers/HostController.php
--- a/modules/icingadb/application/controllers/HostController.php	2022-11-04 12:17:25.000000000 +0100
+++ b/modules/icingadb/application/controllers/HostController.php	2024-12-24 17:36:04.476749994 +0100
@@ -204,10 +204,10 @@ class HostController extends Controller
         $sortControl = $this->createSortControl(
             $services,
             [
-                'service.display_name'                    => t('Name'),
-                'service.state.severity desc'             => t('Severity'),
-                'service.state.soft_state'                => t('Current State'),
-                'service.state.last_state_change desc'    => t('Last State Change')
+                'service.display_name'                                              => t('Name'),
+                'service.state.severity desc,service.state.last_state_change desc'  => t('Severity'),
+                'service.state.soft_state'                                          => t('Current State'),
+                'service.state.last_state_change desc'                              => t('Last State Change')
             ]
         );
 
diff -Npaur a/modules/icingadb/application/controllers/HostsController.php b/modules/icingadb/application/controllers/HostsController.php
--- a/modules/icingadb/application/controllers/HostsController.php	2022-11-04 12:17:25.000000000 +0100
+++ b/modules/icingadb/application/controllers/HostsController.php	2024-12-24 17:35:21.164933448 +0100
@@ -54,10 +54,10 @@ class HostsController extends Controller
         $sortControl = $this->createSortControl(
             $hosts,
             [
-                'host.display_name'                 => t('Name'),
-                'host.state.severity desc'          => t('Severity'),
-                'host.state.soft_state'             => t('Current State'),
-                'host.state.last_state_change desc' => t('Last State Change')
+                'host.display_name'                                           => t('Name'),
+                'host.state.severity desc, host.state.last_state_change desc' => t('Severity'),
+                'host.state.soft_state'                                       => t('Current State'),
+                'host.state.last_state_change desc'                           => t('Last State Change')
             ]
         );
         $viewModeSwitcher = $this->createViewModeSwitcher($paginationControl, $limitControl);
diff -Npaur a/modules/icingadb/application/controllers/ServicesController.php b/modules/icingadb/application/controllers/ServicesController.php
--- a/modules/icingadb/application/controllers/ServicesController.php	2024-12-11 10:28:20.968176894 +0100
+++ b/modules/icingadb/application/controllers/ServicesController.php	2024-12-24 17:36:38.600605978 +0100
@@ -63,11 +63,11 @@ class ServicesController extends Control
         $sortControl = $this->createSortControl(
             $services,
             [
-                'service.display_name'                 => t('Name'),
-                'service.state.severity desc'          => t('Severity'),
-                'service.state.soft_state'             => t('Current State'),
-                'service.state.last_state_change desc' => t('Last State Change'),
-                'host.display_name'                    => t('Host')
+                'service.display_name'                                             => t('Name'),
+                'service.state.severity desc,service.state.last_state_change desc' => t('Severity'),
+                'service.state.soft_state'                                         => t('Current State'),
+                'service.state.last_state_change desc'                             => t('Last State Change'),
+                'host.display_name'                                                => t('Host')
             ]
         );
         $viewModeSwitcher = $this->createViewModeSwitcher($paginationControl, $limitControl);
```

Pour l'appliquer : 

```
# cd /usr/share/icingaweb2/
/usr/share/icingaweb2# patch -p1 < /tmp/icinga2.patch
```

## Liens

[https://www.icinga.com/category/technical/api/](https://www.icinga.com/category/technical/api/)

[https://docs.icinga.com/icinga2/latest/doc/module/icinga2/toc](https://docs.icinga.com/icinga2/latest/doc/module/icinga2/toc)

### API

[https://docs.icinga.com/icinga2/latest/doc/module/icinga2/chapter/icinga2-api#icinga2-api-clients-cli-console](https://docs.icinga.com/icinga2/latest/doc/module/icinga2/chapter/icinga2-api#icinga2-api-clients-cli-console)
