---
categories: tips
...

# Tips Ubuntu

## sources.list

Exemple pour Ubuntu 14.04 :

~~~
deb http://fr.archive.ubuntu.com/ubuntu/ trusty main restricted
deb http://fr.archive.ubuntu.com/ubuntu/ trusty-updates main restricted
deb http://fr.archive.ubuntu.com/ubuntu/ trusty universe
deb http://fr.archive.ubuntu.com/ubuntu/ trusty-updates universe
deb http://fr.archive.ubuntu.com/ubuntu/ trusty multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ trusty-updates multiverse
deb http://security.ubuntu.com/ubuntu trusty-security main restricted
deb http://security.ubuntu.com/ubuntu trusty-security universe
deb http://security.ubuntu.com/ubuntu trusty-security multiverse
~~~

Branches :

* Main : Paquets libres et officiellement supporté par Canonical ;
* Universe : Paquets maintenu par la communauté, pas officiellement supporté par Canonical ;
* Restricted : Paquets qui ont une licence non libre, du type codecs MP3, Sun Java, ... ;
* Multiverse : Paquets propriétaires ; 