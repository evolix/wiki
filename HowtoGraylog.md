---
categories: sysadmin
title: Howto Graylog
...

- Documentation : [https://docs.graylog.org/](https://docs.graylog.org/)
- Code : [https://github.com/Graylog2](https://github.com/Graylog2)
- Licence : [Serveur Side Public License](https://github.com/Graylog2/graylog2-server/blob/master/LICENSE) (non-libre)
- Language : Java
- Rôle Ansible : (à venir)

**Graylog Open** est un gestionnaire de journaux (*logs*). Écrit en Java, il s'appuit sur MongoDB et Elasticsearch (ou OpenSearch) pour la collecte, le stockage, et l'analyse des événements enregistrés dans les journaux des serveurs.

# Requis

Les [requis](https://docs.graylog.org/docs/architecture) du système selon la documentation officielle :

- Le noeud Graylog doit avoir beaucoup de puissance côté des processeurs
- Le noeud Elasticsearch (ou OpenSearch) doit avoir beaucoup de mémoire vive et des disques rapides
- Le noeud MongoDB a besoin de peu de ressources relativement aux deux autres
- Important : il faut obligatoirement un stratégie de sauvegardes pour les données (et les indices) d'Elasticsearch

Il est possible, pour le développement, le test et les petites installations de tout mettre sur un seul serveur. Cependant, règle générale, un [environnement de production multi-noeud](https://docs.graylog.org/v1/docs/multinode-setup) consistera (par exemple) en une première grappe (de trois serveurs ou plus) pour Graylog + MongoDB et une seconde grappe (de trois serveurs ou plus également) pour Elasticsearch. Les deux grappes réponderont aux requêtes derrière un système de répartition de charge.

# Installation

La [procédure](https://docs.graylog.org/v1/docs/debian) d'installation pour Debian 10 (Buster) et 11 (Bullseye) tirée de la documentation officielle suppose qu'on installe tout à neuf sur un seul serveur. En résumé, on installe les dépendances (openjdk, mongodb, elasticsearch) et ensuite on installe graylog (avec en option des plugins et des intégrations).

On installe les dépendances disponibles à même la distribution Debian :

~~~
# apt update && apt upgrade
# apt install apt-transport-https openjdk-11-jre-headless uuid-runtime pwgen dirmngr gnupg wget
~~~

Puis MongoDB à partir du dépôt du développeur :

~~~
# cd /etc/apt/trusted.gpg.d
# wget -O - https://www.mongodb.org/static/pgp/server-4.3.asc
# chmod 644 server-4.3.asc
# echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.3 main" | tee /etc/apt/sources.list.d/mongodb-org-4.3.list
# apt-get update
# apt-get install -y mongodb-org
~~~

On s'assure que MongoDB fonctionne et démarre en même temps que le système :

~~~
# systemctl daemon-reload
# systemctl enable mongod.service
# systemctl restart mongod.service
# systemctl status mongod.service
~~~

MongoDB installé et testé, on passe à Elasticsearch, qu'on installe aussi à partir du dépôt du développeur :

~~~
# cd /etc/apt/trusted.gpg.d
# wget https://artifacts.elastic.co/GPG-KEY-elasticsearch -O GPG-KEY-elasticsearch.asc
# chmod 644 GPG-KEY-elasticsearch.asc
# echo "deb https://artifacts.elastic.co/packages/oss-7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list
# apt update && sudo apt install elasticsearch-oss
~~~

On configure Elasticsearch pour les besoins de Graylog :

~~~
#  tee -a /etc/elasticsearch/elasticsearch.yml > /dev/null << EOT
#  cluster.name: graylog
#  action.auto_create_index: false
#  EOT
~~~

On s'assure que Elasticsearch fonctionne et démarre en même temps que le système :

~~~
# systemctl daemon-reload
# systemctl enable elasticsearch.service
# systemctl restart elasticsearch.service
# systemctl status elasticsearch.service
~~~

Nous voilà prêts à installer l'application Graylog elle-même :

~~~
# wget https://packages.graylog2.org/repo/packages/graylog-4.3-repository_latest.deb
# dpkg -i graylog-4.3-repository_latest.deb
# apt-get update && apt-get install graylog-server graylog-enterprise-plugins graylog-integrations-plugins graylog-enterprise-integrations-plugins
~~~

On remplacera naturellement `4.3` par la version qui convient. 

Graylog refusera de démarrer sans un minimum de configuration (notamment les paramètres `password_secret` et `root_password_sha2`du fichier `/etc/graylog/server/server.conf`). Pour `password_secret`, on peut exécuter la commande suivante :

~~~
# pwgen -N 1 -s 96
~~~

Pour `root_password_sha2`, celle-ci : 

~~~
# echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1
~~~

Ici on voudra conserver dans un gestionnaire le mot de passe choisi en plus de copier le résultat de la commande `sha256sum` dans `/etc/graylog/server/server.conf`.

Le paramètre `http_bind_address` peut être réglé directement sur le nom de l'hôte ou l'adresse IP du serveur dans un environnement simple. Dans un environnement plus complexe (impliquant HAProxy, nginx, Apache ou autre), on pourra se référer aux [paramètres de l'interface web](https://docs.graylog.org/v1/docs/web-interface) de la documentation officielle.

Finalement on s'assure que Graylog fonctionne et démarre en même temps que le système :

~~~
# systemctl daemon-reload
# systemctl enable graylog-server.service
# systemctl restart graylog-server.service
# systemctl status graylog-server.service
~~~

Graylog est maintenant prêt à faire l'ingestion de journaux provenant d'une vaste [gamme de sources](https://docs.graylog.org/v1/docs/sending-data).

# Configuration

[Plusieurs pages](https://docs.graylog.org/docs/configuring-graylog) de la documentation officielle montrent en détail comment configurer Graylog ([server.conf](https://docs.graylog.org/v1/docs/server-conf), [https](https://docs.graylog.org/docs/https), [répartition de charge](https://docs.graylog.org/docs/load-balancers), [sauvegardes](https://docs.graylog.org/docs/backup), [interface web](https://docs.graylog.org/docs/web-interface), [api REST](https://docs.graylog.org/docs/rest-api), [sécurisation](https://docs.graylog.org/docs/sec-mod-security), etc).

Les chemins par défaut des fichiers installés via les paquets Debian  :

|Fichier(s)         |Chemin                             | 
|-------------------|-----------------------------------|
|Conf. principale   | /etc/graylog/server/server.conf   |
|Conf. journal.     | /etc/graylog/server/log4j2.xml    |
|Plugins            | /usr/share/graylog-server/plugin  |
|Binaires           | /usr/share/graylog-server/bin     |
|Scripts            | /usr/share/graylog-server/scripts |
|Param. JVM         | /etc/default/graylog-server       |
|Journaux messages  | /var/lib/graylog-server/journal   |
|Journaux           | /var/log/graylog-server/          |

Note : Il est aussi utile de savoir où se trouvent les [fichiers de MongoDB et Elasticsearch](https://docs.graylog.org/v1/docs/file-locations).

# Mise à jour

~~~
# apt-get update
# apt-get install graylog-server
# systemctl restart graylog-server
~~~

Note : si mettre à jour Graylog est simple, il faut garder en tête les cas possibles d'incompatibilité avec les versions d'Elasticserch et/ou de MongoDB installées précédemment. Par exemple, Graylog 3.3.x n'est compatible qu'avec les versions 5.x et 6.x d'Elasticsearch. On peut s'assurer de ne pas tomber dans une impasse en consultant la [page sur la compatibilité des versions](https://docs.graylog.org/docs/upgrade#version-compatibility) de la documentation officielle.

# Utilisation

## Ingestion 

Graylog peut ingérer depuis diverses sources ([rsyslog, syslog-ng, journald, des fichiers JSON, etc.](https://docs.graylog.org/docs/sending-data)) 

Avec rsyslog, on ajoute une action de sortie directement dans `/etc/rsyslog.conf` (ou sinon dans `/etc/rsyslog.d/graylog.conf`) et on redémarre le service :

~~~
*.*@mongraylog.exemple.org:514;RSYSLOG_SyslogProtocol23Format
~~~

Note 1 : on remplace `@` par `@@` pour passer par TCP au lieu de UDP.

Note 2 : il est possible de configurer rsyslog pour [chiffrer le transport via TLS](https://www.rsyslog.com/doc/v8-stable/tutorials/tls_cert_summary.html?highlight=tls). 

## Interface Web
Pour se familiariser avec Graylog, on se connectera à l'interface web sur le port `9000` (par exemple `http://192.168.0.15:9000`) avec le nom d'utilisateur `admin` et le mot de passe inscrit dans son gestionnaire durant l'installation (plus haut).

## Recherche

Voir [https://docs.graylog.org/docs/queries](https://docs.graylog.org/docs/queries)

## Tuyaux

Voir [https://docs.graylog.org/docs/processing-pipelines](https://docs.graylog.org/docs/processing-pipelines)

## Flux

Voir [https://docs.graylog.org/docs/streams](https://docs.graylog.org/docs/streams)

## Tableaux de bord

Voir [https://docs.graylog.org/docs/dashboards](https://docs.graylog.org/docs/dashboards)

## Mesure

Voir [https://docs.graylog.org/docs/metrics](https://docs.graylog.org/docs/metrics)

## Alertes et notifications

Voir [https://docs.graylog.org/docs/alerts](https://docs.graylog.org/docs/alerts)
