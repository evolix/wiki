**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowtoVirtualBox

## Installer VirtualBox-ose sous Debian Lenny

VirtualBox dans Lenny est très obsolète, puisqu'il est en 1.6.6. Il faut utiliser les [wiki:HowtoDebian/Backports backports] pour avoir la version 3.2.4. (La dernière version est le 4.0.6 daté du 21 Avril)

Installation :

~~~
# aptitude -t lenny-backports install virtualbox-ose virtualbox-ose-dkms
# aptitude install build-essential linux-headers-`uname -r`
# dpkg-reconfigure virtualbox-ose-dkms
# modprobe vboxdrv
~~~

_Note : Si vous avez kvm d'installé il faut décharger le module du kernel avec `modprobe -r kvm_intel`_

## Installer VirtualBox sous Debian Lenny

Il faut utiliser le _repository_ d'Oracle.

~~~
deb <http://download.virtualbox.org/virtualbox/debian> lenny contrib non-free
~~~

Et ajouter la clé public du dépôt : 

~~~
# cd /etc/apt/trusted.gpg.d
# wget http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc
# chmod 644 oracle_vbox.asc
~~~

Et enfin l'installer.

~~~
sudo aptitude update
sudo aptitude install virtualbox-4.0
~~~

## phpvirtualbox

phpvirtualbox est une application web qui permet d'administrer et de gérer les machines virtuelles (VMs), virtualisé sur un Virtualbox.[[BR]]

[[Image(phpvirtualbox.png, 33%)]][[BR]]

_Il est nécessaire d'avoir VirtualBox>=4.0 pour obtenir le support RDP en attendant un jour obtenir un support VNC bien intégré avec VirtualBox ..._

Pré-requis :

* Installation : 
* VirtualBox>=4.0
* Un serveur web
* php>=5.1.0
* Utilisation : 
* Firefox >= 3.6.0
* Internet Explorer >= 8
* Opera >= 10.0
* Safari >= 4.0
* Chrome/Chromium >= 5.0 

### Configurer vboxwebservice

Éditer le fichier `/etc/default/virtualbox` et y changer/ajouter la variable `VBOXWEB_USER=nom_utilisateur` par le nom d'un utilisateur qui lancera le service VBox et donc exécutera les VMs.
Ensuite il faut démarrer le service manuellement.

~~~
/etc/init.d/vboxweb-service start
~~~

Pour l'ajouter au démarrage de l'ordinateur de façon automatique, on peut utiliser la commande suivante : 

~~~
update-rc.d vboxweb-service defaults
~~~

### Installer phpVirtualBox


   1. Télécharger la dernière version de phpVirtualBox sur le [site](http://code.google.com/p/phpvirtualbox/downloads/list) ;
   2. Décompresser l'archive, et copier le contenu dans un dossier de votre serveur web (ou à la racine) ;
   3. Renommer le fichier `config.php-example` en `config.php` et l'éditer avec les bon paramètres (login et mot de passe notamment) ;
   4. Accéder à l'URL de votre serveur web ;
   5. Connectez-vous à l'interface avec le login et mot de passe `admin/admin`
