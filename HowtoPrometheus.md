---
categories: monitoring stub
title: Howto Prometheus
...

* [Documentation officielle](https://prometheus.io/docs)

Prometheus est un système de surveillance système.

# Installation

<!-- TODO -->

# Exporteurs

Prometheus utilise principalement la récupération de métriques système par scraping. Les métriques sont alors présentées par des exporteurs spécifiques aux différents outils présents sur un serveur.

## Node Exporter (métriques système)

Le [Node Exporter](https://github.com/prometheus/node_exporter) permet l'exportation de métriques système d'une machine utilisant un kernel *NIX (Linux, OpenBSD…).

Pour l'installer sur Debian :

~~~
# apt install prometheus-node-exporter
~~~

On peut ensuite configurer l'adresse d'écoute de l'exporter en modifiant le fichier `/etc/default/prometheus-node-exporter` ainsi :

~~~
ARGS="--web.listen-address=:9100"
~~~

Il faut ensuite redémarrer le service `prometheus-node-exporter.service` :

~~~
# systemctl restart prometheus-node-exporter
~~~

## MySQL Server Exporter (métriques serveur MySQL)

[L'exporteur de métriques serveur MySQL/MariaDB](https://github.com/prometheus/mysqld_exporter) permet d'exporter de nombreuses métriques importantes pour un serveur MySQL/MariaDB.

Pour l'installer sur Debian :

~~~
# apt install prometheus-mysqld-exporter
~~~

Il faut ensuite créer un utilisateur MySQL pour l'exporteur :

~~~
CREATE USER IF NOT EXISTS 'prometheus'@'localhost' IDENTIFIED BY '<PASSWORD>';
GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'prometheus'@'localhost';
~~~

Et configurer l'exporteur pour utiliser les identifiants :

~~~
# cat > /etc/mysql/prometheus-exporter.cnf << EOF
[client]
user = prometheus
password = <PASSWORD>
EOF
# chown root:prometheus /etc/mysql/prometheus-exporter.cnf
# chmod 0640 /etc/mysql/prometheus-exporter.cnf
~~~

Il faut ensuite modifier `/etc/default/prometheus-mysqld-exporter` pour indiquer à l'exporteur d'utiliser ce fichier de configuration :

~~~
ARGS="--config.my-cnf /etc/mysql/prometheus-exporter.cnf"
~~~

Enfin, il faut redémarrer le service `prometheus-mysqld-exporter.service` :

~~~
# systemctl restart prometheus-mysqld-exporter
~~~
