# Astuces Qutebrowser

Qutebrowser est un navigateur Web avec des raccourcis clavier de type vim et une interface graphique minimale. Il est piloté essentiellement par un clavier et est inspiré par des logiciels similaires tels que Vimperator et dwb.

Qutebrowser a été développé par Florian Bruhin, pour lequel il a reçu un prix CH Open Source en 2016.

Documentation : <https://www.qutebrowser.org/#_documentation>

### Naviger au clavier 

~~~
f + <keys> : "Hint sur le même onglet (<ESC> pour annuler les 'keys' tapés au clavier)"
F + <keys> : "Hint sur un nouvel onglet (<ESC> pour annuler les 'keys' tapés au clavier)"
/ : "Rechercher un terme dans la page"
H : "Page précédente (historique)"
L : "Page suivante (historique)"
o : "Ouvrir une page"
i : "Mode insert (ajout texte)"
~~~