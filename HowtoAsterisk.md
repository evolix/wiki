**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Asterisk

<http://www.asterisk.org/community/documentation>

## Installation

~~~
# aptitude install asterisk
~~~

On active son démarrage dans /etc/default/asterisk :

~~~
RUNASTERISK=yes
~~~

Puis on démarre :

~~~
# /etc/init.d/asterisk start
Starting Asterisk PBX: asterisk.
~~~

Connexion en mode CLI :

~~~
# asterisk -r
Asterisk 1.6.2.9-2+squeeze10, Copyright (C) 1999 - 2010 Digium, Inc. and others.
Created by Mark Spencer <markster@digium.com>
Asterisk comes with ABSOLUTELY NO WARRANTY; type 'core show warranty' for details.
This is free software, with components licensed under the GNU General Public
License version 2 and other licenses; you are welcome to redistribute it under
certain conditions. Type 'core show license' for details.
=========================================================================
Connected to Asterisk 1.6.2.9-2+squeeze10 currently running on localhost (pid = 32537)
*CLI> 
~~~

Plus d'infos sur les commandes sur <http://www.voip-info.org/wiki/view/Asterisk+CLI>

## Configuration

La configuration se passe dans le répertoire /etc/asterisk. Pour un mode VoIP simple, les fichiers de configuration principaux sont 

* sip.conf
* extensions.conf

## Réseau

Le protocole SIP utilise principalement le port UDP/5060 + des ports UDP divers...

## Mode CLI

Recharger la configuration Asterisk :

~~~
*CLI> reload
~~~


### SIP

Lister les connexions SIP entrantes/sortantes :

~~~
*CLI> sip show peers
Name/username              Host            Dyn Nat ACL Port     Status     
[...]
~~~

Détails sur une connexion SIP particulière :

~~~
*CLI> sip show peer <PEER>
~~~

Lister les channels :

~~~
*CLI> sip show channels
Peer             User/ANR         Call ID          Format           Hold     Last Message    Expiry
1.2.3.4         (None)           53fa8add1dd3cba  0x0 (nothing)    No       Rx: OPTIONS               
192.168.0.21    (None)           32e1708b-5b55-4  0x0 (nothing)    No       Rx: REGISTER              
2 active SIP dialogs
~~~

Mode debug on / off pour SIP :

~~~
*CLI> sip set debug on
SIP Debugging enabled
*CLI> sip set debug off
SIP Debugging Disabled
~~~

### Plan de numérotation

Voir le plan de numérotation dans un contexte :

~~~
*CLI> dialplan show default
[ Context 'default' created by 'pbx_config' ]
  '100' =>          1. Answer()                                   [pbx_config]
                    2. Playback(demo-echotest)                    [pbx_config]
                    3. Echo()                                     [pbx_config]
                    4. Playback(demo-echodone)                    [pbx_config]
                    5. Hangup()                                   [pbx_config]
[...]
~~~

Recharger le plan de numérotation :

~~~
*CLI> dialplan reload
Dialplan reloaded.
~~~

## Administration


Voir les appels en cours :

~~~
CLI> core show channels verbose
~~~

## Meeting Room

### Debian 7

Pour avoir des meeting rooms, il est nécessaire d'avoir le module DAHDI

~~~
# aptitude install linux-headers-`uname -r` dahdi-source asterisk-dahdi
# m-a a-i dahdi
# modprobe dahdi_dummy
# chown -R asterisk: /dev/dahdi/
~~~

On peut ainsi configurer via meetme.conf :

~~~
conf => 1
conf => 2
conf => 3,1234
~~~

Et ensuite dans le plan de numérotation :

~~~
exten => 7001,1,MeetMe(1)
exten => 7002,1,MeetMe(2)
exten => 7003,1,MeetMe(3)
~~~

On peut lister les meeting rooms :

~~~
Meetme list
Conf Num       Parties        Marked     Activity  Creation  Locked
2              0002           N/A        00:11:55  Static    No    
1              0001           N/A        00:53:33  Static    No    

* Total number of MeetMe users: 3
~~~

On peut lister les participants à une meeting room ...et même killer (pratique en cas de bug d'un utilisateur non déconnecté) :

~~~
Meetme list 1
Meetme kick 1 8
~~~

### Debian 8

~~~
exten => 7001,1,ConfBridge(1)
exten => 7002,1,ConfBridge(2)
exten => 7003,1,ConfBridge(3)
~~~

~~~
CLI> confbridge list
Conference Bridge Name           Users  Marked Locked?
================================ ====== ====== ========
1                                     1      0 unlocked
~~~

~~~
CLI> confbridge list 1
Channel                        User Profile     Bridge Profile   Menu             CallerID         Muted
============================== ================ ================ ================ ================ =====
SIP/foobar-00001234            default_user     default_bridge                    foobar           No

CLI> confbridge kick 1 SIP/foobar-00001234
Participant 'SIP/foobar-00001234' kicked out of conference '1'
~~~

### Music On Hold (MOH)

<https://www.voip-info.org/using-ffmpeg-to-convert-music-on-hold-files/>

## Configuration téléphones

Pour les téléphones Linksys/CISCO (SPA922, SPA901, SPA512G, SPA3102, PAP2T, etc.).

On crée un entrée dans sip.conf :

~~~
[telephone1]
type=friend
host=dynamic
username=LOGIN
secret=PASS
quality=yes
nat=no
canreinvite=no
dtfmode=rfc2833
allow=ulaw
~~~

Puis on configure le téléphone ainsi :

~~~
EXT1

Proxy : IP
Register : yes
Display Name : telephone1
User ID : LOGIN
Password : PASS
use Auth ID : no
Auth ID : LOGIN
~~~


### Magic code Cisco / Linksys

Composer `****` puis :

~~~
110# : IP address
73738# puis 1 : reset
7932# puis 1 : enable web on WAN
~~~
