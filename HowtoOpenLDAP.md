**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été complètement révisée.**

# Howto OpenLDAP

* Documentation : <http://www.openldap.org/doc/admin25/>
* Rôle Ansible

[OpenLDAP](http://www.openldap.org) est une implémentation libre du protocole LDAP développée par The OpenLDAP Project.


## Installation

~~~
# apt install slapd ldap-utils ldapvi shelldap

$ /usr/sbin/slapd -VV
@(#) $OpenLDAP: slapd  (May 28 2017 16:59:46) $
        Debian OpenLDAP Maintainers <pkg-openldap-devel@lists.alioth.debian.org>

# systemctl status slapd
● slapd.service - LSB: OpenLDAP standalone server (Lightweight Directory Access Protocol)
   Loaded: loaded (/etc/init.d/slapd; generated; vendor preset: enabled)
     Docs: man:systemd-sysv-generator(8)
    Tasks: 3 (limit: 4915)
   CGroup: /system.slice/slapd.service
           └─23686 /usr/sbin/slapd -h ldap:/// ldapi:/// -g openldap -u openldap -F /etc/ldap/slapd.d
~~~


## Configuration

Les paramètres pour le démon slapd se trouvent dans le fichier `/etc/default/slapd`.
On ajuste notamment sur quels sockets le démon va écouter :

~~~
SLAPD_SERVICES="ldap://127.0.0.1:389/ ldapi:///"
~~~

Depuis Debian 6, OpenLDAP stocke directement sa configuration et de son schéma dans l'arborescence *cn=config*.
Notez que si vous êtes fainéant, il est toujours possible d'utiliser un bon vieux `slapd.conf`.

Le contenu de cette arborescence est aussi stocké sous forme de fichiers, dans le répertoire `/etc/ldap/slapd.d/` et ceux-ci sont chargés au démarrage de OpenLDAP.

Selon vos goûts, différentes commandes pour visualiser la configuration :

~~~
# slapcat -b cn=config
$ ldapsearch -xLLL -b cn=config -x -D cn=admin,cn=config -W
# ldapvi -Y EXTERNAL -h ldapi:// -b cn=config
~~~

Note : Cette dernière commande peut nécessiter :

* de ne pas définir de `base:` dans `$HOME/.ldapvirc`, car l'option `-b` (= `--base`) ne la surchargera pas.
* que l'ACL (`olcAccess`) suivante soit positionnée sur l'entrée `olcDatabase={0}config,cn=config` :

~~~
olcDatabase={0}config,cn=config
objectClass: olcDatabaseConfig
olcDatabase: {0}config
olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by * break
olcRootDN: cn=admin,cn=config
~~~

### Modifier la configuration

On pourra donc modifier la configuration à chaud via `ldapvi` ou tout simplement `ldapmodify` et de superbes fichiers LDIF.

À savoir : lorsqu'on modifie à chaud la configuration, OpenLDAP met alors immédiatement à jour le contenu du fichier correspondant dans `/etc/ldap/slapd.d/` !

Une autre possibilité est de stopper **slapd**, de modifier les fichiers LDIF dans _/etc/ldap/slapd.d/_ et de le redémarrer.
Attention, cela risque de générer des warnings à propos de checksums sur les fichiers. Pour contourner cela, il faut ré-éditer les entrées à chaud pour que les dumps soient regénérés avec les bons checksums...

Autre détail important, avant la version 2.5 il n'est pas possible de faire un delete à chaud sur une entrée de _cn=config_ !
Il faut donc utiliser la méthode nécessitant l'arrêt de _slapd_, faire les modifications sur les fichiers LDIF et le relancer.

### Directives

Pour gérer les logs, on utilise la directive _olcLogLevel_ dans l'entrée _cn=config_.

Exemple pour désactiver les logs :

~~~
cn=config
olcLogLevel: 0
~~~


### Gestion des schémas

Autrefois, il suffisait d'un simple _include_ vers un fichier .schema pour ajouter un schéma.
Mais avec la configuration dans _cn=config_, il faut désormais convertir un schéma en LDIF puis l'injecter !

Voici les étapes pour convertir un .schema en .ldif (exemple avec _amavis.schema_) :

1. Créer un fichier _/tmp/convert-to-ldap.conf_ contenant une seule ligne :

~~~
include /etc/ldap/schema/amavis.schema
~~~

2. On lance la commande suivante :

~~~
# mkdir /tmp/convert
# slaptest -f /tmp/convert-to-ldap.conf -F /tmp/convert
config file testing succeeded
~~~

3. On obtient donc un fichier LDIF : _/tmp/convert/cn\=config/cn\=schema/cn\=\{0\}amavis.ldif_

Pour l'injecter, il faut le modifier :

* ajouter le base DN en fin de 1ère ligne : cn=schema,cn=config
* supprimer les dernières lignes du fichier spécifiques à LDAP : structuralObjectClass, entryUUID, creatorsName, etc.
* renumérotation du _cn={0}_ (dn: et cn:) en fonction des schémas déjà existants

On peut ainsi faire :

~~~
# ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/convert/cn\=config/cn\=schema/cn\=\{0\}amavis.ldif 
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
adding new entry "cn={4}amavis,cn=schema,cn=config"
~~~

Note : Si besoin, on peut bien sûr convertir plusieurs schémas d'un coup via un fichier _/tmp/convert-to-ldap.conf_ du type :

~~~
include /etc/ldap/schema/core.schema
include /etc/ldap/schema/collective.schema
[...]
include /etc/ldap/schema/monschema2.schema
~~~

### Gestion des ACL

Les ACL s'ajoutent via des règles du type :

~~~
olcAccess: {0}to attrs=userPassword,shadowLastChange by self write by anonymous auth by dn="cn=admin,dc=example,dc=com" write by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by self write by dn="cn=admin,dc=example,dc=com" write by peername.ip="127.0.0.1" read by * none
~~~

/!\ en cas de réplication, bien penser à inclure l'utilisateur de réplication pour les attrs=userPassword,shadowLastChange sous peine
de ne pas avoir de synchronisation des mots de passe !!!

Pour ne pas avoir un annuaire lisible publiquement mais accessible depuis l'extérieur, on peut forcer l'authentification en remplaçant "by peername.ip="127.0.0.1" read" par :

~~~
by users read by anonymous auth
~~~

### Réplication (master->slave)

*Sur le master :*

* Ajouter le module syncprov (dans l'objet cn=module{0},cn=config) :

~~~
olcModuleLoad: {1}syncprov
~~~

* Ajouter l'objet suivant :

~~~
dn: olcOverlay={0}syncprov,olcDatabase={1}hdb,cn=config
objectClass: olcOverlayConfig
objectClass: olcSyncProvConfig
olcOverlay: {0}syncprov
structuralObjectClass: olcOverlayConfig
entryUUID: b5786b24-6688-1030-8824-91503a3ebf6f
creatorsName: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
createTimestamp: 20110829124650Z
entryCSN: 20110829124650.326464Z#000000#000#000000
modifiersName: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
modifyTimestamp: 20110829124650Z
olcSpSessionlog: 100
olcSpCheckpoint: 100 10
~~~

* Créer l' _ou_ ldapuser et l'utilisateur _repl_, et lui donner les droits de lecture sur l'ensemble de l'arbre LDAP

*Sur le slave :*

* S'assurer que le schéma LDAP soit le même que sur le master
* Ajouter le module syncprov (dans l'objet cn=module{0},cn=config) :

~~~
olcModuleLoad: {1}syncprov
~~~

* Ajouter les attributs suivants à l'objet "dn: olcDatabase={1}hdb,cn=config" :

~~~
olcDbIndex: entryUUID eq
olcSyncrepl: {0}rid=124 provider=ldap://ldap.example.com bindmethod=simple binddn="cn=repl,ou=ldapuser,dc=example,dc=com" credentials=XXX searchbase="dc=example,dc=com" retry="60 +" type=refreshAndPersist
~~~

### Gestion des index LDAP

On configure des index ainsi :

~~~
olcDbIndex: objectClass eq
olcDbIndex: default eq
olcDbIndex: phpgwContactOwner pres,eq,sub
olcDbIndex: uidNumber,gidNumber pres,eq
olcDbIndex: uid,mail eq,approx
~~~

Attention, si l'on modifie les index, il faut absolument les regénérer avec la commande suivante :

~~~
 # /etc/init.d/slapd stop
 # sudo -u openldap slapindex
 # /etc/init.d/slapd start
~~~

### SSL

<https://www.openldap.org/doc/admin25/tls.html>

On peut activer le LDAPS sur le port 636.

Attention, OpenLDAP est très sensible sur la partie TLS/SSL, il faut donc bien vérifier que le certificat mis en place est bien correct et que les droits
sur les fichiers de clé/certificats sont corrects, sinon OpenLDAP refusera de démarrer sans crier gare.

Pour vérifier les certificats, voir notre documentation sur la vérification de correspondance [entre clef privée et certificat](/HowtoSSL#v%C3%A9rifier-la-correspondance-entre-cl%C3%A9-priv%C3%A9e-certificat-.csr) et [entre la chaîne de certification et le certificat](/HowtoSSL#v%C3%A9rifier-la-correspondance-entre-le-certificat-interm%C3%A9diaire-et-le-certificat).

Pour les droits sur les fichiers, vous devrez peut-être ajouter l'utilisateur `openldap` au groupe `ssl-cert`, dans ce cas n'oubliez pas de redémarrer OpenLDAP avant pour qu'il tourne bien avec ce nouveau groupe secondaire :

~~~
# usermod -aG ssl-cert openldap
# systemctl restart slapd
~~~

Il faut ensuite ajouter ces 3 attributs à `cn=config` :

~~~
# ldapvi -Y EXTERNAL -h ldapi:// -b cn=config
+olcTLSCertificateFile: /etc/ssl/certs/ldap.example.com.crt
+olcTLSCertificateKeyFile: /etc/ssl/private/ldap.example.com.key
+olcTLSCACertificateFile: /etc/ssl/certs/chaine-certification.crt
~~~

Rappel : assurez bien qu'OpenLDAP a bien accès à ces fichiers, sinon OpenLDAP plantera et vous serez condamnés à éditer la configuration à froid avant de redémarrer OpenLDAP.

Si vous voulez utiliser le certificat « snakeoil » du système, vous pourrez ainsi faire :

~~~
# ldapvi -Y EXTERNAL -h ldapi:// -b cn=config
+olcTLSCertificateFile: /etc/ssl/certs/ssl-cert-snakeoil.pem
+olcTLSCertificateKeyFile: /etc/ssl/private/ssl-cert-snakeoil.key
~~~

ou avec du Let's Encrypt ça devrait ressembler à :

~~~
ldapvi -Y EXTERNAL -h ldapi:// -b cn=config
+olcTLSCertificateFile: /etc/letsencrypt/ldap.example.com/live/cert.crt
+olcTLSCertificateKeyFile: /etc/ssl/private/ldap.example.com.key
+olcTLSCACertificateFile: /etc/letsencrypt/ldap.example.com/live/chain.pem
~~~

Il reste maintenant à faire écouter sur le port 636 (LDAPS), il faut ajouter `ldaps:///` dans `/etc/default/slapd` pour écouter sur toutes les interfaces, par exemple :

~~~
SLAPD_SERVICES="ldap://127.0.0.1:389/ ldapi:/// ldaps:///"
~~~

Il faut ensuite redémarrer OpenLDAP.

On pourra tester le bon fonctionnement en ligne de commande :

~~~
$ openssl s_client -CApath /etc/ssl/certs -connect ldap.example.com:636
~~~

Suite à cette mise en place, si en tentant de se connecter au serveur apparaît l'erreur `ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)`, il se peut que ça soit dû au certificat non reconnu. Pour désactiver la vérification du certificat sur le client, ajouter cette ligne dans _/etc/ldap/ldap.conf_ :

~~~
TLS_REQCERT never
~~~

### Paramètre Berkeley Database (DB_CONFIG)

Les données brutes d'OpenLDAP sont stockées au format "Berkeley Database" dans `/var/lib/ldap/`.
Un certain nombre de paramètres peuvent être ajustés via le fichier `DB_CONFIG`.

Une fonctionnalité est notamment la rétention de fichiers de logs permettant de rejouer les modifications.
Ces logs sont dans stockés dans `/var/lib/ldap/log.000*` mais attention, on ne peut pas tous les effacer.
Le plus simple est de lancer la commande `db_archive -d` régulièrement pour purger les anciens fichiers qui ne sont pluys utilisés et éviter que l'espace disque explose.

Voir <https://www.openldap.org/doc/admin24/maintenance.html>

## Utilisation

### ldapvi

_ldapvi_ permet d'éditer un annuaire LDAP avec Vim.

Pour ajouter une entrée, on fera donc :

~~~
$ cat add.ldif
dn: ou=test,dc=example,dc=com
ou: test
objectClass: 
$ ldapadd [-h <hostname>] -D uid=xxx,ou=xxx,dc=evolix,dc=net -f add.ldif -W
add: 1, rename: 0, modify: 0, delete: 0
Action? [yYqQvVebB*rsf+?] y
Done.
~~~

### .ldaprc

Le fichier _~/.ldaprc_ est utilisé pour les outils inclus dans `ldap-utils` il permet de définir des paramètres particulier.
On peut notamment autoriser les certifications non reconnus :

~~~
tls_reqcert never
~~~

### shelldap

Cet outil permet de naviguer dans un annuaire LDAP et de modifier les éléments à la manière d'un shell et d'un système de fichier.

Il utilise les fichiers de configuration `$HOME/.shelldap.rc`, `/usr/local/etc/shelldap.conf` et `/etc/shelldap.conf`. Exemple :

```
binddn: uid=<login>,ou=<foo>,dc=example,dc=com
cacheage: 300
server: <host>
timeout: 10
promptpass: 1
```

Note: pour l'édition, il ne tient pas compte de `$SELECTED_EDITOR` (généralement dans `$HOME/.selected-editor`) mais plutôt de `$EDITOR`. Vous pouvez donc mettre `export EDITOR="/usr/bin/vim.basic"` dansvotre ficher `$HOME/.profile`


## Backup

Rien de plus simple :

~~~
slapcat -l ldap.bak
~~~

Cela dump l'arbre ldap dans un seul fichier LDIF. Cette sauvegarde n'inclut *pas* cn=config.

Pour sauvegarder la configuration (ACLs, Schema...etc) :

~~~
slapcat -b cn=config -l config.ldif
~~~

## Restauration

~~~
/etc/init.d/slapd stop
rm -rf /var/lib/ldap/*
sudo -u openldap slapadd -l ldap.bak
/etc/init.d/slapd start
~~~

## Migration

Afin de migrer une configuration/base LDAP d'un serveur à un autre, il faut procéder de la façon suivante :

Sur le serveur d'origine :

~~~
/etc/init.d/slapd stop
slapcat -n 0 -l config.ldap.bak
slapcat -n 1 -l data.ldap.bak
~~~

On copie ensuite ces deux fichiers vers le nouveau serveur.

Sur le serveur de destination :

~~~
/etc/init.d/slapd stop
rm -rf /etc/ldap/slapd.d/*
sudo -u openldap slapadd -n 0 -F /etc/ldap/slapd.d -l config.ldap.bak
sudo -u openldap slapadd -n 1 -l data.ldap.bak
/etc/init.d/slapd start
~~~

On pensera également à copier le .ldapvirc si besoin.

## Infos sur les attributs LDAP


### Attributs pour authentification Windows


*sambaPwdMustChange*: Timestamp Unix, date d'expiration du mot de passe Windows.

*sambaKickoffTime*: Timestamp Unix, date d'expiration du compte Windows.

*sambaPwdLastSet*: Timestamp Unix, dernière fois que le mot de passe a été changé. (Bizarrerie à ce propos...)

### Attributs pour authentification UNIX
'''
shadowExpire''': Durée en nombre de jours depuis le 1° Janvier 1970. Date d'expiration du compte.

*shadowInactive*: Combien de jours il faut accepter un mot de passe qui a expiré.

*shadowWarning*: Nombre de jours avant que le mot de passe expire et où le système va avertir de la proche expiration.

*shadowFlag*: -1 ???

*shadowLastChange*: Durée en nombre de jours depuis le 1° Janvier 1970 de la dernière fois que le mot de passe a été modifié (Ex: 15579 = 27/08/2012).

*shadowMin*: Age minimum en jours avant que l'utilisateur puisse changer son mot de passe. (Défaut à 0 = quand il veut).

*shadowMax*: Durée en jours de la validité du mot de passe. (Ex 365 = 1 an). Pour désactiver l'expiration du mot de passe, affecter la valeur -1.

## Log

Les logs de OpenLDAP sont écrit sur disque par syslog. Quand OpenLDAP écrit beaucoup de logs, on peut activer la rotation de log par logrotate avec la configuration suivante :

~~~(sh)
# cat <<EOF > /etc/logrotate.d/openldap
/var/log/openldap.log {
   missingok
   compress
   delaycompress
   dateext
   weekly
   rotate 53
   sharedscripts
   postrotate
       # OpenLDAP logs via syslog, tell rsyslog to reopen its log files
       /usr/bin/systemctl kill -s HUP rsyslog.service
   endscript
}
EOF
~~~

## FAQ

### Rechercher une info 

Admettons que l'on veuille lister toutes les machines sous Buster activées dans nagios :

~~~
ldapsearch -x -h $IP -LLL -b 'ou=machine,dc=domaine,dc=org' "(&(computerOS=buster)(NagiosEnabled=TRUE))" 
~~~

Si on veut chercher tous les alias dans le cas d'un ldap sur un serveur mail par exemple :

~~~
ldapsearch -xLLL -b ou=people,dc=domaine,dc=com "(&(objectClass=mailAlias))"
~~~

### Changer un mot de passe LDAP

~~~
# Générer un nouveau mot de passe :
apg -n 1 -m 12
# Calculer le hash LDAP du nouveau mot de passe :
slappasswd
# Mettre à jour le hash du mot de passe du compte LDAP :
ldapvi
~~~

### Logiciels pour gestion de l'annuaire

#### Windows

Client lourd : [http://www.ldapadmin.org/](http://www.ldapadmin.org/)

![Configurer la connexion au LDAP distant. Si pas de TLS activé, utiliser un tunnel SSH](/ldap_admin1.png)
![Rendu complet de l'annuaire](/ldap_admin2.png)

### could not parse entry

Lors d'ajout/modification d'un annuaire LDAP, si vous obtenez un message de ce type :

~~~
str2entry: invalid value for attributeType objectClass #0 (syntax 1.3.6.1.4.1.1466.115.121.1.38)
slapadd: could not parse entry (line=70)
structuralObjectClass: organizationalRole
~~~

C'est sûrement dû au fait que vos schémas spécifiques ne sont pas pris en compte car _slapd_
utilise la configuration présente dans _/etc/ldap/slapd.d_ si le dossier est présent.

### Insufficient access

~~~
Uncaught exception from user code:
        Insufficient access at ./add.pl line 678, <STDIN> line 2.
~~~

Vérifier dans les acl que votre utilisateur a le droit de modifier l'attribut que vous modifier.

### ldapvi ouvre avec nano

Pour changer l’éditeur lors ouverture ldapvi :

~~~{ .bash }
EDITOR=vim ldapvi
~~~

### Too many open files

En wheezy bug rencontré : 

~~~
slapd[15245]: warning: cannot open /etc/hosts.deny: Too many open files
~~~

Pour l'éviter ajouter dans /etc/default/slapd :

~~~
ulimit -n 8192
~~~

### TLS init def ctx failed: -1

Si vous obtenez un message du type :

~~~
slapd: main: TLS init def ctx failed: -1
~~~

Vérifiez que l'utilisation _openldap_ a bien accès aux clés privés et certificats définis dans sa configuration (vous pouvez utiliser `sudo -u openldap /bin/bash` pour vous en assurer).

### ldif_read_file: checksum error

Le répertoire `/etc/ldap/slapd.d/cn=config/` à du être modifié (à ne pas faire lorsque **slapd** tourne), il vous faudra regenérer le checksum manuellement.

On récupère le checksum actuel :

~~~
# apt install libarchive-zip-perl
# crc32 <(cat /etc/ldap/slapd.d/cn=config.ldif | tail -n +3)
42cd80b1
~~~

On le modifie exceptionnellement à la place de l'ancien :

~~~
vim /etc/ldap/slapd.d/cn=config.ldif
~~~

Ce qui donnera ce résultat :

~~~
# head -2 /etc/ldap/slapd.d/cn=config.ldif
# AUTO-GENERATED FILE - DO NOT EDIT!! Use ldapmodify.
# CRC32 42cd80b1
~~~

### Size limit exceeded

La limite par défaut lors d'un ldapseach est de 500. On peut l'augmenter en modifiant l'attribut olcSizeLimit, avec par exemple `ldapvi -Y EXTERNAL -h ldapi:// -b cn=config`.

### no equality matching rule

En cas d'erreur du type :

~~~
ldap_modify: Inappropriate matching (18)
        additional info: modify/delete: foo_attribute: no equality matching rule
~~~

Vérifiez que l'attribut concerné a bien une règle EQUALITY dans le schéma LDAP.

Si ce n'est pas possible d'ajouter une règle EQUALITY dansle schéma LDAP (par exemple pour les SYNTAX binary comme jpegPhoto), n'utilisez pas ldapmodify (LDAP_MOD_REPLACE) mais plutôt suppression (et recréation si besoin) :

~~~
$ cat delete.ldif

dn: cn=foo,dc=example,dc=com
changetype: modify
delete: jpegPhoto

$ ldapvi --ldapmodify delete.ldif
~~~

### no such value

En cas d'erreur du type :

~~~
> ldap_modify: No such attribute (16)
>     additional info: modify/delete: foo_attribute: no such value
~~~

Vérifier que votre attribut n'a pas des caractères spéciaux (saut de ligne, etc.), vous pouvez notamment d'abord remplacer votre ligne avec une valeur "test" pour voir si cela fonctionne.

### TLS error

Les erreurs SSL/TLS ne sont pas très explicites avec OpenLDAP, si vous en avez une, vérifiez d'abord que vos clés/certificats sont bien corrects avec des bons droits pour y accéder.

### LDIF limité à 76 caractères

Les commandes qui génèrent des formats LDIF comme `slapcat` limite leur ligne à 76 caractères et provoquent donc des sauts de lignes qui peuvnet être embêtant (par exemple quand on doit faire un remplacement dans un DN par exemple). Voici une astuce pour supprimer ce saut de ligne, sachant que cela ne pose pas de problème pour la réinjection :

~~~
slapcat | perl -p00e 's/\r?\n //g'
~~~

Voir <https://openldap-technical.openldap.narkive.com/AAz1pHXD/slapcat-generate-extra-space-characters-in-ldif-output>

Si on utilise `ldapsearch` il est aussi possible de passer l'option `-o ldif-wrap=no` pour désactiver ces coupures de lignes.

### Erreur : rootdn is always granted unlimited privileges

Pour certaines vieilles configurations avec `/etc/ldap/slapd.conf` :

* `rootdn` est sur le super-user de la base de données.
* Il est inutile de lui donner des accès car il les a déjà.
* Si on lui donne des droits dans les ACL (directives "access") cela génère l'avertissement ci-dessus. Ce n'est pas un avertissement de sécurité mais de redondance.

Il suffit de supprimer ou commenter les lignes `by` donnant des accès à rootdn dans les blocs `access` pour supprimer les warnings.

Attention, le terme `rootdn` n'est pas explicitement mentionné dans les blocs `access`, recherchez plutôt son uid (vous pouvez le trouver dans la direction de définition du `rootdn`).

Documentation : <https://www.openldap.org/doc/admin24/guide.html#Controlling%20rootdn%20access>

"Never add the rootdn to the by clauses. ACLs are not even processed for operations performed with rootdn identity (otherwise there would be no reason to define a rootdn at all)."
