---
categories: systemd container
title: Howto Nspawn
...

*nspawn* ou *systemd-nspawn* est un système de cloisonnement de processus. On peut faire le parallèle avec les chroot, mais en version plus poussée (ie: meilleure isolation) comme des containers.

*nspawn* peut être utilisé pour faire tourner une commande dans un système léger, isolé du fonctionnement de l'hôte par les mécaniques de *namespaces* (comme pour un container docker)


## Installation

~~~
# apt install systemd-container
~~~

## Utilisation

## Administration

## Optimisation

## Monitoring

### Nagios

### Munin

## Plomberie

## FAQ
