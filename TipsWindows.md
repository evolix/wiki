---
categories: tips windows
...

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Windows

## Windows Server 200x

### Supprimer complètement un AD

Pour supprimer complètement un Active Directory, une seule commande magique :

Dans l'explorateur : 

~~~
dcpromo /forceremoval
~~~

Cela le désintégrera du domaine ;-) (Jeu de mot, il sera en fait « sorti » du domaine et ne sera plus PDC, on pourra ensuite le faire joindre le domaine géré par le Samba).

## Shell natif

Sortir la date et heure :

~~~
@ECHO OFF
ECHO "%date% %time%"
~~~

Sortir des infos diverses :

~~~
echo Utilisateur = %USERNAME%
echo Machine = %COMPUTERNAME%
echo Systeme = %OS%
~~~

Commentaires dans un script :

~~~
REM Ceci est un commentaire
~~~

Faire uen pause dans un script :

~~~
pause
pause Valider
~~~

Redirection de la sortie vers un fichier :

~~~
ECHO foo >> "C:\foo\bar.log"
~~~

Ajuster le PATH :

~~~
SET PATH="C:\Program Files\cwRsync\bin";%PATH%
~~~

Montage/démontage de lecteurs :

~~~
net use X: \\192.0.43.10\XYZ
net use X: \\192.0.43.10\XYZ /persistent:yes
net use /delete X:
~~~

Utilisation du IF :

~~~
if %username%==john.doe net use Y: \\192.0.43.10\YZA
~~~

Appel à un script externe :

~~~
CALL \\192.0.43.10\foo\bar.vbs
~~~

Divers :

~~~
SET TERM=ansi
~~~

## Tips & Astuces

### Vider le cache DNBS

~~~
ipconfig /flushdns
~~~
