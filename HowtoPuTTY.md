# Howto PuTTY

PuTTY est un client SSH permettant d'exécuter des commandes sur un serveur distant.

## Installation sous Windows

Télécharger le fichier <https://the.earth.li/~sgtatham/putty/latest/x86/putty.exe> et mettez le dans votre répertoire préféré !

## Utilisation sous Windows

Double-cliquer sur le programme **putty.exe** et entrer les paramètres suivants :

* Host Name : (mettre le login et le nom de votre serveur, par exemple login@serveur.example.com)
* Port : 22 (à changer si on vous a préciser un n° de port différent)

Puis valider via le bouton **Open**

Vous devez maintenant entrer le mot de passe associé au compte fourni.

Si tout s'est bien passé, vous allez obtenir l'invite de commande suivante :

~~~
login@serveur:~$
~~~~

Vous pouvez alors taper des commandes *Shell*.