---
categories: hardware
title: Howto PDU Eaton
...

Cette documentation concerne les PDU Eaton `EVMAF132X` ainsi que les sondes environnementales `EMPDT1H1C2`.

## Connexion

Deux solutions pour se connecter :

* Via SSH :
    * Brancher le PDU sur un réseau disposant d’un DHCP, et récupérer son IP sur l’écran (appuyer sur `ESC`, IP indiquée sur l’écran principal)
* Via le port USB C :
    * Avec screen : `screen /dev/ttyACM0 9600`
    * Avec minicom : `minicom -b 9600 -D /dev/ttyACM0`
    * Avec minicom et un fichier de configuration :
    ```
    # cat /etc/minicom/minirc.eaton
    pu port /dev/ttyACM0
    pu baudrate 9600
    pu bits 8
    pu parity N
    pu stopbits 1
    pu rtscts No
    # minicom eaton
    ```

## Configuration de base

On peut configurer le réseau avec `netconf` et le temps avec `time`. Le reste (mails, notifications, SNMP, …) se configure avec les nombreuses commandes `rest`.

On peut lister tous les paramètres possibles et leurs valeurs actuelles avec `rest get -d4`. Attention cependant, cette commande renvoie plusieurs milliers de lignes. On peut réduire à `-d3` ou plus bas encore pour avoir un retour plus court mais moins complet.

On peut se déconnecter avec `logout`.

## Contrôle des prises

* Voir les mesures électriques actuelles de la prise \<NUMBER\> :

~~~
rest get /powerDistributions/1/outlets/<NUMBER>/measures
~~~

* Redémarrer la prise \<NUMBER\> (extinction pendant 10s avant rallumage) :

~~~
rest exec /powerDistributions/1/outlets/<NUMBER>/actions/reboot
~~~

* Éteindre la prise \<NUMBER\> :

~~~
rest exec /powerDistributions/1/outlets/<NUMBER>/actions/switchOff
~~~

* Allumer la prise \<NUMBER\> :

~~~
rest exec /powerDistributions/1/outlets/<NUMBER>/actions/switchOn
~~~

* Réinitialiser le cumul d'énergie partiel (le "partiel" peut être réinitialisé, le "total" ne peut pas) sur la prise \<NUMBER\> :

~~~
rest exec /powerDistributions/1/outlets/<NUMBER>/actions/resetPartialEnergy
~~~

## Monitoring via SNMP

Ces 2 MIBS sont disponibles pour monitorer le PDU :

* EATON-EPDU-MIB (mesures électriques du PDU) : <http://www.circitor.fr/Mibs/Html/E/EATON-EPDU-MIB.php>
* EATON-SENSOR-MIB (mesures environnementales des éventuelles sondes présentes) : <https://mibs.observium.org/mib/EATON-SENSOR-MIB/>

Les OIDs les plus intéressants :

~~~
# EATON-EPDU-MIB
## Entrée du PDU
.1.3.6.1.4.1.534.6.6.7.3.2.1.3 (inputVoltage) - Tension d'entrée en mV
.1.3.6.1.4.1.534.6.6.7.3.3.1.3 (inputCurrentCapacity) - Capacité d'intensité en mA
.1.3.6.1.4.1.534.6.6.7.3.3.1.4 (inputCurrent) - Intensité d'entrée en mA
.1.3.6.1.4.1.534.6.6.7.3.3.1.11 (inputCurrentPercentLoad) - Pourcentage d'intensité utilisée par rapport à la capacité
.1.3.6.1.4.1.534.6.6.7.3.4.1.3 (inputVA) - Puissance d'entrée en VA
.1.3.6.1.4.1.534.6.6.7.3.4.1.4 (inputWatts) - Puissance d'entrée en W
.1.3.6.1.4.1.534.6.6.7.3.4.1.5 (inputWh) - Énergie consommée en entrée en Wh depuis le dernier reset
.1.3.6.1.4.1.534.6.6.7.3.4.1.6 (inputWhTimer) - Date (UnixTimeStamp) du dernier reset du compteur précédent

## Banques
.1.3.6.1.4.1.534.6.6.7.5.1.1.3 (groupName) - Nom des banques disponibles
.1.3.6.1.4.1.534.6.6.7.5.1.1.5 (groupBreakerStatus) - Statut des disjoncteur des banques (1 = On, 2 = Off)
.1.3.6.1.4.1.534.6.6.7.5.3.1.3 (groupVoltage) - Tension en mV pour chaque banque
.1.3.6.1.4.1.534.6.6.7.5.4.1.2 (groupCurrentCapacity) - Capacité d'intensité en mA pour chaque banque
.1.3.6.1.4.1.534.6.6.7.5.4.1.3 (groupCurrent) - Intensité en mA utilisée par chaque banque
.1.3.6.1.4.1.534.6.6.7.5.4.1.10 (groupCurrentPercentLoad) - Pourcentage d'intensité utilisée par chaque banque par rapport à leur capacité

## Prises
.1.3.6.1.4.1.534.6.6.7.1.2.1.22 (outletCount) - Nombre de prises
.1.3.6.1.4.1.534.6.6.7.6.1.1.3 (outletName) - Nom de toutes les prises (peut être personnalisé)
.1.3.6.1.4.1.534.6.6.7.6.1.1.6 (outletDesignator) - Nom physique de toutes les prises
.1.3.6.1.4.1.534.6.6.7.6.4.1.2 (outletCurrentCapacity) - Capacité d'intensité en mA pour chaque prise
.1.3.6.1.4.1.534.6.6.7.6.4.1.3 (outletCurrent) - Intensité en mA utilisée par chaque prise
.1.3.6.1.4.1.534.6.6.7.6.5.1.2 (outletVA) - Puissance consommée par chaque prise en VA
.1.3.6.1.4.1.534.6.6.7.6.5.1.3 (outletWatts) - Puissance consommée par chaque prise en W
.1.3.6.1.4.1.534.6.6.7.6.5.1.4 (outletWh) - Énergie consommée par chaque prise en Wh depuis le dernier reset
.1.3.6.1.4.1.534.6.6.7.6.5.1.5 (outletWhTimer) - Date (UnixTimeStamp) du dernier reset du compteur précédent
.1.3.6.1.4.1.534.6.6.7.6.6.1.2 (outletControlStatus) - Statut de chaque prise (0 = Off, 1 = On, 2 = pendingOff, 3 = pendingOn)

# EATON-SENSOR-MIB
## Sondes
.1.3.6.1.4.1.534.6.8.1.1.1 (sensorCount) - Nombre de sondes branchées sur le PDU
.1.3.6.1.4.1.534.6.8.1.1.2.1.4 (sensorAddress) - Adresse modbus de chaque sonde branchée sur le PDU

## Température
.1.3.6.1.4.1.534.6.8.1.2.3.1.3 (temperatureValue) - Température mesurée par chaque sonde en 1/10 de °K (lever 273.2°K pour passer en °C)
.1.3.6.1.4.1.534.6.8.1.2.4.1.1 (temperatureMinValue) - Température la plus basse atteinte pour chaque sonde en 1/10 de °K (lever 273.2°K pour passer en °C)
.1.3.6.1.4.1.534.6.8.1.2.4.1.2 (temperatureMinValueSince) - Date (UnixTimeStamp) à laquelle la température la plus basse a été atteinte pour chaque sonde
.1.3.6.1.4.1.534.6.8.1.2.4.1.3 (temperatureMaxValue) - Température la plus haute atteinte pour chaque sonde en 1/10 de °K (lever 273.2°K pour passer en °C)
.1.3.6.1.4.1.534.6.8.1.2.4.1.4 (temperatureMaxValueSince) - Date (UnixTimeStamp) à laquelle la température la plus haute a été atteinte pour chaque sonde

## Humidité
.1.3.6.1.4.1.534.6.8.1.3.3.1.3 (humidityValue) - Humidité mesurée par chaque sonde en 1/10 de %
.1.3.6.1.4.1.534.6.8.1.3.4.1.1 (humidityMinValue) - Humidité la plus basse atteinte pour chaque sonde en 1/10 de %
.1.3.6.1.4.1.534.6.8.1.3.4.1.2 (humidityMinValueSince) - Date (UnixTimeStamp) à laquelle l'humidité la plus basse a été atteinte pour chaque sonde
.1.3.6.1.4.1.534.6.8.1.3.4.1.3 (humidityMaxValue) - Humidité la plus haute atteinte pour chaque sonde en 1/10 de %
.1.3.6.1.4.1.534.6.8.1.3.4.1.4 (humidityMaxValueSince) - Date (UnixTimeStamp) à laquelle l'humidité la plus haute a été atteinte pour chaque sonde
~~~

Les OIDs indiquant l'énergie consommée (inputWh et outletWh) sont accessibles en écriture afin de pouvoir réinitialiser les compteurs via écriture SNMP. Par exemple, pour réinitialiser toutes les prises d'un PDU avec 42 prises :

~~~
$ for i in {1..42}; do snmpset -v2c -c private <IP> .1.3.6.1.4.1.534.6.6.7.6.5.1.4.0.$i s '0'; done
~~~

Cependant en agissant de cette manière, les dates des derniers reset ne sont pas modifiées.

## Changer le mot de passe

Le mot de passe ne peut se changer que depuis l'interface web. Une fois connecté, cliquer tout en haut à droite sur le logo utilisateur > Change password.

## Installation d'une sonde de température/humidité

1. Configurer son adresse (`modbus address` sur le côté) : s'il est laissé à 0, il ne sera pas détecté ; si plusieurs sondes sont chaînées sur le même PDU, ils doivent chacun avoir une adresse différente
2. Brancher directement le port réseau `from device` de la sonde au port réseau `sensor` du PDU. Les adaptateurs fournis avec la sonde ne sont pas utiles pour ces PDUs
3. Depuis le PDU, exécuter `rest exec /sensors/actions/scanDiscover` pour découvrir la sonde. Elle est prête et fonctionnelle