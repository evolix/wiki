---
title: Howto nftables
categories: sysadmin network firewall routing
...

* WIKI nftables <https://wiki.nftables.org/>

[Nftables](https://wiki.nftables.org/) est un sous-système du noyau Linux assurant le filtrage et la classification des paquets / datagrammes / trames réseau. Il est disponible depuis le noyau Linux 3.13 publié le 19 janvier 2014. Il est destiné à remplacer l'ancien système iptables de Netfilter. Parmi les avantages de nftables par rapport à iptables, il y a moins de duplication de code ainsi qu'une meilleure prise en charge des nouveaux protocoles. La configuration est gérée via le programme nft, tandis que les outils hérités sont configurés via les utilitaires iptables, ip6tables, arptables et ebtables frameworks. Il utilise les éléments des modules Netfilter du noyau Linux, tels que les hooks existants dans la pile réseau, le système de suivi des connexions, le composant de mise en file d'attente de l'espace utilisateur et le sous-système de journalisation.

Depuis Debian Buster, nftables est le backend par défaut lors de l'utilisation d'iptables, grâce à la couche iptables-nft (utilisation de la syntaxe iptables assurant la compatabilité avec le sous-système du noyau nf_tables). Cela affecte également ip6tables, arptables et ebtables.

Le programme iptables-translate(8) permet la traduction des règles iptables au format nftables.

## Installation 
Nftables est disponible à partir de la version 3.13 du noyau Linux, sinon : 

```bash
# apt install nftables
$ uname -sr
# nft -v 
nftables v1.0.6 (Lester Gooch #5)
```

Afin de s'assurer que le jeu de règles nftables soit chargé au démarrage : 
_(Il s'agit d'une unité systemd de type OneShot )_

```bash
# systemctl enable nftables.service
```

# Configuration

La configuration de `nftables` peut être modifiée à la volée via la console interactive :

```sh
# nft -i 
nft> add [...]
```

De manière persistante via un fichier de configuration contenant le jeu de règles à charger (par défaut `/etc/nftables.conf`), ou par le lancement d'un script bash au démarrage : 

```bash
#! /usr/bin/sh
# [...]
# BANNED
nft="/sbin/nft";
${nft} add rule filter input meta iifname ${WAN} ip saddr 121.12.242.43 drop;
# [..]
```

Le fichier de configuration persistante doit suivre la structure suivante : 

```sh
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0; policy drop;
                 # regles ici 
		}
	}
}

```

Par défaut un fichier de configuration unique est chargé. Pour permettre un configuration arborescente et modifier le chemin d'accès à la configuration : 

```sh
# systemctl edit --full nftables
```

et modifier les lignes `ExecStart` et  `ExecReload` comme suit : 

```sh
 ExecStart=/usr/sbin/nft -f /etc/nftables.d/nftables.conf # à remplacer par /path/to/config
 ExecReload=/usr/sbin/nft -f /etc/nftables.d/nftables.conf  # à remplacer par l/path/to/config
```

## Structure globale

Tout comme son prédécesseur, nftables s'articule autour des objets de base : 
* tables
* chaines
* règles
### Les chaîne de type Base chain

A la difference d'IPtables, **seulement certaines chaines/tables sont traversées et pas toutes**. En effet, il existe deux types de chaînes : base et regular.

Les chaînes de type "base" sont liées à un "hook" noyau ( ce dernier est spécifié lors de la création de celle-ci) et indique au noyau de traverser cette chaîne lors de l'inspection de chaque paquet.
Il existe trois types de _base chains_:

* **filter**: Table dédiée au filtrage de paquets.
* **route**: Table dédiée au routage de paquets.
* **nat**: Table de nat.

Ainsi que 6 hooks noyau : 

* **Ingress**: Voit **tout le traffic arrivant** sur la machine, la machine locale peut en être la destinataire ou non.
* **prerouting**: Voit tout le traffic destiné à cette machine **avant toute décision de routage** (la machine locale peut être destinataire finale ou noeud intermédiaire).
* **input**: Voit Tout le traffic destiné à la machine locale.
* **forward**: Voit tout le traffic arrivant sur la machine mais n'étant pas destiné à celle-ci.
* **output**: Voit tout le traffic sortant de la machine locale (exclue les paquets à retransmettre via routage).
* **postrouting**: Voit tout le traffic à réémettre après décision de routage.
### Les chaîne de type Regular chain

Les chaînes de type regular sont des chaînes traversées uniquement sur décision d'une règle. Il est donc nécessaire, pour que les règles contenues dans une telle chaîne soient effectives, qu'une règle ait préalablement validé une décision  `Jump` ou `goto` pointant vers cette chaîne.

## Commandes globales

Pour vider toutes les règles nftables :

```
# nft flush ruleset
```

Tout objet présent dans la configuration se voit attribuer un numéro identifiant ce dernier dans la configuration appelé "handle". Pour afficher les handles : 

```
# nft -a list ruleset
```

Par la suite les opérations de délétion, renommage et modification utiliseront les handles comme identifiant unique de l'objet concerné.
Vérifier la configuration d'un ruleset : 

```
# nft -cf /path/to/ruleset
```

Charger la configuration d'un ruleset : 

```
# nft -f /path/to/ruleset
```

### Tables

#### Configuration via la ligne de commande

Ajouter une table (type  `inet` pour ipv4 et ipv6): 

```bash
# nft add table inet <table_name>
```

Lister les tables

```sh
# nft list tables
```

Supprimer une table

```sh
# nft delete table <table_name>
```

Vider les règles d'une table

```sh
# nft flush table <table_name>
```

#### Configuration via le fichier nftables.conf

La configuration d'une table : 

```nft
table inet <table_name> {
        chain <chain_name> {
        }
}
```

### Chaînes

Une chaîne de base se caractérise par sont type, son nom sa priorité et la base policy.
#### Configuration via la ligne de commande

Lors de l'execution des commandes ci-dessous, les commandes sont directement passées à la console nft, de ce fait il faut échapper les caractères spéciaux susceptibles de perturber le shell.

Ajouter une chaîne de base "input" à la table "filter" de type filter sur le hook "input".

```
# nft add chain inet filter input { type filter hook input priority 0\; }
```

Lister les chaînes : 

```
# nft list chains
```

Pour supprimer une chaîne 
```
# nft delete chain inet filter handle x
```

```
# nft add chain inet filter input { type filter hook input priority 0\; }
```
#### Configuration via le fichier nftables.conf

La configuration d'une base chain "input" de type "filter", sur le hook "input" :

```
chain input { 
        type filter hook input priority 0; policy drop;
  }
```

La configuration d'une regular chain "toto"  :

```
chain toto { 
  }
```

## Filtrage  spécifique

Exemple de règle nftables : 

```
ip saddr 194.168.4.0/24 tcp dport 3000 accept
```
il est possible d'attribuer plusieurs verdicts à une règle : 

```
ip saddr 194.168.4.0/24 tcp dport 3000 log accept
```

Note: Drop Accept et Reject sont des verdicts dits "finaux", et tout autre verdict placé à leur suite ne sera pas pris en compte (toujours placer log ou X verdict avant le verdict final)
## Sets, Maps, Concatenations

nftables apporte une gestion native d'objets complexes.

Exemples de sets : 

```
# variable: 
define lolo = "122.0.0.0/24"
# anonymous set 
define toto = { 22, 8080 }

# named set 
set titi {
    inet_service;
    elements = {
        22, # ssh 
        80, # http
        443, # https
        3434
    }
}

# exemple set anonyme
ip saddr  $lolo tcp dport { 22, 80 } drop

# exemple set nommé
ip saddr 120.0.0.1 tcp dport @titi drop
```

Une concaténation permet de "concaténer" plusieurs règles en conjuguant plusieurs critères (Equivalent d'un AND logique entre les filtres)
L'opérateur de concaténation est le `.`.
Un exemple de set avec concaténation : 

```
set titi {
    type inet_proto . inet_service; # Deux types d'objets différents séparés par l'opérateur de concaténation "."
    elements = {
        ###########TCP###########
        tcp . 22, # ssh  => tcp AND port 22
        ###########UDP###########
        udp . 3434 # X  => udp AND port 3434
    }
} 

# Règle utilisant une règle avec concaténation  :
ip saddr X.X.X.X meta l4proto . th dport @prive accept # => match les champs l4proto et dport (dans transport header) avec les champs de concaténation.
```

## Configuration par inclusion

```
include path/to/file # inclus le fichier sur place.
```

## Mise en place de Log en Userspace

installer paquet 
	configurer stack dans /etc/ulogd.conf
	spécifier group de log dans les regles nftables 0 ,1 ,2 etc

## Monitoring et Tracing spécifique de paquets


## Exemple

Voici un exemple permettant de mettre en place un firewall. Il s'agit ici d'autoriser le trafic SSH à l'IP 203.0.113.1 seulement et le trafic HTTP/HTTPS à tous. 

### Configuration en CLI

On doit donc réaliser les opérations suivantes :

Créer une table « filter » par exemple.

~~~
# nft add table inet filter
~~~

Créer une chaine « input » dans la table « filter » (drop du trafic par défaut).

~~~
# nft 'add chain inet filter input { type filter hook input priority 0 ; policy drop;}'
~~~

Créer une chaine « output » dans la table « filter » (autorisation du trafic par défaut).

~~~
# nft 'add chain inet filter output { type filter hook output priority 0 ; policy accept; }'
~~~
 
Ajouter les règles permettant le trafic SSH a l'IP 203.0.113.1 seulement et le trafic HTTP/HTTPS à tous.

~~~
# nft add rule inet filter input saddr 203.0.113.1/24 tcp dport ssh inet accept comment "Accept SSH on port 22 for IP 203.0.113.1"
# nft add rule inet filter input tcp dport { 80,443 } inet accept comment "Accept HTTP/HTTPS traffic"
~~~

Sous Debian des exemples de configuration sont visibles dans le dossier **/usr/share/doc/nftables/examples/**

## Débogage

## Liens

* [WIKI ARCHLINUX](https://wiki.archlinux.org/index.php/Nftables)
* [WIKI DEBIAN](https://wiki.debian.org/nftables)
* [WIKI NFTABLES](https://wiki.nftables.org/wiki-nftables/index.php/)
* [PAGE WIKIPEDIA](https://en.wikipedia.org/wiki/Nftables)
