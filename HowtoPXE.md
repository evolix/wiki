---
title: Howto PXE
...

Nous allons voir comment mettre en place un serveur de boot PXE qui va nous servir à installer des systèmes depuis le réseau au lieu des clés USB (que l'on peut perdre facilement!)
Pour ce faire, deux protocoles nous intéressent : [DHCP](https://fr.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) et [TFTP](https://fr.wikipedia.org/wiki/Trivial_File_Transfer_Protocol).

Toutes les manipulations seront à réaliser sur le même serveur (192.168.0.10) dont son réseau logique sera en 192.168.0.0/24 avec la passerelle en 192.168.0.254

On notera que dans le BIOS, il y a besoin d'activer et de mettre en priorité l'interface réseau (avant de pouvoir booter sur un disque) et d'être en mode "BIOS" et non pas "UEFI".

# Installation du service DHCP :

Commençons par installer le paquet suivant qui est simple d'utilisation :

~~~
apt install isc-dhcp-server
~~~

Ensuite, nous décrivons la configuration réseau dans le fichier /etc/dhcp/dhcpd.conf

~~~
subnet 192.168.0.0 netmask 255.255.255.0 {
  range 192.168.0.100 192.168.0.150;
  option broadcast-address 192.168.0.255;
  option routers 192.168.0.254;
  option domain-name-servers 192.168.0.254;
  next-server 192.168.0.10;             # Correspond à l'IP du serveur PXE
  filename "pxelinux.0";
}
~~~

## Avertissement

Pour éviter d’interférer avec le serveur DHCP de votre réseau ou pour effectuer des tests, il est nécessaire de recréer un second réseau de manière à ce qu'ils ne puissent pas communiquer entre eux.

Pour ne pas interférer avec l'autre réseau, on peut utiliser une IP dédiée à ce projet en modifiant le fichier /etc/default/isc-dhcp-server
Il s'agira alors de modifier le paramètre suivant :

~~~
INTERFACES="eth1"
~~~

On prend en compte ce changement :

~~~
systemctl restart isc-dhcp-server
~~~

# Installation du service TFTP

L'avantage de ce protocole est qu'il est simple d'utilisation car il y a juste besoin de l'installer (le T signifie "Trivial"). 
Nous allons utiliser l'installateur de Debian pour avoir la liberté de sélectionner son futur système.

~~~
apt install tftpd-hpa
~~~

Nos déploiement se trouveront dans le dossier :

~~~
cd /srv/tftp
~~~

On télécharge l'installateur Debian puis on le décompresse. On vérifie que le service est bien démarré :

~~~
wget http://cdn-fastly.deb.debian.org/debian/dists/jessie/main/installer-amd64/current/images/netboot/netboot.tar.gz
tar xf netboot.tar.gz
~~~

Ainsi nous avons ce fichier binaire pxelinux.0 qui sera demandé par le DHCP.
Il est maintenant possible d'installer un système Debian depuis le réseau.
On peut voir que tous nos services sont en écoute :


~~~
# netstat -lntpu |grep -e dhclient -e tftp
udp        0      0 0.0.0.0:1987            0.0.0.0:*                           411/dhclient
udp        0      0 0.0.0.0:68              0.0.0.0:*                           411/dhclient
udp        0      0 10.0.0.11:69            0.0.0.0:*                           3525/in.tftpd
udp6       0      0 :::18502                :::*                                411/dhclient
~~~

# Utiliser une ISO via PXE

Ajoutons un peu de difficulté en souhaitant utiliser les fichiers ISO pour voir le choix de son futur OS.
Admettons que nous voulons OpenBSD, on va lui créer son dossier pour simplifier l'organisation des ISO (supposons que l'on en fait la collection) :

Créons le dossier et téléchargeons l'ISO :

~~~
mkdir bsd
wget http://mirrors.ircam.fr/pub/OpenBSD/6.0/amd64/install60.iso -O bsd/openbsd60.iso
~~~

On doit ensuite modifier le fichier debian-installer/amd64/boot-screens/txt.cfg pour ajouter ceci :

~~~
label OpenBSD Install
  menu label OpenBSD Install
  kernel memdisk
  initrd bsd/openbsd60.iso
  append iso raw
~~~

Il nous reste à télécharger un dernier paquet pour avoir la possibilité de charger automatiquement les modules que le système aura besoin.

~~~
apt install syslinux
cp /usr/lib/syslinux/memdisk /srv/tftp/
~~~

Vous devez maintenant avoir le menu qui propose d'installer la version 6.0 d'OpenBSD.
