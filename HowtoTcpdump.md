---
title: Howto Tcpdump
categories: sysadmin network
...

[Tcpdump](https://www.tcpdump.org/) est un outil permettant d'analyser en détail le trafic visible sur une interface réseau.

Il est disponible sur les distributions GNU/Linux et systèmes d'exploitation FreeBSD, NetBSD, OpenBSD, Mac OS X et dépend de la bibliothèque libpcap.

<http://www.tcpdump.org/tcpdump_man.html>

## Installation

Sous Debian :

~~~
# apt install tcpdump
# tcpdump --version
tcpdump version 4.9.2
libpcap version 1.8.1
OpenSSL 1.0.2l  25 May 2017
~~~

Sous OpenBSD, tcpdump fait partie du système de base. Cette version
est plus ou moins forkée notamment pour apporter des améliorations
pour la sécurité (rajout de la séparation de privilège entre le parser
et la partie qui écoute le réseau).

## Utilisation de base

* Écouter tout le trafic d'une interface :

~~~
# tcpdump -i <int>
~~~

* Écouter ping sur l'interface :

~~~
# tcpdump -i <int> icmp and icmp[icmptype]=icmp-echo
~~~

* Écouter le trafic d'une interface pour un port particulier :

~~~
# tcpdump -i <int> port XXXX
~~~

* Écouter le trafic d'une interface pour un intervalle de ports particuliers :

~~~
# tcpdump -i <int> portrange XXXX-XXXX
~~~

* Écouter le trafic d'une interface pour un hôte particulier :

~~~
# tcpdump -i <int> host 192.0.2.1
~~~

* Écouter le trafic d'une interface pour un réseau particulier :

~~~
# tcpdump -i <int> net 192.0.2.0/24
~~~

* Écouter le trafic d'une interface pour un hôte et un port particulier  :

~~~
# tcpdump -i <int> host 192.0.2.1 and port XXXX
~~~

* Écouter le trafic UDP d'une interface pour un port particulier :

~~~
# tcpdump -i <int> udp port XXXX
~~~

* Écouter le trafic à destination de 192.0.2.42:XXXX

~~~
# tcpdump -i <int> dst 192.0.2.42 and port XXXX
~~~

* Afficher le protocole CARP avec des détails

~~~
# tcpdump -ni em1 -vvv proto carp
~~~

* Voir le détail des entêtes VLAN

~~~
# tcpdump -ni em1 -e vlan
~~~

> Le paramètre `-e` permet d'afficher les en-têtes de la couche de liaison

* Écouter le trafic sauf ssh (utile sur un serveur où l'on s'y connecte
en ssh)

~~~
# tcpdump -ni <int> not port 22
~~~

* Pouvoir grep la sortie de tcpdump

~~~
# tcpdump -envps 1500 -i enc0 -l | grep 192.0.2.123
~~~

* Enregistrer la sortie de tcpdump vers un fichier avec `-w`, pour l'analyser avec wireshark par exemple

~~~
# tcpdump -i <int> -w /tmp/test.pcap
~~~

## Utilisation avancée

* Écouter le trafic HTTP sur une interface et afficher les headers :

~~~
# tcpdump -A tcp port 80 or port 443 | egrep --line-buffered '^........(GET |HTTP\/|POST |HEAD )|^[A-Za-z0-9-]+: ' | sed -r 's/^........(GET |HTTP\/|POST |HEAD )/\n\n\1/g'
~~~

* Analyser les logs PF (l'attribut « log » doit être présent sur les
règles)

a posteriori

~~~
# tcpdump -n -ttt -e -r /var/log/pflog
~~~

en temps réel

~~~
# tcpdump -n -ttt -e -i pflog
~~~

* Récuper toutes les trâmes de type POST sur le port 80

~~~
# tcpdump -vv -s 0 -A 'tcp dst port 80 and tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x47455420 or tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x504F5354 or tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x48545450 or tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x3C21444F' -w /home/user/capture.pcap
~~~
