---                                                   
categories: installation installer debian dédié hetzner Hetzner cloud
title: ServeurHetzner
...

## Installer Debian sur le serveur dédié

* Se connecter sur le [manager Hetzner](https://robot.hetzner.com) avec les identifiants du serveur.

* Il faut activé le mode rescue sur le serveur, dans l'onglet "Rescue", choisir "Operating system -> Linux" et "Keyboard layout -> fr"
  * L'interface web va vous indiqué le mot de passe pour se connecter en rescue avec l'utilisateur "root".

* Redémarré le serveur dans l'onglet "Reset" -> "Execute an automatic hardware reset"

* Une fois connecter en rescue, utilisé le script "installimage", pour plus d'information voir [la documentation](https://docs.hetzner.com/robot/dedicated-server/operating-systems/installimage/).

* Voici la configuration pour faire un partitionement Evolix en UEFI, avec du LVM, si on veux un "/home" de 50Gio et un "/srv" de 1.2 Tio :

~~~
PART /boot/efi esp 256M
PART /boot ext4 500M
PART / ext4 2G
PART /usr ext4 6G
PART lvm vg0 all
LV vg0 swap swap swap 1G
LV vg0 var /var ext4 5G
LV vg0 tmp /tmp ext4 1G
LV vg0 home /home ext4 50G
LV vg0 srv /srv ext4 1200G
~~~

* Il y a quelques limitations sur la taille des partitions, la partition /boot/efi doit faire 256Mio, la partition / doit faire au minimum 1.5Gio
