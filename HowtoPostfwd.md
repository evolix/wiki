---
categories: mail
title: Howto postfwd
---

# How to postfwd

* Documentation : <https://www.postfwd.org/doc.html>

[postfwd](https://www.postfwd.org/) est un daemon permettant de définir des restrictions pour [postfix](http://www.postfix.org/) de la même manière que pour des parefeu.

## Installation

Pour que postfwd soit utile il faut avoir postfix installer \(voir [HowtoPostfix]()\).

```
apt install postfwd
postfwd2 --version
postfwd2 1.35 (Net::DNS 1.36, Net::Server 2.013, Sys::Syslog 0.36, Time::HiRes 1.977, Storable: 3.26, Perl 5.036000 on linux)
```

Puis il faut ajouter dans la conf postfix `smtpd_recipient_restrictions= <...>, check_policy_service inet:127.0.0.1:10040, <...>` afin que postfix utilise postfwd pour filtrer les messages.

## Configuration

postfwd s'appuie principalement sur `/etc/postfix/postfwd.cf`. Pour verifier la configuration il suffit de faire `postfwd -f /etc/postfix/postfwd.cf -C`.

Cette configuration prend la forme d'une liste de règles, une par ligne, du format :

```
[clé1=valeur; clé=valeur;...] action=<action>
```

(L'ordre n'est pas important donc les clés/valeurs peuvent aussi être après l'action).

L'une des clé utiles à connaître est `id=<nom_de_règle>` qui permet de donner un nom à cette règle pour pouvoir l'identifier plus simplement dans les fichiers de log.

### Actions

Les actions définissent ce qui est fait si le mail correspond à la règle. Elles incluent :

- `dunno`: Le mail est accepté par postfwd (si postfwd est le seul ou le dernier contrôle il est accepté par postfix, sinon il continu à être contrôler),
- `reject [message]`: Le mail est rejeté par postfix avec un message optionnel,
- `jump <id>`: Va directement à la règle correspondante (peut être en arrière mais risque de créer des boucles infini),
- `score <score>`: modifie le score du mail,
  > ⚠ Les scores sont globaux tant qu'ils n'ont pas été redéfinis.
- `rate(<item>/<max>/<temps>/<action>)`: limite le nombre total de messages dans un temps donné avant qu'une action soit effectuée, cette action peut être une action postfix ou postfwd (depuis 1.33, version dans buster = 1.35). Cette limite dépend de `<item>`,
- `size(<item>/<max>/<temps>/<action>)`: limite la taille cumulée des messages dans un temps donné avant qu'une action soit effectuée. La limite dépend aussi de la valeur de `<item>`,
- `rcpt(<item>/<max>/<time>/<action>)`: limite le nombre de receveurs avant une action (même fonctionnement que `rate` et `size`),
- `rate5321(<item>/<max>/<temps>/<action>)`: voir `rate()` mais sensible à la casse pour un émetteur ou receveur local,
- `size5321(<item>/<max>/<temps>/<action>)`: voir `size()` mais sensible à la casse pour un émetteur ou receveur local,
- `rcpt5321(<item>/<max>/<temps>/<action>)`: voir `rcpt()` mais sensible à la casse pour un émetteur ou receveur local.

### Clés

Ces clés peuvent être utilisé pour les conditions des règles, ainsi qu'en tant que variables dans les message de rejection en ajoutant deux `$` devant leur nom (exemple: `$$ratecount`).

Leurs valeurs peuvent être inversées en ajoutant deux points d'exclamations (`!`) devant ces valeurs.

- `id`: Nom pour la règle, utiliser par l'action `jump` et visible dans les logs,
- `date` et `time`: Une plage de dates/d'heures pendant laquelle la règle est validée,
- `days`et `months`: Une plage de jours/de mois pendant laquelle la règle est validée,
- `score`: La règle est validée si le score est supérieur à la valeur donnée,
- `rbl, rhsbl, rhsbl_client, rhsbl_sender, rhsbl_reverse_client`: RBL ou RHSBL à contacté par rapport au mail, la somme de celles répondant positivement (le mail correspond à la RBL/RHSBL) et combinée dans `rblcount` pour les RBL et `rhsblcount` pour les RHSBL,
- `rblcount, rhsblcount`: Somme des RBL ou RHSBL correspondantes au message (parmis celle contactées précédemment),
- `sender_localpart` et `sender_domain`: Partie local (nom d'utilisateur) et domaine de l'adresse mail de l'émetteur,
- `recipient_localpart` et `recipient_domain`: idem que précédemment mais pour le receveur,
- `ratecount`: Utilisable uniquement en tant que variable, la valeur du compteur de limite pour les action `size()`, `rate()` et `rcpt()`,
- ... (Dépend de la version de postfix car cela peut être n'importe quel attribut définit dans le postfix policy delegation protocol, [liste partiel](https://www.postfwd.org/doc.html#ITEMS))

Les valeurs pour ces clés peuvent être récupérées depuis un fichier en utilisant la valeur: `file:</path/to/file>` avec une valeur par ligne (+commentaires potentiels commençant par `#`).

