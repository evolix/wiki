---
categories: git
title: Howto Gerrit
...

* Documentation : <https://gerrit-documentation.storage.googleapis.com/Documentation/2.13.7/index.html>

[Gerrit](https://www.gerritcodereview.com/) est un logiciel libre de revue de code s'appuyant sur [GIT](HowtoGit). Il permet de lire, approuver ou rejeter les modifications d'un code source via une interface web.

# Installation

Les pré-requis sont d'avoir une JRE en version 7 minimum et un serveur SQL.
Nous conseillons d'utiliser _openjdk-7-jre_ (ou _openjdk-8-jre_) et MySQL.

~~~
# apt install default-jre
~~~

## Création de l'utilisateur Unix et MySQL

~~~
# adduser --disabled-password gerrit
# mysql
~~~

~~~{.sql}
mysql> CREATE USER 'gerrit'@'localhost' IDENTIFIED BY 'PASSWORD';
mysql> CREATE DATABASE gerrit;
mysql> GRANT ALL ON gerrit.* TO 'gerrit'@'localhost';
~~~

## Téléchargement du .war

~~~
# sudo -iu gerrit
$ wget https://www.gerritcodereview.com/download/gerrit-2.13.7.war -O gerrit.war
~~~

## Initialiser la configuration

~~~
$ java -jar gerrit.war init -d /home/gerrit
~~~

Une série de questions vous sera posée pour initialiser la configuration. On pourra répondre `?` pour avoir de l'aide.

La configuration est générée dans le fichier `/home/gerrit/etc/gerrit.config`.

Un exemple de session :

~~~
*** Gerrit Code Review 2.13.7
*** 


*** Git Repositories
*** 

Location of Git repositories   [git]: /home/git

*** SQL Database
*** 

Database server type           [h2]: mysql

Gerrit Code Review is not shipped with MySQL Connector/J 5.1.21
**  This library is required for your configuration. **
Download and install it now [Y/n]? y
Downloading https://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.21/mysql-connector-java-5.1.21.jar ... OK
Checksum mysql-connector-java-5.1.21.jar OK
Server hostname                [localhost]: 
Server port                    [(mysql default)]: 
Database name                  [reviewdb]: gerrit
Database username              [gerrit]: 
gerrit's password              : 
              confirm password : 

*** Index
*** 


The index must be rebuilt before starting Gerrit:
  java -jar gerrit.war reindex -d site_path

*** User Authentication
*** 

Authentication method          [OPENID/?]: ?
       Supported options are:
         openid
         openid_sso
         http
         http_ldap
         client_ssl_cert_ldap
         ldap
         ldap_bind
         custom_extension
         development_become_any_account
         oauth
Authentication method          [OPENID/?]: http_ldap
Get username from custom HTTP header [y/N]? ?
Get username from custom HTTP header [y/N]? 
SSO logout URL                 :  
LDAP server                    [ldap://localhost]: 
LDAP username                  : gerrit
gerrit's password               : 
              confirm password : 
Account BaseDN                 : ou=ldapuser,dc=foo,dc=bar
Group BaseDN                   [ou=ldapuser,dc=foo,dc=bar]: 
Enable signed push support     [y/N]? 

*** Review Labels
*** 

Install Verified label         [y/N]? 

*** Email Delivery
*** 

SMTP server hostname           [localhost]: 
SMTP server port               [(default)]: 
SMTP encryption                [NONE/?]: ?
SMTP username                  : 

*** Container Process
*** 

Run as                         [gerrit]: 
Java runtime                   [/usr/lib/jvm/java-7-openjdk-amd64/jre]: 
Copy gerrit-2.13.7.war to /home/gerrit/bin/gerrit.war [Y/n]? 
Copying gerrit-2.13.7.war to /home/gerrit/bin/gerrit.war

*** SSH Daemon
*** 

Listen on address              [*]: 
Listen on port                 [29418]: 

Gerrit Code Review is not shipped with Bouncy Castle Crypto SSL v152
  If available, Gerrit can take advantage of features
  in the library, but will also function without it.
Download and install it now [Y/n]? 
Downloading https://repo1.maven.org/maven2/org/bouncycastle/bcpkix-jdk15on/1.52/bcpkix-jdk15on-1.52.jar ... OK
Checksum bcpkix-jdk15on-1.52.jar OK

Gerrit Code Review is not shipped with Bouncy Castle Crypto Provider v152
** This library is required by Bouncy Castle Crypto SSL v152. **
Download and install it now [Y/n]? 
Downloading https://repo1.maven.org/maven2/org/bouncycastle/bcprov-jdk15on/1.52/bcprov-jdk15on-1.52.jar ... OK
Checksum bcprov-jdk15on-1.52.jar OK
Generating SSH host key ... rsa... dsa... done

*** HTTP Daemon
*** 

Behind reverse proxy           [y/N]? y
Proxy uses SSL (https://)      [y/N]? n
Subdirectory on proxy server   [/]: 
Listen on address              [*]: 127.0.0.1
Listen on port                 [8081]: 
Canonical URL                  [http://127.0.0.1/]: 

*** Cache
*** 


*** Plugins
*** 

Installing plugins.
Install plugin commit-message-length-validator version v2.13.7 [y/N]? y
Installed commit-message-length-validator v2.13.7
Install plugin download-commands version v2.13.7 [y/N]? y
Installed download-commands v2.13.7
Install plugin hooks version v2.13.7 [y/N]? y
Installed hooks v2.13.7
Install plugin replication version v2.13.7 [y/N]? 
Install plugin reviewnotes version v2.13.7 [y/N]? y
Installed reviewnotes v2.13.7
Install plugin singleusergroup version v2.13.7 [y/N]? y
Installed singleusergroup v2.13.7
Initializing plugins.
No plugins found with init steps.
~~~

Si vous obtenez le message d'erreur suivant :

~~~
Exception in thread "main" com.google.gwtorm.server.OrmException: Cannot initialize schema
[…]
Caused by: java.io.IOException: Cannot create repository All-Projects
[…]
Caused by: org.eclipse.jgit.errors.RepositoryNotFoundException: repository not found: Cannot create repository All-Projects
[…]
Caused by: java.io.IOException: Creating directories for /home/git/All-Projects.git failed
~~~

C'est que l'utilisateur `gerrit` n'a pas les droits de créer un dossier dans `/home/git` (ou le chemin renseigné).

## Démarrage du démon

Avant tout chose, il faut construire l'index.

> **Note** : L'utilisateur `gerrit` doit pouvoir accéder aux dépôts GIT dans `/home/git`.

~~~
$ java -jar bin/gerrit.war reindex
~~~

Puis lancer le démon :

~~~
$ bin/gerrit.sh start
~~~

Accéder au serveur web de Gerrit via `http://localhost:8081` par exemple, et créer le premier administrateur.

> **Note** : Si vous obtenez l'erreur `Missing project All-Projects`, il faudra peut-être supprimer la base `gerrit` et la recréer, puis relancer `l'init`.

### Script d'init

Il faut copier le script `gerrit.sh` qui est compatible « script d'init ».

~~~
# install -m 700 -o root -g root /home/gerrit/bin/gerrit.sh /etc/init.d/gerrit
~~~

Puis créer `/etc/default/gerritcodereview`.

~~~
# echo "GERRIT_SITE=/home/gerrit" >> /etc/default/gerritcodereview
~~~

On pourra ensuite activer le script d'init, et faire les actions `{start|stop|restart|check|status|run|supervise|threads}`.


# Configuration avancée

## Reverse Proxy

Pour utiliser [Apache](HowtoApache) devant Gerrit : <https://gerrit-documentation.storage.googleapis.com/Documentation/2.13.7/config-reverseproxy.html>

Exemple de VirtualHost :

~~~{.apache}
<VirtualHost *:80>
  ServerName review.example.com

  CustomLog /var/log/apache2/gerrit.access.log combined
  ErrorLog  /var/log/apache2/gerrit.error.log

  ProxyRequests Off
  ProxyVia Off
  ProxyPreserveHost On

  <Proxy *>
    Require all granted
  </Proxy>

  AllowEncodedSlashes On
  ProxyPass / http://127.0.0.1:8081/ nocanon
</VirtualHost>
~~~

> **Note** : Assurez-vous d'avoir activé le module apache `proxy_http`.

Il faudra s'assurer d'avoir dans la configuration de Gerrit :

~~~
[httpd]
    listenUrl = proxy-http://127.0.0.1:8081/
~~~

## Authentification LDAP via Apache

Via Apache, on peut gérer l'authentification via LDAP (Il est donc nécessaire de mettre en place la section [Reverse Proxy](#reverse-proxy)).

Il suffit d'ajouter dans le VirtualHost :

~~~{.apache]
  <Location "/login/">
    AuthType Basic
    AuthName "Gerrit Code Review"
    Require valid-user
    AuthBasicProvider ldap
    AuthzLDAPAuthoritative on
    AuthLDAPURL "ldap://127.0.0.1:389/ou=people,dc=example,dc=com?uid?sub?(objectClass=*)"
  </Location>
~~~

Une fois passé cette authentification, Gerrit lui-même rapatrie les informations de l'utilisateur via LDAP (champ sn…) en se connectant avec l'utilisateur renseigné dans sa configuration (ou anonymous) `ldap.username`…

# Monitoring

On peut surveiller avec Nagios la bonne réponse de l'interface web et la disponibilité du serveur SSH embarqué :

~~~
/usr/lib/nagios/plugins/check_http -H localhost -p 8081
/usr/lib/nagios/plugins/check_ssh -p 29418 localhost
~~~

# Créer un utilisateur Gerrit via SSH

Avec un compte administrateur, vous devez d'abord uploader votre clé publique dans l'interface web.

Dans cet exemple, on crée le compte _jenkins_, on pousse sa clé publique et on le met dans le groupe « Non-Interactive Users ».

~~~
$ cat /home/gerrit/.ssh/jenkins.pub | ssh -p 29418 admin@localhost gerrit create-account --group "'Non-Interactive Users'" --ssh-key - jenkins
~~~

# FAQ

## Erreur de login

Si vous obtenez l'erreur suivante : 

~~~
[HTTP-56] WARN  com.google.gerrit.server.query.account.InternalAccountQuery : Ambiguous external ID gerrit:foo for accounts: 3, 1
[HTTP-56] ERROR com.google.gerrit.httpd.auth.container.HttpLoginServlet : Unable to authenticate user "foo"
com.google.gerrit.server.account.AccountException: Cannot assign external ID "gerrit:foo" to account 9; external ID already in use.
        at com.google.gerrit.server.account.AccountManager.create(AccountManager.java:268)
[…]
~~~

C'est peut-être un problème de cache. Essayez de le reconstruire avec :

~~~
$ bin/gerrit.sh stop
$ java -jar bin/gerrit.war reindex --index accounts
$ bin/gerrit.sh start
~~~
