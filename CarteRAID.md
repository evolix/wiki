**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Pour installer sous Squeeze :
<http://infogerance-linux.net/2012/installation-de-debian-squeeze-sur-un-ibm-x3550m4-avec-carte-raid-m5110-base-sur-lsi-sas2208-roc-firmware-mars-2012/>

Mettre en place le RAID 1 : <http://www.youtube.com/watch?v=2nyKz9Apg6Q>

## Activer les cache lectures et écritures (avec BBU) :

~~~
megacli -LDSetProp -Cached -LAll -aAll
megacli -LDSetProp EnDskCache -LAll -aAll
megacli -LDSetProp ADRA -LALL -aALL
megacli -LDSetProp WB -LALL -aALL
~~~

## Infos

Voir les infos globales :

~~~
# megacli -AdpAllInfo -aAll
~~~


Voir les infos des volumes :

~~~
# megacli -LDInfo -L0 -a0
# megacli -LDInfo -L1 -a0
~~~
