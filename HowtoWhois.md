---
categories: network openbsd
title: Howto whois
...

Whois  est un service de recherche fourni par les registres Internet, par exemple les Registres Internet régionaux (RIR) ou bien les registres de noms de domaine permettant d'obtenir des informations sur une adresse IP ou un nom de domaine.

# Installation

Sur OpenBSD, il est inclus avec le système de base. Sur Debian :

~~~
# apt install whois

~~~

# Utilisation

L'utilisation de whois est assez différente entre Debian et OpenBSD du fait que les serveurs interrogés par défaut ne sont pas les mêmes. Il vaut mieux donc préciser le serveur whois utilisé :

~~~
$ whois -h whois.ripe.net 31.170.8.42
~~~


## Sur OpenBSD pour faire des requêtes à l'ARIN

Pour un objet route

~~~
$ whois -h whois.arin.net "r < 2607:C000::/48"
~~~

Pour un AS

~~~
$ whois -h whois.arin.net "a AS5645"
~~~

Pour une IP 

~~~
$ whois -h whois.arin.net "n < 2001:504:2d::18:35"
~~~

