**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto mail : introduction

Les courriers électroniques sont l'un des services les plus utilisés sur les réseaux informatiques. De nombreuses structures sont fortement dépendantes de ce service.
C'est la raison pour laquelle il est important de pouvoir le comprendre et le maîtriser.

Avant toute chose, il est recommandé de lire attentivement la [RFC 2821: Simple Mail Transfer Protocol](http://www.rfc-editor.org/in-notes/rfc2821.txt). Des règles d'usage ont également été établies, et toute personne utilisant les courriers électroniques devrait avoir lu la [1855 : Netiquette Guidelines](http://www.rfc-editor.org/in-notes/rfc1855.txtRFC). À défaut, il semble important de diffuser un résumé des principales consignes auprès des utilisateurs d'une structure.

En guise d'introduction, il semble important de rappeler le principe d'une conversation SMTP entre un client et un serveur :

~~~
$ telnet smtp.wanadoo.fr 25

220 [identification of their software and version information]
HELO [hostname]
250-[hostname]
MAIL From: <dest@ination.com>
250 Ok
RCPT To: Plouf Plouf <abuse@wanadoo.fr>
250 Ok
DATA
354 Enter mail, end with "." on a line by itself
From: John Doe <jdoe@example.com>
To: Thomas Oto <toto@example.com>
Subject: Hello World

J'utilise un super client mail ;-)
.
250 Ok: queued as [queue_id]
QUIT
221 Bye
~~~

Pour une récupération via le protocole POP :

~~~
$ telnet pop.example.com 110
+OK Hello there.

user nom_utilisateur
+OK Password required.

pass mot_de_passe
+OK logged in.

stat
+OK 979 5583265

list
+OK POP3 clients that break here, they violate STD53.
1 18759
2 31764
3 2836
4 2837
[...]

uidl
+OK
1 1336738341.6934.foobar.example.com,S=12655
2 1336738356.6953.foobar.example.com,S=12656
3 1336738369.6978.foobar.example.com,S=12659
[...]

retr 42
+OK 2836 octets follow.
Return-Path: 
[...]

top 42 3
+OK headers follow.

dele 42
+OK Deleted.

noop
+OK Yup.

quit
+OK Bye-bye.
~~~

Pour une récupération via le protocole IMAP :

~~~
$ telnet imap.example.com 143

* OK [CAPABILITY IMAP4rev1 UIDPLUS CHILDREN NAMESPACE THREAD=ORDEREDSUBJECT THREAD=REFERENCES SORT QUOTA IDLE ACL ACL2=UNION STARTTLS]
Courier-IMAP ready. Copyright 1998-2008 Double Precision, Inc.  See COPYING for distribution information.

. capability

* CAPABILITY IMAP4rev1 UIDPLUS CHILDREN NAMESPACE THREAD=ORDEREDSUBJECT THREAD=REFERENCES SORT QUOTA IDLE ACL ACL2=UNION STARTTLS
. OK CAPABILITY completed

. login nom_utilisateur mot_de_passe
. OK LOGIN Ok.

. LIST "" *

* LIST (\HasNoChildren) "." "INBOX.Corbeille"
* LIST (\HasNoChildren) "." "INBOX.Deleted Messages"
[...]

* LIST (\Marked \HasChildren) "." "INBOX"
. OK LIST completed

. select INBOX

* FLAGS (\Draft \Answered \Flagged \Deleted \Seen \Recent)
* OK [PERMANENTFLAGS (\* \Draft \Answered \Flagged \Deleted \Seen)] Limited
* 978 EXISTS
* 116 RECENT
* OK [UIDVALIDITY 1235180478] Ok
* OK [MYRIGHTS "acdilrsw"] ACL
. OK [READ-WRITE] Ok

. UID FETCH 1:* RFC822.SIZE

* 1 FETCH (UID 330392 RFC822.SIZE 18759)
[...]

* 978 FETCH (UID 331370 RFC822.SIZE 2837)
. OK FETCH completed.

. UID FETCH 331363 BODY[HEADER]

* 42 FETCH (UID 331363 BODY[HEADER] {3937}
Return-Path:
[...]

* 971 FETCH (FLAGS (\Seen))
. OK FETCH completed.

. UID FETCH 331363 BODY[]

* 971 FETCH (UID 331363 BODY[] {20742}
Return-Path:
[...]
. OK FETCH completed.

. NOOP
. OK NOOP completed

. LOGOUT

* BYE Courier-IMAP server shutting down
. OK LOGOUT completed
~~~