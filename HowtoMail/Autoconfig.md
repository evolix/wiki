# RFC6186 + RFC8314

La méthode d'autoconfiguration des clients de courriel la plus répandue est définie dans la [RFC6186](https://www.rfc-editor.org/rfc/rfc6186) et la [RFC8314](https://www.rfc-editor.org/rfc/rfc8314) qui rajoute le support du TLS à la première.

# Thunderbird

La méthode d'auto-configuration de Thunderbird est aussi supportée par d'autres clients mail comme [Evolution ou KMail, ...](https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat).

L'auto-configuration se fait à partir de fichier de configuration local au client, à partir de la base de donnée hébergé sur les serveurs de Mozilla, mais il est aussi possible de définir cette auto-configuration par nous même. Pour ces ressources distances (qui ne dépendent pas de Mozilla) ça se fait dans [cet ordre-là](https://wiki.mozilla.org/Thunderbird:Autoconfiguration#Implementation) :

1. `http://autoconfig.emailaddressdomain/mail/config-v1.1.xml?emailaddress=alice@example.com`, qui retourne un fichier de config comme décrit ci-dessous
2. `http://example.com/.well-known/autoconfig/mail/config-v1.1.xml`, même fichier attendu que pour le point n°1
3. Devine la configuration en essayant les ports communs ainsi que le support SSL pour les domaines : imap.domain, pop.domain, pop3.domain, smtp.domain and mail.domain

Pour générer un fichier d'autoconfiguration au format [ConfigFileFormat](https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat) pour Thunderbird :

~~~ sh
hostname=mail00.example.com
imap_server=imap.example.com
imap_port=993
smtp_server=smtp.example.com
smtp_port=587

cat <<EOF
<?xml version="1.0"?>
<clientConfig version="1.1">
    <emailProvider id="mail02.evolix.net">
      <domain>${hostname}</domain>
      <displayName>Service de messagerie électronique ${hostname}</displayName>
      <displayShortName>${hostname}</displayShortName>
      <incomingServer type="imap">
         <hostname>${imap_server}</hostname>
         <port>${imap_port}</port>
         <socketType>SSL</socketType>
         <authentication>password-cleartext</authentication>
         <username>%EMAILADDRESS%</username>
      </incomingServer>
      <outgoingServer type="smtp">
         <hostname>${smtp_server}</hostname>
         <port>${smtp_port}</port>
         <socketType>SSL</socketType>
         <username>%EMAILADDRESS%</username>
         <authentication>password-cleartext</authentication>
      </outgoingServer>
    </emailProvider>
</clientConfig>
EOF
~~~

Malheureusement, Mozilla Thunderbird est le principal client mail qui [ne supporte pas (encore ? )](https://bugzilla.mozilla.org/show_bug.cgi?id=342242#c63) la [RFC6186](https://www.rfc-editor.org/rfc/rfc6186), on ne peut donc pas mettre en place de l'autoconfiguration juste en changeant des entrées DNS.

# K-9 Mail

Même fichier XML que ci-dessus, et K-9 Mail recherche directement sur `http://example.com/.well-known/autoconfig/mail/config-v1.1.xml`

# Outlook (Autodiscover)

Depuis au moins 2016, Outlook [supporte](https://support.microsoft.com/en-us/topic/outlook-2016-implementation-of-autodiscover-0d7b2709-958a-7249-1c87-434d257b9087) la [RFC6186](https://www.rfc-editor.org/rfc/rfc6186), ce qui pratique quand on veux configurer l'autconfiguration des clients de courriels exclusivement par DNS.

La documentation du fonctionnement d'Autodiscover ainsi que la description du format de configuration est documenté par Microsoft [ici](https://learn.microsoft.com/en-us/previous-versions/office/office-2010/cc511507(v=office.14)?redirectedfrom=MSDN) et [là](https://learn.microsoft.com/en-us/exchange/client-developer/web-service-reference/pox-autodiscover-response-for-exchange).

Pour générer un fichier d'autoconfiguration pour Outlook :

~~~ sh
imap_server=imap.example.com
imap_port=993
smtp_server=smtp.example.com
smtp_port=587

cat <<EOF
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
  <Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
    <Account>
      <AccountType>email</AccountType>
      <Action>settings</Action>
      <Protocol>
        <Type>IMAP</Type>
        <Server>${imap_server}</Server>
        <Port>${imap_port}</Port>
        <DomainRequired>on</DomainRequired>
        <SPA>off</SPA>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
      </Protocol>
      <Protocol>
        <Type>SMTP</Type>
        <Server>${smtp_server}</Server>
        <Port>${smtp_port}</Port>
        <DomainRequired>on</DomainRequired>
        <SPA>off</SPA>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
      </Protocol>
    </Account>
  </Response>
</Autodiscover>
EOF
~~~

# Apple

Pour générer un fichier d'autoconfiguration pour les clients mails d'Apple :

~~~ sh
hostname=mail00.example.com
hostname_reverse=$(echo "$hostname" | tr . '\n' | tac | tr '\n' . | sed 's/.$//')
imap_server=imap.example.com
imap_port=993
smtp_server=smtp.example.com
smtp_port=587

cat <<EOF
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>EmailAccountType</key>
            <string>EmailTypeIMAP</string>
            <key>IncomingMailServerAuthentication</key>
            <string>EmailAuthPassword</string>
            <key>IncomingMailServerHostName</key>
            <string>${imap_server}</string>
            <key>IncomingMailServerPortNumber</key>
            <integer>${imap_port}</integer>
            <key>IncomingMailServerUseSSL</key>
            <true/>

            <key>OutgoingMailServerAuthentication</key>
            <string>EmailAuthPassword</string>
            <key>OutgoingMailServerHostName</key>
            <string>${smtp_server}</string>
            <key>OutgoingMailServerPortNumber</key>
            <integer>${smtp_port}</integer>
            <key>OutgoingMailServerUseSSL</key>
            <true/>
            <key>OutgoingPasswordSameAsIncomingPassword</key>
            <true/>

            <key>PayloadDisplayName</key>
            <string>Email autoconfiguration profile</string>
            <key>PayloadIdentifier</key>
            <string>${hostname_reverse}</string>
            <key>PayloadType</key>
            <string>com.apple.mail.managed</string>
            <key>PayloadUUID</key>
            <string>$(uuidgen)</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>SMIMEEnablePerMessageSwitch</key>
            <false/>
            <key>SMIMEEnabled</key>
            <false/>
            <key>disableMailRecentsSyncing</key>
            <false/>
        </dict>
    </array>
    <key>PayloadDisplayName</key>
    <string>Email autoconfiguration</string>
    <key>PayloadIdentifier</key>
    <string>${hostname_reverse}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>$(uuidgen)</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
</dict>
</plist>
EOF
~~~
