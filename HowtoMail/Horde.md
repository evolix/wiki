**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Horde

## Installation Horde / IMP / modules

Suivre la documentation "officielle" sur <http://wiki.debian.org/Horde>

## FAQ

Q : `HORDE [error] [imp] Failed to open sendmail [/usr/lib/sendmail] for execution. [pid 1415 on line 720 of "/usr/share/horde3/imp/lib/Compose.php"]`

R : S'assurer que la fonction popen() de PHP est autorisée

Q : Page blanche et aucune erreur dans les logs PHP/Apache

R : s'assurer que la fonction `putenv()` est autorisée dans la conf PHP/Suhosin (l'appel à cette fonction se fait dans _/usr/share/horde3/lib/Horde/NLS.php:611_, méthode `NLS::setLang()`.

Q : `This request cannot be completed because the link you followed or the form you submitted was only valid for 0 seconds.`

R : Tenter de regénérer la configuration de IMP via l'interface d'administration
Ou d'ajouter `$conf['urls']['token_lifetime'] = 1800;` dans /etc/horde/horde3/conf.php

Q : Avec IMP, utilisation de la boîte _Envoyé_ / _Envoy??_... non utilisable en IMAP

R :

~~~
$ apt-get source imp4
$ export LANG=fr_FR && export LC_ALL=fr_FR && export LC_TYPE=fr_FR && export XTERM_LOCALE=fr_FR && xterm
$ sed -i s/"Envoyé"/"Sent"/ imp4-4*/po/fr_FR.po
$ msgfmt imp4-4*/po/fr_FR.po
$ scp messages.mo SERVEUR:
$ ssh SERVEUR sudo mv messages.mo /usr/share/horde3/imp/locale/fr_FR/LC_MESSAGES/imp.mo
$ ssh SERVEUR sudo /etc/init.d/apache2 restart
~~~

Q : J'obtiens plein d'erreurs Javascript depuis une mise-à-jour de Horde ou IMP du type : `$H is not defined`, etc.

R : Les dernières versions de Horde/IMP utilisent des liens symboliques pour le Javascript, il faut donc bien s'assurer d'avoir `Options Indexes MultiViews FollowSymlinks` dans Apache

Q : Avec Turba, j'obtiens l'erreur `Le contact demandé n'existe pas.` en éditant un contact

R : il vous manque le paquet Debian php-mdb2-driver-mysql

Q : Lors de la rédaction d'un message, le domaine n'est pas définit dans le champ From (adresse affichée de la forme : _jdoe@_) et l'envoi de mail n'est donc pas possible.

R : il faut définir le domaine mail du serveur dans la config de imp dans le fichier _/etc/horde/imp4/prefs.php_ dans cette section :

~~~
[...]
// default outgoing mail domain and address completion         
$_prefs['mail_domain'] = array(                                
    'value' => 'example.com',                             
    'locked' => false,                                         
    'shared' => false,                                         
    'type' => 'text',                                          
    'desc' => _("When sending mail or expanding addresses, what domain should we append to unqualified addresses (email addresses without \"@\")?"));
[...]
~~~