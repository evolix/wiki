# Howto Roundcube

Sous licence libre GPL, roundcube est un client de messagerie utilisant l'IMAP.

## Installation 

~~~
# apt install roundcube roundcube-plugins roundcube-plugins-extra
~~~

## Configuration

Il ne faut pas modifier le fichier de configuration par défaut (`/etc/roundcube/defaults.inc.php`).

La configuration se fait dans `/etc/roundcube/config.inc.php`.

On pourrait notamment vouloir ajuster :

~~~
$config['imap_host'] = 'ssl://mail.example.com:993';
$config['smtp_host'] = 'tls://mail.example.com:587';

$config['plugins'] = [
    'managesieve',
];
~~~

### Taille maximum des pièces jointes

Imaginons que l'on veut autoriser une taille maximum des pièces jointes jusqu'à 10 Mo.

Il faut mettre au moins +40% de 10Mo dans `/etc/postfix/main.cf` :

~~~
message_size_limit = 25600000
~~~

Il faut configurer PHP pour avoir des valeurs supérieures à +33% de 10 Mo pour ces paramètres :

~~~
post_max_size = 20M
upload_max_filesize = 20M
~~~

Enfin, il faut configurer Roundcube à exactement +33% de 10 Mo via `/etc/roundcube/config.inc.php` :

~~~
config['max_message_size'] = '13.33M';
~~~


## Plugins

### Plugin managesieve (anciennement sieverules)

Le plugin "managesieve" est fourni par le paquet roundcube-plugins

> Note : sur des anciennes version de Debian, il s'agissait du plugin "sieverules" fourni avec le paquet roundcube-plugins-extra

Ce plugin permet de créer des règles Sieve stockées sur le serveur IMAP.
C'est notamment utile pour gérer des filtres de messages ou l'envoi de message d'absence.

La configuration se fait via `/etc/roundcube/plugins/managesieve/config.inc.php`.

> Note : sur des anciennes version de Debian, c'est `/etc/roundcube/plugins/sieverules/config.inc.php`.

### Plugin vacation

Pour envoyer des messages d'absence, on privilégie l'utilisation de [filtres Sieve](#plugin-sieve) dans Roundcube (Préférences > Filtres). A Evolix, on l'utilise pour les comptes mails virtuels configurés dans LDAP.

Pour les comptes mails Unix, on utilise le plugin Roundcube vacation suivant (attention aux forks !) : <https://github.com/gabor-toth/roundcube-vacation-plugin>

Sinon, on peut utiliser ce fork qui englobe la traduction française : <https://github.com/leeroyke/roundcube-vacation-plugin>

#### Installation du plugin vacation

On utilise généralement ce plugin avec le driver FTP lorsque l'on est dans un setup avec les utilisateurs en compte Unix.
Il faut donc préalablement avoir installé Proftpd, avec le module ldap `proftpd-mod-ldap` et bien charger le module dans la configuration de proftpd.

Au niveau de proftpd on utilise la configuration ldap suivante, dans `/etc/proftpd/ldap.conf` :

~~~
<IfModule mod_ldap.c>
LDAPServer ldap://localhost/??sub
LDAPBindDN "cn=admin,dc=example,dc=com" "PASSWORD"
LDAPUsers "ou=people,dc=example,dc=com" (uid=%u) (uidNumber=%u)

LDAPAuthBinds       on
</IfModule>
~~~

Au niveau de la configuration du plugin vacation, on utilise la configuration suivante dans `/etc/roundcube/plugins/vacation/config.ini` :

~~~
[default]
driver = "ftp"
;server = "localhost"
subject = "Absence / Out of office"
body = "default.txt"
;disable_forward = true


[dotforward]
binary = "/usr/bin/vacation"
flags = ""
message = ".vacation.msg"
database = ".vacation.db"
alias_identities = true
set_envelop_sender = false
always_keep_message = true
~~~

Il ne faut pas oublier également d'installer le paquet *vacation*.


#### Utilisation du plugin vacation

Voir [/HowtoPostfix#forward]() et [/HowtoPostfix#vacation]()

Attention : Il est important de préciser les alias dans l'interface, sinon le mail de vacation ne sera pas envoyé.


### Plugin password

Son utilisé résulte sur la possibilité de changer son propre mot de passe avec de nombreux connecteurs tel que dovecot, LDAP, etc.

On pourra l'activer ainsi :

~~~
$config['plugins'] = array('password');
~~~

Pour les comptes mails stockés dans LDAP il faudra installer `php7.3-ldap` et mettre en place le fichier `/etc/roundcube/plugins/password/config.inc.php` suivant :

~~~
<?php
$config=array();
$config['password_driver'] = 'ldap_simple';
$config['password_confirm_current'] = true;
$config['password_ldap_host'] = '127.0.0.1';
$config['password_ldap_userDN_mask'] = 'uid=%login,ou=people,dc=example,dc=com';
$config['password_ldap_encodage'] = 'ssha';
?>
~~~


## Import de contacts en CSV 

<https://www.pokorra.de/2017/01/import-contacts-via-csv-into-roundcube/>


## FAQ / Troubleshooting

### Mode debug

On spécifie le mode file pour sélectionner le debug que l'on souhaite activer :

~~~
$config['log_driver'] = 'file';
~~~

Les messages seront présent dans `/var/log/roundcube/`.

Activer la ou les debug de protocoles et leur verbosités :

~~~
// DEBUG
// system error reporting, sum of: 1 = log; 4 = show
$config['debug_level'] = 4;

// Log SQL queries
$config['sql_debug'] = false;

// Log IMAP conversation
$config['imap_debug'] = true;

// Log LDAP conversation
$config['ldap_debug'] = false;

// Log SMTP conversation
$config['smtp_debug'] = false;
~~~


### Mailbox doesn’t exist

Si nous avons par exemple l'erreur suivante : "UID MOVE : Mailbox doesn’t exist : Trash"

On devra activer cette option :

~~~
$config['create_default_folders'] = true;
~~~


### Problème d’envoi de pièce jointe

Il n'y a aucune erreur affichée dans l'interface web de Rouncube, mais les pièces jointes ne sont plus envoyées.

Il y a des erreurs de ce genre dans les logs de Roundcube `/var/log/roundcube/errors` lorsqu’on essaye d’envoyer une pièce jointe dans un e-mail :

```
[01-Jan-2020 00:00:00 +0200]: <abcd1234> PHP Error: compte@domaine.com can't read /tmp/rcmAttmntQ2clsS (not in temp_dir) in /usr/share/roundcube/plugins/filesystem_attachments/filesystem_attachments.php on line 216 (POST /?_task=mail&_unlock=loading1599225495530&_lang=undefined&_framed=1&_action=send)
```

Cela est causé par les versions de Roundcube sur Debian 9 (Stretch) et 10 (Bullseye). Lorsque les mises-à-jour sont gérées via apt, la mise-à-jour écrase les permissions de `/var/lib/roundcube/temp/`, ce qui a pour effet de bord de ne plus pouvoir envoyer de pièces-jointes pour les vhosts qui utilisent un AssignUserID dédié.

Le bug est corrigé dans la version 1.4.3+dfsg.1-1 du paquet roundcube (<https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=951194#26>), disponible à partir de Debian 11 (Bullseye).

Sur Debian 10 (Buster), il est possible d'utiliser un backport du paquet.
Pour les version plus anciennes, il est préférable de les mettre à niveau vers Debian 10 ou 11.

Pour solutionner temporairement le problème :

```
# chown www-roundcube:roundcube /var/lib/roundcube/temp
```


### `Erreur SMTP (220) : échec d’authentification`

Le port par défaut est maintenant 587.

Si Roundcube se connecte à Postfix en local, on peut repasser sur le port 25.

Si on a besoin de rester sur le port 587, il peut manquer les options de configuration TLS dans `/etc/roundcube/config.inc.php` :

Debian >= 12 (`smtp_server` renommé `smtp_host` et inclue le port) :

~~~
## SSL
$config['smtp_host'] = 'localhost:587';
## ou TLS
$config['smtp_host'] = 'tls://localhost:587';

$config['smtp_conn_options'] = array(
  'ssl'         => array(
    'verify_peer'  => true,
    'verify_depth' => 3,
    'cafile'       => '/etc/ssl/certs/ca-certificates.crt',
  ),
);
~~~

Debian < 12 :

~~~
## SSL
$config['smtp_server'] = 'localhost';
## ou TLS
$config['smtp_server'] = 'tls://localhost';

$config['smtp_port'] = '587';

$config['smtp_conn_options'] = array(
  'ssl'         => array(
    'verify_peer'  => true,
    'verify_depth' => 3,
    'cafile'       => '/etc/ssl/certs/ca-certificates.crt',
  ),
);
~~~

Si le certificat n'est pas valide ou auto-signé, on peut désactiver la vérification :

~~~
$config['smtp_conn_options'] = array(
  'ssl' => array(
    'verify_peer'      => false,
    'verify_peer_name' => false,
  ),
);
~~~

### Upgrade en urgence vers 1.5.8

Toutes les versions inférieures à 1.5.8 (dont les 1.4.x 1.2.x etc.) et les versions entre 1.6 inférieures à 1.6.8 sont impactées par des failles de sécurité, cf <https://github.com/roundcube/roundcubemail/releases/tag/1.5.8>

Voici une méthode pour passer des anciennes versions en 1.5.8 :

~~~
$ wget https://github.com/roundcube/roundcubemail/releases/download/1.5.8/roundcubemail-1.5.8-complete.tar.gz

# cd /var/lib
# tar xpvf roundcubemail-1.5.8-complete.tar.gz
# chown -R root: roundcubemail-1.5.8
# cd roundcubemail-1.5.8/
# chown www-data: temp/ && chmod 750 temp/
# rm -rf config/ && ln -s /etc/roundcube config
# rm -rf logs/ && ln -s ../../log/roundcube logs
~~~~

Modifier la configuration du VirtualHost, en reprenant les directives de `/etc/roundcube/apache.conf`.

Mettre à jour la base de données :

~~~
# ./bin/updatedb.sh --package=roundcube --dir=SQL
~~~ 

Vous pourriez rencontrer des erreurs diverses du type :

* `ERROR: Error in DDL upgrade 2013061000: [1060] Duplicate column name 'expires'`
* `ERROR: Error in DDL upgrade 2015111100: [1060] Duplicate column name 'failed_login'`
* `ERROR: Error in DDL upgrade 2020020101: [1071] Specified key was too long; max key length is 767 bytes`

Il faut alors adapter les fichiers `.sql` dans `SQL/mysql/` en sautant quand il y a des doublons,
ou en zappant certains problèmes notamment les indexes avec `COLLATE utf8mb4_unicode_ci`.

### Lignes tronquées lors de l'impression d'un message

Si vous cliquez sur « Imprimer ce courriel », la vue proposée peut avoir des lignes tronquées, notamment si vous zoomez.
Cela semble un bug de CSS du skin "elastic".
Un hotfix est d'éditer `/usr/share/roundcube/skins/elastic/deps/bootstrap.min.css` (attention le fichier n'a qu'une seule ligne de code)
et dans la section `@media print{` à la fin de la ligne, remplacer :

~~~
@page{size:a3}body{min-width:992px !important}.container{min-width:992px !important}
~~~

par

~~~
@page{size:a3}body{}.container{min-width:992px !important}
~~~

Cette astuce a été trouvée grâce au [ticket #7732:  Elastic: Preview of message does not wrap white spaces #7732](https://github.com/roundcube/roundcubemail/issues/7732)

Un bug a été ouvert pour ce problème : <https://github.com/roundcube/roundcubemail/issues/9677>

### Mise à jour de la base de donnée Roundcube

Pour mettre à jour la base de donnée Roundcube (si nécessaire), il faut faire la commande suivante :

~~~
# RCUBE_CONFIG_PATH=/etc/roundcube/ /usr/share/roundcube/bin/updatedb.sh --package=roundcube --dir=/usr/share/roundcube/SQL
~~~

### Impossible d'envoyer un mail avec un image dans le body du mail

roundcube fait un requète POST sur `/?_task=mail&...` qui est bloqué par `mod_security` avec un `403 Forbidden`.

Dans ce cas il faut ajouter cette exception pour le vhost roundcube :

~~~
<IfModule mod_security2.c>
    SecRequestBodyAccess Off
</IfModule>
~~~
