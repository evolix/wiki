[Mailgraph](https://mailgraph.schweikert.ch/) est un outil de statistiques mails qui produit des graphiques journaliers, hebdomadaires, mensuels et annuels.

Il est constitué principalement d'un service qui surveille les logs mails et alimente des fichiers de données au format RRD, et d'un script CGI qui trace les graphiques à la demande.


# Comment lire les graphiques de Mailgraph ?

Mailgraph produit deux catégories de graphiques par jour, semaine, mois et année : 

  1. Le graphique qui affiche les mails reçus ;
[[Image(1.png)]][[BR]]

* En bleu est affiché le nombre de messages envoyés à la minute ;
* En vert est affiché le nombre de messages reçus à la minute.

  2. Le graphique qui affiche les types de mails reçus (Bounce, virus, spam, rejected).
[[Image(2.png)]]

* En noir est affiché le nombre de « bounce » envoyés à la minute ;
* En jaune est affiché le nombre de virus reçus à la minute ;
* En gris est affiché le nombre de spams à la minute ;
* Et enfin en rouge est affiché le nombre de « rejected » à la minute.


## Quelques définitions

### Bounced mail

Il s'agit d'un mail que le serveur renvoi à l'expéditeur (en interne) lorsque que l'adresse mail dessinatrice est incorrecte (n'existe pas, inbox pleine, ...).
Par exemple un mail envoyé à jacques.dupond@gmail*l*.com renverra sûrement une erreur car cette adresse mail n'existe sûrement pas.


### Rejected mail

Il s'agit des mails rejetés par le serveur pour plusieurs raisons, par exemple un domaine ou une adresse IP blacklisté, ou bien après analyse du message par un antispam.


# Installation

## Installation CGI et intégration dans Apache

~~~bash
$ apt install mailgraph
~~~

Depuis debian 9, le script CGI a été migré de `/usr/lib/cgi-bin` vers `/usr/share/mailgraph` et il faut ajouter une configuration spécifique dans le (ou les) vhosts Apache :

~~~apache
Alias /mailgraph /usr/share/mailgraph
<Directory /usr/share/mailgraph>
    DirectoryIndex mailgraph.cgi
    Require all granted
    Options +FollowSymLinks +ExecCGI
    AddHandler cgi-script .cgi
</Directory>
~~~

Et recharger Apache :

~~~bash
$ apache2ctl -t && systemctl reload apache2
~~~

Il faut que le répertoire `/var/lib/mailgraph` appartienne à l'utilisateur `www-data` et au groupe `www-data` pour que Apache puisse généré les graphs.

~~~
drwxr-xr-x 4 www-data www-data 4096 Feb  6 09:42 /var/lib/mailgraph
~~~

## Installation sans CGI

Que ça soit pour des raisons de performance, de sécurité ou de simplicité, il est assez commun de ne pas avoir de module CGI sur un serveur (installer du CGI avec nginx est fastidieux par exemple).

Or, l’outil de stats mailgraph n’est prévu que pour tourner en CGI.

Il est possible de s’en affranchir et de générer les graphes mailgraph sans CGI.

Voici un petit script qui peut être placé en crontab, ce qui permet une sauvegarde régulière des graphes générés :

~~~
#!/bin/sh
MAILGRAPH_PATH=/usr/lib/cgi-bin/mailgraph.cgi # Debian
#MAILGRAPH_PATH=/usr/local/www/cgi-bin/mailgraph.cgi # FreeBSD
#MAILGRAPH_PATH=/usr/local/lib/mailgraph/mailgraph.cgi # OpenBSD

MAILGRAPH_DIR=/var/www/mailgraph

umask 022

mkdir -p $MAILGRAPH_DIR

$MAILGRAPH_PATH | sed '1,2d ; s/mailgraph.cgi?// ; s/src="?/src="/' > $MAILGRAPH_DIR/index.html

for i in 0-n 0-e 0-g 1-n 1-e 1-g 2-n 2-e 2-g 3-n 3-e 3-g; do
        QUERY_STRING=$i $MAILGRAPH_PATH | sed '1,3d' > $MAILGRAPH_DIR/$i
done
~~~

Testé sous Debian, FreeBSD et OpenBSD (variable MAILGRAPH_PATH à adapter).
