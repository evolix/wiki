**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Configuration de Sendmail

[Site officiel](http://www.sendmail.org/)

### Configurer le délai d'envoi des "delay mail"

Exemple sur une machine OpenBSD sans configuration sendmail spécifique :

~~~
# cd /usr/share/sendmail/cf/   
# vim openbsd-localhost.mc
~~~

Ajouter la ligne suivante pour envoyer au bout de 240h :

~~~
define(`confTO_QUEUEWARN', `240h')dnl
~~~

Puis

~~~
# mv /etc/mail/localhost.cf /etc/mail/localhost.cf.bak
# make && make install
# pkill sendmail && sendmail -L sm-mta -C/etc/mail/localhost.cf -bd -q30m
~~~

### Configurer un "smarthost" (gateway SMTP)

Même process, avec la ligne de configuration suivante :

~~~
define(`SMART_HOST',`[192.0.2.1]')dnl
~~~
