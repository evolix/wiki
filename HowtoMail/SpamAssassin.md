**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**


## SpamAssassin


### Utilisation avec SpamPD

Il est courant d'utiliser SpamAssassin par l'intermédiaire d'Amavisd. Cependant si pour diverses raisons on ne veut pas s'emcombrer d'Amavisd, il est possible d'utiliser un proxy SMTP, Spam Proxy Daemon (SpamPD) qui sera appelé par Postfix comme « content-filter ».

#### Installation

~~~
# aptitude install spampd
~~~

#### Configuration de SpamPD

Fichier _/etc/default/spampd_ :

~~~
STARTSPAMPD=1
~~~

La configuration de SpamAssassin sera utilisé, mais on peut surcharger celle-ci en spécifiant un fichier de conf spécifique à SpamPD :

~~~
ADDOPTS="--config=/etc/spampd.conf"
~~~

#### Intégration avec Postfix

Fichier _/etc/postfix/main.cf_ :

~~~
content_filter =scan:[127.0.0.1]:10025
~~~

Fichier _/etc/postfix/master.cf_ :

~~~
scan               unix    -        -        y        -        10        smtp
localhost:10026    inet    n        -        y        -        10        smtpd
  -o content_filter=
  -o local_recipient_maps=
  -o relay_recipient_maps=
  -o myhostname=filter.mynetwork.local
  -o smtpd_helo_restrictions=
  -o smtpd_client_restrictions=
  -o smtpd_sender_restrictions=
  -o smtpd_recipient_restrictions=permit_mynetworks,reject
  -o mynetworks=127.0.0.0/8
~~~

## Debug

Comprendre tout ce que fait SA sur un mail en particulier.

```
# spamassassin -D -t < /chemin/vers/un/mail
```

## RBLs

SA dispose de certaines RLBs intégrés, elles sont dans `/usr/share/spamassassin/20_dnsbl_tests.cf`.
Ces RBLs sont utilisés en rapport avec l'adresse IP de l'expéditeur.

~~~
# grep header /usr/share/spamassassin/20_dnsbl_tests.cf
header __RCVD_IN_NJABL          eval:check_rbl('njabl', 'combined.njabl.org.')
header __RCVD_IN_ZEN            eval:check_rbl('zen', 'zen.spamhaus.org.')
header RCVD_IN_SBL              eval:check_rbl_sub('zen', '127.0.0.2')
header RCVD_IN_XBL              eval:check_rbl('zen-lastexternal', 'zen.spamhaus.org.', '127.0.0.[45678]')
[…]
~~~

D'autres RBLs sont utilisés en rapport avec les liens dans le corps du message.

~~~
# grep check_rbl /usr/share/spamassassin/72_active.cf
header RCVD_IN_BRBL_LASTEXT   eval:check_rbl('brbl-lastexternal','bb.barracudacentral.org')
[…]
~~~

On pourra s'assurer d'augmenter le score d'une note tueuse si une adresse IP est dans l'une de ces RBL. Par exemple en mettant `local.cf` :

~~~
score RCVD_IN_XBL 3.0
score RCVD_IN_BRBL_LASTEXT 3.0
~~~

### Ajout d'une RBL

Dans `local.cf` ou ailleurs :

~~~
header RCVD_IN_DNSBL_INPS_DE eval:check_rbl('inps-de','dnsbl.inps.de.')
describe RCVD_IN_DNSBL_INPS_DE Received via a relay in inps.de DNSBL
tflags RCVD_IN_DNSBL_INPS_DE net
score RCVD_IN_DNSBL_INPS_DE 3.0 
~~~

Cet exemple ajoute un check RBL de dnsbl.inps.de.

Quelques RBLs en plus.

~~~
header RCVD_IN_DNSBL_INPS_DE eval:check_rbl('inps-de','dnsbl.inps.de.')
describe RCVD_IN_DNSBL_INPS_DE Received via a relay in inps.de DNSBL
tflags RCVD_IN_DNSBL_INPS_DE net
score RCVD_IN_DNSBL_INPS_DE 1.0

header RCVD_IN_DNSBL_ASCAMS eval:check_rbl('ascams','superblock.ascams.com.')
describe RCVD_IN_DNSBL_ASCAMS Received via a relay in superblock.ascams.com. DNSBL
tflags RCVD_IN_DNSBL_ASCAMS net
score RCVD_IN_DNSBL_ASCAMS 1.0

header RCVD_IN_DNSBL_CBL_ABUSEAT eval:check_rbl('cbl-abuseat','cbl.abuseat.org.')
describe RCVD_IN_DNSBL_CBL_ABUSEAT Received via a relay in cbl.abuseat.org. DNSBL
tflags RCVD_IN_DNSBL_CBL_ABUSEAT net
score RCVD_IN_DNSBL_CBL_ABUSEAT 1.0

header RCVD_IN_DNSBL_JUSTSPAM eval:check_rbl('justspam','dnsbl.justspam.org.')
describe RCVD_IN_DNSBL_JUSTSPAM Received via a relay in dnsbl.justspam.org. DNSBL
tflags RCVD_IN_DNSBL_JUSTSPAM net
score RCVD_IN_DNSBL_JUSTSPAM 1.0

header RCVD_IN_DNSBL_MCAFEE eval:check_rbl('mcafee','cidr.bl.mcafee.com.')
describe RCVD_IN_DNSBL_MCAFEE Received via a relay in cidr.bl.mcafee.com. DNSBL
tflags RCVD_IN_DNSBL_MCAFEE net
score RCVD_IN_DNSBL_MCAFEE 1.0

header RCVD_IN_DNSBL_S5H eval:check_rbl('s5h','all.s5h.net.')
describe RCVD_IN_DNSBL_S5H Received via a relay in all.s5h.net. DNSBL
tflags RCVD_IN_DNSBL_S5H net
score RCVD_IN_DNSBL_S5H 1.0

header RCVD_IN_DNSBL_SPAMCANNIBAL eval:check_rbl('spamcannibal','bl.spamcannibal.org.')
describe RCVD_IN_DNSBL_SPAMCANNIBAL Received via a relay in bl.spamcannibal.org. DNSBL
tflags RCVD_IN_DNSBL_SPAMCANNIBAL net
score RCVD_IN_DNSBL_SPAMCANNIBAL 1.0

header RCVD_IN_DNSBL_UCEPROTECT-1 eval:check_rbl('uceprotect-1','dnsbl-1.uceprotect.net.')
describe RCVD_IN_DNSBL_UCEPROTECT-1 Received via a relay in dnsbl-1.uceprotect.net. DNSBL
tflags RCVD_IN_DNSBL_UCEPROTECT-1 net
score RCVD_IN_DNSBL_UCEPROTECT-1 1.0

header RCVD_IN_DNSBL_JUNKEMAILFILTER eval:check_rbl('junkemailfilter','hostkarma.junkemailfilter.com.', '127.0.0.2')
describe RCVD_IN_DNSBL_JUNKEMAILFILTER Received via a relay in hostkarma.junkemailfilter.com. DNSBL
tflags RCVD_IN_DNSBL_JUNKEMAILFILTER net
score RCVD_IN_DNSBL_JUNKEMAILFILTER 1.0
~~~