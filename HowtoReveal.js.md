---
title: Howto Reveal.js
...

## Pour faire des slides verticaux, il suffit d'englober plusieurs sections dans une même section :

~~~{.html}
<section>
    <section>
	<h2>Vertical Slides</h2>
	<p>
            Slides can be nested inside of each other.
        </p>
    </section>
    <section>
        <h2>Basement Level 1</h2>
	<p>
            Nested slides are useful for adding additional detail underneath a high level horizontal slide.
        </p>
    </section>
</section>
~~~

## Pour intégrer image/gif en background sur un slide en particulier, il faut ajouter l'attribut data-background à la section : 

~~~{.html}
<section data-background="url-de-l-image.png">
    <h2>A slide with a image</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
</section>
~~~

Idem pour un background color :

~~~{.html}
<section data-background="#87CBE7">
    <h2>A slide with a different background-color</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
</section>
~~~

## Pour intégrer une video en background sur un slide en particulier, l'attribut a ajouter est data-background-video : 

~~~{.html}
<section data-background="url-de-la-video.webm">
    <h2>A slide with a video</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
</section>
~~~

## Pour afficher du texte ou des images de façon fragmentée grâce à la class "fragment" :

~~~{.html}
<section>
    <h2>A slide with a image</h2>
    <p class="fragment">Lorem ipsum dolor sit amet...</p>
    <p class="fragment"> ...consectetur adipiscing elit...</p> 
    <p class="fragment">...sed do eiusmod tempor incididunt ut...</p>
    </p>...labore et dolore magna aliqua.</p>
    <img class="fragment" data-src="url-de-l-image.png">
</section>
~~~

## Transformer une présentation en .pdf :

~~~
$ ~/GIT/$ git clone https://github.com/astefanutti/decktape.git
$ ~/GIT/decktape$ curl -L https://github.com/astefanutti/decktape/releases/download/v1.0.0/phantomjs-linux-x86-64 -o phantomjs; chmod +x phantomjs
$ ~/GIT/decktape$ ./phantomjs decktape.js file:///tmp/foo.html foo.pdf
~~~

Note : cette méthode ne marche plus avec les dernières version de phantomjs et/ou decktape.

### Alternative en utilisant Docker

Si vous avez Docker sur votre machine :

~~~
$ docker run -t -v ~:/home/user astefanutti/decktape <fichier HTML> slides.pdf
$ docker cp `docker ps -lq`:slides/slides.pdf .
$ docker rm `docker ps -lq`
~~~

### Alternative en utilisant Chromium

Si vous avez Chromium sur votre machine, assurez-vous que le fichier /css/print/pdf.css soit chargé (il est chargé par défaut dans l'index.html). Si vous utilisez un autre fichier html, vous pouvez ajouter la section suivante dans le HEAD :

~~~{.js}
<script>
    var link = document.createElement( 'link' );
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
    document.getElementsByTagName( 'head' )[0].appendChild( link );
</script>
~~~

* Depuis Chromium, ouvrez votre fichier html et ajoutez à la fin `?print-pdf`. Exemple : revealjs.com?print-pdf.
* Vous pouvez ajouter l'option `&showNotes=true` pour conserver les speaker notes
* Ouvrez la fenêtre d'impression en faisant `control+p`
* Assurez-vous que la destination soit « Save as PDF »
* Le mode doit être « Paysage » ou « Landscape »
* Les marges doivent être à  « None »
* Activez l'option « Background graphics »
* Sauvegardez

### Alternative en utilisant NodeJS

Débrouillez vous pour obtenir un nodejs et npm suffisamment récent puis suivre la procédure officielle : https://github.com/astefanutti/decktape#install (non testé).

## Écran du présentateur

À l'instar des logiciels de présentation traditionnels, Reveal.js permet d'avoir un « écran du présentateur ». Il est affiché sur un écran choisi lorsqu'on dispose d'au moins 2 écrans. Par exemple l'écran de votre ordinateur portable, posé sur le pupitre, alors que le vidéo-projecteur diffuse la vue normale de la présentation.

Pour disposer de l'écran du présentateur et que les 2 pages web soient synchronisées, il faut qu'elles dialoguent avec un serveur web dédié à cette synchronisation. Il s'agit d'une application en Node.js que l'on peut installer avec un simple `# npm install` depuis l'emplacement où se trouve le fichier `index.html`. Si vous ne disposez pas de Node.js sur votre poste, voici notre [Howto Node.js](/HowtoNodeJS).

Une fois les paquets Node installés, il faut lancer l'application : `# npm start`. Cela va démarrer un processus en premier plan, qui indiquera à quelle adresse on peut se connecter pour consulter la présentation. Dans la plupart des cas, il va même ouvrir l'URL dans votre navigateur par défaut.

Il faut ensuite ouvrir la page de l'écran du présentateur avec la touche `s` depuis la page de la présentation. Un nouvel onglet (ou nouvelle fenêtre) s'ouvre. Vous pouvez alors disposer vos fenêtres comme bon vous semble entre vos multiples écrans. Il vous faudra probablement ne pas activer la recopie vidéo entre vos écrans, afin d'avoir un affichage différent sur chacun d'entre eux.

Quelques caractéristiques de l'écran du présentateur :

* le bouton "Layout" permet de changer de disposition dans la page ;
* le chronomètre peut être réinitialisé en cliquant dessus ;
* les commandes de défilement (clavier, souris ou « cliqueur ») fonctionnent quelle que soit la fenêtre qui a le focus.
* pour visualiser les notes, il faut une syntaxe particulière dans les sections.

### Notes du présentateur

Il faut ajouter une balise `<aside class="notes">` dans la `<section>` voulue :

~~~.html
<section>
  <h2>Mon titre</h2>
  <p>mon contenu</p>

  <aside class="notes">
    Il n'y a <b>que</b> le présentateur qui verra ces notes.
  </aside>
</section>
~~~

Chaque diapositive peut alors avoir ses propres notes. On peut y mettre du code HTML, mais le style des notes est difficilement personnalisable.