---
categories: web
title: Howto Awstats
...

* Documentation : <https://www.awstats.org/docs/index.html>
* Statut de cette page : test / bookworm

[AWStats](https://awstats.sourceforge.io/) est un outil pour générer des statistiques en fonction d'un fichier de logs.
Il peut lire des fichiers de logs d'un serveur web (Apache, Nginx, etc.) mais aussi d'un serveur FTP, SFTP, etc.

## Installation

~~~
# apt install awstats

# awstats -version
awstats 7.8 (build 20200416)
~~~

## Configuration pour le web

Pour commencer, on va déplacer la configuration d'origine dans un autre fichier afin de pouvoir créer de multiples configurations qui se baseront sur la configuration d'origine.

~~~
# cd /etc/awstats
# grep -v awstats.conf.local awstats.conf > awstats.conf.local
~~~

Le fichier _/etc/awstats/awstats.conf.local_ doit être adapté :

~~~
LogFormat=1
AllowFullYearView=3
WarningMessages=1
ErrorMessages="An error occured. Contact your Administrator"
~~~

Ensuite, on va créer modifier le fichier /etc/awstats/awstats.conf avec le contenu suivant :

~~~
Include "/etc/awstats/awstats.conf.local"
LogFile="/var/log/apache2/access.log"
SiteDomain="hosting.example.com"
DirData="/var/lib/awstats/"
ShowHostsStats=0
ShowOriginStats=0
ShowPagesStats=0
ShowKeyphrasesStats=0
ShowKeywordsStats=0
ShowHTTPErrorsStats=0
~~~

Ce fichier supprime tous les paramètres non anonyme et permet potentiellement d'avoir des statiques globales pour tous les sites d'un serveur.

Pour un site "classique", on créera un fichier _/etc/awstats/awstats.example.conf_ ainsi :

~~~
Include "/etc/awstats/awstats.conf.local"
LogFile="/home/example/log/access.log"
SiteDomain="www.example.com"
DirData="/home/example/awstats"
~~~

Pour générer les stats de façon régulière, cela se passe dans _/etc/cron.d/awstats_ :

~~~
0,10,20,30,40,50 * * * * root  umask 033; [ -x /usr/lib/cgi-bin/awstats.pl -a -f /etc/awstats/awstats.conf -a -r /var/log/apache2/access.log ] && /usr/lib/cgi-bin/awstats.pl -config=awstats -update >/dev/null
51 * * * * root umask 033; [ -x /usr/lib/cgi-bin/awstats.pl -a -f /etc/awstats/awstats.example.conf -a -r /home/example/log/access.log ] && /usr/lib/cgi-bin/awstats.pl -config=example -update >/dev/null
~~~

Enfin, pour voir les icones Awstats, on rajoutera le fichier _/etc/apache2/cron.d/awstats.conf_ ainsi :

~~~
Alias /awstats-icon/ /usr/share/awstats/icon/
<Directory /usr/share/awstats/icon/>
Require all granted
</Directory>
~~~

## Debug

Voir la sortie comme le ferait apache :

~~~
# sudo -u vhost /usr/lib/cgi-bin/awstats.pl -config=vhost -output
~~~

Si erreurs de droits. Corriger. Exemple :

~~~
Can't locate mime.pm:   ./mime.pm: Permission non accordée at /usr/lib/cgi-bin/awstats.pl line 2217.
~~~

## Astuces

### Ré-injecter tout les logs

Il faut fournir les logs au format texte et **du plus vieux au plus récent** dans le fichier nommé access.log.


~~~
# sudo -u vhost /usr/lib/cgi-bin/awstats.pl -config=vhost -update
~~~

S'il y a un trou dans les stats, parce que la tâche cron ne s'est pas exécutée par exemple, il faut recommencer de zéro, depuis le début premier du mois par exemple.
