# IPv6

Globalement, cela fonctionne comme IPv4. Quelques différences :

* ndp (l'équivalent d'arp) utilise du multicast (vs du broadcast) pour
  la résolution des adresses mac/IPv6
* Les IP à utiliser dans la documentation sont 2001:DB8::/32
* Il y a 2^128 IP. Pour calculer le nombre d'IP dans un bloc
  2^(128-taille-du-préfixe)
* Il n'y a pas d'adresse de réseau ni de broadcast

Les tailles de bloc de bloc généralement :

* /32 : ce qui est alloué par le [RIR](https://fr.wikipedia.org/wiki/Registre_Internet_r%C3%A9gional)
* /48 : ce qui est parfois délégué aux 'end users'
* /56 : ce qui est parfois délégué aux 'end users'
* /64 : taille minimale du bloc pour avoir de l'autoconfiguration

XXX: parler des link local etc

## ROUTEUR : Autoconfiguration ou DHCPv6 sous OpenBSD

### Mettre en place rad

Le service *rad* permet d'annoncer que le routeur gère et donc route de
l'IPv6. Ainsi les clients peuvent _s'auto-configurer_ en utilisant une
adresse IPv6 composée avec le préfixe de l'adresse IPv6 du routeur
(qui est en fait l'adresse du réseau), et de l'adresse MAC de la
machine (à l'aide du processus appelé EUI-64).

Exemple :

* Le préfixe est 2001:db8:33d8:4::/64 ;
* L'adresse MAC d'une machine est 00:1e:c9:47:3e:44 ;
* Son adresse IPv6 devient 2001:db8:33d8:4:21e:c9ff:fe47:3e44/64.

### Configuration de rad

La configuration se fait dans /etc/rad.conf.
Il faut indiquer sur quelle interface faire l'annonce, suivi du
préfixe, et de la longueur de celui-ci. D'autres informations, tel
que le serveur DNS, peuvent également être obtenues par le client.
Pour le _MTU_, voir la partie [#ipv6-et-pppoe]().

~~~
interface re0 {
  prefix 2001:db8::/32
  dns {
    nameserver 2001:db8::1
    nameserver 2001:db8::2
    search example.com
  }
  mtu 1500
}
~~~

### WIDE-DHCPv6

Description du protocole DHCPv6 à faire...

_WIDE-DHCPv6_ est un serveur DHCPv6 développé par [KAME](http://www.kame.net/).
Pour l'installer utiliser _pkg_add_.

Pour diffuser un serveur DNS en IPv6, modifier le fichier
/etc/dhcp6s.conf en indiquant l'adresse IPv6 du serveur DNS :


    option domain-name-servers 2001:7a8:33d8:14:11:11ff:fecc:cf54;


## Client wide-dhcpv6-client

C'est un client qui permet d'interroger un serveur DHPCv6, pour
l'installer sous Debian :


    # apt install wide-dhcpv6-client


Ensuite il faut configurer lors de l'installation ou dans le fichier
`/etc/default/wide-dhcpv6-client` l'interface sur laquelle écouter.

## IPv6 et PPPoE

Lorsqu'on utilise PPPoE il est nécessaire de diffuser un _MTU_ de 1492
dans la configuration de rtadvd, sinon les paquets ne traverseront par
le lien en PPPoE.

Voici un exemple de trame diffusé lorsque le routeur annonce qu'il gère l'IPv6.


    No.     Time        Source                Destination           Protocol Info
       2076 214.592925  fe80::200:24ff:fec9:6b68 ff02::1               ICMPv6   Router advertisement

    Frame 2076 (118 bytes on wire, 118 bytes captured)
    Ethernet II, Src: Olicom_c9:6b:68 (00:00:24:c9:6b:68), Dst: IPv6mcast_00:00:00:01 (33:33:00:00:00:01)
    Internet Protocol Version 6
        0110... = Version: 6
    ... 0000 0000............... = Traffic class: 0x00000000
    ......... 0000 0000 0000 0000 0000 = Flowlabel: 0x00000000
        Payload length: 64
        Next header: ICMPv6 (0x3a)
        Hop limit: 255
        Source: fe80::200:24ff:fec9:6b68 (fe80::200:24ff:fec9:6b68)
        Destination: ff02::1 (ff02::1)
    Internet Control Message Protocol v6
        Type: 134 (Router advertisement)
        Code: 0
        Checksum: 0xa084 [correct]
        Cur hop limit: 64
        Flags: 0x40
            0...... = Not managed
            .1..... = Other
            ..0.... = Not Home Agent
    ...0 0... = Router preference: Medium
        Router lifetime: 1800
        Reachable time: 0
        Retrans timer: 0
        ICMPv6 Option (Source link-layer address)
            Type: Source link-layer address (1)
            Length: 8
            Link-layer address: 00:00:24:c9:6b:68
        ICMPv6 Option (MTU)
            Type: MTU (5)
            Length: 8
            MTU: 1492
        ICMPv6 Option (Prefix information)
            Type: Prefix information (3)
            Length: 32
            Prefix length: 64
            Flags: 0xc0
                1...... = Onlink
                .1..... = Auto
                ..0.... = Not router address
    ...0... = Not site prefix
            Valid lifetime: 2592000
            Preferred lifetime: 604800
            Prefix: 2001:7a8:33d8:4::


## Configuration cliente sous OpenBSD

### Autoconfiguration

Cela va rajouter une adresse link local en ipv6 (en fe80::/10),
puis il va obtenir deux adresses IP, une liée à l'adresse mac (par
exemple avec la mac c8:5b:76:90:5b:35 on obtient
2001:db8:37:129:ca5b:76ff:fe90:5b35) et une autre adresse avec un flag
« autoconfprivacy » qui n'aura aucun lien avec l'adresse mac. C'est
cette dernière qui sera utilisée.

#### Configuration en dur

Dans le fichier /etc/hostname.interface correspondant à votre
interface, il suffit de rajouter le mot clé *rtsol* :


    dhcp
    rtsol

#### Configuration (temporaire) en ligne de commande

    doas ifconfig em0 inet6 autoconf

### Configuration en IP fixe

#### Configuration en dur

Éditer le fichier /etc/hostname.interface correspondant à votre
interface et indiquez l'adresse IPv6 après le mot-clé *inet6* suivie
du préfixe, ici 64. Ce qui donne par exemple :


    inet <IPv4> <masque-IPv4>
    inet6 2001:db8:6666:333:123:45ff:fe1d:3456 64


Pour la passerelle il faut éditer le fichier /etc/mygate et ajouter
une ou plusieurs passerelles (IPv4 et IPv6) :

    192.0.2.1
    2001:db8:42::1

#### Configuration (temporaire) en ligne de commande

    # ifconfig sis0 inet6 2001:db8:42::42/64
    # route add -inet6 default 2001:db8:42::1


### Préférer l'IPv6 ou l'IPv4

Pour préférer l'IPv6, dans `/etc/resolv.conf` il faut mettre (à la
fin) :

    family inet6 inet4

Sinon, l'IPv4 est préférée par défaut.

## Configuration cliente sous Debian

### Configuration en dur

Editer le fichier */etc/network/interfaces* :


    auto lo
    iface lo inet loopback

    auto eth0
    iface eth0 inet static
            address <IPv4>/<masque-IPv4-en-bits>
            gateway <gateway-IPv4>

    iface eth0 inet6 static
        address 2001:db8:42::42/64
        gateway 2001:db8:42::1


### Configuration (temporaire) en ligne de commande


    # ifconfig eth0 add 2001:db8:42::42/64
    # ip route add default via 2001:db8:42::1


_Note : La commande route est fourni via le paquet `iproute`_

### Préféfer l'IPv4 à l'IPv6 lors de la résolution DNS

Mettre ceci dans /etc/gai.conf


    precedence ::ffff:0:0/96  100

## Afficher/manipuler la table d'entrée IPv6 - adresse mac

### OpenBSD

Il y a ndp(8). Pour afficher la table :

    ndp -s

### Debian

Il y a la commande ip. Pour afficher la table :

    ip -6 n

## Avertissements

### Debian, kvm et les bridges

Il se peut que le bridge sur l'hyperviseur ne laisse pas passer le
multicast utilisé par le NDP. Il faut activer le *snooping multicast*.

En CLI :

    echo 0 > /sys/class/net/br0/bridge/multicast_snooping

Dans /etc/network/interfaces :

    auto br0
        iface br0 inet manual
        bridge_ports eth0
        up echo 0 > /sys/class/net/br0/bridge/multicast_snooping

## Désactivation IPv6

### Debian

Pour désactiver *définitivement* l'IPv6 sur toutes les interfaces :

~~~(sh)
# printf 'net.ipv6.conf.all.disable_ipv6 = 1\nnet.ipv6.conf.default.disable_ipv6 = 1\n' > /etc/sysctl.d/zzz-disable-ipv6.conf
# sysctl -p /etc/sysctl.d/zzz-disable-ipv6.conf
~~~

Pour désactiver *temporairement* l'IPv6 sur toutes les interfaces :

~~~(sh)
# sysctl -w net.ipv6.conf.all.disable_ipv6=1 net.ipv6.conf.default.disable_ipv6=1
~~~

## Réactivation IPv6

### Debian

Pour réactiver l'IPv6 sur toutes les interfaces, suite à une désactivation temporaire :

~~~(sh)
# sysctl -w net.ipv6.conf.all.disable_ipv6=0 net.ipv6.conf.default.disable_ipv6=0
# systemctl restart networking.service
~~~
