---
title: Howto Urxvt
...


* Site officiel : <http://software.schmorp.de/pkg/rxvt-unicode.html>
* Documentation : <http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.1.pod>

Urxvt (ou Rxvt-unicode) est un émulateur de terminal, conçu pour avoir un cœur minimaliste et de très riches capacités d'extension pour s'adapter à tous les besoins sans surcharger le logiciel pour tout le monde.

## Installation

~~~
# apt install rxvt-unicode
~~~

Il existe aussi une variante "256 couleurs" avec le paquet `rxvt-unicode-256color`.

~~~
$ rxvt-unicode --help
rxvt-unicode (urxvt) v9.20 - released: 2014-04-26
[…]
~~~


## Configuration

La configuration par défaut propose très peu de fonctionnalités.

Pour sa configuration, il utilise le fichier `~/.Xresources`.

Le système d'extension est basé sur Perl et pour en activer il faut les ajouter à `URxvt.perl-ext-common`

### Défilement

Par défaut, le buffer défile avec la sortie des commandes. Il est possible de l'interrompre lors de l'utilisation des touches PageUp/PageDown ou avec la molette de la souris.

~~~
! ne pas défiler avec la sortie
URxvt*scrollTtyOutput: false

! défilement avec le buffer (molette souris ou PageUp/PageDown)
URxvt*scrollWithBuffer: true

! revenir en bas en appuyant sur une touche
URxvt*scrollTtyKeypress: true
~~~

### Taille du texte

Avec le plugin `resize-font` il est possible de faire varier la taille du texte avec des raccourcis clavier. Ici par exemple, les touches `Ctrl -` pour diminuer, `Ctrl +` pour augmenter… :

~~~
URxvt.perl-ext-common:    resize-font

URxvt.keysym.C-minus:     resize-font:smaller
URxvt.keysym.C-plus:      resize-font:bigger
URxvt.keysym.C-equal:     resize-font:reset
URxvt.keysym.C-question:  resize-font:show
~~~

### Définir ce terminal par défaut

Qu'importe l'environnement de bureau que vous utilisez, on peut la définir via la commande suivante :

~~~
$ sudo update-alternatives --config x-terminal-emulator
~~~

Dans le cas de i3, on peut adapter la configuration (~/.i3/config) ainsi :

~~~
bindsym $mod+Return exec "i3-sensible-terminal"
~~~

### Exemple 

Fichier de configuration que les sysadmin utilisent :

~~~
URxvt.font: xft:monospace:size=10
URxvt*background: [85]#000000
URxvt*foreground: [100]#ffffff
URxvt*scrollBar: false
URxvt*saveLines:  10000
URxvt.perl-ext-common: default,matcher
URxvt.urlLauncher: firefox
URxvt.urgentOnBell: true

##Fond transparent
URxvt.inheritPixmap: true
##URxvt.tintColor: #00000
URxvt.shading: 25

!Pnevma1
*color0: #2F2E2D
*color8: #4A4845
*color1: #A36666
*color9: #D78787
*color2: #8FA57E
*color10: #A9BA9C
*color3: #D7AF87
*color11: #E4C9AF
*color4: #7FA5BD
*color12: #A1BDCE
*color5: #C79EC4
*color13: #D7BEDA
*color6: #8ADBB4
*color14: #B1E7DD
*color7: #D0D0D0
*color15: #EFEFEF
~~~