**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto NetApp

<http://users.cis.fiu.edu/~tho01/psg/netapp.html>

<https://communities.netapp.com/welcome>

<http://blog.tmartin.fr/tag/netapp>

<http://www.wafl.co.uk/options/>

<http://www.datadisk.co.uk/html_docs/netapp/netapp.htm>

## Configuration minicom pour accéder à la carte BMC

/etc/minicom/minirc.netapp :

~~~
pu port             /dev/ttyS0
pu baudrate         9600
pu bits             8
pu parity           N
pu stopbits         1
pu rtscts           No
~~~

## Configuration NetApp

La commande `options` permet de voir, mais aussi changer à chaud les paramètres !

~~~
# Pour voir toutes les options
netapp> options
auditlog.enable              on         
[...]

# Pour voir certaines options :
netapp> options autosupport
netapp> options cifs
netapp> options disk
netapp> options ftpd
netapp> options <httpd>
netapp> options ip
netapp> options iscsi
netapp> options ldap
netapp> options nfs
netapp> options raidsecurity
netapp> options sftp
netapp> options snapmirror
netapp> options ssh
netapp> options timed
netapp> options vol
netapp> options wafl
etc.

# Pour voir une option particulière
netapp> options ip.v6.enable
ip.v6.enable                 off

# Certaines options "dangereuses" nécessitent de passer dans un mode avancé...
netapp> priv set advanced
# ...et pour revenir dans un mode normal
netapp> priv set admin

# Pour changer une option particulière
netapp> options ip.v6.enable on
~~~

Pour information, toutes les options sont stockées dans le fichier `/etc/registry`

Une fois changée, l'option est immédiatement changée ! Son action est donc immédiate, ou
parfois il faut redémarrer un service. Dans tous les cas, il n'est donc pas nécessaire d'exécuter
une commande supplémentaire pour les conserver au prochain reboot.

Enfin, certains paramètres se configurent via des fichiers (`resolv.conf`, etc.) et l'on rappelle
qu'il est fastidieux d'éditer un fichier directement en console (commande `wrfile`).
Une solution est d'avoir un montage en écriture NFS du vol0...

## Administration réseau

<http://datadisk.co.uk/html_docs/netapp/netapp_network.htm>

* Lister les interfaces réseau / routes :

~~~
netapp> ifconfig -a
netapp> route -s
~~~

* Configuration IP "temporaire" :

~~~
netapp> ifconfig bond0 192.0.2.3 netmask 255.255.255.128
netapp> route add default 192.0.2.127 1

netapp> ifconfig bond0 alias 10.0.0.3 netmask 255.255.255.0 # ajouter un alias
netapp> ifconfig bond0 -alias 10.0.0.3 # supprimer un alias
~~~

* Configuration IP "en dur" :

Cela se positionne dans le fichier `/etc/rc` :

~~~
netapp> rdfile /etc/rc
hostname myhostname
ifconfig e0a 192.0.2.3 netmask 255.255.255.128 mtusize 1500
route add default 192.0.2.127 1
routed off
options dns.domainname example.local
options dns.enable on
options nis.enable off
savecore
~~~

Note : on rencontrera parfois une configuration mentionnant `ifconfig e0a `hostname`-e0a`
ce qui est tout à fait possible avec un fichier `/etc/hosts` du type :

~~~
netapp> rdfile /etc/hosts
127.0.0.1 localhost localhost-stack
127.0.10.1 localhost-10 localhost-bsd
127.0.20.1 localhost-20 localhost-sk
192.0.2.3 myhostname-e0a
~~~

* Configuration DNS

~~~
netapp> rdfile /etc/resolv.conf
netapp> options dns.enable on
netapp> options dns.domainname example.local

netapp> dns info
~~~

* Stats réseau :

~~~
netapp> ifstat bond0  

-- interface  bond0  (6 hours, 48 minutes, 21 seconds) --

RECEIVE
 Total frames:      857k | Frames/second:       5  | Total bytes:     61585k
 Bytes/second:      440  | Multi/broadcast: 40625
TRANSMIT
 Total frames:      314k | Frames/second:       4  | Total bytes:      1943m
 Bytes/second:      348  | Multi/broadcast: 14597
~~~

* Création d'un bonding (aggrégat d'interface réseau) simple :

~~~
netapp> ifgrp create bond0 single e0a e0c
netapp> ifconfig bond0 192.0.2.3 netmask 255.255.255.128 mtusize 1500
~~~

Note : à positionner dans le fichier `/etc/rc`.

* Création d'un bonding LACP :

~~~
netapp> ifgrp create bond0 lacp e0a e0c
~~~

* Création d'un "double-bonding" avec priorisation (utile pour avoir 2x 2 interfaces sur 2 switchs distincts) :

~~~
netapp> ifgrp create bond-foo lacp e0a e0c
netapp> ifgrp create bond-bar lacp e0b e0d
netapp> ifgrp create bond0 single bond-foo bond-bar
netapp> ifgrp favor bond-foo
~~~

* Diagnostics divers :

~~~
netapp> ping <IP>
netapp> traceroute <IP>
netapp> arp -a
~~~

* VLAN : commande `vlan`

## Administration système

* Version de l'OS (Data ONTAP) :

~~~
netapp> version
NetApp Release 8.1 7-Mode: Thu Mar 29 13:56:17 PDT 2012
~~~

* Uptime:

~~~
netapp> uptime
  9:01pm up  5:09 88 NFS ops, 53 CIFS ops, 0 HTTP ops, 0 FCP ops, 0 iSCSI ops
~~~

* Aide :

~~~
netapp> help
netapp> <COMMAND> help
~~~

* Date :

~~~
netapp> date
Mon Sep  3 20:53:39 CEST 2012
~~~

* Activer/changer le(s) serveur(s) NTP :

~~~
netapp> options timed.enable on
netapp> options timed.servers 192.0.2.1
~~~

* Accès à l'admin HTTP FilerView via : <http://filer/na_admin/>
* Afficher le contenu d'un fichier (équivalent de `cat`) :

~~~
netapp> rdfile <filename>
~~~

* Écrire un fichier (ATTENTION CELA EFFACE LE CONTENU EXISTANT) :

~~~
netapp> wrfile <filename>
~~~

* Lister le contenu d'un répertoire (attention, plus compliqué !) :

~~~
netapp> priv set advanced
netapp> ls <directory>
~~~

* Paramètres pour l'autosupport (envoi d'emails hebdomadaires récpitulatifs) :

~~~
netapp> options autosupport.enable on
netapp> options autosupport.cifs.verbose on
netapp> options autosupport.mailhost 192.0.2.1
netapp> options autosupport.to test@example.com
netapp> options autosupport.support.transport smtp
~~~

* Forcer l'envoi d'un email récapitulatif :

~~~
netapp> options autosupport.doit test
~~~

* Statistiques diverses :

~~~
netapp> stats show
netapp> cifs stat
netapp> nfs stat
~~~

## Administration des storages

* Lister la taille des volumes existants (dont snapshot) :

~~~
netapp> df -h
Filesystem               total       used      avail capacity  Mounted on
/vol/vol0/               151GB     4784MB      146GB       3%  /vol/vol0/
/vol/vol0/.snapshot     8140MB      668MB     7471MB       8%  /vol/vol0/.snapshot
/vol/foo/             9728GB      364MB     9727GB       0%  /vol/foo/
/vol/foo/.snapshot      512GB     1533MB      510GB       0%  /vol/foo/.snapshot
~~~

* Lister les options des volumes existants :

~~~
netapp> vol status
         Volume State           Status            Options
           vol0 online          raid_dp, flex     root, create_ucode=on
                                64-bit            
            foo online          raid_dp, flex     create_ucode=on
                                64-bit            
~~~

* Lister les QTREE existants :

~~~
netapp> qtree status
Volume   Tree     Style Oplocks  Status   
-------- -------- ----- -------- ---------
vol0              unix  enabled  normal   
foo               unix  enabled  normal   
foo      bar      unix  enabled  normal   
foo      commun   unix  enabled  normal   
foo      partages unix  enabled  normal   
foo      users    unix  enabled  normal   
~~~

* Lister les exports NFS :

~~~
netapp> exportfs
/vol/foo/bar -sec=sys,rw,root=192.0.2.1:192.0.2.2,nosuid
/vol/vol0 -sec=sys,ro,rw=192.0.2.1:192.0.2.2,root=192.0.2.1:192.0.2.2,nosuid
~~~

* Stats NFS :

~~~
netapp> nfsstat
~~~

* Modifier les exports NFS :

Éditer le fichier `/etc/exports` puis :

~~~
netapp> exportfs -a
~~~

* Lister les exports CIFS :

~~~
netapp> cifs shares
Name         Mount Point                       Description
----         -----------                       -----------
ETC$         /etc                              Remote Administration
                        BUILTIN\Administrators / Full Control
C$           /                                 Remote Administration
                        BUILTIN\Administrators / Full Control
[...]
~~~

* Lister les sessions CIFS ouvertes :

~~~
netapp> cifs sessions
[...]
~~~

* Créer un partage CIFS :

~~~
netapp> cifs shares -add test /vol/foo/bar
netapp> cifs access -delete test everyone  
1 share(s) have been successfully modified
netapp> cifs access test -g unixgroup "Full Control"
1 share(s) have been successfully modified
netapp> cifs access test -g DOMAIN\wingroup "Full Control"
1 share(s) have been successfully modified
~~~

* Lister les snapshots d'un volume :

~~~
netapp> snap list vol0
Volume vol0
working...

  %/used       %/total  date          name
----------  ----------  ------------  --------
  2% ( 2%)    0% ( 0%)  Sep 03 20:00  hourly.0       
  4% ( 2%)    0% ( 0%)  Sep 03 16:00  hourly.1       
  6% ( 2%)    0% ( 0%)  Sep 03 12:11  hourly.2       
  7% ( 2%)    0% ( 0%)  Sep 03 08:00  hourly.3       
  9% ( 2%)    0% ( 0%)  Sep 03 00:01  nightly.0      
 11% ( 2%)    0% ( 0%)  Sep 02 20:00  hourly.4       
 12% ( 2%)    0% ( 0%)  Sep 02 16:00  hourly.5       
 15% ( 3%)    0% ( 0%)  Sep 02 00:00  nightly.1      
~~~

## Administration LDAP

Il s'agit ici d'aller récupérer les utilisateurs sur un annuaire LDAP distant.

~~~
netapp> options ldap

netapp> options ldap.base dc=example,dc=com
netapp> options ldap.name cn=netapp,dc=example,dc=com
netapp> options ldap.passwd ******
netapp> options ldap.servers 192.0.2.1
netapp> options ldap.enable on
netapp> options ldap.usermap.enable on  # pour gérer des associations login windows <=> login UNIX via /etc/usermap.cfg

netapp> priv set advanced
netapp> getXXbyYY getpwbyname_r <myuser>
netapp> priv set admin
netapp> wcc -u <myuser>   # liste des infos Unix sur un compte
netapp> wcc -s <myuser>   # liste des infos Windows sur un compte
~~~

Utilisation de 2 serveurs LDAP (failover) :

~~~
netapp> options.ldap.servers=192.0.2.1 192.0.2.2
~~~

## Gestion des permissions pour Unix/Windows/MAC

Sous NetApp, il existe plusieurs types de permissions : ntfs, unix ou mixed.
On les positionne sur un qtree et c'est un point très important pour la gestion des droits.

<http://www.netapp.com/us/library/technical-reports/tr-3490.html>

<http://www.netapp.com/us/library/technical-reports/tr-3771.html>

### Mode Unix

Le NetApp stocke les droits en mode rwxrwxrwx ; c'est évidemment adapté pour des accès
via NFS depuis des clients Linux, mais également pour des accès via CIFS si les droits
restent pensés en mode Unix : permissions uniquement pour un utilisateur ou un groupe
(dans lequel seront tous les utilisateurs nécessitant des accès).

Via CIFS, les utilisateurs/groupes doivent être "mappés" (via LDAP ou manuellement dans le fichier
[usermap.cfg](http://www.wafl.co.uk/usermapcfg/))

### Mode NTFS

Le NetApp stocke les droits en mode NTFS uniquement. Toutes les opérations réalisées sous Linux/Unix (en NFS) seront "ignorées" :

~~~
# ls -l
drwxrwxrwx 6 root root      4096 Oct 19 10:57 data
# chown foo:bar data
# cmod 750 data
# ls -l
drwxrwxrwx 6 root root      4096 Oct 19 10:57 data
~~~


### Mode Mixed

Le NetApp peut stocker les droits en mode Unix OU en mode Windows : attention, c'est l'un OU l'autre ! (cette notion est importante).

Ce mode est parfois déconseillé car il peut devenir un véritable casse-tête si l'on s'organise mal.
Il permet néanmoins une certaine compatibilité lorsque l'on utilise des clients NFS ET CIFS.

Voici les avantages par rappport au mode NTFS :

* dans un Qtree, pouvoir avoir certains répertoires avec des ACLs NTFS, et d'autres répertoires avec des droits pur Unix
* si il y a une migration de données depuis un serveur Linux/Unix, les droits rwxrwxrwx seront conservés
* pouvoir agir sur les données depuis Linux/Unix, par exemple pour réinitialiser des ACLs Windows (chown/chmod -R),
   alors que ce n'est pas possible en mode NTFS (le NetApp ignorera "lâchement" les commandes envoyées).

Voici comment nous l'utilisons :

* Partager le qtree Mixed (CIFS) en mettant des droits "Full Control" pour everyone (ou DOMAIN\Utilisa. du domaine)
* Sous Windows, avec un utilisateur du domaine, on crée un répertoire en ajustant les ACLs sur les utilisateurs/groupes
   et en supprimant les droits pour everyone (en désactivant les droits hérités du parent dans la gestion avancée des droits)
* sur le répertoire du qtree, on met les droits Unix suivants : chgrp 512 + chmod 775 afin de permettre aux Administrateurs
   du domaine de créer des répertoires avec les ACLs kivonbiens, et de permettre au reste du monde de le traverser mais sans pouvoir
   créer de fichiers/répertoires à la racine du partage.

Notes :
* En NFS, les droits visibles des fichiers/répertoires avec des ACLs NTFS sont farfelus (700, 777, etc.)... mais on peut se contenter de les ignorer : ça marche !
* En CIFS, les droits visibles des fichiers/répertoires avec des droits Unix sont farfelus (owner, root, etc.)... mais on peut se contenter de les ignorer : ça marche !
* ATTENTION, modifier les droits sous Linux/Unix (en NFS) revient à perdre les ACLs positionnés sous Windows
* Sauf cas très particuliers, on doit éviter de toucher aux droits sous Linux/Unix (car ça peut devenir un casse-tête si l'on est dans un répertoire avec des ACLs NTFS)
* Sous Windows, il faut parfois (souvent!) "forcer" les droits via la gestion avancée des droits
* Si l'on veut passer la sécurité de fichiers/répertoires d'Unix à NTFS, 777 ne suffit pas, il faut mettre le bon propriétaire (chown/chgrp) sous Unix afin d'avoir les droits de positionner les ACLs sous Windows avec l'utilisateur correspondant (à confirmer)

Options diverses :

* Export NFS : option _root=IP_ permet d'avoir un accès root depuis certains postes (dangereux... mais utile)
* Option _cifs.nfs_root_ignore_acl_ permet de donner un accès total à root sans tenir compte des ACLs (utile ! mais rappel: éviter de modifier les droits en root pour le mode Mixed)
* Option _cifs.preserve_unix_security_ permet de considérer les droits Unix comme prioritaire et de les afficher dans les droits avancés sous Windows !
* Options _wafl_ (Write Anywhere File Layout) intéressantes : default_nt_user, default_qtree_mode, default_security_style, default_unix_user, group_cp, nt_admin_priv_map_to_root, root_only_chown

Pour voir quel est le mode de sécurité d'un fichier/répertoire et ses droits associés sur NetApp :

~~~
netapp> fsecurity show -v vol <path>
[/vol/vol/path - Directory (inum 17388)]
  Security style: Mixed
  Effective style: Unix

  DOS attributes: 0x0010 (----D---)

  Unix security:
    uid: 0 (root)
    gid: 512 (smbadmins)
    mode: 0775 (rwxrwxr-x)

  No security descriptor available.
netapp> fsecurity show -v vol <path>
[/vol/vol/path - Directory (inum 17392)]
  Security style: Mixed
  Effective style: NTFS

  DOS attributes: 0x0030 (---AD---)

  Unix security:
    uid: 10072 (foo)
    gid: 512 (smbadmins)
    mode: 0777 (rwxrwxrwx)

  NTFS security descriptor:
    Owner: BUILTIN\Administrators
    Group: DOMAINE\Administrateurs du domaine
    DACL:
      Allow - Everyone - 0x001200a9 (Read and Execute) - OI|CI
      Allow - DOMAINE\foo - 0x001f01ff (Full Control) - OI|CI
      Allow - DOMAINE\bar - 0x001301bf (Modify) - OI|CI
~~~

## FAQ

J'essaye de me connecter en SSH au NetApp et j'obtiens :

~~~
debug1: Authentication succeeded (password)
Connection to 1.2.3.4 closed.
~~~

Une seule connexion SSH est autorisée par défaut.
Soit il y a déjà une connexion en cours, soit une précédente connexion a mal été fermée (il faut attendre environ 15 minutes pour le timeout).
