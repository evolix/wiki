---
title: Howto Benchmarks
...

# Howto Benchmarks

**ATTENTION, CES TESTS NE DOIVENT PAS ÊTRE EFFECTUÉS SUR UN SERVEUR EN PRODUCTION**

## Tests généraux

### stress-ng

<https://github.com/ColinIanKing/stress-ng>

Installation sous OpenBSD :

~~~
# pkg_add stress
~~~

Installation sous Debian :

~~~
# apt install stress
~~~

***Il est important de noter que la commande est invoquée par `stress-ng` sous Debian et `stress` sous OpenBSD.***

C'est un outil générique de « stress-test » qui va solliciter différentes parties d'un serveur : CPU, RAM, disques…

Exemple pour exécuter tous les tests séquentiellement (chacun pendant 10 minutes), en exploitant tous les CPU disponibles.

~~~
# stress-ng --sequential 0 -t 10m
~~~

Utilisation classique :

~~~
Sous Debian :
# stress-ng -<Arg> <Nombre_dinstances>
Sous OpenBSD :
# stress -<Arg> <Nombre_dinstances>
~~~

Commandes Utiles:


| Argument                   |Description                                                                                         |
| ---------------            |----------------------------------------------------------------------------------------------------|
| `-c` `--cpu`     *\<X\>*   | Stresse le cpu en forkant X processus menant une tâche de calcul.                                  |
| `-m` `--vm`      *<\X\>*   | X processus effectuant plusieurs ```malloc()``` et ```free()``` afin de stresser la RAM.           |
| `-i` `--io`      *\<X\>*   |   X processus tournant ```sync()```. [Manuel ici.](http://manpagesfr.free.fr/man/man8/sync.8.html) |
| `-d` `--hdd`     *\<X\>*   | X processus effectuant écritures et lectures sur disque (Taille de block par défaut ***1GB***)     |
| `-t` `--timeout` *\<T\>* s | Si aucune réponse du processus, tuer ce dernier au bout de *T* secondes. (***Important:*** toujours lancer les tests en utilisant ce flag afin de ne pas Bloquer votre VM [La charge de travail peut faire crash votre machine, la rendant hors de contrôle.]).                                                                           |
|`--vm-bytes`      *\<N\>*   | Taille des blocs alloués par ```malloc()```. Defaut : ***256M***                                   |
|`--vm-strand`     *\<N\>*   | Taille fragmentation des blocks de test en écriture ```malloc()```. Defaut : ***4096M***           |
|`--vm-keep`       *\<T\>* s |                                                                                                    |
|`--vm-hang`       *\<T\>* s | Temps d'attente avant d'executer ```free()```.                                                     |
|`--hdd-bytes`     *\<X\>*   | Ecriture de X octets par *worker*. Defaut : ***1GB***.                                             |


Voir <https://www.cyberciti.biz/faq/stress-test-linux-unix-server-with-stress-ng/> pour plus d'options et de détails.

## Tests sur les performances du disque

### dd

On essaye plus ou moins de désactiver le cache mémoire pour l'écriture du filesystem
(en fait, le cache mémoire se remplit quand même pour optimiser une éventuelle lecture !) :

~~~
# echo 3 > /proc/sys/vm/drop_caches
# sysctl -w vm.dirty_background_ratio=0
# sysctl -w vm.dirty_ratio=0
~~~

Note importante : ces paramètres sont à activer avec précaution : ils peuvent dégrader les performances...
même pour les tests (en provoquant une sorte de yoyo en terme de vitesse d'accès). L'idéal est de tester via
un volume beaucoup plus grand que le taille de la mémoire du serveur.

Performances en écriture :

Un test simple est d'écrire un très gros fichier via la commande _dd_.

~~~
# time dd if=/dev/zero bs=4096 count=52398080 2>/tmp/out | pv > 200G ; cat /tmp/out
 200GB 0:24:40 [ 138MB/s] [  <=>                     ]

real    24m40.378s
user    0m35.902s
sys     11m17.146s
52398080+0 records in
52398080+0 records out
214622535680 bytes (215 GB) copied, 1480.38 s, 145 MB/s
~~~

Note : la combinaison des options bs/count n'a apparemment que peu d'influence sur les résultats.

Et en lecture :

~~~
# echo 2 > /proc/sys/vm/drop_caches
# time dd if=200G bs=4096 count=52398080 2>/tmp/out | pv > /dev/null ; cat /tmp/out
 200GB 0:09:13 [ 370MB/s] [ <=>                 ]

real    9m13.131s
user    0m39.322s
sys     9m6.602s
52398080+0 records in
52398080+0 records out
214622535680 bytes (215 GB) copied, 553.056 s, 388 MB/s
~~~

Et en effacement :

~~~
# time rm /home/32gb

~~~

### cryptsetup

~~~
# cryptsetup benchmark
# Tests are approximate using memory only (no storage IO).
PBKDF2-sha1      1024000 iterations per second
PBKDF2-sha256     668734 iterations per second
PBKDF2-sha512     344926 iterations per second
PBKDF2-ripemd160  624152 iterations per second
PBKDF2-whirlpool  182044 iterations per second
#  Algorithm | Key |  Encryption |  Decryption
     aes-cbc   128b   506.0 MiB/s  2019.3 MiB/s
 serpent-cbc   128b    78.3 MiB/s   272.1 MiB/s
 twofish-cbc   128b   166.4 MiB/s   321.7 MiB/s
     aes-cbc   256b   405.2 MiB/s  1497.9 MiB/s
 serpent-cbc   256b    81.7 MiB/s   272.2 MiB/s
 twofish-cbc   256b   169.5 MiB/s   321.5 MiB/s
     aes-xts   256b  1684.1 MiB/s  1688.8 MiB/s
 serpent-xts   256b   279.3 MiB/s   268.1 MiB/s
 twofish-xts   256b   311.6 MiB/s   317.7 MiB/s
     aes-xts   512b  1304.0 MiB/s  1304.6 MiB/s
 serpent-xts   512b   280.2 MiB/s   268.3 MiB/s
 twofish-xts   512b   315.6 MiB/s   317.7 MiB/s
~~~

### Bonnie++

Bonnie++ est un utilitaire qui permet de faire un bench sur les disques en écrite/lecture de façon différente, par bloc, et par en mode « random ».

Une bonne explication : <http://www.googlux.com/bonnie.html>

Un exemple qui test le disque avec un fichier de 6Go ` bonnie++ -d ./ -s 6144`:

~~~
Version  1.96       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
client           6G   607  99 50366  13 22989   4  2738  92 57663   7 109.1   5
Latency             30503us     736ms     496ms   41723us   52395us     841ms
Version  1.96       ------Sequential Create------ --------Random Create--------
client              -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 20037  41 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency             21643us    2840us    4224us    1399us     481us    2791us
~~~

### iozone

~~~
$ iozone -T -t 4 -s 16g -r 256k -T -I -i0 -i1 -i2
~~~

Crée 4 threads, dont chacun écrit et lit dans un fichier de 16Go, avec un buffer de 256Ko, utilisant O_DIRECT, pour désactiver le cache.

~~~
$ iozone -a -I -b test.ods
~~~

Fais tout les tests iozone en désactivant le cache, et en créant en fichier ODS pour faire un graphique des résultats, exemple :

### pg_test_fsync

~~~
# aptitude install postgresql-contrib
~~~

~~~
$ usr/lib/postgresql/9.2/bin/pg_test_fsync
2000 operations per test
O_DIRECT supported on this platform for open_datasync and open_sync.

Compare file sync methods using one 8kB write:
(in wal_sync_method preference order, except fdatasync is Linux's default)
        open_datasync                                 n/a
        fdatasync                        3806.232 ops/sec
        fsync                            3096.090 ops/sec
        fsync_writethrough                            n/a
        open_sync                        3523.832 ops/sec

Compare file sync methods using two 8kB writes:
(in wal_sync_method preference order, except fdatasync is Linux's default)
        open_datasync                                 n/a
        fdatasync                        2974.951 ops/sec
        fsync                            2238.596 ops/sec
        fsync_writethrough                            n/a
        open_sync                        1777.819 ops/sec

Compare open_sync with different write sizes:
(This is designed to compare the cost of writing 16kB in different write open_sync sizes.)
        16kB open_sync write             3608.877 ops/sec
         8kB open_sync writes            2009.499 ops/sec
         4kB open_sync writes            1238.880 ops/sec
         2kB open_sync writes             135.783 ops/sec
         1kB open_sync writes              67.377 ops/sec

Test if fsync on non-write file descriptor is honored:
(If the times are similar, fsync() can sync data written on a different descriptor.)
        write, fsync, close              2962.546 ops/sec
        write, close, fsync              3015.991 ops/sec

Non-Sync'ed 8kB writes:
        write                           376151.965 ops/sec
~~~

### fio

<https://github.com/axboe/fio>
/!\ Attention on fait les tests sur le disque, sans partition ou FS. /!\

~~~
    Read bandwidth:
    fio --direct=1 --rw=randread --bs=1m \
    --size=5G --numjobs=4 --runtime=10 --group_reporting --name=file1

    Read IOPS:
    fio --direct=1 --rw=randread --bs=4k \
    --size=5G --numjobs=64 --runtime=10 --group_reporting --name=file1

    Write bandwidth:
    fio --direct=1 --rw=randwrite --bs=1m \
    --size=5G --numjobs=4 --runtime=10 --group_reporting --name=file1

    Write IOPS:
    fio --direct=1 --rw=randwrite --bs=4k \
    --size=5G --numjobs=64 --runtime=10 --group_reporting --name=file1


  read : io=1859.1MB, bw=190420KB/s, iops=47604 , runt= 10002msec
  write: io=956556KB, bw=95627KB/s, iops=23906 , runt= 10003msec

  read : io=2632.0MB, bw=268952KB/s, iops=262 , runt= 10021msec
  write: io=2258.0MB, bw=230827KB/s, iops=225 , runt= 10017msec
~~~
