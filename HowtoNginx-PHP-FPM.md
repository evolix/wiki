---
categories: web
title: Howto Nginx + PHP-FPM
...

Nginx et PHP-FPM (FastCGI Process Manager) est un nouveau "setup"
(par rapport à LAMP) pour faire fonctionner des applications PHP,
pour les instructions suivantes, adapter php7.3 à la version désiré.

## Installation

~~~
# aptitude install nginx-full php7.3-fpm
~~~

## Configuration

La configuration de PHP-FPM se trouve dans `/etc/php/7.3/fpm/php-fpm.conf`.

On définit un ou plusieurs "pools" FPM via `/etc/php/7.3/fpm/pool.d/*.conf` :

La configuration qui suit défini un pool avec une séparation au
niveau des usagers ssh et www, et ce pour chacun des pools.

~~~
[foo]

user = www-foo
group = foo

listen = /var/run/php-fpm-foo.sock
listen.owner = www-data
listen.group = www-data

php_flag[display_errors] = on
php_admin_flag[log_errors] = on
php_admin_value[error_log] = /home/foo/log/fpm-php.www.log

pm = dynamic
pm.max_children = 50
pm.start_servers = 20
pm.min_spare_servers = 5
pm.max_spare_servers = 35
pm.max_requests = 0

~~~

On configure maintenant Nginx, par exemple dans un Virtualhost _foo_ :

~~~
server {
    listen 80;
    server_name www.example.com example.com;
    root /home/foo/www;
    index index.html index.php;

    location ~ \.php$ {
        fastcgi_pass   unix:/var/run/php-fpm-foo.sock;
        fastcgi_read_timeout 180s;
        include snippets/fastcgi-php.conf;
    }

    access_log /home/foo/log/nginx-access.log;
    error_log  /home/foo/log/nginx-error.log;
}
~~~

Le fichier snippet/fastcgi-php.conf est inclu par défaut dans
l'installation PHP et devrait être favorisé par rapport à des
configurations custom.
