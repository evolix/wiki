---
categories: wm stump
title: Howto StumpWM
...

StumpWM est un gestionnaire de fenêtre écrit en Common Lisp qui se veut minimimaliste au niveau visuel, personnalisable à chaud grâce au pouvoir de la programmation fonctionnelle et CL, et concentré à un usage central et total au clavier (dans le même esprit que [ratpoison](https://fr.wikipedia.org/wiki/Ratpoison)).

Dernière version stable (non packagé sur Debian Jessie) : 1.0 - 13/01/17

# Installation

~~~
# apt install stumpwm
$ apt-cache policy stumpwm
stumpwm:
  Installed: 2:0.9.8-7
~~~

> *Note*: doc basé sur stumpwm 0.9.8 - package présent sur Debian Jessie - stable.

# Utilisation de base

Comme tout gestionnaire de fenêtre, à lancer lors du lancement de X (à ajouter dans ~/.xinitrc).
Le fichier de conf local doit se trouver dans ~/.stumpwmrc.

> *Note*: il peut être intéressant de mettre la touche **\<CTRL\>** à la place de **Caps_Lock** car celle-ci sera souvent utilisé, et permettra aux doigts d'être toujours dans le même alignement. Voir : [TipsKeyboard](/TipsKeyboard#transformer-caps_lock-en-ctrl)

Une fois démarré et utilisable, vous devriez voir un message en haut à droite de l'écran pour vous inviter à commencer à utiliser le gestionnaire de fenêtre en commençant par **\<Ctrl\>+T** (**^T**).

> *Note*: Tous les messages et autres messages d'informations s'ouvrent en haut à droite de l'écran

Toutes combinaisons doit être précédées de la combinaison de touches **\<Ctrl\>+T** <=> **^T** :

> *La touche **T**, sur clavier qwerty ou azerty n'est pas forcément la plus adéquat => voir pour la changer*


> *Note sur la convention d'écriture sur ce wiki*
>
> - **^X** correspond à la combinaison **^t + ^X** c'est à dire **\<Ctrl\>+t** suivis de **\<Ctrl\> X**.
> - **`X** correspond aussi bien à **^t + ^X** que **^t + X**
> - **X + \<prompt\>** correspond à **^t + X** suivis du texte à écrire (ex: nom commande) indiqué en haut à droite dans le prompt*


# Les premiers pas

**Toutes combinaisons doit être précédées de la combinaison de touches \<Ctrl\>+t <=> ^t :**

> *Note*: L'utilisation de la souris n'a aucun effet pour se déplacer dans les différents 'tiles' ou fenêtres.

## Le minimum vital

*StumpWM* n'a aucun ajout visuel par défaut hormis le cadre en haut à droite pour l'affichage des messages, et du fin rectangle entourant le 'tile' sélectionné. Donc il faut impérativement connaître un minimum de commande clavier, car de toute façon la souris ne vous aidera pas ici :

~~~{ .bash }
^h : "Afficher l'aide help" #^g pour nettoyer l'écran
`a : "Avoir la date du jour + heure" #date
`c : "Ouvrir un terminal" #/etc/alternatives/x-terminal-emulator
`e : "Ouvrir emacs" #/etc/alternatives/emacs
^! + <prompt> : "Executer la commande donné en argument" #/bin/sh -c
~~~

> *Note*: Le prompt se fera aussi comme tous messages en haut à droite de l'écran. Il faudra taper - pour l'exemple **^!** - le nom du programme puis valider avec la touche entrée. L'autocomplétion (avec **\<TAB\>**) fonctionne toujours dans le prompt.


## Gérer la/les fenêtres

~~~{ .bash }
`k : "Tuer la fenêtre" #<=> kill
 K : "Tuer la fenêtre" #<=> kill -9
^w : "Lister les fenêtres (du groupe visible)"
^N + <prompt> : "Assigner un numéro à la fenêtre visible et positionné"
~~~

## Créer les 'tiles'

~~~{ .bash }
 s : "Couper à l'horizontal le 'tile' positionné"
 S : "Couper à la verticale le 'tile' positionné"
 r : "Changer la taille du 'tile' positionné" #Utiliser les flèches pour cela
 R : "Selon le découpage, fusionne avec le découpage précédent" #La fenêtre sélectionné dans le 'tile' où se situe l'action se retrouve en second plan
~~~

## Se déplacer simplement

~~~{ .bash }
 o : "'Tile' suivant"
 M-n : "Se déplacer sur la fenêtre suivante (+1) dans le 'tile' lié"
 [0-9] : "Aller sur la fenêtre x - s'afficher sur le 'tile' où la fenêtre est positionné" #voir ^w pour connaître le numéro de toutes les fenêtres créés
^[0-9] : "Afficher et mettre la fenêtre numéro x dans le 'tile' visible"
 <fleches-directionnelles> : "Se déplacer dans les 'tiles' voisins (si existant ou si écran présent)" 
~~~

## Gestion des fenêtres

~~~{ .bash }

~~~


# Gérer les bureaux (groups)

~~~{ .bash }
 G : "Lister les groupes avec les fenêtres présentes"
 F* : "Choisir le groupe numéro '*'"
 g + c + <prompt> : "Créer un nouveau group"
~~~


# ^h

## Infos sur les commandes

~~~{ .bash }
^h : "message help de base"
 h + c + <prompt> : "avoir info sur une commande"
 h + k + <prompt> : "connaître à quoi correspond le raccourci en question tapé dans le prompt"
 h + ^h : "connaître les raccourcis disponible pour l'aide"
~~~


# Les petits plus

~~~{ .bash }
F11: "mettre la fenêtre sélectionné en plein écran"
`b : "faire disparaître son curseur en bas à droite de l'écran"
^g : "annuler le raccourci en cours (nettoie aussi les messages si présent - en haut à droite)"
 i : "Afficher la géométrie de la fenêtre + numéro de la fenêtre + titre de l'application"
 - : "Faire disparaître toutes fenêtres dans le 'tile' présent"
`m : "Voir le dernier message"
 v : "connaître la version de votre WM"
~~~


# Environnement *conseillé*

*StumpWM* se prête très bien, pour son aspect minimaliste, avec la plupart des logiciels ayant une interface basé sur ncurses ou simplement en CLI (qui permet de faire pratiquement toutes actions au clavier). De plus certains logiciels reprennent la plupart des raccourcis et mouvement permis comme sur *StumpWM*. Voici quelques exemples :

 - editeur texte : emacs, vi
 - administration : screen, tmux
 - navigateur web : *'hint' <=> 'follow link'*
     - **f** : qutebrowser, conkeror, firefox + [vimperator](https://addons.mozilla.org/en-US/firefox/addon/vimperator/?src=search)
     - **\<Ctrl\>** : konqueror
 - client mail : mutt
 - client irc : irssi


## Naviguer au clavier

Astuces avec [Qutebrowser](https://wiki.evolix.org/TipsQutebrowser)

## Configurer son clavier

### Configurer son modmap (Xmodmap)