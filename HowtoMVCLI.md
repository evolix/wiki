---
categories: RAID SAS NVMe BOSS-S2 Dell
title: Howto MVCLI
...

- Documentation : [https://www.dell.com/support/manuals/en-us/poweredge-r7525/boss-s2_ug/boss-s2-cli-commands-supported-on-poweredge-servers?guid=guid-988762b4-feb3-43c8-82b3-8d34c5db0e32]()
- Status de cette page : unstable / bullseye

[MVCLI](https://www.dell.com/support/manuals/en-us/poweredge-r7525/boss-s2_ug/run-boss-s2-cli-commands-on-poweredge-servers-running-the-linux-operating-system?guid=guid-c0f3bd0d-4725-4fed-8bc2-4aa872f3627f) est un utilitaire en ligne de commande pour interagir avec les contrôleurs RAID BOSS-S2 de Dell. **Cet utilitaire ne semble faire que de la lecture d'information.**


# Installation

1. Télécharger l'archive depuis [le site de Dell](https://www.dell.com/support/home/fr-fr/drivers/driversdetails?driverid=mcm7y).
    * Attention : il faut la télécharger depuis un navigateur web, car on obtient une 403 avec `wget` ou `curl`.
1. Copier le binaire `x64/cli/mvcli` sous `/usr/local/bin/`
1. Jouer la commande `# chmod +x /usr/local/bin/mvcli`
1. Copier la bibliothèque `x64/cli/libmvraid.so` sous `/usr/local/lib/`.
1. Jouer la commande `# ldconfig /usr/local/lib/`.

Note : il y a bien un script `install.sh` dans l'archive, mais il n'active pas le bit d'exécution pour `mvcli` et il n'installe pas la bibliothèque sous `/usr/local`.
