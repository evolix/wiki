- Documentation : [https://grafana.com/docs/agent/latest/]()
- Status de cette page : unstable / bulseye

[Grafana agent](https://github.com/grafana/agent) est un collecteur de logs, métriques et traces.

# Installation

~~~(sh)
# wget -q -O /etc/apt/trusted.gpg.d/grafana.asc https://apt.grafana.com/gpg.key
# chmod 644 /etc/apt/trusted.gpg.d/grafana.asc
# echo "deb [signed-by=/etc/apt/trusted.gpg.d/grafana.asc] https://apt.grafana.com stable main" > /etc/apt/sources.list.d/grafana.list
# apt update && apt install grafana-agent-flow
# systemctl daemon-reload
~~~


# Configuration

## Configuration de `grafana-agent-flow`

Par défaut, la configuration de `grafana-agent-flow` est lue à partir du fichier `/etc/grafana-agent-flow.river`. Mais on préfère séparer la configuration par fichiers, on utilise donc le dossier `/etc/grafana-agent-flow/config.d/`.

~~~(sh)
# mkdir -p /etc/grafana-agent-flow/config.d/
~~~

`grafana-agent-flow run` prend comme seul argument positionnel un fichier de configuration au format River ou bien un dossier contenant des fichiers au format River.  Dans le cas d'un dossier, les fichiers `.river` qu'il contient seront [lus par ordre lexical](https://pkg.go.dev/path/filepath#WalkDir) ; c'est dû à l'utilisation de la fonction Golang [`filepath.Walkdir`](https://github.com/grafana/agent/blob/d226a0e7e733dd5a1793a1f39d352457f106e48c/cmd/internal/flowmode/cmd_run.go#L375-L402).



## Configuration du service `grafana-agent-flow.service`

Le service utilise par défaut le fichier d'environnement (`EnvironmentFile`) `/etc/default/grafana-agent-flow` pour passer des variables d'environnements et des arguments au processus `grafana-agent-flow run`.  Afin de les surcharger, on utilise le fichier `/etc/grafana-agent-flow/config.env` avec ce contenu :

~~~(env)
#
# Command line options for grafana-agent
#

# Configuration directory, containing lexicaly ordered *.river files, in River
# configuration format.
CONFIG_FILE="/etc/grafana-agent-flow/config.d/"

# User-defined arguments to pass to the run command:
# - don't send usage statistics to Grafana
CUSTOM_ARGS="--disable-reporting"

# Restart on system upgrade. Defaults to true.
RESTART_ON_UPGRADE=true
~~~

Ces variables d'environnements :

- désactive l'envoi de statistique d'utilisation des composants aux développeurs des Grafana agent en ajoutant `--disable-reporting` dans la variable d'environnement `CUSTOM_ARGS`
- utilise le dossier `/etc/grafana-agent-flow/config.d/` pour stocker les fichiers de configuration en définissant la variable d'environnement `CONFIG_FILE`

Pour que `/etc/grafana-agent-flow/config.env` soit utilisé par le service `grafana-agent-flow.service` on le surcharge comme ceci :

~~~(sh) 
# mkdir /etc/systemd/system/grafana-agent-flow.service.d/
# cat <<EOF > /etc/systemd/system/grafana-agent-flow.service.d/override.conf
[Service]
# Override default configuration
EnvironmentFile=/etc/grafana-agent-flow/config.env
EOF
# systemctl daemon-reload
~~~

