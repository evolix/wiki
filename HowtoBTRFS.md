---
title: Howto Btrfs
categories: filesystem system
...

* Documentation : <https://btrfs.wiki.kernel.org/index.php/Main_Page#Documentation>
* Wikipédia Btrfs : <https://fr.wikipedia.org/wiki/Btrfs>
* BTRFS sous Debian : <https://wiki.debian.org/Btrfs>

[Btrfs](https://btrfs.wiki.kernel.org/index.php/Main_Page) (B-tree file system, se prononce « Butter-FS ») est un système de fichiers basé sur le principe du [Copy-On-Write](https://fr.wikipedia.org/wiki/Copy-on-write) visant à mettre en œuvre des fonctionnalités avancées comme la tolérance aux pannes, la réparation et la facilité d'administration. Sous licence GNU GPL il est développé conjointement par Oracle, Red Hat, Fujitsu, Intel, SUSE, STRATO AG (en)…
En 2012, un effort intense de développement et de test est fourni par la communauté afin de faire de Btrfs le successeur de ext3/ext4, systèmes de fichiers habituels des distributions Linux. 

Btrfs offre notamment les fonctionnalités suivantes absentes d'autres systèmes de fichiers :

- Instantanés (snapshots)
- Sommes de contrôle
- Compression
- Transfert de données de sous-volumes (send/receive)
- Multi-disque (attention, certains niveaux de RAID ne sont pas considérés comme stable et ne doivent pas être utilisés)

Nous utilisons pour l'instant Btrfs uniquement sur des systèmes de stockages secondaires, comme les serveurs de sauvegardes, où l'on profite de la performance des snapshots.


## Installation

Le support du système de fichier BTRFS est intégré par le noyau Linux de base. Néanmoins, il faut installer le paquet [`btrfs-progs`](https://packages.debian.org/bookworm/btrfs-progs) pour bénéficier des outils relatifs à Btrfs (création, maintenance)

~~~
# apt install btrfs-progs
~~~

## Utilisation de base

Formater un volume en Btrfs :

~~~
# mkfs.btrfs <DISQUE/PARTITION>

## Exemple :
# mkfs.btrfs /dev/vdz
btrfs-progs v6.2
See http://btrfs.wiki.kernel.org for more information.

NOTE: several default settings have changed in version 5.15, please make sure
      this does not affect your deployments:
      - DUP for metadata (-m dup)
      - enabled no-holes (-O no-holes)
      - enabled free-space-tree (-R free-space-tree)

Label:              (null)
UUID:               004a38a0-b5dd-4629-90e0-0d5481bfd27c
Node size:          16384
Sector size:        4096
Filesystem size:    9.77GiB
Block group profiles:
  Data:             single            8.00MiB
  Metadata:         DUP             256.00MiB
  System:           DUP               8.00MiB
SSD detected:       no
Zoned device:       no
Incompat features:  extref, skinny-metadata, no-holes
Runtime features:   free-space-tree
Checksum:           crc32c
Number of devices:  1
Devices:
   ID        SIZE  PATH
    1     9.77GiB  /dev/vdz
~~~

Montage persistant au boot de notre volume Btrfs en configurant le *fstab* :

~~~
# echo "/dev/vdz /backup btrfs defaults 0 0" >> /etc/fstab
# mount /backup
~~~

> *Remarque*: Il n'est plus nécessaire de spécifier l'option 'ssd' sur les systèmes récents pour signaler au kernel que le disques est un SSD


Lister les volumes btrfs montés :

~~~
# btrfs filesystem show
Label: none  uuid: 004a38a0-b5dd-4629-90e0-0d5481bfd27c
	Total devices 1 FS bytes used 144.00KiB
	devid    1 size 9.77GiB used 536.00MiB path /dev/vdz
~~~

> Note : `filesystem` peut être abrégé `fi`.

Afficher l'espace occupé :

~~~
# btrfs filesystem df <POINT_DE_MONTAGE>

## Exemple :
# btrfs filesystem df /backup
Data, single: total=8.00MiB, used=0.00B
System, DUP: total=8.00MiB, used=16.00KiB
Metadata, DUP: total=256.00MiB, used=128.00KiB
GlobalReserve, single: total=3.50MiB, used=0.00B
~~~


Augmenter la taille d'une partition montée :

~~~
# btrfs filesystem resize +42g /backup
~~~

Réduire la taille d'une partition montée :

~~~
# btrfs filesystem resize -42g /backup
~~~



Vérification sur une partition non montée :

~~~
# btrfs check --progress <DISQUE/PARTITION>

## Exemple : 
# btrfs check --progress /dev/vdz
Opening filesystem to check...
Checking filesystem on /home/lpoujol/test.btrfs
UUID: 004a38a0-b5dd-4629-90e0-0d5481bfd27c
[1/7] checking root items                      (0:00:00 elapsed, 16 items checked)
[2/7] checking extents                         (0:00:00 elapsed, 24 items checked)
[3/7] checking free space tree                 (0:00:00 elapsed, 4 items checked)
[4/7] checking fs roots                        (0:00:00 elapsed, 5 items checked)
[5/7] checking csums (without verifying data)  (0:00:00 elapsed, 1 items checked)
[6/7] checking root refs                       (0:00:00 elapsed, 6 items checked)
[7/7] checking quota groups skipped (not enabled on this FS)
found 196608 bytes used, no error found
total csum bytes: 0
total tree bytes: 196608
total fs tree bytes: 81920
total extent tree bytes: 16384
btree space waste bytes: 186279
file data blocks allocated: 0
 referenced 0

~~~

### subvolumes et snapshots

Un subvolume (ou sous-volume) est comparable à un simple répertoire (il peut contenir des fichiers et d'autres répertoires). Lorsque que l'on utilise Btrfs, il existe au moins un subvolume, le subvolume racine.

Un subvolume peut avoir des options de montages spécifique (à quelques exceptions près, comme la compression qui sera globale au volume Btrfs).
Si on souhaite avoir des options spécifiques, ou monter le subvolume dans un autre emplacement on utilisera la commande `mount` ou le `fstab` si on souhaite que ce soit persistant, on ajoutera l'option de montage`subvol=MY_SUBVOL`, avec *MY_SUBVOL* le chemin relatif du sous-volume par rapport à la racine du volume Btrfs.

~~~
# btrfs subvolume create <POINT_DE_MONTAGE_DU_SUBVOLUME>

## Exemple :
# btrfs subvolume create /backup/aaa
Create subvolume '/mnt/aaa'

# btrfs subvolume create /backup/bbb
Create subvolume '/mnt/bbb'
~~~

Liste des subvolumes disponibles d'un volume Btrfs :

~~~
# btrfs subvolume list <POINT_DE_MONTAGE>

## Exemple :
# btrfs subvolume list /backup/
ID 256 gen 8 top level 5 path aaa
ID 257 gen 9 top level 5 path bbb
~~~

Un snapshot est un intantané figé de toutes les données contenues dans un subvolume. Si par exemple vous disposez deux fichiers ("foo" et "bar") dans un subvolume, un snapshot avant la suppression d'un de ces deux vous permettra de le récupérer dans ce snapshot. Un snapshot reste un subvolume, manipulable avec les commandes relatives au sobvolumes, mais en plus son contenu sera par défaut en lecture seule.

> **Important** : **Un snapshot n'est pas une sauvegarde**, il s'appuie sur le mécanisme de copy-on-write de Btrfs. Il partage donc les mêmes blocs de données. Ainsi, si les données sont endommagées, elles le seront aussi bien sur le snapshot que sur le subvolume. Cette fonctionnalité est utile pour conserver une ou plusieurs copies locales qui peuvent enuite être utilisées pour effectuer un rollback ou une sauvegarde à partir de l'état figé d'un subvolume.

Créer un snapshot :

~~~
# btrfs subvolume snapshot <POINT_DE_MONTAGE_DU_SUBVOLUME> <POINT_DE_MONTAGE_DU_SNAPSHOT>

## Exemple  : 
# btrfs subvolume snapshot /backup/aaa /backup/snap_aaa
Create a snapshot of '/backup/aaa' in '/backup/snap_aaa'
~~~

Supprimer un subvolume (ou snapshot) :

~~~
# btrfs subvolume delete /backup/snap_aaa
~~~

Lister les propriétés d'un subvolume :

~~~
# btrfs property list -ts <POINT_DE_MONTAGE_DU_SUBVOLUME>
~~~

Passer un subvolume en RW :

~~~
# btrfs property set -ts <POINT_DE_MONTAGE_DU_SUBVOLUME> ro false
~~~

Passer un subvolume en RO :

~~~
# btrfs property set -ts <POINT_DE_MONTAGE_DU_SUBVOLUME> ro true
~~~

#### Manipulations d'un subvolume

Pour déplacer un subvolume, il faut créer un snapshot en read-only du subvolume que l'on souhaite déplacer/renommer puis supprimer l'original. 

~~~
# btrfs sub snap -r <POINT_DE_MONTAGE_DU_SUBVOLUME> <POINT_DE_MONTAGE_DU_SNAPSHOT>
# btrfs subvolume delete <POINT_DE_MONTAGE_DU_SUBVOLUME>
~~~

On passera ensuite le volume en RO/RW :

~~~
# btrfs property set -ts <POINT_DE_MONTAGE_DU_SNAPSHOT> ro false
~~~

### Envoyer un subvolume

Pour transférer un subvolume vers un autre serveur, il faut créer un snapshot en RO du subvolume en question.

~~~
# btrfs sub snap -r <POINT_DE_MONTAGE_DU_SUBVOLUME> <POINT_DE_MONTAGE_DU_SNAPSHOT>
~~~

On peut ensuite envoyer le volume via SSH :

~~~
# btrfs send <POINT_DE_MONTAGE_DU_SNAPSHOT> | ssh root@192.0.2.1 "btrfs receive /path/to/remote-snapshot"
~~~

### Compression

On peut monter le volume avec l'option `compress` pour activer une compression à la volée des fichiers. 
Selon la version du Kernel système, on pourra choisir l'algorithme de compression (zstd, lzo, zlib) ainsi que son éventuel réglage du niveau de compression.

Exemple : `compress=zstd:3` pour utiliser la compression *zstd* au niveau de compression *3* (le niveau par défaut qui offre un bon compromis entre temps cpu et niveau de compression)

Si le volume avant n'avait pas l'option `compress` on pourra tout recompresser avec `btrfs filesystem defragment -czstd` ou `-clzo`/`-czlib`.

> **ATTENTION** : La commande defrag va entrainer la re-éctiture des des éléments pour leur compression. Cette opération ne va pas préserver le partage des extent (les structures de données du système de fichier qui contiennent les données de vos fichiers). Elles sont partagées quand on fait `cp --reflink` ou quand on crée des snapshots d'un volume pour économiser l'espace disque. Ainsi, dans cette situation, ça fera augmenter la consomation d'espace disque.
>

## Maintenance


### Scrub

Vérifier l'intégrité d'un subvolume avec l'opération **scrub** qui lance une lecture de l'ensemble des données et métadonnées du système de fichiers et utilise les sommes de contrôle pour identifier et réparer les données corrompues éventuelles :

~~~
# btrfs scrub start <POINT_DE_MONTAGE>
# btrfs scrub status <POINT_DE_MONTAGE>

## Exemple: 
# btrfs scrub start /backup/
scrub started on /backup, fsid 004a38a0-b5dd-4629-90e0-0d5481bfd27c (pid=422087)

# btrfs scrub status /backup/
UUID:             004a38a0-b5dd-4629-90e0-0d5481bfd27c
Scrub started:    Fri Feb 23 10:24:12 2024
Status:           finished
Duration:         0:00:00
Total to scrub:   400.00KiB
Rate:             0.00B/s
Error summary:    no errors found
~~~


Les statistiques de la partition ont des compteur d'erreurs qui permettent de voir si le volume a eu des erreurs d'écritures ou de lecture.
Attention, des compteurs ne sont pas remis 0 au redémarrage de la machine. Il s'agit des compteurs d'erreurs depuis le formatage en Btrfs

~~~
# btrfs dev stats <POINT_DE_MONTAGE>
 
## Exemple : 
# btrfs dev stats /backup
[/dev/vdz].write_io_errs    0
[/dev/vdz].read_io_errs     0
[/dev/vdz].flush_io_errs    0
[/dev/vdz].corruption_errs  0
[/dev/vdz].generation_errs  0
~~~

### Balance

### Trim

### Balance

### Automatisation des tâches de maintenance

Dans Debian, il existe un paquet [btrfsmaintenance](https://packages.debian.org/bookworm/btrfsmaintenance) qui contient un ensemble de scripts pour automatiser l'éxécution des taches de maintenance (Srub, Balance, Trim et Defrag)