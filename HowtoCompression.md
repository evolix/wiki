# Howto Compression

# Vitesse et niveau de compression

Voir ce [site](https://catchchallenger.first-world.info/wiki/Quick_Benchmark:_Gzip_vs_Bzip2_vs_LZMA_vs_XZ_vs_LZ4_vs_LZO#Compression_time).

Résumé en terme de vitesse : lz4 > lzop > gzip > lzma >= xz > bzip  
Résumé en terme de compression : lzma ~= xz > bzip2 > gzip > lz4 > lzop


## TAR

Les options `-a` ou `--auto-compress` permettent à TAR de détecter la méthode de compression selon le suffixe. Exemple :

~~~
$ tar -acf archive.tar.bz2 .
$ tar -acf archive.tar.lzma .
$ tar -acf archive.tar.xz .
~~~

### LZMA

#### Compression

~~~
tar cvaf dossier.tar.lzma dossier
OU
tar cvf dossier.tar.lzma dossier --lzma
~~~

On peut aussi lancer en dry-run ( ne fait rien mais liste les fichiers qui seraient dans l'archive ) 

~~~
tar cvf /dev/null --exclude='*.webm' dossier
# en excluant les fichiers en .webm par exemple
~~~

#### Décompression

~~~
tar axvf dossier.tar.lzma
OU
tar xvf dossier.tar.lzma dossier --lzma
~~~

### XZ

#### Compression

~~~
tar cvaf dossier.tar.xz dossier
tar cvJf dossier.tar.xz dossier
~~~

#### Décompression

~~~
tar axvf dossier.tar.xz
tar Jxvf dossier.tar.xz
~~~

## LZ4

Pour un dump MySQL par exemple.

~~~
mysqldump --all-databases | lz4 > mysql.bak.lz4 
lz4 -cd mysql.bak.lz4 | mysql                                                                                   
~~~

Pour utilisation avec tar.

~~~
tar cvf - dossier | lz4 > dossier.tar.lz4
~~~
