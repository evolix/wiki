**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Quagga

<http://www.nongnu.org/quagga/docs/quagga.html>

BGP est un protocole de routage, permettant d'échanger des routes entre les opérateurs pour... constituer Internet.
On peut faire tourner un routeur BGP sous BSD (OpenBGPD), Linux (Quagga), Cisco, Juniper, etc.

<http://www.cisco.com/en/US/tech/tk365/tk80/tsd_technology_support_sub-protocol_home.html>

<http://fr.wikipedia.org/wiki/Border_Gateway_Protocol>

<http://www.ietf.org/rfc/rfc1771.txt>

<http://www.falz.net/tech/openbgpd>

<http://www.ris.ripe.net/cgi-bin/lg/index.cgi>

<http://www.ripe.net/data-tools/stats/ris>

## Installation

Sous Debian Squeeze, on peut installer Quagga  0.99.20.1 :

~~~
# aptitude install quagga
~~~

## Configuration

La configuration se passe dans le répertoire _/etc/quagga/_.

On active _zebra=yes_ et _bgpd=yes_ dans le fichier _daemons_.
Puis on crée un fichier _zebra.conf_ du type :

~~~
! Config Quagga
hostname test
password 1234
enable password 5678
~~~

Puis un fichier _bgpd.conf_ du type :

~~~
! Config Quagga BGP
router bgp 65001
bgp router-id 10.0.0.1
network 10.0.0.0/8
neighbor 10.0.0.2 remote-as 65002
log file /var/log/quagga/bgpd.log
line vty
~~~

On peut alors démarrer Quagga ainsi :

~~~
# /etc/init.d/quagga start
Loading capability module if not yet done.
Starting Quagga daemons (prio:10): bgpd.
line vty
~~~

Et l'on peut se connecter interactivement à un shell Quagga (Cisco-like) :

~~~
# vtysh 

Hello, this is Quagga (version 0.99.20.1).
Copyright 1996-2005 Kunihiro Ishiguro, et al.

test# show version
Quagga 0.99.20.1 ().
Copyright 1996-2005 Kunihiro Ishiguro, et al.
~~~

ou alors en telnet (accès plus limité a priori) :

~~~
$ telnet 127.0.0.1 2601
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.

Hello, this is Quagga (version 0.99.20.1).
Copyright 1996-2005 Kunihiro Ishiguro, et al.


User Access Verification

Password: 
test> en
Password: 
test#
~~~

Quelques commandes :

~~~
# conf t
test(config)# 
test-quagga(config)# exit

# write  
Building Configuration...
Configuration saved to /etc/quagga/zebra.conf
Configuration saved to /etc/quagga/bgpd.conf
[OK]

# show interface
# show interface description
# show memory
# show running-config
# show ip bgp
# show ip bgp summary
# show ip bgp neighbors
# show ip route
# show bgp summary

# debug bgp
# debug bgp updates

test(config)# ip route 1.2.3.0/24 10.0.0.5
~~~

## Exemple de 2 firewalls connectés à 2 routeurs du même AS

Afin de gérer une bascule automatique, voici un exemple de configuration
de 2 firewalls (actif/passif) connectés à 2 routeurs (actif/actif) d'un même AS.
Les 2 firewalls annonceront leur réseau, et les 2 routeurs la route _default_

Voici la configuration sur le firewall actif :

~~~
!debug bgp events
!debug bgp filters
!debug bgp fsm
!debug bgp keepalives
!debug bgp updates

router bgp 65001
 bgp router-id 192.168.0.41
 bgp log-neighbor-changes
 network 1.2.3.0/24
 no synchronization
 no auto-summary

neighbor 192.168.0.37 remote-as 1234
neighbor 192.168.0.37 soft-reconfiguration inbound
neighbor 192.168.0.38 remote-as 1234
neighbor 192.168.0.38 soft-reconfiguration inbound

log file /var/log/quagga/bgpd.log
line vty
~~~

Sur le firewall passif, c'est similaire, on ajoute juste un as-preprend
pour qu'il ne soit choisit qu'en cas de coupure avec le firewall actif :

~~~
!debug bgp events
!debug bgp filters
!debug bgp fsm
!debug bgp keepalives
!debug bgp updates

router bgp 65001
 bgp router-id 192.168.0.42
 bgp log-neighbor-changes
 network 1.2.3.0/24
 no synchronization
 no auto-summary

neighbor 192.168.0.37 remote-as 1234
neighbor 192.168.0.37 route-map ANNONCEMYNET out
neighbor 192.168.0.37 soft-reconfiguration inbound
neighbor 192.168.0.38 remote-as 1234
neighbor 192.168.0.38 route-map ANNONCEMYNET out
neighbor 192.168.0.38 soft-reconfiguration inbound

log file /var/log/quagga/bgpd.log
line vty

route-map ANNONCEMYNET permit 100
 set as-path prepend 65001 65001
~~~

Il reste en général à mettre en place une IP load-balancée sur le LAN
entre le firewall actif et passif, IP qui sera la gateway des machines internes.
Par exemple, via _keepalived_.