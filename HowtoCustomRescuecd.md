---
title: Howto Custom SystemRescueCD
...

* Documentation : <http://www.system-rescue-cd.org/Customization/>

[SystemRescueCD](http://www.system-rescue-cd.org/) est un live CD intégrant de nombreux outils pour récupérer un système endommagé (parted, grub, sfdisk, ...) ou installer un système (debootstrap).

Nous allons ici personnaliser SystemRescueCD pour qu'il intègre directement nos clés SSH dès son lancement.

## Pré-requis

Tout d'abord, nous avons besoin de l'image ISO accessible à cette adresse : http://www.system-rescue-cd.org/Download/

On vérifie l'image avec sha256sum :

~~~
sha256sum systemrescuecd-x86-x.y.z.iso
~~~

## Extraction de l'image

Pour customiser l'image ISO nous avons besoin d'outils disponibles seulement dans SystemRescueCd, nous allons donc lancer le live CD dans [KVM](HowtoKVM).

Une fois arrivé sur un terminal, on devra réserver une partition d'au moins 8Go pour placer à l'intérieur toute l'arborescence du système personnalisé :

~~~
mkdir /mnt/custom
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt/custom
~~~

Nous allons extraire l'image du système courant :

~~~
% /usr/sbin/sysresccd-custom extract
  /mnt/custom is mounted -> ok
    there is enough estimated free space here (.... MB) -> ok
~~~

## Personnalisation

Il est maintenant possible de personnaliser l'image dans le dossier /mnt/custom/customcd/files/.

### Inclure des clés publique SSH

Afin de ne pas avoir a mettre un mot de passe a chaque boot, nous allons inclure notre clé SSH :

~~~
mkdir -vp /mnt/custom/customcd/files/root/.ssh
echo "ssh-rsa tW8XUS9U4mF/sL[...]5Zc1jyx0yXHc85KaQ/lhtOPxHOLqANvpcrST28kRZoccBmvfmAn48QG3TKWLj4S utilisateur@machine" > /mnt/custom/customcd/files/root/.ssh/authorized_keys
~~~

## Generation de la nouvelle image

Les modifications faites, nous passons à la réalisation de la nouvelle image squashfs :

~~~
/usr/sbin/sysresccd-custom squashfs
~~~

*Cette procédure est assez lente et demande environ une vingtaine de minutes.*

On créer enfin la nouvelle image ISO :

~~~
/usr/sbin/sysresccd-custom isogen my_srcd
~~~

Nous obtiendrons ainsi ce fichier :

~~~
/mnt/custom/customcd/isofile/sysresccd-20170406-1401.iso
~~~

## Graver l'image officielle

Il faudra ce paquet : 

~~~
apt install syslinux-utils
~~~

Récupéer l'ISO : http://www.system-rescue-cd.org/Download/

~~~
isohybrid systemrescuecd-x86-x.x.x.iso
dd if=/path/to/systemrescuecd-x86-x.x.x.iso of=/dev/sdb bs=1M && sync
~~~





