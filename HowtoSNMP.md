---
categories: network monitoring
title: Howto SNMP
...

* Explorateur de MIB : <https://cric.grenoble.cnrs.fr/Administrateurs/Outils/MIBS/> ou <http://www.circitor.fr/Mibs/Mibs.php> (peut donner plus de détails sur l'interprétation des valeurs)

## Principe de fonctionnement

SNMP (pour Simple Network Management Protocol) est un protocole qui permet la supervision d'équipements réseau (serveurs, routeurs, switchs...). Il utilise le protocole de transport UDP sur les ports 161 et 162 pour les traps.

Il se base sur 3 éléments :

* Le manager (ou superviseur) : il centralise toutes les données qu'il collecte sur les nœuds ;
* Les nœuds sont les équipements informatiques à superviser ;
* Les agents sont des démons qui tournent en permanence sur les nœuds pour répondre aux requêtes du manager.

Toutes les données qu'un agent fournit sont référencés dans une MIB, accessible grâce à un OID. Une MIB (Management Information Base) est une base de données dans laquelle les informations sont classées sous forme d'arbre. Chaque information est alors accessible par son OID (Object Identifier), qui est en fait le chemin hiérarchique de l'élément.
Par exemple, 1.3.6.1.2.1.2.2.1.2 est l'OID ifDescr qui est la chaîne de caractères décrivant une interface réseau.

Le protocole SNMP peut également être utilisé pour modifier des valeurs dans la MIB (donc plus seulement superviser, mais administrer). Enfin, il existe un concept d'interruption (traps), grâce auquel une notification est envoyée par l'agent au manager SNMP lorsqu'un évènement particulier se produit.

Du point de vue de la sécurité, les versions 1 et 2 du protocoles permettent d'utiliser une communauté comme mot de passe pour pouvoir lire ou écrire dans la MIB, mais celle-ci transite en clair sur le réseau. La version 3 est plus sécurisé puisqu'elle se base sur du chiffrement DES avec des mots de passes ou des clés partagées entre l'agent et le manager.

## Mise en place

Le principe est d'installer un agent SNMP (SNMPD = NET SNMP Agent) sur toutes les machines à superviser. Sur la machine de supervision, on installera un client/manager SNMP destiné à recueillir les informations des agents SNMP.

### Configuration de l'agent

Par défaut, snmpd n'écoute que sur 127.0.0.1 et [::1].

La configuration se trouve dans `/etc/snmp/snmpd.conf` :

~~~
# Écouter sur 192.0.2.1
agentaddress  192.0.2.1

# Indiquer la localisation et le contact de l'équipement
sysLocation    Marseille, France
sysContact     Me <me@example.org>

# Views : limiter l'accès à certains OID uniquement
view   systemonly  included   .1.3.6.1.2.1.1
view   systemonly  included   .1.3.6.1.2.1.25.1

# Communautés IPv4 et IPv6 : communauté read-only `public` accessible par tout le monde, avec limitation sur le view `systemonly`
rocommunity  public default -V systemonly
rocommunity6 public default -V systemonly
~~~

Puis on redémarre l'agent SNMP :

~~~
# systemctl restart snmpd
~~~

### Configuration du manager

Afin de traduire les OID en leur description textuelle, il faut activer le repository apt `non-free` et installer `snmp-mibs-downloader` :

~~~
# apt install snmp-mibs-downloader
~~~

On l'active ensuite en éditant `/etc/snmp/snmp.conf` pour commenter la ligne suivante :

~~~
#mibs:
~~~

Puis on met à jour les MIB :

~~~
# download-mibs
~~~

### Interroger la MIB de l'agent

Le paquet `snmp` doit être installé sur la machine de supervision.

Avec `snmpget`, on obtient le résultat de l'OID exact demandé. Avec `snmpwalk`, on obtient le résultat de toute une arborescence de plusieurs OID.

~~~
$ snmpget -v2c -c public 192.0.2.1 system.sysContact.0
SNMPv2-MIB::sysContact.0 = STRING: Me <me@example.org>
$ snmpget -v2c -c public 192.0.2.1 system.sysLocation.0
SNMPv2-MIB::sysLocation.0 = STRING: Marseille, France
$ snmpget -v2c -c public 192.0.2.1 system.sysDescr.0
SNMPv2-MIB::sysDescr.0 = STRING: Linux routeur 2.4.31-grsec-fw-i386 #1 Tue Aug 23 12:18:10 CEST 2005 i586
$ snmpwalk -v2c -c public 192.0.2.1
~~~

L'OID peut être indiquée de plusieurs manières : l'OID numérique, l'OID textuel abrégé, ou encore l'OID textuel complet.

~~~
$ snmpget -v 1 -c public 192.0.2.1 .1.3.6.1.2.1.1.3.0
DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (88640) 0:14:46.40
$ snmpget -v 1 -c public 192.0.2.1 system.sysUpTime.0
DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (102902) 0:17:09.02
$ snmpget -v 1 -c public 192.0.2.1 .iso.org.dod.internet.mgmt.mib-2.system.sysUpTime.0
DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (107510) 0:17:55.10
~~~

### Recevoir des traps SNMP

Le démon `snmptrapd` permet de recevoir et logger les traps SNMP, puis éventuellement exécuter une action suite à la réception d'une trap.

#### Installation

~~~
# apt install snmptrapd
~~~

Cependant, l'installation de ce paquet installe également le paquet `snmpd`, ce qui installe donc un agent SNMP sur notre manager SNMP. Si l'on souhaite que notre manager reste uniquement manager, il faut désactiver les fonctionnalités de l'agent, en éditant `/etc/snmp/snmpd.conf`. On indique à l'agent de n'écouter sur aucune IP :

~~~
agentaddress  none
~~~

Tout le reste peut être commenté, puis on finit par redémarrer l'agent :

~~~
# systemctl restart snmpd
~~~

#### Modifier les arguments de démarrage

Par défaut, `snmptrapd` va écrire les traps reçues dans syslog, à partir de la facilité "warning".

On modifie les arguments de démarrage par :

* `-A` : n'écrase pas le fichier de log à chaque redémarrage du service, mais ajoute de nouvelles lignes
* `-LF 6 /var/log/snmptrap.log` : redirige les logs vers le fichier `/var/log/snmptrap.log`, à partir de la facilité syslog "info" et au-dessus
* `-f` : ne pas forker
* `-p /run/snmptrapd.pid` : fichier PID

* Si on utilise `SysvInit`, on modifie les arguments de démarrage du démon dans `/etc/default/snmptrapd` :

~~~
TRAPDOPTS='-A -LF 6 /var/log/snmptrap.log -f -p /run/snmptrapd.pid'
~~~

* Si on utilise `Systemd`, on modifie les arguments de démarrage du démon en surchargeant l'unité

~~~
# systemctl edit snmptrapd.service
[Service]
ExecStart=
ExecStart=/usr/sbin/snmptrapd -A -LF 6 /var/log/snmptrap.log -f -p /run/snmptrapd.pid

# systemctl daemon-reload
~~~

#### Configuration

La configuration se fait dans `/etc/snmp/snmptrapd.conf` :

~~~
# Écouter sur l'IP 192.0.2.5
snmpTrapdAddr 192.0.2.5

# Autoriser à recevoir des traps sur la communauté private, et à uniquement logger ces traps
authCommunity log private
~~~

Par défaut le format dans lequel s'ecrivent les trap SNMP en version 2c correspond à `%.4y-%.2m-%.2l %.2h:%.2j:%.2k %B [%b]:\n%v\n` (soit la date, l'heure, l'hostname de la machine source, les IPs et ports source et destination, puis le contenu de la trap ; voir les explications du format dans [man snmptrapd(8)](https://manpages.debian.org/bullseye/snmptrapd/snmptrapd.8.en.html#FORMAT_SPECIFICATIONS). Il n'est pas très lisible et on peut le modifier pour simplement ajouter un saut de ligne entre chaque champs de la trap :

~~~
# Format des traps SNMPv2 : ajouter un saut de ligne entre chaque champs de la trap
format print2 %.4y-%.2m-%.2l %.2h:%.2j:%.2k %B [%b]:\n%V\n%v\n\n
~~~

Si on veut exécuter une action à la réception d'une trap en particulier, on peut utiliser l'option `traphandle`. Le paquet `traptoemail` est inclus avec `snmptrapd` et permet d'envoyer le contenu de la trap SNMP par email. Les autorisations doivent alors être changées pour rajouter le droit d'exécution :

~~~
# Autoriser à recevoir des traps sur la communauté private, et à logger ces traps puis exécuter une action
authCommunity log,execute private

# Envoyer un mail quand on reçoit certaines trap SNMP
traphandle IF-MIB::linkDown /usr/bin/traptoemail -s 127.0.0.1 me@example.org     # Link Down
traphandle IF-MIB::linkUp /usr/bin/traptoemail -s 127.0.0.1 me@example.org       # Link Up
traphandle default /usr/bin/traptoemail -s 127.0.0.1 notme@example.org           # Tout le reste
~~~

Si `default` est indiqué dans le champ OID, alors le programme sera invoqué pour chaque OID ne correspondant pas aux autres OID pour lesquels une `traphandle` est définie.

On finit par redémarrer le service :

~~~
# systemctl restart snmptrapd
~~~

Si on utilise un firewall, il faut penser à autoriser la réception de paquets sur le port 162. Exemple avec `iptables` :

~~~
/sbin/iptables -A INPUT -p udp --sport 1024:65535 --dport 162 -j ACCEPT
~~~

#### Astuce : obtenir le hostname de la machine source de la trap SNMP sans DNS

Le champ "hostname" (correspondant à `%B` dans le format de la trap) est déduit de l'IP source par résolution DNS. Si l'IP n'a pas de résolution DNS (et que l'on ne souhaite pas en configurer), on peut utiliser le fichier `/etc/hosts` sur la machine recevant les traps :

~~~
192.0.2.1     SW01
192.0.2.2     SW02
192.0.2.3     SW03
192.0.2.4     SW04
192.0.2.5     SW05
~~~

Ainsi, le nom `SW01` sera affiché comme hostname source si l'IP source est 192.0.2.1, et ainsi de suite pour les autres IP. S'il n'y a aucune résolution, alors le champ sera noté `<Unknown>`.

## Intégration à Icinga

Le plugin NRPE `check_snmp` permet de réaliser des requêtes SNMP et d'interpréter le résultat comme étant _OK_, _Warning_ ou _Critical_.

### Utilisation

Les paramètres les plus utiles du plugin sont :

* `-H` : machine à surveiller ;
* `-P` : version du protocole à utiliser (1, 2c ou 3) ;
* `-o` : OID sur lequel faire la requête ;
* `-u` : unité de la valeur de l'OID (utilisé comme suffixe).

Définitions des seuils :

* `-w` : plage de valeur du résultat (sous la forme `"x:y"`) qui ne sera pas considéré comme un _Warning_ (les valeurs x et y sont compris dans la plage) ;
* `-c` : idem, mais pour le _Critical_.

Exemple :

~~~
$ /usr/lib/nagios/plugins/check_snmp -H 192.0.2.1 -P 2c -o 1.3.6.1.4.1.1714.1.2.1.11.1 -u "failed hard drive" -w "0:0" -c "0:1"
~~~

Dans ce cas, le résultat de la requête sera considéré comme _OK_ si il vaut _0_, _Warning_ si il vaut _1_, et _Critical_ si il est supérieur à _1_.
