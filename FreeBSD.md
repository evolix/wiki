**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto FreeBSD

## Installer un package

~~~
# setenv PACKAGESITE ftp://ftp.freebsd.org/pub/FreeBSD/ports/amd64/packages-8.3-release/Latest/
# setenv FTP_PASSIVE_MODE 1
# pkg_add -r php5-simplexml
Fetching ftp://ftp.freebsd.org/pub/FreeBSD/ports/amd64/packages-8.3-release/Latest/php5-simplexml.tbz... Done.
~~~

## Compiler depuis l'arbre des ports

~~~
# cd /usr/ports/categorie_du_paquet/nom_du_paquet
# make
# make install
~~~

## Voir les paquets installés

~~~
# pkg_info -Ix nom_du_paquet
~~~


## Mises à jour avec freebsd-update(8)

<http://www.freebsd.org/doc/handbook/updating-freebsdupdate.html>

Exemple :

~~~
# pkg_delete -a   (supprime tous les packages !!)
# freebsd-install
# reboot
# freebsd-install
~~~

## Optimisations

<http://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/configtuning-kernel-limits.html>

Optimisations réseau pour permettre un grand nombre de connexions :

~~~
# sysctl kern.ipc.nmbclusters=262144
# sysctl kern.ipc.maxsockets=204800
# sysctl net.inet.tcp.msl=20000
~~~

<http://rerepi.wordpress.com/2008/04/19/tuning-freebsd-sysoev-rit/>

## Admin

<http://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/users-groups.html>

Ajout d'un groupe :

~~~
# pw groupadd <groupe>
# pw groupmod <groupe> -M <user>
~~~

## Installer des paquets à l'identique d'un serveur à l'autre

~~~
server1# for i in /var/db/pkg/*; do pkg_create -b $i; done
server1# rsync *.tbz server2:

server2# pkg_add -i *.tbz
~~~
