---
title: Howto Tideways
---

Doc officielle : <https://support.tideways.com/>

Tideways est un APM pour les applications PHP. Il permet d'analyser des traces et repérer aisément d'où proviennent les lenteurs (requêtes SQL, appels externes, …).

# Installation

## Installer le paquet

Source : https://support.tideways.com/documentation/setup/installation/debian-ubuntu.html

```sh
apt install wget gnupg apt-transport-https
echo 'deb https://packages.tideways.com/apt-packages-main any-version main' | tee /etc/apt/sources.list.d/tideways.list
wget -O /etc/apt/trusted.gpg.d/tideways.asc https://packages.tideways.com/key.gpg
dos2unix /etc/apt/trusted.gpg.d/tideways.asc
chmod 644 /etc/apt/trusted.gpg.d/tideways.asc
apt update
apt install tideways-php tideways-daemon
```

Le paquet se charge d'activer le module PHP automatiquement.

## Activer la génération de traces

Créer une nouvelle organisation (la première fois) / une nouvelle application dans l'interface de Tideways et suivre les instructions.

* Exemple à placer pour un vhost Apache et un WordPress :

~~~
SetEnv TIDEWAYS_APIKEY CléAPI
SetEnv TIDEWAYS_FRAMEWORK wordpress
SetEnv TIDEWAYS_SAMPLERATE 25
~~~

* Exemple à placer pour un pool PHP et un WordPress :

~~~
env[TIDEWAYS_APIKEY] = CléAPI
env[TIDEWAYS_FRAMEWORK] = wordpress
env[TIDEWAYS_SAMPLERATE] = 25
~~~

Si le framework n'est pas connu par Tideways, il suffit de ne pas mettre la directive.
La variable `TIDEWAYS_SAMPLERATE` permet de définir le taux de requêtes (pourcentage) qui sera analysé via le profiler Tideways.
