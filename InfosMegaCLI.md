**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été (beaucoup) révisée.**

# Infos MegaCLI

<http://www.admin-linux.fr/?p=8535>
<http://erikimh.com/megacli-cheatsheet/>
<http://www.ens-lyon.fr/PSMN/doku.php?id=faq:sysadmin:megacli>

## Commandes de base

~~~
# megaraidsas-status
# megaclisas-status
# megacli -adpallinfo -aALL
# megacli -pdlist -aALL
# megacli -LDInfo -Lall -aALL
# megacli -AdpEventLog -GetEvents -aALL
# megacli -AdpAlILog -aALL
~~~

Note : -aALL indique d'appliquer la commande sur tout les contrôleur disponibles, souvent on en a qu'un seul, donc c'est pratique, plutôt que de chercher son ID (Même si c'est souvent a0).

Récupérer l'Enclosure Id (E) et le numéro de Slot (S) des disques :

~~~
# megacli -PDList -aALL |grep -E 'Enclosure Device|Slot|Inquiry'
~~~

On notera alors *\[E:S\]* l'emplacement du disque souhaité.

*E:* est l'enclosure su disque et *S* est le slot du disque souhaité. 

Note : Si *E (Enclosure Id)* est N/A, on mettra alors \[:S\]

Infos sur un disque :

~~~
# megacli -pdinfo -PhysDrv \[E:S\] -a0
~~~

## Passer un disque en HotSpare

Typiquement après le changement d'un disque défectueux, si le rebuild ne se fait pas automatiquement, l'astuce est de passer le nouveau disque en Hot Spare. Ainsi le contrôleur « prendra » le disque Hot Spare pour faire le rebuild.

~~~
megacli -PDHSP -Set -PhysDrv \[65535:2\] -a0
~~~

Cela passe le disque d'ID 2, enclosure 65535, en Hot Spare.

## Utiliser un disque HotSpare

Parfois, un disque est disponible en tant que HotSpare, mais le controleur ne l'utilise pas… Une astuce est de dire qu'il n'est plus HS, puis le repasser HS.

~~~
megacli -PDHSP -Rmv -PhysDrv [8:4] -a0
megacli -PDHSP -Set -Dedicated -Array0 -PhysDrv [8:4] -a0 
~~~

Cela remet le disque d'ID 4; enclosure 8, en tant que HS dédié pour l'array 0.

## Contrôler le rebuild d'un disque

~~~
# megacli -pdrbld -showprog -physdrv\[E:S\] -a0
                                     
Rebuild Progress on Device at Enclosure E, Slot S Completed 10% in 23 Minutes.
~~~

## Passer un disque en OFFLINE

~~~
megacli -PDOffline -PhysDrv \[E:S\] -a0
~~~

## Passer un disque en ONLINE

**!!! ATTENTION !!! ATTENTION !!! ATTENTION !!!**

L’erreur fatale à ne pas commettre lorsque vous constatez un disque en état Firmware state: Failed est de le forcer à revenir Online. En effet, cela force à repasser en mode RAID sans reconstruction et vous pouvez dire adieu à votre système de fichiers. NE FAITES DONC PAS :

~~~
megacli -PDOnline -PhysDrv \[64:2\] -a0
~~~

## Passer un disque en missing

Ça le sort du RAID.

~~~
megacli -PDMarkMissing -PhysDrv \[E:S\] -a0
~~~

## Stopper la rotation d'un disque (dans le but de le retirer)

~~~
megacli -PDPrpRmv -PhysDrv \[E:S\] -a0
~~~

Notes :

* `PrpRmv` signifie «  prepare removal ».
* Dans son status, on voit ensuite que le disque est `spun down` (rotation arrêtée)

## Passer un disque de bad à good (on peut utiliser l'option `-force`)

Si la statut du disque est `Unconfigured(bad)` et qu'en lançant une commande vous rencontrez l'erreur `The specified device is in a state that doesn't support the requested command`, passer son état en `good` peut résoudre le problème :

~~~
megacli -PDMakeGood -PhysDrv\[E:S\] -a0
~~~

## Remplacer un disque missing

Lister d'abord :

~~~
# megacli -PdGetMissing -a0     
                                              
    Adapter 0 - Missing Physical drives       
                                              
    No.   Array   Row   Size Expected         
    0     0       0     139236 MB             
                                              
Exit Code: 0x00                               
~~~

Le disque (row 0) de l'array 0 est missing. Pour cet exemple, on va dire au disque physique 64:2 de prendre la place du row 0.

~~~
megacli -PdReplaceMissing -PhysDrv \[64:2\] -Array0 -row0 -a0 
~~~

## Faire clignoter un disque

~~~
# megacli -PdLocate -start -physdrv\[E:S\] -a0
~~~

Note : cela ne fonctionne pas sur tous les contrôleurs

## Configurer un volume RAID à partir de nouveaux disques

Vérifier que les disques ne sont pas déjà dans un volume RAID

~~~
# megacli -ldpdinfo -a0 |grep -E '(Virtual|Inquiry|Slot)'
~~~

## Créer un nouveau _virtual disk_

~~~
# megacli -CfgLdAdd -r1[E:S1,E:S2] -a0
# megacli -CfgLdAdd -r0[E:S] -a0
~~~

`-r0` / `-r1` / `-r5` pour le type de RAID

## Supprimer un _virtual disk_

~~~
# megacli -CfgLdDel -L2 -a0
~~~

Note : attention à bien le faire sur le bon !

## Passer un disque en JBOD

pour rappel, un JBOD est une sorte de "pass-thru" pour faire comme si l'on voyait le disque sans passer par le controlleur (pas de RAID donc).
c'est *presque* équivalent à ajouter un _virtual disk_ (VD) avec un seul disque en RAID0 (bon, en fait pas tout à fait).

~~~
# megacli -PDMakeJBOD -PhysDrv [:2] -a0
~~~

Note : attention, cette commande n'est pas acceptée sur tous les types de cartes... dans certains cas il faut mettre à jour le firmware de la carte RAID pour avoir accès à cette fonction.

## accès aux logs du controleur disque

~~~
# megacli -AdpEventLog -GetEvents -aALL
# megacli -AdpAlILog -aALL
~~~

Note : attention, ça peut être long et verbeux !

## Scanner les disques avec une config "étrangère" (attention, les disques doivent être en "GOOD") 

~~~
# megacli -CfgForeign -Scan -a0
# megacli -PdInfo -PhysDrv '[E:S]' -a0 | grep -i foreign
~~~

## Supprimer la config "étrangère" des disques :

~~~
# megacli -CfgForeign -Clear -a0
~~~

## Importer la config "étrangère" du ou des disques :

~~~
# megacli -CfgForeign -Import -a0
~~~



## PDs in LD have incompatible EEDP types ou PDs in LD have different block sizes

Les disques dur neufs d'entreprises sont parfois activés avec un PI (Protection Information, 8 octets supplémentaire sur les secteurs de 512 octets).

Parfois c'est le contraire PI est activé sur les disques du RAID et le nouveau disque n'est pas PI. Et dans ce cas on est coincé car souvent le disque est **PI Eligible = No**.

Pour le vérifier :

[Télécharger/Installer et utiliser PERCCLI.](https://www.dell.com/support/home/en-us/drivers/driversdetails?driverid=f48c2)

~~~
# rpm2cpio ./perccli-007.0127.0000.0000-1.noarch.rpm | cpio -idmv
# cd opt/MegaRAID/perccli/
# ./perccli64 /c0 /eall /sall show
Controller = 0
Status = Success
Description = Show Drive Information Succeeded.


Drive Information :
=================

----------------------------------------------------------------------------
EID:Slt DID State DG      Size Intf Med SED PI SeSz Model                Sp 
----------------------------------------------------------------------------
32:0      0 Onln   1 185.75 GB SATA SSD N   N  512B INTEL SSDSC2BX200G4R U  
32:1      1 Onln   1 185.75 GB SATA SSD N   N  512B INTEL SSDSC2BX200G4R U  
32:2      2 Onln   0  3.637 TB SAS  HDD N   Y  512B MG04SCA40EN          U  
32:3      3 UGood  -  3.637 TB SAS  HDD N   N  512B MG04SCA40EN          U  
----------------------------------------------------------------------------

~~~

On voit ici que le volume DG 0 avec son disque 32:2 est en PI=Y, mais que le disque 32:3 est en PI=N.

~~~
./perccli64 /c0/e32/s3 show all
Controller = 0
Status = Success
Description = Show Drive Information Succeeded.


Drive Information :
=================

-----------------------------------------------------------------------
EID:Slt DID State DG     Size Intf Med SED PI SeSz Model            Sp 
-----------------------------------------------------------------------
32:3      3 UGood -  3.637 TB SAS  HDD N   N  512B MG04SCA40EN      U  
-----------------------------------------------------------------------

[…]

PI Eligible = No
[…]
~~~

**PI Eligible = No** !!

## Gestion du cache


Attention les commandes suivantes modifie le cache pour tout les adaptateurs / disques.


* Voir le status actuel du cache :

~~~
megacli  -LDInfo -Lall -aALL -nolog | grep "Current Cache"
~~~

* Configuré write cache policy à write-back :

~~~
megacli -LDSetProp WB -LALL -aALL -nolog 
~~~

* Activé cache policy si le status BBU est dans un mauvais état :

~~~
megacli -LDSetProp CachedBadBBU -LALL -aALL -nolog
~~~

* Désactivé le cache policy :

~~~
megacli -LDSetProp NoCachedBadBBU -LALL -aALL -nolog
~~~

* Définir le cache en mode *Direct* du contrôleur en lecture seule, *Cached* est le cache du contrôleur en lecture/écriture :

~~~
megacli -LDSetProp -Direct -Lall -aALL -nolog
~~~

## Nombre maximum de disques

~~~ { .bash }
megacli -AdpAlILog -aALL | grep 'Maximum'
~~~