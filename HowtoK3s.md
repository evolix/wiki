---
categories: sysadmin
title: HowtoK3s
...

* Documentation : <https://docs.k3s.io/>

K3s est une distribution de Kubernetes légère et facile à installer (packagé dans un seul binaire).
On peut installer Kubernetes avec different composants, avec K3s, Rancher a fait un certain nombre de choix par défaut sur pour ses composant ce qui va grandement simplifier l'installation.

## Installation

Documentation : <https://docs.k3s.io/quick-start> et <https://docs.k3s.io/installation>

Prérequis :

* Il faut avoir un noyau récent qui supporte les CGroupv2 >= 5.8
* Vérifier la version de Iptable installée https://docs.k3s.io/known-issues#iptables
* Désactiver ufw si présent (les règles iptables de k3s passerons devant) 
* Il faut mettre minifirewall à DOCKER='on'

> Attention k3s rajoutera ses règles iptables par-dessus ; il faut faire attention à ce qu'on expose si la machine est sur internet.

Pour installer, on peut simplement jouer le script d'installation qui va récupérer le binaire et mettre en place l'unité systemd ou openrc.

~~~{.bash}
### Installer
# curl -sfL https://get.k3s.io | sh -
### Installer, configurer et lancer avec des arguement (exemple)
curl -sfL https://get.k3s.io | K3S_TOKEN=12345 sh -s - server --flannel-backend none
~~~

On peut ensuite lancer k3s en mode server ou agent avec des options de configurations

~~~{.bash}
k3s server ...
~~~

### Mise à jour

On peut rejouer le script d'installation ou installer le binaire d'une version en particulier :

> **Attention** : si à l'installation, on a joué le script d'installation avec des arguments, il ne faut pas en donner cette fois-ci ! autrement, ils écraseront les anciens argument.

~~~{.bash}
# Jouer le script d'installation sans arguments ...
curl -sfL https://get.k3s.io | sh -

# Installer le binaire d'une version https://github.com/k3s-io/k3s/releases
$ tag=v1.29.0+k3s1
$ curl -Lo /usr/local/bin/k3s https://github.com/k3s-io/k3s/releases/download/${tag}/k3s; chmod a+x /usr/local/bin/k3s
~~~

## Configuration

Selon les besoins, on peut configurer K3s de différente manière en même temps :

* Au lancement avec des ENVs et argument

* Avec un fichier de configuration dans `/etc/rancher/k3s/config.yaml` ou des fichiers dans `/etc/rancher/k3s/config.yaml.d/*.yaml` (lu et appliqué dans l'ordre alphabétique : la dernière valeur lue pour une clef sera appliqué).

~~~{.ini}
$vim /etc/rancher/k3s/config.yaml
write-kubeconfig-mode: "0644"
disable: 
  - servicelb
~~~

## Administration

## Optimisation

## Monitoring

### Nagios

### Munin

## Plomberie

## FAQ
