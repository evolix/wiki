# Howto UniFi

Points d'accès Wi-Fi Ubiquiti : <https://www.ubnt.com/unifi/unifi-ap/>

## Installation

### Debian

Récupérer la dernière version de UniFi Network Controller sur <https://www.ui.com/download/unifi/>

~~~
# apt install ./unifi_sysvinit_all.deb

$ firefox <https://127.0.0.1:8443/>
~~~

### OpenBSD

Il y a un port mais à cause des restrictions d'un point de vue de license, il n'y a pas que paquet

~~~
cd /usr/ports/net/unifi && make install
~~~

## Administration

Attention, l'interface ne détecte la borne qui si elle est dans un même segment réseau (niveau 2, donc pas de routage...).

Une fois la borne configurée, vous pouvez y accéder en SSH (récupérer son IP via l'interface... ou fixer son IP via DHCP) et le login / pass de l'interface, vous avez un OpenWRT :)

~~~
$ ssh root@10.0.0.11

BZ.v3.2.7# info

Model:       UAP
Version:     3.2.7.2816
MAC Address: 24:a4:3e:a6:42:a2
IP Address:  10.0.0.11
Hostname:    UBNT
Uptime:      2151 seconds

Status:      Connected (http://10.0.0.1:8080/inform)
~~~

## Portail Captif

Pour mettre en place un portail captif :

* Aller dans l'onglet "Settings"
* Dans "WiFi", cliquer sur "Create New"
* Compléter les "Name" (SSID) et "Password" (ou le mettre en "Open" sous "Security" si pas de mot de passe souhaité) du réseau, sélectionner un groupe de points d'accès si nécessaire ("Broadcasting AP"), puis dans "Advanced" sélectionner "Manual" pour pouvoir cocher "Hotspot Portal"
* Configurer les autres paramètres comme souhaité, puis valider


La page de connexion est désactivée par défaut. Elle peut être entièrement personnalisée :

* Aller dans l'onglet "Insights"
* En haut à droite, sélectionner "Hotspot", puis "Landing Page"
* Cliquer sur "Enable"
* Personnaliser la page comme souhaitée : l'image de fond, le logo, les couleurs, les conditions générales d'utilisation, le texte de bienvenue, la durée après laquelle un utilisateur doit à nouveau accepter les conditions, la langue du portail, les réseaux autorisés sans besoin d'accepter les conditions, les réseaux interdits même après acceptation des conditions, … Puis valider

À la connexion, l'utilisateur sera automatiquement redirigé sur le portail captif avant de pouvoir accéder à Internet.

## FAQ

### Supprimer la preallocation de MongoDB

Si vous avez oublié d'ajuster "nojournal=true" vous aurez 3,3 Go dans data/db/journal/ :

~~~
$ pkill java
$ rm data/db/journal/*
$ vim data/system.properties

unifi.db.nojournal=true

$ java -jar lib/ace.jar start
~~~

### Reset de la borne

Appuyer 10 secondes sur le bouton RESET de la borne, elle pase au rose et reboot.

### Sauvegarde des données de l'interface

Si vous n'avez plus vos données d'interface, il est quasi-impossible d'accéder à la borne sans réinstaller une interface et reset la borne.
Il est donc conseillé de sauvegarder les données :

~~~
$ tar cf unifi-backup.tar data/
~~~

Ces données peuvent être restaurées sur une nouvelle installation pour retrouver les paramètres.

### Commandes existantes

Aide succinte

~~~
$ help
~~~

Pour voir la configuration actuelle

~~~
$ less /tmp/system.cfg
~~~
