**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto ACL

Savoir que le fichier a un ACL configuré (`+`) :

~~~
$ ls -dl foo/
drwxr-x---+ ...
~~~

### Obtenir ACL

~~~
# getfacl foo/
~~~

récupérer le statut ACL sur le fichier.


### Modifier ACL

~~~
# setfacl -R -m group:example:rwx foo/
~~~

`-R` : récursif, `-m` : pour modifier.

~~~
# setfacl -d -R -m group:example:rwx foo/
~~~

`-d` : défaut

### Supprimer ACL

~~~
# setfacl -b foo/
# ls -ld foo
drwxr-xr- 2 evouser evouser 4096 Mar 14 16:33 foo/
~~~