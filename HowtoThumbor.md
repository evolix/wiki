# Installation sur Debian 8

Thumbor n'est disponible que sur sid. Certaines dépendances ne sont disponible que via les backports.

~~~
Paquet                    | Version | Jessie | Jessie BP |
--------------------------+---------+--------+-----------+
libc6                     | 2.14    |        |           |
python                    | 2.7     |        |           |
python-argparse           |         |        |           |
python-cairosvg           |         |        |           |
python-concurrent.futures |         |        |           |
python-crypto             | 2.1.0   | 2.6.1  |           |
python-derpconf           | 0.2.0   |        | 0.8.0     |
python-imaging            |         |        |           |
python-libthumbor         |         |        |           |
python-pexif              | 0.15    |        | 0.15.1    |
python-pil                | 3.0.0   | 2.6.1  | 3.4.2     |
python-pycurl             |         |        |           |
python-schedule           |         |        |           |
python-statsd             | 3.0.1   |        | 3.2.1     |
python-tornado            | 4.1.0   | 3.2.2  | 4.4.3     |
python-tz                 |         |        |           |
python-webcolors          |         |        |           |
~~~

La procédure suivante fonctionne, a priori :

~~~
# echo 'deb http://mirror.evolix.org/debian jessie-backports main' >>/etc/apt/sources.list
# cat >/etc/apt/preferences.d/backports <<eof
Package: *
Pin: release n=jessie-backports
Pin-Priority: 50

Package: python-backports-abc python-pil python-tornado python-derpconf python-pexif python-schedule python-statsd
Pin: release n=jessie-backports
Pin-Priority: 999
eof
# apt update
# apt install \
    libc6                     \
    python                    \
    libpython2.7-stdlib       \
    python-cairosvg           \
    python-concurrent.futures \
    python-imaging            \
    python-pycurl             \
    python-tz                 \
    python-webcolors          \
    python-six                \
    python-pkg-resources      \
    python-singledispatch     \
    python-backports-abc      \
    python-pil                \
    python-tornado            \
    python-derpconf           \
    python-pexif              \
    python-schedule           \
    python-statsd             \
    python-opencv
# wget \
    http://ftp.fr.debian.org/debian/pool/main/libt/libthumbor/python-libthumbor_1.3.2-1_all.deb \
    http://ftp.fr.debian.org/debian/pool/main/t/thumbor/thumbor_6.3.2-4_amd64.deb
# dpkg -i \
    python-libthumbor_1.3.2-1_all.deb \
    thumbor_6.3.2-4_amd64.deb
~~~

Thumbor utilise le port 8888 par défaut. Pour le modifier :

~~~
# cat /lib/systemd/system/thumbor.service
# systemctl daemon-reload
# systemctl start thumbor
~~~


# Configuration


## Configuration Apache

Fonctionne pour les URL *unsafe*, ici sur le port 8890 :

~~~
    ProxyPreserveHost On
    ProxyPass /unsafe http://127.0.0.1:8890/unsafe
    ProxyPassReverse /unsafe http://127.0.0.1:8890/unsafe
    <Proxy *>
        Require all granted
    </Proxy>
~~~

## Configuration

On peut générer un fichier de conf comme ceci :

~~~
thumbor-config >/etc/thumbor.d/thumbor.conf
~~~

Il s'agit d'un script Python entièrement commenté.
