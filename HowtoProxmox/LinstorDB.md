---
categories: databases
title: Administration de la base de donnée Linstor
...

## Backup et restauration de la base de donnée Linstor

Linstor a un outil pour faire un backup et une restauration de sa base de données

Cet outil est un binaire situé dans `/usr/share/linstor-server/bin/linstor-database` et peut etre appellé avec deux sous-commandes `export-db` et `import-db`.

Pour être sur d'avoir un backup intègre, il faut s'assurer que le service `linstor-controller` ne tourne pas pendant le backup.

* Faire un backup de la base de donnée, dans `/home/backup/` :

~~~
# systemctl stop linstor-controller
# /usr/share/linstor-server/bin/linstor-database export-db /home/backup/linstor_db
# systemctl start linstor-controller
~~~

Vous pouvez utilisé l'option `--config-directory` pour spécifié le dossier ou se trouve le fichier de configuration `linstor.toml`, s'il n'est pas spécifié, cela va utilisé le dossier par défaut `/etc/linstor`.

* Restaurer un backup de la base de donnée :

~~~
# systemctl stop linstor-controller
# /usr/share/linstor-server/bin/linstor-database import-db /home/backup/linstor_db
# systemctl start linstor-controller
~~~

Vous pouvez restaurer la base de donnée d'un précédent backup seulement si la version de Linstor et la même (ou supérieure) que la version à partir de laquelle vous avez créé la sauvegarde.

Si la version de LINSTOR actuellement installée est supérieure à la version à partir de laquelle la sauvegarde de la base de données a été créée, lorsque vous importez la sauvegarde, les données sont restaurées avec le même schéma de base de données que la version utilisée lors de l'exportation.

Ensuite, au prochain démarrage du contrôleur, celui-ci détectera que la base de données possède un ancien schéma et migrera automatiquement les données vers le schéma de la version actuelle.

### Conversion de la base de donnée

Étant donné que le fichier de base de données exporté contient certaines métadonnées, un fichier de base de données exporté peut être importé dans un type de base de données différent de celui à partir duquel il a été exporté.

Cela permet à l'utilisateur de convertir, par exemple, une configuration etcd en une configuration basée sur SQL. Il n'existe aucune commande spéciale pour convertir le format de la base de données.

Il vous suffit de spécifier le fichier de configuration `linstor.toml` correct en utilisant l'argument `--config-directory` (ou en mettant à jour le fichier par défaut `/etc/linstor/linstor.toml` et en spécifiant le type de base de données que vous souhaitez utiliser avant l'importation).

Vous pouvez suivre la [documentation de Linstor](https://linbit.com/drbd-user-guide/linstor-guide-1_0-en/#s-linstor-external-database) pour configurer en fonction du type de base de donnée externe que vous souhaitez utiliser.

Quel que soit le type de base de données à partir duquel la sauvegarde a été créée, elle sera importée dans le type de base de données spécifié dans le fichier de configuration `linstor.toml`.

## Déplacer le controlleur Linstor

Si la machine où le controlleur Linsor est instalée est indisponible, ou a une panne matérielle, il est possible de déplacer le controlleur Linstor

*On part du principe que l'on utilise la base de donnée de Linstor, et pas une base de donnée externe, ni etcd.*

* On arrête le controlleur sur la machine, ainsi que satellite (si un satellite tourne sur la machine ou se trouve le contrôlleur) :

~~~
# systemctl stop linstor-controller
# systemctl stop linstor-satellite
~~~

* On arrête le satellite sur la machine voisine :

~~~
# systemctl stop linstor-satellite
~~~

* On installe le controlleur sur la machine voisine, et on s'assure qu'il ne tourne pas :

~~~
# apt install linstor-controller
# systemctl stop linstor-controller
~~~

* On supprime le fichier de base de donnée que Linstor a créer lors de l'installation :

~~~
# rm /var/lib/linstor/linstordb*
~~~

* On copie le fichier de base de donnée qu'on a récupéré depuis la machine où le controlleur tournait :

~~~
# rsync -avP /var/lib/linstor/linstordb.mv.db 10.0.0.1:/var/lib/linstor/
~~~

* On supprime les paquets et la configuration sur la machine où il y avait l'ancien controlleur :

~~~
# apt purge linstor-satellite linstor-controller
~~~

* On demmare le controlleur sur la nouvelle machine, et on vérifie qu'il récupère bien les info du cluster, et on démarre le satellite :

~~~
# systemctl start linstor-controller.service
# linstor node list
# systemctl start linstor-satellite.service
~~~

* On installe seulement la partie `satellite` sur l'ancienne machine controlleur :

~~~
# apt install linstor-satellite linstor-proxmox
~~~

* Ensuite il faut redéfinir le type de noeud sur chaque machine, sur la machine `tac` va etre de type `combined` et la machine `tic` de type `satellite`, ces commandes ce joues sur le controlleur :

~~~
# linstor node modify tac --node-type COMBINED
# linstor node modify tic --node-type satellite
~~~ 

* On demarre `linstor-satellite` sur la machine qui avait l'ancien controleur :

~~~
# systemctl start linstor-satellite.service
~~~
