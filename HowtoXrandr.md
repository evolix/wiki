**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Xrandr

xrand -q

xrandr --output VGA1 --auto
xrandr --output VGA1 --off

xrandr --output LVDS1 --mode 1024x768

xrandr --output HDMI2 --right-of HDMI1

xrandr --output HDMI3 --mode 1440x900 --rotate left
xrandr --output HDMI1 --mode 2560x1080 --left-of HDMI3

ssh HOSTNAME xrandr -display :0.0 -q

