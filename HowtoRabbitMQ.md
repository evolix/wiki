# Howto RabbitMQ

* Documentation : <https://www.rabbitmq.com/documentation.html>

[RabbitMQ](https://www.rabbitmq.com/) est un système de gestion de file d'attentes. Il est écrit en Erlang et implémente le protocole AMQP (Advanced Message Queuing Protocol). Il est notamment utile pour gérer des tâches asynchrones.

## Installation

~~~
# apt install rabbitmq-server
~~~

~~~
# rabbitmqctl status

 {running_applications,[{rabbit,"RabbitMQ","3.3.5"},
                        {mnesia,"MNESIA  CXC 138 12","4.12.3"},
                        {os_mon,"CPO  CXC 138 46","2.3"},
                        {xmerl,"XML parser","1.3.7"},
                        {sasl,"SASL  CXC 138 11","2.4.1"},
                        {stdlib,"ERTS  CXC 138 10","2.2"},
                        {kernel,"ERTS  CXC 138 10","3.0.3"}]},
 {os,{unix,linux}},
 {erlang_version,"Erlang/OTP 17 [erts-6.2] [source] [64-bit] [smp:4:4] [async-threads:30] [kernel-poll:true]\n"},
~~~

## Configuration

La configuration par défaut de RabbitMQ est souvent utilisable en l'état, sans besoin de modification.

Si l'on doit modifier la configuration, voici les fichiers :

* `/etc/rabbitmq/rabbitmq-env.conf` : variables d'environnement pour RabbitMQ
* `/etc/rabbitmq/rabbitmq.config` : options pour RabbitMQ
* `/etc/default/rabbitmq-server` : configuration système (ulimit)

### Variables d'environnement pour RabbitMQ

Il faut donc créer le fichier :

~~~
# touch /etc/rabbitmq/rabbitmq-env.conf
# chmod 600 /etc/rabbitmq/rabbitmq-env.conf
# chown rabbitmq:rabbitmq /etc/rabbitmq/rabbitmq-env.conf
~~~

Différentes variables *RABBITMQ_\** sont utilisables :

~~~
RABBITMQ_NODE_PORT=672
RABBITMQ_SERVER_ERL_ARGS="+K true +A30 +P 1048576 -kernel inet_default_connect_options [{nodelay,true}]"
~~~

### Options pour rabbitmq.config

Il faut donc créer le fichier :

~~~
# touch /etc/rabbitmq/rabbitmq.config
# chmod 600 /etc/rabbitmq/rabbitmq.config
# chown rabbitmq:rabbitmq /etc/rabbitmq/rabbitmq.config
~~~

Différentes options sont utilisables (voir `/usr/share/doc/rabbitmq-server/rabbitmq.config.example.gz` pour un exemple).

Si l'on veut que RabbitMQ n'écoute que sur l'interface *localhost* :

~~~
[
  {rabbit, [
    {tcp_listeners, [{"127.0.0.1", 5672},
                     {"::1",       5672}]}
  ]}
].
~~~

### Configuration système

`/etc/default/rabbitmq-server` :

~~~
ulimit -n 2048
~~~

## Utilisation

La documentation officielle propose un [ensemble de tutoriels](https://www.rabbitmq.com/getstarted.html) pour Python, Java, Ruby, PHP, etc.

Les lignes suivantes proposent un cas d'utilisation minimal avec un émetteur, une queue et un récepteur, le tout illustré en PHP. (Voir [ici](https://www.rabbitmq.com/tutorials/tutorial-one-php.html) pour la source d'inspiration).

Prérequis : une instance du serveur RabbitMQ en local sur le port standard (5672), PHP 7/8 et composer.

Dans un dossier de travail (par exemple `~/src/lapin`), éditer un nouveau fichier `composer.json` avec ce contenu :

~~~
{
    "require": {
        "php-amqplib/php-amqplib": ">=3.0"
    }
}
~~~

Installer `php-amqplib` (un client pour RabbitMQ) avec la commande suivante :

~~~
$ composer install
~~~

Récupérer les fichiers `send.php` et `receive.php` dans le dépôt officiel des tutoriels de RabbitMQ :

~~~
$ wget https://raw.githubusercontent.com/rabbitmq/rabbitmq-tutorials/main/php/send.php
$ wget https://raw.githubusercontent.com/rabbitmq/rabbitmq-tutorials/main/php/receive.php
~~~

On voit en inspectant le code que le récepteur (fichier `receive.php`) :

- établie une nouvelle connexion au serveur RabbitMQ en tant qu'invité
- ouvre un canal et déclare une queue qui sera suivie
- se met en attente de messages à recevoir

De son côté l'émetteur (fichier `send.php`) :

- établie une nouvelle connexion au serveur RabbitMQ en tant qu'invité
- ouvre un canal et déclare une queue (la même que le récepteur)
- construit un message `Hello World!` et le publie dans le canal

Pour tester que ça fonctionne, il faut d'abord lancer le récepteur dans un terminal et le laisser rouler :

~~~
$ php receive.php
~~~

Et dans un second terminal lancer l'émetteur (autant de fois qu'on veut) :

~~~
$ php send.php
~~~

On peut obtenir des infos sur les queues gérées par RabbitMQ avec la commande suivante :

~~~
# rabbitmqctl list_queues
~~~

Note : en production, avec PHP, il est recommandé d'utiliser [amqproxy](https://github.com/cloudamqp/amqproxy) pour une meilleure persistance des connexions.

## Management RabbitMQ

### rabbitmqctl

~~~
# rabbitmqctl status
# rabbitmqctl list_connections
# rabbitmqctl list_queues
~~~

### Activation de l'interface web de management

Pour activer l'interface web de management :

~~~
# sudo -u rabbitmq rabbitmq-plugins enable rabbitmq_management
# systemctl restart rabbitmq-server
# sudo -u rabbitmq rabbitmqctl add_user admin PASSWORD
# sudo -u rabbitmq rabbitmqctl set_user_tags admin administrator
# sudo -u rabbitmq rabbitmqctl set_permissions -p "/" "admin" ".*" ".*" ".*"
~~~

On a ainsi accès à l'interface web via <http://127.0.0.1:15672/>

On peut aussi mettre en place un proxy via Nginx :

~~~
location /rabbitmq/ {
    proxy_pass http://127.0.0.1:15672/;
    include proxy_params;
}                                       
~~~

Ou Apache :

~~~
    ProxyRequests Off
    ProxyPreserveHost On

    <Proxy *>
       Require all granted
    </Proxy>

    AllowEncodedSlashes NoDecode
    ProxyPass / http://localhost:15672/ nocanon
    ProxyPassReverse / http://localhost:15672/

~~~

### Activation CLI

<https://www.rabbitmq.com/management-cli.html>

On peut installer un binaire permettant d'avoir un mode CLI pour RabbitMQ :

~~~
$ wget localhost:15672/cli/rabbitmqadmin > rabbitmqadmin
$ chmod +x rabbitmqadmin
~~~

avec `~/.rabbitmqadmin.conf` :

~~~
[default]
username = admin
password = PASSWORD
~~~

On a ainsi des commandes disponibles :

~~~
$ rabbitmqadmin list users
$ rabbitmqadmin list queues
$ rabbitmqadmin help subcommands
~~~

### _Vhosts_

On peut créer des _vhosts_ dans RabbitMQ :

~~~
# rabbitmqctl add_vhost <my_vhost>
~~~

Le _vhost_ `my_vhost` devrait être visible depuis l’interface web.

### Tracing des messages

> **Activer le tracing des messages cause une réduction de performance qui peut être importante.**

Afin d'obtenir un log des messages passant par un cluster rabbitmq, il est possible d'activer sa fonction de tracing.

Soit en créant un échange `amq.rabbitmq.trace` avec la commande `rabbitmqctl trace_on` (la désactivation globale étant avec `rabbitmqctl trace_off`).

Soit par l'intermédiaire du plugin `rabbitmq_tracing` qui permet l'écriture de ces traces dans des fichiers de logs et la gestion de ce qui est tracé par l'interface de gestion.

Pour activer le plugin `rabbitmq_tracing` il faut faire les actions suivantes :

1. Activer le plugin.

    ```
    # rabbitmq-plugins enable rabbitmq_tracing
    ```

2. Configurer le plugin dans `/etc/rabbitmq/rabbitmq.conf`. L'utilisateur configuré doit avoir les droits de créé une queue, de la lié à `amq.rabbitmq.trace` et de consommé les messages dans cette queue.

    ```
    [...]
    {rabbitmq_tracing,
       [
           {directory, "/path/to/tracing/log/directory"},
           {username, <<"admin">>},
           {password, <<"password">>}
       ]
    },
    [...]
    ```

3. Redémarrer le cluster RabbitMQ.
4. Les administrateurs devraient à présent avoir accès à un onglet "Tracing" dans l'interface de gestion.

## Sauvegarde

À l'aide du CLI :

~~~
$ rabbitmqadmin export rabbitmq.dump
Exported definitions for localhost to "rabbitmq.dump"
~~~

## Munin

On utilise *rabbitmq_connections* (merci [ask](https://github.com/ask)) :

~~~
$ wget https://raw.githubusercontent.com/ask/rabbitmq-munin/master/rabbitmq_connections
$ chmod +x rabbitmq_connections
$ ./rabbitmq_connections
~~~

Attention, *rabbitmq_connections* utilise la commande `rabbitmqctl` vous devez donc adapter le script ou configurer `/etc/munin/plugin-conf.d/munin-node` ainsi :

~~~
[rabbitmq_connections]
user rabbitmq
EOT
~~~


## Nagios

On utilise *check_rabbitmq* (merci [CaptPhunkosis](https://github.com/CaptPhunkosis)) :

~~~
# apt install python-requests
$ wget https://raw.githubusercontent.com/CaptPhunkosis/check_rabbitmq/master/check_rabbitmq
$ chmod +x check_rabbitmq
$ ./check_rabbitmq -a connection_count -C 100 -W 80
~~~

Attention, *check_rabbitmq* utilise la commande `sudo rabbitmqctl` vous devez donc adapter le script ou configurer *sudo*.

~~~
nagios ALL = (rabbitmq) NOPASSWD: /usr/sbin/rabbitmqctl list_connections
nagios ALL = (rabbitmq) NOPASSWD: /usr/sbin/rabbitmqctl list_queues
nagios ALL = (rabbitmq) NOPASSWD: /usr/sbin/rabbitmqctl list_status
~~~

## FAQ

### vhost '/' is down

Si des queues ne fonctionnent plus et que vous retrouvez une erreur contenant `vhost '/' is down` dans les logs `/var/log/rabbitmq/rabbit@${HOSTNAME}.log` vous pouvez tenter de relancer le vhost en question en exécutant :
```
# rabbitmqctl restart_vhost
```
Il est probable que cette commande retourne une erreur qui produise dans les logs des `CRASH REPORT` faisant référence à des chemins du style `/var/lib/rabbitmq/mnesia/rabbit@$HOSTNAME/msg_stores/vhosts/628WB79CIFDYO9LJ`. Il se peut que la cause du problème soit l'arrêt impromptu de rabbitmq qui peut entrainer un mauvais nettoyage des fichiers d'état qui alors bloquent le fonctionnement correct du service. Dans ce cas-là il faut arrêter le service, supprimer les données problématiques puis relancer le service :
```
# systemctl stop rabbitmq-server.service
# rm -r /var/lib/rabbitmq/mnesia/rabbit@$HOSTNAME/msg_stores/vhosts/628WB79CIFDYO9LJ
# systemctl start rabbitmq-server.service
```
