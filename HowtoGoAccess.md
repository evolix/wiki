---
categories: web sysadmin system
title: Howto GoAccess
...

* Documentation: <https://goaccess.io/man>
* Statut de cette page : test / bookworm

[GoAccess](https://goaccess.io) est un outil pour analyser facilement des logs Apache ou Nginx (et d'autres formats) : générer rapidement une page de rapport HTML, stats ncurses et même surveillance temps réel.


## Installation

~~~
# apt install goaccess geoip-database

goaccess -V
GoAccess - 1.7.
For more details visit: https://goaccess.io/
Copyright (C) 2009-2022 by Gerardo Orellana

Build configure arguments:
  --enable-utf8
  --enable-geoip=mmdb
  --with-openssl
~~~

Si besoin d'une version plus récente, l'upstream distribue des paquets Debian avec une version plus récente.

~~~
# echo "deb https://deb.goaccess.io $(lsb_release -cs) main" >> /etc/apt/sources.list.d/goaccess.list
# wget -O /etc/apt/trusted.gpg.d/goaccess.asc https://deb.goaccess.io/gnugpg.key
# dos2unix /etc/apt/trusted.gpg.d/goaccess.asc
# chmod 644 /etc/apt/trusted.gpg.d/goaccess.asc
# apt update && apt install goaccess geoip-database
~~~

## Configuration

On peut ensuite éditer `/etc/goaccess.conf`, en choisissant le format de log, par exemple :

~~~
time-format %H:%M:%S
date-format %d/%b/%Y
log-format %h %^[%d:%t %^] "%r" %s %b "%R" "%u"
~~~

Pour HAProxy, selon les versions, on peut tester les `log-format` suivants :

~~~
log-format %^ %^ %^ %^ %^ %^ %h:%^ [%d:%t.%^] %^ %^ %^/%^/%^/%L/%^ %s %b %^ %^ %^ %^ %^ {%v|%u} "%m %U %H"

log-format %^ %~%^ %^ %^ %^ %h:%^ [%d:%t.%^] %^ %^ %^/%^/%^/%L/%^ %s %b %^ %^ %^ %^ %^ {%v|"%u"} "%r"

log-format %^]%^ %h:%^ [%d:%t.%^] %^/%^/%^/%^/%L/%^ %s %b %^"%r"
~~~

Pour aller plus loin, voir la documentation de `log-format` : <https://goaccess.io/man#custom-log>

Note pour HaProxy : si on fait écouter HaProxy sur les port 80 et 443 avec l'option `v4v6`, HaProxy loggue toutes les IPs en mapping IPv4 vers IPv6 (`::ffff:<IPv4>`). GoAccess ne sait pas lire ce format hybride. Une solution est de séparer les deux formats d'IP dans HaProxy, comme indiqué dans [cette discussion](https://discourse.haproxy.org/t/make-haproxy-stop-prepending-to-ipv4-addresses-in-x-forwarded-for/3368).


## Utilisation

Raccourcis de base pour naviguer dans l'interface NCurse :

* Naviguer entre les modules : Tab / Shift+Tab (ou le numéro du module 1-10, mais ne permet pas d'aller au-delà de 10)
* Naviguer dans la page principale : ArrowUp / ArrowDown
* Dérouler le contenu d'un module : Enter
* Naviguer dans un module : PgUp / PgDown

~~~
# goaccess /var/log/apache2/access.log
~~~

Si vous utilisez le format de logs `COMBINED` ou `VCOMBINED` s'il y a des vhosts :

~~~
# goaccess /home/foo/log/access.log --log-format=COMBINED
# goaccess /var/log/apache2/access.log --log-format=VCOMBINED
~~~

> Note : avec d'anciennes versions (comme 0.8.3), la syntaxe était `goaccess -f access.log`

Si on veut cibler un fenêtre temporelle particulière ou accélérer le traitement, on filtrera avec grep dans un fichier temporaire :

~~~
# grep "<PATTERN>" /var/log/apache2/access.log > goaccess.tmp
# goaccess goaccess.tmp --log-format=VCOMBINED
~~~

Pour générer un rapport HTML (un seul fichier, les styles sont inline) :

~~~
# goaccess /var/log/apache2/access.log --log-format=VCOMBINED -a -o /var/www/rapport.html
~~~

Pour suivre un access.log en temps réel :

~~~
# goaccess -f access.log --log-format=VCOMBINED
~~~

Pour avoir un access de 12h à 16h du 1er Fevrier :

~~~
# grep "1/Fev/2017:1[2-6]" /var/log/apache2/access.log > goacces.tmp
# goaccess -f goaccess.tmp --log-format=VCOMBINED -a -o html
~~~


## Interface web GoAccess via WebSocket

Pour avoir le suivi en temps réel dans un navigateur, il faut servir le fichier HTML généré par Goaccess ainsi que l'exécuter en tant que démon pour recevoir les données en temps réel via sa WebSocket.


On peut créer une unité [systemd](HowtoSystemd) qui lancera GoAccess et sa WebSocket.

`/etc/systemd/system/goaccess.service` :

~~~
[Unit]
Description=GoAccess real time web stats.
After=network.target

[Service]
ExecStart=/usr/bin/goaccess -p /etc/goaccess/goaccess.conf /var/log/haproxy.log -o /var/www/goaccess.html --real-time-html
Type=simple
User=goaccess
Nice=19
IOSchedulingPriority=7

[Install]
WantedBy=default.target
~~~

~~~
# systemctl daemon-reload
# systemctl enable goaccess
# systemctl start goaccess
~~~

On sert ensuite `/var/www/goaccess.html` via [Apache](HowtoApache) et on « proxifie » par la WebSocket :

~~~
<VirtualHost *:443>
        ServerName example.org

        Include /etc/apache2/ssl/example.conf
        DocumentRoot /var/www/

        # Goaccess
        <Location "/goaccess.ws/">
            ProxyPass "ws://localhost:7890/"
        </Location>
</VirtualHost>
~~~

Notez qu'ainsi Apache fait la terminaison SSL sans que Goaccess accède aux certificats.

Enfin, nous pouvons indiquer au client Goaccess (dans le navigateur) d'accéder au WebSocket via l'URL proxifié par Apache. Il est important d'utiliser un nom de domaine et non une adresse IP autrement Apache dirigera la requête vers le Vhost par défaut, ici example.com.

`/etc/goaccess/goaccess.conf` :

~~~
ws-url wss://example.com:443/goaccess.ws/
~~~


## FAQ

### `Fatal error has occurred`

~~~
Fatal error has occurred
Error occured at: src/parser.c - parse_log - xxxx
No log format was found on your conf file.
~~~

Si cette erreur survient, il faut modifier le fichier de conf `/etc/goaccess.conf` pour décommenter les valeurs `logformat` correspondantes.


### Géolocalisation manquante

Installer le paquet `geoip-database` :

~~~
# apt install geoip-database
~~~

### Texte illisible à cause des couleurs

Le texte peut être illisible à cause du choix des couleurs de GoAccess, particulier si on utilise un terminal avec un thème clair. Il est possible de modifier les couleurs avec l'option [`--color`](https://manpages.debian.org/bookworm/goaccess/goaccess.1.en.html#color=_fg:bg_attrs,), mais c'est assez lourd : il faut changer les couleurs pour chaque élément de l'interface. Un contournement est de désactiver les couleurs avec l'option [`--no-color`](https://manpages.debian.org/bookworm/goaccess/goaccess.1.en.html#no_color).
