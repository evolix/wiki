---
categories: vpn openvpn security network
title: Howto shellpki
...

[Shellpki](https://gitea.evolix.org/evolix/shellpki) est un script Shell pour gérer une petite [PKI](https://fr.wikipedia.org/wiki/Infrastructure_%C3%A0_cl%C3%A9s_publiques) en ligne de commandes.

## Installation

Voir le [README](https://gitea.evolix.org/evolix/shellpki#install) du projet.

## Utilisation

Voir le [README](https://gitea.evolix.org/evolix/shellpki#usage) du projet.

## Migration depuis une ancienne version

Si l'on veut migrer depuis une très ancienne version de shellpki vers la dernière, les emplacements des différents fichiers sont différents. Une très ancienne version concernée contiendra la variable `PREFIX=/etc/openvpn/ssl` en début de script, se nommera `shellpki.sh` au lieu de simplement `shellpki`, et se trouvera dans `/etc/openvpn/ssl/shellpki.sh`.

Dans ce cas, voici la marche à suivre :

Il faut tout d'abord suivre la [procédure d'installation](#installation) propre à votre OS.
On peut ensuite passer à la copie des fichiers :

~~~
# cd /etc/shellpki/
# mkdir pkcs12 private requests tmp certs openvpn
# touch crl.pem
# cp /etc/openvpn/ssl/ca/cacert.pem /etc/shellpki/
# cp /etc/openvpn/ssl/ca/dh2048.pem /etc/shellpki/
# cp /etc/openvpn/ssl/ca/index.txt* /etc/shellpki/
# cp /etc/openvpn/ssl/ca/serial* /etc/shellpki/
# cp /etc/openvpn/ssl/template.conf /etc/shellpki/ovpn.conf
# cp /etc/openvpn/ssl/ca/private.key /etc/shellpki/cakey.key
# cp /etc/openvpn/ssl/certs/* /etc/shellpki/certs/
# chown -R _shellpki:_shellpki /etc/shellpki/
# chmod 640 cacert.pem openssl.cnf ovpn.conf certs/*
# chmod 600 cakey.key dh2048.pem index.txt* serial*
# chmod 604 crl.pem
# chmod 750 pkcs12/ private/ requests/ tmp/ certs/ openvpn/
~~~

Copier la clé `.key` du serveur vers le nouvel emplacement (vérifier son nom via le paramètre `key` de la configuration OpenVPN) :

~~~
# cp /etc/openvpn/ssl/files/<CN>-<timestamp>/<CN>.key /etc/shellpki/private/<CN>-<timestamp>.key
# chmod 600 /etc/shellpki/private/bazile-fw.evolix.net-1666261809.key 
~~~

Penser à mettre à jour les chemin dans `/etc/openvpn/server.conf` :

~~~
ca   /etc/shellpki/cacert.pem
cert /etc/shellpki/certs/fw.vpn.example.com.crt
key  /etc/shellpki/private/fw.vpn.example.com-1621504035.key
dh   /etc/shellpki/dh2048.pem
~~~

Puis redémarrer le service OpenVPN : `rcctl restart openvpn` sous OpenBSD ou `systemctl restart openvpn@server.service` sous Debian.

Pour indiquer que le dossier `/etc/openvpn/ssl` n'est plus utilisé, il peut être renommé en `/etc/openvpn/ssl-old`.

Penser à mettre à jour le script [cert-expirations.sh](https://gitea.evolix.org/evolix/shellpki/raw/branch/dev/cert-expirations.sh) permettant d'avertir régulièrement des prochaines expirations des certificats clients et serveur :

~~~
# wget https://gitea.evolix.org/evolix/shellpki/raw/branch/dev/cert-expirations.sh -O /usr/share/scripts/cert-expirations.sh
~~~

## Troubleshooting 

### TXT_DB error

À la création d'un certificat, si nous avons l'erreur suivante :

~~~
failed to update database
TXT_DB error number 2
~~~

Il faut éditer le fichier /etc/openvpn/ssl/ca/index.txt et supprimer la ligne concernant le CN qui pose problème.