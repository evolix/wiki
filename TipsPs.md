# Connaître l'uid du process pid=xxx

~~~{.bash}
$ ps -f -p xxx -eo uid | tail -n1 | xargs id
~~~

# Lister tout les process en State=Z (Zombie)

~~~{.bash}
$ ps aux | awk '$8=="Z"' | awk '{ print $2 }'
~~~