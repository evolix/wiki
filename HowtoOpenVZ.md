**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto OpenVZ

OpenVZ permet de compartimenter les processus au sein de conteneurs. Ce n'est pas vraiment de la virtualisation car un seul et même noyau est utilisé par tous les conteneurs.
Cependant un VE (Virtual Environment) ne peut voir que ses processus (un init avec un PID=1 est recréé pour chaque VE, ce qui ne perturbe pas le schéma standard des processus dans le VE).

Le principe de conteneur permet d'obtenir de meilleures performances (un seul noyau chargé en mémoire, accès aux périphériques plus rapide (moins de couches logicielles à traverser))
et de modifier les ressources matérielles allouées à chaud.

Cependant, tous les systèmes virtualisés partagerons la même version du noyau et les mêmes modules.
Il ne peut donc y avoir un système Debian et un FreeBSD sur la même machine physique par exemple.

## Configuration du système hôte (VE0)

Au niveau du partitionement, il peut être utile de créer une partition contenant tous les VE, à monter dans _/var/lib/vz/_, étant donné que les fichiers des VE sont des _vrais fichiers_ pour le système de fichiers hôte (contrairement à d'autres solutions de virtualisation où les fichiers sont dans une image).

Ensuite, on installe le noyau Linux avec le support d'OpenVZ (ne pas oublier de rebooter dessus après l'install avant de continuer) :

~~~
# aptitude install linux-image-2.6-openvz-amd64
~~~

Configuration du réseau : il faut activer le forwarding des paquets :

/etc/sysctl.conf

~~~
net.ipv4.conf.default.forwarding=1
net.ipv4.conf.default.proxy_arp = 0
net.ipv4.ip_forward=1
~~~

Prise en compte de la configuration :

~~~
sysctl -p
~~~

~~~
echo 1 > /proc/sys/net/ipv4/ip_forward
~~~

## Création d'un VE

Il faut avant tous posséder un template du système à installer. On peut les trouver [ici](http://download.openvz.org/contrib/template/precreated/). Un template se présente sous la forme d'une archive contenant le système de fichier du système à installer. Elle doit être placée (non décompressée) dans _/var/lib/vz/template/cache/_.

Ensuite on peut créer un nouveau VE en utilisant ce template :

~~~
vzctl create 1 --ostemplate debian-5.0-amd64-minimal --hostname --ipadd x.x.x.x --hostname foo

~~~
_1_ représente l'id du VE. l'id 0 est réservé au système hôte. l'ostemplate doit correspondre au nom du template sans l'extension. ipadd permet de spécifier l'adresse IP du VE.

On peut également définir une configuration à l'aide de `vzctl set`, par exemple :

~~~
vzctl set 1 --nameserver x.x.x.x
~~~

## Gestion des VE

Démarrer, arréter un VE :

~~~
vzctl (start|stop|restart) VEID
~~~

Se connecter à un VE :

~~~
vzctl enter VEID
~~~

Exécuter une commande sur un VE (depuis l'hôte) :

~~~
vzctl exec VEID COMMAND
~~~

Lister les VE actifs :

~~~
# vzlist
      VEID      NPROC STATUS  IP_ADDR         HOSTNAME                        
         1          6 running x.x.x.x         foo
~~~

Lister les VE actifs *et* éteints :

~~~
# vzlist --all
~~~

Modifier le hostname :

~~~
# vzctl set VMID --hostname foo.evolix.net --save
~~~

Modifier l'IP :

~~~
# vzctl set VMID --ipdel 1.2.3.4 --save
# vzctl set VMID --ipadd 4.3.2.1 --save
~~~

Ajouter de la RAM :

~~~
vzctl set VMID --ram 3072M --save
~~~

## Migration de VE à chaud

Il est possible de migrer un VE d'une machine physique à une autre en utilisant la commande `vzmigrate`.
Avant tout, il faut s'assurer d'avoir le _PermitRootLogin yes_ et une clé SSH autorisée sur la machine cible.

Ensuite, on peut démarrer la migration :

~~~
vzmigrate -v --online TARGET_HOST VEID
~~~

L'option _online_ permet de minimiser le temps indisponibilité du VE (il est en pratique de quelques secondes).

Cette méthode a plusieurs limitations, qui peuvent être gênantes dans certains cas : 

* Impossibilité de renommé l'ID du VE pendant la migration. Si un VE possède déjà le même ID sur la machine cible, la migration échouera ;
* Impossibilité de changer les paramètres réseau (IP, passerelle, DNS...). Si la machine cible se trouve dans un autre réseau, ou bien l'IP du VE est déjà prise, la migration échouera.

Enfin, il faut s'assurer également que les limitations matérielles (quota mémoire, disque...) sont cohérentes avec l'environnement cible.

## Créer et restaurer une snapshot d'un VE

Un snapshot consister à freeze tous les processus et sauvegarder toutes les informations d'état, puis créer une image du VE.
L'image pourra ensuite être restaurer pour que tous les processus se retrouvent exactement dans le même état au moment du freeze.

Freezer le VE :

~~~
# vzctl chkpnt VEID --suspend
~~~

À partir de maintenant la machine devient indisponible. Il faut donc sauvegarder son état :

~~~
# vzctl chkpnt VEID --dump --dumpfile /path/foo
~~~

Enfin, on peut réveiller le VE :

~~~
# vzctl chkpnt VEID --resume
~~~

Pour ce qui est de la réstauration, il faut avant tout stopper le VE si il est actif :

~~~
# vzctl stop VEID
~~~

Ensuite, on peut restaurer le snapshot :

~~~
# vzctl restore VEID --dumpfile /path/foo
~~~

Le VE est alors démarré en utilisant le snapshot et retrouve son ancien état.

## Limiter les ressources utilisées par VE

### Quotas disque

~~~
vzctl set VEID --diskspace SOFT_LIMIT:HARD_LIMIT --save
vzctl set VEID --diskinodes SOFT_LIMIT:HARD_LIMIT --save
~~~

De la même manière que pour les quotas Linux, on peut définir une limite soft et une limite hard.

### Temps CPU

On peut partager le temps CPU de 2 manière différenetes :

* soit on limite un VE à un certain pourcentage d'utilisation du processeur :

~~~
vzctl set VEID --cpulimit POURCENTAGE --save
~~~

* soit on indique une proportion d'utilisation du CPU en fonction des autres VE :

~~~
vzctl set 1 --cpuunits 10 --save
vzctl set 2 --cpuunits 20 --save

~~~
Dans cet exemple, si tous les VE ont besoins du CPU en même temps, OpenVZ accordera 2 fois plus de temps CPU au VE 2. L'avantage de cette méthode est que si le VE 2 n'utilise pas le CPU, le VE 1 peut prendre jusqu'à 100% du temps CPU. Ainsi, il n'y a pas de ressources inutilisées inutilement.
}}}

### Les paramètres UBC

Les UBC (Users BeanCounters) permettent de régler plus finement l'utilisation des ressources par VE. En général (ce n'est pas le cas de tous les paramètres), il faut définir une valeur barrière (limite soft) et une valeur limite (limite hard), dans le même esprit que les quotas Linux.

La liste des UBC peut se trouver [ici](http://wiki.openvz.org/UBC_parameters_table)

## OpenVZ via Proxmox

sources.list :

~~~
deb <http://download.proxmox.com/debian> wheezy pve-no-subscription
~~~

Installer :

~~~
# aptitude install pve-kernel-2.6.32-39-pve proxmox-ve-2.6.32
~~~

Interface d'admin sur le port 8006


Les configurations des VEs se retrouvent dans /etc/pve/nodes/VE0/openvz/VEID.conf

