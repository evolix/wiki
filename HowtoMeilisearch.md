---
categories: databases bdd nosql
title: Howto Meilisearch
...

- Documentation : [https://www.meilisearch.com/docs](https://www.meilisearch.com/docs)
- Code : [https://github.com/meilisearch/meilisearch](https://github.com/meilisearch/meilisearch)
- Licence : [MIT License](https://github.com/meilisearch/meilisearch/blob/main/LICENSE) 
- Langage : Rust

**Meilisearch** est un engin de recherche doté d'une API RESTful. Mis à disposition aux conditions de la licence MIT et écrit en Rust, il se démarque de ses concurrents (Elasticsearch, Algolia, etc. Voir le [tableau comparatif](https://www.meilisearch.com/docs/learn/what_is_meilisearch/comparison_to_alternatives)) par sa rapidité d'exécution et un focus sur l'expérience utilisateur. Les données de Meilisearch sont stockées dans [LMDB](https://www.meilisearch.com/docs/learn/advanced/storage#understanding-lmdb).

# Requis

Le code roule sur [différents systèmes d'exploitation](https://www.meilisearch.com/docs/learn/getting_started/installation#supported-operating-systems) dont les principales distributions de GNU/Linux utilisant `glibc 2.27` ou + sur l'architecture matérielle `amd64/x86_64`.

# Installation

Une [procédure](https://www.meilisearch.com/docs/learn/cookbooks/running_production) d'installation pour Debian est disponible dans la documentation officielle. 

La première étape est d'ajouter le dépôt privé du développeur avant d'installer le paquet Debian :

~~~
# echo "deb [trusted=yes] https://apt.fury.io/meilisearch/ /" | tee /etc/apt/sources.list.d/fury.list
# apt update && apt install meilisearch
~~~

Ensuite on ajoute un nouvel utilisateur UNIX non privilégié qui sera chargé d'exécuter Meilisearch :

~~~
# useradd -d /home/meilisearch -b /bin/false -m -r meilisearch
~~~

Puis on récupère une copie du fichier de configuration principal qu'on ajuste ensuite au besoin :

~~~
# curl https://raw.githubusercontent.com/meilisearch/meilisearch/latest/config.toml > /etc/meilisearch.toml
# chown meilisearch: /etc/meilisearch.toml
# editor /etc/meilisearch.toml
~~~

Voici un exemple de contenu pertinent pour le fichier `/etc/meilisearch.toml` :

~~~
env = "production"
master_key = "YOUR_MASTER_KEY_VALUE"
db_path = "/home/meilisearch/data"
dump_dir = "/home/meilisearch/dumps"
snapshot_dir = "/home/meilisearch/snapshots"
~~~

Pour des raisons de sécurité évidentes, il faut mettre une vraie chaîne aléatoire (générée par exemple via la commande `apg -n1 -m64`) à la place de `YOUR_MASTER_KEY_VALUE`. Aussi, les répertoires cités ci-haut n'existent pas encore, alors il faut les ajouter :

~~~
# sudo -iu meilisearch
$ mkdir data dumps snapshots log
$ exit
~~~

Meilisearch peut être lancé via systemd. On commence par éditer un nouveau fichier de configuration nommé `/etc/systemd/system/meilisearch.service` dans lequel on insère quelque chose comme ceci :

~~~
[Unit]
Description=Meilisearch
After=systemd-user-sessions.service

[Service]
Type=simple
WorkingDirectory=/home/meilisearch
ExecStart=/usr/bin/meilisearch --config-file-path /etc/meilisearch.toml
User=meilisearch
Group=meilisearch

[Install]
WantedBy=multi-user.target

~~~

... et on lance la nouvelle unité systemd après l'avoir activée :

~~~
# systemctl enable --now meilisearch
# systemctl status meilisearch
~~~

Le service Meilisearch est maintenant prêt à fonctionner en local (`127.0.0.1:7700`). On voudra probablement le faire communiquer avec le monde extérieur via un serveur mandataire (*proxy*) comme Nginx, HAProxy ou Apache. La doc officielle [donne un exemple pour nginx](https://www.meilisearch.com/docs/learn/cookbooks/running_production#step-5-secure-and-finish-your-setup). En voici un autre pour Apache, qui aura besoin d'un fichier de configuration pour vhost (disons `/etc/apache2/sites-available/meilisearch.conf`) semblable à celui-ci :

~~~
<VirtualHost *:80 *:443>

    # FQDN principal
    ServerName meilisearch.evolix.net

    # Repertoire principal
    DocumentRoot /home/meilisearch/www/

    # SSL
    SSLEngine On
    SSLCertificateFile /etc/letsencrypt/meilisearch/live/fullchain.pem
    SSLCertificateKeyFile /etc/ssl/private/meilisearch.key

    # Propriete du repertoire
    <Directory /home/meilisearch/www/>
        Require all denied
        Include /etc/apache2/whitelist.conf
    </Directory>

    # user - group 
    AssignUserID meilisearch meilisearch

    # LOG
    CustomLog /var/log/apache2/access.log vhost_combined
    CustomLog /home/meilisearch/log/access.log combined
    ErrorLog  /home/meilisearch/log/error.log

    # REDIR HTTPS
    RewriteEngine On
    RewriteCond %{HTTPS} !=on
    RewriteCond %{HTTP:X-Forwarded-Proto} !=https
    RewriteRule ^ https://%{HTTP:Host}%{REQUEST_URI} [L,R=permanent]

    ProxyPreserveHost On
    ProxyPass / http://localhost:7700
    ProxyPassReverse / http://localhost:7700
    <Proxy *>
        Require all denied
        Include /etc/apache2/whitelist.conf
    </Proxy>

</VirtualHost>
~~~

Le vhost donné en exemple ci-haut écoute sur les ports 80 et 443 (SSL), force la redirection sur `https://` et n'autorise l'accès qu'à une liste d'adresses IP de confiance. Aussi, la directive `AssignUserID` suppose l'installation de Apache 2 MPM ITK. L'activation du vhost se fait de la manière suivante :

~~~
# a2ensite meilisearch
# apache2ctl -t && { systemctl reload apache2.service; systemctl status apache2.service; }
~~~

Voir [HowtoApache](HowtoApache) au besoin pour plus d'informations.

# Mise à jour

~~~
# apt-get update
# apt-get upgrade meilisearch
# systemctl restart meilisearch
~~~

# Règles de classement

Meilisearch utilise l'algorithme de tri par paquets (*bucket sort*) pour classer les documents qui correspondent aux mots-clés des requêtes. 

Il y a six règles de classement par défaut (dans cet ordre) :


* "words", = **nombre décroissant de termes de la requêtes qui correspondent** aux documents indexés. Retourne en premier les documents qui contiennent tous les termes.
* "typo", = **nombre croissant de typos**. Retourne en premier les documents qui contiennent les termes de la requête avec le moins de typos.
* "proximity", = **distance croissante entre les termes de la requêtes**. Retourne en premier les documents pour lesquels les termes de la requête sont près l'un de l'autre et dans le même ordre que la requête.
* "attribute", = les résultats sont triés suivant **l'ordre de classement des attributs** des documents indexés. Retourne en premier les documents qui contiennent les termes de la requête dans les attributs les plus importants (exemple : titre du document). Voir https://www.meilisearch.com/docs/learn/core_concepts/relevancy#attribute-ranking-order
* "sort", = les résultats sont triés suivant **les paramètres décidés au moment de la requête**. (pas activé par défaut)
* "exactness" = les résultats sont triés selon **la ressemblance des mots des documents avec les mots de la requête**. Retourne en premier les documents qui contiennent exactement les mêmes termes.

Voir des exemples du fonctionnement de diverses règles de classement : https://www.meilisearch.com/docs/learn/core_concepts/relevancy#examples

# Utilisation

Voir le [guide de démarrage rapide](https://www.meilisearch.com/docs/learn/getting_started/quick_start) pour apprendre comment ajouter des documents dans la base de données de Meilisearch, gérer l'indexation et effectuer des recherches. 

  * Lister les paramètres de réglage de l'instance

~~~
curl -s -X GET https://$DOMAIN/indexes/$INDEXE/settings -H "Authorization: Bearer $KEY" |jq '' -r
~~~

  * Supprimer tout ce qui est liée à un filtre

~~~
curl -s https://$DOMAIN/indexes/$INDEXE/documents/delete \
  -H "Authorization: Bearer $KEY" -H 'Content-Type: application/json' \
  --data-binary '{ "filter": "source = redmine" }' |jq '' -r
~~~

  * Lister les attribues de filtrage

~~~
curl -s -X GET https://$DOMAIN/indexes/$INDEXE/settings/filterable-attributes \
  -H "Authorization: Bearer $KEY" -H 'Content-Type: application/json' |jq '' -r
~~~

  * Appliquer des attribues de filtrage

~~~
curl -s -X PUT https://$DOMAIN/indexes/$INDEXE/settings/filterable-attributes \
  -H "Authorization: Bearer $KEY" -H 'Content-Type: application/json' \
  --data-binary '["source", "categories"]' |jq '' -r
~~~
