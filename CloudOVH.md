## Créer un utilisateur

Pour effectuer des tâches en utilisant l'API OpenStack ou l'interface Horizon, il faut d'abord créer un utilisateur OpenStack. Pour se faire, il faut, dans un projet, cliquer sur les boutons suivant comme [documenté par OVH](https://docs.ovh.com/fr/public-cloud/creation-et-suppression-dun-utilisateur-openstack/#en-pratique) :

~~~
Public Cloud -> Users & Roles -> Créer un utilisateur
~~~

## Préparations des outils

Cloud OVH utilise OpenStack, pour contrôler les instances en ligne de commande il faut donc installer les outils OpenStack comme [OVH le documente](https://docs.ovh.com/fr/public-cloud/preparer-lenvironnement-pour-utiliser-lapi-openstack/). Pour avoir différentes verision des ces outils on utilise virtualenv pour séparer ces logiciels du reste du systême :

~~~
virtualenv venv
. ./venv/bin/activate
pip3 install --upgrade pip
pip3 install python-openstackclient python-novaclient python-swiftclient python-cinderclient python-heatclient
~~~

Maintenant que les outils sont installés, il faut récupérer la configuration pour utiliser le cloud OVH en récupérant un fichier `openrc.sh` pour l'utilisateur que vous souhaitez utiliser. La [documentation OVH](https://docs.ovh.com/fr/public-cloud/charger-les-variables-denvironnement-openstack/) indique de cliquer sur :

~~~
Project Management -> Users & Roles -> ... (à droite de l'utilisateur voulu) -> Télécharger le fichier RC d'OpenStack
~~~

Une fois le fichier `openrc.sh` obtenu, il ne reste plus qu'à le sourcer dans votre shell, entrer le mot de passe de l'utilisateur OpenStack puis exécuter votre première commande :

~~~
. openrch.sh
nova list
~~~

## Utilisation des outils OpenStack

Voir la [documentation OpenStack](HowtoOpenStack).

## Adressage IP

Attention, pour chaque sous-réseau (public ou privé) il ne faut *pas* utiliser (outre NetID et Broadcast évidemment) les 3 adresses les plus hautes.

Par exemple, pour 172.21.22.0/24 :

~~~
172.21.22.0 : NetID 
172.21.22.252 : réservé pour OVH
172.21.22.253 : réservé pour OVH
172.21.22.254 : gateway OVH
172.21.22.255 : Broadcast OVH
~~~
