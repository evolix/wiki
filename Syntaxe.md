---
categories: network web
toc: no
title: Conventions pour le wiki Evolix
...

# Conventions de syntaxe

<http://daringfireball.net/projects/markdown/basics>

## Metadata

    ---
    categories: network web
    toc: no
    title: Haskell and Category Theory
    ...

## Syntaxe

Voir [Help#markdown]()

Commandes Shell (sans output) :

    ~~~
    $ apt-cache search foo
    # apt install foo
    ~~~

Commandes Shell (avec output) :

    ~~~
    $ dmesg
    [8044916.823878] [IPTABLES DROP] : IN=eth0 OUT= MAC=00:00:00:28:71:00:00:54:00:8f:01:00:00:00 SRC=192.0.2.1 [...]
    ~~~

Fichiers avec coloration syntaxique si disponible (voir ci-dessous) :

    ~~~{.apache}
    ServerName www.example.com
    ~~~~

    ~~~{.bash}
    #!/bin/bash
    echo toto
    ~~~~

Liens externes :

    <http://www.example.com/>
    [Lien vers Example](http://www.example.com/)

Liens internes absolus (conseillés) :

    [/HowtoFoo]()
    [/HowtoFoo#name]()
    [#name]()

Liens internes relatifs (attention aux liens morts en cas de déplacement de la page) :

    [HowtoFoo]()
    [HowtoFoo#name]()
    [#name]()

> La syntaxe [nomLien](/HowtoFoo#name) est aussi valide, on peut mettre le lien dans les parenthèses et le nom dans les crochets (ce qui permet d'avoir l'autocompletion dans vscode notamment).

### Syntaxes Markdown

*italique* **gras** ~~barré~~  H~2~O m^2^

Pour forcer un saut de ligne :
terminer sa ligne avec deux espaces !

Pour afficher le "symbole" `/!\`, il est important d'échaper le dernier caractère : `/!\\`.

## Conventions

### Conventions de syntaxe

Une commande lancée en root sera précédée de **#** ; en utilisateur elle sera précédée de **$**.
Dans la mesure du possible, le maximum de commande devra être lancée en tant qu'utilisateur.

~~~
$ apt-cache search foo
# apt install foo
~~~

Si on montre plusieurs commandes dans un seul bloc, et qu'on veut également montrer leur sortie, il convient de sauter une ligne entre chaque commande et la fin de la sortie de la précédente.

On évitera d'avoir des lignes trop longues dans les commandes Shell ou fichiers sur le wiki, car cela devient illisible en HTML.
On mettra ainsi les commandes Shell sur plusieurs lignes si besoin, on utilisera *[...]* si nécessaire, etc.

Si l'on doit afficher une commande courte dans le paragraphe du texte, par exemple `/etc/init.d/foo restart`, on utilise les backticks « \`/etc/init.d/foo restart\` ». On peut utiliser la même chose pour les noms de fichier, tel `foo.bar`.

Si l'on doit utiliser un utilisateur quelconque, on utilisera le login **jdoe** (pour John Doe).

Si l'on doit mentionner un mot de passe, on choisira **PASSWORD** (en majuscule).

Si l'on doit mentionner un fichier quelconque, on choisira **FICHIER** (en majuscule).

Si l'on doit utiliser un terme quelconque, on privilégiera **foo** et **bar** (puis baz, qux, quux, corge, grault, garply, waldo, fred, plugh, xyzzy, thud). Ainsi, pour un chemin quelconque, on utilisera par exemple `cp /foo/FICHIER /bar/`.

Si l'on utilise des adresses IP, on utilisera des IPv4 en **192.0.2.0/24**, **198.51.100.0/24**, ou **203.0.113.0/24** [RFC5737](https://tools.ietf.org/html/rfc5737#section-3) et des IPv6 en **2001:db8::/32** [RFC3849](https://tools.ietf.org/html/rfc3849).

Les adresses IPv6 et les adresses MAC auront leurs caractères alphabétiques en minuscule.

Si l'on utilise un système autonome, on utilisera un AS entre **64496 et 64511** ou entre **65536 et 65551** [RFC5398](https://tools.ietf.org/html/rfc5398).

Si l'on utilise des noms de domaine, on utilisera **example.com** [RFC2606](https://tools.ietf.org/html/rfc2606) notamment pour les emails, URLs ou serveurs à atteindre.

Pour relancer un service : `/etc/init.d/foo restart` OU `systemctl restart foo`. (??)

Pour installer un paquet sous Debian, on utilise la commande `apt` → `apt install foo`.

Par défaut on considère que l'on écrit de la documentation pour la version *stable* d'OpenBSD ou de Debian. Des remarques **obligatoires** doivent être faites pour Debian *oldstable*. Des remarques éventuelles peuvent être faites pour les version précédentes (*oldoldstable*, *oldoldoldstable*, etc). Ces remarques devraient être mises sous forme de "notes" du type :

> *Note* : Pour Debian 8 et 9, on peut faire :
>
> ~~~
> # aptitude install foo
> ~~~

### Conventions de forme

* Il doit y avoir des metadatas avec au minimum "categories" et "title".
* La 2<sup>e</sup> ligne de la documentation doit être un choix réfléchi de catégories en fonction des catégories déjà existantes.
* Si une documentation officielle existe, elle doit être notée tout en haut sous la forme : `Documentation : <lien>`.
* Si un rôle Ansible existe, il doit être noté avec le lien vers la branche stable sur la forge sous la forme : `Rôle Ansible : <lien>`.
* À la fin de la liste commençant par la documentation officielle, il doit y avoir un point `Statut de cette page : prod/test / version` où "version" est la version de Debian (ou OpenBSD) visée par la documentation (si applicable) et on mettra "prod" si les principales phases de la documentation (installation et configuration au moins) ont été jouées au moins 1 fois sur un serveur de prod tournant sous "version" (sinon on notera "test").
* La description doit être succinte (entre 3 et 10 lignes) ET subjective d'un point de vue Evolix
* Un lien vers le site officiel doit être positionné sur la 1<sup>ère</sup> occurence du nom de la techno dans la description.
* Une section installation doit être présente. On considère que l'on utilise la dernière version de Debian et/ou d'OpenBSD par défaut, si besoin on précise les exceptions pour des releases précédentes comme précisé plus haut.
* Dans la section installation, on montrer la commande apt d'installation, mais un moyen d'obtenir la version installée en temps qu'utilisateur non-root et l'état de l'éventuel démon avec sysctemctl status SAUF la ligne "active".
* En général, il doit y avoir au moins les titres : Installation / Configuration / Administration / Optimisation / Plomberie / Monitoring (avec sous-titres Munin et Nagios) / FAQ.
* L'organisation des titres/sous-titres doit être cohérente quand on regarde la table des titres.
* Les conventions de syntaxe doivent être appliquées partout (voir ci-dessous) notamment pour les adresses IP, les "Note :", les fichiers sous la forme `/etc/foo.conf`, etc.
* Les exemples de code ou contenu de fichiers doivent utiliser la syntaxe avec trois tildes.
* Si l'on mentionne d’autres documentations, mettre des liens internes.
* Objectif zéro faute d'orthographe ou de grammaire.
* Tout ce qu'on a mis en production sur des serveurs doit être mentionné.
* Quand on mentionne des commandes avec des arguments, il faut mettre les versions longues (exemple: `--dry-run` au lieu de `-d` si possible).
* Si la doc est longue, on hésitera pas à avoir un titre "Configuration de base" et "Configuration avancée", ou alors un sous-titre "Utilisation de base" avec les commandes de base si l'on doit lire la doc en 1 minute.
* Si la doc est très longue, elle peut être découpée en sous-pages à l'exemple du [HowtoMySQL]().
* On peut faire des liens vers des manpages via le site <https://manned.org/>, par exemple <https://manned.org/man/debian/journalctl> pour la dernière version d'une commande sous Debian, ou <https://manned.org/man/debian-bookworm/journalctl> pour la version bookworm
* On peut faire des liens vers des RFC en utilisant <https://tools.ietf.org/html/rfc1> avec avec des ancres vers des chapitres de la forme <https://tools.ietf.org/html/rfc4978#section-3>

À titre d'exemple, on pourra prendre le [HowtoApache]().

Pour s'aider, on peut partir d'un template :

~~~
---
categories: voir https://wiki.evolix.org/_categories
title: Howto Foo
...

* Documentation : <https://foo.io/doc>
* Rôle Ansible : <https://forge.evolix.org/projects/ansible-roles/repository/show/foo>

[Foo](https://foo.io/) est un logiciel qui...

## Installation

~~~
# apt install foo

$ /usr/sbin/foo -v

# systemctl status foo
~~~

## Configuration

## Administration

## Optimisation

## Monitoring

### Nagios

### Munin

## Plomberie

## FAQ
~~~


## Coloration syntaxique

~~~
$ ./.cabal/bin/pandoc -v
pandoc 1.17.2
Compiled with texmath 0.8.6.5, highlighting-kate 0.6.2.1.
Syntax highlighting is supported for the following languages:
    abc, actionscript, ada, agda, apache, asn1, asp, awk, bash, bibtex, boo, c,
    changelog, clojure, cmake, coffee, coldfusion, commonlisp, cpp, cs, css,
    curry, d, diff, djangotemplate, dockerfile, dot, doxygen, doxygenlua, dtd,
    eiffel, elixir, email, erlang, fasm, fortran, fsharp, gcc, glsl,
    gnuassembler, go, hamlet, haskell, haxe, html, idris, ini, isocpp, java,
    javadoc, javascript, json, jsp, julia, kotlin, latex, lex, lilypond,
    literatecurry, literatehaskell, llvm, lua, m4, makefile, mandoc, markdown,
    mathematica, matlab, maxima, mediawiki, metafont, mips, modelines, modula2,
    modula3, monobasic, nasm, noweb, objectivec, objectivecpp, ocaml, octave,
    opencl, pascal, perl, php, pike, postscript, prolog, pure, python, r,
    relaxng, relaxngcompact, rest, rhtml, roff, ruby, rust, scala, scheme, sci,
    sed, sgml, sql, sqlmysql, sqlpostgresql, tcl, tcsh, texinfo, verilog, vhdl,
    xml, xorg, xslt, xul, yacc, yaml, zsh
~~~
