**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

= Adaptec Series 7=

## Check Raid

Utiliser le aacraid-status de hw-raid : <http://hwraid.le-vert.net/wiki/DebianPackages>
Télécharger arcconf : <http://download.adaptec.com/raid/storage_manager/arcconf_v1_8_21375.zip>
Dézipper et copier arcconf dans /usr/sbin
Patcher aacraid-status : <http://paste.evolix.org/paste/30848/raw/>


Résumé : 

~~~
# wget <http://download.adaptec.com/raid/storage_manager/arcconf_v1_8_21375.zip>
# unzip arcconf_v1_8_21375.zip -d arcconf
# install -m755 arcconf/linux_x64/cmdline/arcconf /usr/sbin/
# wget <http://paste.evolix.org/paste/30848/raw/> -O /tmp/aacraid-status.patch
# patch /usr/sbin/aacraid-status < /tmp/aacraid-status.patch
~~~