---
title: Guide pour enregistrer une présentation
---

Il nous arrive de faire des présentations (en interne ou en public) et de vouloir les enregistrer.

Il nous est rarement possible d'avoir un opérateur et nous avons peu de matériel. Voilà ce que nous avons expérimenté et quelques idées d'amélioration.

**Avertissement : Ce guide est imparfait, pas encore finalisé et n'est que le reflet de nos essais (et pas de l'état de l'art).
Nous sommes preneurs de vos idées, commentaires et retours d'expériences sur le sujet.**

## Affichage des diapositives

Selon que nous utilisons [Reveal.js](http://lab.hakim.se/reveal-js/), [LibreOffice Impress](http://www.libreoffice.org/discover/impress/) ou d'autres solutions, nous privilégions les outils de présentation qui proposent un "écran du présentateur". Ça permet de voir des notes, quelle est la diapositive d'après, le temps passé…

Il faut donc régler son ordinateur pour afficher l'écran du présentateur sur l'écran interne (pour un ordinateur portable) et la vue publique sur le vidéo-projecteur.

Nous utilisons beaucoup [i3](HowtoI3) comme gestionnaire de fenêtres.

Pour un Lenovo X1 Carbon v3, nous configurons notre affichage avec Xrandr et ce script bien utile :

~~~{.bash}
#!/bin/sh

internal_display="eDP1"
internal_mode="--mode 1360x768"
external_display=${1-"HDMI2"}
external_position=${2-"right"}
external_mode=${3-"--auto"}

/usr/bin/xrandr --output ${internal_display} ${internal_mode}
/usr/bin/xrandr --output ${external_display} --${external_position}-of ${internal_display} ${external_mode}
~~~

L'écran interne est réglé sur une résolution plus basse que celle par défaut, afin de bien voir, y compris à plus d'1m.

L'écran externe est réglé en mode automatique, idéalement "1920×1080" (format dit [Full-HD](https://fr.wikipedia.org/wiki/Full_HD)).

**Idées d'amélioration**

Il serait pratique d'avoir un script fiable pour basculer ponctuellement en mode "recopie vidéo" dans une situation de démo en direct, pour éviter de se tordre le cou.

## Vidéo/son des diapositives

Pour capter la vidéo de ce qui est présenté (sur le projecteur), nous utilisons [FFmpeg](http://ffmpeg.org).

### Vidéo

**Note: la partie "vidéo" n'a pas encore été testée.**

#### Capture

Le flux vidéo est géré par [x11grab](https://ffmpeg.org/ffmpeg-devices.html#x11grab). Il faut lui indiquer quelle partie de l'affichage il faut capturer :

Avec un écran interne (à gauche) d'une résolution de "1360×768" et un vidéo-projecteur d'une résolution de "1920x1080", les paramètres FFmpeg sont :

~~~
$ ffmpeg […] -video_size 1920x1080 -framerate 30 -f x11grab -i :0.0+1360,0 […]
~~~

On indique donc une zone de capture de la taille de l'écran externe, décalée de 1360 pixels vers la droite. Sans ce décalage, x11grab capturait à partir du coin haut-gauche de l'écran interne.

Il est possible de générer ces dimensions et positions de manière automatique :

~~~{.bash}
#!/bin/sh

screen_name=${1:-HDMI2}

xrandr_info=$(xrandr | grep ${screen_name})
screen_state=$(echo ${xrandr_info} | cut -d ' ' -f 2)

if [[ $screen_state != "connected" ]]; then
    echo "Screen ${screen_name} is disconnected"
    exit 1
fi

screen_geometry=$(echo ${xrandr_info} | cut -d ' ' -f 4)
screen_size=$(echo ${screen_geometry} | cut -d '+' -f 1)
screen_pos_x=$(echo ${screen_geometry} | cut -d '+' -f 2)
screen_pos_y=$(echo ${screen_geometry} | cut -d '+' -f 3)
screen_pos="${screen_pos_x},${screen_pos_y}"

ffmpeg […] -video_size ${screen_size} -framerate 30 -f x11grab -i :0.0+${screen_pos} […]
~~~

Il est possible de limiter le "framerate" (nombre d'images capturées par seconde) si vous constatez des frames perdues et si la présentation capturée le permet. Préférez alors 25 ou 15 images par secondes pour limiter les soucis d'interpolation de frames au montage.

#### Encodage

Le choix du codec vidéo est large, selon ce qu'on veut faire avec la vidéo.

Nous conseillons un codec "lossless" (sans perte) et dont la compression ne se fait pas entre frames, afin de faciliter le travail du logiciel de montage.

Nous verrons plus loin la question du montage, mais notre notre première tentative de montage s'est faite sur iMovie — un logiciel de montage propriétaire d'Apple. Nous avons donc choisi un codec approprié : [ProRes](https://trac.ffmpeg.org/wiki/Encode/VFX#Prores).

Le détail du support sous Linux de la partie encodage reste à valider.

~~~
$ ffmpeg […] -c:v prores […]
~~~

### Audio

#### Capture

Pour le son, nous avons testé l'utilisation d'un micro [Yeti](http://www.bluemic.com/products/yeti/), placé proche du ou des présentateurs (entre 70 cm et 2 m), en mode cardioïde, avec un gain d'environ 40%.

C'est encore FFmpeg qui assure la captation de ce flux, via PulseAudio. Il faut commencer par déterminer quelle est l'entrée audio à capturer. Nous en avons fait un script appelé `pulse_input_yeti` :

~~~{.bash}
#!/bin/sh

echo "list-sources" | \
    pacmd | \
    grep name: | \
    grep Yeti | \
    grep -v monitor | \
    cut -d' ' -f2 | \
    sed -e 's/^.\(.*\).$/\1/'
~~~

Il donnera une sortie ressemblant à `alsa_input.usb-Blue_Microphones_Yeti_Stereo_Microphone_REV8-00-Microphone.analog-stereo`.

~~~
$ ffmpeg -f pulse -i $(pulse_input_yeti) […]
~~~

**Idées d'amélioration**

Nous envisageons d'utiliser un ou plusieurs micro-cravates pour une meilleur captation du ou des présentateurs. Pour éviter le risque du fil qui se balade, on peut faire une captation avec le micro-cravate branché sur un smartphone. La piste son sera alors récupérée séparément pour le montage.

#### Encodage

Comme pour la vidéo, si on doit faire un montage par la suite, il vaut mieux choisir un codec sans perte.

~~~
$ ffmpeg […] -c:a pcm_s16le […]
~~~

### Résumé

Nous faisons là une capture du vidéo-projecteur en _Full-HD_ et du micro Yeti branché en USB.

~~~{.bash}
#!/bin/sh

output_file=/localhome/jlecour/tmp/capture.mov
screen_name=${1:-HDMI2}

mic_input=$(pulse_input_yeti)

if [ -z ${mic_input} ]; then
    echo "Yeti microphone is disconnected"
    exit 1
fi

xrandr_info=$(xrandr | grep ${screen_name})
screen_state=$(echo ${xrandr_info} | cut -d ' ' -f 2)

if [ $screen_state != "connected" ]; then
    echo "Screen ${screen_name} is disconnected"
    exit 2
fi

screen_geometry=$(echo ${xrandr_info} | cut -d ' ' -f 4)
screen_size=$(echo ${screen_geometry} | cut -d '+' -f 1)
screen_pos_x=$(echo ${screen_geometry} | cut -d '+' -f 2)
screen_pos_y=$(echo ${screen_geometry} | cut -d '+' -f 3)
screen_pos="${screen_pos_x},${screen_pos_y}"

echo "Starting capture of ${screen_name} (${screen_size}) and Yeti microphone to ${output_file}"
echo "=========="

ffmpeg -f pulse -i $(pulse_input_yeti) -video_size ${screen_size} -framerate 30 -f x11grab -i :0.0+${screen_pos} -c:v prores -c:a pcm_s16le ${output_file}
~~~

Nous recommandons que FFmpeg enregistre son résultat sur un disque rapide pour éviter les pertes de _frames_.

## Vidéo/son d'ensemble

Nous avons utilisé une caméra Panasonic HC-V770 en mode 1080p et son "5.1".

Le son "5.1" est plutôt anecdotique, mais cela signifie que le son capturé vient de tout autour de la caméra, qui a été placée en milieu de pièce, avec du public tout autour.

La caméra était branchée au secteur et fixée sur pied afin de n'avoir aucun mouvement.

Nous conseillons bien sûr une carte SD rapide et de grande capacité. Par exemple, nous avons utilisé une SD-XC de 64Go de classe 10 (80 Mo/s).

## Montage

### Multi-pistes

Disposer de plusieurs pistes vidéo permet de varier l'élément mis en valeur : l'écran pour une lecture détaillée ou bien le présentateur pour une vue générale.

Idem pour l'audio : la voix du présentateur est la plus important donc elle doit être bien captée, mais les remarques et questions du public sont aussi importantes pour un rendu fidèle de la présentation, surtout lorsqu'il y a une vraie discussion.

### Logiciel

L'offre en terme de logiciels de montage vidéo sous Linux est malheureusement très pauvre. Dans un objectif de productivité et de pragmatisme, nous avons opté pour la simplicité d'[iMovie](http://www.apple.com/imovie/) (logiciel propriétaire d'Apple).

Cependant, nous souhaitons le plus possible utiliser des logiciels libres donc nous tenterons d'autres solutions.

Il semble que [Blender](https://www.blender.org) (en mode vidéo, pas en mode 3D) soit une alternative crédible sur le plan fonctionnel, malgré une complexité importante.

[Kdenlive](https://kdenlive.org) semble aussi une bonne alternative à iMovie.

## Diffusion

Pour la diffusion, nous avons pour le moment essayé YouTube et DailyMotion.

Ce type de plateformes offre l'avantage de la bande passante "gratuite" et surtout d'un _player_ avancé : choix de la qualité, facilité à embarquer sur une page… Un seul encodage en pleine qualité est suffisant, c'est la plateforme qui se charge du reste.

Il reste à déterminer les éventuels réglages optimaux pour un export sur YouTube ou autre, afin de leur éviter tout recodage de la version HD et donc sans perte de qualité.