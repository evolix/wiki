---
categories: openbsd network
title: Howto Ifstated
---

* Documentation : <http://man.openbsd.org/man5/ifstated.conf.5>

Ifstated est un démon qui exécute des commandes en réponse aux changements d'état du réseau, qu'il détermine en surveillant l'état des interfaces ou en effectuant des tests externes.

## Configuration

Ifstated fait partie du système de base, il est donc présent par défaut sous OpenBSD. On le configure via le fichier `/etc/ifstated.conf`.

On le lance de manière habituelle :

~~~
# rcctl enable ifstated
# rcctl start ifstated
~~~

Sa configuration se compose de 3 sections principales : la configuration globale, les macros/variables, et la définition des états.

### Exemple de configuration

~~~
# Configuration globale
init-state neutral  # Indique l'état initial. Si non indiqué, le premier état défini sera l'état initial.

# Macros
dmz_if_up = 'em2.link.up'
dmz_if_down = 'em2.link.down'

syslog_ok = '"ping -q -c 1 -w 1 192.168.1.10 >/dev/null 2>&1" every 10' # Un ping toutes les 10 secondes

# Définition des états
state neutral {
    if $dmz_if_down {
        run "logger -st ifstated 'interface to syslog server em2 is down'"
        set-state demoted
    }
    if ! $syslog_ok {
        run "logger -st ifstated 'could not reach syslog server'"
        set-state demoted
    }
}

state demoted {
    init {
        run "ifconfig -g carp carpdemote"
    }
    if $dmz_if_up && $syslog_ok {
        run "logger -st ifstated 'syslog server is ok again'"
        # remove our carp demotion
        run "ifconfig -g carp -carpdemote"
        set-state neutral
    }
}
~~~

Cette configuration est un exemple pour 2 firewalls redondants utilisant [CARP]() et une interface `em2`, à travers laquelle les 2 firewalls peuvent se joindre, et également joindre un serveur syslog ayant l'IP 192.168.1.10.

On commence en étant dans l'état "neutral". Si ifstated détecte que l'interface em2 est down, ou que la commande ping échoue, alors on le log, puis on passe à l'état "demoted". À l'initialisation de l'état "demoted", on lance une commande pour forcer une bascupe CARP. Ensuite, si l'interface em2 est détectée up et qu'en même temps, la commande ping est un succès, alors on le log, on remet à 0 le compteur demote pour revenir à l'état initial, puis on repasse à l'état "neutral".

### Surveiller l'état d'une interface

Les interfaces peuvent être surveillées en suivant ce format  : \<nom_interface\>.link.\<état\>.

Une interface peut avoir 3 états : up, down, ou unknown. Pour une interface CARP, up signifie un état master, et down signifie un état backup.