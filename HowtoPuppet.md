**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Puppet

## Mise en place de l'infra

Puppet fonctionne avec un _puppetmaster_, qui centralise la configuration à déployer sur les différents nœuds, sur lesquels tournent des agents Puppet.

### Puppet master

#### Installation

~~~
# aptitude install puppetmaster
~~~

On peut d'ores-et-déjà lancer le démon :

~~~
# /etc/init.d/puppetmaster start
~~~

### Agents Puppet

#### Installation

~~~
# aptitude install puppet
~~~

#### Configuration

Activer l'agent Puppet dans `/etc/default/puppet.conf` :

~~~
START=yes
~~~

Ajouter l'adresse du puppetmaster dans `/etc/puppet/puppet.conf` :

~~~{.ini}
[agent]
server=puppetmaster.example.net
~~~

Puis :

~~~
# /etc/init.d/puppet start
~~~

La communication entre l'agent et le maître Puppet se fait en SSL. Lors du premier lancement, l'agent Puppet va alors envoyer automatiquement une demande de certificat (CSR) au maître.

On peut s'assurer de la bonne connexion entre l'agent et le maître :

~~~
# puppet agent --test
~~~

À ce stade, la commande doit retourner une erreur SSL disant que le certificat ne peut être vérifié.

Sur le maître, on peut lister les certificats ainsi :

~~~
# puppet cert --list
~~~

Et signer le bon certificat :

~~~
# puppet cert --sign client.example.net
~~~

## Utilisation de Puppet

### Les Manifests

Les fichiers Manifests (extension `.pp`) sont les « programmes » Puppet. Ils permettent de déclarer des ressources.

Le fichier de base est `/etc/puppet/manifests/site.pp`. Généralement, il est bon d'éclater les diverses déclarations dans d'autres fichiers/répertoires, et de ne faire que des inclusion dans ce fichier.

Exemple de fichier `site.pp` :

~~~
import "nodes"
~~~

Ici, on indique d'inclure le fichier `/etc/puppet/manifest/nodes.pp`, qui contiendra la déclaration des différents nœuds recevant leur configuration.

Exemple de fichier `nodes.pp` :

~~~
node myhost.example.net {
    # déclaration spécifique à ce nœud
}

node myhost2.example.net {
    # déclaration spécifique à ce nœud
}
~~~

À noter qu'il est important que le fqdn du nœud qui se connecte au master corresponde à `myhost.example.net` (dans l'exemple) (cela peut néanmoins se configurer).

## Exemple

### Tester un manifest

Créer le manifest suivant, test.pp

~~~
 file {'testfile':
      path    => '/tmp/testfile',
      ensure  => present,
      mode    => 0640,
      content => "I'm a test file.",
    }
~~~

Pour vérifier, on peut l'appliquer directement sur le serveur par exemple, avec la commande suivante :

~~~
puppet apply test.pp
notice: /Stage[main]//File[testfile]/ensure: created
~~~

Modifier les droits du fichiers, puis re-faites un `apply` :

~~~
chmod 777 /tmp/testfile
puppet apply test.pp
notice: /Stage[main]//File[testfile]/mode: mode changed '777' to '640'
~~~

### Manifests en Master/Agent (Serveur/Client)

Sur le serveur il faut faire une liste des nœuds (clients) et quels manifests leur affecter.

/etc/puppet/manifests/site.pp

~~~
class test{
 file {'testfile':
      path    => '/tmp/testfile',
      ensure  => present,
      mode    => 0640,
      content => "I'm a test file.",
    }
}
node client {
    include test
}
~~~


### Distribution des fichiers via puppet

~~~
#/etc/puppet/fileserver.conf
[files]
path /etc/puppet/files
allow 192.168.0.0/16
~~~

~~~
class test{
 file {'testfile':
      path    => '/tmp/testfile',
      ensure  => present,
      mode    => 0640,
      source => "puppet://master.mondomaine.com/files/apps/sudo/sudoers"
    }
}
~~~
