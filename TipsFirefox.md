---
categories: web tips
...

# Astuces Firefox

## Bookmark dynamique

Créer un bookmark vers une URL du type <https://www.google.fr/?q=%s> avec le "keyword" gg

Vous pouvez ainsi chercher dans votre toolbar : `gg test` , `%s` sera substitué par `test` 

et vous aurez une recherche Google !

> A savoir : on ne peut pas avoir plusieur bookmarks dynamique avec la meme URL, cela dit on peut rajouter `&` à la fin de l'URL pour les differentier et profiter d'avoir plusieur bookmarks dynamique qui arrivent au meme endroit !

La substitution avec `%s` ne marche pas sur la partie nom de domaine de l'URL

Pour que ça marche quand même, on peut utiliser une fonction javascript pour l'URL : `javascript:(function(){location='https://%s.test.net/'})()`

## Avoir un Flash récent

Le support de Flash pour Linux se poursuit avec "Pepper Flash Player" qui est désormais téléchargeable sur <https://get.adobe.com/flashplayer/otherversions/> avec la version **PPAPI** compressé en tar.gz

Suite au téléchargement du fichier, voici la procédure à suivre :

~~~
# tar xf flash_player_ppapi_linux.x86_64.tar.gz
# mkdir /usr/lib/pepperflashplugin-nonfree/
# cp libpepflashplayer.so /usr/lib/pepperflashplugin-nonfree/
~~~

Il faut vérifier qu'il y ait bien l'option contrib dans jessie-backports :

~~~
# cat /etc/apt/sources.list.d/backports.list 
# deb http://ftp.debian.org/debian jessie-backports contrib main
# apt update
~~~

Puis on installe le plugin suivant :

~~~
# apt install browser-plugin-freshplayer-pepperflash
~~~

Note 1 : browser-plugin-freshplayer-pepperflash est dans jessie-backports et stretch

Note 2 : au lieu de télécharger le plugin, on peut installer le paquet pepperflashplugin-nonfree de testing, cela fonctionne pour Jessie/Stretch : <https://packages.debian.org/testing/amd64/pepperflashplugin-nonfree/download>

## plugin Java

~~~
# apt install icedtea-7-plugin
~~~

## Gestionnaire de profils

~~~
$ firefox --ProfileManager
~~~

## Force l'usage d'un serveur DNS 

Depuis la barre de recherche : about:config

Puis définir cette option :

~~~
network.dns.localDomains = 9.9.9.9
~~~

Ou mettre 127.0.0.1 pour que le système puisse faire une résolution en local et lire le fichier /etc/hosts.

## Forcer l'affichage en punycode des noms de domaines internationalisés 

Aller dans about:config et forcer `network.IDN_show_punycode` à `true`

## Mettre Firefox en RAM

Si l'on a des problèmes de performances disques, ou que l'on utilise du NFS pour son /home, il peut être intéressant de minimiser les accès disques via l'utilisation de la RAM.

Pour désactiver le cache disque et utiliser la RAM, aller dans about:config et forcer :

- browser.cache.disk.enable => false
- browser.cache.memory.enable => true

## Utiliser l'édition développeur (déconseillé)

On commence par la télécharger sur le site officiel : https://www.mozilla.org/fr/firefox/developer/

Décompresser le fichier dans le dossier /opt/ :

~~~
$ sudo  tar xjf firefox-*.tar.bz2 -C /opt
~~~

Créer un lien symbolique du binaire :

~~~
# ln -s /opt/firefox/firefox /usr/bin/firefox-dev
~~~

Il est maintenant possible de le lancer depuis son terminal avec firefox-dev

## Supprimer l'historique navigateur 

Notamment utile pour oublier le mot de passe saisie lors d'une authentification via htpasswd 

~~~
Ctrl + Shift + Del
~~~

## Downgrade de profil

Si besoin de downgrade son profil :

~~~
$ MOZ_ALLOW_DOWNGRADE=1 firefox -P 
~~~ 

## Crash reports

On peut avoir des infos sur la rapport de plantage de Firefox en allant sur **about:crashes**

Attention, cela peut prendre de la place, il est conseillé de les supprimer de temps en temps ("Tout supprimer" dans **about:crashes**)

## sessionstore-backups

Lors de la fermeture ou d'un plantage, une session Firefox semble stockée dans recovery.baklz4 et recovery.jsonlz4

Le répertoire sessionstore-backups/ contient des sauvegardes de sessions stockées... a priori les vieux fichiers de ce répertoire peuvent être supprimés pour gagner de la place !

## Fichiers .sqlite

Firefox stocke pas mal de données dans des fichiers .sqlite

À faire avec grande précaution mais il semble intéressant de faire un VACUUM dans ces fichiers de temps en temps pour réduire leur taille et optimiser la performance.

On peut le faire - quand Firefox ne tourne pas - ainsi :

~~~
$ sqlite3 favicons.sqlite
sqlite> vacuum;
~~~

Voir <https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Storage/Performance>

## safebrowsing

La "Protection contre les contenus trompeurs et les logiciels dangereux" de Firefox peut impacter les performances. 
On peut le désactiver via les Préférences de Sécurité ou via **about:config** :

~~~
browser.safebrowsing.malware.enabled false
browser.safebrowsing.phishing.enabled false
browser.safebrowsing.blockedURIs.enabled false
browser.safebrowsing.downloads.enabled false
browser.safebrowsing.passwords.enabled false
~~~

## Sélectionner du texte dans un lien

On peut sélectionner du texte dans un lien sans suivre le lien, par exemple pour sélectionner « example » dans [http://example.org/](http://example.org/), en maintenant la touche *Alt* durant la sélection du texte.

* Maintenir *Alt*.
* Sélectionner le texte.
* Relâcher *Alt*


## Perfomances

Vous pouvez observer les performances via **about:performance**

Vous pouvez observer les processes via **about:processes**

## about

Vous pouvez observer les about via **about:about**

## Supprimer une entrée du remplissage automatique d'un formulaire

Il suffit d'aller sur l'entrée suggérée et d'appuyer sur "Suppr"

Voir <https://support.mozilla.org/fr/kb/controler-remplissage-automatique-formulaires>

## Cache DNS

about:networking#dns permet de vider le cache DNS mais aussi de voir toutes les entrées en cache !

