---
categories: ticketing
title: Howto Zammad
...

* Documentation : <https://docs.zammad.org/>
* Statut de cette page : test / bookworm

[Zammad](https://zammad.org/) est un outil libre de gestion de tickets.

## Installation

Pré-requis : Postgresql, Nginx, opendkim?, elasticsearch, Redis, Memcached

Forcer temporairement pour l'installation (à remettre après) :

~~~
# cat /etc/default/locale
"LANG=en_US.UTF-8"
~~~

Nous utilisons l'installation par paquets :

<https://docs.zammad.org/en/latest/install/package.html>


~~~
# cat /etc/apt/sources.list.d/zammad.list 
deb [signed-by=/etc/apt/keyrings/pkgr-zammad.gpg] https://dl.packager.io/srv/deb/zammad/zammad/stable/debian 12 main

# curl https://dl.packager.io/srv/zammad/zammad/key | gpg --dearmor > /etc/apt/keyrings/pkgr-zammad.gpg
# chmod 644 /etc/apt/keyrings/pkgr-zammad.gpg

# apt install zammad

...
# Enforcing Redis...
# Starting Redis server
...
# Installing zammad on postgresql
...
# Creating zammad postgresql user
CREATE ROLE
# Creating zammad postgresql db
# Grant privileges to new postgresql user
GRANT
# Updating database.yml
...
== 20120101000001 CreateBase: migrating =======================================
-- create_table(:sessions, {:id=>:integer})
   -> 0.0574s
...
== 20120101000010 CreateTicket: migrated (4.5342s) ============================

I, [2024-11-04T12:52:19.464426#3919693-6860]  INFO -- : Migrating to UpdateTimestamps (20150979000001)
== 20150979000001 UpdateTimestamps: migrating =================================
== 20150979000001 UpdateTimestamps: migrated (0.0049s) ========================
...
# Configuring Elasticsearch...
-- Nevermind, no es_url is set, leaving Elasticsearch untouched ...!
-- The above is all right if you don't want to use Elasticsearch (locally) - if this is not intended, consult https://docs.zammad.org !
I, [2024-11-04T12:53:13.002528 #3920166]  INFO -- : ActionCable is using the redis instance at redis://localhost:6379.
I, [2024-11-04T12:53:13.006694#3920166-6120]  INFO -- : Using Zammad's file store as Rails cache store.
I, [2024-11-04T12:53:13.006737#3920166-6120]  INFO -- : Using the File back end for Zammad's web socket session store.
I, [2024-11-04T12:53:13.956332#3920166-6120]  INFO -- : Setting.set('es_url', "")
# (Re)building Elasticsearch searchindex...
# Enforcing 0600 on database.yml ...
Setting default Logging to file, set via "zammad config:set RAILS_LOG_TO_STDOUT=true" if you want to log to STDOUT!
...

# systemctl status zammad
● zammad.service
     Loaded: loaded (/etc/systemd/system/zammad.service; enabled; preset: enabled)
     Active: active (running) since Mon 2024-11-04 12:53:14 CET; 25min ago
   Main PID: 3920327 (sleep)
      Tasks: 1 (limit: 4682)
     Memory: 168.0K
        CPU: 607us
     CGroup: /system.slice/zammad.service
             └─3920327 /bin/sleep infinity
~~~

Ajustements `/etc/nginx/sites-enabled/zammad.conf` : listen 443, server_name, SSL, etc.

Attention, cela installe environ 1 Go dans `/opt/zammad`.
On conseille de déplacer ailleurs :

~~~
# mv /opt/zammad/ /home/opt-zammad
# ln -s /home/opt-zammad /opt/zammad
~~~

a priori il faut bien activer "exec" pour `/home`


## Configuration

Fichier `/etc/default/zammad`

/opt/zammad/

### Memcached 

~~~
# apt install memcached
~~~

Config mémoire 256M

~~~
$ zammad config:set MEMCACHE_SERVERS=127.0.0.1:11211
~~~

### Elasticsearch

<https://docs.zammad.org/en/latest/install/elasticsearch.html>

~~~
# /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
-> Installing ingest-attachment
[ingest-attachment] is no longer a plugin but instead a module packaged with this distribution of Elasticsearch
-> Please restart Elasticsearch to activate any plugins installed

# /usr/share/elasticsearch/bin/elasticsearch-users useradd zammad
# /usr/share/elasticsearch/bin/elasticsearch-users roles zammad -a superuser

$ zammad run rails r "Setting.set('es_url', 'https://localhost:9200')"
$ zammad run rails r "Setting.set('es_user', 'zammad')"
$ zammad run rails r "Setting.set('es_password', '<password>')"
$ zammad run rails r "Setting.set('es_ssl_verify', false)"

$ zammad run rake zammad:searchindex:rebuild
Dropping indexes... done.
Deleting pipeline... done.
Creating indexes...done.
Creating pipeline... done.
Reloading data... 
  - Chat::Session... 
    done in 0 seconds.
...
~~~

> Note : besoin de créer un rôle plus restreint... https://github.com/zammad/zammad-documentation/issues/247



## Administration

~~~
# systemctl restart zammad
~~~

~~~
$ zammad run rails c
Loading production environment (Rails 7.0.8.5)
[1] pry(main)> Setting.get('http_type')
=> "https"
~~~

## FAQ

### Performance tuning

<https://docs.zammad.org/en/latest/appendix/configure-env-vars.html#performance-tuning>

### Message d'erreur "relation [...] does not exist"

Cela signifie que le schéma de la base de donnée de zammad n'est pas à jour.

La solution minimale pour corriger le problème est de faire la commande suivante (en tant que l'utilisateur `zammad`) :

```
$ zammad run rake db:migrate
```

Mais ce problème ne devrait se produire que lors d'une migration entre deux versions de zammad (la mise à jour du paquet faisant automatiquement cette commande). Dans ce cas, il est fortement recommandé de réinstaller le paquet [(étape 9 pour une migration avec "full filesystem dump" dans la documentation officielle)](https://docs.zammad.org/en/latest/appendix/backup-and-restore/migrate-hosts.html) :

```
# dpkg -r --force-depends zammad
# apt install zammad
```