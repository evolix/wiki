# Installation Redmine

Ce document explique l'installation du logiciel Redmine depuis les sources.

Cette installation à été testé sur une Debian Jessie avec Redmine en version 3.0, 3.1, 3.2, 3.3 et 3.4.

À noter :

* La catégorie nommées prérequis ne sont à faire qu'une seule fois par serveur.
* Les commandes sont à lancer avec l'utilisateur entre parenthèse.

## Prérequis (User = root)

Installations des dépendances :

~~~
# aptitude install libpam-systemd libmysqlclient-dev ruby ruby-dev bundle imagemagick git-core git-svn gcc build-essential libxml2-dev libxslt1-dev libssl-dev
~~~

Installation des dépendances 2 (L'installation en une fois crée des conflits !) :

~~~
# aptitude install libmagickwand-dev libmagickcore-dev
~~~

Si Squid est présent vous devez rajouter les sites github et rubygems dans sa liste blanche !

~~~
echo "https://github.com/.*" >> /etc/squid3/whitelist.conf
echo "http://rubygems.org/.*" >> /etc/squid3/whitelist.conf
echo "http://.*.rubygems.org/.*" >> /etc/squid3/whitelist.conf
~~~

**/home ne doit pas avoir l'attribut noexec !!!**


## Création du compte Unix (User = root)

Choix de l'utilisateur $REDMINE propriétaire de l'application

~~~
REDMINE='redmine'
~~~

Création de l'utilisateur $REDMINE :

~~~
useradd $REDMINE -d "/home/$REDMINE" -c "Redmine $REDMINE" -s "/bin/bash" -m
~~~

Ajout de l'utilisateur www-data au groupe $REDMINE :

~~~
adduser www-data $REDMINE
~~~


## Création de la base de donnée (User = root)

[HowtoMySQL]()


## Installation (User = $REDMINE)

Se connecter avec l'utilisateur $REDMINE :

~~~
su - $REDMINE
~~~

Choisir la $BRANCHE de la version de Redmine :

* voir le dépôt [Github](https://github.com/redmine/redmine) pour choisir une version
* vérifier les prérequis sur [redmine.org](http://www.redmine.org/projects/redmine/wiki/RedmineInstall#Exigences)

~~~
BRANCHE=3.4-stable
~~~

Configuration du profile :

~~~
cat > ~/.profile <<EOF
# ~/.profile: executed by the command interpreter for login shells.

umask 027

export XDG_RUNTIME_DIR=/run/user/$UID
export PATH="\$HOME/.gems/ruby/2.1.0/bin:\$PATH"
export RAILS_ENV=production
EOF
source ~/.profile
~~~

Clonage du dépôt Git du projet Redmine :

~~~
git clone https://github.com/redmine/redmine.git -b $BRANCHE ~/www
~~~

Création des dossiers nécessaires :

~~~
mkdir -m 0750 ~/files
~~~

Copie de la configration de Redmine :

~~~
cat > ~/www/config/configuration.yml <<EOF
production:
  email_delivery:
    delivery_method: :smtp
    smtp_settings:
      address: localhost
      port: 25
      domain: "$(hostname -d)"
      ssl: false
      enable_starttls_auto: false
  attachments_storage_path: /home/$USER/files
  autologin_cookie_secure: true
EOF
~~~

Configuration des logs :

~~~
cat > ~/www/config/additional_environment.rb <<EOF
config.paths['log'] = "/home/$USER/log/redmine.log"
config.log_level = :warn
EOF
~~~

Récupération du mot de passe Mysql :

~~~
MYSQLPASS=`grep password ~/.my.cnf|cut -d'=' -f2|tr -d ' '`
~~~

Configuration de la base de donnée :

~~~
cat > ~/www/config/database.yml <<EOF
production:
  adapter: mysql2
  database: $USER
  host: localhost
  username: $USER
  password: "$MYSQLPASS"
  encoding: utf8
EOF
~~~

Ajout de la gem puma aux dépendances :

~~~
cat > ~/www/Gemfile.local <<EOF
gem "puma"
# En Debian Jessie il faut forcer la version de "xpath" pour être compatible Ruby 2.1
#gem "xpath", "< 3.0.0"
EOF
~~~

Installation des dépendances Gem avec bundle (cela peut durer plusieurs minutes) :

~~~
bundle install --gemfile=~/www/Gemfile --path=~/.gems
~~~

Génération d'un clé aléatoire utilisé pour encoder les cookies de session :

~~~
cd ~/www/
bundle exec rake -qf ~/www/Rakefile generate_secret_token
~~~

Création des schémas de la base de données redmine :

~~~
bundle exec rake -qf ~/www/Rakefile db:migrate
~~~

Chargement des données par défaut :

~~~
bundle exec rake -qf ~/www/Rakefile redmine:load_default_data REDMINE_LANG=fr
~~~


### Serveur d'application

Maintenant que Redmine est installé, il faut configurer [Puma](https://wiki.evolix.org/HowtoPuma).

**La Gem est déjà installe grâce au Gemfile.local.**


## Lancement de l'application

Démarrer/éteindre l'application :

~~~
systemctl --user start/stop puma
~~~

Recharger la configuration après avoir modifier /etc/puma/$USER.rb (pas de coupure) :

~~~
systemctl --user reload puma
~~~

Redémarrer l'application :

~~~
systemctl --user restart puma
~~~

Activer/désactiver l'application au démarrage :

~~~
systemctl --user enable/disable puma
~~~


## Gestions des plugins

### Taches post installation / post mise a jour

Mise à jour des gem :

~~~ { .bash }
bundle install --gemfile=~/www/Gemfile --path=~/.gems
~~~

Migration des plugins :

~~~ { .bash }
( cd ~/www && bundle exec rake -qf ~/www/Rakefile redmine:plugins:migrate )
~~~


### Desinstallation d'un plugin :

~~~ { .bash }
( cd ~/www && bundle exec rake -qf ~/www/Rakefile redmine:plugins:migrate[plugin_name,0] )
~~~


## Mise a jour

### Mineure

~~~
cd ~/www
git pull
bundle update
systemctl --user stop puma
bundle exec rake -q db:migrate
systemctl --user daemon-reload
systemctl --user start puma
systemctl --user status puma
~~~


### Majeure

~~~
systemctl --user stop puma
cd ~/www
git pull
git checkout $BRANCHE
rm Gemfile.lock
bundle update
bundle exec rake -q db:migrate
systemctl --user daemon-reload
systemctl --user start puma
systemctl --user status puma
~~~


### Après une mise à jour de Debian

Après une mise à jour du type Debian 9 vers Debian 10, il faut remettre à jour les gems.

~~~
bundle install --redownload
~~~


## Taches d'administration

Lancer un shell ruby dans l'environnement de production :

~~~
cd ~/www
bundle exec rails console
~~~


### Créer un compte admin / mot de passe admin

~~~
user = User.new(:firstname => "<FIRSTNAME>", :lastname => "<LASTNAME>", :mail => "<MAIL>", :mail_notification => "none", :status => 1)
user.login = '<LOGIN>'
user.password = '<PASSWORD>'
user.password_confirmation = '<PASSWORD>'
user.valid?   # will return true if the user is valid
user.admin = true
user.save
~~~


### Compter les issues d'un projet / les supprimer

Ceci est notamment utile à faire avant de supprimer un projet, ce qui est peut-être impossible via l'interfaces web si il y atrop d'issues :

~~~
> p = Project.find(42)
> p.issues.count
   (0.1ms)  SELECT COUNT(*) FROM "issues" WHERE "issues"."project_id" = 42
 => 1337
> p.issues.each(&:destroy)
...
~~~


### Changer le mot de passe de l’administrateur

Dans le cas où il n’y a qu’un seul utilisateur administrateur, on peut toujours changer son mot de passe en éditant directement la base de données. La procédure est dans [la FAQ de Redmine](https://www.redmine.org/projects/redmine/wiki/FAQ#Reset-password-lost-without-admin-redmine-account-but-with-admin-redmine-database-account).


## API

<https://www.redmine.org/projects/redmine/wiki/rest_api>

Il y a besoin d'une clé pour questionner l'API en se rendant dans *les paramètres de son compte* (https://instance-redmine.net/my/account). Dans la colonne de droite, il y a la section *Clé d'accès API*. Soit :

~~~
INSTANCE="https://instance-redmine.org"
KEY=XXXXXXXXXXXXXXXXXXXXXXX
~~~

  * Afficher les projets
 
~~~
curl -s -X GET "${INSTANCE}/projects.json?key=${KEY}" | jq '.projects[]'
~~~

  *  Lister les pages d'un wiki en rapport avec un $projet

~~~
curl -s -X GET "${INSTANCE}/projects/${projet}/wiki/index.json?key=${KEY}" |jq '.wiki_pages[] |.title'
~~~

  * Récupérer le contenu d'une page d'un $projet

~~~
 curl -s -X GET "${INSTANCE}/projects/${projet}/wiki/${page}.json?key=${KEY}" |jq '.wiki_page.text '
~~~

# FAQ

## Déterminer la version de Redmine

L’information est accessible depuis l’interface dans _Administration_ puis _Informations_. En ligne de commande, il faut vérifier le contenu du fichier `/<racine_redmine>/lib/redmine/version.rb`. La version est lisible dans les variable `MAJOR`, `MINOR` et `TINY`. Par exemple, pour une instance Redmine en 5.0.3 :

~~~
$ cat ./lib/redmine/version.rb
[…]
    MAJOR = 5
    MINOR = 0
    TINY  = 3
[…]
~~~

> Commande AWK pour extraire la version au format `x.y.z` :
> 
> ~~~
> $ awk '/^ *MAJOR/ { major = $NF } /^ *MINOR/ { minor = $NF } /^ *TINY/ { tiny = $NF } END { printf "%u.%u.%u", major, minor, tiny }' ./lib/redmine/version.rb
> ~~~


## Changer un mot de passe avec `rails`

~~~
# su - REDMINE
$ cd ~/www
$ RAILS_ENV=production bundle exec rails c
> user = User.where('login').first
> user.password = 'mon-nouveau-mot-de-passe'
> user.password_confirmation = 'mon-nouveau-mot-de-passe'
> user.save!
~~~


## Configuration email

<https://www.redmine.org/projects/redmine/wiki/emailconfiguration>

Cela se passe dans `config/configuration.yml` :

Pour envoi via SMTP vers 127.0.0.1 sans SSL/TLS (Redmine supporte mal les certificats auto-signés) :

~~~
production:
  email_delivery:
    delivery_method: :smtp
    smtp_settings:
      address: 127.0.0.1
      port: 25
      domain: example.com
      enable_starttls_auto: false
      openssl_verify_mode: "NONE"
~~~

> Note : sur certaines versions, pour désactiver SSL/TLS, il faut utiliser `disable_start_tls: true`

Pour envoi via Sendmail :

~~~
production:
  email_delivery:
    delivery_method: :sendmail
    sendmail_settings:
      arguments: "-oi -t -f redmine@example.com" 
~~~

Pour envoi via serveur distant avec authentification :

~~~
production:
  email_delivery:
    delivery_method: :smtp
    smtp_settings:
      enable_starttls_auto: true
      openssl_verify_mode: 'none'
      address: smtp.example.com
      port: 587
      domain: "example.com"
      authentication: :login
      user_name: "jdoe"
      password: "PASSWORD"
~~~


## Problème lors des mises à jour

Le genre de message suivant peut-être rencontré lors d’une mise à jour.

~~~
$ bundle exec rake -q db:migrate
rake aborted!
NameError: uninitialized constant OpenStruct

class OpenStruct2 < OpenStruct
                    ^^^^^^^^^^
/home/redmine/www/plugins/redmine_checklists/lib/redmine_checklists/patches/compatibility/open_struct_patch.rb:20:in `<top (required)>'
[…]
Tasks: TOP => db:migrate => db:load_config => environment
(See full trace by running task with --trace)
~~~

La référence à `/home/redmine/www/plugins/redmine_checklists` est un bon indice d’incompatibilité du plugin `redmine_checklists`. Il est possible de rapidement le désactiver (le déplacer hors du dossier /plugins) afin de terminer la mise à jour. Ensuite, on peut trouver en ligne une version plus récente et compatible du plugin afin de le réactiver.
