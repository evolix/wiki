**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Ganeti

<http://docs.ganeti.org/ganeti/2.2/html/>

<http://www.howtoforge.com/xen-cluster-management-with-ganeti-on-debian-lenny>

<http://git.ganeti.org/?p=ganeti.git;a=summary>

~~~
# aptitude install ganeti2
~~~

* Config bridge br0
* Config /etc/hostname
* Config LVM d'un VG
* Utilisation d'un IP pour le cluster
* mkdir /root/.ssh/

~~~
# /etc/init.d/ganeti stop
# gnt-cluster init --enabled-hypervisors=kvm --master-netdev br0 -g vg mycloud
# gnt-node add node01
-- WARNING -- 
Performing this operation is going to replace the ssh daemon keypair
on the target machine (node01.example.com) with the ones of the current one
and grant full intra-cluster ssh root access to/from it

The authenticity of host 'xxx' can't be established.
RSA key fingerprint is 70:69:4b:d8:33:68:b5:77:c4:be:6e:85:9a:62:08:bb.
Are you sure you want to continue connecting (yes/no)? yes
root@node01's password: 
Mon Nov 29 13:36:57 2010  - INFO: Node will be a master candidate

root@node00:~# gnt-node list
Node       DTotal  DFree MTotal MNode MFree Pinst Sinst
node00 232.2G 232.2G  31.5G  195M 31.3G     0     0
node01 232.2G 232.2G  31.5G  154M 31.3G     0     0
~~~
