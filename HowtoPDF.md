---
title: Howto PDF
...


## Faire un PDF à partir de plusieurs PDF

~~~
$ pdftk 1.pdf 2.pdf 3.pdf 4.pdf 5.pdf cat output out.pdf
~~~

## Découper un PDF

Sélectionner que certaines pages :

~~~
$ pdftk ori.pdf cat 2-5 10-end output out.pdf
~~~

## Pivoter un PDF

De 90° les pages 2 à 5 :

~~~
$ pdftk ori.pdf rotate 2-5right output out.pdf
~~~

left pour -90°, down pour +180°.

## Compresser un PDF :

~~~
$ gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=out.pdf in.pdf
~~~

Si l'on veut jouer avec qualité/taille :

* -dPDFSETTINGS=/screen : faible taille/qualité
* -dPDFSETTINGS=/ebook : taille/qualité moyenne
* -dPDFSETTINGS=/printer : taille/qualité importante

## Créer un booklet au format A5

~~~
# apt install psutils
$  pdf2ps in.pdf - | psbook | psnup -2 | ps2pdf - out.pdf
~~~

## Changer le format d'un PDF

Depuis un format source vers A2 :

~~~
$ gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sPAPERSIZE=a2 -dFIXEDMEDIA -dPDFFitPage -sOutputFile=out.pdf in.pdf
~~~

## Couper un pdf A2 en 4 pages A4

~~~
# apt install pdfposter
$ pdfposter -pA2 -mA4 in.pdf out.pdf
~~~

L'option `-p` indique le format source, et `-m` indique le format de destination.

La source n'a pas besoin d'être dans le format spécifié. Par exemple si la source est en A4 et qu'on indique `-pA2` et `-mA4`, elle sera automatiquement agrandie pour être considérée comme A2, avant d'être découpée en 4 pages A4.

Pour un fichier svg, si son format source est trop petit avant d'être découpé, il faut modifier les propriétés du document depuis inkscape en le transformant par exemple en A2, sélectionner tout et agrandir pour remplir le cadre A2 en maintenant Ctrl pour garder la proportion, l'exporter en PDF (File > Save As… > choisir le format PDF) puis le couper avec pdfposter.

Dans tous les cas, il est préférable d'exporter un fichier en .pdf directement depuis son logiciel plutôt que de par exemple le convertir depuis .jpg vers .pdf.

## Transformer une image en PDF

~~~
# apt install imagemagick
$ convert in.jpg -auto-orient out.pdf
~~~

En cas d'erreur `attempt to perform an operation not allowed by the security policy PDF' @ error/constitute.c/IsCoderAuthorized/426`, modifier le fichier `/etc/ImageMagick-6/policy.xml` en vérifiant que la directive suivant est active :

~~~
<policy domain="coder" rights="read|write" pattern="PDF" />
~~~

## Cracker un fichier PDF avec un mot de passe

~~~
$ gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=out.pdf -f in.pdf
~~~

