---
categories: tip
title: Tips général
...
# Clavier

## [Devenir plus efficace avec son clavier](/TipsKeyboard)

[TipsKeyboard](/TipsKeyboard)

## Bloquer le touchpad si clavier utilisé

~~~
# apt install xserver-xorg-input-synaptics
$ syndaemon -d -i 0.8 -p $HOME/.syndaemon.pid
~~~

# Écran

## Afficher une image en arrière plan

~~~{.bash}
$ display -backdrop -window root <img>
~~~

## Changer l’apparence des fenêtre GTK (thêmes)

Voir du côté de :
 - ~/.gtkrc-2.0
 - ~/.config/gtk-X.0/settings.ini

~~~{.bash}
$ lxappearance
~~~

## Rendu

### Exporter en image le rendu d'une page web (WebKit)

~~~{.bash}
$ cutycapt --url=http://evolix.org --out=evolix-site.png
$ cutycapt --url=file:///home/evolix/index.html --out=test-site.png
~~~

[/HowtoWkhtmltopdf]()

~~~{.bash}
$ wkhtmltopdf http://evolig.org out.pdf
~~~

### std2html

~~~
# apt install aha
$ script-foo-colors | aha > output.html
~~~

# Autres outils

## Recevoir un email depuis un flux RSS lors nouvel article

~~~
# apt install rss2email
~~~

~~~{.bash}
$ r2e new email@example.com
$ r2e add http://flux.rss.example.com/feed email@example.com
$ #ou simplement $ r2e add http://flux.rss.example.com/feed
$ r2e run --no-send #la première fois, car sinon envoie de plusieurs mails
$ crontab -l
# check rss toutes les heures
42 * * * * r2e run
$ r2e list #connaître l'état actuel des flux surveillés
~~~
