---
title: Howto WeeChat
categories: tips
...

# WeeChat

WeeChat est un client IRC léger en ligne de commande.

Documentation officielle :

* Quickstart guide (en français) : <https://weechat.org/files/doc/stable/weechat_quickstart.fr.html>
* Guide utilisateur : <https://weechat.org/files/doc/stable/weechat_user.fr.html>

Configurations intéressantes :

* <https://gist.github.com/pascalpoitras/8406501>
* <https://lord.re/posts/118-config-weechat/>


## Installation

Sur Debian :

~~~{.bash}
apt install weechat
~~~

Sur OpenBSD :

~~~{.bash}
pkg_add weechat
~~~

Lancer weechat :

~~~{.bash}
$ weechat
~~~


## Configuration rapide

### Serveurs et channels

Changer son nick, username, realname

~~~
/set irc.server_default.nicks "foo"
/set irc.server_default.username "foo"
/set irc.server_default.realname "foo bar"
~~~

Ajouter des servers avec connexion automatique

~~~
/server add freenode chat.freenode.net/6697 -ssl -autoconnect
~~~

Ajouter des channels avec connexion automatique

~~~
/set irc.server.freenode.autojoin "#debian,#openbsd,#evolix"
~~~

Se connecter à un serveur avec authentification

~~~
/set irc.server.$mysrv.username foo
/set irc.server.$mysrv.password XXX
/connect $mysrv
~~~

Se déconnecter

~~~
/disconnect $mysrv
~~~

Détecter plus rapidement des déconnexions/reconnexions réseau (défaut 300s) :

~~~
/set irc.network.lag_reconnect 5
~~~

### Configurer ses highlights

Mots à « highlighter »

~~~
/set weechat.look.highlight foo,bar
~~~

Faire des highlights avec des regex

~~~
/set weechat.look.highlight_regex ^\*|^\* :|^\*:
~~~

Pour les désactiver

~~~
/unset weechat.look.highlight_regex
~~~

Couleur des highlights dans les channels

~~~
/set weechat.color.chat_highlight yellow
/set weechat.color.chat_highlight_bg red
~~~

Couleur des highlights dans la bar de statut

~~~
/set weechat.color.status_data_highlight red
/set weechat.color.status_data_private red
~~~

Être highlighté à chaque message dans le canal courrant

~~~
/buffer set highlight_regex .*
~~~


### Organiser ses buffers et son layout

Inverser les buffers 3 et 4

~~~
/buffer swap 4 3
~~~

Déplacer un buffer

~~~
/buffer move 3
~~~

Merger des buffers

ex : dans un des buffers qu'on veut merger

~~~
/buffer merge <n° du buffer que l'on veut ajouter>
~~~


Sauvegarder

~~~
/save
~~~

Sauvegarder son layout

~~~
/layout store
/save
~~~

Sortir d'un buffer

~~~
/part
~~~

Fermer un buffer

~~~
/close
~~~

Découper en plusieurs écrans

~~~
/window splith
/window splitv
~~~

### Raccourcis clavier

Voir <https://weechat.org/files/doc/stable/weechat_user.fr.html#key_bindings>

* `Alt+u` Se positionner sur la première ligne non lue du tampon. `/window scroll_unread`

### Souris

Weechat support des actions faites à la souris : <https://weechat.org/files/doc/stable/weechat_user.en.html#key_bindings_mouse>

Lorsque c'est activé cela peut perturber les actions habituelles de sélection de texte…

On peut activer/désactiver cela avec `Alt-m` ou la commande `/mouse toggle`