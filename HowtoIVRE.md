---
categories: scan, nmap
title: Howto IVRE
---

**Attention, peinture fraiche ! - Cette page est en cours de rédaction ;)**

## Introduction
IVRE (*Instrument de veille sur les réseaux extérieurs*) ou DRUNK (*Dynamic Recon of UNKnown networks*) est un cadriciel libre pour faire de la reconnaissance de réseau informatiques. Il s'appuie sur des outils libres tels que Nmap, Zmap, etc pour faire du scan, stoquer les résultats une base mongodb et donner le moyen d'analyser ces données via l'interface web, la ligne de commande ou même en python.

## Installation

On va avoir besoin de `python`, `virtualenv`, `mongodb` et de quelques autres dépendances optionnelles (`phantomjs`, `imagemagick`, `ffmpeg`). Pour `MongoDB`, tout est expliqué dans le [Howto MongoDB](https://wiki.evolix.org/HowtoMongoDB)

~~~
# apt install build-essential autoconf python-dev python-pip  python-virtualenv python-pil imagemagick nmap
# apt install -t jessie-backports phantomjs ffmpeg
~~~

Ensuite, on créée un compte utilisateur pour y installer ivre dans un environement virtuel.

Exemple ici avec un compte `ivre` :
~~~
# su ivre
$ virtualenv ivre
$ source ivre/bin/activate
$ pip install ivre
~~~

Dans la suite, on suppose que toutes les commandes ivre, faites en super-utilisateur `root` ou avec le compte `ivre` sont faites avec l'environement virtuel chargé !

*Note* : Dans le `virtualenv`, il va y avoir quelques soucis avec le positionnement du dossier contenant les informations de GeoIP. Mais cela peut être réglé avec un petit fichier de configuration, et ainsi forcer l'utilisation d'un dossier comme `/usr/share/GeoIP/`

~~~
# cat /etc/ivre.conf
GEOIP_PATH = "/usr/share/GeoIP/"
~~~

On peut maintenant initialiser la base de données :

~~~
$ ivre scancli --init
This will remove any scan result in your database. Process ? [y/N] y
$ ivre ipinfo --init
This will remove any passive information in your database. Process ? [y/N] y
$ ivre ipdata --init
This will remove any country/AS information in your database. Process ? [y/N] y
# ivre runscansagentdb --init
This will remove any agent and/or scan in your database and files. Process ? [y/N] y
# ivre ipdata --download
Downloading http://geolite.maxmind.com/download/geoip/database/GeoLiteCity_CSV/GeoLiteCity-latest.zip to /usr/share/GeoIP/GeoIPCityCSV.zip: done.
Downloading http://thyme.apnic.net/current/data-raw-table to /usr/share/GeoIP/BGP.raw: done.
Downloading http://geolite.maxmind.com/download/geoip/database/asnum/GeoIPASNum2.zip to /usr/share/GeoIP/GeoIPASNumCSV.zip: done.
Downloading http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip to /usr/share/GeoIP/GeoIPCountryCSV.zip: done.
Downloading http://dev.maxmind.com/static/csv/codes/iso3166.csv to /usr/share/GeoIP/iso3166.csv: done.
Unpacking: done.
$ ivre ipdata --import-all --no-update-passive-db
~~~

Il ne manque plus qu'a installer l'interface web et les script nmap proposés.
**Attention** : Le script `vnc-screenshot.nse` ne doit pas être copié si version inférieure à 7.25BETA2 (ie: nmap dans debian jessie est en 6.47-3) :

~~~
# cp /home/ivre/ivre/ivre/share/ivre/nmap_scripts/* /usr/local/share/nmap/scripts/
# nmap --script-updatedb
~~~

Pour l'interface web, on prend comme base de départ le [Howto Apache](https://wiki.evolix.org/HowtoApache), il ne reste plus qu'a faire un peu de configuration apache

~~~
ScriptAlias /ivre/cgi-bin/ /home/ivre/ivre/share/ivre/web/cgi-bin/
<Directory /home/ivre/ivre/share/ivre/web/cgi-bin/>
        AssignUserID ivre ivre
        Options +ExecCGI
        AddHandler cgi-script .py
        Require all granted
</Directory>

Alias /ivre /home/ivre/ivre/share/ivre/web/static
<Directory /home/ivre/ivre/share/ivre/web/static>
        AssignUserID ivre ivre
        Options SymLinksIfOwnerMatch
        AllowOverride AuthConfig Limit FileInfo Indexes
        Require all granted
</Directory>
~~~

Après un rechargement de la configuration, l'interface web sera accessible à http://example.net/ivre/

## Utilisation

### Scan de réseaux

On peut directement utiliser ivre en cli, il va alors piloter nmap pour effectuer le scan des machines demandées.

Exemple, pour scanner le réseau 192.0.2.0/24, on peut utiliser la commande suivante :

~~~
# ivre runscans --network 192.0.2.0/24 --output=XMLFork
~~~

Il faut ensuite procéder à l'import des résultats dans la base de données pour pouvoir les manipuler :

~~~
# ivre scan2db -c TESTS,TEST-0001 -s InterwebsScanner -r scans/NET-192.168.4.148_32/up/
~~~

## Liens utiles

* [Site officiel](https://ivre.rocks/)
* [Projet sur GitHub](https://github.com/cea-sec/ivre)
