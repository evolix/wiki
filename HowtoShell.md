**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

### Générer des caractères alphanumériques aléatoires et lisibles

~~~
$ false; while [ $? != 0 ]; do apg -c /dev/urandom -n1 -E oOlL10 | egrep '^[[:alnum:]]+$'; done
~~~

### Débloquer une connexion SSH gelée

Faire la séquence suivante sur votre clavier : Entrée + ~ + . (Entrée puis tilde, puis point.)

### Lister l'environnement d'un processus

~~~
$ cat /proc/_pid_/environ
~~~

On peut aussi créer un petit script dans _/usr/local/bin/pidenv_

~~~
#!/bin/sh

pid="$1"
tr '\000' '\n' < /proc/$pid/environ
~~~

Et ainsi disposer d'un formattage plus lisible :

~~~
# pidenv 1
SHLVL=1
HOME=/
init=/sbin/init
TERM=linux
drop_caps=
BOOT_IMAGE=/vmlinuz-4.6.0-0.bpo.1-amd64
PATH=/sbin:/usr/sbin:/bin:/usr/bin
PWD=/
rootmnt=/root
~~~