---
categories: network
title: Howto Scapy
---
* Documentation : <https://scapy.readthedocs.io/en/latest/>

[Scapy](https://github.com/secdev/scapy) est un programme et une librairie de manipulation de paquets écrite en Python.  L'outil permet de forger, décoder, émettre et capturer des segments de communication (format [PCAP](https://www.endace.com/learn/what-is-a-pcap-file)). Aussi, les procédures de prototypage de paquets sont simplifiées grâce à une auto-complétion intelligente.

C'est avant tout une librairie Python offrant aux utilisateur une interface accessible directement via l’interpréteur.

**Objectif :** Forger, manipuler, écouter les flux de communication réseau. 

## Principe de fonctionnement et Concepts

Scapy se sert du mode **promiscuous** des interfaces et réintroduit intégralement une stack TCP/IP sur laquelle il possède les pleins pouvoirs. Cela simplifie notamment la traversée du niveau 2 (Modèle OSI) dans la stack TCP/IP noyau. L'interface étant configurée en **promiscuous** le kernel est alors supplanté par Scapy et le matériel à disposition.

Cette structure nous permets de définir entièrement un environnement réseau spécifique. Scapy nous offre la possibilité de créer/renommer/ajouter des interfaces réseau, créer une table routage dédiée indépendante de celle du système "hôte", la gestion de certificats d'encryption et bien d'autres à travers l'objet ```conf```.

L'outil se base sur les nivaux du modèle OSI afin de structurer les couches composant les objets descripteurs de paquets. Ainsi un objet de type PKT dans Scapy se compose généralement de plusieurs couches en fonction des choix de l'utilisateur,  et est essentiellement un dictionnaire bidimensionnel : ```PKT['IP']['TCP']...```.


### Structures et Modèle OSI

#### Couches

Les couches mises en place par Scapy dans sa représentation des paquets sont celles modélisées par le modèle OSI, en effet, chaque interaction avec la librairie ou l'outil cible un couche particulières.
Le principale impacte de cette structure en couche sur l'utilisation de Scapy, est en réalité étalé sur deux aspects: 

* Lisibilité et la facilité d'utilisation.

* Stack TCP/IP séparée, cela signifie que la couche Ethernet est un peu spéciale.

**Scapy offre donc deux types de fonction interagissant avec le réseau :**

* Les fonctions de **niveau 3** et plus, utilisant la **stack TCP/IP du système par défaut**  (ex : ```sr()```,```send()```).
 * Les fonctions de **niveau 2** spécifiant l'utilisation de la **stack de Scapy** (```srp()```,```sendp()```).


## Installation 

Scapy est suporté par *nix, macOS et Windows, mais seules les procédures sous Linux seront documentées ici. Pour plus d'information concernant les procédures d'installation propre à d'autres plateformes, se réferer à la [documentation officielle](https://scapy.readthedocs.io/en/latest/installation.html#installing-scapy-v2-x)

### Debian

Installation sous Debian :

~~~
# apt install python3-scapy
~~~

Puis vérifier que Scapy est bien installé : 

~~~
# scapy
~~~

Vérification de la version (2.4.4 à ce jour) : 

~~~python
>>> conf.version
'2.4.4'
~~~


## Utilisation

L'utilisation principale de Scapy se fait au travers de l’interpréteur python directement, et l'outil nécessitant l'accès au mode **promiscuous**, il requiert donc les droits root au lancement : 

~~~
# scapy
~~~

Une fois scapy lancé, l’interpréteur python est le nouvel environnement de travail.

### Configuration 

Dans l'interpréteur, toute la configuration de Scapy se fait via l'objet ```conf```. Les champs qui le composent permettent de moduler le comportement de l'outil en fonction de vos .
Nous pouvons retrouver dans ce dictionnaire les champs concernant la stack TCP/IP propre à Scapy, notamment la table de routage, les interfaces, la taille de buffer etc...

~~~python
>>> conf
[...]
iface      = 'enp0s31f6'
promisc    = True
route      = Network Netmask Gateway Iface Output IP Metric 0.0.0.0 0.0.0.0 ...
bufsize    = 65536
[...]
~~~

La modification d'un champs de configuration se fait naturellement, à la manière d'une allocation de variable traditionnelle : 

~~~python
>>> conf.iface = 'vio0'      # Modification de l'interface Scapy en vio0
~~~ 

### Intégration Python

Scapy est avant tout un module python, il est donc possible d’intégrer les fonctionnalités de Scapy à ses scripts python : 

~~~python
from scapy.all import *
from scapy.utils import *

[...]
~~~

Après quoi,  il est possible de créer des scripts : 

~~~python
# Exemple 3way handshake :
#--------------------------
from scapy.all import *

SYN=IP(dst="www.evolix.fr")/TCP(sport=2444, dport=80, flags="S", seq=42)
# SYN,ACK
SYNACK=sr1(SYN)
# ACK 
ACK=IP(dst="www.evolix.fr")/TCP(sport=SYNACK.dport, dport=80, flags="A", seq=SYNACK.ack, ack=SYNACK.seq + 1)
reply=sr1(ACK)
reply.show()

~~~

### Création de paquets

La bibliothèque de Scapy mets en place plusieurs méthodes et surcharges d’opérateurs permettant de *construire* un paquet "de bas en haut" suivant le modèle OSI.
En parallèle plusieurs classes dérivant du type ```PKT``` existent afin de représenter les protocoles et le processus d'encapsulation visuellement lors de la construction du paquet.

~~~python
>>> x = IP()
>>> x.show()
###[ IP ]### 
  version= 4
  ihl= None
  tos= 0x0
  len= None
  id= 1
  flags= 
  frag= 0
  ttl= 64
  proto= hopopt
  chksum= None
  src= 127.0.0.1
  dst= 127.0.0.1
  \options\
~~~

Ici ```x``` est un objet ```layer``` de niveau trois appartenant à la spécialisation IP et contenant tous les champs destinés à ce niveau.
Chaque champs est accessible de la même façon que l'on accéderait à un champs de dictionnaire traditionnel : 

~~~python
>>> x["IP"].dst = "9.9.9.9"
~~~

Pour construire les autres "layers" : 

~~~python
>>> y = x / TCP()    # OU
>>> y = IP() / TCP() 
>>> y.show()
###[ IP ]### 
  version= 4
  ihl= None
  tos= 0x0
  len= None
  id= 1
  flags= 
  frag= 0
  ttl= 64
  proto= tcp
  chksum= None
  src= 127.0.0.1
  dst= 127.0.0.1
  \options\
###[ TCP ]### 
     sport= ftp_data
     dport= http
     seq= 0
     ack= 0
     dataofs= None
     reserved= 0
     flags= S
     window= 8192
     chksum= None
     urgptr= 0
     options= []

~~~

L'opérateur ```/``` est l'opérateur **d'encapsulation**.
La méthode `show()` permets de visualiser la structure du paquet et de la manipuler.
L'inverse se fait au travers de la fonction ```del()```, versatile, qui permet de supprimer un layer, et des champs.

~~~python
#Suppression d'un layer
>>> del(y["TCP"])
>>> y.show()
###[ IP ]### 
  version= 4
  ihl= None
[...]
~~~

~~~python
#Suppression du contenu (reset) d'un champs
>>> y = IP(src="10.1.1.1",dst="9.9.9.9")
>>> y.summary()
'10.1.1.1 > 9.9.9.9 hopopt'
>>> del(y.src)
>>> y.summary()
'192.X.X.X > 9.9.9.9 hopopt'        # Récupération de l'addresse IP par défaut, celle de la stack du systeme.
~~~

#### Génération de collections de paquets

La génération de paquets peut se faire par expansion comme suit :

Par expansion simple : 

~~~python
>>> a = IP(dst="192.168.1.1/30")
>>> a
>>> [ p for p in a]
[<IP  dst=192.168.1.0 |>,
 <IP  dst=192.168.1.1 |>,
 <IP  dst=192.168.1.2 |>,
 <IP  dst=192.168.1.3 |>]
~~~

> *[] pour les listes d'attributs.*
> *() pour les ranges d'attributs.*

~~~python
>>> 
>>> b=IP(ttl=[1,2,(5,9)])
>>> b
<IP ttl=[1, 2, (5, 9)] |>
~~~

Par expansion conjuguée sur plusieurs champs et plusieurs couches : 

~~~python
>>> a = IP(dst="192.168.1.1/30")
>>> c=TCP(dport=[80,443])
>>> [x for x in a/c]
[<IP  frag=0 proto=tcp dst=192.168.1.0 |<TCP  dport=https |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.0 |<TCP  dport=http |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.1 |<TCP  dport=https |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.1 |<TCP  dport=http |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.2 |<TCP  dport=https |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.2 |<TCP  dport=http |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.3 |<TCP  dport=https |>>,
 <IP  frag=0 proto=tcp dst=192.168.1.3 |<TCP  dport=http |>>]
~~~
#### Modification de paquets

Lors de la génération, Scapy remplis les champs par des valeurs par défaut cohérentes, incluant adresses IPs, ports, checksum, TTL etc..

Afin de lister les Champs d'une couche et leur valeurs par défaut : 

~~~python
>>> ls(IP)
version    : BitField  (4 bits)                  = (4)
ihl        : BitField  (4 bits)                  = (None)
tos        : XByteField                          = (0)
len        : ShortField                          = (None)
id         : ShortField                          = (1)
flags      : FlagsField  (3 bits)                = (<Flag 0 ()>)
frag       : BitField  (13 bits)                 = (0)
ttl        : ByteField                           = (64)
proto      : ByteEnumField                       = (0)
chksum     : XShortField                         = (None)
src        : SourceIPField                       = (None)
dst        : DestIPField                         = (None)
options    : PacketListField                     = ([])
~~~

Pour entrer des informations spécifiques dans ces champs, nous pouvons : 

Utiliser le constructeur : 

~~~python
>>> a = IP(dst="192.168.1.1/30")
>>> a
<IP  dst=Net('192.168.1.1/30') |>
~~~

Modifier un champs par accès directe : 

~~~python
>>> a.ttl = 9
<IP  ttl=9 dst=Net('192.168.1.1/30') |>
~~~

Retourner aux défauts proposés : 

~~~python
>>> del(a.ttl)
<IP dst=Net('192.168.1.1/30') |>
~~~
### Écoute du réseau

~~~python
>>> x = sniff(filter="icmp and host 127.0.0.1", count=2, iface="lan0")
<Sniffed: TCP:0 UDP:0 ICMP:0 Other:2>
>>> x.show()
0001 Ether / ARP who has XXX.XXX.XXX says XXX.XXX.XXX / Padding
0002 Ether / ARP who has XXX.XXX.XXX says XXX.XXX.XXX / Padding
~~~

Il est possible de fournir une lambda fonction à exécuter sur chaque paquet : 

> Ici execute la méthode show() pour chaque paquet reçu.

~~~python
>>> x = sniff(iface="lan0", prn=lambda x: x.show())
~~~

> Afficher uniquement les adresses ip : 

~~~python
>>> x = sniff(iface="lan0", prn=lambda x: x.sprintf("%IP.src% --> %IP.dst%"))
~~~
#### Sauvegarde de communication écoutées 

##### Enregistrement dans des fichiers PCAP

~~~python
>>> wrpcap("filename.pcap", sniff(iface="lan0", prn=lambda x: x.sprintf("%IP.src% --> %IP.dst%")))
~~~

##### Lecture de fichier PCAP

~~~python
>>> x = rdpcap("filename.pcap")
~~~
##### Analyse de contenu

Manuel : 

~~~python
>>> x = rdpcap("filename.pcap")
>>> for p in x : 
>>>	p.show()
~~~

En utilisant la fonction sniff : 

~~~python
>>> sniff(offline="test.pcap",prn=lambda x: x.show())
~~~
### Émissions sur les réseau

Emission d'un paquet sur le réseau puis écoute de réponse sur un seul paquet : 

~~~python
>>> reponse = sr1(IP(dst="127.0.0.1")/ICMP()/"XXXXXXXXXXX")
~~~

Ecoute sur plusieurs paquets, classés answered et unanswered : 

~~~python
>>> ans,unans = sr(IP(dst="127.0.0.1")/ICMP()/"XXXXXXXXXXX")
~~~
#### A partir de fichiers PCAP

~~~python
>>> ans,unans = sr(rdpcap("filename.pcap"))
~~~