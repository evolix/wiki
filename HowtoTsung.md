---
title: Howto Tsung
categories: tips network utilities tools sysadmin 
...

* Site officiel : <http://tsung.erlang-projects.org/>
* Manuel de Tsung : <http://tsung.erlang-projects.org/user_manual/index.html>

Tsung (anciennement IDX-Tsunami) est un outil de test de monté en charge. Il peut être utilisé pour les services suivants : HTTP, WebDAV, SOAP, PostgreSQL, MySQL, AMQP, MQTT, LDAP et Jabber / XMPP.

Tsung est développé en Erlang.

## Principe

L’atout principal de Tsung réside dans sa capacité à simuler un très grand nombre d’utilisateurs simultanés à partir d’une seule machine. Il peut être également utilisé en tirant parti de plusieurs machines, en mode cluster. Lorsqu'il est utilisé sur un cluster, il permet de générer une charge vraiment très impressionnante à partir d'un cluster modeste, facile à configurer et à gérer.

## Installation

Sous Debian :

~~~
# apt install tsung
~~~

> *Note* : sur Debian 9/10 il y a [un souci](https://github.com/processone/tsung/issues/276) qui peut être contourné avec ces commandes :

~~~
# ln -s /usr/share /usr/lib/share
# ln -s /usr/lib/x86_64-linux-gnu/tsung /usr/lib/tsung
~~~

Sous OpenBSD :

~~~
# pkg_add tsung
~~~

## Utilisation

Pour utiliser Tsung il faut commencer par construire un scénario de test que l'on pourra ensuite donner en paramètre à la commande tsung et ainsi lancer un test de montée en charge.

### Constituer un scénario

Pour créer un scénario de test Tsung, il y a deux possibilités :

- écrire un scénario depuis un fichier d'exemple
- générer un scénario via le proxy Tsung fourni (`tsung-recorder`)

**Écriture d'un scenario depuis un fichier exemple**

Dupliquer et compléter les entêtes XML grâces aux exemples dans `/usr/share/doc/tsung/examples/`

Un exemple :

~~~
<?xml version="1.0"?>                                                                       
<!DOCTYPE tsung SYSTEM "/usr/share/tsung/tsung-1.0.dtd">                                    
<tsung loglevel="notice" version="1.0">                                                     
                                                                                            
  <!-- Client side setup -->                                                                
  <clients>                                                                                 
    <client host="mamachine" maxusers="30000"/>                                            
  </clients>                                                                                
                                                                                            
  <!-- Server side setup -->                                                                
<servers>                                                                                   
  <server host="www.test.com" port="80" type="tcp"></server>                          
</servers>                                                                                  
                                                                                            
                                                                                            
<load duration="5" unit="minute">                                                          
  <arrivalphase phase="1" duration="5" unit="minute">                                       
    <users interarrival="1" unit="second"></users>     
   </arrivalphase>                         
</load>                                                                                     

<sessions>                                                                                  
<session name='test' probability='100'  type='ts_http'>   
<request><http url='<http://www.example.com'> version='1.1' method='GET'></<http></request>
[…]
</session>
</sessions>
</tsung>
~~~

**Générer un scenario en enregistrant une session via tsung-recorder**

Tsung recorder est un serveur proxy fourni avec Tsung qui permet la capture de l'ensemble du trafic réseau HTTP/HTTPS pour le retranscrire au format XML. Ce fichier généré peut ensuite être exécuté par tsung en tant que script de test.

L'utilisation de tsung-recorder pour générer un scénario est à préférer car il permet de gérer les ressources liées qui sont souvent très nombreuses et pour lesquelles l'écriture depuis un fichier d'exemple comme décrit dans la section précédente représenterait un travail considérable.

Pour commencer à enregistrer une session de navigation via tsung-recorder on lance la commande suivante :

~~~
$ tsung-recorder start
~~~

Désormais, un serveur proxy est configuré et écoute le trafic sur le port `8090` (par défaut). On aura pris soin de modifier la configuration du nivagateur de son choix afin que celui-ci proxyfie ses requêtes via ce port.

À ce stade, lors de la navigation sur un site vous, devriez pouvoir voir la retranscription de la session de navigation s'enregistrer dans un fichier `tsung_recorderXXXXXXXX-XXX.xml` situé dans `~/.tsung/`.

Il faut noter que lors de l'utilisation du proxy, pour les navigations en HTTPS il faudra utiliser `http://-` au lieu de `https://` dans son navigateur.

Une fois arrivé en fin de scénario, on pourra arrêter l'enregistrement :

~~~
$ tsung-recorder stop
~~~

Le fichier XML enregistré se trouve donc dans `~/.tsung`.

### Lancer un scenario de test

Une fois le scénario constitué au format XML, on pourra passer le fichier en paramètre à Tsung :

~~~
tsung -f tsung-toto.xml start
~~~

> *Note* : Il faudra probablement accepter la signature SSH via `ssh localhost` au préalable.

### Consulter le test en cours

Tsung démarre un serveur web intégré et accessible sur [http://localhost:8091/]()

Cela permet de consulter les infos sur le test pendant son fonctionnement et même de stopper le test si on constate qu'il pose problème.

Si le test est lancé avec l'option `-k` le serveur web n'est pas arrêté lorsque le test est terminé.

### Générer les stats

~~~
cd ~/.tsung/log/20081103-16:21
/usr/lib/tsung/bin/tsung_stats.pl
~~~

## Améliorer les performances de Tsung

* ulimit
* Baisser le loglevel

## Mode multi clients

Il faut pouvoir se connecter en ssh via clé et sans passphrase.  
Il faut que le nom de machine du master résout sur les slaves. (On pourra modifier le fichier hosts).  
/!\\ Il faut la même version de tsung et erlang et des librairies sur chaque machine.  
/!\\ Il ne faut pas de pare-feu, des ports dynamiques sont ouverts pour communiquer entre le master et les slaves.  

## Erreur badmatch,{error,enoent}

Il faut créer des liens symboliques :

~~~
# ln -s /usr/share /usr/lib/share
# ln -s /usr/lib/x86_64-linux-gnu/tsung /usr/lib/tsung
~~~
