---
title: Howto PiVPN
categories: sysadmin network vpn
...

[PiVPN](https://www.pivpn.io/) est un wrapper autour des VPN [OpenVPN](/HowtoOpenVPN) et [WireGuard](/HowtoWireGuard). Nous l'utilisons pour la gestion de WireGuard.
Il a été conçu pour les Raspberry Pi mais fonctionne parfaitement sur une machine classique.

## Installation

Créer un utilisateur Unix `pivpn` :

~~~
# adduser pivpn
~~~

Cloner le dépôt et exécuter le script d'install :

~~~
# git clone https://github.com/pivpn/pivpn.git
# bash pivpn/auto_install/install.sh
~~~

Quand le script demande dans quel utilisateur il souhaite stocker les fichiers de configuration générés, choisir l'utilisateur pivpn.

Il est possible de modifier certaines valeurs par défaut dans `/etc/pivpn/wireguard/setupVars.conf`. Utile par exemple pour modifier les serveurs DNS ou le paramètre `AllowedIPs` par défaut pour les clients.

### Droits sudo

Facultatif : on peut configurer sudo pour le groupe `pivpn`, avec `visudo -f /etc/sudoers.d/pivpn` :

~~~
%pivpn          ALL = (ALL:ALL) /usr/local/bin/pivpn add
%pivpn          ALL = (ALL:ALL) /usr/local/bin/pivpn list
%pivpn          ALL = (ALL:ALL) /usr/local/bin/pivpn remove
%pivpn          ALL = (ALL:ALL) /usr/local/bin/pivpn clients
%pivpn          ALL = (ALL:ALL) /usr/local/bin/pivpn qrcode
%pivpn          ALL = (ALL:ALL) /usr/local/bin/pivpn backup
~~~

Puis de rajouter les utilisateurs ayant le droit d'utiliser pivpn dans le groupe `pivpn` : `adduser <user> pivpn`.

Il faut enfin ajuster les droits pour les utilisateurs du groupe `pivpn` puissent accéder en lecture aux fichiers de configuration générés :

~~~
# chmod g+x /home/pivpn /home/pivpn/configs
~~~

## Utilisation

L'utilisation est très simple, et l'aide suffisamment renseignée :

~~~
# pivpn
::: Control all PiVPN specific functions!
:::
::: Usage: pivpn <command> [option]
:::
::: Commands:
:::    -a, add              Create a client conf profile
:::    -c, clients          List any connected clients to the server
:::    -d, debug            Start a debugging session if having trouble
:::    -l, list             List all clients
:::   -qr, qrcode           Show the qrcode of a client for use with the mobile app
:::    -r, remove           Remove a client
:::  -off, off              Disable a client
:::   -on, on               Enable a client
:::    -h, help             Show this help dialog
:::    -u, uninstall        Uninstall pivpn from your system!
:::   -up, update           Updates PiVPN Scripts
:::   -bk, backup           Backup VPN configs and user profiles
~~~

### Création d'une configuration client

À la création d'une configuration pour un client, un nom est demandé. Il n'est utilisé que localement pour que l'utilisateur sache à quoi correspond la configuration, mais ce nom n'a aucune incidence sur la configuration elle-même.

WireGuard est automatiquement rechargé, et tous les fichiers nécessaires sont générés ou modifiés :

* `/etc/wireguard/configs/clients.txt` : liste les clients pour lesquels une configuration existe
* `/etc/wireguard/wg0.conf` : configuration pour le serveur, une section "[Peer]" est créée avec le nouveau client
* `/etc/wireguard/configs/<user>.conf` et `/home/pivpn/configs/<user>.conf` : configuration générée et à transmettre au client
* `/etc/wireguard/keys/<user>_priv` : clé privée du client
* `/etc/wireguard/keys/<user>_psk` : clé pré-partagée entre le client et le serveur
* `/etc/wireguard/keys/<user>_pub` : clé publique du client

Seul le fichier de configuration `/etc/wireguard/wg0.conf` est utile au bon fonctionnement de WireGuard. Le reste est utile à la gestion par PiVPN.