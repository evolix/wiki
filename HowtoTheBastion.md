---
categories: sysadmin
title: Howto The Bastion
...

* [Documentation officielle](https://ovh.github.io/the-bastion/)

The Bastion est un bastion SSH créé par OVH permettant la centralisation des accès SSH et la traçabilité et l'auditabilité des actions faîtes par SSH.

## Installation

The Bastion peut s'installer sur une machine Debian sécurisée. Il est recommandé de chiffrer le `/home` car il contient les clés privées SSH ainsi que les enregistrements de sessions.

```
git clone https://github.com/ovh/the-bastion /opt/bastion
git -C /opt/bastion checkout $(git -C /opt/bastion tag | tail -1)
/opt/bastion/bin/admin/packages-check.sh -i
/opt/bastion/bin/admin/install-ttyrec.sh -a
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.old
/opt/bastion/bin/admin/install --new-install
/opt/bastion/bin/dev/perl-check.sh
```

Puisque The Bastion modifie la configuration SSH pour augmenter sa sécurité, il faut comparer la configuration ssh initial avec celle fournie par The Bastion.

```
vimdiff /etc/ssh/sshd_config.old /etc/ssh/sshd_config
```

### Configuration

La configuration principale est gérée dans `/etc/bastion/bastion.conf`.

### Création du compte administrateur initial

```
/opt/bastion/bin/admin/setup-first-admin-account.sh "${username:?}" auto
```

## Administration

Toutes les commandes se font à partir du shell d'un compte SSH Bastion.

### Créer un compte utilisateur

```
accountCreate --account <nom_de_compte>
```

Il sera ensuite demandé de fournir une clé publique SSH (au format d'OpenSSH) pour la connexion à ce compte.

### Lister les comptes utilisateurs

```
accountList
```

Pour plus d'information (nécessite d'être d'avoir accès à la commande spéciale `auditor`) (c'est bien plus long) :

```
accountList --audit
```

### Ajouter un accès à un serveur distant pour un utilisateur

Pour créer un accès à un serveur distant pour un utilisateur il faut exécuter la commande suivante :

```
accountAddPersonalAccess --account <nom_du_compte_utilisateur> --host <IP|nom_d_hote|IP/mask> --port <port_ssh_du_serveur_distant> --user <nom_utilisateur_sur_serveur_distant>
```

Pour permettre l'utilisation de sftp pour cet accès, il faut créer un second accès au serveur avec la commande suivante :

```
accountAddPersonalAccess --account <nom_du_compte_utilisateur> --host <IP|nom_d_hote|IP/mask> --port <port_ssh_du_serveur_distant> --sftp
```

### Donner accès à une commande privilégiée

```
accountGrantCommand --account <nom_du_compte_utilisateur> --command <commande>
```

### Révoquer accès à une commande privilégiée

```
accountRevokeCommand --account <nom_du_compte_utilisateur> --command <commande>
```

### Bloquer et débloquer un compte

Pour bloquer manuellement un compte :

```
accountFreeze --account <nom_du_compte_utilisateur> [--reason "'<raison_du_bloquage>'"]
```

Pour débloquer un compte bloqué manuellement :

```
accountUnfreeze --account <nom_du_compte_utilisateur>
```

Pour débloquer un compte bloqué automatiquement par `pam_tally`, `pam_tally2` ou `pam_faillock` :

```
accountUnlock --account <nom_du_compte_utilisateur>
```

### Lister qui a accès à un serveur distant

```
whoHasAccessTo --host <serveur_distant> [--port <port_ssh>] [--user <utilisateur_distant>]
```

Si `--port` ou `--user` ne sont pas précisés, alors le champ correspondant n'est pas pris en compte.

> Note: Cela ne vérifie pas que l'accès soit fonctionnel mais seulement que le bastion soit configuré pour autoriser cet accès.

### Voir ce qui a été fait lors d'une session SSH distante

À chaque connexion ssh vers un serveur distant, un `ttyrec` est démarré, enregistrant tout ce qui est affiché sur la console utilisateur. Le chemin de ces enregistrements est `/home/<compte_utilisateur_bastion>/ttyrec/<IP_du_serveur_distant>/<fichier>.ttyrec`.

Il est possible de jouer ses enregistrements `ttyrec` avec la commande bastion `selfPlaySession`. Il est possible de jouer les enregistrements `ttyrec` d'autrui, uniquement avec accès local au bastion, en utilisant le programme `ttyplay` (ou tout autre programme pouvant lire les fichiers générés par `ttyrec`).

## Utilisation courante

> Lors de la création initiale du compte (et à chaque exécution de la commande bastion `info`), un alias est fourni pour `ssh <compte_bastion>@<bastion> -t --`.

### Connexion à un serveur distant

```
ssh <compte_bastion>@<bastion> -t -- <utilisateur_distant>@<serveur_distant>
```

### Utilisation d'une commande bastion

```
ssh <compte_bastion>@<bastion> -t -- --osh <commande_bastion>
```

### Utilisation de sftp

Récupérer le script d'aide :

```
ssh <compte_bastion>@<bastion> -t -- --osh sftp
```

Puis suivre les instructions pour l'installer sur sa machine, puis (considérant que le script est `~/sftp_bssh`) :

```
sftp -S ~/sftp_bssh <utilisateur_distant>@<serveur_distant>
```

Il est possible qu'il faille modifier le script d'aide pour qu'il utilise le FQDN du bastion au lieu de son nom court.
