---
categories: Tang chiffrement
title: Howto Tang
...

* Github / Documentation : <https://github.com/latchset/tang>

[Tang](https://github.com/latchset/tang) est un serveur pour permettre le déblocage/déchiffrement automatique de ressources quand elles sont présentes dans un réseau donné. C'est parfois appellé NBDE "Network Bound Disk Encryption". Il est utilisé comme PIN par [Clevis](./HowtoClevis)


## Installation

~~~
# apt install tang

# systemctl status tangd.socket
● tangd.socket - Tang Server socket
     Loaded: loaded (/lib/systemd/system/tangd.socket; enabled; preset: enabled)
     Active: active (listening) since Mon 2024-05-02 14:48:44 CEST; 17s ago
     Listen: [::]:80 (Stream)
   Accepted: 0; Connected: 0;
      Tasks: 0 (limit: 9144)
     Memory: 8.0K
        CPU: 613us
     CGroup: /system.slice/tangd.socket

may 02 14:48:44 hostname systemd[1]: Listening on tangd.socket - Tang Server socket.
~~~

## Configuration

Tang n'a pas de daemon actif. C'est une socket de [systemd](./HowtoSystemd) (`tangd.socket`) qui est en écoute. Elle lancera un service `tangd@` qui se chargera de répondre à la requête.

Par défaut la socket écoute sur le port 80 de toutes les interfaces réseau pour tangd. Il est possible de changer ce réglage via une surcharge (override) de la configuration systemd avec la commande `systemctl edit tangd.socket` (Ou alors manuellement éditier un fichier `/etc/systemd/system/tangd.socket.d/evolinux.conf` puis recharger la configuration `systemctl daemon-reload` et redémarrer la socket `systemctl restart tangd.socket`)


Exemple pour forcer l'écoute sur le port 42 de l'inteface d'un réseau privée ayant 192.0.2.10 comme IP (et avec localhost en plus) :

```
# systemctl edit tangd.socket

### Editing /etc/systemd/system/tangd.socket.d/override.conf
### Anything between here and the comment below will become the new contents of the file

[Socket]
# Rappel : Dans une surcharge de configuration systemd, si on souhaite écraser totalement des directives pouvant être répétées (comme ListenStream), il faut la préciser une première fois sans valeur. Dans le cas contraire, ça sera défini en plus de ce qu'il y a dans le fichier originel.
ListenStream=
ListenStream=127.0.0.1:42
ListenStream=[::1]:42
ListenStream=192.0.2.10:42

[Unit]
After=network-online.target
Wants=network-online.target
```

Référence : [systemd.socket(5)](https://manpages.debian.org/bookworm/systemd/systemd.socket.5.en.html)

## Administration

Les clés cryptographiques sont enregistrées dans `/var/lib/tang`. S'il n'y a pas de clés présente, elle sera générée à la premère requête. Les fichier de clés cachés (ie: commençant par un ".") ne sont pas annoncés, mais sont toujours utilisables par les clients, le  temps qu'ils basculent sur les nouvelles clés.

### Afficher la/les clé actuelle

On peut afficher localement l'identifiant de la clé active avec la commande `tang-show-keys [port (si différent de 80)]`

Exemple (avec tang sur le port *42* ) :

```
# tang-show-keys 42
iYzCrNa251c3iSXP5bc3o8EZpPgy6l4nFZisgeFHZmE
```

> *Remarque* : On ne peut pas préciser l'URL, seulement le port. Il faut donc que la socket de tang écoute aussi sur localhost.

### Rotation de clés

Il est possible de renouveller les clés utilisées par tang. L'ancienne clée sera mise de côté (ie: renommage du fichier avec un "." en début). Elle reste utilisable mais elle ne sera plus annoncée.

L'outil `/usr/libexec/tangd-rotate-keys` permet de faire cette opération simplement. Il faut spécifier avec l'option *-d* le dossier des clés.

Exemple :

```
# /usr/libexec/tangd-rotate-keys
Please specify the keys directory with -d switch
Usage: /usr/libexec/tangd-rotate-keys [-h] [-v] -d <KEYDIR>

Perform rotation of tang keys

  -d KEYDIR  The directory with the keys, e.g. /var/db/tang

  -h         Display this usage information

  -v         Verbose. Display additional info on keys created/rotated


# tang-show-keys 42
zlY3DlZ-RsvtFfRR4VMHX-WU8Bwmqw-wWLQVc56UdP8

# /usr/libexec/tangd-rotate-keys -v -d /var/lib/tang/
Disabled advertisement of key YjndnmP4TKYBVKVukvf-2i3RdrWrynovBnmV8Uxh6p8.jwk -> .YjndnmP4TKYBVKVukvf-2i3RdrWrynovBnmV8Uxh6p8.jwk
Disabled advertisement of key zlY3DlZ-RsvtFfRR4VMHX-WU8Bwmqw-wWLQVc56UdP8.jwk -> .zlY3DlZ-RsvtFfRR4VMHX-WU8Bwmqw-wWLQVc56UdP8.jwk
Created new key X4rKb0DNu7rpmrjHF0SbeQfMQ-mUoVHdUiiNOmiFx1c.jwk
Created new key -5uJrqweM-kkHFOtfi_UWBbjPUJs8h7w2VfBascFaSw.jwk
Keys rotated successfully

# tang-show-keys 42
X4rKb0DNu7rpmrjHF0SbeQfMQ-mUoVHdUiiNOmiFx1c

```

Il faut ensuite mettre à jour les bindings de tous les clients avec la nouvelle clé.
Voir [dans le HowtoClevis, section rotation de clés](./HowtoClevis#rotation-de-clés-de-tang)

Il ne faut pas supprimer les clés désactivées. Même non-annoncées elles permettent toujours à des clients qui ne sont pas encore à jour de dévérouiller leurs éléments chiffrés.

## Monitoring

## FAQ
