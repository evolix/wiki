---
categories: network
title: Howto iperf
...

* Documentation : <https://iperf.fr/iperf-doc.php>
* Manpage : <https://manpages.debian.org/stable/iperf/iperf.1.en.html>
* Code source iperf v3 : <https://github.com/esnet/iperf>
* FAQ iperf v3 : <http://software.es.net/iperf/faq.html>

[iperf](http://software.es.net/iperf/) est un outil de diagnostic réseau permettant de mesurer le débit sur des réseaux IP.


## Principe

iperf doit être installé sur les deux machines entre lesquelles le débit est mesuré : il sera lancé en mode serveur sur l'une, et en mode client sur l'autre.

Si l'on utilise le protocole TCP, le débit sera aussi élevé que la liaison de bout en bout le permet ; avec le protocole UDP, le client devra choisir quel débit émettre ou recevoir, et un rapport affichera le nombre de paquets perdus.

iperf a été ré-écrit en version 3 (commande `iperf3`), avec notamment le support d'IPv6. Suivant les cas d'usage, nous utilisons encore iperf en version 2 (commande `iperf`) notamment dans le cas de tests en mode UDP où iperf3 a pas mal de bugs.

Par défaut iperf3 en mode serveur ouvre la socket réseau `TCP/5201`, pour iperf c'est `TCP/5001`.


## Installation

Sous Debian :

~~~
# apt install iperf iperf3
~~~

Sous OpenBSD :

~~~
# pkg_add iperf iperf3
~~~


## Utilisation basique

Lancer en mode serveur :

~~~
$ iperf3 -s
-----------------------------------------------------------
Server listening on 5201
-----------------------------------------------------------
~~~

Sur le client, on lance le test de débit pendant 10 secondes :

~~~
$ iperf3 -c 192.0.2.1 -t 10
Connecting to host 192.0.2.1, port 5201
[  4] local 192.0.2.2 port 56832 connected to 192.0.2.1 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec  11.9 MBytes  99.8 Mbits/sec    0    129 KBytes       
[  4]   1.00-2.00   sec  11.3 MBytes  94.9 Mbits/sec    0    156 KBytes       
[  4]   2.00-3.00   sec  11.2 MBytes  93.8 Mbits/sec    0    156 KBytes       
[  4]   3.00-4.00   sec  11.1 MBytes  93.3 Mbits/sec    0    156 KBytes       
[  4]   4.00-5.00   sec  11.1 MBytes  92.8 Mbits/sec    0    163 KBytes       
[  4]   5.00-6.00   sec  11.1 MBytes  93.3 Mbits/sec    0    163 KBytes       
[  4]   6.00-7.00   sec  11.2 MBytes  93.8 Mbits/sec    0    163 KBytes       
[  4]   7.00-8.00   sec  11.2 MBytes  93.8 Mbits/sec    0    163 KBytes       
[  4]   8.00-9.00   sec  11.1 MBytes  92.8 Mbits/sec    0    163 KBytes       
[  4]   9.00-10.00  sec  11.2 MBytes  93.8 Mbits/sec    0    163 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec   112 MBytes  94.2 Mbits/sec    0             sender
[  4]   0.00-10.00  sec   111 MBytes  93.4 Mbits/sec                  receiver

iperf Done.
~~~


## Utilisation avancée

### Firewall

Afin de pouvoir utiliser iperf, il faut s'assurer que les ports entre le client et le serveur sont bien ouverts.
Par défaut il faut donc autoriser les flux TCP entre le port client (1024 et 65535) et le port serveur (5201).

### UDP

Par défaut en mode UDP, une bande passante de **1Mb/s** sera utilisée.
Il faut préciser la bande passante souhaitée pour le test avec l'option `-b`, par exemple pour 10Mb/s : `-b 10M`.

#### iperf (version 2)

Il faut lancer explicitement le serveur en mode UDP :

~~~
$ iperf -s -u
------------------------------------------------------------
Server listening on UDP port 5001
Receiving 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
~~~

On peut alors lancer le client en mode UDP :

~~~
$ iperf -c 192.0.2.1 -u -b 10M
------------------------------------------------------------
Client connecting to 192.0.2.1, UDP port 5001
Sending 1470 byte datagrams, IPG target: 1176.00 us (kalman adjust)
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  3] local 192.0.2.2 port 50090 connected with 192.0.2.1 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec  11.9 MBytes  10.0 Mbits/sec
[  3] Sent 8505 datagrams
[  3] Server Report:
[  3]  0.0-10.0 sec  11.9 MBytes  10.0 Mbits/sec   0.051 ms    0/ 8505 (0%)

~~~

#### iperf3

Avec iperf3, on ne lance pas le serveur en mode UDP, c'est le client qui va gérer la bascule sur le protocole UDP.
Ce comportement s'explique notamment pour des raisons de sécurité, pour éviter de pouvoir envoyer trop facilement un flux UDP vers n'importe quelle IP.

On lance donc le serveur de façon standard :

~~~
$ iperf3 -s
-----------------------------------------------------------
Server listening on 5201
-----------------------------------------------------------
~~~

Et on précise le mode UDP au niveau client :

~~~
$ iperf3 -c 192.0.2.1 -u -b 10M
Connecting to host 192.0.2.1, port 5201
[  4] local 192.0.2.2 port 34378 connected to 192.0.2.1 port 5201
[ ID] Interval           Transfer     Bandwidth       Total Datagrams
[  4]   0.00-1.00   sec  1.09 MBytes  9.17 Mbits/sec  140  
[  4]   1.00-2.00   sec  1.19 MBytes  9.96 Mbits/sec  152  
[...]
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-10.00  sec  11.8 MBytes  9.92 Mbits/sec  0.539 ms  0/1512 (0%)  
[  4] Sent 1512 datagrams

iperf Done.
~~~


### Options

Options générales intéressantes :

* `-p N` : utiliser un port spécifique
* `-i N` : afficher un rapport intermédiaire toutes les N secondes
* `-J` : sortie au format JSON (iperf3 uniquement)

Options intéressantes pour le serveur :

* `-D` : lancer en mode daemon
* `-1` : traiter un seul client et quitter (iperf3 uniquement)

Options intéressantes pour le client :

* `-t N` : transmission pendant N secondes (au lieu de 10 secondes par défaut)
* `-P N` : lancer N clients en parallèle
* `-d` : lancer une transmission bidirectionnelle simultanément (iperf v2 uniquement)
* `-r` : lancer une transmission bidirectionnelle séparément (iperf v2 uniquement)
* `-R` : lancer une transmission dans le sens inverse (le client reçoit, iperf3 uniquement)


## Serveurs iperf publics

Maintenir un serveur iperf public est complexe, la plupart des serveurs de la liste <https://iperf.fr/iperf-servers.php> sont injoignables en général. Voici les serveurs les plus fiables de la liste :

~~~
$ iperf3 -c ping.online.net
$ iperf3 -c iperf.eenet.ee
~~~
