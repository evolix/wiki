---
categories: mail
title: Howto OfflineIMAP
...

* Documentation : <https://www.offlineimap.org/documentation.html>
* Statut de cette page : test / bookworm

[OfflineIMAP](https://www.offlineimap.org/) est un outil pour faire une synchronisation à deux sens
depuis un serveur IMAP distant vers une « Maildir » locale.

## Installation

~~~
# apt install offlineimap3
~~~

## Utilisation

~~~
$ offlineimap -c .offlineimaprc42

$ cat .offlineimaprc42

[general]
accounts = Test42

[Account Test42]
localrepository = Local42
remoterepository = Remote42

[Repository Local10]
type = Maildir
localfolders = ~/offlineimap/foo@example.com/Maildir/

[Repository Remote10]
type = IMAP
remotehost = imap.example.com
cert_fingerprint = XXX
remoteuser = foo@example.com
remotepass = PASSWORD
readonly = True
~~~


