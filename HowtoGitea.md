---
title: Howto Gitea
categories: sysadmin git forge
...

**ATTENTION**. Cette documentation n'est plus mise à jour, Nous avons basculé vers [Forgejo](./HowtoForgejo)


* Documentation : <https://docs.gitea.io/fr-fr/>


[Gitea](https://gitea.io/) est un logiciel libre de gestion de code source permettant de créer des projets avec un dépôt [Git](HowtoGit), des tickets, un wiki et une collaboration autour du code (fork, pull requests, etc.). Gitea est un fork de [Gogs](https://gogs.io/) qui ressemble fortement au logiciel propriétaire *Github*, il est écrit en Go et nous le trouvons simple à installer et maintenir contrairement à [Gitlab](HowtoGitlab/10).


## Installation


Malheureusement, Gitea n'est [pas encore distribué sous forme de paquet Debian](http://bugs.debian.org/792101). Une alternative est donc d'utiliser directement les binaires produits par les développeurs de Gitea. Ils sont récupérables sur <https://dl.gitea.io/gitea/>.

~~~
# cd /usr/local/bin
# wget https://dl.gitea.io/gitea/1.5.2/gitea-1.5.2-linux-amd64
# chmod +x gitea-1.5.2-linux-amd64
# ln -s gitea-1.5.2-linux-amd64 gitea

# mkdir /etc/gitea/

# gitea --version
Gitea version 1.5.2 built with: bindata, sqlite
~~~

> *Note* : Même si gitea peut fonctionner de manière autonome avec SQLite, à partir d'une certaine taille/utilisation, il est préférable de basculer vers un moteur de base de données tel que [HowtoMySQL]() ou [HowtoPostgreSQL]() et d'ajouter [HowtoRedis]() pour le cache/sessions.


## Mise à jour

Le processus de mise à jour est assez simple et similaire au processus d'installation. Après avoir regardé s'il n'y a pas d'actions à faire manuellement, on peut simplement télécharger le dernier binaire dans `/usr/local/bin/`. Lui appliquer les bon droits (*chmod 755*) et mettre à jour le lien symbolique `/usr/local/bin/gitea` vers ce nouveau binaire.

Il ne reste plus qu'à redémarrer l'instance. C'est terminé !


## Configuration 

Dans la suite de cette documentation, on se place dans le cas d'une instance simple, fonctionnant avec l'utilisateur **git**. On part du principe que :

* [MySQL](HowtoMySQL) est déjà installé
* Le compte UNIX **git** existe sur la machine. 
* La configuration sera écrire dans `/etc/gitea/git.ini` 
* Tous les fichiers de gitea seront dans `/home/git/` 
* L'adresse de l'instance sera gitea.example.org

Au final, Côté fichiers applicatifs, on va se retrouver avec la hiérarchie suivante : 

~~~
$ tree /home/git
├── internals 
│   ├── custom            << Fichiers de customisation de l'interface web de gitea
│   │   ├── public        << Fichiers servis par le serveur web
│   │   └── templates     << Templates des pages
│   │       └── home.tmpl
│   │ 
│   └── data              << Toutes les données de gitea (autres que les dépôts git)
│       ├── avatars
│       ├── indexers
│       ├── lfs
│       └── sessions
│
├── log                    << Journaux applicatifs
│   ├── gitea.log
│   ├── http.log
│   ├── serv.log
│   └── xorm.log
│
└── repositories            << Données des dépôts git
    └── foo
        └── hello_world.git
~~~

Ces dossiers seront créés au premier lancement de Gitea. Il faut donc s'assurer que certaines variables d'environnement sont bien positionnées. 
 
Lors de la première exécution de Gitea, une page d'installation va demander tous les paramètres nécessaires pour initialiser l'instance. Celle-ci va aussi permettre de créer le premier compte administrateur. 

### /etc/gitea/git.ini

Avant de commencer, on doit initialiser le fichier de configuration de gitea avec les commandes suivantes. 
A noter qu'on ne laisse les droits d'écritures uniquement pour l'installation. 

~~~
# touch /etc/gitea/git.ini
# chown root:git /etc/gitea/git.ini
# chmod g+rw /etc/gitea/git.ini
~~~

On ajoute aussi quelques réglages initiaux, pour que l'interface de Gitea soit directement fonctionnelle (avec des logs en cas de souci) :

~~~
# cat /etc/gitea/git.ini
APP_NAME = Gitea
RUN_USER = git
RUN_MODE = prod

[server]
PROTOCOL               = unix
DOMAIN                 = gitea.example.org
HTTP_ADDR              = /home/git/gitea.sock
UNIX_SOCKET_PERMISSION = 660
OFFLINE_MODE           = true
SSH_DOMAIN             = gitea.example.org
ROOT_URL               = http://gitea.example.org/

[repository]
ROOT                   = /home/git/repositories

[log]
ROOT_PATH              = /home/git/log/
MODE                   = file
LEVEL                  = Info

[i18n]
LANGS = fr-FR, en-US
NAMES = Français,English
~~~

### systemd

On peut utiliser l'unité systemd suivante. Elle est inspirée de celle [proposée par les développeurs de gitea](https://github.com/go-gitea/gitea/blob/master/contrib/systemd/gitea.service). Nous l'avons ajustée pour correspondre au dossiers et réglages choisis plus haut.

~~~
# cat /etc/systemd/system/gitea.service

[Unit]
Description=Gitea (Git with a cup of tea) 
After=syslog.target
After=network.target

[Service]
User=git
Group=git

Type=simple
RestartSec=2s
Restart=always

WorkingDirectory=/home/git
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/git.ini
Environment=GITEA_WORK_DIR=/home/git/internals 


[Install]
WantedBy=multi-user.target
~~~

### MySQL

Par défaut, Gitea utilise une base SQLite. Mais on peut utiliser MySQL (ou PostgreSQL). 
Gitea nécessite seulement une base MySQL avec un compte.

~~~
# mysqladmin create git
# mysql
mysql> GRANT ALL PRIVILEGES ON git.* TO 'git'@localhost IDENTIFIED BY 'PASSWORD';
~~~

Côté `gitea.ini`, la base de donnée MySQL se configure avec la section ci-dessous. 

> *Note* : La page d'installation peut remplir cette section de la configuration automatiquement.

~~~
[database]
DB_TYPE  = mysql
HOST     = /run/mysqld/mysqld.sock
NAME     = git
USER     = git
PASSWD   = PASSWORD
~~~

### Nginx

~~~
# cat /etc/nginx/sites-enabled/git
upstream gitea_git {
    server unix:/home/git/gitea.sock;
}

server {
    listen 0.0.0.0:80;
    listen [::]:80;
    #listen 0.0.0.0:443 ssl http2;
    #listen [::]:443 ssl http2;

    server_name gitea.example.org;

    #include /etc/nginx/letsencrypt.conf;
    #include /etc/nginx/ssl/git.conf;

    #if ( $scheme = http ) {
    #    return 301 https://$server_name$request_uri;
    #}

    location / {
        proxy_pass http://gitea_git;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_read_timeout 10;
    }
}
~~~

### Redis

On peut utiliser Redis comme support pour les sessions et le cache de Gitea.

#### Cache

~~~
# cat /etc/gitea/git.ini
[cache]
ADAPTER = redis
HOST    = network=tcp,addr=127.0.0.1:6379,password=PASSWORD,db=1,pool_size=100,idle_timeout=180

~~~

#### Sessions

~~~
# cat /etc/gitea/git.ini
[session]
PROVIDER         = redis
PROVIDER_CONFIG  = network=tcp,addr=127.0.0.1:6379,password=PASSWORD,db=0,pool_size=100,idle_timeout=180
~~~

### HTTPS

Après avoir activé le HTTPS sur le vhost nginx faisant office de reverse-proxy, il est utile d'ajuster quelques paramètres de configuration.

* Mettre à jour le `ROOT_URL` pour remplacer `http://gitea.example.org` par `https://gitea.example.org`
* Ajouter `COOKIE_SECURE    = true` dans la section `[session]`
* (Optionnel) Rajouter le préfixe `__Secure-` au nom des cookies. Il s'agit des paramètres `COOKIE_NAME` dans la section `[session]` ainsi que `COOKIE_USERNAME` et `COOKIE_REMEMBER_NAME` dans la section `[security]`


## Avancé

### Multiples instances

La simplicité de gitea permet d'heberger plusieurs instances sur une même machine.

Comme pour la première instance, il a besoin de :

* D'un nouveau compte unix spécifique
* (Si MySQL) D'un nouveau compte avec une base associée.

On peut ainsi basculer sur une unité systemd avec paramètre comme celle donnée en exemple.
Ainsi, les instances seront manipulées avec `systemctl (start|restart|stop) gitea@INSTANCE` où _INSTANCE_ se trouve être le nom de compte dédié à l'instance gitea


~~~
# cat /etc/systemd/system/gitea@.service

[Unit]
Description=Gitea (Git with a cup of tea) for %i
After=syslog.target
After=network.target
After=mysqld.service

[Service]
User=%i
Group=%i

Type=simple
RestartSec=2s
Restart=always

WorkingDirectory=/home/%i
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/%i.ini
Environment=GITEA_WORK_DIR=/home/%i/internals 

[Install]
WantedBy=multi-user.target
~~~

## Miroir

Les dépôts peuvent être configurés en miroir d’autres forges, ou pour mettre à jour des miroir sur d’autres forges.

### Gitlab (push)

Pour maintenir un [miroir sur une instance de Gitlab](https://docs.gitea.io/en-us/repo-mirror/#setting-up-a-push-mirror-from-gitea-to-gitlab), les quelques étapes suivantes suffisent.

1. Créer un [jeton d’accès personnel](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) **sur Gitlab** avec portée (« scope ») `write_repository`.

  * Sélectionner l’avatar en haut à droite.
  * Sélectionner « Modifier le profil » (« Edit profile »).
  * Dans la barre de gauche, sélectionner « Jetons d’accès » (« Access Tokens »).
  * Saisir un nom et éventuellement un date d’expiration pour le jeton.
  * Sélectionner la portée (« scope ») `write_repository`.
  * Cliquer sur « Créer jeton d’accès personnel » (« Create personal access token »).

2. Dans les réglages (« Settings » en haut à droite) du dépôt sur Gitea, entrer l’URL du dépôt distant (« Git Remote Repository URL ») dans les réglages du miroir (« Mirror Settings ») : `https://<hôte_de_destination>/<nom_ou_groupe_gitlab>/<projet_gitlab>.git`.

3. Remplir les champs d’autorisation « Authorization » avec `oauth2` comme nom d’utilisateur (« Username ») et le jeton d’accès personnel précédent comme mot de passe (« Password »).

4. Cliquer sur « Add Push Mirror » pour sauvegarder la configuration.

## FAQ / Plomberie

### Erreur d'enregistrement avec SHOW_REGISTRATION_BUTTON = false

A l'heure actuelle (Nov. 2018) il n'est pas possible de laisser les inscriptions actives (`DISABLE_REGISTRATION = false`) tout en masquant le lien d'inscription (`SHOW_REGISTRATION_BUTTON = false`).
Le formulaire d'inscription sera bien accessible. Mais quand on le valide, une erreur 403 est retournée.

C'est un [bug connu](https://github.com/go-gitea/gitea/issues/5183) par les développeurs de Gitea.

### La page d'accueil d'un projet ne se met pas à jour après le premier push

Il faut s'assurer que la partition qui contient le dossier des dépôts gits ne soit **pas** en `noexec`

### Impossible de visionner le diff d'un gros commit

> File diff suppressed because it is too large.

L'astuce consiste à mettre dans le fichier de configuration :

```
[git]
MAX_GIT_DIFF_LINES = 1000000
```

Augmenter à la valeur de votre choix.

### Chercher tous les accès d'un compte

```
# mysql
MariaDB [(none)]> use git
MariaDB [git]> select repository.name, user.name, access.mode from access  INNER JOIN user ON user.id = access.user_id INNER JOIN repository ON repository.id = access.repo_id WHERE user.name like '%USER%' \G
*************************** 1. row ***************************
name: REPO
name: USER
mode: 2
1 row in set (0.00 sec)
```
