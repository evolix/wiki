---
categories: web
title: Howto Aegir
...

* Documentation : <https://docs.aegirproject.org/>

[Aegir](https://www.aegirproject.org/) est un système de gestion et de déploiement de sites web [Drupal](HowtoDrupal) en mode [multisites](https://www.drupal.org/docs/multisite-drupal).
On peut par exemple déployer du Drupal 8, 9 ou 10 en local ou sur des serveurs distants.

Code source : <https://github.com/aegir-project>

Aegir est composé des éléménts suivants :

* [hostmaster](https://www.drupal.org/project/hostmaster) : interface web en Drupal 7 : <https://www.drupal.org/project/hostmaster>
* [provision](https://www.drupal.org/project/Provision) : suite de commandes [drush](HowtoDrupal#drush) pour installer des sites Drupal (cf `/usr/share/drush/commands/provision/`
* [hosting](https://www.drupal.org/project/hosting) : modules Drupal pour permettre l'UI de l'interface web d'Aegir
* [eldir](https://www.drupal.org/project/eldir) : thème Drupal pour Aegir

## Installation

Pré-requis : Debian 10 ou Debian 11 avec PHP par défaut, on passera en PHP 8.2 dans un second temps (pour Debian 12 voir [installation sous Debian 12](#installation-sous-debian-12))

Préparer un service [MySQL](HowtoMySQL) permettant ainsi au package Debian `aegir3-hostmaster` va notamment créer un utilisateur MySQL `aegir_root`.
Si MySQL n'est pas utilisable sans mot de passe via l'utilisateur Unix `root`, le package `aegir3-hostmaster` a besoin d'un identifiant MySQL "admin" et va le demander via debconf (bizarrement même si MySQL est utilisable sans mot de passe, debconf demande quand même le mot de passe : laisser le champ vide) :

~~~
# echo "deb [signed-by=/usr/share/keyrings/pub_evolix.asc] http://pub.evolix.org/evolix aegir main" > /etc/apt/sources.list.d/aegir.list

## la clef est fournie par le paquet evolix-archive-keyring
# wget https://pub.evolix.org/evolix/pool/main/e/evolix-archive-keyring/evolix-archive-keyring_0~2024_all.deb
# apt install ./evolix-archive-keyring_0~2024_all.deb

# apt update && apt install aegir3

Unpacking aegir3 (3.192) ...
...
Adding user aegir to group www-data
Creating config file /etc/sudoers.d/aegir with new version
Setting up aegir3-hostmaster (3.192) ...
'drush' cache was cleared.                                           [success]
...
Adding hosting-dispatch cron entry to run every minute               [ok]
The command did not complete successfully, please fix the issues and [error]
re-run this script
Aegir 7.x-3.192 automated install script

Enabling hosting-queued daemon
The following extensions will be enabled: hosting_queued
Do you really want to continue? (y/n): y
hosting_queued was enabled successfully.                             [ok]
Enabling Hosting queue daemon feature.                               [status]
Synchronizing state of hosting-queued.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable hosting-queued
...
Use this URL to login on your new site:
http://aegir.example.com/user/reset/1/1664384789/BPynMpO0tjjTOlvr8aOW4z7g0rNGsfKgGEFOuUr8oVU/login

# ls -d /var/aegir/hostmaster-*
/var/aegir/hostmaster-7.x-3.192+nmu1

# /usr/local/bin/drush version
 Drush Version   :  8.4.8 
~~~

> Note 1 : le dépôt APT Aegir n'existant plus, nous hébergeons les vieux paquets Aegir récupérés via [Koumbit (merci!!)](http://debian.koumbit.net/debian/pool/main/a/aegir3-provision/)

> Note 2 : sur certaines installations, il faut relancer une 2e fois l'installation ou lancer un hack en parallèle de l'installation du type `screen -S aegir-dpkg-workaround -dm bash -c "while true; do systemctl daemon-reload; done"` puis `pkill -9 screen && screen -wipe` à la fin

On peut ensuite activer différents modules via l'interface web d'Aegir : options pour Git, Let's Encrypt, etc.

Si l'on utilise pas "Composer" pour déployer, nous conseillons de désactiver le module "Aegir Platform Composer" (sans cela on constate que le code Drupal n'était pas déployé sur les infra multi-serveurs).

### Installation sous Debian 12

Il est intéressant d'installer Aegir sous Debian 12 afin d'avoir un système qui soit [supporté au moins jusqu'en 2026](https://docs.evolix.org/ServeurDedie/versionsOS#debian-12-bookworm).

La procédure d'installation est similaire à ce qui est expliqué au-dessus, sauf que l'installation va échouer ainsi :

~~~
Enabling hosting-queued daemon
Could not find the alias @hostmaster [error]
~~~

En effet, Drush 8.4.8 ne fonctionne pas bien avec PHP 8.2, il faut donc [installer Drush 8.4.12](#installation-drush-8.4.12) et relancer :

~~~
# apt install aegir3
~~~

puis appliquer [les patchs pour PHP 8](#patches-pour-php-8) et [les patchs pour Drupal 10](#patches-pour-drupal-10).

De façon expérimentale, vous pouvez récupérez les patchs complets sur notre dépôt [aegir-patchs](https://gitea.evolix.org/evolix/aegir-patchs.git) :

~~~
# su - aegir
$ git clone https://gitea.evolix.org/evolix/aegir-patchs.git

$ cd /var/aegir
$ patch -p1 < /var/aegir/aegir-patchs/big-patchs/var_aegir.patch
$ chmod g+r -R .config/composer/vendor/drush/drush/lib/Drush/
$ patch /var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting/alias/hosting_alias.module /var/aegir/aegir-patchs/hostmaster/hosting_alias.module.patch 
$ cd hostmaster-7.x-3.192+nmu1/sites/HOSTMASTER_FQDN
$ patch settings.php < /var/aegir/aegir-patchs/provision/3374479-2.patch
File settings.php is read-only; trying to patch anyway
patching file settings.php
Hunk #1 succeeded at 45 (offset -14 lines).
File settings.php is read-only; trying to patch anyway
patching file settings.php
Reversed (or previously applied) patch detected!  Assume -R? [n]

# cd /
# patch -p0 < /var/aegir/aegir-patchs/big-patchs/usr_share_drush.patch
# chmod g+r,o+r -R /usr/share/drush/
~~~

### Passer en PHP 8.2 pour Drupal 9.5 / 10

Pour la compatibilité avec Drupal 9.5 / 10, nous passons PHP 8.2 via Sury (PHP 8.1 fonctionne également) :

~~~
# mount -o remount,rw /usr
# curl -sSLo /tmp/debsuryorg-archive-keyring.deb https://packages.sury.org/debsuryorg-archive-keyring.deb
# apt install /tmp/debsuryorg-archive-keyring.deb
# echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/sury.list
# apt update

# apt install php8.2 php8.2-cli php8.2-mysql php8.2-gd php8.2-xml php8.2-mbstring

# php -v
PHP 8.2.16 (cli) (built: Feb 16 2024 15:53:11) (NTS)

# a2dismod php php7.3 php7.4
# a2enmod php8.2
# systemctl restart apache2
~~~

À ce stade, Aegir fonctionne presque bien en PHP 8, il faut appliquer quelques patches.

### Patches pour PHP 8

Commençons par appliquer le patch de l'[issue Provision 3374479](https://www.drupal.org/project/provision/issues/3374479) pour éviter l'affichage d'erreurs *Deprecated* :

~~~
# wget https://www.drupal.org/files/issues/2023-08-14/3374479-2.patch
# cd /usr/share/drush/commands/provision/Provision/Config/Drupal
# patch -p4 < /root/3374479-2.patch
~~~

Il faut aussi appliquer « partiellement » le patch sur le fichier `settings.php` (remplacer `aegir.example.com` par votre FQDN choisi pour l'admin AEGIR) :

~~~
# patch /var/aegir/hostmaster-7.x-3.192+nmu1/sites/aegir.example.com/settings.php < 3374479-2.patch
~~~

Puis un petit patch simple qui permet de réparer la page « Create Platform » :

~~~
# cat field.form.inc.patch
--- /var/aegir/hostmaster-7.x-3.192+nmu1/modules/field/field.form.inc   2023-12-06 15:25:06.000000000 +0100
+++ field.form.inc      2024-02-20 12:11:12.437834696 +0100
@@ -74,7 +74,7 @@
     $field_state = array(
       'field' => $field,
       'instance' => $instance,
-      'items_count' => count($items),
+      'items_count' => count((array) $items),
       'array_parents' => array(),
       'errors' => array(),
     );

# patch /var/aegir/hostmaster-7.x-3.192+nmu1/modules/field/field.form.inc < field.form.inc.patch
~~~

Un petit patch pour gérer l'authentification HTTP :

~~~
# cat http_basic_auth.drush.inc.patch
49c49,50
<     $pass = crypt($pass);
---
>     $salt = substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()))), 0, 22);
>     $pass = crypt($pass,$salt);

# patch /var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting_tasks_extra/http_basic_auth/drush/http_basic_auth.drush.inc < http_basic_auth.drush.inc.patch
~~~

Un petit patch pour éviter une erreur :

~~~
# cat hosting_alias.module.patch
--- hosting_alias.module        2024-07-08 14:52:51.094914293 +0200
+++ hosting_alias.module.new    2024-07-08 14:52:31.758272416 +0200
@@ -412,7 +412,7 @@
 function hosting_alias_node_view($node, $view_mode, $langcode) {
   if ($node->type == 'site') {
 
-    if (count($node->aliases)) {
+    if (count((array) $node->aliases)) {
       foreach ($node->aliases as $link) {
         $links[] = l($link, "http://$link");
       }
@@ -432,7 +432,7 @@
     }
 
     // List the automatic aliasses.
-    if (count($node->aliases_automatic)) {
+    if (count((array) $node->aliases_automatic)) {
       $links = array();
       foreach ($node->aliases_automatic as $link) {
         $links[] = l($link, "http://$link");

# patch /var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting/alias/hosting_alias.module < hosting_alias.module.patch
~~~

Enfin, il faut désactiver la vérification qu'une plateforme n'a pas le même chemin car cela échoue en PHP 8 :

~~~
# cat hosting_platform.module.patch
--- /var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting/platform/hosting_platform.module.old 2024-02-20 13:15:41.490390915 +0100
+++ /var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting/platform/hosting_platform.module     2024-02-20 13:18:07.850720426 +0100
@@ -647,13 +647,13 @@
     }
 
     // Make sure the path is unique. Remote servers can't have the same path to a platform that is in use by another server.
-    $exists = hosting_platform_path_exists($node->publish_path);
-    if ($exists) {
-      form_set_error('publish_path',
-        t('Path is already in use by platform %name. Platform paths must be unique across all servers.',
-        array('%name' => $result->name))
-      );
-    }
+    //$exists = hosting_platform_path_exists($node->publish_path);
+    //if ($exists) {
+    //  form_set_error('publish_path',
+    //    t('Path is already in use by platform %name. Platform paths must be unique across all servers.',
+    //    array('%name' => $result->name))
+    //  );
+    //}
 
     if (is_null($node->web_server)) {
       form_set_error('web_server', t('Platform needs to be associated with a webserver. Make sure you have a verified webserver on this Aegir install!'));

# patch /var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting/platform/hosting_platform.module < hosting_platform.module.patch
~~~

Sinon au moment de la création d'une nouvelle plateforme vous obtiendrez :

~~~
'PDOException', '!message' => 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens: SELECT n.title AS name FROM {hosting_platform} AS h INNER JOIN {node} AS n ON n.nid = h.nid WHERE publish_path = !publish_path AND h.status >= !h.status; Array ( [!publish_path] => /var/aegir/platforms/PLATFORM [!h.status] => 0 ) ', '%function' => 'hosting_platform_path_exists()', '%file' => '/var/aegir/hostmaster-7.x-3.192+nmu1/profiles/hostmaster/modules/aegir/hosting/platform/hosting_platform.module', '%line' => 669, 'severity_level' => 3, ) 
~~~

> TODO : un patch plus gros est fourni par BOA, à voir l'intérêt

### Installation Drush 8.4.12

On va installer composer à la main pour installer Drush 8.4.12 et quelques modules (dont provision) :

~~~
# mv /usr/local/bin/drush /usr/local/bin/drush.bak
# su - aegir

$ wget -O setup-composer.php https://getcomposer.org/installer
$ mkdir bin
$ php setup-composer.php --install-dir=./bin --filename=composer
All settings correct for using Composer
Downloading...

Composer (version 2.7.1) successfully installed to: /var/aegir/bin/composer
Use it: php ./bin/composer

$ rm setup-composer.php

$ ./bin/composer global require drush/drush:8.4.12

$ .config/composer/vendor/bin/drush.php version
 Drush Version   :  8.4.12 

# ln -s /var/aegir/.config/composer/vendor/bin/drush.php /usr/local/bin/drush
~~~

### Patches pour Drupal 10

Pour gérer du Drupal 10, il faut ajouter les fichiers suivants dans `/usr/share/drush/commands/provision/platform/drupal/` :

* clear_10.inc
* cron_key_10.inc
* deploy_10.inc
* import_10.inc
* install_10.inc
* packages_10.inc

> TODO: fournir la source de ces fichiers, voir notamment l'[issue Provision 3406925](https://www.drupal.org/project/provision/issues/3406925)

Il faut supprimer le Drush 12 inclus dans les sources :

~~~
$ rm platforms/DRUPAL10/vendor/bin/drush
$ rm -rf platforms/DRUPAL10/vendor/drush
~~~

Il faut aussi appliquer ce patch [drush-8-symfony-console-compat.patch](https://www.drupal.org/files/issues/2023-11-28/drush-8-symfony-console-compat.patch) et en partie ce patch [3353492-symfony-console-4-update_1.patch](https://www.drupal.org/files/issues/2023-12-11/3353492-symfony-console-4-update_1.patch) sur tous les fichiers Symfony (TODO: à relister) de l'[issue Provision 3353492](https://www.drupal.org/project/provision/issues/3353492) :

~~~
# patch /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/Command/DrushInputAdapter.php < drush-8-symfony-console-compat.patch
# cd /usr/share/drush/commands/provision/vendor/symfony/console
# patch -p1 < 3353492-symfony-console-4-update_1.patch
~~~

Attention, ce patch nécessite alors de patcher aussi le code de Drupal 9.5 pour qu'il reste « installable » :

~~~
# cat drush-8-symfony-console-compat-drupal95.patch
29c29
<     public function getFirstArgument();
---
>     public function getFirstArgument(): ?string;
44c44
<     public function hasParameterOption($values, $onlyParams = false);
---
>     public function hasParameterOption(string|array $values, bool $onlyParams = false): bool;
60c60
<     public function getParameterOption($values, $default = false, $onlyParams = false);
---
>     public function getParameterOption(string|array $values, string|bool|int|float|array|null $default = false, bool $onlyParams = false);
81c81,82
<     public function getArguments();
---
>     public function getArguments(): array;
> 
111c112,113
<     public function hasArgument($name);
---
>     public function hasArgument($name): bool;
> 
118c120,121
<     public function getOptions();
---
>     public function getOptions(): array;
> 
148c151,152
<     public function hasOption($name);
---
>     public function hasOption($name): bool;
> 
155c159,160
<     public function isInteractive();
---
>     public function isInteractive(): bool;
> 
# patch /var/aegir/platforms/PLATEFORM/vendor/symfony/console/Input/InputInterface.php < drush-8-symfony-console-compat-drupal95.patch
~~~

> Note : le patch sur `Input/InputInterface.php` nécessite un coup de main (TODO ;)

On crée le fichier `Sql10.php` :

~~~
$ cat /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/Sql/Sql10.php
<?php
namespace Drush\Sql;

use Drupal\Core\Database\Database;

class Sql10 extends Sql9 {
}
~~~

Sinon on aura des erreurs lors de l'installation d'un Drupal 10 :

~~~
Drush\Sql\SqlException: Unable to find a matching SQL Class. Drush cannot find your database connection details...
~~~

ET idem pour DrupalBoot10.php, StatusInfoDrupal10.php, User10.php et UserSingle10.php :

* /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/Boot/DrupalBoot10.php
* /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/UpdateService/StatusInfoDrupal10.php
* /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/User/User10.php
* /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/User/UserSingle10.php

> TODO: fichiers à fournir

Il faut aussi patcher dans le code de Drupal 10 :

1. supprimer "string|\Stringable " dans les fichiers suivants :

* vendor/psr/log/src/LoggerTrait.php (18 fois)
* core/lib/Drupal/Core/Logger/LoggerChannel.php (1 fois)
* core/lib/Drupal/Core/Logger/RfcLoggerTrait.php (9 fois)
* core/modules/dblog/src/Logger/DbLog.php (1 fois)

2. supprimer ": void" dans les fichiers suivants :

* core/lib/Drupal/Core/Logger/RfcLoggerTrait.php (9 fois)
* vendor/psr/log/src/LoggerInterface.php (9 fois)
* core/modules/dblog/src/Logger/DbLog.php (1 fois)

3. Supprimer ": void" uniquement dans "function log(...)" dans les fichiers suivants :

* vendor/psr/log/src/LoggerTrait.php
* core/lib/Drupal/Core/Logger/LoggerChannel.php

Enfin, il faut appliquer le patch suivant sur le fichier 
`/var/aegir/.config/composer/vendor/drush/drush/lib/Drush/Boot/DrupalBoot.php` :

~~~
# cat DrupalBoot.php.patch
@@ -157,7 +157,12 @@
           $ignored_modules[] = $cached->data;
         }
         foreach (array_diff(drush_module_list(), $ignored_modules) as $module) {
-          $filepath = drupal_get_path('module', $module);
+          if (drush_drupal_major_version() <= 9) {
+            $filepath = drupal_get_path('module', $module);
+          }
+          else {
+            $filepath = \Drupal::service('extension.list.module')->getPath($module);
+          }
           if ($filepath && $filepath != '/') {
             $searchpath[] = $filepath;
           }
@@ -165,7 +170,12 @@
 
         // Check all enabled themes including non-default and non-admin.
         foreach (drush_theme_list() as $key => $value) {
-          $searchpath[] = drupal_get_path('theme', $key);
+          if (drush_drupal_major_version() <= 9) {
+            $searchpath[] = drupal_get_path('theme', $key);
+          }
+          else {
+            $searchpath[] = \Drupal::service('extension.list.theme')->getPath($key);
+          }
         }
         break;
     }

# patch /var/aegir/.config/composer/vendor/drush/drush/lib/Drush/Boot/DrupalBoot.php < DrupalBoot.php.patch
~~~


## Utilisation

Le principe d'Aegir est de définir des plateformes associés à un serveur web (ou plusieurs, cf plus bas le [mode multi-serveurs](HowtoAegir#multi-serveurs)).
Un usage typique est de définir une plateforme de "PROD" et une plateforme de "PREPROD" associés à des serveurs web différents.
Une plateforme déploie le code Drupal sur le serveur associé, cela peut par exemple être un Drupal 8.8.5 ou en Drupal 9.4.5.
On peut ensuite créer des sites associé à une plateforme, cela s'appuye sur la fonctionnalité [multisites](https://www.drupal.org/docs/multisite-drupal) de Drupal.

### Servers

Par défaut il y a le serveur web local (type `Apache`) et un serveur de base de données local (type `MySQL`).

### Platforms

Ajouter une plateforme est l'étape la plus critique, car c'est là qu'Aegir va déployer le code source de Drupal.
Plusieurs [stratégies de déploiement](https://docs.aegirproject.org/usage/advanced/deployment/) sont possibles : récupérer le code de Drupal via Git, déploiement via Drush, etc.
Attention, à cette étape Aegir va scanner la présence d'un fichier `composer.lock` et lancera un `composer install` si il est présent… et ne se déploiera pas en cas d'erreur !

### Sites

Il faut définir un nom de domaine et l'associer à une plateforme.
On peut aussi : choisir un language pour le site, un profil (minimal, standard, personnalisé, etc.) et serveur de base de données.


## Plomberie

L'installation initiale d'Aegir se fait via un `drush hostmaster-install` du type :

~~~
# su - aegir

$ drush hostmaster-install --http_service_type='apache' --backend aegir.example.com
~~~

Lorsque que l'on définit des serveurs, plateformes, sites, cela va créer des fichiers de contexte stockés dans `/var/aegir/.drush/` :

~~~
# ls /var/aegir/.drush/

hm.alias.drushrc.php
hostmaster.alias.drushrc.php
server_localhost.alias.drushrc.php
server_master.alias.drushrc.php
platform_hostmaster.alias.drushrc.php
platform_PROD.alias.drushrc.php
platform_PREPROD.alias.drushrc.php
FQDN1.alias.drushrc.php
FQDN2.alias.drushrc.php
FQDN3.alias.drushrc.php
...
~~~

Chaque action dans Aegir va provoquer des « commandes Drush » cf <https://docs.aegirproject.org/develop/provision/>

Par exemple le `Run` d'une plateforme va notamment provoquer :

~~~
$ drush --backend=2 --root=/var/aegir/platforms/PREPROD --uri= provision-save '@platform_PREPROD' 2>&1
$ drush --backend=2 @platform_PREPROD provision-verify 2>&1
~~~


Cela va également écrire dans la base de données MySQL propre à Aegir.

Par exemple quand on crée un site, il est présent au moins dans les tables `hosting_site` et `hosting_context`.

Par exemple pour lister les serveurs :

~~~
MariaDB> select title,service,hosting_service.type,restart_cmd,port,uid,hosting_server.status,created,changed from hosting_server,hosting_service,node where hosting_server.vid=hosting_service.vid and hosting_server.nid=node.nid;
+-------------+---------+---------+------------------------------------+------+-----+--------+------------+------------+
| title       | service | type    | restart_cmd                        | port | uid | status | created    | changed    |
+-------------+---------+---------+------------------------------------+------+-----+--------+------------+------------+
| foo1        | http    | apache  | sudo /usr/sbin/apache2ctl graceful |   80 |   1 |      1 | 1663071992 | 1663072005 |
| foo2        | http    | apache  | sudo /usr/sbin/apache2ctl graceful |   80 |   1 |      1 | 1663175297 | 1663175299 |
| foo3        | http    | apache  | sudo /usr/sbin/apache2ctl graceful |   80 |   1 |      1 | 1663175326 | 1663175327 |
| 127.0.0.1   | db      | mysql   |                                    | 3306 |   1 |      1 | 1663071992 | 1663230576 |
| webcluster  | http    | cluster |                                    |    0 |   1 |      1 | 1663175362 | 1663230664 |
~~~


Au sein d'un site Drupal, on peut collecter différentes informations via [Drush](HowtoDrupal#drush) :

~~~
$ cd $ROOT
$ drush sql-connect
$ drush status
~~~

## Ligne de commande

Grâce aux fichiers de configuration stockés dans `/var/aegir/.drush/` on peut ainsi lancer plein de commandes Drush du type :

~~~
# su - aegir

$ drush @hostmaster provision-verify
$ drush @platform_PREPROD provision-verify --debug
$ drush @platform_PREPROD provision-delete --debug --force
$ drush @FQDN1 --backend=2 provision-install
$ drush @FQDN1 provision-verify --debug
~~~


## Gestion des Tasks

Aegir lance des Tasks pour toutes les actions : installation, suppression, vérification, etc.

Pour cela, Aegir s'appuie sur un service `hosting-queued` :

~~~
# systemctl status hosting-queued.service
● hosting-queued.service - Hosting queue daemon
   Loaded: loaded (/lib/systemd/system/hosting-queued.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2022-09-13 14:28:45 CEST; 5s ago
 Main PID: 1704 (php)
    Tasks: 3 (limit: 4701)
   Memory: 67.6M
   CGroup: /system.slice/hosting-queued.service
           ├─1704 php /usr/local/bin/drush --quiet @hostmaster hosting-queued
           ├─1815 sh -c /usr/local/bin/drush  --strict= --uri=aegir.example.com --root=/var/aegir/hostmaster-7.x-3.192  hosting-task 13 2>&1
           └─1816 php /usr/local/bin/drush --strict= --uri=aegir.example.com --root=/var/aegir/hostmaster-7.x-3.192 hosting-task 13
~~~

et un cron lancé chaque minute :

~~~
# crontab -u aegir -l
* * * * * /usr/bin/env php /usr/local/bin/drush '@hostmaster' hosting-dispatch 
~~~

qui lancera ensuite des commandes du type `drush --php=/usr/bin/php --php-options= --quiet @hostmaster hosting-queued`

En cas de soucis, blocage des Tasks par exemple, on peut déboguer la tâche en récupérant son n° (visible sur l'interface web) :

~~~
# su - aegir
$ drush @hostmaster hosting-task 42 --debug
~~~

Parfois, lancer une vérification en ligne de commande peut débloquer :

~~~
# su - aegir
$ drush @FQDN1 provision-verify --debug
~~~

On peut aussi modifier le Cron et ajouter l'option `--debug`.

Ou alors vider le cache Drush :

~~~
# su - aegir
$ drush cache-clear drush
~~~

Par l'interface web on peut accéder à la liste des Tasks via <http://aegir.example.com/admin/content/node/overview> et supprimer une Task (à utiliser avec précaution).

Enfin, si les tâches ne se lancent plus, il faut sûrement relancer le service :

~~~
# systemctl status hosting-queued.service
# systemctl start hosting-queued.service
~~~

Pour d'autres infos, voir <https://community.aegirproject.org/content/administrator/troubleshooting-aegir/>

## Multi-serveurs

<https://docs.aegirproject.org/usage/servers/clustering/>

Nous préconisons d'utiliser la méthode "Web Cluster".

Il faut activer le module Aegir "Web Cluster" :

~~~
Enabling Web clusters feature.
The configuration options have been saved.
~~~

Sur les « noeuds » web :

Pré-requis :

* Debian 11 avec PHP 7.4 (par défaut sous Debian 11)
* WIP : Debian 11 avec PHP 8.2 via Sury ou Debian 12
* créer un utilisateur "aegir" avec les mêmes UID/GID que sur le serveur Aegir, par exemple `addgroup --gid 513 aegir && adduser --home /var/aegir --uid 513 --gid 513 --disabled-password aegir`

Puis il faut installer le paquet `aegir3-cluster-slave` :
 
~~~
# echo "deb [signed-by=/usr/share/keyrings/pub_evolix.asc] http://pub.evolix.org/evolix aegir main" > /etc/apt/sources.list.d/aegir.list

## la clef est fournie par le paquet evolix-archive-keyring
# wget https://pub.evolix.org/evolix/pool/main/e/evolix-archive-keyring/evolix-archive-keyring_0~2024_all.deb
# apt install ./evolix-archive-keyring_0~2024_all.deb

# apt update && apt install aegir3-cluster-slave
# chown aegir: /var/aegir/platforms
~~~

TODO : en Debian 11, problème avec php-mysql qui installe une version trop récente

Note : en Debian 11/12, cela installe un méta-paquet mysql-client_8.0_all.deb fourni par Evolix :

~~~
# wget https://pub.evolix.org/evolix/pool/main/m/mysql-client/mysql-client_8.0_all.deb
# apt install ./mysql-client_8.0_all.deb
~~~

L'installation du paquet `aegir3-cluster-slave` va :

* valider la connexion SSH entre l'utilisateur "aegir" depuis le serveur Aegir vers l'utilisateur "aegir" du noeud (avec une clé SSH sans mot de passe)
* /etc/sudoers.d/aegir
* Installer Apache / PHP
* (ignorer la demande pour NFS)

On peut ensuite ajouter des "serveurs" pour chacun des noeuds, puis ajouter un serveur "Web cluster" qui rassemble les noeuds.
Il faut ensuite ajouter une plateforme associée à ce noeud. Puis l'on devrait pouvoir créer des sites sur cette plateforme « clusterisée ».


## FAQ

### Troubleshooting

En cas de soucis, on peut tester différentes astuces.

Vider le cache Drush :

~~~
# su - aegir
$ drush cache-clear drush
~~~

Vérifier que les configurations dans `/var/aegir/.drush/` sont conformes à ce que l'on a mis via l'interface web (cela semble parfois boguer).

Lancer des commandes à la main, cf [Ligne de commande](HowtoAegir#ligne-de-commande)

Voir <https://community.aegirproject.org/content/administrator/troubleshooting-aegir/>


### Skipping unset context property: git_repository_path

Cela signifie qu'il manque cette option dans le fichier associé `/var/aegir/.drush/XXX.drushrc.php`.
Un contournement est d'ajouter cette option à la main :

~~~
./platform_PREPROD42.alias.drushrc.php:  'git_repository_path' => NULL,
./platform_PREPROD43.alias.drushrc.php:  'git_repository_path' => '/var/aegir/platforms/platform_PREPROD43',
~~~

Si cette option est manquante, vous aurez peut-être l'erreur :

~~~
Cloning `GIT_REPO:REPO.git` to ``
Running `git clone --recursive --depth 1 --no-progress --quiet --branch 'dev' 'GIT_REPO:REPO.git' ''`
Command failed. The specific errors follow:
fatal: could not create work tree dir '': No such file or directory
~~~

### Données non présentes dans `/var/aegir/plateforms/PREPROD/`

Vérifier que les chemins sont corrects dans le fichier associé `/var/aegir/.drush/XXX.drushrc.php`

### Erreurs composer

Supprimer les fichiers `composer.lock` et `composer.json` dans `/var/aegir/plateforms/PREPROD/`

### Erreur "Unable to find a matching SQL Class"

Si vous avez une erreur du type :

~~~
Drush\Sql\SqlException: Unable to find a matching SQL Class. Drush cannot find your database connection details.
~~~~

Soit vous n'êtes pas dans le bon répertoire pour lancer Drush (on peut aussi utiliser l'option `--root=`),
soit vous devez utiliser une version de Drush plus récente : on a par exemple rencontré cette erreur
avec Drush 8.1.16 et c'était OK avec Drush 8.4.8

### PROVISION_DB_CONNECT_FAIL : Dummy connection failed to fail...Can't connect to MySQL server...(115)

Si vous utilisez un proxy pour accéder à MySQL, vous pouvez avoir une erreur de ce type lors de l'installation d'un site :

~~~
PROVISION_DB_CONNECT_FAIL : Dummy connection failed to fail. Either your MySQL permissions are too lax, or the response was not understood. See http://is.gd/Y6i4FO for more information. ERROR 2002 (HY000): Can't connect to MySQL server on 'proxy-mysql' (115)
~~~

Il s'agit d'un code de retour système 115 `Operation now in progress` (cf commande `perror 115`) qui devrait être ignoré par Aegir.

Nous [proposons le patch](https://github.com/aegir-project/provision/issues/41) suivant à intégrer au fichier `/usr/share/drush/commands/provision/db/Provision/Service/db/mysql.php` :

~~~
146a147,149
>     elseif (preg_match("/Can't connect to MySQL server on.*\(115\)/", $output, $match)) {
>       return $match[1];
>     }
~~~

### Erreur si présence d'un "/" dans une branche Git

Si vous utilisez une branche Git pour récupérer le code de votre Plateforme, Aegir plante si il y un `/` dans le nom de la branche, par exemple `foo/bar`.
Solution : renommez votre branche… ou faites à la main :

~~~
$ cd platforms
$ git clone --recursive --depth 1 --branch 'foo/bar' 'john@git.example.com:PROJECT/DRUPAL.git' PLATFORM/
~~~

### ERROR 1133 (28000) at line 1: Can't find any matching row in the user table

Lors de l'installation d'Aegir, si vous obtenez cette erreur, c'est probablement que l'utilisateur SQL `aegir_root` est mal désinstallé :

~~~
mysql> DELETE FROM mysql.user where User='aegir_root';
mysql> FLUSH PRIVILEGES;
~~~

### Could not find the alias @hostmaster

Cela veut dire que l'installation du *Drupal hostmaster* n'a pas été au bout, et notamment que le fichier `/var/aegir/.drush/hostmaster.alias.drushrc.php` n'existe pas.

Pour comprendre où l'installation bloque, relancer l'installation en ligne de commande en mode *debug* :

~~~
# su - aegir

$ drush hostmaster-install --http_service_type='apache' --backend aegir.example.com --debug
~~~

### Installation Aegir via BOA

[BOA (Barracuda, Octopus and Aegir)](https://github.com/omega8cc/boa) est un système d'installation et de gestion de sites Drupal.
Il inclut notamment Aegir, et il est intéressant de voir les particularités de son installation.

BOA installe les modules Drush suivants :

* clean_missing_modules
* drupalgeddon
* drush_ecl
* registry_rebuild
* safe_cache_form_clear
* utf8mb4_convert
* provision_boost 
* security_review

BOA fournit les fichiers suivants pour Drupal 10 que l'on peut mettre dans `/usr/share/drush/commands/core/dupal/` :

* batch_10.inc
* cache_10.inc
* environment_10.inc
* site_install_10.inc
* update_10.inc

### Drush 12 pour Drupal 10

Cette [issue du projet drush-ops](https://github.com/drush-ops/drush/issues/5741) semble indiquer qu'il est possible de faire tourner Drush 8 et Drush 12 en même temps, même si cela ne semble pas nécessaire.

### Désactiver les warnings Composer

Si vous n'utilisez pas du tout Composer à l'installation d'un site sur le serveur, vous éviterez un warning en créant le fichier `/var/aegir/.drush/local.drushrc.php` ainsi :

~~~
<?php

$options['hosting_features'] = array (
'provision_composer_install_platforms' => '0',
);
$options['provision_composer_install_platforms'] = '0';
~~~

### Migration

Pour migrer d'un Aegir à un autre (version éventuellement inférieure vers `hostmaster-7.x-3.192+nmu1`), voici les étapes :

1. Installer un Aegir de zéro avec les patches nécessaires.

Nous conseillons fortement de garder la même URL :

~~~
$ grep uri /var/aegir/.drush/hostmaster.alias.drushrc.php 
  'uri' => 'aegir.example.com',
~~~

2. Tester le bon fonctionnement d'Aegir sans réinjecter des données

3. Vous devez créer le répertoire `/var/log/aegir/` :

~~~
# mkdir /var/log/aegir/
# chmod 750 /var/log/aegir/
# chgrp aegir /var/log/aegir/
~~~

3. Depuis le nouveau serveur, on réinjecte la base de données :

~~~
# mysqldump aegirexamplecom > aegirexamplecom.ori.sql
# ssh OLDSERVEUR mysqldump aegirexamplecom > aegirexamplecom.new.sql
# mysql aegirexamplecom < aegirexamplecom.new.sql
~~~

Nous déconseillons de le faire, mais si vous avez changé l'URL :

* avant d'injecter le DUMP, faire un chercher/remplacer général : `:%s/aegir.example.com/aegir-new.example.com/g`
* une fois les données réinjectées, vous devrez ajuster une ligne de la table `variable` (la valeur "28" est à ajuster en fonction du remplacement) :

~~~
mysql> update variable set value='s:28:"http://aegir-new.example.com";' where name='install_url';
~~~

4. On réinjecte une partie des données depuis l'ancien serveur :

~~~
# rsync -av --exclude cache --exclude cache.bak --exclude drushrc.php --exclude hm.alias.drushrc.php --exclude hostmaster.alias.drushrc.php --exclude platform_hostmaster.alias.drushrc.php --exclude server_localhost.alias.drushrc.php --exclude server_master.alias.drushrc.php OLDSERVEUR:/var/aegir/.drush/ /var/aegir/.drush/
# rsync -av OLDSERVEUR:/var/aegir/platforms/ /var/aegir/platforms/
# rsync -av OLDSERVEUR:/var/aegir/config/ /var/aegir/config/
# rsync -av OLDSERVEUR:/var/aegir/.ssh/ /var/aegir/.ssh/
~~~

5. Ajustements

Vous devrez peut-être ajouter une colonne à la table "users" :

~~~
mysql aegirexamplecom

mysql> ALTER TABLE users ADD changed int(11) NOT NULL DEFAULT 0 COMMENT 'Timestamp for when user was changed.' AFTER created;
~~~

Et supprimer ce fichier si il existait avant :

~~~
# rm /var/aegir/config/server_master/apache/platform.d/platform_hostmaster7x3192.conf
~~~

Vous devrez peut-être créer un lien symbolique :

~~~
# ln -s /var/aegir/hostmaster-7.x-3.192+nmu1 /var/aegir/hostmaster-7.x-3.192
~~~

Vous devrez peut-être ajuster le `/var/aegir/.drush/drushrc.php` notamment pour `$options['include']`.

Si vous être en mode muti-serveurs, vous devrez peut-être ajuster les droits, notamment si vous avez des nouveaux serveurs web.

6. Si vous avez besoin de migrer les anciennes bases, vous pouvez le faire ainsi à vos propres risques :

~~~
$ cat migr.sh

#!/bin/bash
echo $1
echo "CREATE DATABASE $1" | mysql
ssh OLDSERVEUR mysqldump $1 > /tmp/$1.sql
mysql $1 < /tmp/$1.sql 
ssh OLDSERVEUR pt-show-grants | grep $1 | mysql

# for i in $(cat all.txt); do bash migr.sh $i; done
~~~


