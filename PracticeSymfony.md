# Practice Symfony

* Documentation : <https://symfony.com/doc/current/index.html>
* Rôle : <https://forge.evolix.org/projects/ansible-roles/repository/show/php> avec `php_symfony_requirements: True`

[Symfony](http://symfony.com/) est le framework PHP le plus populaire.

## Prérequis

<http://symfony.com/doc/current/reference/requirements.html>

* PHP 7.1.3 ou supérieur

### Apache

<http://symfony.com/doc/current/setup/web_server_configuration.html>

Les applications Symfony contiennent souvent des fichiers `.htaccess` avec des directives `DirectoryIndex`, `Options -MultiViews`, `Rewrite*`, `Require`.
Il est donc plutôt conseillé d'utiliser [Apache](HowtoApache) en général.

La configuration Apache du VirtualHost doit donc contenir au minimum :

~~~{.apache}
AllowOverride Limit FileInfo Indexes Options=All,MultiViews
~~~

### PHP

Le module JSON doit être installé :

~~~
# apt install php7.0-json
~~~

> *Note* : Pour Debian 8
>
> ~~~
> # aptitude install php5-json
> ~~~

Et il faut préciser le paramètre `date.timezone` dans les fichiers de configuration PHP.
Par exemple pour la France :

~~~
# grep -r date.timezone /etc/php/7.0/*/conf.d/zzz-evolinux-custom.ini

/etc/php/7.0/apache2/conf.d/zzz-evolinux-custom.ini: date.timezone = "Europe/Paris"
/etc/php/7.0/cli/conf.d/zzz-evolinux-custom.ini: date.timezone = "Europe/Paris"
/etc/php/7.0/fpm/conf.d/zzz-evolinux-custom.ini: date.timezone = "Europe/Paris"
~~~

Il faut aussi s'assurer que des fonctions comme `putenv` ne sont pas bloquées


## Installation

La méthode conseillée est d'utiliser [Composer](HowtoPHP#composer).
Par exemple pour installer Symfony 3.3 :

~~~
$ echo '{"require": {{"symfony/framework-standard-edition": "~3.3"}}' > composer.json
$ composer update
Loading composer repositories with package information
[...]

$ cd vendor/symfony/framework-standard-edition

$ composer install
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
  - Installing doctrine/lexer (v1.0.1)
    Loading from cache
[...]
~~~

Pour tester la configuration, des tests peuvent être lancés ainsi :

~~~
$ cd vendor/symfony/framework-standard-edition
$ php bin/symfony_requirements

Symfony Requirements Checker

> PHP is using the following php.ini file:
  /etc/php/7.0/cli/php.ini

> Checking Symfony requirements:
  ................................W........
                                         
 [OK]                                         
 Your system is ready to run Symfony projects 
[...]
~~~

## Configuration

<https://symfony.com/doc/current/configuration.html>

## Application Symfony

Pour avoir une application de test, on peut utiliser l'application de nos amis d'[Acseo](http://www.acseo.fr/) disponible sur <https://github.com/acseo/symfony-perf> :

On applique le <https://github.com/acseo/symfony-perf/blob/master/INSTALL.md> :

~~~
$ git clone https://github.com/acseo/symfony-perf.git
$ cd symfony-perf
$ composer install
$ chmod -R g+w var
~~~

## Erreur opendir /var/lib/php/sessions failed Permission denied

Voir [HowtoPHP#erreur-opendir-varlibphpsessions-failed-permission-denied]()

## Gestion des permissions

<https://symfony.com/doc/current/setup/file_permissions.html>

## Logging

<https://symfony.com/doc/current/logging.html>

<https://symfony.com/blog/logging-in-symfony-and-the-cloud>
