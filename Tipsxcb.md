
# Installation

~~~
# apt install xcb
~~~

«Pigeon holes for your cut and paste selections»
Permet de gérer un nombre voulu de buffers pour la copie et le collage de texte.

# Utilisation

Par défaut :
La case avec le texte en fond noir et le texte en blanc est celui sélectionné. Pour changer la case, il faut simplement cliquer sur une autre.

~~~{.bash}
<fleches_directionnelles> : "se déplacer"
q : "quitter"
~~~

# CLI *(4ever)*

~~~{.bash}
xsel : "Coller le buffer sélectionné"
~~~