---
title: Glossaire
...

# Glossaire

## ACL

ACL = [Access Control List](https://fr.wikipedia.org/wiki/Access_Control_List) (Liste de contrôle d'accès)

## DNS

DNS = [Domain Name System](https://fr.wikipedia.org/wiki/Domain_Name_System) (Système de Noms de Domaine)

## TSIG

TSIG (transaction signature ou signature de transaction) est un protocole réseau qui utilise un secret partagé entre plusieurs hôtes et une fonction de hachage unidirectionnelle pour permettre la mise à jour de zone [DNS](#dns). [RFC2845](https://datatracker.ietf.org/doc/html/rfc2845)

## HTTP

HTTP = [Hypertext Transfer Protocol](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol)

Toutes les pages à propos de [HTTP](/_search?patterns=HTTP)

## ISO

Dans un contexte de stockage, il s'agit d'[images disques](https://fr.wikipedia.org/wiki/Image_disque).

## LIR

LIR = [Local Internet Registry](https://fr.wikipedia.org/wiki/Local_Internet_registry) (Registre Internet local)

Toutes les pages à propos de [LIR](/_search?patterns=LIR)

## PKI

PKI = [Public Key Infrastructure](https://fr.wikipedia.org/wiki/Infrastructure_à_clés_publiques) (Infrastructure à clés publiques)

Toutes les pages à propos de [PKI](/_search?patterns=VPN)

## SMTP

SMTP = [Simple Mail Transfer Protocol](https://fr.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol)

## SSH

SSH = [Secure Shell](https://fr.wikipedia.org/wiki/Secure_Shell)

## TLS

TLS = [Transport Layer Security](https://fr.wikipedia.org/wiki/Transport_Layer_Security)

Toutes les pages à propos de [TLS](/_search?patterns=VPN)

## URL

URL = [Uniform Resource Locator](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator)

## VoIP

VoIP = [Voice over IP](https://fr.wikipedia.org/wiki/Voix_sur_IP) (Voix sur IP)

## VPN

VPN = [Virtual Private Network](https://fr.wikipedia.org/wiki/Réseau_privé_virtuel) (Réseau privé virtuel)

Toutes les pages à propos de [VPN](/_search?patterns=VPN)

## AES

## ANSI

## BGP

## BSD

## BTRFS

## C

## C++

## CLI

## CPU

## CRL

## CSS

## DHCP

## DisplayPort

## DKIM

## DMARC

## DPkg

## DSA

## Elixir

## EOF

## Erlang

## Ethernet

## Firewall

## FTP

## FTPS

## GPG

## GPT

## GRsec

## GZip

## Haskell

## HDMI

## Hostname

## HTML

## IMAP

## IRC

## Java

## Javascript

## JSON

## Kernel

## LDAP

## Linux

## LLVM

## LVM

## NAS

## netfow/sflow

## NTP

## OCSP

## ODBC/JDBC

Java Database Connectivity est une API utilisée dans Java pour communiquer avec des bases de données. Il faut spécifier une URL jdbc pour se connecter à une base.  

## OSPF

## PDF

## PHP

## Ping

## POP

## PTR

## PXE

## Python

## RAID

## RDBMS

## RIPE

## RPM

## RSA

## Ruby

## SAN

## SCP

## SCSI

## SFTP

## SIP

## SMART

## SNMP

## SOAP

## Socket

## Spanning-Tree

## SPF

## SQL

## SSD

## sudo/doas

## SVN/Subversion

## Systemd

## Stateless

Qualifie une application ou process qui ne stocke pas de données de manière persistante et ne fait pas référence à des transactions passées. La configuration d'une application stateless se fait en lui donnant des options à son démarrage.

## TAR

## TCP/UDP

## TFTP

## USB

## UTC

## UUID

## VCS

## VGA

## VLAN

## VRRP

## Whois

## XML

## ZFS
