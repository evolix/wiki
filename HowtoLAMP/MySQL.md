**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Pour installer, configurer et administrer MySQL, voir [wiki:HowtoMySQL]

## phpMyAdmin

~~~
# aptitude install phpmyadmin
~~~

Il est conseillé de ne pas rendre accessible _phpMyAdmin_ via une URL "devinable"
et/ou l'autoriser uniquement depuis certaines adresses IP.

L'idée est ainsi d'ajouter au Virtualhost _default_ :

~~~
Alias /phpmyadmin-SwasHyk4 /usr/share/phpmyadmin/
~~~

## Interdire l'écriture sur un slave

<http://dev.mysql.com/doc/refman/5.0/en/replication-options-slave.html#option_mysqld_read-only>

--read-only

Cause the slave to permit no updates except from slave threads or from users having the SUPER privilege. On a slave server, this can be useful to ensure that the slave accepts updates only from its master server and not from clients. This variable does not apply to TEMPORARY tables. 

## Diagnostic des erreurs

Errno 24 : <http://blog.tmartin.fr/MySQL_errno_24___augmenter_le_nombre_de_descripteurs_de_fichiers.html>

