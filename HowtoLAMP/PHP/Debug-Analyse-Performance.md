**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowTo Debug PHP

Diverses astuces pour débuger / identifier / analyser des soucis de performances.

## strace

Au niveau système, pour voir exactement ce qui est fait. (Ouverture de fichiers, connexion distante, requêtes MySQL).
Exemple d'utilisations.


~~~
# Affiche les fichiers ouverts.
$ strace -ff -e trace=open php index.php
# Affiche les appels réseaux.
$ strace -ff -e trace=network php index.php
# Compte les appels systèmes et le temps passé.
$ strace -c php index.php
# Calcul du temps d'exécution d'un appel.
$ strace -T php index.php
# Affichage d'un timestamp devant chaque appel.
$ strace -ttt php index.php
# Strace verbeux.
$ strace -s 65535 -ff php index.php
~~~

Dans certains cas, des CMS gèrent mal le fait qu'on exécute le code via PHP-CLI. On pourra utiliser PHP-CGI pour le tromper, c'est notamment le cas de Magento.

~~~
$ HTTP_HOST="www.monsite.com" strace php-cgi index.php 
~~~

Par exemple, peut récupérer les requêtes MySQL ainsi.

~~~
$ strace -s65535 -e trace=write -ff -o strace php-cgi index.php
$ grep SELECT strace.*
~~~


## xdebug

Voir : <http://trac.evolix.net/infogerance/wiki/HowtoLAMP/PHP/xdebug>

## MySQL

On pourra activer les slow queries (voir : <http://trac.evolix.net/infogerance/wiki/HowtoMySQL#Logdesrequ%C3%AAteslentes)> en mettant une valeur à 0 pour voir toutes les requêtes par exemple.