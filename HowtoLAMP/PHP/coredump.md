**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**


<https://bugs.php.net/bugs-generating-backtrace.php>

Installation :

~~~
# aptitude install gdb php5-dbg
# echo 1 > /proc/sys/kernel/core_uses_pid
~~~

Note : En Squeeze il est nécessaire d'utiliser le gdb de squeeze-backports.

Pour activer :

~~~
# mkdir /home/coredump
# chmod 1777 /home/coredump
# echo '/home/coredump/core-%e-%p' > /proc/sys/kernel/core_pattern
# echo "*  soft  core  unlimited" >> /etc/security/limits.conf
# ulimit -c unlimited
# echo "ulimit -c unlimited" >> /etc/default/apache2
# /etc/init.d/apache2 restart
~~~

Pour désactiver :

~~~
# echo '' > /proc/sys/kernel/core_pattern
~~~

Pour lire les traces de PHP-FPM :

~~~
$ gdb /usr/sbin/php-fpm core-php5-fpm.7409

(gdb) bt
(gdb) bt -100
~~~

## Quelques messages typiques

Erreurs de mémoire (souvent des boucles) :

~~~
#0  xbuf_format_converter (xbuf=0x77a812cee810, fmt=0xa6d5a1 "%ld", ap=Cannot access memory at address 0x77a812cedf10)

#0  0x00007ffd348abe53 in _zend_mm_free_canary_int (heap=0x7ffd39b11130, p=0x21143c453bb97f0f) at /build/buildd/php5-5.3.2/Zend/zend_alloc_canary.c:2090
2090	/build/buildd/php5-5.3.2/Zend/zend_alloc_canary.c: No such file or directory.
   in /build/buildd/php5-5.3.2/Zend/zend_alloc_canary.c

#0  0x00000000007e10b1 in zend_mm_free_cache ()
~~~

Astuce : utiliser xdebug pour en savoir plus sur la boucle (xdebug étant limité à 100 récursions, il affichera les infos via une PHP Fatal Error :

~~~
Fatal error: Maximum function nesting level of '100' reached, aborting! in [...]
~~~



