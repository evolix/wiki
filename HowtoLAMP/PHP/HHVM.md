**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto HHVM

## Installation

Doc officielle : 
<https://github.com/facebook/hhvm/wiki/Prebuilt-Packages-on-Debian-8>
<https://github.com/facebook/hhvm/wiki/FastCGI>

~~~
# apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x5a16e7281be7a449
# echo deb <http://dl.hhvm.com/debian> jessie main > /etc/apt/sources.list.d/hhvm.list
# apt update && apt install hhvm
~~~


## Configuration

Fichier /etc/hhvm/server.ini.

On pourra passer en mode socket plutôt que sur un port TCP.

~~~
;hhvm.server.port = 9000
hhvm.server.file_socket=/var/run/hhvm/sock
~~~


## Mise en place avec NginX


~~~
# /usr/share/hhvm/install_fastcgi.sh
# /etc/init.d/nginx restart
~~~

Cela crée le fichier /etc/nginx/hhvm.conf.

~~~
location ~ \.(hh|php)$ {
    fastcgi_keep_conn on;
    #fastcgi_pass   127.0.0.1:9000;
    fastcgi_pass unix:/var/run/hhvm/sock
    fastcgi_index  index.php; 
    fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include        fastcgi_params;
}
~~~

HHVM écoute par défaut sur le port 9000. (Attention si vous avez une configraution FPM qui écoute aussi sur ce port là).
Il faudra inclure ce fichier de configuration dans votre vhost qui a besoin de HHVM.


~~~
include /etc/nginx/hhvm.conf;
~~~

## Mise en place avec Apache

TODO