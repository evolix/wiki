**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Performances sur une plateforme LAMP

<http://gtmetrix.com/>
<https://developers.google.com/speed/pagespeed/insights/>

Les performances d'une plateforme web, cela signifie :

* Un affichage rapide pour vos visiteurs/utilisateurs (critère A)
* Une utilisation minimale des ressources de votre infrastructure (critère B)
* Une perception performante pour les moteurs de recherche (critère C)

## Au niveau applicatif

* Choix du langage

## Au niveau système

* Apache/mod_php vs Apache/CGI vs Nginx/FPM ?
* Reverse/proxy/cache

### Optimisation de la mémoire de MySQL

### Optimisation des process Apache

Apache sera d'autant plus réactif qu'il a des process en mode "idle" prêt à répondre.
Pour cela, il faut une méthode empirique : observer les process sur plusieurs jours/semaines/mois
avec courbes Munin par exemple, puis ajuster les valeurs pour s'assurer qu'il y a en permanence
un minimum de process en mode "idle".

Voici un exemple d'optimisation :

~~~
    Timeout              10
    KeepAlive            On
    MaxKeepAliveRequests 25
    KeepAliveTimeout      3
    StartServers         50
    MinSpareServers      30
    MaxSpareServers      50
    ServerLimit         250
    MaxClients          250
    MaxRequestsPerChild   0
~~~


### Compression GZIP / DEFLATE

Le principe est de compresser les fichiers HTML/Javascript/CSS côté serveur, de les envoyer compressés au navigateur, et le navigateur les décompresse.

Avec Apache, cela se fait via le module mod_deflate et avec une configuration du type :

~~~
<IfModule mod_deflate.c>
    # OK avec tous la navigateurs (dont MSIE 6, si si)
    AddOutputFilterByType DEFLATE text/html text/plain text/xml

    # ...bon, des vieilles versions de MSIE 6 peuvent raler pour ca...
    AddOutputFilterByType DEFLATE text/css
    AddOutputFilterByType DEFLATE application/x-javascript application/javascript application/ecmascript
    AddOutputFilterByType DEFLATE application/rss+xml

    # On ajoute donc des exceptions :
    # pour Netscape Navigator 4...
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4.0[678] no-gzip
    # ...mais tout est OK pour MSIE, on exclue de l'exclusion...
    BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
    # ...sauf pour MSIE 6 et inferieur, que l'on exclue definitivement (paix a leurs ames).
    BrowserMatch "MSIE [3456]" no-gzip
</IfModule>
~~~


## mod_pagespeed

Nous déconseillons l'utilisation de _mod_pagespeed_

Le problème principal de _mod_pagespeed_ est qu'il est mal codé : en résumé il embarque de nombreuses bibliothèques au lieu d'utiliser celles du système, ce qui pose des problèmes de sécurité, voir <https://github.com/pagespeed/mod_pagespeed/issues/226> ; et qu'il n'est donc pas inclu dans les distributions Linux, notamment dans Debian, voir <http://bugs.debian.org/602316>

Deuxième problème avec "mod_pagespeed" il utilise des répertoires de cache partagés entre les différents comptes ce qui est incompatible avec le module Apache-ITK indispensable pour cloisonner les différents comptes sur un serveur.
Ce problème nécessite une configuration spécifique avec ModPagespeedFileCachePath à mettre en place dans chaque VirtualHost qui l'utilise.

Nous préconisons plutôt l'optimisation du code HTML/JS/CSS/etc. indépendamment de "mod_pagespeed" en appliquant les préconisations via <https://developers.google.com/speed/pagespeed/insights>

## Au niveau infrastructure

* DNS
* Cloud ?
* CDN ?
