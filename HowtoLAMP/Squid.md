**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Squid

Pour sécuriser le serveur web, nous installons un proxy HTTP en sortie. Cela permet d'interdire ou
de garder des traces des requêtes faites vers l'extérieur.

## Installation

~~~
# aptitude install squid3
~~~

## Configuration

La configuration se passe dans le fichier _squid.conf_ qui contiendra :

~~~
# ports
<http_port> 8888 transparent
icp_port 0

# ACL
#acl all src 0.0.0.0/0.0.0.0
acl localhost src 127.0.0.1/32
acl INTERNE src ADRESSE_IP/32 127.0.0.0/8
acl Safe_ports port 80          # http
acl SSL_ports port 443 563

acl WHITELIST url_regex "/etc/squid3/whitelist.conf"
acl REFERER referer_regex "/etc/squid3/whitelist-referer.conf"

<http_access> deny !REFERER !WHITELIST
<http_access> allow INTERNE
<http_access> deny all

logformat combined   %>a %[ui %[un [%tl] "%rm %ru HTTP/%rv" %>Hs %<st "%{Referer}>h" "%{User-Agent}>h" %Ss:%Sh
access_log /var/log/squid3/access.log combined
~~~

Le fichier _whitelist-referer.conf_ contiendra les URLs autorisées en sortie dont le referer est l'un des suivants :

~~~
<https://foo.org/.*>
<http://bar.org/.*>
~~~

Le fichier _whitelist.conf_ contiendra les URLs autorisées en sortie :

~~~
<http://.*debian.org/.*>
<http://pub.evolix.org/.*>
<http://www.kernel.org/.*>
<http://pear.php.net/.*>
<http://.*akismet.com/.*>
<http://.*wordpress.org/.*>
<http://.*twitter.com/.*>
<http://feeds.feedburner.com/.*>
<http://feeds2.feedburner.com/.*>
<http://sync.openx.org/.*>
<http://oxc.openx.org/.*>
<http://code.openx.org/.*>
<http://pc.openx.com/.*>
<http://api.pc.openx.com/.*>
<http://bid.openx.net/.*>
<http://blog.openx.org/.*>
<http://forum.openx.org/.*>
<http://www.backports.org/.*>
<http://.*.facebook.com/.*> 
<http://.*.fbcdn.net/.*> 
<http://.*.google-analytics.com/.*>
<http://ajax.googleapis.com/.*>
~~~

La liste des URLs que nous utilisons en standard peut-être trouvée sur <http://forge.evolix.org/scm/viewvc.php/trunk/etc/whitelist.conf?root=packweb&view=log>

Il reste évidemment à rediriger les requêtes HTTP en sortie vers Squid avec IPTables :

~~~
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -m owner --uid-owner proxy -j ACCEPT
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -d  ADRESSE_IP -j ACCEPT
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -d 127.0.0.1 -j ACCEPT
/sbin/iptables -t nat -A OUTPUT -p tcp --dport 80 -j REDIRECT --to-port 8888
~~~

## Tips

### Squid est lent / indispo

Cas possible où squid tente de sortir en IPv6 alors qu'il n'y a qu'un lien local !

Astuce : rajouter `tcp_outgoing_address $IP`

### Astuce "pêche à la ligne"

Si vous voyez passer des requêtes vers des sites suspects et que vous voulez identifier quel utilisateur les provoque, voici l'astuce "pêche à la ligne" : 
~~~
# iptables -I OUTPUT -p tcp --syn -s <IP locale> --dport 8888 -j LOG --log-uid
~~~


### Desactiver le cache

Dans certains cas, on peut vouloir désactiver le cache de façon globale :

~~~
cache deny all
~~~

### Problème avec Expect 100-continue

En cas d'erreur 417 sur des Expect 100-continue, ajouter la directive :

~~~
ignore_expect_100 on
~~~
