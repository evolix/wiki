**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto Postfix pour du LAMP

Il est désormais essentiel pour un service web de pouvoir envoyer des mails.
Pour cela, on installe Postfix et on réalise une configuration de base.

Sous Debian :

~~~
# aptitude install postfix
~~~

Avec un fichier de configuration _main.cf_ du type :

~~~
smtpd_banner = $myhostname ESMTP mail server
biff = no
append_dot_mydomain = no
#delay_warning_time = 4h
myhostname = hosting.example.com
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = $myhostname
mydestination = $myhostname localhost.localdomain localhost
relayhost = 
mynetworks = 127.0.0.0/8
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = ipv4

minimal_backoff_time = 2h
maximal_backoff_time = 6h
maximal_queue_lifetime = 4d
queue_run_delay = 100s
bounce_queue_lifetime = 1d

initial_destination_concurrency = 5
default_destination_concurrency_limit = 20

slow_destination_rate_delay = 0
slow_destination_concurrency_limit = 1
slow_destination_concurrency_failed_cohort_limit = 100
slow_destination_recipient_limit = 25

transport_maps = hash:$config_directory/transport
~~~

Et un fichier _master.cf_ du type :

~~~
slow      unix     -       -       n       -       -    smtp
~~~

Et un fichier _transport_ du type :

~~~
orange.fr slow:
wanadoo.fr slow:
voila.fr slow:
laposte.net slow:
~~~

Faire un postmap dessus :

~~~
postmap transport
~~~

Et enfin (re)démarrer le service postfix.