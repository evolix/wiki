**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto PHP

<http://php.net/supported-versions.php>

* Debian 6 : PHP 5.3.3
* Debian 7 : PHP 5.4.45
* Debian 8 : PHP 5.6.19
* Debian 9 : PHP 7.0.10


## Installation

Installation avec Apache :

~~~
# apt install php5 libapache2-mod-php5 php5-gd php5-imap php5-ldap php5-mcrypt php5-mhash php5-mysql php5-pgsql php-gettext librsvg2-bin
~~~

PHP est ainsi actif. On le vérifiera aisément avec :

~~~
$ php -v
PHP 5.6.19-0+deb8u1 (cli) (built: Mar 14 2016 10:22:33) 
Copyright (c) 1997-2016 The PHP Group
Zend Engine v2.6.0, Copyright (c) 1998-2016 Zend Technologies
    with Zend OPcache v7.0.6-dev, Copyright (c) 1999-2016, by Zend Technologies

# echo "<?php phpinfo(); ?>" > /var/www/info.php

$ wget <http://127.0.0.1/info.php>
~~~

## Configuration de base

Il existe un fichier _php.ini_ suivant l'utilisation de PHP : Apache, CLI, FPM, etc.
Pour un exemple de "sécurisation" de PHP pour Apache via _/etc/php5/apache2/php.ini_ :

~~~
short_open_tags = Off
disable_functions = exec, shell-exec, system, passthru, putenv, popen
expose_php = Off
display_errors = Off
log_errors = On
allow_url_fopen = Off
memory_limit = 128M
max_execution_time = 10
open_basedir = /home
~~~

Note : par défaut, les erreurs PHP sont enregistrés dans le fichier _ErrorLog_ du VVirtualHost Apache.


## Cache/accélérateur pour PHP 

Depuis PHP 5.6 (Debian 8), un cache d'OPcode (code "intermédiaire" généré par PHP) est intégré à PHP permet d'accélérer la génération du résultat des scripts PHP.
Il remplace les accélérateurs comme APC, eAccelerator, etc. utilisé dans les versions précédentes de PHP.
C'est le module OpCache configurable via /etc/php5/mods-available/opcache.ini

La valeur de cache par défaut est de 64M, paramètre *opcache.memory_consumption*

Pour surveiller OpCache on peut utiliser :
- <https://github.com/amnuts/opcache-gui> [[BR]]
- Plugin Munin <https://github.com/munin-monitoring/contrib/blob/master/plugins/php/php_opcache> + <https://github.com/munin-monitoring/contrib/blob/master/plugins/php/php_opcache.php>


### APC (deprecated)

<http://fr.php.net/manual/en/book.apc.php>

En PHP 5.3/5.4 (Debian 6/7), on installait souvent le module APC pour accélérer la génération des scripts PHP.

~~~
# aptitude install php-apc
~~~

La directive `apc.enabled`, qui détermine l'activation d'APC, est à 1 par défaut. Elle s'active uniquement globalement
(pour désactiver/activer par VirtualHost, on utilisera la directive _apc.cache_by_default_).

Example de configuration apc.ini :

~~~
extension=apc.so
apc.stat = 1
apc.include_once_override = 0
apc.shm_size=256M
apc.enabled = 1
apc.cache_by_default = 1
~~~

Ne pas oublier de faire :

~~~
# sysctl -w kernel.shmmax=536870912
# echo 'kernel.shmmax=536870912' >> /etc/sysctl.conf
~~~

Pour activer APC pour un site en particulier, on ajoutera dans son VirtualHost :

~~~
php_admin_value apc.cache_by_default 1
~~~

Un script est fourni par Debian pour accéder aux statistiques APC. Il suffit de le déposer dans un répertoire accessible par le serveur web :

~~~
# zcat /usr/share/doc/php-apc/apc.php.gz >/var/www/apc.php
~~~

Positionner un mot de passe dans le fichier _apc.php_ pour pouvoir vider le cache APC via cette page...

Il peut arriver que le error.log grossisse de façon alarmante avec la configuration par défaut en cas de fort trafic :

~~~ 
[apc-warning] Unable to allocate memory for pool. in /var/www/foo/bar.php on line XXX 
~~~

Il suffit généralement d'augmenter la taille du cache APC dans /etc/php5/apache2/conf.d/apc.ini :

~~~
apc.shm_size=64
~~~

Pour augmenter le cache à 64Mo.

/!\ Préciser l'unité (MB ou Mo) peut empêcher apache de redémarrer, par défaut le cache est déjà en MB /!\
/!\ Assurez-vous d'avoir une valeur kernel de kernel.shmmax supérieur (à adapter avec sysctl) /!\

### APcu

Pour faire du cache de clé-valeur sans installer un service externe (comme Memcached par exemple), on pourra installer APCu.


## Sessions PHP

<http://php.net/manual/en/book.session.php>

Lorsque des sessions PHP sont utilisées (fonctions session_*() dans le code), des informations sont stockées côté serveur.
Le navigateur conserve uniquement identifiant pour accéder à ces informations, stockés dans un cookie ou une variable du type PHPSESSID dans l'URL (cela tend à être obsolète).

Par défaut, ces informations sont conservées dans des fichiers sur le disque (un fichier par session) mais il est conseillé d'utiliser
une méthode plus performante si vous avez un serveur dédié. Évidemment il est EXCLU de stocker les sessions dans une base de données SQL
sauf si vous voulez payer cher pour avoir un site lent et tuer des bébés phoques.

* La plus simple si votre site web est en mono-serveur ou en mode sticky : monter le répertoire qui stocker les fichiers en *TMPFS*.
Cela se fait par exemple en ajoutant la ligne suivante dans votre _fstab_ :

~~~
tmpfs /var/lib/php5 tmpfs defaults,noexec,nosuid,nodev,size=256m 0 0
~~~

* Les sessions peuvent être stockées dans un ou plusieurs serveurs *Memcached* avec les paramètres suivants :

~~~
session.save_handler = memcached
session.save_path = "192.0.43.10:11211,192.0.43.11:11211"
~~~

Par contre, contrairement aux idées reçues, ce n'est pas conseillé. Memcached est fait pour "cacher" et non "stocker".
Voir le blog d'un développeur de Memcached à ce sujet : <http://dormando.livejournal.com/495593.html>

* Les sessions peuvent être stockées dans un ou plusieurs serveurs *Redis* avec les paramètres suivants :

~~~
session.save_handler = redis
session.save_path = "tcp://192.0.43.10:6379?auth=<password>,tcp://192.0.43.11:6379?auth=<password>"
~~~

* Les sessions peuvent être stockées dans [Sharedance](http://www.pureftpd.org/project/sharedance), un petit logiciel spécialement conçu pour
stocker les sessions PHP. Il n'est plus maintenu, mais très simple, il est toujours fonctionnel. Voici un exemple d'utilisation :

~~~
auto_prepend_file = /usr/local/etc/sharedance.php
;session.save_handler = user
;session.save_handler = files
~~~

* Les sessions peuvent être stockées dans *Tokyo Tyrant/Tycoon* avec les paramètres suivants :

~~~
tokyo_tyrant.session_salt="randomlongstring"
session.save_handler=tokyo_tyrant
session.save_path="tcp://192.0.43.10:1978,tcp://192.0.43.10:1978"
~~~

## Modules de paiement

### SPPlus

Pré-requis :

~~~
# apt install php5-dev
~~~

Attention à bien vérifier que /tmp n'est pas monté en noexec pour la suite.

Compilation du module :

~~~
cd /tmp
wget <http://kits.spplus.net/integration/spplus/kits/spplus-1.2.tar.gz>
tar xzf spplus-1.2.tar.gz
cd spplus-1.2
phpize
./configure
make
cp modules/php_spplus.so /usr/lib/php5/<builddate>/
chmod 644 /usr/lib/php5/<builddate>/php_spplus.so
~~~

Ajout dans la conf PHP :

~~~
echo "extension=php_spplus.so" >/etc/php5/conf.d/spplus.ini
chmod 644 /etc/php5/conf.d/spplus.ini
~~~

## Module de protection suhosin (deprecated)

<http://www.hardened-php.net/suhosin/configuration.html>

Suhosin était un module du projet Hardened-PHP permettant d'ajouter une couche de protection à PHP.
Il existait en PHP 5.2/5.3 (Debian 6/7), mais n'existe plus en PHP 5.6 (Debian 8).

~~~
# aptitude install php5-suhosin
~~~

Un exemple de configuration dans le fichier _/etc/php5/conf.d/suhosin.ini_ :

~~~
extension=suhosin.so

;log
suhosin.log.syslog = S_ALL 
;mode "simulation" pour tester
;suhosin.simulation = On
;empecher d'augmenter le memory_limit défini
suhosin.memory_limit = 0
;Action en cas de soucis
;suhosin.filter.action = 406,<http://hosting.evolix.net/406.html>
suhosin.filter.action = 406
;Fonctions interdites
suhosin.executor.func.blacklist="exec,shell-exec,system,passthru,putenv,popen"
;Restrictions sur les variables GET/REQUEST/POST...
suhosin.get.max_name_length = 126
suhosin.request.max_varname_length = 256
suhosin.request.max_array_index_length = 128
suhosin.request.max_vars = 1000
suhosin.post.max_vars = 1000
~~~

## Module du framework phalcon

<https://docs.phalconphp.com/fr/latest/reference/install.html>
<https://github.com/phalcon/phalcon-devtools>

Installer les dépendances suivantes :

~~~
# apt install php5-dev libpcre3-dev gcc make php5-mysql
~~~

Cloner la dernière version stable (2.0.x à ce jour) :

~~~
git clone --depth=1 git://github.com/phalcon/cphalcon.git -b 2.0.x
~~~

Compiler et installer le module :

~~~
cd cphalcon/build
sudo ./install
~~~

Configurer PHP et Apache :

~~~
# Dans le fichier /etc/php5/mods-available/phalcon.ini
extension=phalcon.so
# Puis activer le module
ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/cli/conf.d/30-phalcon.ini
ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/apache2/conf.d/30-phalcon.ini
service apache2 reload
~~~

Installer les phalcon devtools :

~~~
git clone <https://github.com/phalcon/phalcon-devtools.git> -b 2.0.x /opt/phalcon-devtools
ln -s /opt/phalcon-devtools/phalcon.php /usr/local/bin/phalcon
chmod ugo+x /usr/local/bin/phalcon
~~~

## FAQ

* J'ai un message d'erreur du type : _/usr/lib/php5/20090626/xcache.so doesn't appear to be a valid Zend extension_

Réponse : remplacer zend_extension= par extension= pour le chargement du module PHP

* Certaines fonctions GD n'existent pas : imagerotate(), imageantialias(), etc.

Réponse : Pour des raisons de sécurité et de maintenabilité, la version de
PHP de Debian n'embarque par le GD modifié par PHP. Ces fonctions ne sont
pas disponibles. Pour contourner ce problème, nous conseillons d'écrire vos
propres fonctions (il y a des exemples sur <http://php.net).>
Cela peut éventuellement être mis dans un fichier partagé qui
sera ensuite inclus systématiquement dans votre code lorsque
l'utilisation est nécessaire.
