## IonCube

Voici une procédure d'installation rapide de IonCube pour PHP sur un serveur LAMP.

* Téléchargement de ioncube_loaders (prenez la version correspondant à votre architecture) :

~~~
# wget <https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz>
# tar zxvf ioncube_loaders_lin_x86-64.tar.gz
~~~

* On place ensuite les fichiers au bon endroit :

~~~
# mv ioncube /usr/local/
~~~

* Puis on rajoute ceci dans un fichier que l'on nomme `/etc/php/8.2/mods-available/ioncube.ini` (prenez le .so correspondant à la version de PHP installée) :

~~~
zend_extension = /usr/local/ioncube/ioncube_loader_lin_8.2.so
~~~

* Il faut aussi activer l’extension et redémarrer le processus, par exemple pour php-fpm :

~~~
# ln -s /etc/php/8.2/mods-available/ioncube.ini /etc/php/8.2/fpm/conf.d/00-ioncube.ini
# php-fpm8.2 -t && systemctl reload php8.2-fpm
~~~

----

* Sur Debian Jessie, le fichier est : `/etc/php5/mods-available/ioncube.ini`.

~~~
zend_extension = /usr/local/ioncube/ioncube_loader_lin_5.6.so
~~~
    
Puis on fait un lien symbolique de /etc/php5/apache2/conf.d/00-ioncube.ini -> /etc/php5/mods-available/ioncube.ini 

**Note** : Le préfixe `00-` est important, si il n'est pas chargé en premier, ioncube génère une erreur fatale quit fait crasher Apache.

~~~
ln -s /etc/php5/mods-available/ioncube.ini /etc/php5/apache2/conf.d/00-ioncube.ini
~~~

* Enfin, on redémarre Apache

~~~
# /etc/init.d/apache2 restart
~~~
