---
title: Howto XFCE
category: desktop-environment WIP
---

## Installation

~~~
# apt install xfce4 xfce4-goodies
~~~

On peut éventuellement installer directement le méta-paquet `task-xfce-desktop` qui va en plus installer des utilitaires et autres programmes pour avoir un environnement de bureau pleinement opérationnel.

~~~
# apt install task-xfce-desktop
~~~


## Utile & Pratique 

### Racourcis clavier

* `ALT` + `INS` -> Ajouter un nouveau workdspace
* `ALT` + `DEL` -> Supprimer le dernier workspace
* `CTRL` + `ALT` + `NUM_PAD` -> Déplace la fenêtre active sur le workspace spécifié
* `CTRL` + `ALT` + `HOME` // `CTRL` + `ALT` + `END` -> Déplace la fenêtre active sur le workspace de gauche/droite
* `CTRL` + `ALT` + `LEFT` // `CTRL` + `ALT` + `RIGHT` -> Basculer sur le workspace de gauche/droite
* `CTRL` + `F1` // `CTRL` + `F2` // ... -> Basculer directement au workspace 1/2/...

* `ALT` + `F6` -> Afficher la fenêtre active sur tous les workspaces
* `ALT` + `F12` -> Activer/Désactiver l'affichage de la fenêtre active par dessus tout

* `SUPER` + `P` -> Paramètres d'affichage
* `CTRL` + `ALT` + `L` // `CTRL` + `ALT` + `DEL` -> Verrouiller la session