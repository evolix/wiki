---
categories: cli ssh tmux xpanes cssh
title: Howto xpanes
...

* Documentation : <https://github.com/greymd/tmux-xpanes>

xpanes est un « diviseur de terminal » qui s'appuie sur [Tmux](https://wiki.evolix.org/HowtoTmux). Son usage le plus classique est d'avoir des connexions SSH multiples et synchronisées, mais il dispose de nombreuses autres fonctionnalités.

## Installation

Un paquet _.deb_ est disponible, mais uniquement au téléchargement direct et pas via un dépôt. : <https://github.com/greymd/tmux-xpanes/releases>

## Usage courant

Avoir une fenêtre Tmux avec 3 panneaux, un pour chacun des 3 serveurs. Une fois la session terminée, le panneau associé est fermé.

~~~{.bash}
xpanes --ssh -ss serveur1 serveur2 serveur3
# ou bien
xpanes -c "ssh user@{}" serveur{0,1,2}
~~~

Retrouver la session tmux :

Si vous êtes sorti avec un `ctrl + b puis d`, vous pouvez vous y re-attacher et retrouvant sa socket dans ...

~~~{.bash}
ls ~/.cache/xpanes/socket.*
tmux -S ~/.cache/xpanes/socket.2032305 a
~~~

