---
title: Howto Debian: migration Squeeze (6) -> Wheezy (7)
---

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

* Release Notes i386 : <http://www.debian.org/releases/wheezy/i386/release-notes/>
* Release Notes amd64 : <http://www.debian.org/releases/wheezy/amd64/release-notes/>

Attention, il est [recommandé](http://www.debian.org/releases/wheezy/amd64/release-notes/ch-upgrading.en.html#upgrading-full) d'utiliser `apt-get` et non `aptitude` :


Avant de mettre à jour penser à vérifier les paquets étiquetés et suspendus et modifier si besoin :

~~~
# apt-cache policy
# aptitude search "~ahold"
~~~

Fichiers [`sources.list` conseillés](SourcesList#wheezy-7).

Commencer par télécharger l'ensemble des paquets qui devront être installés (afin de limiter le temps effectif d'installation).

~~~
# apt-get dist-upgrade --download-only
~~~

Faire ensuite une mise à niveau "simple", pour appliquer les mises à jour triviales :

~~~
# apt-get upgrade
~~~

Enfin, appliquer les mises à jour non triviales (nécessitant des changements de paquets dépendants, des suppressions…) afin d'avoir un œil plus précis sur ce qui sera fait, avant de valider :

~~~
# apt-get dist-upgrade
~~~

## VERSIONS

Passage de Squeeze à Wheezy :

* MySQL 5.1.49 → 5.5.53
* PHP 5.3.3 → 5.4.45
* Apache 2.2.16 → 2.2.22
* Tomcat 6.0.35 → 6.0.45

## /etc/securetty

La syntaxe a changé. Si il y a des restrictions particulières, il faudra les remettre en place.

## /etc/nagios/nrpe.cfg

Si vous avez une erreur :

~~~
nrpe: Cannot write to pidfile '/var/run/nrpe.pid' - check your privileges.
~~~

Workaround : modifier pid_file :

~~~
pid_file=/var/run/nagios/nrpe.pid
~~~

## Shell

Attention, vérifiez bien que le lien `/bin/sh` ne change pas de destination (de Bash à Dash) sans prévenir !!
On a constaté un cas où `/bin/sh` pointait vers Dash après mise-à-jour alors que ça pointait vers Bash avant.
Vérifier dans les backups vers quoi pointait `/bin/sh` avant l'upgrade

## Apache

`httpd.conf` n'existe plus par défaut : si il était vide mais que vous conservez votre ancien `apache2.conf` vous aurez une erreur du type :

~~~
Starting web server: apache2
apache2: Syntax error on line 207 of /etc/apache2/apache2.conf: Could not open configuration file /etc/apache2/<httpd.conf:> No such file or directory
~~~

Workaround : touch `/etc/apache2/httpd.conf`

## /etc/sudoers

Il est conseillé de migrer la configuration en utilisant `/etc/sudoers.d/XXXX` : <http://www.debian.org/releases/stable/amd64/release-notes/ch-upgrading.en.html#package-specific-issues>

## PHP

### Directives obsolètes

Les directives suivantes n'existe plus, il faut donc les commenter ou supprimer de la configuration.

* `allow_call_time_pass_reference`
* `magic_quotes_gpc`
* `register_long_arrays`


### php5-suhosin

> The php5-suhosin package has been removed. If your PHP configuration included the suhosin module, it will fail to load after the PHP upgrade. Run dpkg --purge php5-suhosin to remove the leftover configuration in /etc/php5/conf.d/suhosin.ini.


## Evomaintenance

> /usr/share/scripts/evomaintenance.sh: 54: /usr/share/scripts/evomaintenance.sh: sendmail: not found

Il faut soit mettre le path complet dans evomaintenance : `/usr/sbin/sendmail`


## Evoadmin Web

Version de Squeeze incompatible avec Wheezy, il faut prendre la version GIT :

Il faut mettre dans `/etc/apache2/envvars`

~~~
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
~~~

## Dovecot

La configuration de Dovecot est désormais découpée en Wheezy, cela peut donner des résultats
surprenants lors de la mise-à-jour. Il est conseillé de reprendre la configuration de zéro
ou preque. Bien penser à éditer différents fichiers :

~~~
# vim conf.d/10-auth.conf

disable_plaintext_auth = no
!include auth-ldap.conf.ext

# vim conf.d/15-lda.conf

protocol lda {
  auth_socket_path = /var/run/dovecot/auth-master
  postmaster_address = postmaster@example.com
}

# vim conf.d/10-ssl.conf

ssl_cert = </etc/ssl/certs/dovecot.pem
ssl_key = </etc/ssl/private/dovecot.pem

# vim conf.d/10-master.conf

service auth {
  unix_listener auth-userdb {
  }
  # Postfix smtp-auth
  unix_listener /var/spool/postfix/private/auth {
    group = postfix
    mode = 0660
  }
  unix_listener auth-master {
    group = vmail
    mode = 0660
  }
  user = root
}

# vim conf.d/10-mail.conf
mail_location = maildir:/home/vmail/%d/%n
mail_uid = vmail
mail_gid = vmail
~~~

Attention également, il faudra éventuellement installer :

~~~
# aptitude install dovecot-ldap
# mv /etc/dovecot/dovecot-ldap.conf /etc/dovecot/dovecot-ldap.conf.ext
~~~


Test de la configuration :

~~~
# dovecot -n
~~~


## MySQL

### option secure_file_priv

Penser à ajouter la valeur par défaut à `secure-file-priv` dans la config de MySQL (premier trouvé par ordre de préférence) :

* `/etc/mysql/conf.d/evolinux.cnf`
* `/etc/mysql/conf.d/evolix.cnf`
* `/etc/mysql/my.cnf`
* `/etc/mysql/conf.d/000-evolinux-defaults.cnf`

~~~{.ini}
[mysqld]
secure-file-priv = ""
~~~

### option default-character-set

~~~
[ERROR] /usr/sbin/mysqld: unknown variable 'default-character-set=utf8'
~~~

Contournement : désactiver cette option…

Erreurs :

~~~
mysqld_safe[27427]: 150828  1:11:22 [ERROR] An old style --language value with language specific part detected: /usr/share/mysql/english/
mysqld_safe[27427]: 150828  1:11:22 [ERROR] Use --lc-messages-dir without language specific part instead.
~~~

Désactiver l'option obsolète `language = /usr/share/mysql/english`


Pour désactiver le moteur InnoDB, skip-innodb n'est plus suffisant, vous allez obtenir l'erreur :

~~~
mysqld: 150828  1:16:04 [ERROR] Unknown/unsupported storage engine: InnoDB
~~~

Vous devez ajouter :

~~~{.ini}
skip-innodb
default-storage-engine=myisam
~~~


### mysqldump

Depuis la version 5.5, on ne peut plus "dumper" la base performance_schema, on obtient une erreur :

~~~
mysqldump: Got error: 1142: SELECT,LOCK TABL command denied to user 'mysqladmin'@'localhost' for table 'cond_instances' when using LOCK TABLES
~~~

Ceci est normal, la base performance_schema ne devant pas être dumpée en général.

Voir <http://bugs.mysql.com/bug.php?id=61414> et <http://dev.mysql.com/doc/refman/5.5/en/performance-schema-restrictions.html>

### .my.cnf

Si vous obtenez une erreur :

~~~
Warning: Using unique option prefix pass instead of password is deprecated and will be removed in a future release. Please use the full name instead.
~~~

Corrigez vos fichiers `.my.cnf` pour mettre `password` à la place de `pass`.

## base-passwd

Si vous obtenez ce message :

~~~
update-passwd has found some differences between your system accounts
and the current Debian defaults. It is advisable to allow update-passwd
to change your system; without those changes some packages might not work
correctly.  For more documentation on the Debian account policies please
see /usr/share/doc/base-passwd/README.

The list of proposed changes is :
[…]
~~~

Il est conseillé de refuser et de conserver les uid/gid utilisés.

## firmware bnx2

Rajouter non-free dans sources.list pour installer la mise à jour des firmwares bnx2.

## Oracle

Oracle veut utiliser `/dev/shm` en tmpfs, mais celui-ci a été déplacé en Wheezy, c'est maintenant `/run/shm`, avec un symlink vers `/dev/shm`, mais le symlink ne suffit pas ! Il faut donc revenir à l'ancienne méthode, pour cela on mettra dans le fstab :


~~~
tmpfs     /dev/shm  tmpfs     nosuid,nodev,size=50%,mode=1777    0    0
~~~

## libnss LDAP

Attention, si vous avez répondu YES à "Make the configuration file readable/writeable by its owner only?" le fichier `/etc/libnss-ldapconf` repassera en 600.

Si nécessaire :

~~~
# chmod 644 /etc/libnss-ldapconf
# dpkg-reconfigure libnss-ldap
~~~


## ProFTPD

~~~
proftpd[24268]: Fatal: LoadModule: error loading module 'mod_vroot.c': Operation not permitted on line 74 of '/etc/proftpd/modules.conf'
~~~

Désactiver le module `mod_vroot` ou installer le paquet `proftpd-mod-vroot`.

~~~
proftpd[18591]: Fatal: unknown configuration directive 'LDAPDNInfo' on line 14 of '/etc/proftpd/ldap.conf'
proftpd[18591]: Fatal: unknown configuration directive 'LDAPDoAuth' on line 15 of '/etc/proftpd/ldap.conf'
~~~

Les directives `LDAPDNInfo` et `LDAPDoAuth` sont à remplacer par `LDAPBindDN` et `LDAPUsers`.

## Roundcube

Si la mise à jour automatique de la bdd roundcube se passe mal, et que l'on a l'erreur "[Native message: Unknown column 'changed' in 'field list']" dans `/var/log/roundcube/error.log` :

~~~
# mysql
> USE roundcube;
> ALTER TABLE `identities` ADD `changed` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' AFTER `identity_id`;
~~~

## Mailgraph

Il faut appliquer ce patch pour /usr/share/scripts/mailgraph.sh

~~~{.diff}
--- old 2015-06-11 18:01:16.476793959 +0200
+++ mailgraph.sh        2015-06-11 17:59:05.926043623 +0200
@@ -1,5 +1,6 @@
 #!/bin/sh
 MAILGRAPH_PATH=/usr/lib/cgi-bin/mailgraph.cgi # Debian
+export SCRIPT_NAME="mailgraph.cgi"
 #MAILGRAPH_PATH=/usr/local/www/cgi-bin/mailgraph.cgi # FreeBSD
 #MAILGRAPH_PATH=/usr/local/lib/mailgraph/mailgraph.cgi # OpenBSD

@@ -11,6 +12,7 @@

 $MAILGRAPH_PATH | sed '1,2d ; s/mailgraph.cgi?//' > $MAILGRAPH_DIR/index.html

-for i in 0-n 0-e 1-n 1-e 2-n 2-e 3-n 3-e; do
+for i in 0-g 1-g 2-g 3-g 0-n 0-e 1-n 1-e 2-n 2-e 3-n 3-e; do
         QUERY_STRING=$i $MAILGRAPH_PATH | sed '1,3d' > $MAILGRAPH_DIR/$i
 done
~~~

## Script Perl add.pl mail-add.pl

Cas pack samba ou mail. Il faut patcher le script et installer une librairie.


~~~
# aptitude install libdigest-sha-perl
# sed -i 's/SHA1/SHA/g' add.pl
~~~



~~~{.diff}
--- mail-add.pl 2015-08-31 09:27:24.000000000 +0200
+++ /tmp/a      2015-08-31 09:37:00.000000000 +0200
@@ -148,7 +148,7 @@
     my $result = $ldap->search(
         base => $edn,
         filter => "(uid=$ldapuid)",
-        attrs => "uid"
+        attrs => ["uid"]
     );

     $result->code && die $result->error;
@@ -206,7 +206,7 @@
     $result = $ldap->search(
         base => $edn,
         filter => "(mailacceptinggeneralid=$mail)",
-        attrs => "uid"
+        attrs => ["uid"]
         );
     $result->code && die $result->error;
     if ($result->entries) {
@@ -225,7 +225,7 @@
         $result = $ldap->search(
             base => $edn,
             filter => "(objectClass=posixAccount)",
-            attrs => "uidNumber"
+            attrs => ["uidNumber"]
             );
         $result->code && die $result->error;
         my $uid = $minuid; # uidNumber initial
@@ -374,7 +374,7 @@
         $result = $ldap->search(
             base => $dn,
            filter => '(objectClass=posixGroup)',
-            attrs => "gidNumber",
+            attrs => ["gidNumber"],
         );
         $result->code && die $result->error;
         my @entries = $result->entries;
@@ -492,7 +492,7 @@
     my $result = $ldap->search(
          base => $edn,
          filter => "(mailacceptinggeneralid=$alias)",
-        attrs => "mailacceptinggeneralid"
+        attrs => ["mailacceptinggeneralid"]
     );
     $result->code && die $result->error;
     if ($result->entries) {
@@ -540,7 +540,7 @@
     my $result = $ldap->search(
         base => $dn,
         filter => "(objectClass=mailAccount)",
-        attrs => "uid"
+        attrs => ["uid"]
     );
     $result->code && die $result->error;

@@ -590,7 +590,7 @@
     my $result = $ldap->search(
         base => $dn,
         filter => "(objectClass=mailAlias)",
-        attrs => "mailacceptinggeneralid"
+        attrs => ["mailacceptinggeneralid"]
     );
     $result->code && die $result->error;
~~~

## Asterisk

Si vous utilisez le module "meetme" vous devrez désormais installer le paquet "asterisk-dahdi"

## Sympa

La config évolue :

~~~
#ScriptAlias /wws /usr/lib/cgi-bin/sympa/wwsympa.fcgi
ScriptAlias /wws /usr/lib/cgi-bin/sympa/wwsympa-wrapper.fcgi
~~~

avec les droits suivants :

~~~
-rwxr-xr-x 1 sympa sympa 610018 janv. 17  2015 /usr/lib/cgi-bin/sympa/wwsympa.fcgi
-rwsr-sr-x 1 sympa sympa   6192 janv. 17  2015 /usr/lib/cgi-bin/sympa/wwsympa-wrapper.fcgi
~~~

## Evocheck

Si **IS_DPKGWARNING_FAILED**, télécharger `/etc/apt/apt.conf.d/80evolinux` et supprimer `/etc/apt/apt.conf`.
