# Howto Downgrade

Warning : downgrader n'est pas officiellement supporté donc à vos risques et périls blabla

## Downgrade majeure, exemple de Stretch à Jessie

### Préliminaires

Il faut avoir un maximum de paquets *.deb sous la main !

- Avoir tous les paquets /var/cache/apt/archives/
- Avoir tous les paquets de base, en faisant quelque part "deboostrap jessie tmp" et en envoyant tous les paquets *deb récupérés
- Avoir le reste des paquets installés ne fera pas de mal

Il faut avoir les sorties de `dpkg -l` et `dpkg --get-selections` du système avant upgrade.

S'assurer d'avoir "apt-get", "aptitude" et "apt" installés.

### Dans le dur

Remettre le sources.list avec la version désirée, puis `apt-get update`.

Récupérer la liste des paquets upgradés :

~~~
$ grep upgrade /var/log/dpkg.log | cut -d" " -f4,5 | sed "s/:all /_/ ; s/:amd64 /_/"
~~~

Les réinstaller via :

~~~
$ dpkg -i --dry-run locales_2.19-18+deb8u9*.deb libc-dev-bin[...]

$ apt-get install -f
$ aptitude install -f
~~~

Si souci(s), essayer de :

* `dpkg -i` le(s) paquet(s) qui pose(nt) souci(s).
* `apt-get install` le(s) paquet(s) qui pose(nt) souci(s).
* `aptitude install` le(s) paquet(s) qui pose(nt) souci(s)

Une fois fait cela, vous devriez avoir rebasculé une majorité de paquets dans la bonne version (dont "libc6").

Reste à faire le ménage, en downgradant le reste des paquets et supprimant les paquets nouvellement installés :

~~~
$ grep ^ii pkg.old | tr -s ' ' | cut -d" " -f2,3 | sed "s/:all /=/ ; s/:amd64 /=/ ; s/ /=/"
~~~

puis `apt-get install` de cette liste… devrait finir le nettoyage.

Si vous avez "You are running a kernel (version 4.9.0-3-amd64) and attempting to remove the same version." même pas peur.

Reste à `diff <(dpkg -l) dpkg-l.old` pour identifier puis supprimer les derniers paquets en trop.

Ressource à étudier :

https://ispire.me/downgrade-from-debian-sid-to-stable-from-jessie-to-wheezy/