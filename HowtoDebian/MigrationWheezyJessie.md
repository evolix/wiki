---
categories: web
title: Howto Migration Wheezy (7) vers Jessie (8)
...

Voici les principaux changements de version :

* MySQL 5.5.31 -> 5.5.43
* PHP 5.4.4 -> 5.6.7
* Apache 2.2.22 -> 2.4.10
* Tomcat 7.0.28 -> 7.0.56
* Bind 9.8.4 -> 9.9.5
* noyau Linux 3.2 -> 3.16

Release Notes amd64 : <http://www.debian.org/releases/jessie/amd64/release-notes/>

Avant de mettre à jour penser à vérifier les paquets étiquetés et suspendus et modifier si besoin :

~~~
# apt-cache policy
# aptitude search "~ahold"
~~~

Éditer les dépôts dans `/etc/apt/sources.list` et `/etc/apt/sources.list.d/*.list` pour remplacer _wheezy_ par _jessie_.

Résultat dans les **fichiers [`sources.list` conseillés](SourcesList#jessie-8)**.

> *Note* : si vous utilisez des backports pour Jessie, ils ne sont plus mis à jour, vous devrez donc faire : `echo "Acquire::Check-Valid-Until no;" > /etc/apt/apt.conf.d/99no-check-valid-until`

Puis mettre à jour le cache APT avec la commande :

~~~
# dpkg-divert --remove --rename --divert /usr/bin/apt-get.bak /usr/bin/apt-get
# apt-get update
~~~

Commencer par télécharger l'ensemble des paquets qui devront être installés (afin de limiter le temps effectif d'installation).

~~~
# apt-get dist-upgrade --download-only
~~~

Faire ensuite une mise à niveau "simple", pour appliquer les mises à jour triviales :

~~~
# apt-get upgrade
~~~

Enfin, appliquer les mises à jour non triviales (nécessitant des changements de paquets dépendants, des suppressions, ...) afin d'avoir un oeil plus précis sur ce qui sera fait, avant de valider :

~~~
# apt-get dist-upgrade
~~~

Bien vérifier que le noyau Linux a été mis à jour également :

~~~
# uname -a # version qui tourne actuellement
# dpkg -l |grep linux-image # version nouvellement installée
~~~

Si la carte ethernet est un Broadcom, vérifier également que le paquet _firmware-bnx2_ a été mis à jour.

Merger les nouvelles conf au cas par cas :

~~~
# find /etc -name "*.dpkg-*"
~~~

~~~
# for file in $(find /etc -iname '*.dpkg-dist'); do vimdiff ${file%%.dpkg-dist} $file; done
# for file in $(find /etc -iname '*.ucf-dist'); do vimdiff ${file%%.ucf-dist} $file; done
~~~

## systemd

Debian 8 utilise systemd par défaut. Pour passer effectivement en systemd (conseillé), il faut :

~~~
# apt install systemd-sysv
~~~

Informations de survie :

* lire <http://trac.evolix.net/infogerance/wiki/HowtoSystemd>
* Repasser en sysV (non conseillé/testé) : <http://without-systemd.org/wiki/index.php/How_to_remove_systemd_from_a_Debian_jessie/sid_installation>

Attention, le passage a systemd est délicat avec le démarrage réseau ! En effet, si des scripts liés au réseau sont démarrés via /etc/network/if-*up*/ vous aurez un blocage complet du démarrage...
Exemple : si le paquet openntpd est désinstallé et non purgé, votre démarrage sera bloqué => *dpkg -P openntpd*
Il faut donc vérifier que tous les scripts liés au réseau sont bien d'actualité : ls -l /etc/network/if-*up*/
Plus de détails sur <http://trac.evolix.net/infogerance/wiki/HowtoSystemd#networking>

## Linux Standard Base (LSB)

Le fichier /etc/lsb-release n'est plus mis a jour a partir de Debian Jessie mais est encore utilisé par la command lsb_release -a et par Ansible, il faut donc le supprimer pour ne pas se retrouver avec des incohérences du type "Debian 7 Jessie" :

~~~
# mv /etc/lsb-release /etc/lsb-release.old
~~~

## Bind

Si vous utilisez un chroot et systemd, vous devez (installer pkg _bind9_) -> voir <http://trac.evolix.net/infogerance/wiki/HowtoBind#InstallationetconfigurationdeBind>

~~~
# cp -a /lib/systemd/system/bind9.service /etc/systemd/system/
~~~

Ajuster la section [Service] :

~~~
EnvironmentFile=-/etc/default/bind9
ExecStart=/usr/sbin/named -f $OPTIONS
~~~

Puis :

~~~
# systemctl daemon-reload
~~~

Autre information importante pour vos zones "slave" elles sont désormais binaires par défaut,
pour conserver les anciennes il faut donc ajouter l'option (par exemple dans /etc/bind9/named.conf.options) :

~~~
masterfile-format text;
~~~

## Redis

/run n'est plus persistant. Il faut préciser dans l'unit de redis le RuntimeDirectory pour que la création de /var/run/redis soit effectuée au démarrage (et placer la socket et le pid dans /var/run/redis).

~~~
# cp -a /lib/systemd/system/redis-server.service /etc/systemd/system/
~~~

Editer /etc/systemd/system/redis-server.service et ajuster la section [Service] :

~~~
RuntimeDirectory=redis
RuntimeDirectoryMode=700
~~~

Puis :

~~~
# systemctl daemon-reload
~~~

## Apache

* Merger la conf :

~~~
# cd /etc/apache2
# vimdiff apache2.conf apache2.conf.dpkg-dist
~~~

* Autoriser /home/ ou /srv dans /etc/apache2/apache2.conf

~~~
[Wed Nov 25 16:40:33.466946 2015] [authz_core:error] [pid 17204] [client XX.XX.XX.XX:48740] AH01630: client denied by server configura
tion: /home/XXX/www/YYY/ZZZ            
~~~

~~~
<Directory /home/>                    
        Options Indexes FollowSymLinks
        AllowOverride None            
        Require all granted           
</Directory>                          
~~~

* Migrer conf.d vers conf-available

~~~
# wget https://gitea.evolix.org/evolix/evocheck-private/raw/branch/master/fix_IS_APACHE_CONFENABLED.sh -O /tmp/
# bash /tmp/fix_IS_APACHE_CONFENABLED.sh
~~~

* Il faut s'assurer que les liens/fichiers dans /etc/apache2/sites-enabled/* se terminent en .conf en les renommant. (Ou modifier la directive dans apache2.conf. **Non conseillé car cela casse a2{en,dis}site.**)

~~~
# cd /etc/apache2/sites-enabled/
# for file in *; do if ! [[ "$file" =~ ".conf" ]]; then mv "$file" "${file}.conf"; fi; done
~~~

* Il faut supprimer la directive :

~~~
AH00526: Syntax error on line 89 of /etc/apache2/apache2.conf:                                                 
Invalid command 'LockFile', perhaps misspelled or defined by a module not included in the server configuration 
~~~

* Il faut supprimer les directives `NameVirtualHost`:

~~~
AH00548: NameVirtualHost has no effect and will be removed in the next release
~~~

* Il faut remplacer la directive `SSLSessionCache` :

~~~
SSLSessionCache: 'shm' session cache not supported (known names: default,shmcb). Maybe you need to load the appropriate socache module (mod_socache_shm?).
~~~

par :

~~~
SSLSessionCache shmcb:/var/log/apache2/ssl_gcache_data(512000)
~~~

* La directive `SSLMutex` est osbolète :

~~~
Invalid command 'SSLMutex', perhaps misspelled or defined by a module not included in the server configuration
~~~

* Les directives Options doivent avoir + ou -.

~~~
AH00526: Syntax error on line 27 of /etc/apache2/sites-enabled/app:  
Either all Options must start with + or -, or no Option may.

# sed -i 's/Options ExecCGI -MultiViews/Options +ExecCGI -MultiViews/g' /etc/apache2/sites-available/*
~~~

* RewriteLog et RewriteLogLevel n'existent plus, à supprimer

~~~
AH00526: Syntax error on line 61 of /etc/apache2/sites-enabled/app:                                             
Invalid command 'RewriteLog', perhaps misspelled or defined by a module not included in the server configuration

# cd /etc/apache2/sites-available/
# sed -i '/RewriteLog/d' *
# sed -i '/RewriteLogLevel/d' *
~~~

* **Il ne faut plus utiliser les directives MaxClients mais MaxRequestWorkers !**

* La directive `AuthzLDAPAuthoritative` est obsolète, il faut la remplacer par les directives _Require_ adéquats : <https://httpd.apache.org/docs/2.4/mod/mod_authnz_ldap.html>

* **Le module PHP est incapable d'exécuter des binaires 32 bits**, on obtiendra une erreur « Bad system call ». Si du code PHP `exec()` des binaires, il faut absolument qu'ils soient en 64 bits.

### ModSec

* deprecated:

~~~
ModSecurity: WARNING Using transformations in SecDefaultAction is deprecated (/etc/apache2/conf.d/mod-security2.conf:19
~~~

Il faut mettre :

~~~
SecDefaultAction "log,auditlog,deny,status:406,phase:2"
~~~

* id

~~~
ModSecurity: Rules must have at least id action 
~~~

Il faut rajouter des id aux rules.

~~~
# File name                                                                                         
SecRule REQUEST_FILENAME "modsecuritytest1" "id:1"                                                  
# Complete URI                                                                                      
SecRule REQUEST_URI "modsecuritytest2" "id:2"                                                       
SecRule REQUEST_FILENAME "(?:n(?:map|et|c)|w(?:guest|sh)|cmd(?:32)?|telnet|rcmd|ftp)\.exe" "id:3"   
~~~

### Sécurisation des vhosts avec les directives Require

Pour sécurisé les vhosts par défaut, il faut supprimer le directory vers / et le remplacer par /var/www/ et y copier les directive Require all denied et Require ip IP, exemple :

~~~
<Directory /var/www/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride None
                Require all denied
                Require ip 88.179.18.233
                Require ip 62.212.121.90
                Require ip 31.170.9.129

                # Client
                Require ip 88.180.236.62
                Require ip 62.212.120.245
                Require ip 84.14.143.194
                Require ip 88.180.236.62
                Require ip 62.212.111.72
                Require ip 109.190.0.17
</Directory>
~~~

## Squid

Attention Squid3 semble non compatible avec le kernel 3.2 de Wheezy, il faut bien rebooter sur le nouveau kernel pour que Squid soit fonctionnel.

Pour installer le nouveau kernel, parfois nécessaire de préciser l'installation du kernel, en indiquant le package concerné : _linux-image-xx_

Si `apt-get update` ne marche plus, et certains sites sont inaccessibles (timeout) :

* vérifier que tcp_outgoing_address est présent dans la conf de _Squid_
* mettre à jour `init.d/minifirewall`
* restart minifirewall

### Mettre à jour minifirewall

Pour que squid fonctionne, il faut que la version de minifirewall soit récente. Auquel cas le message d'erreur suivant peut apparaître dans cache.log et squid ne démarre pas.

~~~
2016/06/30 00:37:26 kid1| sendto FD 11: (1) Operation not permitted   
2016/06/30 00:37:26 kid1| ipcCreate: CHILD: hello write test failed   
~~~

Il manque une règle ip6tables. Cela a été corrigé via [ce commit](https://forge.evolix.org/projects/minifirewall/repository/revisions/705c4683a2d7214c3f9664577955f1dd32cfdf54/diff/minifirewall).

*Il faut donc mettre à jour le script d'init minifirewall* !

## Elasticsearch

Le fichier /etc/default/elasticsearch doit contenir les informations de configuration (il ne les a pas forcément par défaut !!) :

~~~
LOG_DIR=/var/log/elasticsearch
DATA_DIR=/var/lib/elasticsearch
WORK_DIR=/tmp/elasticsearch
CONF_DIR=/etc/elasticsearch
CONF_FILE=/etc/elasticsearch/elasticsearch.yml
ES_HOME=/usr/share/elasticsearch
ulimit -n 65535
sysctl -q -w vm.max_map_count=262144
~~~

Sinon, vous aurez notamment des erreurs du type :

~~~
elasticsearch[6821]: Failed to configure logging...
elasticsearch[6821]: org.elasticsearch.ElasticsearchException: Failed to load logging configuration
~~~

Il faut apparemment forcer l'activation au démarrage :

~~~
# systemctl enable elasticsearch
~~~

## Spamassassin

SA peut gueuler, voir le package sa-compile qui n'arrive pas à s'installer.
Sans doute Razor plugin qui râle, donc vérifier le chargement de celui-ci :

~~~
grep -ri razor /etc/spamassassin/local_evolix.cf
~~~

-> commenter _razor_timeout_ (autre solution?)

## MySQL

Penser à ajouter la valeur par défaut à _secure-file-priv_ dans la config de MySQL (premier trouvé par ordre de préférence) :

* /etc/mysql/conf.d/evolinux.cnf
* /etc/mysql/conf.d/evolix.cnf
* /etc/mysql/my.cnf
* /etc/mysql/conf.d/000-evolinux-defaults.cnf

~~~
[mysqld]
secure-file-priv = ""
~~~

## Redmine

Lors de la migration, le package peut avoir été désinstallé. Regarder dans les logs dpkg:

~~~
grep redmine /var/log/dpkg.log
~~~

Réinstaller le package, et s'assurer du PassengerDefaultUser pour www-data dans le mod apache2:

~~~
$ vi /etc/apache2/mods-available/passenger.conf
  PassengerDefaultUser www-data
~~~

Si besoin, changer _adapter_ de mysql vers mysql2 (avec package à installer si non présent) : `/etc/redmine/default/database.yml`

## Evoadmin-web

### ITK

Via sudo/PHP on peut avoir ce type d'erreur :

~~~
sudo: PERM_ROOT: setresuid(0, -1, -1): Operation not permitted
~~~

Il faut mettre ceci dans votre configuration apache, section ITK :

~~~
LimitUIDRange       0 6000
LimitGIDRange       0 6000
~~~

### web-add

À cause de Apache 2.4 en Jessie, ils ne sont pas compatibles. Il faut installer la version de la branche Jessie.
Idéalement relancer le module evolinux jessie pour evoadmin (supprimer le vhost 'evoadmin' puis relancer evolinux en faisant grandement attention à ne valider que seulement l'installation de evoadmin-web).

## Evoadmin-mail

Evoadmin-mail dispose maintenant d'un paquet Debian dans le dépôt Evolix (deb http://pub.evolix.org/evolix stretch main).

~~~
apt install evoadmin-mail
~~~

Récupérer l'ancienne configuration dans `/home/evoadmin-mail/www/htdocs/config/conf.php` et mettre à jour `/etc/evoadmin-mail/config.ini` (notamment `$conf['admin']['mail']` et `$conf['admin']['logins']`).

### Apache

Si evoadmin-mail utilise un vhost Apache, il faut changer le **DocumentRoot** et rajouter un **SetEnv** pour indiquer l'emplacement du fichier de configuration dans `/etc/apache2/sites-enabled/evoadmin-mail.conf` :

~~~
DocumentRoot /usr/share/evoadmin-mail/
SetEnv EVOADMINMAIL_CONFIG_FILE /etc/evoadmin-mail/config.ini
~~~

### Nginx

Si evoadmin-mail utilise un vhost Nginx, il faut changer le **root** dans `/etc/nginx/sites-enabled/evoadmin-mail` :

~~~
root /usr/share/evoadmin-mail/
~~~

Et rajouter une **SetEnv** dans `/etc/php5/fpm/pool.d/evoadmin-mail.conf` :

~~~
env["EVOADMINMAIL_CONFIG_FILE"] = /etc/evoadmin-mail/config.ini
~~~

## rbenv

Attention, une mise-à-jour majeure (tout comme une mise-à-jour majeure d'ailleurs) va casser les compilations (sauf coup de chance).

## sympa

Si l'on utilise Sympa avec PostgreSQL, [la mise à jour ne gère apparemment pas le schéma SQL](http://bugs.debian.org/782273). Il faudra recréer la table *session_table* :

~~~
$ psql -U sympa -h /var/run/postgresql sympadb
Password for user sympa: 

sympadb=> DROP TABLE session_table;
sympadb=> CREATE TABLE session_table (
           data_session   varchar(500),
           date_session   int4 NOT NULL,
           email_session  varchar(100),
           hit_session    int4,
           id_session     varchar(30) NOT NULL,
           prev_id_session        varchar(30),
           refresh_date_session   int4,
           remote_addr_session    varchar(60),
           robot_session  varchar(80),
           start_date_session     int4 NOT NULL,
           CONSTRAINT ind_session PRIMARY KEY (id_session) );
~~~

## ircd-hybrid

ircd-hybrid passe d'une version 7 à 8, sa configuration doit être refaite complètement à la main !
Par ailleurs, ircd-hybrid n'intègre plus le module *m_opme*, il faut envisager un autre serveur IRC comme ircd-ratbox ou charybdis.

## OpenLDAP

Attention, le format des données OpenLDAP change, et l'upgrade intègre cette migration.

Si cela dysfonctionne, vous pouvez faire cela à la main :

~~~
# systemctl stop slapd
# mv /var/lib/ldap /var/lib/ldap.bak
# db5.3_recover -eh /var/lib/ldap
# cp /usr/share/slapd/DB_CONFIG /var/lib/ldap
# slapadd -l /var/backups/slapd-2.4.31-2+deb7u2/*.ldif
# chown -R openldap: /var/lib/ldap
# systemctl start slapd
~~~

Et si besoin de zapper l'upgrade automatique car vous l'avez géré à la main, éditez `/var/lib/dpkg/info/slapd.postinst` et positionnez temporairement un `exit 0` pour la fonction _postinst_upgrade_configuration()_ avant de relancer votre `dist-upgrade` (pensez à remettre comme c'était après).

Si on a des erreurs de connexion a LDAP avec postfix ou dovecot par exemple, il faut vérifié les droits du fichier /etc/libnss-ldap.conf :

~~~
-rw-r--r-- 1 root root 130 Apr  3 12:16 /etc/libnss-ldap.conf
~~~

## NSS/PAM

En cas d'erreurs du type `pam_ldap: error trying to bind (Invalid credentials)` ou `sshd: Invalid user` pensez à vider le cache _nscd_.

## Asterisk

Le module "meetme" n'existe plus, il est remplacé par "confbridge"

## logrotate

Si vous avez une erreur du type : _zsyslog: missing '{' after log files definition_
c'est qu'il faut préciser une section du type :

~~~
/var/log/mail.log
{
    daily
    rotate 365
}
~~~

## Squid

~~~
015/06/16 06:44:02| WARNING: (B) '127.0.0.1' is a subnetwork of (A) '127.0.0.1'
2015/06/16 06:44:02| WARNING: because of this '127.0.0.1' is ignored to keep splay tree searching predictable
2015/06/16 06:44:02| WARNING: You should probably remove '127.0.0.1' from the ACL named 'localhost'
~~~

Il faut supprimer l'acl localhost dans la conf qui est présente dans le core.

## PHP

PHP 5.4 -> 5.6 :

* <http://php.net/manual/fr/migration55.php>
* <http://php.net/manual/fr/migration56.php>

## Si on doit installer certbot sur une machine qui a été migré en jessie, voici la procedure :

* <https://wiki.evolix.org/HowtoLetsEncrypt#mettre-%C3%A0-jour-certbot-en-debian-8-suite-a-larret-du-protocole-acmev1>
