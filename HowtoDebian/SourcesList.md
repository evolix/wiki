---
categories: web debian upgrade
title: Howto sources.list
...

* Statut de cette page : prod

Nous utilisons les `sources.list` suivants en fonction des versions majeures de Debian.
Assurez vous d’avoir les [clés PGP](#clés-pgp) associées aux dépôts externes ([Evolix](#clé-pgp-pour-evolix),
[eLTS](#clé-pgp-pour-elts), etc.)

## Trixie (13)

Actuelle version testing de Debian (WIP)

<details>
~~~
# cat /etc/apt/sources.list
# Empty file, we moved to deb822 format
# https://manpages.debian.org/trixie/apt/sources.list.5.en.html#DEB822-STYLE_FORMAT

# cat /etc/apt/sources.list.d/system.sources
Types: deb
URIs: http://mirror.evolix.org/debian
Suites: trixie trixie-updates
Components: main
Signed-by: /usr/share/keyrings/debian-archive-trixie-automatic.gpg

# cat /etc/apt/sources.list.d/security.sources
Types: deb
URIs: http://security.debian.org/debian-security
Suites: trixie-security
Components: main
Signed-by: /usr/share/keyrings/debian-archive-trixie-security-automatic.gpg

# cat /etc/apt/sources.list.d/evolix_public.sources
Types: deb
URIs: http://pub.evolix.org/evolix
Suites: trixie
Components: main
Signed-by: /usr/share/keyrings/pub_evolix.asc
~~~
</details>

## Bookworm (12)

À partir de Bookworm, nous utilisons le format [deb822](Deb822) au lieu
du format sur une ligne.

~~~
# cat /etc/apt/sources.list
# Empty file, we moved to deb822 format
# https://manpages.debian.org/bookworm/apt/sources.list.5.en.html#DEB822-STYLE_FORMAT

# cat /etc/apt/sources.list.d/system.sources
Types: deb
URIs: http://mirror.evolix.org/debian
Suites: bookworm bookworm-updates
Components: main
Signed-by: /usr/share/keyrings/debian-archive-bookworm-automatic.gpg

# cat /etc/apt/sources.list.d/security.sources
Types: deb
URIs: http://security.debian.org/debian-security
Suites: bookworm-security
Components: main
Signed-by: /usr/share/keyrings/debian-archive-bookworm-security-automatic.gpg

# cat /etc/apt/sources.list.d/evolix_public.sources
Types: deb
URIs: http://pub.evolix.org/evolix
Suites: bookworm
Components: main
Signed-by: /usr/share/keyrings/pub_evolix.asc
~~~

## Bullseye (11)

~~~
# cat /etc/apt/sources.list
deb http://mirror.evolix.org/debian bullseye main
deb http://mirror.evolix.org/debian bullseye-updates main
deb http://security.debian.org/debian-security bullseye-security main

# cat /etc/apt/sources.list.d/evolix_public.list
deb http://pub.evolix.org/evolix bullseye main
~~~

## Buster (10)

~~~
# cat /etc/apt/sources.list
deb http://archive.debian.org/debian buster main
deb http://security.debian.org/debian-security buster/updates main
deb http://elts.evolix.org/extended-lts buster-lts main

# cat /etc/apt/sources.list.d/evolix_public.list
deb http://pub.evolix.org/evolix buster main
~~~

## Stretch (9)

~~~
# cat /etc/apt/sources.list
deb http://archive.debian.org/debian stretch main
deb http://archive.debian.org/debian-security stretch/updates main
deb http://elts.evolix.org/extended-lts stretch-lts main

# cat /etc/apt/sources.list.d/evolix_public.list
deb http://pub.evolix.org/evolix stretch main
~~~

## Jessie (8)

*Remarque* : les deux premiers dépôts ont été commentés car la clé PGP utilisée pour signer ces dépôt est maintenant expirée,
mais les paquets fournis par ces dépôts sont également fournis par notre miroir du dépôt eLTS.

~~~
# cat /etc/apt/sources.list
# deb http://archive.debian.org/debian jessie main
# deb http://archive.debian.org/debian-security jessie/updates main
deb http://elts.evolix.org/extended-lts jessie main
deb http://elts.evolix.org/extended-lts jessie-lts main

# cat /etc/apt/sources.list.d/evolix_public.list
deb http://pub.evolix.net/ jessie/
~~~

## Wheezy (7)

*Remarque* : les clés PGP de ces dépôts sont expirées, vous aurez donc un warning du type `Do you want to ignore this warning and proceed anyway?` en installant des paquets.

~~~
# cat /etc/apt/sources.list
deb http://archive.debian.org/debian wheezy main
deb http://archive.debian.org/debian-security wheezy/updates main
deb http://deb.freexian.com/extended-lts wheezy-lts main

# cat /etc/apt/sources.list.d/evolix_public.list
deb http://pub.evolix.net/ wheezy/
~~~

## Squeeze (6)

*Remarque* : les clés PGP de ces dépôts sont expirées, vous aurez donc un warning du type `Do you want to ignore this warning and proceed anyway?` en installant des paquets.

~~~
# cat /etc/apt/sources.list
deb http://archive.debian.org/debian squeeze main
deb http://archive.debian.org/debian-security squeeze/updates main
deb http://deb.freexian.com/extended-lts squeeze-lts main

deb http://pub.evolix.net/ squeeze/
deb http://pub.evolix.net/ kernel/
~~~

## Clés PGP

### Clé PGP pour Evolix

#### pub.evolix.org

~~~
# wget https://pub.evolix.org/evolix/pool/main/e/evolix-archive-keyring/evolix-archive-keyring_0~2024_all.deb
# apt install ./evolix-archive-keyring_0~2024_all.deb
~~~

#### pub.evolix.net

~~~
# cd /etc/apt/trusted.gpg.d
# wget 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x0c016d3bd1195d30105837cc44975278b8612b5d' -O reg.asc
# chmod 644 reg.asc
~~~

### Clé PGP pour eLTS

~~~
# wget https://deb.freexian.com/extended-lts/archive-key.gpg -O /etc/apt/trusted.gpg.d/freexian-archive-extended-lts.gpg
# chmod 644 /etc/apt/trusted.gpg.d/freexian-archive-extended-lts.gpg
~~~

## FAQ

### Bookworm (12) ancien format

~~~
# cat /etc/apt/sources.list
deb http://mirror.evolix.org/debian bookworm main
deb http://mirror.evolix.org/debian bookworm-updates main
deb http://security.debian.org/debian-security bookworm-security main

# cat /etc/apt/sources.list.d/evolix_public.list
deb [signed-by=/usr/share/keyrings/pub_evolix.asc] http://pub.evolix.org/evolix bookworm main
~~~

### Components

On peut remplacer `main` par `main contrib non-free` pour avoir des paquets non libres distribués par Debian.

À partir de Debian 12, on peut aussi ajouter `non-free-firmware` pour avoir les firmwares non libres.
