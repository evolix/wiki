---
categories: system
title: Howto Debian backports
...

* Documentation : <https://backports.debian.org/Instructions/>

Une distribution stable de Debian garde en permanence (ou presque) les mêmes versions des logiciels (c'est le principe d'une version stable).
Certaines versions plus récentes sont néanmoins distribuées via des packages Debian de [backports](https://backports.debian.org).


## Utilisation

À partir de Debian 12, pour disposer des backports de Debian 13, on ajoutera le dépôt `bookworm-backports` suivant dans un fichier `/etc/apt/sources.list.d/backports.sources` :

~~~
Types: deb
URIs: http://mirror.evolix.org/debian
Suites: bookworm-backports
Components: main
Signed-by: /usr/share/keyrings/debian-archive-bookworm-automatic.gpg
~~~

Si besoin de `non-free`, l’ajouter à `Suites`.

~~~
Types: deb
URIs: http://mirror.evolix.org/debian
Suites: bookworm-backports
Components: main contrib non-free
Signed-by: /usr/share/keyrings/debian-archive-bookworm-automatic.gpg
~~~

Nous mettons de plus à disposition un dépôt avec quelques paquets rétroportés qui ne sont pas disponibles dans le dépôt officiel. Il est nécessaire d’avoir mis en place [la clef de notre dépôt](/HowtoDebian/SourcesList#clef-pgp-pour-evolix).

~~~
Types: deb
URIs: http://pub.evolix.org/evolix
Suites: bookworm-backports
Components: main
Signed-by: /usr/share/keyrings/pub_evolix.asc
~~~

> Pour Debian 11, le contenu de `/etc/apt/sources.list.d/backports.list` sera :
>
> ~~~
> deb http://mirror.evolix.org/debian bullseye-backports main
> ~~~
>
> Pour Debian 10, le contenu de `/etc/apt/sources.list.d/backports.list` sera :
>
> ~~~
> deb http://archive.debian.org/debian buster-backports main
> ~~~
>
> Pour Debian 9, le contenu de `/etc/apt/sources.list.d/backports.list` sera :
>
> ~~~
> deb http://archive.debian.org/debian/ stretch-backports main
> ~~~
>
> Pour Debian 8, le contenu de `/etc/apt/sources.list.d/backports.list` sera :
>
> ~~~
> deb http://archive.debian.org/debian/ jessie-backports main
> ~~~
>
> avec la commande suivante car les backports de Jessie et Stretch ne sont plus tenus à jour (il faut donc éviter de les utiliser) :
>
> ~~~
> # echo "Acquire::Check-Valid-Until no;" > /etc/apt/apt.conf.d/99no-check-valid-until
> ~~~

Depuis Squeeze, le dépôt des retroportages annonce « NotAutomatic: yes » and « ButAutomaticUpgrades: yes ». Cela signifie que les paquets de ces dépôts ne sont pas installés automatiquement (bien qu’ils aient un numéro de version supérieur à celui de la version de Debian utilisé), mais qu’ils sont mis à jour s’ils ont été installés depuis ce dépôt. Ainsi, il n’est pas nécessaire de configurer d’épinglage (« pinning APT »).

Installer un paquet de backports revient à forcer temporairement l’utilisation du dépôt (`apt install -t bookworm-backports <paquet> [...]`) ou forcer simplement la version du paquet (`apt install <paquet>/bookworm-backports [...]`).


## Récupérer le noyau Linux de la version suivante

Par exemple, pour installer le noyau de Trixie (Debian 13) dans Bookworm (Debian 12) :
 
`/etc/apt/sources.list.d/backports.sources` :

~~~
Types: deb
URIs: http://mirror.evolix.org/debian
Suites: bookworm-backports
Components: main
Signed-by: /usr/share/keyrings/debian-archive-bookworm-automatic.gpg
~~~

Si besoin de firmwares comme `firmware-bnx2` ou `firmware-bnx2x`, ajouter les composants `contrib` et `non-free` (on verra des erreurs à l'étape `update_initramfs` de la post-installation).

~~~
# apt update
# apt install linux-image-amd64/bookworm-backports
~~~

Note : Depuis Debian 6 (Squeeze), le pinning des paquets backportés n'est plus nécessaire.

> *Note* : pour faire joli, vous devez supprimer `firmware-iwlwifi` sur les serveurs


## Créer une image CD netinst de stretck avec un kernel backports (DRAFT)

~~~
# apt build-dep debian-installer
~~~

~~~
# dpkg-checkbuilddeps
~~~

~~~
$ apt-get source debian-installer
$ cd debian-installer-20170615+deb9u3/build
~~~

Éditer le fichier ./build/config/amd64.cfg:

~~~
KERNELVERSION = 4.15.0-0.bpo.2-amd64
KERNELMAJOR = 4.15
~~~

Éditer le fichier ./build/config/common:

~~~
LINUX_KERNEL_ABI ?= 4.15.0-0.bpo.2
DEBIAN_RELEASE = stretch
USE_UDEBS_FROM ?= stable
~~~

Éditer le fichier ./build/sources.list.udeb.local :

~~~
deb http://deb.debian.org/debian stretch main
deb http://deb.debian.org/debian stretch main/debian-installer
deb http://deb.debian.org/debian stretch-backports main/debian-installer
~~~

~~~
# make reallyclean
# fakeroot make build_netboot
[...]
install -m 644 -D ./tmp/netboot/mini.iso dest/netboot/mini.iso
update-manifest dest/netboot/mini.iso "tiny CD image that boots the netboot installer" ./tmp/netboot/udeb.list
~~~

Si tout se passe bien, vous obtiendrez un nouvel ISO:

~~~
$ file ./tmp/netboot/mini.iso dest/netboot/mini.iso
./tmp/netboot/mini.iso: DOS/MBR boot sector; partition 2 : ID=0xef, start-CHS (0x3ff,254,63), end-CHS (0x3ff,254,63), startsector 240, 832 sectors; partition 3 : ID=0x1, start-CHS (0x25,0,1), end-CHS (0x2a,63,32), startsector 75776, 12288 sectors
dest/netboot/mini.iso:  DOS/MBR boot sector; partition 2 : ID=0xef, start-CHS (0x3ff,254,63), end-CHS (0x3ff,254,63), startsector 240, 832 sectors; partition 3 : ID=0x1, start-CHS (0x25,0,1), end-CHS (0x2a,63,32), startsector 75776, 12288 sectors
~~~

Pour copier le fichier ISO sur une clé USB:

~~~
$ cat dest/netboot/mini.iso > /dev/sdX
$ fdisk -l /dev/sdX
Disque /dev/sdX : 14,6 GiB, 15664676864 octets, 30595072 secteurs
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : dos
Identifiant de disque : 0x0421e6d7
~~~
