---
categories: debian system
title: Howto Debian packages
...

* dpkg(1) : <https://manpages.debian.org/stable/dpkg/dpkg.1.en.html>
* apt-get(1) : <https://manpages.debian.org/stable/apt/apt-get.8.en.html>
* apt-cache(1) : <https://manpages.debian.org/stable/apt/apt-cache.8.en.html>

Debian est pourvu du meilleur format de packages : le `.deb` qui se manipule avec l'outil `dpkg`.
Debian possède le programme de gestion de packages le plus avancé du monde : APT (Advanced Packaging Tool).

## Format .deb

Un package `.deb` est une archive au format **ar** qui contient un fichier `debian-binary` et 2 tarballs : `data.tar` (arborescence de fichiers à installer) et `control.tar` (scripts et meta-informations sur le package).

~~~
$ ar x apt_1.6.4_amd64.deb
$ ls -l
-rw-r--r-- 1 gcolpart evolix    6896 août  24 23:19 control.tar.xz
-rw-r--r-- 1 gcolpart evolix 1305248 août  24 23:19 data.tar.xz
-rw-r--r-- 1 gcolpart evolix       4 août  24 23:19 debian-binary

$ tar xvf control.tar.xz
./conffiles
./control
./md5sums
./postinst
./postrm
./preinst
./prerm
./shlibs
./triggers

$ tar xvf control.tar.xz
./etc/
./etc/apt/
./etc/apt/apt.conf.d/
./etc/apt/apt.conf.d/01autoremove
[…]
./usr/
./usr/bin/
./usr/bin/apt
./usr/bin/apt-cache
./usr/bin/apt-cdrom
./usr/bin/apt-config
./usr/bin/apt-get
./usr/bin/apt-key
./usr/bin/apt-mark
[…]
~~~

Pour avoir des informations sur un paquet `.deb` :

~~~
$ dpkg-deb -I apt_1.6.4_amd64.deb 
 nouveau paquet Debian, version 2.0.
 taille 1312336 octets : archive de contrôle=6896 octets.
     121 octets,     4 lignes      conffiles            
    1412 octets,    27 lignes      control              
   11573 octets,   166 lignes      md5sums              
    5042 octets,   137 lignes   *  postinst             #!/bin/sh
    1370 octets,    45 lignes   *  postrm               #!/bin/sh
     254 octets,     6 lignes   *  preinst              #!/bin/sh
     485 octets,    11 lignes   *  prerm                #!/bin/sh
      23 octets,     1 lignes      shlibs               
      67 octets,     2 lignes      triggers             
 Package: apt
 Version: 1.6.4
 Architecture: amd64
[…]

$ dpkg-deb -c apt_1.6.4_amd64.deb
drwxr-xr-x root/root         0 2018-08-20 17:38 ./
drwxr-xr-x root/root         0 2018-08-20 17:38 ./etc/
drwxr-xr-x root/root         0 2018-08-20 17:38 ./etc/apt/
drwxr-xr-x root/root         0 2018-08-20 17:38 ./etc/apt/apt.conf.d/
-rw-r--r-- root/root       861 2018-08-20 17:38 ./etc/apt/apt.conf.d/01autoremove
[…]
~~~

## dpkg

`dpkg` est un outil bas niveau pour manipuler des packages Debian :

Installer un package

~~~
# dpkg -i foo.deb
~~~

Dépaqueter un package sans le configurer

~~~
# dpkg --unpack foo.deb
~~~

Configurer un package non configuré

~~~
# dpkg --configure foo
~~~

Re-configurer un package déjà configuré

~~~
# dpkg-reconfigure foo
~~~

Re-configurer un package déjà configuré avec un maximum de questions

~~~
# dpkg-reconfigure -plow foo
~~~

Supprimer un package

~~~
# dpkg -r foo
~~~

Purger un package supprimé

~~~
# dpkg -P foo
~~~

`dpkg` permet aussi d'avoir quelques informations de base :

Lister les packages installés de façon « conviviale »

~~~
$ dpkg -l
$ dpkg -l $mypackage
~~~

Lister les packages installés de façon exhaustive

~~~
$ dpkg --get-selections
~~~

Installer une liste de packages générée par "dpkg --get-selections"

~~~
# dpkg --merge-avail < apt-cache-dumpavail.txt
# dpkg --set-selections < dpkg-get-selections.txt
# apt dselect-upgrade
~~~

Lister les fichiers d'un package installé

~~~
$ dpkg -L foo
~~~

Lister les packages installé qui contiennent un fichier

~~~
$ dpkg -S bin/bar
~~~

Comparer 2 versions de package en utilisant l'algorithme de dpkg

~~~
$ dpkg --compare-versions 2:1-1 gt-nl 2:1-1~0 && echo OK
~~~
Les opérateurs sont :

* `lt` : inférieur ("less than")
* `le` : inférieur ou égal ("less than or equal")
* `eq` : égal ("equal")
* `ne` : différent ("not equal")
* `ge` : supérieur ou égal ("greater than or equal")
* `gt` : supérieur ("greater than")

Les variantes `lt-nl`, `le-nl`, `ge-nl`, `gt-nl` considèrent qu'une valeur nulle est supérieure.


## apt

Des ensembles de packages `.deb` sont regroupés sur un site HTTP, un CD-ROM, une clé USB, etc.
Le programme APT permet de gérer ces ensembles de packages grâce à un algorithme de dépendances.

Le plus classique est d'utiliser des dépôts (sites HTTP/HTTPS) qui mettent à disposition des paquets.
On les définit via le fichier `/etc/apt/sources.list` ou des fichiers `/etc/apt/sources.list.d/*.list` avec des lignes du type :

~~~
deb http://pub.evolix.org/evolix buster main
~~~

On récupère la liste de ces packagess avec une certain nombre d'informations (versions, description, etc.) via un fichier `Packages.gz` en faisant la commande :

~~~
# apt update
~~~

On peut ensuite manipuler ces listes de packages téléchargées avec la commande `apt-cache` :

Donner des informations sur un package 

~~~
$ apt-cache show foo
~~~

Rechercher les packages dont le nom ou la description courte contient foo

~~~
$ apt-cache search foo
~~~

Donner les dépendances d'un package

~~~
$ apt-cache depends foo
~~~

Lister les paquets qui dépendent d'un paquet (reverse dependencies) :

~~~
$ apt-cache rdepends $mypackage
$ apt-cache --installed rdepends $mypackage  # lister seulement ceux qui sont installés
~~~

Et l'on peut installer/supprimer/etc. des paquets avec la commande `apt` :

Installer un package

~~~
# apt install foo
~~~

Installer la dernière version d'un package spécifique, seulement s'il est déjà présent :

~~~
# apt install --only-upgrade foo
~~~

Supprimer un package

~~~
# apt remove foo
~~~

Supprimer et purger un package

~~~
# apt purge foo
~~~

Mettre à jour tous les packages installés (sauf si cela installe/supprime d'autres packages)

~~~
# apt upgrade
~~~

Mettre à jour tous les packages installés (même si cela installe/supprime d'autres packages)

~~~
# apt dist-upgrade
~~~

Nettoyer les packages téléchargés du cache local

~~~
# apt clean
~~~

Lister des packages pouvant être mis au jour (inclus les package en hold) :

~~~
$ apt list --upgradable
~~~

On peut utiliser d'autres commandes comme `apt-get` ou `aptitude` qui offrent des options différentes :

Lister des packages pouvant être mis au jour (plus précis que `apt list --upgradable`)

~~~
$ aptitude upgrade -sV
~~~

Installer les dépendances pour recompiler un package

~~~
$ apt-get build-dep foo
~~~


### Paquets « immobilisés » (hold / unhold)

Apt permet de figer la version d'un paquet et d'empêcher sa mise-à-jour. Ce mécanisme est appelé « hold » :

~~~
# apt-mark hold foo
foo passé en figé (« hold »).

# apt-mark showhold
foo

# dpkg -l foo
hi foo […]
~~~

Pour « rendre sa liberté » au paquet (unhold) :

~~~
# apt-mark unhold foo
~~~


### Paquets installés manuellement ou automatiquement

Pour savoir si un paquet a été installé manuellement ou automatiquement (dépendance) :

~~~
$ apt-mark showauto
$ apt-mark showauto $mypackage

$ apt-mark showmanual
$ apt-mark showmanual $mypackage
~~~

La sortie de `apt search $mypackage` affiche également l'information entre crochets.


### Préférences

<https://manpages.debian.org/stable/apt/apt_preferences.5.fr.html>

Par défaut, les packages situés sur un dépôt ont une priorité de 500.
Sauf le dépôt des Backports qui a une priorité de 100.

Ces priorités sont utilisées pour gérer plusieurs dépôts.
On peut afficher les priorités des dépôts avec la commande :

~~~
$ apt-cache policy

 100 /var/lib/dpkg/status
     release a=now
 100 http://mirror.evolix.org/debian buster-backports/main amd64 Packages
     release o=Debian Backports,a=buster-backports,n=buster-backports,l=Debian Backports,c=main,b=amd64
     origin mirror.evolix.org
 500 http://pub.evolix.org/evolix buster/main amd64 Packages
     release v=10,o=Evolix Public Repository,a=buster,n=buster,l=Evolix Public Repository,c=main,b=amd64
     origin pub.evolix.org
 500 http://security.debian.org/debian-security buster/updates/non-free amd64 Packages
     release v=9,o=Debian,a=stable,n=buster,l=Debian-Security,c=non-free,b=amd64
     origin security.debian.org
[…]
~~~

On peut réaliser du _pinning APT_ en modifiant la priorité des packages via le fichier `/etc/apt/preferences` ou des fichiers `/etc/apt/preferences.d/*`.

Voici quelques informations utiles :

* avec la priorité -1 un package n'est jamais installé
* de 1 à 99, le package sera installé uniquement si aucune version n'est déjà installée
* de 100 à 499, le package sera installé/mis à jour SAUF si il existe une version dans un autre dépôt avec priorité supérieure à 500
* de 500 à 989, le package sera installé (ou mis à jour si version plus récente) => CAS STANDARD
* de 990 à 999, le package sera installé (ou mis à jour si version plus récente) même si une version par défaut a été définie (`APT::Default-Release`)
* au dessus de 1000, le package sera installé ou mis à jour même si sa version est inférieure à celle installée

### Ajout de dépôt

L'ajout d'un dépôt non officiel est déconseillé (même les backports) car les mises à jour de sécurité sont incertaines et ne sont pas garanties pour durer jusqu'à la fin de vie de votre version de Debian.

On peut ajouter un nouveau dépôt en créant un fichier avec l'extension `.list`, par exemple `/etc/apt/sources.list.d/foo.list`

~~~
deb http://foo.example.com/ buster
~~~

Et il faut ajouter la clé GPG qui signe le dépôt dans le répertoire `/etc/apt/trusted.gpg.d/`, soit dans un fichier avec l'extension `.gpg` (clé sous format binaire), soit dans un fichier avec l'extension `.asc` (clé sous format ASCII). Par exemple :

~~~
# wget https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc -O /etc/apt/trusted.gpg.d/postgresql.asc
# dos2unix /etc/apt/trusted.gpg.d/postgresql.asc
# chmod 644 /etc/apt/trusted.gpg.d/postgresql.asc
~~~

> *Note* : pour les fichiers `.asc` il est important qu'ils aient des sauts de ligne Unix et non DOS… sinon ils seront ignorés. On conseille d'utiliser `dos2unix` pour s'en assurer.

Pour mettre en place le dépôt backports, voir [Backports]()

### apt-key

On peut lister les clés GPG présentes dans l'éventuel keyring `/etc/apt/trusted.gpg` ET les keyrings secondaires du répertoire `/etc/apt/trusted.gpg.d/` :

~~~
# apt-key list

etc/apt/trusted.gpg
--------------------
pub   rsa4096 ...

/etc/apt/trusted.gpg.d/debian-archive-buster-automatic.gpg
----------------------------------------------------------
pub   rsa4096 ...
~~~

Pour des raisons pratiques, nous déconseillons d'ajouter des clés dans le keyring « principal » `/etc/apt/trusted.gpg`.

On préfèrera ajouter des clés dans le répertoire `/etc/apt/trusted.gpg.d/` : soit dans un fichier avec l'extension `.gpg` (clé sous format binaire), soit dans un fichier avec l'extension `.asc` (clé sous format ASCII)

> *Note* : pour les fichiers `.asc` il est important qu'ils aient des sauts de ligne Unix et non DOS… sinon ils seront ignorés. On conseille d'utiliser `dos2unix` pour s'en assurer.

On peut supprimer des clés via la commande `apt-key del` suivi des derniers caractères du fingerprint de la clé :

~~~
# apt-key del 12345678
OK
~~~


## Astuces

### Lister les packages selon leur pool

Lister les packages installés, notamment leur pool pour différencier main/contrib/non-free :

~~~
$ dpkg --get-selections | tr '\t' ' ' | cut -d" " -f1 | cut -d: -f1 | xargs apt-cache show | grep ^Filename
~~~

### Lister les packages par taille

~~~
$ dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n
~~~

### Unattended upgrades / Auto upgrades

Sur un serveur non critique (backup, dev, …) on peut vouloir faire les mises à jour automatiquement :

~~~
# apt install unattended-upgrades
# dpkg-reconfigure -plow unattended-upgrades
~~~

La configuration se trouve dans le fichier `/etc/apt/apt.conf.d/50unattended-upgrades`.
On peut choisir où envoyer le mail de rapport ou encore si on doit redémarrer la machine automatiquement si nécessaire, etc.

Par exemple, pour mettre à jour Debian 11, on active ceci :

~~~
Unattended-Upgrade::Origins-Pattern {
        "origin=Debian,codename=${distro_codename},label=Debian";
        "origin=Debian,codename=${distro_codename},label=Debian-Security";
        "origin=Debian,codename=${distro_codename}-security,label=Debian-Security";
};
~~~

Il est possible d'automatiser le redémarrage du système (sans demande de confirmation !) en cas de besoin (présence du fichier `/var/run/reboot-required`, généralement provoqué par la disponibilité d'un nouveau noyau) :

~~~
Unattended-Upgrade::Automatic-Reboot "true";
~~~

Il est possible également d'exclure un ou plusieurs paquets de la mise à jour automatique. Par exemple, pour exclure tous les paquets contenant `firefox` dans le nom :

~~~
// Python regular expressions, matching packages to exclude from upgrading
Unattended-Upgrade::Package-Blacklist {
    "firefox";
};
~~~

### Accepter les changements d'informations de release

Si un dépôt change ses données de "release" (version, nom de code…), APT va demander confirmation explicite. C'est lié à `apt-secure`.

Ça peut par exemple prendre cette forme :

~~~
E: Repository 'http://pub.evolix.net bullseye/ Release' changed its 'Codename' value from '' to 'bullseye/'
N: This must be accepted explicitly before updates for this repository can be applied. See apt-secure(8) manpage for details.
Do you want to accept these changes and continue updating from this repository? [y/N] 
~~~

En mode interactif, on peut accepter ou refuser le changement, mais en mode non-interactif (script, Ansible…) ça n'est pas possible.

On peut alors jouer cette commande pour accepter un changement d'information :

~~~
# apt update --allow-releaseinfo-change
~~~

De manière plus ciblée, on peut utiliser cette commande pour accepter un changement de nom de code uniquement :

~~~
# apt update -o Acquire::AllowReleaseInfoChange::Codename=true
~~~

Cette dernière option (`Acquire::AllowReleaseInfoChange::Codename=true`) peut aussi être ajoutée à un fichier de `/etc/apt/apt.conf.d/`.

### Modifier le contenu d'un .deb avec 'ar'

Par exemple pour modifier le fichier `control` d'un paquet `foo.deb`.

On extrait le contenu et on modifie le fichier :

~~~
$ ar x foo.deb
$ tar xvf control.tar.xz
$ vi control
~~~

On regénère le `control.tar.xz` avec le control modifié :

~~~
$ rm control.tar.xz
$ tar cvf control.tar control md5sums
~~~

On liste le contenu du `foo.deb` et on remplace son `control.tar.xz` :

~~~
$ ar t foo.deb
$ ar r foo.deb control.tar.xz
$ ar t foo.deb
~~~

Et voilà !

Pour info, quelques commandes utiles avec `ar` :

~~~
$ ar d foo.deb control.tar.xz // supprimer un fichier
$ ar help                     // liste des commandes
~~~

### Installation en mode debug

Passer debconf en mode debug :

~~~
DEBCONF_DEBUG=developer
export DEBCONF_DEBUG
~~~

Utiliser `dpkg -D` par exemple `dpkg -i -D73773` pour un debug maximum.

Voici les options possibles :

~~~
$ dpkg -Dh
dpkg option de débogage, --debug=<octal> or -D<octal>:

 Nombre  Réf. ds source   Description
      1  general          Informations de progression en général utiles
      2  scripts          Lancement et état des scripts de maintenance
     10  eachfile         Affichage de chaque fichier traité
    100  eachfiledetail   Affichage verbeux pour chaque fichier traité
     20  conff            Affichage pour chaque fichier de configuration
    200  conffdetail      Affichage verbeux pour chaque fichier de configuration
     40  depcon           Dépendances et conflits
    400  depcondetail     Affichage verbeux pour les dépendances et conflits
  10000  triggers         Activation et traitement des actions différées
  20000  triggersdetail   Affichage verbeux pour les actions différées
  40000  triggersstupid   Affichage délirant pour les actions différées
   1000  veryverbose      Beaucoup de bruit à propos p.ex. du répertoire dpkg/info
   2000  stupidlyverbose  Bruit délirant (NdT : n'importe quoi)

Les options de débogage peuvent être combinées avec un « or » (bitwise-or).
Noter que les valeurs et leur signification peuvent changer.
~~~

