## Howto Installer Debian

Nous avons installé des centaines de serveurs Debian passés en production.
Nous listons ici certaines modifications appliquées dans une optique d'optimisation et de sécurisation.
Attention, ces modifications ne sont pas forcément applicables dans le cas général !

### Avant d'installer

Divers conseils :

* Vérifiez la mémoire à l'aide de l'outil _memtest86_
* Valider le bon fonctionnement de la machine avec un live-CD comme Knoppix
* Sauvegarder le MBR et la table des partitions
* Sauvegarder les éventuelles partitions contenant des utilitaires !
* Tester les disques durs avec un outil comme _badblocks_

### Installation

Via CD-ROM ou PXE, quelques commentaires sur nos choix :

* Choisir _anglais/us_ pour la langue, notamment pour avoir des messages d'erreur en anglais et faciliter la recherche dans les moteurs de recherche
* Nous choisissions le miroir Debian _mirror.evolix.org_ hébergé par nos amis de l'Ecole Centrale de Marseille
* Le partitionnement est le grand moment d'une installation. Entre les habitudes et recettes de grand-mère, voici nos préconisations :

~~~
Sauf cas particuliers, optez pour 3 partitions primaires :

200 M pour /boot
500 M pour /
10 G pour /var

Ensuite, des partitions logiques :

5 G pour /usr
500 M pour /tmp
500 M pour swap
500 M pour swap
~~~

 Si nécessaire (pour des logs web par exemple) créer une partition /var/log de 10 G

 Si nécessaire pour des bases de données, créer une partition en LVM afin de bénéficier des snapshots pour une sauvegarde optimisée

 L'espace restant (en prenant l'habitude de garder 5 à 10 G d'espace libre) sera réservé pour /home de type LVM si besoin de flexibilité
* Le système de fichiers à choisir est _ext4_ sauf rares exceptions.
* Ne choisir d'installer aucun des meta-package proposé (décocher tout).

### Configuration post-install 

Voici nos préconisations post-installation :

* Régler le niveau de debconf à _low_ :

~~~
# dpkg-reconfigure -plow debconf
~~~

* Ajuster le contenu du fichier [`sources.list` suivant la version](/HowtoDebian/SourcesList).

Puis ne pas oublier d'exécuter :

~~~
# apt update
~~~

* Désactiver l'installation automatiques des paquets recommandés en ajoutant dans le fichier _/root/.aptitude/config_ :

~~~
# aptitude "";
aptitude::Suggests-Important "false";
aptitude::Recommends-Important "false";
~~~

* Si vous choississez de monter les partitions avec des permissions particulières (par exemple "read-only" pour _/usr_ et "noexec" pour _/tmp_), on peut automatiser les changements et afficher un avertissement, via le fichier _/etc/apt/apt.conf_ ou dans un fichier personnalisé placé dans _/etc/apt/apt.conf.d/_ :

~~~
DPkg {
    // Warn the user about the auto remount of partitions.
    Pre-Invoke {
       "echo 'Remounting partitions. Are you sure? Else exit with ^C'; read REPLY; mount -oremount,exec /tmp && mount -oremount,rw /usr";
    };
    Post-Invoke {
        "mount -oremount /tmp && mount -oremount /usr || exit 0";
    };
}
~~~

* Installation de paquets ou mises-à-jour en pensant à bien regarder les paquets "recommandés" lors d'une installation…
* Installer Postfix, le meilleur serveur mail du monde :

~~~
# aptitude install postfix
~~~

 Postfix sera configuré en en fonction de l'utilisation du serveur
* Installer un certain nombre de paquets indispensables (voir les dépendances du meta-package _serveur-base_ hébergé sur le repository Evolix,
ceci ne fonctionnera que si vous l'avez activé dans le _sources.list_) :

~~~
# aptitude install serveur-base ssh
~~~

 Note : les paquets distribués par Evolix ne sont pas signés donc ne pas tenir compte des erreurs "untrusted packages"

 Note : pour _apt-listchanges_ choisir "pager" puis "both" et d'envoyer sur l'adresse de votre choix
* S'assurer que les services inutiles sont désactivés. Ceci n'est plus nécessaire depuis les dernières versions de Debian, mais par réflexe, on fera :

~~~
# aptitude purge portmap hotplug nfs-common pidentd
# update-inetd --disable discard,daytime,time,ident
~~~

* On choisira l'éditeur par défaut du système (Vim étant le meilleur éditeur du monde...) :

~~~
# update-alternatives --config editor
~~~

* Configurer les alias mail :

~~~
# getent passwd | cut -d":" -f1 | sed "s/$/: root/" > /etc/aliases
~~~

  On ajustera ensuite les mails pour "root" vers l'adresse mail de votre choix.
  Et on pourra ajouter les alias suivants : postmaster, abuse, et mailer-daemon.
  Puis :

~~~
# newaliases
~~~

* Ajuster le fichier _/etc/fstab_ suivant les restrictions nécessaires. Par exemple :

~~~
proc            /proc           proc    defaults        0       0
/dev/sda1       /boot           ext'    defaults        0       2
/dev/sda2       /               ext4    defaults,errors=remount-ro 0       1
/dev/sda3       /var            ext4    defaults        0       2
/dev/sda5       /usr            ext4    defaults,ro     0       2
/dev/sda6       /tmp            ext4    defaults,noexec,nosuid,nodev       0       2
/dev/sda7       none            swap    sw              0       0
/dev/sda8       none            swap    sw              0       0
tmpfs           /var/tmp        tmpfs   defaults,noexec,nosuid,nodev   0       2
tmpfs           /var/lock       tmpfs   defaults,noexec,nosuid,nodev   0       2
/dev/mapper/myvol1-home /home   ext4    defaults,noexec,nosuid,nodev,usrquota,grpquota        0       2
~~~

 Note: notez que la partition /usr est montée en read-only par défaut ET que /var/tmp/ et /var/lock sont en TMPFS (ce qui signifie purgé à chaque reboot !!).
Ceci est complètement non-standard et contraire au FHS, donc n'appliquer cela que si vous savez ce que vous faites !

* Gestion des quotas. On supprimera le script _quotarpc_ inutile :

~~~
# update-rc.d -f quotarpc remove
~~~

 Si besoin de gérer les quotas, outre l'activation dans le _fstab_ :

~~~
# quotaoff -a
# quotacheck -auvg
# quotaon -auvg
# edquota -t
~~~

 Dans un environnement professionel, nous conseillons de mettre la période de grâce à 30 jours minimum.
* Gestion des ACL si besoin. Outre l'installation du paquet _acl_, on ajoutera l'option _acl_ dans le _fstab_ pour la partition concernée (probablement /home)
* Changer les heures par défaut dans le fichier /etc/crontab afin d'éviter des surcharges divers à des moments donnés. Par exemple, ainsi :

~~~
hourly à Xh13
daily à 1h23
weekly à 3h33
monthly à 4h37
~~~

* Par défaut, les journaux système sont gérés par _rsyslog_. Sa configuration devra être ajustée via le fichier _/etc/rsyslog.conf_ pour notamment activer les logs de cron ou encore désactiver l'écriture en double de certains journaux.
* Pour la rotation des logs système, depuis la dernière version, tout est géré via logrotate.d (et non plus savelog). Attention, les paramètres par défaut conservent très peu de temps certains journaux (quelques semaines voire quelques jours...) et il convient de les ajuster pour les conserver pendant un an (ni plus ni moins). D'autres ajustements peuvent être faits pour dater les fichiers de logs via des actions _postrotate_ de logrotate.
* Configuration de Munin via `/etc/munin/munin.conf`, notamment le hostname.

 Si le noyau est patché _grsec_, il faut ajouter les lignes suivantes dans le fichier /etc/munin/plugin-conf.d/munin-node :

~~~
[processes]
user root
~~~

* Pour avoir une machine toujours à l'heure, on utilise _ntp_ avec le fichier _/etc/ntp.conf_ :

~~~
server ntp.evolix.net
~~~

* Droits des utilisateurs
* /etc/profile	Définir umask 027 ou umask 077 en fonction de l'utilisation du serveur (gestion de groupes ou non)
* /root/.bashrc	Définir umask 077, les fichiers créés par root ne doivent pas être visibles par les utilisateurs !
* /etc/sudoers	Voir plus bas la partie sur _sudo_
* /root	        Mettre les droits 0700
* /usr/share/scripts	Mettre les droits 0700 (répertoire spécifique aux scripts d'Evolix)
* /etc/adduser.conf  Régler DIR_MODE
* Pour gérer les accès _root_, utiliser le logiciel _sudo_, avec une configuration du type :

~~~
# visudo

Defaultsenv_reset, umask=0077
User_Alias ADMIN = pnom1, pnom2, pnom3
ADMIN   ALL=(ALL) ALL
~~~

* Configurer SSH en interdisant l'accès _root_ direct dans le fichier sshd_config :

~~~
PermitRootLogin no
~~~

 Il est également conseillé d'utiliser la directive AllowUsers pour restreindre l'accès à certains utilisateurs et certaines adresses IP :

~~~
AllowUsers pnom1@1.2.3.4 pnom1@::ffff:1.2.3.4
~~~

* Soyons parano, on peut restreindre l'accès root à la 5e console ainsi :

~~~
# echo tty5 > /etc/securetty
~~~

* Installer _minifirewall_. Cela consiste à télécharger les fichiers (principalement _minifirewall_ et _firewall.rc_) à partir du repository GIT <http://git.evolix.org/?p=evolinux/minifirewall.git;a=summary> et les placer respectivement dans _/etc/init.d/_ et _/etc_. Il faut ensuite procéder aux ajustements dans le fichier _firewall.rc_
* Créer le fichier /etc/init.d/alert5 suivant :

~~~
#!/bin/sh
date | mail -s'boot/reboot' mail@example.com
/etc/init.d/minifirewall start
~~~

 Et activer tout cela :

~~~
# chmod +x /etc/init.d/alert5 /etc/init.d/minifirewall
# update-rc.d alert5 start 99 2 .
~~~

* Pour diverses raisons, on peut limiter le temps de connexion en plaçant une ligne _export TMOUT=3600_ dans le fichier /etc/profile.
* Enfin, il faut ajuster les droits les moins permissifs possibles. On utilisera par exemple :

~~~
# find /etc/ -perm -004
~~~

* De la même façon au niveau réseau, on pourra tester les ports ouverts, par exemple avec l'une des commandes suivantes :

~~~
# netstat -a -u -t -n -p
# lsof -i
# time  nmap -sS -O -T Insane -p 0- YOUR-IP
~~~

* Script *evomaintenance* : pour une administration à plusieurs, nous utilisons un script qui permet d'informer à chaque intervention et de retenir ces informations dans une base de données. Voici comment nous l'utilisons :

~~~
# aptitude install evomaintenance
~~~

 Puis ajuster le fichier de configuration _/etc/evomaintenance.cf_ et autoriser ce script à se lancer sans mot de passe pour les administrateurs via sudo. On ajoutera donc dans le suoders :

~~~
Cmnd_Alias MAINT = /usr/share/scripts/evomaintenance.sh
ADMIN ALL=NOPASSWD: MAINT
~~~

 Note : la règle finale doit être ajoutée à la fin pour avoir plus d'importance que les règles déjà en place.

 Pour l'utilisation, les administrateurs placeront dans leur fichier .profile ou .bashrc :

~~~
trap "sudo /usr/share/scripts/evomaintenance.sh" 0
~~~

* Si l'on est sur une machine virtuelle (VM), on remplace le kernel par défaut par un kernel « Cloud » :

~~~
# apt install linux-image-cloud-amd64
# apt remove linux-image-amd64
# reboot
~~~
