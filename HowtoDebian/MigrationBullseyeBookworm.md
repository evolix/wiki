---
categories: web debian upgrade bookworm
title: Howto Migration Debian 11 (Bullseye) vers Debian 12 (Bookworm)
...

Release Notes amd64 : <https://www.debian.org/releases/bookworm/amd64/release-notes/>


## Debian Bookworm

Debian Bookworm _aka Debian 12_ est la dernière version majeure de Debian publiée. Dans les changements notables de la distribution, on note :

* Apache : 2.4.X →  2.4.Y (suivi des versions amont)
* Serveur DNS BIND : 9.16 → 9.18
* Cryptsetup : 2.3 → 2.6.1
* Dovecot MTA : 2.3.13 → 2.3.19
* GCC : 10.2 → 12.2
* GnuPG : 2.2.20 → 2.2.40
* Bibliothèque C GNU : 2.31 → 2.36
* Image du noyau Linux : Série 5.10 → série 6.1
* MariaDB : 10.5 → 10.11
* Nginx : 1.18 → 1.22
* OpenJDK : 11 → 17
* OpenSSH : 8.4p1 → 9.2p1
* Perl : 5.32 → 5.36
* PHP : 7.4 → 8.2
* Postfix MTA : 3.5 → 3.7
* PostgreSQL : 13 → 15
* Python 3 : 3.9.1 → 3.11.2
* Samba : 4.13 → 4.17
* Tomcat : 9.0.43 → 9.0.70 ou 10.1.X (suivi des versions amont)
* Vim : 8.2 → 9.0
* LibreOffice : 7.0 → 7.4
* Scribus : 1.5.6 → 1.5.8


## Actions préalables

Nous conseillons quelques actions qui aideront en cas de problème.

Si le script Evolix `dump-server-state` est disponible, exécutez le avant la mise à jour :

~~~
# dump-server-state --verbose --force --dump-dir /home/backup/bullseye-$(date +%s) --all
~~~

Sinon, vous pouvez faire certaines opérations manuellement

Sauvegarder localement certaines ressources (dans `/home/backup/bullseye` par exemple)

~~~
# cat before-upgrade.sh

cd /etc
git add .
git commit -am "Commit balai avant upgrade en Bookworm"
backup_dir="bullseye-$(date +%s)"
mkdir -p ${backup_dir}
cd ${backup_dir}
cp -r /etc ./
mkdir -p var/lib/apt
cp -r /var/lib/dpkg ./var/lib/
cp -r /var/lib/apt/extended_states ./var/lib/apt/
dpkg --get-selections "*" > ./current_packages.txt
uptime > uptime.txt
ps auwx > ps.out
pstree -pan > pstree.out
ss -tanpul > listen.out
if [ -f /root/.mysql_history ]; then cp -a /root/.mysql_history /root/.mysql_history.bak; fi
~~~


## Mise à jour du système

Éditer les dépôts dans `/etc/apt/sources.list` et `/etc/apt/sources.list.d/*.list` pour remplacer _bullseye_ par _bookworm_.

~~~
# sed --follow-symlinks --in-place 's/bullseye/bookworm/g' /etc/apt/sources.list /etc/apt/sources.list.d/*.list
~~~

Résultat dans les **fichiers [`sources.list` conseillés](SourcesList#bookworm-12)**.

S’il y a des backports... **les désactiver** car en général ils ne sont plus nécessaires !

Puis mettre à jour le cache APT avec la commande :

~~~
# apt update
~~~

Commencer par télécharger l'ensemble des paquets qui devront être installés (afin de limiter le temps effectif d'installation).

~~~
# apt full-upgrade --download-only
~~~

**Attention, si il y a des instances LXC, il faut les stopper avant la mise à niveau !**

Faire ensuite une mise à niveau sans nouveaux paquets, pour appliquer les mises à jour triviales :

~~~
# apt upgrade --without-new-pkgs
~~~

Faire ensuite une mise à niveau avec nouveaux paquets mais sans désinstallations :

~~~
# apt upgrade --with-new-pkgs
~~~

**TODO: à vérifier**

Attention, si MySQL/MariaDB est installé, il faut stopper les instances supplémentaires car non gérées lors de la mise à jour (et cela va casser avec *There is a MySQL server running, but we failed in our attempts to stop it*) :

~~~
# mysqld_multi stop
~~~

Ensuite, appliquer les mises à jour non triviales (nécessitant des changements de paquets dépendants, des suppressions…) afin d'avoir un œil plus précis sur ce qui sera fait, avant de valider :

~~~
# apt full-upgrade
~~~

Puis lancer le nettoyage des vieux paquets en lisant attentivement la liste… en cas de doute, réinstaller le paquet !

~~~
# apt autoremove
~~~

Enfin, il faut redémarrer sur le nouveau noyau Linux installé :

~~~
# reboot
~~~


## PATH / su

Lorsque l'on utilise `su` cela ne change plus automatiquement le PATH.
Soit il faut prendre l'habitude faire `su -`, soit il faut ajouter `ALWAYS_SET_PATH yes` dans le fichier `/etc/login.defs` (mais cela génère un message d'erreur pour un certain nombre d'applications...).

Pour plus d'informations, voir `/usr/share/doc/util-linux/NEWS.Debian.gz` ou <http://bugs.debian.org/918754>


## Nagios NRPE

### Checks pressure PSI

Activer les checks `pressure_cpu`, `pressure_io` et `pressure_mem` de Nagios, disponibles seulement à partir de Bookworm.


### Overwrite `/etc/nagios/nrpe.cfg` ?

`/etc/nagios/nrpe.cfg` peut être écrasé sans risque si les modifications sont celles faites pour l'installation de `monitoringctl` : checks commentés et/ou ajout de `alerts_wrapper` aux checks.

On peut le vérifier avec la commande :

~~~
cd /etc; git log -p /etc/nagios/nrpe.cfg
~~~


## MariaDB / MySQL

### `mysql_upgrade`

Normalement, le script postupgrade du paquet joue le script `mysql_upgrade` pour mettre a jour les tables, vues... suite à un upgrade majeur.
Cependant, si le datadir est volumineux, il le saute (c'est visible dans les logs MySQL) et il faut le faire à la main :

~~~
# mysql_upgrade
~~~

S'il y a plusieurs instances, il faut l'exécuter pour chacune avec le port en argument : `mysql_upgrade -p $port`


### Connexion à MySQL

Si des scripts (par exemple de backup) se connectent à MySQL via le port, il faut les mettre-à-jour.

Par exemple, remplacer `-P 3306` par `--protocol=socket -S /run/mysqld/mysqld.sock`.



### Historique

Note : La commande a été ajoutée ci-dessus dans la section « Actions préalables »

La bibliothèque qui gère l'historique a changé dans MariaDB 10.5, c'est libedit qui est utilisé désormais.
Ce changement fait que lors de la mise à jour de MariaDB 10.3 à 10.5, le fichier `.mysql_history` est effacé, on sauvegardera le fichier avant la mise à jour :

~~~
cp -a /root/.mysql_history /root/.mysql_history.bak
~~~ 


## PHP

Il faut parfois forcer le passage à PHP 8.2 :

~~~
# apt remove libapache2-mod-php7.4 && a2enmod php8.2 && /etc/init.d/apache2 restart
~~~

puis nettoyer les anciens paquets `php7.4-*`


### PHPMyAdmin

PHPMyAdmin a fait son retour dans les paquets Debian. Si vous l'aviez installé par les sources en Debian 10, vous pouvez le désinstaller et remettre le paquet.


## LXC

### LXC et AppArmor

**TODO: à vérifier, probablement toujours d'actualité**

Si des conteneurs LXC sont configurés, il faut installer `lxc-templates` et `apparmor` afin d'installer des configurations nécessaires à leur fonctionnement.

Et il faut mettre à jour la config de tous les conteneurs :

~~~
# lxc-update-config -c /var/lib/lxc/foo/config
~~~

AppArmor peut causer des problèmes de re-montage de devices dans les conteneurs LXC.
Pour le désactiver pour les conteneurs :

~~~
# vim /var/lib/lxc/$container_name/config
+ lxc.apparmor.profile = unconfined
# systemctl restart lxc
~~~


### LXC et cgroupv2

Sous Bullseye la version de la hiérarchie cgroup par défaut est passé à cgroupv2 au lieu de la hiérarchie hybride. 

Cela ne pose pas de problème en soi pour LXC, qui supporte cgroupv2 sous Bullseye **mais** cela peut provoquer des problèmes pour les conteneurs dont l'init dépend de cgroupv1 (tel que Debian 8), dans le cas où le support pour ces conteneurs est nécessaire il faut modifier la ligne de commande du noyau Linux **de l'hôte** en changeant `/etc/default/grub`:

```.diff
- GRUB_CMDLINE_LINUX=""
+ GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=false systemd.legacy_systemd_cgroup_controller=false"
```

puis régénérer la configuration de Grub :

```
grub-mkconfig -o /boot/grub/grub.cfg
```

(Ou simplement via `dpkg-reconfigure grub-pc`)


## PostgreSQL

**Attention, Il faut faire la mise à jour de version avec cette méthode, seulement si on utilise la version des dépôt Debian**

Il faut migrer manuellement, si on utilise la verions des dépôt PGDG.

**Cette migration entraine du downtime du serveur Postgresql, il faut vérifié aussi que on a le double de place dans la partition où se trouve le datadir**

~~~
# pg_upgradecluster -v 15 13 main -m dump
# pg_dropcluster 13 main
~~~


## usrmerge

Les installations fraîches de Debian Bookworm (ainsi que Bullseye et Buster) ont un `/usr` fusionné.  
Les répertoires `/{bin,sbin,lib}/` deviennent des liens symboliques vers `/usr/{bin,sbin,lib}/`.  
Si on veut faire la fusion il faut installer le paquet `usrmerge`.

```
# apt install usrmerge 
```


## Écosystème Ruby on Rails

**TODO: à vérifier, probablement toujours d'actualité**

On peut avoir des erreurs avec des gems.

~~~
bundle[3404]: LoadError: libmariadbclient.so.18: cannot open shared object file: No such file or directory - 
~~~

Il faut mettre à jour si le projet utilise bundle.

~~~
$ RAILS_ENV=production bundle install --redownload
~~~


## Logcheck

Dans bookworm, le nouveau comportement par défaut est de vérifier le journal.
On le désactive pour éviter que les mails de logcheck soient bloqués car trop volumineux en commentant la ligne `journal` dans `/etc/logcheck/logcheck.logfiles.d/journal.logfiles`.


## Squid

**TODO: à vérifier, probablement obsolète**

Il faut s'assurer d'avoir l'override systemd.

```
# systemctl edit squid
# systemd override for Squid
[Service]
ExecStart=
ExecStart=/usr/sbin/squid -sYC -f /etc/squid/evolinux-defaults.conf
# systemctl daemon-reload
# systemctl restart squid
```


## libvirt

**TODO: à vérifier, probablement toujours d'actualité**

~~~
Errors were encountered while processing:
 libvirt-daemon-system
~~~

Il faut stopper `virtlogd-admin.socket` puis relancer l'upgrade.

~~~
# systemctl stop virtlogd-admin.socket
# apt install -f
~~~


## Postfix

Il faut assurer que `smtpd_relay_restrictions` soit présent dans `/etc/postfix/main.cf`.
Si l'option n'est pas présente, ajouter :

~~~
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
~~~


## ProFTPD

**TODO: à vérifier, probablement obsolète**

La version 1.3.6 a un bug pour lister les répertoires avec plus de 10k fichiers :
https://github.com/proftpd/proftpd/issues/863
Un contournement est de désactiver le "cachefs" de ProFTPD comme suggéré ici :
http://bugs.proftpd.org/show_bug.cgi?id=4360#c14


## Bind

En cas de souci de redémarrage de Bind, voir : <https://wiki.evolix.org/HowtoBind#probl%C3%A8mes-de-permissions-avec-chroot>


## Shelldap

Pour que `shelldap` demande la saisie du mot de passe, il est nécessaire de modifier la configuration (généralement `~/.shelldap.rc`), en ajoutant la ligne suivante :

~~~
[…]
promptpass: 1
[…]
~~~


## ldapsearch (ldap-utils)

Les commandes du package `ldap-utils` comme `ldapsearch`, `ldapadd` etc. n'accepte plus l'option `-h SERVEUR` !
Il faut remplacer par `-H ldap://SERVEUR` (ou quelque chose approchant).


## bkctld

`bkctld` est la partie serveur de l'outil de backup [Evobackup](https://gitea.evolix.org/evolix/evobackup) développé par Evolix.

Les version de `bkctld` antérieures à la 2.12.0 peuvent être désinstallées par la mise à jour à cause de dépendances non satisfaites.
Il suffit de réinstaller [la dernière version de bkctld](https://gitea.evolix.org/evolix/evobackup/releases).


## Check_rabbitmq en erreur, module requests introuvable
 
Le check rabbitmq que nous utilisons pour notamment le check icinga `rab_connection_count` est en python2, et certains module python2 ne sont plus disponible en debian 11 :

~~~
Traceback (most recent call last):
  File "/usr/local/lib/nagios/plugins/check_rabbitmq", line 6, in <module>
    import requests
ImportError: No module named requests
~~~

Pour le check `rab_connection_count` nous n'avons pas besoin des appels au modules request, on peux donc commenter le module dans le script `/usr/local/lib/nagios/plugins/check_rabbitmq``:

~~~
-import requests
+#import requests
~~~

## Nettoyage post-upgrade

Certains paquets (comme `phpX.Y-*`) ne soient pas supprimés par `apt autoremove` alors qu'ils ne sont plus dans les dépôts Debian. Lister et supprimer ces paquets avec les commandes suivantes, **attention aux dépendances** :

~~~
apt list '~o'
apt remove '~o'
# Ou pour supprimer toutes les traces :
apt purge '~o'
~~~
