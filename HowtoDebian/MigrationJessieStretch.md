---
categories: web debian upgrade
title: Howto Migration Debian 8 (Jessie) vers Debian 9 (Stretch)
...

Release Notes amd64 : <http://www.debian.org/releases/stretch/amd64/release-notes/>

## Actions préalables

Sauvegardons localement certaines ressources en cas de problème dans `/home/backup/jessie` :

~~~
# cat before-upgrade.sh
#!/bin/bash

set -x

cd /etc
git add .
git commit -am "Commit balai avant upgrade en Stretch"
mkdir -p /home/backup/jessie/
cd /home/backup/jessie
cp -r /etc ./
mkdir -p var/lib/apt
cp -r /var/lib/dpkg ./var/lib/
cp -r /var/lib/apt/extended_states ./var/lib/apt/
dpkg --get-selections "*" > ./current_packages.txt
uptime > uptime.txt
ps auwx > ps.out
pstree -pan > pstree.out
ss -tanpul > listen.out
netstat -laputen > netstat.out
{ /sbin/iptables -L -n -v; /sbin/iptables -t filter -L -n -v; } > iptables.txt
~~~


## Mise à jour du système

Éditer les dépôts pour remplacer _jessie_ par _stretch_. Nous avons ici par exemple:

Résultat dans les **fichiers [`sources.list` conseillés](SourcesList#stretch-9)**.

S'ils sont présent, **désactiver les backports** car plus nécessaire.

> **Note** : Si apt ne vous propose pas de mise à jour, c'est probablement due à un pinning qui désactive la release stretch.

S'il y a ce dépôt, faire:

~~~
# cat /etc/apt/sources.list.d/levert.list
deb http://hwraid.le-vert.net/debian stretch main
~~~

Puis mettre à jour le cache APT:

~~~
# apt update
~~~

Commencer par télécharger l'ensemble des paquets qui devront être installés (afin de limiter le temps effectif d'installation).

~~~
# apt dist-upgrade --download-only
~~~

Avant de poursuivre, essayons de voir si le service gérant les unités fonctionnent et peuvent être mis à jour.

Tout d'abord on vérifie l'état de systemd :

~~~
# systemctl status
# systemctl --failed
# ps -p 1 www
~~~

Si le process n'est pas dans un état normal, il est conseillé de redémarrer le système avant de poursuivre.


Vérifions également que les commandes `insserv` ne se bloquent pas avec le service `mailgraph` que l'on install partout :

~~~
# insserv mailgraph
~~~

Il faut vérifier que le process ne se bloque pas avec `ps auwwwx |grep insserv`. Si c'est le cas, il est conseillé de redémarrer le système avant de poursuivre.

Appliquer les mises à jour triviales :

~~~
# apt upgrade
~~~

Attention, s'il y a Mysql/MariaDB, il faut créer l'override de l'unité systemd **avant** :

~~~
# mkdir -p /etc/systemd/system/mariadb.service.d/
# wget "https://gitea.evolix.org/evolix/ansible-roles/raw/branch/stable/mysql/templates/mariadb.systemd.j2" -O /etc/systemd/system/mariadb.service.d/evolinux.conf
# systemctl daemon-reload
~~~

Il faut également stopper les instances Mysql/MariaDB supplémentaires car non gérées lors de l'upgrade (et cela va casser avec *There is a MySQL server running, but we failed in our attempts to stop it*) :

~~~
# mysqld_multi stop
~~~

Ensuite, appliquer les mises à jour non triviales (nécessitant des changements de paquets dépendants, des suppressions…) afin d'avoir un œil plus précis sur ce qui sera fait, avant de valider :

~~~
# apt dist-upgrade
~~~

Puis lancer le nettoyage des vieux packages en lisant attentivement la liste… en cas de doute, réinstaller le paquet !

~~~
# apt remove
# apt install acl php7.0-soap php-soap php-net-url php-net-dime php-http-request etc.
~~~

Enfin, il faut redémarrer sur le nouveau noyau Linux installé :

~~~
# reboot
~~~

Si nécessaire, on peut forcer le reboot ainsi :

~~~
# systemctl --force reboot
~~~

## APT

APT utilise maintenant l'utilisateur *_apt* pour certaines de ses fonctionnalités (ex. téléchargement des paquets), les dossiers `/etc/apt/` et `/var/cache/apt/` doivent donc être en *755* :

~~~
# chmod 755 /etc/apt /var/cache/apt
~~~

Les fichiers `/etc/apt/apt.conf.d/10evolix` et `/etc/apt/apt.conf.d/80evolinux` ont été concaténés en un seul fichier `/etc/apt/apt.conf.d/z-evolinux.conf` :

~~~
# cat /etc/apt/apt.conf.d/10evolix /etc/apt/apt.conf.d/80evolinux >/etc/apt/apt.conf.d/z-evolinux.conf
# rm /etc/apt/apt.conf.d/10evolix /etc/apt/apt.conf.d/80evolinux
# cat /etc/apt/apt.conf.d/z-evolinux.conf
APT::Install-Recommends "0";
APT::Install-Suggests "0";
DPkg {
    Pre-Invoke {
        "mount -oremount,exec /tmp && mount -oremount,rw /usr || true";
    };
    Post-Invoke {
        "mount -oremount /tmp && mount -oremount /usr || exit 0";
    };
}
~~~

`apt-listchanges` et `aptitude`ne sont plus utilisés, ils doivent être désinstallé.

En cas d'erreur :

~~~
W: There is no public key available for the following key IDs:
112695A0E562B32A
~~~

Un contournement (salé) est d'ajouter la clé ainsi :

~~~
# cd /etc/apt/trusted.gpg.d
# wget 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x112695A0E562B32A' -O debian10.asc
# chmod 644 debian10.asc
~~~

À noter que lors de l'upgrade `/bin/sh /var/lib/dpkg/info/apt.postinst configure 1.0.9.8.6` peut prendre beaucoup de temps (plusieurs minutes), il faut patienter.

## PHP

Il faut forcer le passage à PHP 7.0 :

~~~
# apt remove libapache2-mod-php5 php5-common && a2enmod php7.0 && /etc/init.d/apache2 restart
~~~

Puis nettoyer les anciens paquets php5-* et réinstaller les paquets PHP 7.0 correspondants.

Il faut également reprendre les directives de configuration de PHP 5.6 et les reporter en PHP 7.0 : memory_limit, max_*, etc.

Au niveau du code PHP, certaines fonctions n'existent plus du tout en PHP, il faut donc ré-écrire votre code, par exemple :
- remplacer la fonction `split()` par `explode()` ou `str_split()`

## Apache

La configuration spécifique d'Apache est maintenant faite dans les 2 fichiers _/etc/apache2/conf-available/z-evolinux-defaults.conf_ et _/etc/apache2/conf-available/zzz-evolinux-custom.conf_. Les fichiers de configuration standards tel que _apache.conf_ doivent donc être ceux de l'_upstream_ et les modifications éventuelles ajoutées dans _/etc/apache2/conf-available/zzz-evolinux-custom.conf_.

### mod_proxy_wstunnel

Le comportement du module **mod_proxy_wstunnel** semble avoir changé, avant il supportait une sorte de compatibilité HTTP/WS,
désormais il renverra une erreur `AH01144: No protocol handler was valid for the URL /foo/bar. If you are using a DSO version of mod_proxy, make sure the proxy submodules are included in the configuration using LoadModule.`si on lui parle en HTTP, cf <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=880195>

Le contournement est d'adapter la réponse en fonction des types de requêtes, du genre :

~~~
RewriteCond %{REQUEST_URI}  ^/socket.io            [NC]
RewriteCond %{QUERY_STRING} transport=websocket    [NC]
RewriteRule /(.*)           ws://127.0.0.1:8080/$1 [P,L]

RewriteCond %{REQUEST_URI}  ^/socket.io [NC]
RewriteRule /(.*)           http://127.0.0.1:8080/$1 [P,L]

RewriteCond %{REQUEST_URI}  ^/foo/bar [NC]
RewriteRule /(.*)           http://127.0.0.1:8080/$1 [P,L]

RewriteRule /(.*)           ws://127.0.0.1:8080/$1 [P,L]
~~~

## PHPMyAdmin

Il faut rajouter /usr/share/php/php-php-gettext a la directive open_basedir de /etc/phpmyadmin/apache.conf.

## Certbot

[Mise-à-jour de certbot vers Debian 9](https://wiki.evolix.org/HowtoLetsEncrypt#mise-%C3%A0-jour-de-certbot-vers-debian-9)

## Xorg

`startx` ou `startxfce4` donne : `parse_vt_settings /dev/tty0 (permission denied)`

Un contournement : `startxfce4 -- :1 vt1`

## Vim

par défaut, si pas de `~/.vimrc` Vim charge plein de features moisies (mouse, etc.)

décommenter dans `/etc/vim/vimrc` : `let g:skip_defaults_vim = 1`

## log2mail

La configuration de `log2mail` est maintenant répartie dans plusieurs fichier dépendamment des services surveillés : _/etc/log2mail/config/mysql.conf_, _/etc/log2mail/config/apache_ et _/etc/log2mail/config/squid.conf_

`log2mail` est lancé par une unité systemd et non plus par un script sysV init, à mettre dans _/etc/systemd/system/log2mail.service_.

## sudo

L'attribution des droits sudo des utilisateurs admin se fait à travers le groupe _evolinux-sudo_ (à créer). Le fichier _/etc/sudoers.d/evolinux_ est à mettre à jour en conséquence.

Les utilisateurs admin sont aussi à ajouter au groupe _adm_.

## MySQL/MariaDB

MariaDB est la version utilisée par défaut au lieu de Oracle MySQL qui n'est plus du tout inclus (il est possible d'avoir Oracle MySQL 5.7 via un repository extérieur mais on déconseille sauf si complètement bloquant).

La migration va conserver un fichier `/etc/mysql/my.cnf.migrated` (`/etc/mysql/my.cnf` est un lien symbolique vers `/etc/alternatives/my.cnf` lui-même un lien symbolique vers `/etc/mysql/my.cnf.migrated`). 

ATTENTION : `/etc/mysql/mariadb.conf.d/*` n'est pas pris en compte avec cette ancienne configuration ! Il ne faut pas déplacer les fichiers de configuration et les laisser dans `/etc/mysql/mariadb.conf.d/*`?

Et attention, les fichiers de configuration doivent être en 644, donc il est conseillé de faire :

~~~
# chmod 644 /etc/mysql/conf.d/*.cnf
~~~

Il faut avoir un override de l'unité systemd :

~~~
# mkdir -p /etc/systemd/system/mariadb.service.d/
# wget "https://forge.evolix.org/projects/ansible-roles/repository/revisions/stable/raw/mysql/templates/mariadb.systemd.j2" -O /etc/systemd/system/mariadb.service.d/evolinux.conf
# systemctl daemon-reload
~~~
> **Note** : C'est très important, notamment si vous avez un paramétrage qui utilise un chemin dans /home.

Attention, le moteur FEDERATED n'est plus activé par défaut (l'option `federated` dans la configuration doit ainsi être retiré).
Si on veut l'activer, voir <https://wiki.evolix.org/HowtoMySQL/Troubleshooting#storage-engine-federated>

La directive *log_slow_queries* n'est désormais plus du tout supportée : il faut utiliser obligatoirement *slow_query_log_file*.

Après la mise à jour, bien penser regarder les logs MySQL, il est parfois nécessaire de faire manuellement un `mysql_upgrade` si c'est indiqué dans les logs.

### Instances MySQL/MariaDB

Si vous utilisez des instances, il est désormais obligatoire d'avoir une directive *log_error* du type :

~~~
log_error = /var/log/mysqld1/error.log
~~~

## PHP

La configuration spécifique doit être faite dans les 2 fichiers de configuration _/etc/php/7.0/cli/conf.d/z-evolinux-defaults.ini_ et _/etc/php/7.0/cli/conf.d/zzz-evolinux-custom.ini_.

Pour récupérer les anciennes configurations des pool FPM, il suffit simplement de les copier et de redémarrer le service `php7.0-fpm`.

~~~
# cp /etc/php5/fpm/pool.d/* /etc/php/7.0/fpm/pool.d/
~~~

Les modules PHP ne sont pas automatiquement mis a jour car les noms ne sont plus les mêmes (ex. php5-gd => php-gd).
Il faut donc installer les nouveaux modules a la main : `apt install php-gd`.

## Squid

La configuration est éclatée dans plusieurs fichier de configuration : _/etc/squid/evolinux-defaults.conf_, _/etc/squid/evolinux-defaults.conf_, _/etc/squid/evolinux-acl.conf_, _/etc/squid/evolinux-httpaccess.conf_ et _/etc/squid/evolinux-custom.conf_. La liste blanche des sites se trouve dans _/etc/squid/evolinux-whitelist-defaults.conf_ et _/etc/squid/evolinux-whitelist-custom.conf_.

Il faut mettre à jour le fichier _/etc/default/squid_ pour que la directive _CONFIG_ pointe sur _/etc/squid/evolinux-defaults.conf_.

Le fichier _squid.conf_ n'est plus utilisé du tout.

## OpenDKIM

Le fichier `/etc/default/opendkim` n'est désormais plus pris en compte
puisqu'OpenDKIM a son unité systemd. Les éventuelles directives présentent dans
ce fichier, et notamment la socket, doivent se retrouver dans
`/etc/opendkim.conf` :

Par exemple, `SOCKET="inet:8889@127.0.0.1"` dans le fichier _default_ devient
`Socket inet:8889@[127.0.0.1]` dans le fichier de conf.

Pour les autres options, voir `opendkim.conf(5)`.

L'option `PidFile` doit obligatoirement être présente dans la conf (ce n'est
pas le cas par défaut), car l'unité systemd telle que fournie par Debian
s'attend à ce que le fichier `/var/run/opendkim/opendkim.pid` soit présent.
Autrement un `systemctl start opendkim` (ou équivalent sysV init) reste bloqué
et finit par générer un timeout, bien que le process soit bien lancé :

```
PidFile /var/run/opendkim/opendkim.pid
```

## Xchat -> hexchat

Xchat2 est remplacé par hexchat.
Pour conserver la configuration, **avant** de lancer hexchat, faire quelques manipulations du type :

~~~
$ cp -pr .xchat2/ .config/hexchat/
$ mv .config/hexchat/xchat.conf .config/hexchat/hexchat.conf
$ mv .config/hexchat/xchat2logs .config/hexchat/logs 
~~~

## Evoadmin-web

Pour mettre à jour vers la nouvelle version d'evoadmin-web, il faut se mettre sur la branche master et faire un git pull, avec l'utilisateur evoadmin:

~~~
$ su - evoadmin

evoadmin@hostname:~$ git checkout master
evoadmin@hostname:~$ git pull
~~~

Ensuite il faut ajouter un utilisateur, en éditant le fichier */home/evoadmin/www/conf/config.local.php*, avec la configuration suivante :

~~~
$localconf['logins']['foo'] = 'd5d3c723fb82cb0078f399888af7820d02e52751bb64597a8c20ebaba8ba4303';
~~~

Il faut généré un sha256 pour le mot de passe avec la commande suivante :

~~~
$ echo -n password |sha256sum
~~~

## Evoadmin-mail

Si evoadmin-mail n'as pas été mis à jour pendant la migration de Wheezy vers Jessie => [https://wiki.evolix.org/HowtoDebian/MigrationWheezyJessie]()

## Fail2Ban

On passe de Fail2Ban 0.8 à 0.9, et il y a un certain nombre de changements :

- la jail _ssh_ est renommée en _sshd_ : **attention, cela peut avoir des conséquences importantes, il faut adapter sa configuration spécifique qui utilise cette jail !**
- pour cette jail concernant SSH, elle définissant `maxretry = 6`, ce n'est plus le cas donc cela change et revient au paramètre par défaut, à savoir `maxretry = 5` en Debian 9
- le paramètre par défaut `maxretry = 3` passe à `maxretry = 5`

Il est conseillé de repartir de la configuration `jail.conf` du nouveau paquet, sinon vous aurez des erreurs du type :

~~~
fail2ban-client[23033]: ERROR  Failed during configuration: While reading from '/etc/fail2ban/jail.conf' [line 119]: option 'port' in section 'pam-generic' already exists
~~~

## mytop

Installer le paquet *libconfig-inifiles-perl* pour éviter l'erreur :

~~~
Can't locate Config/IniFiles.pm in @INC (you may need to install the Config::IniFiles module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.24.1 /usr/local/share/perl/5.24.1 /usr/lib/x86_64-linux-gnu/perl5/5.24 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl/5.24 /usr/share/perl/5.24 /usr/local/lib/site_perl /usr/lib/x86_64-linux-gnu/perl-base) at /usr/bin/mytop line 18.
BEGIN failed--compilation aborted at /usr/bin/mytop line 18.
~~~

## lsb_release

Celui-ci est utilisé par quelques programmes (dont des modules LSB) qui ont besoin de connaitre la version source d'une distribution. Aujourd'hui, on utilise dans la majorité des cas le fichier /etc/debian_version a la place de /etc/lsb-release qui peut être supprimé.

## Passer tomcat 7 à 8

Avant la supression des anciennes versions, il faudra bien penser à vérifier qu'elles peuvent supporter les mises à jours.
La méthode a suivre est de recréer de nouvelles instances avec les bonnes versions pour que le client puisse tester son bon fonctionnement. Seulement après les bascules ont réalise les suppressions.

## roundcube

Lors de l'upgrade avec dbconfig / MySQL, vous risquez d'avoir une erreur :

~~~
mysqldump: Couldn't execute 'SHOW FUNCTION STATUS WHERE Db = 'roundcube'': Cannot load from mysql.proc. The table is probably corrupted (1728)
~~~

Il faudrait d'abord faire l'upgrade MySQL pour éviter cette erreur.

Lancer à nouveau l'upgrade des bases Roundcube n'est pas trivial, relancer le `postinst configure` ne semble pas parfait.

## Courier

Le paquet `courier-ldap` tente de lancer un démon `ldapaliasd` qui n'est pas nécessaire en général.

Le plus simple est de désactiver :

~~~
# systemctl disable courier-ldap
~~~

## systemd / journald

Le démon `systemd-journald` prend parfois beaucoup de mémoire.
Si vraiment nécessaire, il peut être killé et devrait se relancer tout seul :

~~~
ps auwx
root     21537  0.3 39.4 52060656 51986900 ?   Ss    2019 3304:55 /lib/systemd/systemd-journald

kill -9 21537

ps auwx
root     21537  0.3  0.0      0     0 ?        Rs    2019 3304:58 [systemd-journal]

ps auwx
root     23573  0.0  0.0  70668  8748 ?        Ss   09:05   0:00 /lib/systemd/systemd-journald
~~~



