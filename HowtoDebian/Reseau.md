# Howto Reseau sous Debian

## Configuration IP fixe

La configuration d'une interface réseau avec une IP fixée se passe dans
le fichier _/etc/network/interfaces_. Voici un exemple simple :

~~~
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
        address 192.0.43.10/24
        gateway 192.0.43.254
~~~

Pour l'IPv6 il faut rajouter :

~~~
iface eth0 inet6 static
         address 2001:db8::10/48
         gateway 2001:db8::254
~~~

Si besoin d'exécuter des commandes après l'activation de l'interface (classiquement
des ajouts de routes), on utilise l'option _up_. Par exemple :

~~~
auto eth0
iface eth0 inet static
        address 192.0.43.10/24
        gateway 192.0.43.254
        up /sbin/ip route add 192.0.42.0/24 via 192.0.43.242
~~~

## Configuration IP secondaire (alias)

Pour ajouter une IP secondaire (appelé aussi *alias*), toujours via _/etc/network/interfaces_ on fera :

~~~
auto eth0:0
iface eth0:0 inet static
        address 192.0.43.11/24
~~~

Note 1 : certains hébergeurs comme OVH préconisent une configuration un peu différente, voir [/ServeurOVH#ip-failover]()

Note 2 : dans certains cas particuliers (comme le besoin de préciser le nom de l'interface
dans une commande _iptables_) on souhaite avoir un nom d'interface virtuelle sans les ":".
Il faudra donc passer par la commande _ip_ (voir ci-dessous).

Mais tout cela se gère également dynamiquement via les commande _ifconfig_ et _ip_.
Quelques exemples :

~~~
# ifconfig eth0 add 192.0.43.11 netmask 255.255.255.0
# ip addr add 192.0.43.12/24 dev eth0
# ip addr add 192.0.43.13/24 dev eth0 label eth0:3
# ip addr del 192.0.43.12/24 dev eth0
~~~

Attention, avec la commande _ip_ le résultat ne sera pas visible avec _ifconfig_
si aucun label du type eth0:N n'est donné à l'interface ; on ne verra l'adresse qu'avec un :

~~~
# ip addr show
~~~

Notons que dans certains cas particuliers (comme le besoin de préciser le nom de l'interface
dans une commande _iptables_) on souhaite avoir un nom d'interface virtuelle sans les ":".
On utilisera donc simplement un label sans ":" :

~~~
# ip addr add 192.0.43.13/24 dev label eth00
# iptables -t nat -A POSTROUTING -o eth00 [...]
~~~

## Configuration serveur(s) DNS

Le fichier _/etc/hosts_ permet d'indiquer manuellement des enregistrements DNS, du type :

~~~
127.0.0.1 ad.doubleclick.net
~~~

Pour note, ces enregistrements sont par défaut prioritaires sur les réponses des serveurs DNS
(et pour rappel, les commandes dig et host n'utilisent PAS _/etc/hosts_).

La configuration du (ou des) serveur(s) DNS s'effectue dans le fichier _/etc/resolv.conf_.
Voici un exemple classique :

~~~
search example.com
nameserver 192.0.42.53
nameserver 192.0.43.53
options timeout:2 attempts:2
~~~

Pour des précisions sur les options possibles : _man resolv.conf_

Note : si l'on utilise Postfix, il faut le redémarrer suite à un changement dans _/etc/resolv.conf_ (car il est chrooté par dédaut et il gère sa propre copie dans /var/spool/postfix)

## Bridge

Pour créer un bridge :

~~~
# apt install bridge-utils
# brctl addbr br0
# brctl addif br0 eth0
# ifconfig eth0 up
~~~

Au niveau /etc/network/interfaces on aura ainsi :

~~~
auto eth0
iface eth0 inet manual

auto br0    
iface br0 inet manual
        bridge_ports eth0
~~~

Et l'on listera les infos d'un bridge via :

~~~
# brctl show
bridge name     bridge id               STP enabled     interfaces
br0            8000.001e0beac6c0       no               eth0
~~~

Pour avoir un bridge vide :

~~~
auto br0    
iface br0 inet manual
        bridge_ports none
~~~

Attention, si vous avez un bridge et que vous faites une [upgrade de Debian 10 vers 11](MigrationBusterBullseye#bridge), il est possible que l'adresse MAC associée au bridge change.
Pour éviter cela il faut fixer son adresse MAC actuelle via l'option `bridge_hw` :

~~~
auto br0
iface br0 inet manual
        bridge_ports eth0
        bridge_hw ab:cd:01:23:45:67
~~~

Plus d'infos sur [#991416](https://bugs.debian.org/991416)


## VLAN

~~~
# aptitude install vlan
# modprobe 8021q
~~~

Pour ajouter dans le VLAN 42

~~~
# vconfig add eth0 42
# ifconfig eth0.42
~~~

Le fichier /etc/network/interfaces :

~~~
# cat /etc/network/interfaces
auto eth0.42
iface eth0.42 inet static
        address 192.0.43.10/24
        gateway 192.0.43.254
~~~

~~~
# systemctl enable networking
# systemctl start networking
~~~

### Gestion des VLAN avec iproute

L'utilitaire ip (paquet iproute) permet aussi de gérer les VLAN.
Exemple :


~~~
# ip link add link eth0 name eth0.42 type vlan id 42
# ip addr add 192.168.1.1/24 dev eth0.42
# ip link set dev eth0.42 up
~~~

## Mélange bridge et VLAN (cas d'un KVM)

Pour une configuration KVM, où l'on souhaiterait avoir plusieurs bridges selon les VLANs, il faut en premier configurer l'interface physique enoX, puis une interface enoX.YY par VLAN, et un bridge par VLAN dans lequel sera ce même enoX.YY. L'IP du KVM doit être attribuée au bridge qui correspond au VLAN dans lequel ce KVM est. Par exemple, avec un KVM dont l'IP est dans le VLAN 2, et des VMs devant être dans les VLANs 2, 3 et 4 :

~~~
auto eno1
iface eno1 inet manual

# VLAN2
auto eno1.2
iface eno1.2 inet manual

## Bridge
auto bridge_vlan2
iface bridge_vlan2 inet static
    address 192.0.2.1/24
    gateway 192.0.2.254
    bridge_ports eno1.2

# VLAN 3
auto eno1.3
iface eno1.3 inet manual

## Bridge
auto bridge_vlan3
iface bridge_vlan3 inet manual
    bridge_ports eno1.3

# VLAN 4
auto eno1.4
iface eno1.4 inet manual

## Bridge
auto bridge_vlan4
iface bridge_vlan4 inet manual
    bridge_ports eno1.4
~~~

L'IP du KVM est 192.0.2.1 dans le VLAN2. Ensuite, le bridge correspondant au VLAN souhaité sera attribué à la VM.

## Bonding

Le bonding, ou l'agrégation de lien, permet d'agréger plusieurs liens physiques en un seul lien virtuel. Selon le mode utilisé, cela peut offrir une tolérance de panne, de l'équilibrage de charge (load-balancing), et/ou une augmentation de débit.

~~~
# apt install ifenslave
# modprobe bonding
~~~

Son implémentation sous Debian est documentée dans `/usr/share/doc/ifenslave/README.Debian.gz`.

Il existe plusieurs modes : `balance-rr` (round-robin), `active-backup`, `balance-xor`, `broadcast`, `802.3ad` (LACP), `balance-tlb` (Adaptive Transmit Load Balancing), et `balance-alb` (Adaptive Load Balancing). Chacun est numéroté de 0 à 6.

Les modes active-backup, ALB et TLB ne nécessitent pas de configuration particulière des switchs, alors que les autres modes oui.

### Mode ALB

Pour du bonding sans configuration du côté des switchs, nous utilisons le mode ALB (ou mode "6"), qui comprend les caractéristiques du mode TLB avec des suppléments.
En effet avec le mode TLB, seul le trafic sortant bénéficie du load-balancing entre les différentes interfaces, et le trafic entrant n'est reçue que sur une seule interface. Le mode ALB permet également le load-balancing sur le trafic entrant à l'aide de négociations ARP.

Dans ce mode, chacune des interfaces physiques faisant partie du bonding parent possède sa propre adresse MAC. Ainsi, les switchs uplink − qui ne sont pas au courant de ce bonding − ne verront pas de flapping d'adresses MAC.

Le débit total du serveur peut être augmenté lors de multiples connexions selon le nombre d'interfaces physiques, mais le débit d'une seule même connexion ne pourra pas dépasser celui d'une interface physique seule, dû au fonctionnement du load-balancing.

Ce mode pose problème lorsque du VRRP est nécessaire sur la machine. En effet, le driver du mode ALB modifie l'adresse MAC correspondant à l'IP utilisée selon l'interface de sortie (pour avoir une adresse MAC par interface, comme indiqué ci-dessus). Du coup, l'adresse MAC de type 00:00:5e:00:01:XX utilisée par VRRP et partagée par les machines membres VRRP n'est jamais vue par les voisins, et une bascule VRRP impliquera une coupure de l'IP le temps que le cache ARP des voisins expire.

Ce mode se configure ainsi :

~~~
# cat /etc/network/interfaces

auto eno1
iface eno1 inet manual

auto eno2
iface eno2 inet manual

auto bond0
iface bond0 inet static
    address 192.0.2.1/24
    gateway 192.0.2.254
    bond-mode balance-alb
    bond-slaves eno1 eno2
    bond-miimon 100
    bond-downdelay 200
    bond-updelay 200
~~~

L'état peut se vérifier dans `/proc/net/bonding/bond0`.

### Mode active-backup

Pour du bonding sans configuration du côté des switchs, nous utilisons également le mode active-backup (ou mode "1"), où seule une interface sur les 2 est utilisée activement. Ce mode permet une redondance, et est préférable au mode ALB lorsque du VRRP est nécessaire sur la machine.

Ce mode se configure ainsi :

~~~
# cat /etc/network/interfaces

auto eno1
iface eno1 inet manual

auto eno2
iface eno2 inet manual

auto bond0
iface bond0 inet static
    address 192.0.2.1/24
    gateway 192.0.2.254
    bond-mode active-backup
    bond-slaves eno1 eno2
    bond-miimon 100
    bond-downdelay 200
    bond-updelay 200
~~~

L'état peut se vérifier dans `/proc/net/bonding/bond0`, où l'interface actuellement active est visible.

L'interface active peut être changée à chaud, par exemple pour utiliser eno2 :

~~~
# ifenslave -c bond0 eno2
~~~

### Mode 802.3ad (LACP)

Pour du bonding avec une configuration du côté des switchs, nous utilisons le mode 802.3ad/LACP (ou mode "4"), qui est standardisé.

Pour la configuration côté switch, voir par exemple [/SwitchCisco#vpc-virtual-port-channel]() pour du VPC.

Ce mode se configure ainsi :

~~~
# cat /etc/network/interfaces

auto bond0
iface bond0 inet static
    address 192.0.2.1/24
    gateway 192.0.2.254
    bond-mode 802.3ad
    bond-slaves eno1 eno2
    bond-miimon 100
    bond-downdelay 200
    bond-updelay 200
~~~

L'état peut se vérifier dans `/proc/net/bonding/bond0`.

<span style="color:red">**Attention, ne pas oublier d'installer le paquet `ifenslave`.**</span>

#### Configuration pour un KVM

Pour une configuration KVM, où l'on souhaiterait avoir plusieurs bridges selon les VLANs, il faut en premier configurer l'interface bond0, puis une interface bond0.XX par VLAN, et un bridge par VLAN dans lequel sera ce même bond0.XX. L'IP du KVM doit être attribuée au bridge qui correspond au VLAN dans lequel ce KVM est. Par exemple, avec un KVM dont l'IP est dans le VLAN 2, et des VMs devant être dans les VLANs 2, 3 et 4 :

~~~
# Bond
auto bond0
iface bond0 inet manual
    bond-mode 802.3ad
    bond-slaves eno1 eno2
    bond-miimon 100
    bond-downdelay 200
    bond-updelay 200

# VLAN2
## Bond
auto bond0.2
iface bond0.2 inet manual

## Bridge
auto bridge_vlan2
iface bridge_vlan2 inet static
    address 192.0.2.1/24
    gateway 192.0.2.254
    bridge_ports bond0.2

# VLAN 3
## Bond
auto bond0.3
iface bond0.3 inet manual

## Bridge
auto bridge_vlan3
iface bridge_vlan3 inet manual
    bridge_ports bond0.3

# VLAN 4
## Bond
auto bond0.4
iface bond0.4 inet manual

## Bridge
auto bridge_vlan4
iface bridge_vlan4 inet manual
    bridge_ports bond0.4
~~~

L'IP du KVM est 192.0.2.1 dans le VLAN2. Ensuite, le bridge correspondant au VLAN souhaité sera attribué à la VM.

> Note : Attention à la configuration du bonding selon la version Debian, voir au-dessus.

<span style="color:red">**Attention, ne pas oublier d'installer le paquet `ifenslave`.**</span>

## Ajouter une IPv6

Pour configurer l'IPv6 sur une machine qui a déjà une IPv4 configurée :

~~~
# ip -6 addr add 2001:db8::100/48 dev eth0
# ip -6 route add default via 2001:db8::254
~~~

Puis, configurer dans `/etc/network/interfaces` :

~~~
iface eth0 inet6 static
        address 2001:db8::100/48
        gateway 2001:db8::254
~~~

## Désactiver IPv6

De façon temporaire :

~~~
# sysctl -w net.ipv6.conf.all.disable_ipv6=1
~~~

De façon définitive :

~~~
# echo net.ipv6.conf.all.disable_ipv6=1 >> /etc/sysctl.conf
~~~

et décommenter la ligne suivante dans `/etc/gai.conf` :

~~~
precedence ::ffff:0:0/96  100
~~~

## Changer son adresse MAC

~~~
# ifconfig eth0 hw ether 01:23:45:67:89:0a
~~~

## Configurer une route statique

Si l'on veut joindre un réseau en particulier qui n'est pas joignable à travers le routeur par défaut, il faut configurer une route statique.

Par exemple, "pour joindre le réseau 198.51.100.0/24, il faut passer par 192.0.2.200" se traduit par cette commande :

~~~
# ip route add 198.51.100.0/24 via 192.0.2.200
~~~

Pour rendre la route persistente, il faut l'ajouter dans `/etc/network/interfaces` sous l'interface qui correspond au réseau où se trouve la gateway :

~~~
auto eth0
iface eth0 inet static
        address 192.0.2.10/24
        gateway 192.0.2.254
        up /sbin/ip route add 198.51.100.0/24 via 192.0.2.200
~~~

## Utiliser une table de routage secondaire

Il est possible de configurer d'autres tables de routage, permettant par exemple d'utiliser plusieurs routes par défaut selon certaines conditions. Le paquet `iproute2` est nécessaire.

Un exemple de nécessité peut être dans le cas où un serveur possède 2 adresses IP différentes, dans 2 réseaux différents :

~~~
# ip --brief a
eth0             UP             192.0.2.1/24 
eth1             UP             198.51.100.1/24 
~~~

Disons que la route par défaut est 192.0.2.254 :

~~~
# ip r
default via 192.0.2.254 dev eth0
~~~

Alors tous les paquets sortiront par cette route par défaut, et cela peut être problématique si l'on souhaite joindre l'IP 198.51.100.1 dans l'autre réseau.

On peut alors créer une nouvelle table de routage en l'ajoutant dans `/etc/iproute2/rt_tables`. Ici, j'ajoute la table `secondfai` :

~~~
#
# reserved values
#
255	local
254	main
253	default
0	unspec
#
# local
#
#1	inr.ruhep
2	secondfai
~~~

On voit qu'il existe déjà plusieurs tables. La table `local` s'occupe du trafic devant rester local à la machine, ainsi que du broadcast. La table `main` est celle utilisée et affichée par défaut lorsqu'on ne précise pas de table. La table `default` est vide.

On va maintenant remplir cette nouvelle table de routage, en y spécifiant la route par défaut 198.51.100.254 du second réseau :

~~~
# ip route add default via 198.51.100.254 table secondfai
~~~

On peut consulter son contenu :

~~~
# ip route show table secondfai
default via 198.51.100.254 dev eth1
~~~

Il nous reste à spécifier dans quels cas utiliser cette table. Si l'on veut que tout le trafic que la machine émet depuis ce second réseau passe forcément par son routeur 198.51.100.254, on peut spécifier cette règle :

~~~
# ip rule add from 198.51.100.0/24 lookup secondfai prio 1000
# ip rule add to 198.51.100.0/24 lookup secondfai prio 1000
~~~

Ces règles se traduisent par « pour tout trafic dont la source est dans le réseau 198.51.100.0/24, je consulte la table `secondfai` ; pour tout trafic dont la destination est dans le réseau 198.51.100.0/24, je consulte la table `secondfai` », et leur priorité est de 1000. Plus la priorité sera basse, et plus la règle sera prioritaire.

Les règles en place peuvent être consultées de cette manière :

~~~
# ip rule
0:	from all lookup local 
1000:	from all to 198.51.100.0/24 lookup secondfai 
1000:	from 198.51.100.0/24 lookup secondfai 
32766:	from all lookup main 
32767:	from all lookup default 
~~~

Ainsi, tout le trafic que la machine émet depuis 192.0.2.1/24 sortira par la route par défaut de la table par défaut `main`, et tout le trafic que la machine émet depuis 198.51.100.1/24 ou à destination de 198.51.100.0/24 sortira par la route par défaut de la table secondaire `secondfai`.

Ne pas oublier d'ajouter cette configuration en dur pour qu'elle reste après un redémarrage du serveur, par exemple dans /etc/network/interfaces :

~~~
auto eth0
iface eth0 inet static
    address 192.0.2.1/24
    gateway 192.0.2.254

auto eth1
iface eth1 inet static
    address 198.51.100.1/24
    up /sbin/ip route add default via 198.51.100.254 table secondfai
    up /sbin/ip rule add from 198.51.100.0/24 lookup secondfai prio 1000
    up /sbin/ip rule add to 198.51.100.0/24 lookup secondfai prio 1000
~~~

Les règles peuvent être supprimées de la même façon qu'elles ont été ajoutées, en remplaçant le mot-clé `add` par `delete`.


## Migrer de networkd à ifup

À partir de Stretch, certaines machines sont installés d'office avec networkd (qui fait partie de systemd), on peut vouloir migrer à ifup (`/etc/network/interfaces`) pour plusieurs raisons (compatibilité, …).
Voici les opérations à faire :

- On récupère les informations avec `networkctl status nomInterface` et/ou `cat /etc/systemd/network/50-default.network` ;
- On construit le fichier `/etc/network/interfaces` avec les informations récupérés (se baser sur un existant par exemple) ;
~~~
networkctl status --all | sed 's/^/# /' >> /etc/network/interfaces
~~~
- On installe `ifupdown`
- On désactive `systemd-networkd` et on redémarre le service `networking` :

~~~
# systemctl disable --now systemd-networkd.{service,socket}; systemctl restart networking
~~~

Un exemple de conversion :

~~~
# networkctl status eno1
● 2: eno1
       Link File: /etc/systemd/network/50-public-interface.link
    Network File: /etc/systemd/network/50-default.network
            Type: ether
           State: routable (failed)
            Path: pci-0000:03:00.0
          Driver: igb
          Vendor: Intel Corporation
           Model: I210 Gigabit Network Connection
      HW Address: 00:01:02:03:04:05 (Intel Corporate)
         Address: 192.0.2.100
                  2001:db8::100
         Gateway: 192.0.2.254 (Cisco Systems, Inc)
                  fe80::12bd:18ff:ffff:ffff (Cisco Systems, Inc)
             DNS: 127.0.0.1
                  192.0.2.254
             NTP: ntp.ovh.net

# cat /etc/systemd/network/50-default.network
# This file sets the IP configuration of the primary (public) network device.
# You can also see this as "OSI Layer 3" config.
# It was created by the OVH installer, please be careful with modifications.
# Documentation: man systemd.network or https://www.freedesktop.org/software/systemd/man/systemd.network.html

[Match]
MACAddress=00:01:02:03:04:05

[Network]
Description=network interface on public network, with default route
DHCP=no
Address=192.0.2.100/24
Gateway=192.0.2.254
#IPv6AcceptRA=false
NTP=ntp.ovh.net
DNS=127.0.0.1
DNS=192.0.2.254
Gateway=2001:41d0:0002:f5ff:ff:ff:ff:ff

[Address]
Address=2001:41d0:0002:f55c::/64

[Route]
Destination=2001:41d0:0002:f5ff:ff:ff:ff:ff
Scope=link
~~~

On reporte dans `/etc/network/interfaces` :

~~~
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eno1
iface eno1 inet static
  address 192.0.2.100/24
  gateway 192.0.2.254

iface eno1 inet6 static
  address 2001:db8::100/64
  gateway 2001:41d0:0002:f5ff:ff:ff:ff:ff
  post-up /sbin/ip -6 route add 2001:41d0:0002:f5ff:ff:ff:ff:ff dev eno1
  post-up /sbin/ip -6 route add default via 2001:41d0:0002:f5ff:ff:ff:ff:ff
~~~

On désactive/stop networkd et on relance le service networking :

~~~
# systemctl stop systemd-networkd.service; systemctl disable systemd-networkd.service; systemctl restart networking
~~~

## Haute Performance

Si besoin de « haute performance », on appliquera la configuration [sysctl](https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt) suivante :

* `net.nf_conntrack_max=262144` (utiliser `conntrack -L` pour bien ajuster la valeur)
* `net.core.somaxconn=1024`
* `net.ipv4.tcp_max_syn_backlog=1024`
* `net.ipv4.tcp_tw_reuse=1`
* `net.ipv4.tcp_fin_timeout=30`
* `net.ipv4.ip_local_port_range=2000 65000`

Si cela ne suffit pas, voici quelques pistes pour aller plus loin :

* lire <https://vincent.bernat.ch/fr/blog/2014-tcp-time-wait-state-linux>
* lire <http://www.nateware.com/linux-network-tuning-for-2013.html> et notamment les options `net.core.*mem*`, `net.core.optmem_max`, `net.ipv4.tcp_*mem`, `net.core.netdev_max_backlog`, `net.ipv4.tcp_max_tw_buckets`
* augmenter le buffer de certains cartes réseau avec [ethtool]() : voir `ethtool -g eth0` et modifier avec `ethtool -G eth0 rx/rx-mini/rx-jumbo/tx NNNN`


## arp-sk

Dans certains cas, les routeurs gardent un cache ARP trop long, et il est pratique de les « empoisonner » pour le forcer à changer.
Cela peut être utilisé dans le cas d'un changement d'une adresse IP d'un serveur vers un autre sur le même réseau.

On utilise un vieil outil magique [arp-sk](http://sid.rstack.org/arp-sk/).
L'idée est de le compiler en static et de l'envoyer sur la machine avec la nouvelle IP :

~~~
# apt install libnet1-dev
$ cd /tmp
$ wget http://sid.rstack.org/arp-sk/files/arp-sk-0.0.16.tgz
$ tar xf arp-sk-0.0.16.tgz
$ cd arp-sk-0.0.16
$ ./configure LDFLAGS="-static"
$ make
$ scp src/arp-sk serveur.example:
~~~

puis sur ce serveur on spécifie l'IP déplacée (par exemple 192.0.2.42), l'IP du routeur (par exemple 192.0.2.254) et la nouvelle adresse MAC (par exemple 52:54:00:12:34:56) :

~~~
# /home/foo/arp-sk -r -S 192.0.2.42 -D 192.0.2.254 -s 52:54:00:12:34:56
...
~~~

## Serveur temporaire avec socat ou nc

Il arrive qu'on veuille vérifier qu'il est possible de joindre un port TCP au travers d'un firewall, alors que rien n'écoute sur ce port à ce moment là. On peut alors utiliser `socat(1)` ou `nc(1)` pour créer un serveur temporaire, qui imprimera sur la sortie standard ce qui arrive sur le port. Exemple sur le port 2121, en écoute sur toutes les interfaces du serveur :

~~~
# socat - TCP-LISTEN:2121,crlf
~~~

~~~
# nc -lk -p 2121
~~~

## Man-in-the-middle du pauvre

Lorsqu'on souhaite consulter ce qui passe en TCP entre 2 processus ou serveurs (par exemple entre HAProxy et Apache), pour analyser par exemple les en-têtes échangés… on peut utiliser tcpdump, mais cela n'est pas toujours aisé.
Il est possible d'intercaler `socat(1)` entre les 2 processus. Il fera le passe-plat mais nous indiquera aussi (sur la sortie standard) ce qui est échangé.

Ici on ouvre localement le port 8080 et on renvoie le flux vers le port 8080 de example.com.

~~~
# socat -v tcp-listen:8080,keepalive=1 tcp:example.com:8080
~~~

NB : cette commande socat n'est pas optimale, la connexion coupe après le premier échange, mais elle illustre le principe.

## FAQ

### Bascule réseau à chaud

Si l'on fait une modification réseau sur un serveur distant il est important de ne pas perdre la main.
Voici une méthode possible en utilisant [screen](HowtoScreen), exemple avec la bascule sur un bridge :

~~~
# cp /etc/network/interfaces /var/backups/
# vim /etc/network/interfaces
[...]

# screen -S network -dm bash -c "sleep 300; cp /var/backups/interfaces /etc/network/; systemctl stop networking; systemctl start networking"
# screen -S reboot -dm bash -c "sleep 600; reboot"
# systemctl restart networking; ip a d 192.0.2.42/24 dev eth0; ip a a 192.0.2.42/24 dev br0; ip r d default via 192.0.2.1 dev eth0; ip r a default via 192.0.2.1 dev br0
~~~

Ainsi, si l'on perd la main, la machine revient accessible en quelques minutes.
Si l'on ne perd pas la main, il faut stopper les commandes lancées dans les screens.


### Divers

<https://vincent.bernat.im/fr/blog/2014-tcp-time-wait-state-linux.html>

### Test des ports TCP

Pour tester l'ouverture d'un port TCP en sortie, on peut utiliser <http://portquiz.net/> qui écoute sur tous les ports TCP possibles :

~~~
$ telnet portquiz.net 42
Trying 52.47.209.216...
Connected to portquiz.net.
Escape character is '^]'.
~~~

