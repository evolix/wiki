**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Créer un média d'installation de Debian

Télécharger une ISO sur <http://cdimage.debian.org/debian-cd/current/amd64/iso-cd/>

## Via clé USB

Copier simplement vers le média *non partitionné* :

~~~
# cp debian-8.6.0-amd64-CD-1.iso /dev/sdb
# sync
~~~

# Créer un média Debian-Live

Télécharger une ISO sur <http://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/>

## Via clé USB

Copier simplement vers le média *non partitionné* :

~~~
# cp debian-live-8.6.0-amd64-standard.iso /dev/sdb
# sync
~~~

Infos Debian-Live :

* login # user, password live
* passer en azerty : apt install console-data
