---
title: Howto Debian: migration Lenny (5) -> Squeeze (6)
---

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Migration Lenny -> Squeeze

Release Notes i386 : <http://www.debian.org/releases/squeeze/i386/release-notes/>

Release Notes amd64 : <http://www.debian.org/releases/squeeze/amd64/release-notes/>

## VERSIONS

Passage de Lenny à Squeeze :

MySQL 5.0.51 -> 5.1.49

PHP 5.2.6 -> 5.3.3

Apache 2.2.9 -> 2.2.16

Tomcat 5.5.26->6.0.35


## APT

Fichiers [`sources.list` conseillés](SourcesList#squeeze-6).

* Bien vérifier les paquets proposés à la suppression !
* Dans certains cas particuliers, on pourra utiliser apt-get à la place d'aptitude, qui peut avoir une gestion différente... mais attention,
   les opérations avec apt-get ne seront donc pas loguées dans /var/log/aptitude !

## Munin

* Commenter toutes les lignes de /etc/apache2/conf.d/munin
* Modifier l'URL de domaine/machine.html vers domaine/machine/index.html
* S'assurer que Munin soit toujours accessible

## Apache

* Pour mod_ssl, il est désormais nécessaire d'avoir des directives SSLxxx dans chaque VH *:443 => il faut donc dupliquer ces directives dans les autres VHs...
* Bien s'assurer de la présence de la variable APACHE_LOG_DIR dans le fichier */etc/apache2/envvars* sous peine d'avoir des erreurs du type :

~~~
Starting web server: apache2Action 'start' failed.
The Apache error log may have more information.
 failed!

[error] (2)No such file or directory: could not open transfer log file /etc/apache2/${APACHE_LOG_DIR}/other_vhosts_access.log.
Unable to open logs
~~~

## PHP

* Le module mhash n'existe plus (intégré dans PHP) : mhash.ini à supprimer donc
* methode goto apparait cf <http://infogerance-linux.net/2012/php-5-2-goto-php-5-3/> 
* Les directives register_long_arrays et magic_quotes_gpc sont désormais deprecated : à voir avec le client si elles peuvent être supprimées.
* Les directives session_is_registered et session_register le sont egalement, elles peuvent cependant être remplacée facilement :

~~~{.php}
// if (!session_is_registered ("variable"))
// est remplacée par 
if (!isset($_SESSION["variable"]))

// session_register("variable");
// est remplacée par
$_SESSION["variable"] == TRUE;
~~~

* possibilité de repasser à PHP5.2 sous Squeeze, cf <http://infogerance-linux.net/2012/downgrader-php53/>
* L'affichage des warnings _PHP Deprecated_ peut être désactivé au niveau du virtualhost :

~~~
# E_ALL & ~E_STRICT & ~E_DEPRECATED
php_admin_value error_reporting 22527 
~~~

## MySQL

* L'option _skip-bdb_ n'existe plus, il faut la retirer du fichier _my.cnf_
* S'assurer que _bind-address_ est bien resté le même (debconf peut reposer la question là-dessus)
* Erreur _ERROR 1577 (HY000) at line 1: Cannot proceed because system tables used by Event Scheduler were found damaged at server start_ : dans ce cas, on peut supprimer la table _event_ qui sera recréée : _drop table mysql.event_ puis _mysql_upgrade --force_. Au pire, voici la table sur une Squeeze :

~~~
CREATE TABLE `event` (
  `db` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` char(64) NOT NULL DEFAULT '',
  `body` longblob NOT NULL,
  `definer` char(77) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `execute_at` datetime DEFAULT NULL,
  `interval_value` int(11) DEFAULT NULL,
  `interval_field` enum('YEAR','QUARTER','MONTH','DAY','HOUR','MINUTE','WEEK','SECOND','MICROSECOND','YEAR_MONTH','DAY_HOUR','DAY_MINUTE','DAY_SECOND','HOUR_MINUTE','HOUR_SECOND','MINUTE_SECOND','DAY_MICROSECOND','HOUR_MICROSECOND','MINUTE_MICROSECOND','SECOND_MICROSECOND') DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_executed` datetime DEFAULT NULL,
  `starts` datetime DEFAULT NULL,
  `ends` datetime DEFAULT NULL,
  `status` enum('ENABLED','DISABLED','SLAVESIDE_DISABLED') NOT NULL DEFAULT 'ENABLED',
  `on_completion` enum('DROP','PRESERVE') NOT NULL DEFAULT 'DROP',
  `sql_mode` set('REAL_AS_FLOAT','PIPES_AS_CONCAT','ANSI_QUOTES','IGNORE_SPACE','NOT_USED','ONLY_FULL_GROUP_BY','NO_UNSIGNED_SUBTRACTION','NO_DIR_IN_CREATE','POSTGRESQL','ORACLE','MSSQL','DB2','MAXDB','NO_KEY_OPTIONS','NO_TABLE_OPTIONS','NO_FIELD_OPTIONS','MYSQL323','MYSQL40','ANSI','NO_AUTO_VALUE_ON_ZERO','NO_BACKSLASH_ESCAPES','STRICT_TRANS_TABLES','STRICT_ALL_TABLES','NO_ZERO_IN_DATE','NO_ZERO_DATE','INVALID_DATES','ERROR_FOR_DIVISION_BY_ZERO','TRADITIONAL','NO_AUTO_CREATE_USER','HIGH_NOT_PRECEDENCE','NO_ENGINE_SUBSTITUTION','PAD_CHAR_TO_FULL_LENGTH') NOT NULL DEFAULT '',
  `comment` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `originator` int(10) unsigned NOT NULL,
  `time_zone` char(64) CHARACTER SET latin1 NOT NULL DEFAULT 'SYSTEM',
  `character_set_client` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `collation_connection` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `db_collation` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `body_utf8` longblob,
  PRIMARY KEY (`db`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Events';
~~~

* Si il y a des instances MySQL, il faudra forcer le process d'upgrade de la base de données, par exemple : _mysql_upgrade --port=3307_
* ATTENTION, un bug affecte MySQL à partir de 5.1, les noms de bases avec un '-' doivent être évités... car le cache MySQL ne fonctionne pas !!! Voir <http://bugs.mysql.com/bug.php?id=64821> et <http://dev.mysql.com/doc/relnotes/mysql/5.6/en/news-5-6-9.html>

## SHELL

* Garder bash en shell sur /bin/sh ... sauf être vraimment sûr de soi ! (la question est posée par debconf)
* Attention /bin/false n'est plus listé dans /etc/shells (peut avoir des soucis pour PROFTPD)

## ProFTPD

* Le paquet peut être supprimé dans certains cas particulier

## /etc/fstab

* Upgrade du fstab en UUID : accepter a priori (pas de vrais problèmes rencontrés jusqu'ici...)

## GRUB-2 ou GRUB-PC

* Si celui-ci pose un soucis de type « out of partition »
* Ré-installer grub 1 avec le paquet grub-legacy
* Relancer l'installation à la main, taper grub puis
* root (hd0,0)
* setup (hd0)
* quitter et exécutez update-grub

Si le "demi-passage" à GRUB2 se passe bien, on peut ensuite exécuter (puis rebooter) :

~~~
# upgrade-from-grub-legacy
# rm -f /boot/grub/menu.lst*
~~~

Note : avec Xen, on inversera l'ordre de linux et linux_xen pour avoir :

~~~
# ls /etc/grub.d/
00_header  05_debian_theme  10_linux_xen  20_linux  30_os-prober  40_custom  41_custom  README
~~~

## KERNEL avec GRSEC et GRUB

Lors d'une mise-à-jour du noyau, vous rencontrez une erreur du type :

~~~
Searching for GRUB installation directory ... found: /boot/grub
User postinst hook script [update-grub] exited with value 137
dpkg: error processing linux-image-2.6.26-2-amd64 (--configure):
 subprocess post-installation script returned error exit status 137
Errors were encountered while processing:
 linux-image-2.6.26-2-amd64

# dmesg
PAX: From 192.0.43.10: execution attempt in: <anonymous mapping>, 7cec1c0bc000-7cec1c0d1000 7ffffffea000
PAX: terminating task: /usr/sbin/grub-probe(grub-probe):14743, uid/euid: 0/0, PC: 00007cec1c0cfe98, SP: 00007cec1c0cfe48
PAX: bytes at PC: 41 bb 70 40 40 00 49 ba 90 fe 0c 1c ec 7c 00 00 49 ff e3 00 
PAX: bytes at SP-8: 
grsec: From 192.0.43.10: denied resource overstep by requesting 4096 for RLIMIT_CORE against limit 0 for /usr/sbin/grub-probe[grub-probe:14743] uid/euid:0/0 gid/egid:0/0, parent /usr/sbin/update-grub[update-grub:14742] uid/euid:0/0 gid/egid:0/0
PAX: From 192.0.43.10: execution attempt in: <anonymous mapping>, 74d7be68f000-74d7be6a5000 7ffffffe9000
PAX: terminating task: /usr/sbin/grub-probe(grub-probe):17804, uid/euid: 0/0, PC: 000074d7be6a3ac8, SP: 000074d7be6a3a78
PAX: bytes at PC: 41 bb 70 40 40 00 49 ba c0 3a 6a be d7 74 00 00 49 ff e3 00 
PAX: bytes at SP-8: 
grsec: From 192.0.43.10: denied resource overstep by requesting 4096 for RLIMIT_CORE against limit 0 for /usr/sbin/grub-probe[grub-probe:17804] uid/euid:0/0 gid/egid:0/0, parent /usr/sbin/update-grub[update-grub:17803] uid/euid:0/0 gid/egid:0/0
~~~

Pour résoudre ce soucis :

~~~
# aptitude install paxctl
# paxctl -Cpermxs /usr/sbin/grub-probe
# aptitude upgrade
~~~

Lors d'une mise-à-jour du noyau, vous rencontrez une erreur du type :

~~~
update-initramfs: Generating /boot/initrd.img-2.6.32-5-amd64
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8105e-1.fw for module r8169
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8168e-2.fw for module r8169
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8168e-1.fw for module r8169
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8168d-2.fw for module r8169
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8168d-1.fw for module r8169
~~~

Pour résoudre ce soucis ajouter les dépôts contrib et non-free dans le sources.list puis :

~~~
# aptitude install firmware-realtek
update-initramfs -u -k all
~~~

## AWStats

* Le fichier _/etc/cron.d/awstats_ pourrait être écrasé sans confirmation : par sécurité, sauvegarder le avant la mise-à-jour si vous l'avez modifié

## Dovecot

Si ce message s'affiche après la mise-à-jour :

~~~
Error in configuration file /etc/dovecot/dovecot.conf line 680: Unknown setting: sieve Fatal: Invalid configuration
~~~

Il faut déplacer les options _sieve=_ et _sieve_storage=_ de la section _protocol managesieve_ vers la section _plugin_

Si c'est ce message :

~~~
Error in configuration file /etc/dovecot/dovecot.conf line 94: Unknown setting: ssl_disable
~~~

Il faut commenter l'option _ssl_disable = no_.


Si c'est ce message :

~~~
Error in configuration file /etc/dovecot/dovecot-ldap.conf line 120: Unknown setting: user_global_uid
~~~

Il faut s'assurer que tous les uids/gids des comptes virtuels soient à 5000/5000 dans ldap (ou en tout cas les même que le compte vmail) et commenter ces lignes.

Enfin, pour un message du type :

~~~
Warning: fd limit 1024 is lower than what Dovecot can use under full load (more than 5696). Either grow the limit or change login_max_processes_count and max_mail_processes settings
~~~

Il faut augmenter le _ulimit -n_ dans /etc/default/dovecot

On a également noté des soucis avec le paramètre _mail_plugins = quota imap_quota_

Si dans le mail.log vous avez pleins de warning sur des type de maildir non trouvé (dbox, cydir), il faut spécifier dans la conf que l'on utilise seulement une maildir : 

~~~
mail_location = maildir:~/Maildir
~~~

Autre contrainte, dans le _dovecot.conf_, changer :

~~~
mail_plugins = cmusieve
~~~

en :

~~~
mail_plugins = sieve
~~~


## Horde

~~~
Fatal error: Call to undefined method PEAR_Error::listFolder() in
~~~

Voir <http://bugs.horde.org/ticket/6359>

## Xen

En passant à un kernel 2.6.32 dans le domU il faut changer sda en xvda (dans le paramètre _root=_ de la config de la VM et dans le fstab du domU)
et dans /etc/inittab du domu, changer _1:2345:respawn:/sbin/getty 38400 tty1_ en _1:2345:respawn:/sbin/getty 38400 hvc0_

## Courier IMAP

Si vous avez une notification du type :

~~~
MAP server information: Filesystem notification initialization error -- contact your mail administrator (check for configuration errors with the FAM/Gamin library)
~~~

Installer le package fam :

~~~
# aptitude install fam
~~~

Attention, on peut théoriquement également installer aussi _gamin_ mais nous avons constaté des performances déplorables
(lenteurs avec des Maildir bien remplis, load élevé, etc.). Voir notamment <http://bugs.debian.org/578937>

Note : installer nscd ou nslcd peut également améliorer les performances, etc.

## Samba

Si LDAPS n'est pas utilisé, il est désormais obligatoire d'ajouter dans le smb.conf :

~~~
ldap ssl = off
~~~

Pour un fonctionnement avec FreeRadius + WINBIND, il faut désormais ajouter l'utilisateur _freerad_
dans le groupe _winbindd_priv_ :

~~~
# adduser freerad winbindd_priv
~~~

Enfin, pour l'authentification NTLM dans le cadre d'une authentification Freeradius+EAP/MSCHAPv2+ntlm_auth :

1. La version 3.5 de Samba ne gère plus l'authentification locale NTLM !!!

On obtient :

~~~
NT_STATUS_INVALID_HANDLE: Invalid handle (0xc0000008)
~~~

Voir <http://bugs.debian.org/612049> et <https://bugzilla.samba.org/show_bug.cgi?id=7481>

Le bug est corrigé dans version 3.6 !

Si besoin, il faut donc passé à la version présente dans wheezy/sid (3.6.3 à ce jour, et pas encore backportée mais cela fonctionne)

2. Mais même en 3.6, l'authentification machine ne fonctionne plus ! On obtient l'erreur :

~~~
NT_STATUS_NOLOGON_WORKSTATION_TRUST_ACCOUNT (PAM: 9)
~~~

À la lecture du code source de Samba (!), on voit le code suivant :

~~~
        if (acct_ctrl & ACB_WSTRUST) {
                if (!(user_info->logon_parameters & MSV1_0_ALLOW_WORKSTATION_TRUST_ACCOUNT)) {
                        DEBUG(2,("sam_account_ok: Wksta trust account %s denied by server\n", pdb_get_username(sampass)));
                        return NT_STATUS_NOLOGON_WORKSTATION_TRUST_ACCOUNT;
                }
        }
~~~

C'est apparemment une vérification d'envoi d'un flag lors de l'authentification machine.
En désactivant cette vérification (recompilation d'un paquet Samba sous Sid)... cela fonctionne.
Cela concerne précisément les paquets : samba samba-common samba-common-bin samba-tools winbind libwbclient0

Voir <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=658707> et <https://bugzilla.samba.org/show_bug.cgi?id=8744>

## LDAP

Un cas rencontré où _ldapsearch_ ne fait plus ses requêtes sur 127.0.0.1 par défaut : il a fallu rajouter explicitement _-h 127.0.0.1_

## Postgrey

* Son port d'écoute change du 60000 vers le 10023 ! Il faut donc adapter la configuration de Postfix en conséquence !
* Si Postgrey segfault, il faut supprimer ses données : rm /var/lib/postgrey/* cf <http://ubuntuforums.org/showthread.php?t=1789045>

## PAM_LDAP

Il est conseillé d'utiliser le paquet _libpam-ldapd_ au lieu de _libpam-ldap_... mais si l'on veut mettre à jour l'attribut _shadowLastChange_
via la commande *password*, il faut utiliser _libpam-ldap_ à cause du bug <http://bugs.debian.org/619881>

## Evomaintenance

* Supprimer metche
* cd /etc/ && git init
* Ajouter les directives Git à _evomaintenance.sh_
