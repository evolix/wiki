---
categories: web
title: Howto Migration Debian 9 (Stretch) vers Debian 10 (Buster)
...

Release Notes amd64 : <https://www.debian.org/releases/buster/amd64/release-notes/>

## Debian Buster ?

Debian Buster aka Debian 10 est une version majeure de Debian publiée le 6 Juillet 2019. Dans les changements notables de la distribution, on note :

* Apache 2.4.25 → 2.4.38
* Nginx 1.10 → 1.14
* PHP 7.0 → 7.3
* MariaDB 10.1 → 10.3
* Postgresql 9.6 → 11
* OpenJDK 8 → 11
* Noyau Linux 4.9 → 4.19
* LXC 2 → 3
* CUPS 2.2.1 → 2.2.10
* RabbtiMQ 3.6.6 → 3.7.8

## Actions préalables

Nous conseillons quelques actions qui aideront en cas de problème.

Sauvegarder localement certaines ressources (dans `/home/backup/stretch` par exemple)

~~~
# cat before-upgrade.sh

cd /etc
git add .
git commit -am "Commit balai avant upgrade en Buster"
mkdir -p /home/backup/stretch/
cd /home/backup/stretch
cp -r /etc ./
mkdir -p var/lib/apt
cp -r /var/lib/dpkg ./var/lib/
cp -r /var/lib/apt/extended_states ./var/lib/apt/
dpkg --get-selections "*" > ./current_packages.txt
uptime > uptime.txt
ps auwx > ps.out
pstree -pan > pstree.out
ss -tanpul > listen.out
~~~

## Mise à jour du système

Éditer les dépôts dans `/etc/apt/sources.list` et `/etc/apt/sources.list.d/*.list` pour remplacer _stretch_ par _buster_.

Résultat dans les **fichiers [`sources.list` conseillés](SourcesList#buster-10)**.

S’il y a des backports... **les désactiver** car en général ils ne sont plus nécessaires !

Puis mettre à jour le cache APT avec la commande :

~~~
# apt update
~~~

Commencer par télécharger l'ensemble des paquets qui devront être installés (afin de limiter le temps effectif d'installation).

~~~
# apt dist-upgrade --download-only
~~~

*Attention, si il y a des instances LXC, il faut les stopper avant les mises à jour !*

Faire ensuite une mise à niveau "simple", pour appliquer les mises à jour triviales :

~~~
# apt upgrade
~~~

Attention, si MySQL/MariaDB est installé, il faut stopper les instances supplémentaires car non gérées lors de la mise à jour (et cela va casser avec *There is a MySQL server running, but we failed in our attempts to stop it*) :

~~~
# mysqld_multi stop
~~~

Ensuite, appliquer les mises à jour non triviales (nécessitant des changements de paquets dépendants, des suppressions…) afin d'avoir un œil plus précis sur ce qui sera fait, avant de valider :

~~~
# apt dist-upgrade
~~~

Puis lancer le nettoyage des vieux packages en lisant attentivement la liste… en cas de doute, réinstaller le paquet !

~~~
# apt autoremove
~~~

Enfin, il faut redémarrer sur le nouveau noyau Linux installé :

~~~
# reboot
~~~

## PATH / su

Lorsque l'on utilise `su` cela ne change plus automatiquement le PATH.
Soit il faut prendre l'habitude faire `su -`, soit il faut ajouter `ALWAYS_SET_PATH yes` dans le fichier `/etc/login.defs` (mais cela génère un message d'erreur pour un certain nombre d'applications...)

Pour plus d'informations, voir `/usr/share/doc/util-linux/NEWS.Debian.gz` ou <http://bugs.debian.org/918754>

## MariaDB

Si le check_mysql indique une erreur en rapport avec `libmariadb.so.3` il faut installer le paquet `libmariadb3`.

~~~
# /usr/lib/nagios/plugins/check_mysql -H localhost -f /etc/mysql/debian.cnf 
/usr/lib/nagios/plugins/check_mysql: error while loading shared libraries: libmariadb.so.3: cannot open shared object file: No such file or directory
~~~

** ToDo **

> Remarque: MariaDB 10.2 a introduit un changement de la valeur de [`SQL_MODE`](https://mariadb.com/kb/en/library/sql-mode/), avec notamment l'ajout de `STRICT_TRANS_TABLES` qui vas rendre MariaDB plus strict sur les transactions. Il se peut que, dans certains cas, l'activation de ce mode soit problématique pour des applications assez anciennes.

## X.Org

En cas de clavier/souris inactif, il peut être nécessaire d'ajouter l'utilisateur dans le groupe `input`

## PHP

Il faut parfois forcer le passage à PHP 7.3 :

~~~
# apt remove libapache2-mod-php7.0 && a2enmod php7.3 && /etc/init.d/apache2 restart
~~~

puis nettoyer les anciens paquets php7.0-*

## PHPMyAdmin

Garder en tête que Buster n'a pas le paquet PHPMyAdmin dans ses dépots principaux mais dans les dépots backports et que la version utilisée dans stretch n'est pas compatible avec les PHP 7.3.x. Il faut donc ajouter et configurer les dépots backports pour le mettre à jour.


## Nautilus sous XFCE

Si vous avez de la difficulté à ouvrir des fichiers dans Nautilus sous XFCE, cela peut venir d'un problème avec `exo-utils`.
Un contournement est disponible ici : <https://askubuntu.com/questions/968857/xubuntu-17-10-nautilus-doesnt-open-files>

## LXC et AppArmor

Si des conteneurs LXC sont configurés, il faut installer lxc-templates et apparmor afin d'installer des configurations nécessaire à leur fonctionnement.

Et il faut mettre à jour la config de tous les conteneurs :

~~~
# lxc-update-config -c /var/lib/lxc/foo/config
~~~

## PostgreSQL

Il faut migrer manuellement.

~~~
# pg_dropcluster --stop 11 main
# pg_upgradecluster 9.6 main -m upgrade -k
# pg_ctlcluster 11 main start
~~~

Si le check est au vert alors :

~~~
# pg_dropcluster 9.6 main
~~~

## RabbitMQ

Il faut dé commenter dans /etc/rabbitmq/rabbitmq-env.conf "NODE_IP_ADDRESS" et "RABBITMQ_NODENAME". Autrement rabbitmq-server ne se lancera pas.

## usrmerge

Les installations fraîches de Debian Buster ont un `/usr` fusionné.  
Les répertoires `/{bin,sbin,lib}/` deviennent des liens symboliques vers `/usr/{bin,sbin,lib}/`.  
Si on veut faire la fusion il faut installer le paquet `usrmerge`.

```
# apt install usrmerge 
```

## Écosystème Ruby on Rails

On peut avoir des erreurs avec des gems.

~~~
bundle[3404]: LoadError: libmariadbclient.so.18: cannot open shared object file: No such file or directory - 
~~~

Il faut mettre à jour si le projet utilise bundle.

~~~
$ RAILS_ENV=production bundle install --redownload
~~~

## Squid

Il faut s'assurer d'avoir l'override systemd.

```
# systemctl edit squid
# systemd override for Squid
[Service]
ExecStart=
ExecStart=/usr/sbin/squid -sYC -f /etc/squid/evolinux-defaults.conf
# systemctl daemon-reload
# systemctl restart squid
```

## Iptables

La commande iptables change entre Debian 9 et 10. Si on rencontre des soucis de firewall en cours de route on peut utiliser la commande `iptables-legacy` et éventuellement purger les tables de NAT avec `iptables-legacy -t nat -F`.

## libvirt

~~~
Errors were encountered while processing:
 libvirt-daemon-system
~~~

Il faut stopper `virtlogd-admin.socket` puis relancer l'upgrade.

~~~
# systemctl stop virtlogd-admin.socket
# apt install -f
~~~

### Erreur apparmor="DENIED" avec libvirt et volumes Ceph

Si on a l'erreur suivante au démarrage d'une VM, qui a un ou des disques virtuel sur Ceph :

~~~
apparmor="DENIED" operation="open" profile="libvirt-bddf16e3-6b6c-4c4d-ac22-168272e1def4" name="/etc/ceph/ceph.client.libvirt.keyring" pid=6599 comm="kvm" requested_mask="r" denied_mask="r" fsuid=64055 ouid=0
~~~

il faut autorisé le chemin **/etc/ceph/** dans la configuration de apparmor dans **/etc/apparmor.d/abstractions/libvirt-qemu** et rajouter ces lignes :

~~~
/etc/ceph/ r,
/etc/ceph/* r,
~~~

### Moniteur Ceph inaccessible avec une mise à jour du client en Ceph 14

Si un **ceph -s** ne répond pas suite a une mise à jour de Ceph en version 14, il faut vérifier que le port **3300** est bien accessible depuis le client vers le cluster Ceph.

## Postfix

Il faut assurer que `smtpd_relay_restrictions` soit présent dans `/etc/postfix/main.cf`.
Si l'option n'est pas présente, ajouter :

~~~
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
~~~

## Dovecot

Le module statistiques de Dovecot a changé entre les versions 2.2 et 2.3.
Cela casse notamment les plugins Munin.

Pour la rétro-compatibilité, [nous réactivons l'ancien module de statistiques](/HowtoDovecot#dovecot-2.3-debian-buster).

Aussi, vérifier [cette section du HowtoDovecot](/HowtoDovecot#erreur-varrundovecotstats-writer-failed-permission-denied).
Vérifier si vous trouvez l’erreur suivante dans les logs mail :


## ProFTPD

### Erreur "fatal: unknown configuration directive 'IdentLookups'"

La directive **IdentLookups** doit être dans une balise ou l'on charge le module **mod_ident.c** comme ceci :

~~~
<IfModule mod_ident.c>
  IdentLookups off
</IfModule>
~~~

### Bug pour lister les répertoires de plus de 10k fichiers

La version 1.3.6 a un bug pour lister les répertoires avec plus de 10k fichiers :
https://github.com/proftpd/proftpd/issues/863
Un contournement est de désactiver le "cachefs" de ProFTPD comme suggéré ici :
http://bugs.proftpd.org/show_bug.cgi?id=4360#c14

## Bind

En cas de souci de redémarrage de Bind, voir : <https://wiki.evolix.org/HowtoBind#probl%C3%A8mes-de-permissions-avec-chroot>

## Docker

En cas d'erreur `layer does not exist` au démarrage de docker, c'est possible que ce soit causé par un trop gros saut de version.

Généralement, on peut résoudre en re-mettant le home de docker à zero :

```
$ mv /var/lib/docker /var/lib/docker.before.upgrade
## relancer docker -- il devrait se lancer sans problème et recréer /var/lib/docker
## arréter de nouveau docker
## récupérer les volumes persisent
$ rm -r /var/lib/docker/volumes
$ mv /var/lib/docker.before.upgrade/volumes /var/lib/docker/volumes
# rédemarrer le démon docker
$ docker volume ls
# il devrait y avoir les anciens volumes
# il ne restes qu'a rebuild/relancer les conteneur
```

## NFS

Si un client monte un volume NFS en NFSv3, il peut être nécessaire d’activé le service `rpc-statd`, cf. [la page dédiée](/HowtoNFS#service-rpc-statd)

## Postgrey

En Debian 10, le script "/etc/init.d/postgrey" a un bug qui provoque une erreur lors du reload :

~~~
# systemctl reload postgrey
Job for postgrey.service failed because the control process exited with error code.
See "systemctl status postgrey.service" and "journalctl -xe" for details.
~~~

Il faut appliquer le patch https://bugs.debian.org/cgi-bin/bugreport.cgi?att=1;bug=934068;filename=postgrey_init.patch;msg=5
à savoir ajouter "--user $DAEMON_USER" sur la ligne 97 du fichier "/etc/init.d/postgrey"




