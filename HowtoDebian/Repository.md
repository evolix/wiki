# Howto Dépôt Debian

Il peut parfois être intéressant de disposer d'un dépôt Debian privé pour distribuer des packages qui ne sont pas prêts à être ajoutés à l'archive de Debian ou alors qui ne sont pas d'intérêt public.

Il existe plusieurs manières de monter un dépôt Debian.

## apt-ftparchive

Une façon "old school" est d'avoir un répertoire avec des paquets Debian dedans, exemple `stretch/` puis de lancer les commandes suivantes :

~~~
$ apt-ftparchive packages stretch/ > stretch/Packages
$ apt-ftparchive sources stretch/ > stretch/Sources
$ apt-ftparchive contents stretch/ > stretch/Contents
$ apt-ftparchive -o APT::FTPArchive::Release::Codename="stretch/" release stretch/ > stretch/Release
$ gzip -f stretch/Sources
$ #gzip -f stretch/Packages
$ gzip -f stretch/Contents
~~~

Pour Debian 9 (Stretch), le fichier `Release` doit impérativement être signé via GPG :

~~~
$ gpg -abs -o Release.gpg Release
~~~

Le répertoire doit ensuite être accessible en HTTP et l'on pourra ajouter à son _sources.list_ :

~~~
deb http://pub.example.com/ stretch/
~~~

## reprepro

~~~
# apt install reprepro
~~~

On a besoin d'une clé GPG pour signer le dépôt. Voici quelques éléments importants :

* Il est important de ne pas ajouter de "Comment" quand GPG nous le demande
* Pour un dépôt, on choisit une clé de type RSA en mode signature seulement
* On choisi une clef la plus longue possible, soit 4096

~~~
$ gpg2 --gen-key
~~~

### Configuration

Nous choisissons de placer notre dépôt Debian dans `/var/www/repos` :

~~~
# mkdir -p /var/www/repos/debian/conf
~~~

On crée par la suite `/var/www/repos/debian/conf/distributions`, le fichier qui servira à `reprepro` pour identifier les différentes versions de Debian à distribuer. Le fichier devrait parler de lui-même. Pour chaque version de Debian, on ajoute un bloc supplémentaire à la fin du fichier. N'oubliez pas de:

* remplacer le fingerprint GPG par le vôtre
* spécifier les architectures que vous souhaitez supporter

~~~
Origin: Evolix public repository
Label: Evolix public repository
Codename: jessie
Architectures: i386 amd64 armhf
Components: main
Description: Evolix public repository
SignWith: 677F54F1FA8681AD8EC0BCE67AEAC4EC6AAA0A97

Origin: Evolix public repository
Label: Evolix public repository
Codename: wheezy
Architectures: i386 amd64 armhf
Components: main
Description: Evolix public repository
SignWith: 677F54F1FA8681AD8EC0BCE67AEAC4EC6AAA0A97

Origin: Evolix public repository
Label: Evolix public repository
Codename: squeeze
Architectures: i386 amd64 armhf
Components: main
Description: Evolix public repository
SignWith: 677F54F1FA8681AD8EC0BCE67AEAC4EC6AAA0A97

Origin: Evolix public repository
Label: Evolix public repository
Codename: kernel
Architectures: i386 amd64 armhf
Components: main
Description: Evolix public repository
SignWith: 677F54F1FA8681AD8EC0BCE67AEAC4EC6AAA0A97
~~~

Finalement, on crée `/var/www/repos/debian/conf/options`, un fichier d'options pour se simplifier la vie :

~~~
verbose
basedir /var/www/repos/debian
ask-passphrase
~~~

Pour que les gens puissent profiter de votre dépôt signé, n'oubliez pas d'exporter votre clé GPG publique et de la rendre accessible :

~~~
$ gpg2 --armor --output /var/www/repos/debian/ma_clef.gpg.asc --export <fingerprint>
~~~

Et voilà, `reprepro` est prêt à être utilisé !

### Apache

Pour que votre dépôt soit disponible en ligne, nous allons ajouter un VHost [Apache](/HowtoApache) via `/etc/apache2/sites-available/repository.conf`:

~~~{.apache}
<VirtualHost *:80>
	ServerName debian.example.com

	DocumentRoot /var/www/repos

	ErrorLog ${APACHE_LOG_DIR}/error.log

	<Directory /var/www/repos/ >
        	# We want the user to be able to browse the directory manually
        	Options Indexes FollowSymLinks Multiviews
                Require all granted
	</Directory>

	<Directory "/var/www/repos/debian/db/">
                Require all denied
	</Directory>

	<Directory "/var/www/repos/debian/conf/">
                Require all denied
	</Directory>

	<Directory "/var/www/repos/debian/incoming/">
                Require all denied
	</Directory>

</VirtualHost>
~~~

### Ajouter des packages à son dépôt

Votre dépôt est maintenant en ligne, mais est toujours vide. Pour y ajouter des packages, lancez la commande suivante :

~~~
# reprepro -b /var/www/repos/debian includedeb jessie mon_super_package.deb
~~~

`reprepro` vous demande alors le mot de passe de votre clé GPG, signe votre package, crée les dossiers nécessaires et génère tous les fichiers pour vous.

Vous avez maintenant un dépôt Debian !

### Télécharger des packages à partir du dépôt

Pour télécharger des packages de votre archive, il est tout d'abord nécessaire d'installer la clé GPG qui a servi à signer les packages :

~~~
# wget http://mondepot.fqdn.org/debian/ma_clef.gpg.asc -O /etc/apt/trusted.gpg.d/ma_clef.gpg.asc
# dos2unix /etc/apt/trusted.gpg.d/ma_clef.gpg.asc
~~~

Une fois que cela est fait, il ne vous reste plus qu'à ajouter votre dépôt dans votre _sources.list_ :
~~~
deb http://debian.example.com/debian/ jessie main
~~~

### Particularités importantes

Parce que `reprepro` se veut un utilitaire simple créer des dépôts Debian (comparé à `dak` ou encore `mini-dak`), certaines choses ne sont pas possible :

* Il n'est pas possible d'avoir plusieurs versions du même package dans une même distribution. La version plus ancienne est automatiquement remplacée par la plus récente. Il est cependant possible d'avoir des versions différentes pour chaque distribution.
* Un package ne peut pas avoir la même version dans plusieurs distributions tout en ayant un SHAsum différent. Dans ce cas, il est nécessaire de changer la version du package pour les distinguer (ex. `1.3.3-1+jessie` sur jessie et `1.3.3-1+wheezy` sur wheezy). Ceci n'est cependant pas un problème si le package est le même (même SHAsum) à travers plusieurs distributions.

### Mode miroir

Pour faire un miroir de dépôt ne proposant pas d’accès rsync, `reprepro` permet de faire une partie du travail.

**TODO** : Peut-être que [debmirror](https://packages.debian.org/stable/debmirror) serait une meilleure alternative.

Nous réutiliserons les fichiers Release (y compris leur signature d’origine), donc il n’est pas nécessaire de configurer une clef PGP pour ces dépôts (il est néanmoins utile d’obtenir la partie publique de la clef utilisée sur le dépôt d’origine pour vérifier la signature des fichiers d’origine).

Voici un exemple de la configuration pour un miroir partiel (amd64) de eLTS pour Stretch. `ExportOptions: noexport` permet de ne pas reconstruire le répertoire `dists/`.

~~~
$ head -n-1 /var/www/repos/extended-lts/conf/*

==> /var/www/repos/extended-lts/conf/distributions <==
Origin: Freexian
Label: Freexian-Extended-LTS
Suite: wheezy
Codename: wheezy-lts
Version: 7
Architectures: amd64 source
Components: main contrib non-free
Description: Extended LTS Updates for Debian 7 Wheezy
Update: wheezy-lts
Log: /var/www/repos/extended-lts/log/wheezy-lts.log
ExportOptions: noexport

==> /var/www/repos/extended-lts/conf/options <==
basedir /var/www/repos/extended-lts/
outdir /srv/mirrors/extended-lts/
gnupghome /var/www/repos/extended-lts/.gnupg

==> /var/www/repos/extended-lts/conf/updates <==
Name: wheezy-lts
Method: http://deb.freexian.com/extended-lts
Suite: wheezy
Components: main contrib non-free
Architectures: amd64 source
VerifyRelease: A07310D369055D5A
~~~

La première synchronisation (manuelle) est longue (à faire dans un `screen`).

~~~bash
$ wget https://deb.freexian.com/extended-lts/archive-key.gpg
$ gpg --homedir /var/www/repos/extended-lts/.gnupg --import archive-key.gpg
$ umask 022; reprepro --confdir /var/www/repos/extended-lts/conf update
~~~

Mise à jour automatique (via cron):

~~~bash
$ crontab -l

1 * * * * perl -e 'sleep int(rand(1800))' && umask 022 && reprepro --silent --confdir /home/miroir/extended-lts/conf update
~~~

Enfin, comme il manque les fichiers `Release` avec cette méthode, un script comme le suivant
permet d’en faire une copie (à lancer en cron à la suite).

~~~bash
#!/bin/sh
#
# Recursive copy of /dists directory for mirror without rsync
#

set -e

mirror=https://deb.freexian.com/extended-lts/dists/

# Unwanted versions i.e, directories (it’s a full mirror)
reject=wheezy\|jessie\|stretch

# Unwanted architecures
noarch=arm64\|armel\|armhf\|i386

# Number of directories to trim: All but the last
# (3: protocol, hostname and last directory).
cutdir=$(( $(echo $mirror | tr '/' ' ' | wc -w) - 3 ))

# Should not need to be changed, might be taken from URL mirror…
target=dists

# Start from scratch
rm -rf $target/
wget \
        --quiet \
        --recursive \
        --level=inf \
        --reject *.html \
        --reject-regex "(/($reject)/|$noarch)" \
        --no-parent \
        --no-host-directories \
        --cut-dirs=$cutdir \
        $mirror

# Clean up temporary files
find $target/ -name index.html.tmp -delete
~~~
