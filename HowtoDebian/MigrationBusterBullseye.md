---
categories: web debian upgrade bullseye
title: Howto Migration Debian 10 (Buster) vers Debian 11 (Bullseye)
...

Release Notes amd64 : <https://www.debian.org/releases/bullseye/amd64/release-notes/>


## Debian Bullseye

Dans les changements notables de la distribution, on note :

* Apache : 2.4.38 → 2.4.46
* Serveur DNS BIND : 9.11 → 9.16
* Cryptsetup : 2.1 → 2.3
* Dovecot MTA : 2.3.4 → 2.3.13
* GCC : 8.3 → 10.2
* GnuPG : 2.2.12 → 2.2.20
* Bibliothèque C GNU : 2.28 → 2.31
* Image du noyau Linux : Série 4.19 → Série 5.10
* MariaDB : 10.3 → 10.5
* Nginx : 1.14 → 1.18
* OpenJDK : 11 → 11
* OpenSSH : 7.9p1 → 8.4p1
* Perl : 5.28 → 5.32
* PHP : 7.3 → 7.4
* Postfix MTA : 3.4 → 3.5
* PostgreSQL : 11 → 13
* Python 3 : 3.7.3 → 3.9.1
* Samba : 4.9 → 4.13
* Tomcat : 9.0.31 → 9.0.43
* Vim : 8.1 → 8.2


## Actions préalables

Nous conseillons quelques actions qui aideront en cas de problème.

Sauvegarder localement certaines ressources (dans `/home/backup/buster` par exemple)

~~~
# cat before-upgrade.sh

cd /etc
git add .
git commit -am "Commit balai avant upgrade en Bullseye"
mkdir -p /home/backup/buster/
cd /home/backup/buster
cp -r /etc ./
mkdir -p var/lib/apt
cp -r /var/lib/dpkg ./var/lib/
cp -r /var/lib/apt/extended_states ./var/lib/apt/
dpkg --get-selections "*" > ./current_packages.txt
uptime > uptime.txt
ps auwx > ps.out
pstree -pan > pstree.out
ss -tanpul > listen.out
if [ -f /root/.mysql_history ]; then cp -a /root/.mysql_history /root/.mysql_history.bak; fi
~~~

### bridge

Si votre serveur a un bridge réseau, il est probable que l'upgrade en Debian 11 change son adresse MAC.
Pour éviter cela il faut fixer son adresse MAC actuelle via l'option `bridge_hw` :

~~~
auto br0
iface br0 inet manual
        bridge_ports eth0
        bridge_hw ab:cd:01:23:45:67
~~~

Plus d'infos sur [#991416](https://bugs.debian.org/991416)


## Mise à jour du système

Éditer les dépôts dans `/etc/apt/sources.list` et `/etc/apt/sources.list.d/*.list` pour remplacer _buster_ par _bullseye_.

~~~
# sed -i 's/buster\/updates/bullseye-security/g' /etc/apt/sources.list
# sed -i 's/buster/bullseye/g' /etc/apt/sources.list /etc/apt/sources.list.d/*.list
~~~

Résultat dans les **fichiers [`sources.list` conseillés](SourcesList#bullseye-11)**.

S’il y a des backports... **les désactiver** car en général ils ne sont plus nécessaires !

Puis mettre à jour le cache APT avec la commande :

~~~
# apt update
~~~

Commencer par télécharger l'ensemble des paquets qui devront être installés (afin de limiter le temps effectif d'installation).

~~~
# apt full-upgrade --download-only
~~~

**Attention, si il y a des instances LXC, il faut les stopper avant la mise à niveau !**

Faire ensuite une mise à niveau sans nouveaux paquets, pour appliquer les mises à jour triviales :

~~~
# apt upgrade --without-new-pkgs
~~~

Faire ensuite une mise à niveau avec nouveaux paquets mais sans désinstallations :

~~~
# apt upgrade --with-new-pkgs
~~~

**TODO: à vérifier**

Attention, si MySQL/MariaDB est installé, il faut stopper les instances supplémentaires car non gérées lors de la mise à jour (et cela va casser avec *There is a MySQL server running, but we failed in our attempts to stop it*) :

~~~
# mysqld_multi stop
~~~

Ensuite, appliquer les mises à jour non triviales (nécessitant des changements de paquets dépendants, des suppressions…) afin d'avoir un œil plus précis sur ce qui sera fait, avant de valider :

~~~
# apt full-upgrade
~~~

Puis lancer le nettoyage des vieux paquets en lisant attentivement la liste… en cas de doute, réinstaller le paquet !

~~~
# apt autoremove
~~~

Enfin, il faut redémarrer sur le nouveau noyau Linux installé :

~~~
# reboot
~~~


## PATH / su

Lorsque l'on utilise `su` cela ne change plus automatiquement le PATH.
Soit il faut prendre l'habitude faire `su -`, soit il faut ajouter `ALWAYS_SET_PATH yes` dans le fichier `/etc/login.defs` (mais cela génère un message d'erreur pour un certain nombre d'applications...).

Pour plus d'informations, voir `/usr/share/doc/util-linux/NEWS.Debian.gz` ou <http://bugs.debian.org/918754>


## Nagios NRPE

### Overwrite `/etc/nagios/nrpe.cfg` ?

`/etc/nagios/nrpe.cfg` peut être écrasé sans risque si les modifications sont celles faites pour l'installation de `monitoringctl` : checks commentés et/ou ajout de `alerts_wrapper` aux checks.

On peut le vérifier avec la commande :

~~~
cd /etc; git log -p /etc/nagios/nrpe.cfg
~~~


## MariaDB / MySQL

### `mysql_upgrade`

Normalement, le script postupgrade du paquet joue le script `mysql_upgrade` pour mettre a jour les tables, vues... suite à un upgrade majeur.
Cependant, si le datadir est volumineux, il le saute (c'est visible dans les logs MySQL) et il faut le faire à la main :

~~~
# mysql_upgrade
~~~

**S'il y a plusieurs instances**, il faut l'exécuter pour chacune avec le port en argument : `mysql_upgrade -p <PORT>`


Si vous rencontrez cette erreur :

~~~
Instance 3307: slave query error: Access denied; you need (at least one of) the SUPER, SLAVE MONITOR privilege(s) for this operation
~~~

C'est que vous avez oublié d'exécuter `mysql_upgrade -p <PORT>` sur une instance ou ne s'est pas terminé correctement. En effet, les permissions de monitoring ont changé entre MariaDB 10.3 et  10.5 et `mysql_upgrade` les met à jour automatiquement.


### Historique

Note : La commande a été ajoutée ci-dessus dans la section « Actions préalables »

La bibliothèque qui gère l'historique a changé dans MariaDB 10.5, c'est libedit qui est utilisé désormais.
Ce changement fait que lors de la mise à jour de MariaDB 10.3 à 10.5, le fichier `.mysql_history` est effacé, on sauvegardera le fichier avant la mise à jour :

~~~
cp -a /root/.mysql_history /root/.mysql_history.bak
~~~ 


## PHP

Il faut parfois forcer le passage à PHP 7.4 :

~~~
# apt remove libapache2-mod-php7.3 && a2enmod php7.4 && /etc/init.d/apache2 restart
~~~

puis nettoyer les anciens paquets `php7.3-*`


### PHPMyAdmin

PHPMyAdmin a fait son retour dans les paquets Debian. Si vous l'aviez installé par les sources en Debian 10, vous pouvez le désinstaller et remettre le paquet.


## LXC

### LXC et AppArmor

**TODO: à vérifier, probablement toujours d'actualité**

Si des conteneurs LXC sont configurés, il faut installer `lxc-templates` et `apparmor` afin d'installer des configurations nécessaires à leur fonctionnement.

Et il faut mettre à jour la config de tous les conteneurs :

~~~
# lxc-update-config -c /var/lib/lxc/foo/config
~~~

AppArmor peut causer des problèmes de re-montage de devices dans les conteneurs LXC.
Pour le désactiver pour les conteneurs :

~~~
# vim /var/lib/lxc/$container_name/config
+ lxc.apparmor.profile = unconfined
# systemctl restart lxc
~~~


### LXC et cgroupv2 (nécessaire quand il y a un container php56 (Debian 8) par exemple)

Sous Bullseye la version de la hiérarchie cgroup par défaut est passé à cgroupv2 au lieu de la hiérarchie hybride. 

Cela ne pose pas de problème en soi pour LXC, qui supporte cgroupv2 sous Bullseye **mais** cela peut provoquer des problèmes pour les conteneurs dont l'init dépend de cgroupv1 (tel que Debian 8), dans le cas où le support pour ces conteneurs est nécessaire il faut modifier la ligne de commande du noyau Linux **de l'hôte** en changeant `/etc/default/grub`:

```.diff
- GRUB_CMDLINE_LINUX=""
+ GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=false systemd.legacy_systemd_cgroup_controller=false"
```

puis régénérer la configuration de Grub :

```
grub-mkconfig -o /boot/grub/grub.cfg
```

(Ou simplement via `dpkg-reconfigure grub-pc`)


## PostgreSQL

**Attention, Il faut faire la mise à jour de version avec cette méthode, seulement si on utilise la version des dépôt Debian**. Sinon, la migration doit être manuelle et plus détaillé si l'on utilise la versions des dépôt PGDG!

**Downtime du service à prévoir et vérifier que l'on a le double de place sur la partition où se trouve le datadir**

~~~
# pg_upgradecluster -v 13 11 main -m dump
~~~

S'il dit "Error: target cluster 13/main already exists" **vérifier la présence des données sur la nouvelle instance** avant de supprimer définitivement l'ancienne. Si c'est ok alors :

~~~
# pg_dropcluster 11 main
~~~


## usrmerge

Les installations fraîches de Debian Bullseye (et Buster) ont un `/usr` fusionné.  
Les répertoires `/{bin,sbin,lib}/` deviennent des liens symboliques vers `/usr/{bin,sbin,lib}/`.  
Si on veut faire la fusion il faut installer le paquet `usrmerge`.

```
# apt install usrmerge 
```


## Écosystème Ruby on Rails

**TODO: à vérifier, probablement toujours d'actualité**

On peut avoir des erreurs avec des gems.

~~~
bundle[3404]: LoadError: libmariadbclient.so.18: cannot open shared object file: No such file or directory - 
~~~

Il faut mettre à jour si le projet utilise bundle.

~~~
$ RAILS_ENV=production bundle install --redownload
~~~


## Squid

**TODO: à vérifier, probablement obsolète**

Il faut s'assurer d'avoir l'override systemd.

```
# systemctl edit squid
# systemd override for Squid
[Service]
ExecStart=
ExecStart=/usr/sbin/squid -sYC -f /etc/squid/evolinux-defaults.conf
# systemctl daemon-reload
# systemctl restart squid
```


## libvirt

**TODO: à vérifier, probablement toujours d'actualité**

~~~
Errors were encountered while processing:
 libvirt-daemon-system
~~~

Il faut stopper `virtlogd-admin.socket` puis relancer l'upgrade.

~~~
# systemctl stop virtlogd-admin.socket
# apt install -f
~~~


## Postfix

Il faut assurer que `smtpd_relay_restrictions` soit présent dans `/etc/postfix/main.cf`.
Si l'option n'est pas présente, ajouter :

~~~
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
~~~


## ProFTPD

### ed25519

S'il y a un vhost SFTP, on peut activer une clé ED25519.

Dans `/etc/proftpd/conf.d/sftp.conf` :

~~~
SFTPHostKey /etc/ssh/ssh_host_ed25519_key
~~~

Sans oublier de vérifier que la clé existe bien.


### IdentLookups

Si ProFTPd bloque à la mise à jour, il faut vérifier la syntaxe de la configuration avec `proftpd -t`. Si le souci vient de `IdentLookups`, on peut appliquer ce diff :

~~~{.diff}
@@ -10,7 +10,10 @@ Include /etc/proftpd/modules.conf
 # Set off to disable IPv6 support which is annoying on IPv4 only boxes.
 UseIPv6                                on
 # If set on you can experience a longer connection delay in many cases.
-IdentLookups                   off
+<IfModule mod_ident.c>
+  IdentLookups                 off
+</IfModule>
+
~~~

**TODO: à vérifier, probablement obsolète**

La version 1.3.6 a un bug pour lister les répertoires avec plus de 10k fichiers :
https://github.com/proftpd/proftpd/issues/863

Un contournement est de désactiver le "cachefs" de ProFTPD comme suggéré ici :
http://bugs.proftpd.org/show_bug.cgi?id=4360#c14


## Bind

En cas de souci de redémarrage de Bind, voir : <https://wiki.evolix.org/HowtoBind#probl%C3%A8mes-de-permissions-avec-chroot>


## Shelldap

Pour que `shelldap` demande la saisie du mot de passe, il est nécessaire de modifier la configuration (généralement `~/.shelldap.rc`), en ajoutant la ligne suivante :

~~~
[…]
promptpass: 1
[…]
~~~


## bkctld

`bkctld` est la partie serveur de l'outil de backup [Evobackup](https://gitea.evolix.org/evolix/evobackup) développé par Evolix.

Les version de `bkctld` antérieures à la 2.12.0 peuvent être désinstallées par la mise à jour à cause de dépendances non satisfaites.
Il suffit de réinstaller [la dernière version de bkctld](https://gitea.evolix.org/evolix/evobackup/releases).


## Check_rabbitmq en erreur, module requests introuvable
 
Le check rabbitmq que nous utilisons pour notamment le check icinga `rab_connection_count` est en python2, et certains module python2 ne sont plus disponible en debian 11 :

~~~
Traceback (most recent call last):
  File "/usr/local/lib/nagios/plugins/check_rabbitmq", line 6, in <module>
    import requests
ImportError: No module named requests
~~~

Pour le check `rab_connection_count` nous n'avons pas besoin des appels au modules request, on peux donc commenter le module dans le script `/usr/local/lib/nagios/plugins/check_rabbitmq``:

~~~
-import requests
+#import requests
~~~

## Ceph

Si le serveur n’arrive plus à joindre les membres du _cluster_, le problème vient peut-être du port 3300 qui n’est pas autorisé en sortie et, si c’est un serveur Ceph, en entrée. Voir [/HowtoCeph#connexion-impossible-après-une-mise-à-niveau-dun-client-vers-debian-11].

## Roundcube

Le fichier `/etc/roundcube/apache.conf` installé par le paquet Debian change le chemin `/var/lib/roundcube/` pour `/var/lib/roundcube/public_html/`.

Il faut soit revenir à `/var/lib/roundcube/` dans `/etc/roundcube/apache.conf` ou ajuster dans le vhost de roundcube.

## Nettoyage post-upgrade

Il est possible que certains paquets (notamment les paquets php en `phpX.Y-*`) ne soient pas supprimés par le `apt autoremove` lors de la mise à jour alors qu'ils ne sont plus dans les dépôts Debian. Il est possible de lister et supprimer ces paquets avec les commandes suivantes :

~~~
apt list '~o'
apt remove '~o'
# Ou pour supprimé toutes les traces
apt purge '~o'
~~~

## Sympa

Vous devrez créer l'unité systemd `wwwsympa.service` et modifier le vhost Apache `sympa.conf en copiant les contenus sur [HowtoSympa](https://wiki.evolix.org/HowtoSympa) :

~~~
# apt install spawn-fcgi
# vim /etc/systemd/system/wwsympa.service

# systemctl daemon-reload
# systemctl enable --now wwsympa

# a2enmod proxy_fcgi

# vim /etc/apache2/sites-enabled/sympa.conf
# apache2ctl -t && systemctl restart apache2
~~~
