**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Google Cloud

## Infos Cloud Engine

- Présence de zones aux USA, Europe et Asie. L’Europe est à 50ms d'une Freebox.[[BR]]
- Ajouter alias IPv4 --> *Cela ne semble pas possible*. 1 seule adresse IP publique par machine. Le compte démo « free trial » est limité à 1 adresse IP cela dit… Peut-être en mode payant, on peut.[[BR]]
- Quid de l'IPv6 --> *IPvQuoi ? Le néant*.[[BR]]
- Gestion des volumes / disques --> Choix de SSD local à l'hyperviseur (Forcé à 375G, Max 8 vols, Peut être crée que lors de la création de la VM *Empêche la migration/déplacement de VM*), SSD (Max 65T), HDD (Max 65T). (Tous en persistant).[[BR]]
- Perfs disques[[BR]]

~~~
 Iops annoncés[[BR]]
- Local SSD : 100 000 lecture / 70 000 écriture[[BR]]
- SSD : 3 000 / 3000 (BP 48Mo/s)[[BR]]
- HDD : 150 / 750 (BP 60/45Mo/s)[[BR]]

Local SSD : 
time dd if=/dev/zero bs=4096 count=4194304 2>/tmp/out | pv > 16G ; cat /tmp/out
17179869184 bytes (17 GB) copied, 39.8078 s, 432 MB/s

SSD :
time dd if=/dev/zero bs=4096 count=4194304 2>/tmp/out | pv > 16G ; cat /tmp/out
17179869184 bytes (17 GB) copied, 620.13 s, 27.7 MB/s
Note : Saturation à 100% du disque, gros ralentissement de la machine, lors du @dd@ de bench.

HDD : 
time dd if=/dev/zero bs=4096 count=4194304 2>/tmp/out | pv > 16G ; cat /tmp/out
17179869184 bytes (17 GB) copied, 219.622 s, 78.2 MB/s

Conclusion :
Plus rapide en BP, SSD Local, HDD et SSD.
Plus rapide en iops, SSD Local, SSD, et HDD. (Théorie, non testé).
~~~

*Bon à savoir que le SSD est plus lent en bande passante que le HDD ! *[[BR]]
*Impossible de détacher le disque rootfs d'une machine. Impossible de faire du rescue et monter le disque sur une autre machine.* [[BR]]
Un snapshot ne permet pas de remonter la machine à la date du snapshot, c'est juste une copie du disque (pour faire des backups à froid ou autre utilisation).

- Snapshots --> Gestion classique.[[BR]]
- Iperf --> *UDP limité à 1Mbps*, TCP 1Gbps[[BR]]

~~~
root@evolinux:~# iperf -u -c ping.online.net
------------------------------------------------------------
Client connecting to ping.online.net, UDP port 5001
Sending 1470 byte datagrams
UDP buffer size:  208 KByte (default)
------------------------------------------------------------
[  3] local 10.132.0.2 port 54641 connected with 62.210.18.40 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec  1.25 MBytes  1.05 Mbits/sec
[  3] Sent 893 datagrams
[  3] Server Report:
[  3]  0.0-10.0 sec  1.25 MBytes  1.05 Mbits/sec   0.036 ms    0/  893 (0%)
~~~

~~~
root@evolinux:~# iperf -t60 -c ping.online.net
------------------------------------------------------------
Client connecting to ping.online.net, TCP port 5001
TCP window size: 85.0 KByte (default)
------------------------------------------------------------
[  3] local 10.132.0.2 port 40783 connected with 62.210.18.40 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-60.0 sec  7.03 GBytes  1.01 Gbits/sec
~~~

- Gestion du firewalling --> Gestion classique, dans le sens Internet --> VM seulement.[[BR]]

### Scripts Google

Des scripts (Python et Shell) spécifique à Google sont installés sur les VM, dans /usr/share/google/, et des scripts d'init dans /etc/init.d/ et /etc/rc.local.

Deux démons en Python tournent en permanence :

* /usr/share/google/google_daemon/manage_accounts.py
* /usr/share/google/google_daemon/manage_addresses.py

Le premier s'assure que la liste des comptes Unix et leur clé SSH autorisée (~/.ssh/authorized_keys) sont bien présents sur le système, en comparant avec ceux déclarés dans l'interface web, et les recrée si besoin.
Le second gère les adresses IPv4 supplémentaires en gérant les fichiers dans /etc/network/interfaces.d/.

Les 2 scripts font appel à une API accessible en interne sur metadata.google.internal.

### Port SMTP bloqué

Le port réseau SMTP est bloqué en sortie sans possibilité de désactiver le blocage ! Il faut soit passer par un relai SMTP externe accessible sur un autre port, soit utilisé un service d'envoi autorisé par Google.
