---
title: Howto ClamAV
...

## Installation

~~~
# apt install clamav-daemon
# systemctl enable clamav-freshclam

# clamd -V
ClamAV 1.0.5/27440/Sun Oct 27 09:30:27 2024

# systemctl status clamav-daemon.service
● clamav-daemon.service - Clam AntiVirus userspace daemon
     Loaded: loaded (/lib/systemd/system/clamav-daemon.service; enabled; preset: enabled)
    Drop-In: /etc/systemd/system/clamav-daemon.service.d
             └─extend.conf
     Active: active (running) since Mon 2024-10-07 17:15:55 CEST; 2 weeks 6 days ago
TriggeredBy: ● clamav-daemon.socket
       Docs: man:clamd(8)
             man:clamd.conf(5)
             https://docs.clamav.net/
   Main PID: 120703 (clamd)
      Tasks: 4 (limit: 4673)
     Memory: 1.1G
        CPU: 14min 12.830s
     CGroup: /system.slice/clamav-daemon.service
             └─120703 /usr/sbin/clamd --foreground=true

# systemctl status clamav-freshclam
● clamav-freshclam.service - ClamAV virus database updater
     Loaded: loaded (/lib/systemd/system/clamav-freshclam.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-10-08 14:47:24 CEST; 2 weeks 5 days ago
       Docs: man:freshclam(1)
             man:freshclam.conf(5)
             https://docs.clamav.net/
   Main PID: 978994 (freshclam)
      Tasks: 1 (limit: 4673)
     Memory: 8.7M
        CPU: 2min 21.563s
     CGroup: /system.slice/clamav-freshclam.service
             └─978994 /usr/bin/freshclam -d --foreground=true
~~~

## Configuration

La configuration pour le démon ClamAV se trouve dans `/etc/clamav/clamd.conf`
et pour freshclam dans `/etc/clamav/freshclam.conf`.

~~~
/etc/clamav/
├── clamd.conf
├── freshclam.conf
├── onerrorexecute.d
├── onupdateexecute.d
└── virusevent.d
~~~

### Ecouter sur port TCP

Pour écouter sur un port TCP 3310 par exemple :

Il faut ajouter dans `/etc/clamav/clamd.conf` :

~~~
TCPSocket 3310
TCPAddr 127.0.0.1
~~~

ET autoriser dans Systemd :

~~~
# mkdir /etc/systemd/system/clamav-daemon.socket.d
# vim /etc/systemd/system/clamav-daemon.socket.d/tcp-socket.conf

[Socket]
ListenStream=3310

# systemctl daemon-reload
# systemctl restart clamav-daemon.socket
# systemctl status clamav-daemon.socket
# journalctl -u clamav-daemon.socket
~~~

## Mises à jour

Le démon `freshclam` tourne en permanence afin de récupérer régulièrement des mises à jour de la base de virus :

~~~
# freshclam -V
ClamAV 1.0.5/27449/Tue Nov  5 10:36:43 2024
# systemctl status clamav-freshclam
● clamav-freshclam.service - ClamAV virus database updater
     Loaded: loaded (/lib/systemd/system/clamav-freshclam.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-10-08 14:47:24 CEST; 4 weeks 0 days ago
       Docs: man:freshclam(1)
             man:freshclam.conf(5)
             https://docs.clamav.net/
   Main PID: 978994 (freshclam)
      Tasks: 1 (limit: 4673)
     Memory: 5.4M
        CPU: 3min 34.159s
     CGroup: /system.slice/clamav-freshclam.service
             └─978994 /usr/bin/freshclam -d --foreground=true

Nov 06 09:02:32  freshclam[978994]: Wed Nov  6 09:02:32 2024 -> Received signal: wake up
Nov 06 09:02:32  freshclam[978994]: Wed Nov  6 09:02:32 2024 -> ClamAV update process started at Wed Nov  6 09:02:32 2024
Nov 06 09:02:35  freshclam[978994]: Wed Nov  6 09:02:35 2024 -> daily.cld database is up-to-date (version: 27449, sigs: 2067669, f-level: 90, builder: raynman)
Nov 06 09:02:35  freshclam[978994]: Wed Nov  6 09:02:35 2024 -> main.cvd database is up-to-date (version: 62, sigs: 6647427, f-level: 90, builder: sigmgr)
Nov 06 09:02:35  freshclam[978994]: Wed Nov  6 09:02:35 2024 -> bytecode.cvd database is up-to-date (version: 335, sigs: 86, f-level: 90, builder: raynman)
Nov 06 10:02:35  freshclam[978994]: Wed Nov  6 10:02:35 2024 -> Received signal: wake up
Nov 06 10:02:35  freshclam[978994]: Wed Nov  6 10:02:35 2024 -> ClamAV update process started at Wed Nov  6 10:02:35 2024
Nov 06 10:02:36  freshclam[978994]: Wed Nov  6 10:02:36 2024 -> daily.cld database is up-to-date (version: 27449, sigs: 2067669, f-level: 90, builder: raynman)
Nov 06 10:02:36  freshclam[978994]: Wed Nov  6 10:02:36 2024 -> main.cvd database is up-to-date (version: 62, sigs: 6647427, f-level: 90, builder: sigmgr)
Nov 06 10:02:36  freshclam[978994]: Wed Nov  6 10:02:36 2024 -> bytecode.cvd database is up-to-date (version: 335, sigs: 86, f-level: 90, builder: raynman)
~~~

Sa configuration est dans `/etc/clamav/freshclam.conf` et en général il tente de mettre à jour la base de virus 1 fois par heure (cf ci-dessus).

La base dans de virus est stockée dans `/var/lib/clamav/` :

~~~
# ls -l /var/lib/clamav/
total 366076
-rw-r--r-- 1 clamav clamav    289733 Oct  7 16:33 bytecode.cvd
-rw-r--r-- 1 clamav clamav 204075520 Nov  5 11:01 daily.cld
-rw-r--r-- 1 clamav clamav        69 Oct  7 16:33 freshclam.dat
-rw-r--r-- 1 clamav clamav 170479789 Oct  7 16:33 main.cvd
~~~

D'après <https://blog.clamav.net/2021/03/clamav-cvds-cdiffs-and-magic-behind.html> :

* CLD : A CLD is the uncompressed ClamAV signature database archive. CLD files are created by FreshClam when a CVD or CLD database archive is updated with a CDIFF patch file.
* CVD : A CVD is an compressed ClamAV signature database archive that is signed and distributed by Cisco-Talos. "CVD" refers to the .cvd file extension, although when updated using a CDIFF database patch file, the extension is changed to .cld.


## Monitoring

~~~
$ /usr/lib/nagios/plugins/check_clamd -H /var/run/clamav/clamd.ctl -v
Using service CLAMD
Port: 3310
flags: 0x2
Send string: PING
server_expect_count: 1
        0: PONG
looking for [PONG] in beginning of [PONG
]
found it
received 6 bytes from host
#-raw-recv-------#
PONG

#-raw-recv-------#
CLAMD OK - 0.000 second response time on socket /var/run/clamav/clamd.ctl [PONG]|time=0.000347s;;;0.000000;10.000000


$ /usr/lib/nagios/plugins/check_file_age -w 86400 -c 172800 -f /var/lib/clamav/daily.cld
FILE_AGE OK: /var/lib/clamav/daily.cld is 84986 seconds old and 204012544 bytes | age=84986s;86400;172800 size=204012544B;0;0;0
~~~
 
## FAQ

### LibClamAV Warning

Si le ratio signal/bruit de la sortie de `clamscan` est trop élevé dû aux warnings de LibClamAV, il faut utiliser l'option `--stdout` et ignorer la stderr. Voici un exemple de commande :

```
clamscan --recursive --infected --stdout /home/ 2>/dev/null > /tmp/clamscan.report
```

### /var/lib/clamav/daily.cvd -> daily.cld

Auparavant la base était stockée dans `/var/lib/clamav/daily.cvd`, c'est désormais dans `/var/lib/clamav/daily.cld`


