**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Pulse Audio

~~~
$ pacmd
Welcome to PulseAudio! Use "help" for usage information.
>>> list-sources
>>> list-sinks

>>> set-default-source alsa_input.usb-Logitech_Logitech_B530_USB_Headset-00-Headset.analog-mono
>>> set-default-sink alsa_output.usb-Logitech_Logitech_B530_USB_Headset-00-Headset.analog-stereo
~~~

~~~
$ alsamixer --device pulse
$ pavucontrol
~~~