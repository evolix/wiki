**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Github

On fork un projet via l'interface web, puis récupère le fork :

~~~
$ git clone git@github.com:gcolpart/phpredis.git
~~~

On code, on code, on commit... puis on se resynchronise avec le projet principal :

~~~
$ git remote add upstream git://github.com/nicolasff/phpredis.git
$ git fetch upstream
$ git merge upstream/master (ou rebase ?)
~~~
