**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## PowerVault 3600i

### Client Linux

Le SAN n'est pas administrable par telnet, ssh, ou une interface web.
Il est donc obligatoire de passer par le client lourd de Dell, heureusement disponible pour Linux.

~~~
...
~~~

Le client utilise une forme d'auto-découverte des SAN disponibles sur le réseau, si plusieurs SAN sont présents,
il est possible de les faire clignoter un par un pour les identifier physiquement.

### Configuration

Le SAN récupère par défaut une IP en DHCP, il est ensuite possible de le paramétrer en IP fixe.
A partir du logiciel de gestion SMClient, sélectionner le SAN concerné, puis dans l'onglet configuration,
ouvrir la section "Configurer les ports de gestion Ethernet". Il est alors possible de configurer les
paramètres réseau de l'interface de gestion.

Une fois les volume logiques créés, il faut créer des hôtes ou des groupes d'hôtes dans l'onglet Adressage
afin de pouvoir y associer les volumes.
Les hôtes déjà connectés au SAN seront visible dans les menus de configuration, ce qui évite de devoir entrer
l'identifiant manuellement.
Lors de la création des hôtes, il faut que chaque nom soit unique.
Il est alors possible d'associer les volumes à un ou plusieurs hôtes, afin de les rendre visible comme des disques
locaux.

### Arrêt

Pour arrêter le SAN, pas la peine de chercher des heures dans l'interface de gestion, il faut arrêter ou 
déconnecter tous les hôtes utilisant les volumes montés sur le SAN et ensuite éteindre électriquement ce
dernier.

Il est possible de forcer la déconnexion des hôtes iSCSI dans l'interface de gestion : [[BR]]
    *Attention, cela revient à débrancher un disque en cours d'utilisation*[[BR]]
    Sur l'utilitaire de gestion, dans la fenêtre du SAN concerné, menu "Matrice de stockage", puis "iSCSI" et
    enfin "Terminer les sessions".

### Problèmes divers

Les interfaces iSCSI ne répondent pas (les diodes des ports Ethernet ne sont pas allumées) :
    Le SAN s'attend par défaut à être sur un réseau 10Gigabits, il faut donc forcer le Gigabits simple.
    Suite à un bug dans la version du manager qui nous était fournie il faut passer la langue de l'OS en
    anglais avant de relancer MDSM. Une fenêtre de configuration des interfaces iSCSI devrait apparaitre 
    et permettre la modification des débits.

Le SAN ventile à fond/fait un bruit énorme :
    Vérifier que les deux alimentations soient branchées et allumées.
    Le SAN utilise les ventilateurs des alimentations comme extraction, si les deux ne sont pas actives,
    il augmente la vitesse de celle restante pour éviter la surchauffe.
