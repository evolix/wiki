---
categories: network
title: Howto Keepalived
...

* Documentation : <https://keepalived.readthedocs.io/en/latest/introduction.html>
* Rôle Ansible : <https://gitea.evolix.org/evolix/ansible-roles/src/branch/stable/keepalived>

Keepalived est un service d'équilibrage de charge et de haute disponibilité. Il implémente le protocole [VRRP](/HowtoVRRP) permettant à plusieurs équipements sur un même segment réseau de partager une même adresse IP en gérant des états *master*/*backup*.

## Installation

~~~
# apt install keepalived
~~~

## Configuration

La configuration se fait dans `/etc/keepalived/keepalived.conf` :

~~~
vrrp_script chk_sshd {
        script "/usr/bin/pkill -0 sshd"
        interval 5
        weight -4
        fall 2
        rise 1
}

vrrp_instance vrrp {
        interface ens3
        virtual_router_id 67
        state MASTER
        priority 100

        virtual_ipaddress {
            192.0.2.5
        }

        authentication {
                auth_type PASS
                auth_pass XXXXXX
        }

        track_script {
                chk_sshd
        }
        notify /etc/keepalived/notify.sh
}
~~~

Le bloc `vrrp_script` indique des checks permettant de vérifier que le serveur est opérationnel, on peut en avoir plusieurs :

* `script` indique un script ou une commande à exécuter comme vérification. Ici, `pkill -0 sshd` va vérifier que sshd tourne, sans envoyer aucun signal au processus
* `interval` est l'intervalle en seconde à laquelle faire le check
* `weight` est la valeur de priorité à modifier. Ici, si le check échoue, 4 points de priorité sont levés
* `fall` est le nombre de fois que le check doit être en échec avant que le paramètre `weight` soit appliqué
* `rise` est le nombre de fois que le check doit être en succès avant d'inverser le paramètre `weight`

Le bloc `vrrp_instance` définit les paramètres VRRP :

* `interface` définit l'interface sur laquelle l'IP VRRP sera ajoutée
* `virtual_router_id` est l'identifiant VRID, qui doit être unique sur un même segment réseau
* `state` définit l'état dans lequel le process doit être au démarrage
* `priority` est la priorité utilisé pour élir le master : le plus haut sera master ; c'est cette valeur qui est modifié par le paramètre `weight`
* `virtual_ipaddress` est l'IP VRRP à configurer sur l'interface spécifiée par `interface`
* `authentication` permet d'avoir une authentification entre les nœuds VRRP ayant le même VRID
* `track_script` indique les scripts sur lesquels se baser pour vérifier que le serveur est opérationnel ou non
* `notify` indique un chemin vert un script qui permettra d'indiquer l'état VRRP du nœud, utile par exemple pour un check NRPE

## Firewall

Au niveau du pare-feu, il faut ajouter la règle suivante :

~~~
# iptables -A INPUT -s <adresse IP du pair> -d 224.0.0.18 -j ACCEPT
~~~

## NRPE

Un script `notify` peut être utilisé, par exemple dans `/etc/keepalived/notify.sh` :

~~~
#!/bin/bash
echo $1 $2 is in $3 state > /var/run/keepalive.state 
~~~

Le fichier contiendra par exemple le texte : « INSTANCE vrrp is in MASTER state »

Ensuite, ce [check NRPE check_keepalived](https://gitea.evolix.org/evolix/ansible-roles/raw/branch/stable/keepalived/files/check_keepalived) peut être utilisé. Il lira le contenu du fichier `/var/run/keepalive.state` et sera en succès ou en échec selon l'état `master` ou `backup` attendu (et configuré dans le check).

## Différences avec vrrpd

Concernant le protocole VRRP, le paquet [vrrpd](/HowtoVRRP) et le paquet keepalived ont une différence majeure : pour attribuer l'IP flottante virtuelle, keepalived n'utilise pas d'interface supplémentaire contrairement à vrrpd, mais il attribue l'IP comme alias sur l'interface parente. Cela a plusieurs conséquences :

* L'IP virtuelle n'est pas visible avec `ifconfig`, mais est visible avec `ip a`
* L'adresse MAC de l'IP virtuelle est celle de l'interface parente, sur la machine master, et change donc selon quelle est la machine master
* Les paramètres sysctl nécessaires au fonctionnement de vrrpd ne sont pas nécessaires à keepalived