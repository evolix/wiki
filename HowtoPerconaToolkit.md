---
categories: databases mysql mongodb
toc: yes
title: Howto Percona Toolkit
...

* Documentation: [docs.percona.com/percona-toolkit/](https://docs.percona.com/percona-toolkit/)
* Dépôt: [github.com/percona/percona-toolkit](https://github.com/percona/percona-toolkit)

Le « Percona Toolkit » est un ensemble d'outils édités par la société Percona, pour faciliter la gestion courante des serveurs de la famille MySQL (Oracle MySQL, MariaDB, Percona Server), mais aussi MongoDB.

## Installation

~~~
# apt install percona-toolkit

# dpkg -l percona-toolkit 
[…]
ii  percona-toolkit 3.2.1-1      all          Command-line tools for MySQL and system tasks
~~~

## Utilisation

Il y a de nombreux outils disponibles. Voici ceux que nous utilisons plus ou moins souvent.

La plupart dispose d'un ensemble d'options communes pour configurer la manière de connecter au service…

### pt-show-grants

_Normalise et montre les permissions MySQL pour faciliter leur réplication, leur comparaison ou leur versionnement._

Page de man : <https://docs.percona.com/percona-toolkit/pt-show-grants.html>

Conseils :

* Mettez cette commande dans votre routine de sauvegarde pour suivre l'évolution des permissions dans le temps.

### pt-mysql-summary

_Résumé Montre un résumé complet de l'état d'un service MySQL._

Page de man : <https://docs.percona.com/percona-toolkit/pt-mysql-summary.html>

Conseils :

* Mettez cette commande dans votre routine de sauvegarde pour suivre les variations dans le temps.
* Exécutez cette commande et garder la sortie avant de mettre à jour ou d'appliquer une nouvelle configuration, vous pourrez alors comparer avant/après.

### pt-kill

_Tue des connexions MySQL lorsqu'elles correspondent à des critères._

Page de man : <https://docs.percona.com/percona-toolkit/pt-kill.html>

Les critères peuvent être un type de requête, une durée d'exécution…

Il y a un mécanisme de filtrage assez avancé.

Il peut fonctionner de manière _attachée_ ou bien sous forme de démon.

### pt-stalk

_Collecte des données forensic à propos de MySQL lorsqu'un problème survient_

Page de man : <https://docs.percona.com/percona-toolkit/pt-stalk.html>

On lui indique un critère à surveiller (nombre de connexions actives, type de requêtes…) et lorsque le critère est satisfait il déclenche des actions à propos du système (état CPU, réseau…) et de MySQL (liste des connexions, état des variables…).

C'est très pratique pour collecter très réactivement des données lorsqu'un problème survient, avant que ces données ne soient plus disponibles (plantage, redémarrage auto…)

Il peut fonctionner de manière _attachée_ ou bien sous forme de démon.

Exemple pour lister (une seule fois) les processus actifs lorsqu'il y en a plus que 100 :

~~~
# pt-stalk --no-stalk --function status --variable=Threads_connected --threshold=100 --dest=/var/lib/pt-stalk --verbose 3 --mysql-only --iterations=1
~~~

### pt-table-checksum

_Vérifie l'intégrité d'une réplication MySQL_

Page de man : <https://docs.percona.com/percona-toolkit/pt-table-checksum.html>

L'outil vérifie l'intégrité de la réplication en effectuant des requêtes de checksum (crc32 par défaut) sur le primaire, puis les requêtes sont joués sur les réplicas permettant de trouver des différences.

La méthode la plus simple pour l'utiliser est d'autoriser le primaire à se connecter au réplica (authentification MySQL). Ainsi, il s'occupe lui-même de faire le nécessaire pour identifier les erreurs. Il suffira donc de lancer la commande sans argument pour qu'il identifie les incohérences. On pourra réaliser un cron avec l'argument `-q` qui ne fait remonter que les erreurs.

~~~
MAILTO=jdoe@example.com
42 9 * * 7 pt-table-checksum -q
~~~

Si on à l'erreur suivante :

~~~
CRC32 never needs BIT_XOR optimization at /usr/bin/pt-table-checksum line 6165.
~~~

Il faut jouer la commande avec l'option `--function MD5`, car le CRC32 n'est pas disponible dans MariaDB :

~~~
# pt-table-checksum --function MD5
~~~

Si sur le primaire, le format de binlogs n'est pas `STATEMENT`, le check va retourner un Warning, dû à des [limitations de l'outil](https://docs.percona.com/percona-toolkit/pt-table-checksum.html#limitations), il faut alors le lancer avec la commande `--no-check-binlog-format` :

~~~
# pt-table-checksum --no-check-binlog-format --function MD5
~~~

Si on veut afficher seulement les différences, on peut utiliser l'option `--replicate-check-only` exemple avec un `pt-table-checksum` sur une base en particulier :

~~~
# pt-table-checksum --replicate-check-only --databases foo
~~~

### pt-table-sync

_Rétabli la synchronisation entre des tables MySQL_

Page de man : <https://docs.percona.com/percona-toolkit/pt-table-sync.html>

Si `pt-table-checksum` vous a remonté des incohérences, vous pouvez avec cet outil les corriger. Cela va identifier les différences et les corriger avec un `REPLACE` sur le primaire (qui sera donc répliqué sur le réplica), garantissant la cohérence des données.

Exemple :

~~~
# pt-table-sync --print --replicate percona.checksums localhost
# pt-table-sync --execute --replicate percona.checksums localhost
~~~

En cas de `Can't make changes on the master because no unique index exists`. On peut synchroniser directement les différences sur le slave depuis le master.

~~~
# pt-table-sync --execute --no-check-slave localhost slave
~~~

### pt-query-digest

_Analyse des requêtes SQL depuis des logs, listes de processus ou traces tcpdump._

Page de man : <https://docs.percona.com/percona-toolkit/pt-query-digest.html>

**ATTENTION** : Si le fichier de slow query est trop lourd, il n'est pas recommandé de l'analyser sur un serveur de production, car l'analyse peut nécessiter un cœur CPU à 100%, pendant quelques minutes.

Cet outil analyse le fichier `mysql-slow.log` et fait un rapport général des requêtes qui prennent le plus de temps, par temps de réponse, pourcentage d’exécution, nombres d'appels et le type de requête.

~~~
# pt-query-digest /var/log/mysql/mysql-slow.log

# Profile
# Rank Query ID           Response time  Calls R/Call  V/M   Item
# ==== ================== ============== ===== ======= ===== =============
#    1 0x67A347A2812914DF 830.5323 62.8%    41 20.2569 87.06 SELECT foo_bar
#    2 0xC5C7772FD65D8C64 141.2796 10.7%    35  4.0366  0.07 SELECT lignes commandes lignes
#    3 0x6DEF8BB001FA8845 139.9141 10.6%     7 19.9877  0.04 SELECT commandes commandes_remboursements vendeurs_factures_remises paiements livraisons commandes_statut commandes_groupes acheteurs codes_promos adresses pays adresses pays vendeurs adresses pays
#    4 0xC8A44AAD56105646  41.5405  3.1%    17  2.4436  0.02 SELECT produits_tendances vendeurs declinaisons texteproduits produits_tendances
#    5 0xFD38100E607E1331  26.7823  2.0%     2 13.3912  5.07 SELECT OUTFILE commandes_etiquettes
#    6 0xFD7F7A9702EE1C3C  21.4709  1.6%     6  3.5785  0.00 UPDATE commandes
#    7 0xE63D0402530F63BE   9.5435  0.7%     4  2.3859  0.00 UPDATE commandes
#    8 0x4EEE742BBDA4EE47   8.9621  0.7%     2  4.4810  0.02 SELECT OUTFILE vendeurtracking
#    9 0xB28EA31EC4438F2F   8.6173  0.7%     2  4.3087  0.02 SELECT OUTFILE lignes
#   10 0xFFAD96608E3F459B   6.7241  0.5%     2  3.3620  0.38 SELECT produits_tendances vendeurs declinaisons texteproduits produits_tendances
~~~

### pt-heartbeat

_Mesure la latence de réplication MySQL_

Page de man : <https://docs.percona.com/percona-toolkit/pt-heartbeat.html>

Pour mesurer la latence de mabière plus réaliste que le classique "retard de réplication", l'outil insère une valeur datée de l'heure actuelle (timestamp) sur le primaire et compare l'information avec celle trouvée dans le réplica.

~~~
# mysql -e "CREATE DATABASE percona;"
# mysql -e "GRANT ALL PRIVILEGES ON \`percona\`.* TO 'percona'@'%' IDENTIFIED BY 'password';"
# mysql -e "GRANT REPLICATION CLIENT ON *.* TO 'percona'@'%';"
# adduser --disabled-password percona
# vim /home/percona/.my.cnf

[client]
user = percona
password = password

# chmod 600 /home/percona/.my.cnf
# chown percona: /home/percona/.my.cnf
# pt-heartbeat --defaults-file /home/percona/.my.cnf --create-table --database percona --table heartbeat --update
~~~

Le lancer en démon :

~~~
# pt-heartbeat --defaults-file /home/percona/.my.cnf --create-table --database percona --table heartbeat --update --daemonize
~~~

Le mettre dans une unité systemd `/etc/systemd/system/pt-heartbeat.service` :

~~~
[Unit]
Description=Check slave lag.
After=network.target mysql.service

[Service]
User=percona
ExecStart=/usr/bin/pt-heartbeat --defaults-file /home/percona/.my.cnf --create-table --database percona --table heartbeat --update
Type=simple
Restart=always

[Install]
WantedBy=default.target
~~~

Consulter la latence sur le slave :

~~~
# pt-heartbeat -defaults-file /home/percona/.my.cnf --create-table --database percona --table heartbeat --check
~~~

On pourra ensuite surveiller en temps réel la latence ou écrire un cron de surveillance, voir un check Nagios.

~~~
# pt-heartbeat -defaults-file /home/percona/.my.cnf --create-table --database percona --table heartbeat --monitor
~~~