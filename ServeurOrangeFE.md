---
categories: web hosting
title: Howto Orange Flexible Engine
...

* Documentation : <https://docs.prod-cloud-ocb.orange-business.com/index.html>
* Accès via : <https://selfcare.cloud.orange-business.com/>
* Numéro de téléphone support : 01 85 14 92 44

# Création d'une VM

Dans le cas de la création d'une simple machine, nous aurons besoin d'utiliser les 3 sections suivantes de la console technique

![Console FE](/FE_console.png)

## Création d'un ECS

Dans le cadre de la plateforme Orange FE, un serveur virtuel est appelé un ECS. Pour créer un ECS on se rendra dans la catégorie située à gauche sur la capture précédente.

![Aperçu section ECS](/FE_ECS.png)

Une fois rendu dans la catégorie ECS, on cliquera sur « + Create ECS » en haut à droite.

Sur l'écran suivant en dehors de certains paramètres à choisir, il faudra notamment cliquer en face de « EIP » sur « Automatically assign » et glisser le curseur sur une valeur raisonnable (minimum 100Mbit/s). Cela permettra à notre serveur de pouvoir communiquer avec l’extérieur.

Une fois la configuration finalisée on pourra cliquer sur « Create Now ». Dans la liste des ECS on pourra voir l'IP publique qui aura été attribuée à notre serveur virtuel.

## Disque

Lors de la création d'un ECS un volume est automatiquement attribué. On peut, en revanche, vouloir ajouter un disque supplémentaire. Pour créer un disque on se rendra dans la catégorie située au centre sur la première capture en haute de cet article.

![Aperçu section disque](/FE_Disque.png)

Une fois rendu dans la catégorie Elastic Volume Service, on cliquera sur « + Create Disk » en haut à droite.

## VCP et Security Group

Lorsque l'on crée un VPC (crée tout seul lors de l'initialisation), il faudra penser à autoriser SSH et/ou ICMP dans le security group. Pour se rendre dans la catégorie on cliquera sur « Virtual Private Cloud » à droite sur la première capture en haute de cet article.

Une fois rendu dans la catégorie, on cliquera sur « Security Group » dans la colonne de gauche puis sur « default ».

## V2V/P2V

Lorsqu'il est question de réaliser un P2V ou V2V il sera alors nécéssaire de créer 2 ECS(VMs) car les disques attachés à posteriori ne permettent pas une utilisation système.

Il faudra donc une fois les deux ECS créés, détaché le disque de l'un des deux pour l'attaché sur l'autre. L'un des ECS se verra donc doté de son disque et de celui de l'autre ECS. On pourra alors réaliser la synchronisation. Une fois cette tâche terminée, on pourra rattacher le disque sur l'autre ECS. L'ECS qui aura servi à la synchronisation pourra alors être détruit.

## reverse DNS

On peut modifier le reverse DNS d'une EIP (Elastic IP) en allant dans `Service List > Network > Domain Name Service` où l'on peut alors créer des **PTR Records**.