---
categories: web hosting
title: Howto Microsoft Azure
...

* Documentation : <https://docs.microsoft.com/fr-fr/azure/virtual-machines/linux/>
* Accès via : <https://portal.azure.com/>

# Création d'une VM

Comme la plupart des solutions Cloud (AWS, GCE, Scaleway, …), plusieurs modules sont à disposition, comme les VM. Azure utilise clairement le terme machine virtuelle.
À la création d'une VM on doit définir via plusieurs étapes :

## Options de base

- L'abonnement et le groupe de ressources (concerne la facturation) ;
- Le nom de la VM ;
- La région (France, USA, …) ;
- L'image (Debian, Ubuntu, …) ;
- La taille (CPU, RAM, Disque) ;
- Connexion par clé ou mot de passe ;

## Disque

On pourra choisir le disque système (SSD Standard, SSD Premium ou HDD) et en créer d'autres. On préconise de créer des /home et/ou /srv.

## Mise en réseau

Si nécessaire on pourra mettre la VM dans un autre réseau virtuel que celui de base.  
On pourra aussi choisir un groupe de sécurité avancé pour écrire ses propres règles de pare-feu.

## Paramètres avancés

Permet d'ajouter des extensions, ce sont finalement des logiciels préinstallés tel que Datadog.  
On pourra aussi ajouter des scripts cloud-init.

# Documentation utile

## Déconnecter un disque

Pour le supprimer ou l'attacher sur une autre VM : <https://docs.microsoft.com/fr-fr/azure/virtual-machines/linux/detach-disk#detach-a-data-disk-using-the-portal>
