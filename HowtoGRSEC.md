**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto GRSEC

### Enlever une protection

Installation des paquets nécessaires :

~~~
# aptitude install chpax
~~~

Pour une lib :

~~~
# execstack -c /usr/lib/libfoo.so
~~~

Pour un binaire :

~~~
# chpax -permsx /usr/local/bin/wkhtmltopdf
~~~