---
title: Howto MongoDB
categories: databases nosql
---

* Documentation : <https://docs.mongodb.com/manual/>
* Référence de configuration : <https://docs.mongodb.com/manual/reference/configuration-options/>
* Statut de cette page : test / bullseye

[MongoDB](https://www.mongodb.com) est un système de base de données de type NoSQL, orienté gestion de documents.

## Installation

Nous installon MongoDB et lançons le service, sous Debian (testé sous Debian 11 et 12) ainsi :

~~~
# version="7.0"
# echo "deb [signed-by=/etc/apt/trusted.gpg.d/mongodb.asc] http://repo.mongodb.org/apt/debian $(awk -F= '/VERSION_CODENAME/{print $2}' /etc/os-release)/mongodb-org/${version} main" >> /etc/apt/sources.list.d/mongodb-org-"${version}".list
# wget -O /etc/apt/trusted.gpg.d/mongodb.asc https://www.mongodb.org/static/pgp/server-"${version}".asc
# chmod 644 /etc/apt/trusted.gpg.d/mongodb.asc

# apt update && apt install mongodb-org

# systemctl enable --now mongod
# systemctl status mongod
● mongod.service - MongoDB Database Server
     Loaded: loaded (/lib/systemd/system/mongod.service; disabled; vendor preset: enabled)
     Active: active (running) since Fri 2023-10-27 10:25:34 CEST; 1s ago
       Docs: https://docs.mongodb.org/manual
   Main PID: 111550 (mongod)
     Memory: 20.1M
        CPU: 645ms
     CGroup: /system.slice/mongod.service
             └─111550 /usr/bin/mongod --config /etc/mongod.conf
# mongod --version
db version v7.0.2
Build Info: {
    "version": "7.0.2",
    "gitVersion": "02b3c655e1302209ef046da6ba3ef6749dd0b62a",
    "openSSLVersion": "OpenSSL 1.1.1n  15 Mar 2022",
    "modules": [],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "debian11",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}
~~~

> *Note* : à partir de Debian 10 (Buster), MongoDB n'est plus distribué dans les dépôts officiels de Debian. Pour les versions 3.6 et 4.0 il est préférable d'utiliser Debian 9, et MongoDB supporte Debian 10 à partir de sa version 4.2.1

Tous les détails sont disponibles sur <https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/>.


## Configuration

La configuration de MongoDB se trouve dans le fichier `/etc/mongod.conf` qui est un fichier au format YAML.
Il n'y a pas d'autres fichiers de configuration.

Pour recharger la configuration, il faut restart le service :

~~~
$ systemctl restart mongod
~~~


### Logrotate

Les paquets fournis par mongodb.org n'ont pas de configuration pour logrotate !
On peut simplement ajouter cette configuration dans `/etc/logrotate.d/mongod`

~~~
/var/log/mongodb/mongod.log {
    daily
    missingok
    rotate 365
    dateext
    compress
    delaycompress
    notifempty
    sharedscripts
    postrotate
        kill -USR1 $(cat /var/lib/mongodb/mongod.lock)
    endscript
}
~~~

Il faut aussi penser à modifier la configuration (`/etc/mongod.conf`) pour forcer le mode _reopen_ de la directive `logRotate` :

~~~
systemLog:
  destination: file
  logRotate: reopen
  logAppend: true
  path: /var/log/mongodb/mongod.log
~~~

Il y a tout un chapitre (pas toujours très clair) sur la manière de gérer la rotation de logs : <https://docs.mongodb.com/manual/reference/command/logRotate/>.

> **Note** : Attention, par défaut mongodb log les requêtes lentes de plus de 100ms. Ça peut générer beaucoup de logs… On pourra les modifier avec [slowOpThresholdMs](https://docs.mongodb.com/manual/reference/configuration-options/#operationProfiling.slowOpThresholdMs).


### Authentification

<https://docs.mongodb.com/manual/core/authentication/>

MongoDB a deux modes de fonctionnement pour l'authentification :

* Un mode sans aucune authentification
* Un mode avec authentification

#### Mode sans aucune authentification

Il s'agit du mode par défaut.

La configuration sans authentification se fait ainsi :

~~~
security:
  authorization: disabled
~~~

Dans ce mode, il n'y a besoin d'aucun login / mot de passe pour accéder à l'ensemble des dbs, données, etc.
Ce mode est donc assez dangereux : il faut s'assurer que seuls des programmes de confiance accèdent
au port TCP de MongoDB.

#### Mode avec authentification

L'authentification avec MongoDB peut paraître surprenante à première vue. En effet chaque utilisateur est associé à une base pour l'authentification. Mais les privilèges de l'utilisateur peuvent aller au-delà de la base de données qui sert à l'authentification.
Aussi, un utilisateur, pour s'authentifier, a besoin de son login, de son mot de passe **et** du nom de sa base d'authentification.

> **Note** : Il peut donc y avoir plusieurs utilisateurs `foo` avec des privilèges différent. Ce qui les distinguera sera la base d'authentification

Par convention, on définira un utilisateur d'administration `mongoAdmin` dans la base `admin`

Pour activer authentification dans les réglages du service MongoDB, il faut alors passer la directive `authorization` à enabled, dans

~~~
security:
  authorization: enabled
~~~

Il n'est pas forcément nécessaire de créer un compte avec un rôle administrateur avant d'activer l'authentification.
En effet, en l'absence de comptes utilisateurs, MongoDB va accepter les connexions *locales* sans authentification pour permettre notamment de créer un compte.

~~~
$ mongosh # mongo sur les anciennes versions
test> use admin

admin> db.createUser(
  {
    user: "mongoAdmin",
    pwd: "PASSWORD",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
Successfully added user.

admin> db.system.users.find()
[
  {
    _id: 'admin.mongoAdmin',
    userId: new UUID("4efb0723-ca57-4cb7-b2ba-221aeb8cae86"),
    user: 'mongoAdmin',
    db: 'admin',
    credentials: [...],
    roles: [ { role: 'userAdminAnyDatabase', db: 'admin' } ]
  }
]
~~~

C'est seulement une fois ce premier utilisateur `mongoAdmin` créé que l'authentification est activée !
Si aucun utilisateur n'est crée, l'authentification reste inactive.

On peut ensuite se connecter à la base `admin` ainsi :

~~~
$ mongosh admin -u mongoAdmin -p
Enter password:
~~~

> *Note*: On peut spécifier `-p <PASSWORD>`, mais il sera visible dans votre historique shell et dans la liste des processus.

Pour les accès aux bases, les utilisateurs sont stockés dans la collection _system.users_ propre à chaque base.


### Mémoire

La mémoire utilisée par Mongo se répartit en deux types :

* La mémoire résidente : celle nécessaire au stockage des bases de données, requêtes, indexes.
* La mémoire cache.

**MongoDB n'a pas de plafonnement de la mémoire totale utilisée. Il considère qu'il est le seul service sur le serveurs (!).**

Par défaut, il limite la taille de son cache à la moitié de la (RAM - 1G), avec un minimum de 256M. Par exemple, sur un serveur avec 6G, il limitera son cache à `(6-1)/2 = 2.5G`.

Le problème, c'est que si la mémoire résidente consomme la moitié de la RAM (ou plus), même ponctuellement, on se retrouve avec des erreurs `tcmalloc: allocation failed` dans `/var/log/syslog` et d'autres service qui se font OOM kill.

Dans ce cas, on peut limiter le processus avec les cgroups dans Systemd :

~~~
# systemctl edit mongod.service
[Service]
Restart=always
MemoryHigh=xG # RAM * 0.7
MemoryMax=yG # RAM * 0.8
~~~

Attention, il peut aussi être nécessaire de désactiver les directives legacy présentes dans l'unité systemd, sans quoi les nouvelles peuvent ne pas être prises en compte :

~~
LimitFSIZE=
LimitCPU=
LimitAS=
LimitNOFILE=
LimitNPROC=
LimitMEMLOCK=
~~~

~~~
# systemctl restart mongod.service
# systemctl status mongod.service
● mongod.service - MongoDB Database Server
(…)
    Memory: 1G (high: xG max: yG)
~~~


## Accès en CLI

Comme pour MySQL, on peut manipuler MongoDB en CLI en mode interactif avec la commande `mongosh`.

```
$ mongosh admin -u mongoAdmin -p
Enter password:
```

Ici, pour se connecter à la base locale `admin`. Comme la base d'authentification n'est pas spécifiée, ça sera la base à laquelle on se connecte (ici `admin`) qui servira pour l'authentification.

> *Note*: Pour vous connecter à une base différente de celle d'authentification, on pourra spécifier le nom de cette base avec l'argument `--authenticationDatabase XXX`


## Gestion des utilisateurs

Rappel : chaque utilisateur est associé à une base.


### Lister les utilisateurs

~~~
> use admin
admin> db.system.users.find()
{ "_id" : "admin.mongoAdmin", "userId" : UUID("3d475ec9-b82c-4312-8ce6-399bcbe3ca7a"), "user" : "mongoAdmin", "db" : "admin", "credentials" : { "SCRAM-SHA-1" : { "iterationCount" : 10000, "salt" : "lO6VBPnKD0dC5TwRcCJwZg==", "storedKey" : "AUUwnA6v/8LbSEUpDU5EXQmp9sY=", "serverKey" : "DjHukfkoW0QE5Zy/5plD9FLrsu8=" }, "SCRAM-SHA-256" : { "iterationCount" : 15000, "salt" : "fqRp+/wrOGTyk1hiTn4RoPQr4jWVlqw79GsD4g==", "storedKey" : "/aaz0CJHlFfFDD5bSBPOBCnipn07ezx+X0pHFABeOV0=", "serverKey" : "W6BOf07RoYytpgJc4KOoTTTID7qJ1iah9Wqwub9xjgE=" } }, "roles" : [ { "role" : "userAdminAnyDatabase", "db" : "admin" } ] }
~~~


### Créer un utilisateur

~~~
> use myApp;
myApp> db.createUser(
  {
    "user" : "myAppOwner",
    "pwd" : "PASSWORD",
    roles: [
        { "role" : "dbOwner", "db" : "myApp" }
    ]
  }
)
~~~


### Changer le mot de passe d’un utilisateur

~~~
# apg -n1 -m23
# mongosh …
> use admin
admin> db.changeUserPassword("my_user", passwordPrompt())
~~~

Sans _prompt_ :

~~~
> use admin
admin> db.changeUserPassword("my_user", "my_password")
~~~


### Supprimer un utilisateur

Supprimons l'utilisateur `foo` de la base admin :

~~~
> use admin
admin> db.dropUser('foo')
true
~~~


## Gestion des bases de données

### Lister les bases de données

~~~
> show dbs
admin   0.000GB
config  0.000GB
foo     0.000GB
local   0.000GB
~~~


### Lister les collections d'une base de données

~~~
> use foo
switched to db foo

foo> show collections
bar
baz
~~~


### Créer une base de données

Il n’y a pas de commande équivalente à `CREATE DATABASE` dans MongoDB. Une base de données est effectivement créée lorsque des données sont insérées dans la base.

Pour créer une base de données, il faut s’y attacher avec la commande `use <DB>` puis ajouter des données dans une collection :

```
> use math
math> db.constants.insert({name: "e", value: "2.7182818"})
```

Les commandes précédentes permettent d’ajouter un élément à une collection `constants` dans la base `math`.
`name` et `value` sont des chaînes de caractères quelconques et non des mots-clés.


### Supprimer une base de données

Pour supprimer la base foo : 

~~~
> use foo
foo> db.runCommand( { dropDatabase: 1 } )
{
        "dropped" : "foo",
        "ok" : 1
}
~~~


## Autres commandes diverses

Voici quelques opérations de base

~~~
> db.bar.find()
> db.bar.save( { a : 1 } )
> db.bar.save( { a : 2 } )
> show collections
bar
system.indexes
> db.bar.find()
{ "_id" : ObjectId("4e64dbf619a27977131978ad"), "a" : 1 }
{ "_id" : ObjectId("4e64dbf919a27977131978ae"), "a" : 2 }
> db.foo.find({a:1})
{ "_id" : ObjectId("4e0674a47888d8941034f4e1"), "a" : 1 }
> db.foo.find().limit(1)
{ "_id" : ObjectId("4e0674a47888d8941034f4e1"), "a" : 1 }
> exit
bye
~~~

Quelques commandes utiles :

Voir le compteur des connexions :

~~~
db.serverStatus().connections
~~~

Modifier un champ d'un objet existant :

~~~
> db.bar.save( { a : 4, b : 0 } )
> db.bar.update( { "_id" : ObjectId("4e64de8a19a27977131978b0") } , { $set : {b:1} } )
> db.bar.update( { "_id" : ObjectId("4e64de8a19a27977131978b0") } , { $unset : {b:1} } )
~~~

Voir uniquement certains champs :

~~~
> db.bar.find({a:1},{flip:1})
> db.bar.find({a:1},{flip:1,flap:1})
~~~

Voir plus d'objets (en évitant le _has more_) :

~~~
> DBQuery.shellBatchSize = 100
> db.foo.find()
~~~

Requête sur des dates :

~~~
db.foo.find( { "date": {"$gte": ISODate("2011-12-05T00:21:00Z")} } )
db.foo.find( { "date": {"$lt": ISODate("2011-12-05T04:00:00Z")} } )
db.foo.find( { "date": {"$gte": ISODate("2011-12-05T00:21:00Z"), "$lt": ISODate("2011-12-05T04:00:00Z")} } )
~~~

En PHP :

~~~
$m = new Mongo();
$db = $m->foo;
$bar = $db->bar;
$bar->insert(array("a"=>"1","a"=>"2"));
$res = $bar->find();
foreach ($res as $obj) {
    echo $obj["a"] . "\n";
}
~~~


## Sauvegarde via snapshots du système de fichier 

Voir <https://docs.mongodb.com/manual/tutorial/backup-with-filesystem-snapshots/>


## Sauvegarde et restoration avec `mongodump` et `mongorestore`

Il existe plusieurs stratégies possibles de sauvegarde, comme détaillées dans la documentation de MongoDB : <https://docs.mongodb.com/manual/core/backups/>

Si l'authentification est activée, on crée un compte dédié à la sauvegarde et à la restauration, qu'on appelle `mongoBackup`, et auquel on donne les rôles built-in `backup` et `restore` :

~~~
# mongosh admin -u mongoAdmin -p
admin> db.createUser(
  {
    user: "mongoBackup",
    pwd: "PASSWORD",
    roles: [ "backup", "restore" ]
  }
)
~~~


### Sauvegarde

`mongodump` permet de créer des sauvegardes dans des fichiers au format BSON utilisables par la commande `mongorestore`.

> **Important**: `mongodump` sauvegarde uniquement les documents d'une base. Cela signifie qu'après leur ré-injections, il y aura une phase de reconstructions des index de la base de données (qui sera plus ou moins longue en fonction de la quantité de données et des performances de la machine).

Pour sauvegarder :

~~~
# mkdir -p /home/backup/mongodump/
# mongodump -u mongoBackup -p <PASSWORD> --authenticationDatabase admin --out /home/backup/mongodump/
Enter password:
~~~

MongoDB écrit la sauvegarde dans `/home/backup/mongodump/`, avec un dossier par base.

Il est possible de générer un dump compressé avec l'option `--gzip`. Lors de l'injection de la sauvegarde avec `mongorestore` il faudra alors préciser à nouveau cet argument.


### Restauration

Pour restaurer une base :

~~~
# cd /home/backup/mongodump/
# ls
# mongorestore -u mongoBackup -p <PASSWORD> --authenticationDatabase admin --db <dbname> <dump_dir>
Enter password:
~~~

> **Note** : Si le dump est compressé, il faut ajouter l'option `--gzip` à la commande.

Pour restaurer complètement MongoDB (si le `datadir` est cassé par exemple), il faut recréer `/var/lib/mongodb` et [désactiver temporairement l'authentification](#authentification), car on ne peut plus lire la base `admin` pour s'authentifier :

~~~
# systemctl stop mongod
# mv /var/lib/mongodb /var/lib/mongodb.bak
# install -d -o mongodb -g mongodb /var/lib/mongodb
# vim /etc/mongod.conf
security:
-  authorization: enabled
+  authorization: disabled
# systemctl start mongod
# systemctl status mongod
~~~

Restaurer les bases de données :

~~~
# for db in $(ls /home/backup/mongodump/); do
>   mongorestore --db "$db" /home/backup/mongodump/"$db"
> done
~~~

Réactiver l'authentification :

~~~
# vim /etc/mongod.conf
security:
+  authorization: enabled
-  authorization: disabled
# systemctl restart mongod
# systemctl status mongod
~~~

Puis vérifier que vous arrivez bien à vous connecter à Mongo.


## Réplication avec replica set

La réplication avec "replica set" nécessite au moins 3 machines. En effet, la sélection d'un nœud primaire où se produisent les
écritures se fait au travers d'un processus d'élection. Les nœuds secondaires sont seulement utilisables en lecture.

> **Note**: Si pour une quelconque raison, on ne peut avoir que deux nœuds mongodb supportant des données. On peut alors, introduire dans le cluster, un nœud de type 'arbitre'. Celui-ci doit bien être situé sur une machine indépendante des deux autres nœuds mongodb.
il ne supportera aucune donnée dans son datadir local, mais participera a l'élection du nœud primaire.

### En Version 3.2+

Si vos mongod ne sont pas sur un LAN il est recommandé de mettre en place du TLS avec <https://docs.mongodb.com/manual/tutorial/configure-x509-member-authentication/.>
Sinon on pourra utiliser un keyfile <https://docs.mongodb.com/manual/tutorial/deploy-replica-set-with-keyfile-access-control/>

> **Attention** : Le datadir des nœuds à ajouter à un replica set doit être vierge.*

#### Avec keyfile & authentification

Créer le keyfile sur une machine et le déployer sur les autres membres du replica set

~~~
# openssl rand -base64 755 > /etc/mongod.key
# chmod 400 /etc/mongod.key
# chown mongodb: /etc/mongod.key
~~~

Modifier la configuration :

~~~
security:
  keyFile: /etc/mongod.key

replication:
  replSetName: rs0
~~~

On va alors démarrer mongodb sur la machine qui sera initialement "primaire".
C'est via celle-ci que l'on va amorcer le cluster.

~~~
# systemctl restart mongod
~~~

Se connecter pour créer un compte administrateur *mongoAdmin* avec les roles `userAdminAnyDatabase` et `clusterAdmin`

~~~
# mongo

> use admin
> db.createUser(
  { 
    user: "mongoAdmin",
    pwd: "PASSWORD",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" },
             { "role" : "clusterAdmin", "db" : "admin" } ]
  }
)

~~~

On peut alors modifier la configuration pour activer l'authentification, et redémarrer le mongodb primaire.
Et démarrer tous les autres membres

~~~
security:
  keyFile: /etc/mongod.key
  authorization: enabled
~~~

Sur le nœud primaire, on va amorcer le cluster :

~~~
# mongo -u mongoAdmin -p
Enter password:

> rs.initiate()
~~~

On peut ensuite, ajouter les membres seconaires du cluster :


```
> rs.add("192.0.2.3")
```

Si on soutaite ajouter un membre de typer *arbitre* :

```
> rs.addArb("192.0.2.100")
```


Le Replica Set est maintenant en place.

Pour s’y connecter, il faudra spécifier une URL de cette forme : `mongodb://<host1>:27017,<host2>:27017,<host3>:27017/?replicaSet=<mon_replica_set>`. Par exemple :

```
mongosh -u someuser mongodb://10.0.0.1:27017,10.0.0.2:27017,10.0.0.3:27017/?replicaSet=rs0
```

Avec cette URL, le _shell_ `mongosh` survivra au cas où le serveur primaire devient indisponible et qu’une nouvelle élection à lieu.


### État du _replica set_

```
> rs.status()
```

#### Avec X.509

TODO


### Sortir un mongo de la réplication

Voici les étapes pour sortir un mongo de la réplication :

1. Sauvegarder (mongodump)
1. Stopper MongoDB
1. Sauvegarder les datas
1. Supprimer les fichiers local.*
1. Désactiver les replica dans la configuration
1. Relancer MongoDB

### Resynchroniser un mongo slave

En cas de soucis, il peut être nécessaire de resynchroniser complètement un serveur MongoDB
par rapport à un master. Il suffit de supprimer complètement le _dbpath_ :

~~~
# /etc/init.d/monogdb stop
# mv /var/lib/mongodb /var/lib/mongodb.old
# mkdir /var/lib/mongodb
# chown mongodb:mongodb /var/lib/mongodb
# chmod 750 /var/lib/mongodb
# /etc/init.d/monogdb start
~~~

Le serveur slave repassera en RECOVERING puis en SECONDARY :

~~~
RECOVERING> db.printReplicationInfo()
this is a slave, printing slave replication info.
source:   master.example.com:27017
         syncedTo: Thu Jan 01 1970 01:00:00 GMT+0100 (CET)
                 = 1327878874 secs ago (368855.24hrs)
source:   otherslave.example.com:27017
         syncedTo: Mon Jan 30 2012 00:14:09 GMT+0100 (CET)
                 = 25 secs ago (0.01hrs)
SECONDARY> db.printReplicationInfo()
configured oplog size:   1025.1384765625MB
log length start to end: 741secs (0.21hrs)
oplog first event time:  Mon Jan 30 2012 00:14:09 GMT+0100 (CET)
oplog last event time:   Mon Jan 30 2012 00:26:30 GMT+0100 (CET)
now:                     Mon Jan 30 2012 00:27:26 GMT+0100 (CET)
~~~

### Erreurs de synchronisation

En cas d'erreur du type :

~~~
    "optimeDate" : ISODate("2012-01-29T21:34:07Z"),
    "lastHeartbeat" : ISODate("2012-01-29T23:02:43Z"),
    "pingMs" : 11,
    "errmsg" : "syncTail: 0 assertion db/pdfile.cpp:1881, syncing: { ts: Timestamp 1327872847000|109, h: -1582883220170752094, op: \"i\", ns: \"DB_MONGODB.tmp.mr.COLL_MONGODB_396779\", o: { _id: \"#colorsplash\", value: 1.0 } }"
~~~

Côté slave, on a plus de détails :

~~~
[rsSync] replHandshake res not: 1 res: { ok: 1.0 }
[rsSync]  local.oplog.rs Assertion failure !loc.isNull() db/pdfile.cpp 1881
0x57eeb6 0x589d6b 0x8a2746 0x82f486 0x821d8d 0x8231d8 0x82439a 0x824820 0xaa4560 0x7f1a33ef08ba 0x7f1a334ac02d
 /usr/bin/mongod(_ZN5mongo12sayDbContextEPKc+0x96) [0x57eeb6]
 /usr/bin/mongod(_ZN5mongo8assertedEPKcS1_j+0xfb) [0x589d6b]
 /usr/bin/mongod() [0x8a2746]
 /usr/bin/mongod(_ZN5mongo11_logOpObjRSERKNS_7BSONObjE+0x236) [0x82f486]
 /usr/bin/mongod(_ZN5mongo11ReplSetImpl8syncTailEv+0xced) [0x821d8d]
 /usr/bin/mongod(_ZN5mongo11ReplSetImpl11_syncThreadEv+0xc8) [0x8231d8]
 /usr/bin/mongod(_ZN5mongo11ReplSetImpl10syncThreadEv+0x4a) [0x82439a]
 /usr/bin/mongod(_ZN5mongo15startSyncThreadEv+0xa0) [0x824820]
 /usr/bin/mongod(thread_proxy+0x80) [0xaa4560]
 /lib/libpthread.so.0(+0x68ba) [0x7f1a33ef08ba]
 /lib/libc.so.6(clone+0x6d) [0x7f1a334ac02d]
[rsSync] replSet syncTail: 0 assertion db/pdfile.cpp:1881, syncing: { ts: Timestamp 1327872847000|109, h: -1582883220170752094, op: "i", ns: "DB_MONGODB.tmp.mr.COLL_MONGODB_396779", o: { _id: "#colorsplash", value: 1.0 } }
~~~

Cela semble être une corruption de la base côté slave...
L'une des solutions consiste à resynchroniser complètement la base
(voir plus haut).


### Forcer à changer de master

~~~
> rs.stepDown()
~~~

/!\ à ne faire que si les slaves sont synchros sous peine
d'avoir un état rollback & ennuis...

### Etat ROLLBACK

Master devient indispo alors que les slaves n'était pas 100% synchro

<http://www.mongodb.org/display/DOCS/Replica+Sets+-+Rollbacks>
<http://dba.stackexchange.com/questions/18020/mongodb-replica-set-secondary-stuck-in-rollback-state>
<http://comerford.cc/2012/05/28/simulating-rollback-on-mongodb/>
<http://comments.gmane.org/gmane.comp.db.mongodb.user/78137>

ROLLBACK -> RECOVERY -> SECONDARY.. ou #FAIL avec Error RS102

<http://www.mongodb.org/display/DOCS/Resyncing+a+Very+Stale+Replica+Set+Member>

### replicaset lag

<http://docs.mongodb.org/manual/administration/replica-sets/#replica-set-replication-lag>
<http://metabroadcast.com/blog/mongodb-replication-lag-and-the-road-to-recovery>

## Réparer la base de données en cas de crash

Plusieurs méthodes selon cas.

1. Si la journalisation est activé, ce qui est le cas par défaut pour les version >1.9.2 et en 64 bits.
MongoDB devrait automatiquement se servir des fichiers journaux, regardez si tout se passe bien dans les logs.
Si malheureusement ce message apparaît :

~~~
old lock file: mongod.lock.  probably means unclean shutdown,
but there are no journal files to recover.
~~~

Il faut vérifier que le système de fichier est OK !

1. Si la journalisation n'est pas activé.

Supprimez le verrou et démarrer mongodb avec l'option pour lancer une réparation.

~~~
rm /var/lib/mongodb/mongod.lock
mongod --dbpath /var/lib/mongodb/ --repairpath /tmp --repair
~~~

Vérifiez les droits du dbpath !

~~~
chown -R mongodb:mongodb /var/lib/mongodb
~~~

Démarrez mongodb, lancez un mongo shell et lancer une réparation de toutes les base de données.

~~~
db.repairDatabase();
~~~

## Mise à jour

### 1.8 vers 2.0

Voici la procédure pour passer en Mongo 2.0 (version sortie en sept/oct 2011) :

~~~
# aptitude update
# aptitude install mongodb-10gen
~~~

La version 2.0 active désormais par défaut la journalisation.
Vous pouvez donc remplacer dans votre configuration :

~~~
journal = true
~~~

par :

~~~
# Disables write-ahead journaling
# nojournal = true
~~~

Il faut également mettre-à-jour ses indexes ! On le fera via le mongo shell :

~~~
> use foo
> db.bar.getIndexes()
[
        {
                "key" : {
                        "_id" : 1
                },
                "ns" : "foo.bar",
                "name" : "_id_",
                "v" : 0
        }
]
~~~

Le * "v" : 0 * signifie que l'index n'est pas à jour.

~~~
> db.runCommand({compact : 'bar'})
{ "ok" : 1 }
> db.bar.getIndexes()
[
        {
                "v" : 1,
                "key" : {
                        "_id" : 1
                },
                "ns" : "foo.bar",
                "name" : "_id_"
        }
]
~~~

Répéter l'opération pour toutes les collections ayant des indexes !


## Monitoring

### Créer un accès 

Si l'authentification est activée, il faut alors créer un compte. On peut utiliser le role `clusterMonitor`

~~~
> use admin
> db.createUser(
    {
      user: "mongoMonitoring",
      pwd: "PASSWORD",
      roles: [ "clusterMonitor" ]
    }
)
~~~

### Munin : Plugin mongo-munin

<https://github.com/comerford/mongo-munin>

~~~
# git clone https://github.com/comerford/mongo-munin.git /tmp/mongo-munin
# mkdir -p /usr/local/share/munin/plugins
# cp /tmp/mongo-munin/mongo_* /usr/local/share/munin/plugins
# cd /etc/munin/plugins/
# ln -s /usr/local/share/munin/plugins/mongo_* .
# chmod -R u=rwX,g=rwX,o=rX /usr/local/share/munin/
~~~

/etc/munin/plugin-conf.d/mongo

~~~
[mongo_*]
env.MONGO_DB_URI mongodb://mongoMonitoring:PASSWORD@localhost:27017/admin
~~~

### NRPE : check_mongo

Les anciennes versions du `check_mongo` renvoient toujours OK, même quand le service MongoDB est failed.

Le rôle `nagios-nrpe` du dépôt Evolix public [ansible-roles](https://gitea.evolix.org/evolix/ansible-roles) permet d'installer une version plus récente fonctionnelle fournie par <https://github.com/mzupan/nagios-plugin-mongodb/>.


## FAQ

### Modifier mot de passe `mongoAdmin`

On va arrêter MongoDB et démarrer le service à la main en *background*.

```bash
systemctl stop mongod.service
sudo -u mongodb /usr/bin/mongod --config /etc/mongod.conf --noauth &
mongo
```

Dans le *shell* MongoDB :

```js
use admin;
db.createUser( { user: "mongoAdmin", pwd: "MOT_DE_PASSE", roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] } )
exit
```

Une fois sorti du *shell* MongoDB, on tue le service démarré à la main et on démarre MongoDB normalement :

```bash
kill %%
systemctl start mongod.service
```

Pour vérifier si le mot de passe fonctionne :

```bash
mongo -u mongoAdmin -p
Enter password: -p
```
