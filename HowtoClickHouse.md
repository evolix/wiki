---
categories: databases
title: Howto ClickHouse
...

- Documentation : [https://clickhouse.com/docs/](https://clickhouse.com/docs/)
- Code : [https://github.com/ClickHouse/ClickHouse](https://github.com/ClickHouse/ClickHouse)
- Licence : [Apache 2.0](https://github.com/ClickHouse/ClickHouse/blob/master/LICENSE)
- Language : C++
- Rôle Ansible : (à venir)

**ClickHouse** est un système de gestion de base de données (SGBD) de la catégorie dite «orientée colonnes» (*column-oriented*). À l'instar des [autres systèmes comparables](https://en.wikipedia.org/wiki/Comparison_of_OLAP_servers), il est conçu d'abord pour le traitement analytique en ligne (*On-Line Analytical Processing, OLAP*). «En ligne» veut dire ici «temps réel».

# Requis

Les [requis](https://clickhouse.com/docs/en/operations/requirements) du système selon la documentation officielle :

- Processeurs : architecture x86_64 avec SSE 4.2 (AArch64 et PowerPC64LE possible en compilant soi-même depuis les sources) ; 16 cœurs à 2600 MHz est préférable à 8 cœurs à 3600 MHz, Turbo Boost et hyper-threading sont recommandés
- Mémoire vive : 4 Go de RAM minimum ; calculer la quantité requise en estimant la taille des données temporaires des opérations comme GROUP BY, DISTINCT, JOIN.
- Partition ou fichier d'échange : à désactiver en production
- Espace de stockage : 2 Go minimum pour l'installation
- Réseau : 10 Gbps ou supérieur recommandé

La documentation fournit plusieurs [recommandations d'usage](https://clickhouse.com/docs/en/operations/tips) et on peut même trouver des [résultats de tests de performance](https://benchmark.clickhouse.com/hardware/) de diverses configurations matérielles.

# Installation

[Procédure](https://clickhouse.com/docs/en/getting-started/install/#install-from-deb-packages) d'installation pour Debian et Ubuntu tirée de la documentation officielle :

~~~
# apt-get install -y apt-transport-https ca-certificates dirmngr
# cd /etc/apt/trusted.gpg.d
# wget 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3a9ea1193a97b548be1457d48919f6bd2b48d754' -O clickhouse.asc
# chmod 644 clickhouse.asc

# echo "deb https://packages.clickhouse.com/deb stable main" | tee \
    /etc/apt/sources.list.d/clickhouse.list
# apt-get update

# apt-get install -y clickhouse-server clickhouse-client

# service clickhouse-server start
$ clickhouse-client # ou "clickhouse-client --password" si un mot de passe a été configuré.
~~~

Note 1 : il est possible de changer `stable` par `lts` dans la troisième ligne de la procédure ci-haut.

Note 2 : les paquets binaires précompilés (.deb, .rpm, .tgz) nécessitent le support des instructions SSE 4.2 côté CPU, ce qui peut être vérifié par la commande suivante : `grep -q sse4_2 /proc/cpuinfo && echo "SSE 4.2 supported" || echo "SSE 4.2 not supported"`

Note 3 : Une version de 2018 (18.16.1) est disponible directement dans Debian 10 et 11.

# Configuration

Avec les paquets pour Debian et Ubuntu, la configuration passe par des fichiers installés là où on les attend :

~~~
/etc/clickhouse-server/config.xml
/etc/clickhouse-server/users.xml
/etc/clickhouse-server/config.d/
/etc/clickhouse-server/user.d/
/etc/clickhouse-client/config.xml
/lib/systemd/system/clickhouse-server.service
/var/log/clickhouse-server/
~~~

Les données sont par défaut dans ce répertoire :

~~~
/var/lib/clickhouse
~~~

La documentation sur la [configuration](https://clickhouse.com/docs/en/operations/configuration-files) nous apprend qu'il est possible d'utiliser `.yaml` au lieu de `.xml` et qu'une bonne pratique est de supplanter ou d'étendre la configuration via `config.d` and `users.d`.

# Mise à jour

~~~
# apt-get update
# apt-get install clickhouse-client clickhouse-server
# service clickhouse-server restart
~~~

# Administration

## Lister les utilisateurs

~~~
:) SHOW USERS;
~~~

## Changer un mot de passe

~~~
:) ALTER USER mon_utilisateur IDENTIFIED BY 'mon_mot_de_passe';
~~~

# Utilisation

ClickHouse parle le language SQL. Un [tutoriel](https://clickhouse.com/docs/en/tutorial) permet d'apprendre les bases (créer une nouvelle table, y insérer un grand ensemble de données et tester quelques requêtes).

Plusieurs [interfaces](https://clickhouse.com/docs/en/interfaces) sont disponibles, notamment en ligne de commande (`clickhouse-client`) et via HTTP (`http://localhost:8123/play`)

Pour se connecter en ligne de commande directement sur le serveur (localhost, port 9000) avec l'utilisateur par défaut :

~~~
$ clickhouse-client --password
~~~

Après avoir saisi le mot de passe on atterrit dans l'invite de ClickHouse où on peut obtenir une liste des commandes disponibles en tappant :

~~~
:) help
~~~

Pour voir quelles bases de données sont présentes sur le serveur :

~~~
:) SHOW DATABASES
~~~

Créer une base de données `helloworld` :

~~~
:) CREATE DATABASE IF NOT EXISTS helloworld
~~~

Créer une table dans la base de données `helloworld` :

~~~
:) CREATE TABLE helloworld.ma_table
   (
       user_id UInt32,
       message String,
       timestamp DateTime,
       metric Float32
   )
   ENGINE = MergeTree()
   PRIMARY KEY (user_id, timestamp)
~~~

Insérer des données dans la table `helloworld.ma_table` de manière interactive :

~~~
:) INSERT INTO helloworld.ma_table (user_id, message, timestamp, metric) VALUES
    (101, 'Coucou, ClickHouse!',                                          now(),       -1.0    ),
    (102, 'Insérez beaucoup d\'entrées en lot',                           yesterday(), 1.41421 ),
    (102, 'Triez vos données grâce à vos requêtes favorites',             today(),     2.718   ),
    (101, 'Les granules sont les plus petits segments de données lues',   now() + 5,   3.14159 )
~~~

Sélectionner toutes les entrées insérées dans la table :

~~~
:) SELECT * FROM helloworld.ma_table
~~~

Sélectionner toutes les entrées de la table, cette fois en triant par date et en formattant la sortie différemment :

~~~
:) SELECT * FROM helloworld.ma_table ORDER BY timestamp FORMAT JSON
~~~

La documentation officielle contient [une page présentant les différents formats disponibles](https://clickhouse.com/docs/en/interfaces/formats/).

Créer la table `trips` dans la base de données `helloworld` pour mieux illustrer la force de ClickHouse :

~~~
:) CREATE TABLE helloworld.trips (
    trip_id             UInt32,
    pickup_datetime     DateTime,
    dropoff_datetime    DateTime,
    pickup_longitude    Nullable(Float64),
    pickup_latitude     Nullable(Float64),
    dropoff_longitude   Nullable(Float64),
    dropoff_latitude    Nullable(Float64),
    passenger_count     UInt8,
    trip_distance       Float32,
    fare_amount         Float32,
    extra               Float32,
    tip_amount          Float32,
    tolls_amount        Float32,
    total_amount        Float32,
    payment_type        Enum('CSH' = 1, 'CRE' = 2, 'NOC' = 3, 'DIS' = 4),
    pickup_ntaname      LowCardinality(String),
    dropoff_ntaname     LowCardinality(String)
)
ENGINE = MergeTree
PRIMARY KEY (pickup_datetime, dropoff_datetime)
~~~

Insérer des données dans la table à partir de trois gros fichiers CSV distants proposés en exemple par ClickHouse. Ils contiennent des trajets de taxi de la Ville de New York de 2009 à nos jours :

~~~
:) INSERT INTO helloworld.trips
SELECT
    trip_id,
    pickup_datetime,
    dropoff_datetime,
    pickup_longitude,
    pickup_latitude,
    dropoff_longitude,
    dropoff_latitude,
    passenger_count,
    trip_distance,
    fare_amount,
    extra,
    tip_amount,
    tolls_amount,
    total_amount,
    payment_type,
    pickup_ntaname,
    dropoff_ntaname
FROM url(
    'https://datasets-documentation.s3.eu-west-3.amazonaws.com/nyc-taxi/trips_{0..2}.gz',
    'TabSeparatedWithNames'
)
~~~

Confirmer l'insertion de plus de 3 millions d'entrées :

~~~
SELECT count()
FROM helloworld.trips
~~~

Laisser tomber la base de données `helloworld` avant de sortir de ClickHouse :

~~~
:) DROP DATABASE helloworld
:) exit
~~~

# Sauvegarde et restauration

Des exemples de [sauvegarde et de restauration](https://clickhouse.com/docs/en/operations/backup) sont proposées dans la documentation officielle.
