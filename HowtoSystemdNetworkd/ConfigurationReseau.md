---
categories: system sysadmin systemd
title: Howto systemd-networkd - Configuration réseau
...

# Configuration réseau avec systemd-networkd

* [Documentation officielle](https://www.freedesktop.org/software/systemd/man/latest/systemd.network.html)

La configuration réseau avec systemd-networkd se fait dans des fichiers `.network` dans `/etc/systemd/network/`. Ces fichiers devraient idéallement être préfixé par un numéro. Les fichiers sont appliqués dans l'ordre alphanumérique et seul le premier fichier pouvant affecter une interface est appliqué.

## Configuration d'IP statiques

```
[Match]
Name=dummy0

[Network]
Address=192.0.2.42/24
Gateway=192.0.2.254

Address=3fff:7e83:e2c7:54::42/48
Gateway=3fff:7e83:e2c7::254
```

### Configuration d'IP statique avec ajout de route

```
[Match]
Name=dummy0

[Network]
Address=192.0.2.42/24
Gateway=192.0.2.254

[Route]
Destination=198.51.100.0/24
Gateway=192.0.2.30
```

## Configuration automatique (DHCPv4 et SLAAC)

```
[Match]
Name=dummy0

[Network]
DHCP=ipv4
IPv6AcceptRA=yes
```

## Détails

### Choix des interfaces affectées

Les interfaces affectées par un fichier `.network` sont sélectionnées dans la section `[Match]`, toutes les directives présentes doivent correspondre à une interface pour que le fichier s'applique à cette interface.

Toutes les directives sont des listes séparées par des espaces blancs. La condition peut être inversée en préfixant la valeur de la directive par "!". Les directives peuvent être réinitialisées en ajoutant une ligne avec cette directive vide.

Les directives possibles sont les suivantes (les listes sont séparées par des espaces blancs) :

- `MACAddress=`: L'adresse MAC _actuelle_ de l'interface doit être dans la liste d'adresses MAC donnée à cette directive. Si cette directive est présente plusieurs fois, les listes sont fusionnées. Pour réinitialiser la liste, il faut ajouter une ligne avec cette directive vide.
- `PermanentMACAddress=`: L'adresse MAC _permanente_ de l'interface doit être dans la liste d'adresses MAC donnée à cette directive. Si cette directive est présente plusieurs fois, les listes sont fusionnées. Pour réinitialiser la liste, il faut ajouter une ligne avec cette directive vide.
- `Path=`: Correspond à la propriété `udev` `ID_PATH`. Il s'agit d'une liste dans laquelle cette propriété doit se trouver.
- `Driver=`: Le driver lié à l'interface doit être dans la liste.
- `Type=`: Le "type" de l'interface tel qu'indiqué par `networkctl list` doit être dans la liste. Les valeurs possibles incluent "ether", "loopback", "wlan" et "wwan" (liste non exhaustive).
- `Kind=`: Le "kind" de l'interface tel qu'indiqué par `ip -d link show INTERFACE` doit être dans la liste. Les valeurs possibles incluent "bond", "bridge", "gre", "tun" et "veth" (liste non exhaustive).
- `Property=`: Contient une liste de propriété `udev` et leur valeur, si plusieurs propriétés sont déclarées, l'ensemble de résultat des tests doivent être valide.
- `Name=`: Un des noms de l'interface doit être dans la liste. (Cela peut être un des noms alternatifs.)
- `WLANInterfaceType=`: Le type de réseau sans-fil lié à cette interface doit être dans la liste. Les valeurs possibles sont "ad-hoc", "station", "ap", "ap-vlan", "wds", "monitor", "mesh-point", "p2p-client", "p2p-go", "p2p-device", "ocb" et "nan".
- `SSID=`: SSID du réseau Wi-Fi sur lequel l'interface est connectée.
- `BSSID=`: Adresse physique de la station Wi-Fi à laquelle l'interface est connectée.

Certaines directives peuvent s'appliquer à la machine en général et non à l'interface :

- `Host=`: Vérifie le nom d'hôte ou le [machine-id](https://www.freedesktop.org/software/systemd/man/latest/machine-id.html) de la machine.
- `Virtualization=`: Peut être un booléen (auquel cas cela vérifier si l'environnement est virtualisé) ou une technologie détectable par systemd (cf. [`ConditionVirtualization=` dans `systemd.unit(5)`](https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionVirtualization=)).
- `KernelCommandLine=`: Vérifie si une option est fournie dans la ligne de commande du kernel (cf. [`ConditionKernelCommandLine=` dans `systemd.unit(5)`](https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionKernelCommandLine=)).
- `KernelVersion`: Vérifie la version kernel contre une expression (cf. [`ConditionKernelVersion=` dans `systemd.unit(5)`](https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionKernelVersion=)).
- `Architecture=`: Vérifie l'architecture du système

### Définition physique de l'interface (Niveau 2 + politique d'activation)

La configuration des paramètres correspondant au niveau 2 du système OSI ainsi que celles liées à l'état voulu de cette interface (up, down) se trouve dans la section `[Link]`.

Directives utiles dans cette section :

- `MTUBytes=`: Défini la MTU de cette interface, peut avoir des suffix K, M et G qui sont compris comme sur la base 1024.
- `RequiredForOnline=`: Défini si l'interface a besoin d'être configurée pour que systemd-networkd considère le système comme en ligne (et donc atteindre `network-online.target`). Peut être un booléen ou un état operationel (cf. `networkctl(1)` pour la liste des états opérationels).
- `RequiredFamilyForOnline=`: Déclare quelle(s) famille d'adresse IP est nécessaire pour que l'interface soit considérée comme en ligne. Les valeurs possible sont : "ipv4", "ipv6", "both" ou "any".
- `ActivationPolicy=`: Défini si l'interface doit être mise dans l'état administratif "up" ou "down" au boot (et si systemd-networkd doit les conservés en up ou down, bloquant `ip link set INTERFACE down` ou `ip link set INTERFACE down`). Valeurs possibles : "up", "always-up", "manual", "always-down", "down", ou "bound".
- `MACAddress=`: Nouvelle adresse MAC à mettre sur l'interface.
- `Promiscuous=`: Place l'interface en mode "promiscuous" (indique à la carte réseau d'accepter tout le réseau qu'elle voit au lieu de seulement accepter les frames qui lui sont destinées)
- `Unmanaged=`: Déclare que l'interface n'est pas à gérer par `systemd-networkd`. (Par exemple si elle est gérée par un autre gestionnaire de réseau tel que NetworkManager.)

### Définition générale du réseau

La configuration du réseau se fait dans les sections `[Network]`.

Directives utiles dans cette section :

- `DHCP=`: Active le client DHCP ou DHCPv6 (**ce n'est généralement pas la bonne directive pour activer la configuration automatique d'IPv6 en général**). Peut avoir les valeurs suivantes : `yes` (active DHCP et DHCPv6), `no` (n'active ni DHCP, ni DHCPv6), `ipv4` (active DHCP) et `ipv6` (active DHCPv6). Désactivé par défaut.
- `Address=`: Adresse statique (**avec son préfix**) à configurer. Peut être présent plusieurs fois. Équivalent à une section `[Address]` avec uniquement une directive `Address=`.
- `IPv6AcceptRA=`: Active la configuration automatique d'IPv6 (SLAAC ou DHCPv6 dépendant de ce qu'il y a dans les Router Advertisement). C'est un booléen. La configuration détaillé de la configuration automatique est faite dans les sections `[IPv6AcceptRA]` et `[DHCPv6]`.
- `Gateway=`: Adresse de la gateway, ou les valeurs spéciales `_dhcp4` (pour utiliser celle fournie par DHCP, ce qui est fait par défaut quand DHCP est utilisé et cette directive n'est pas présente pour IPv4) et `_ipv6ra` (pour utiliser celle fournie par les Router Advertisement d'IPv6 (SLAAC), aussi utilisé par défaut si SLAAC est activé). Peut être présent plusieurs fois (notamment pour configuré une gateway pour IPv4 et une pour IPv6). Équivalent à une section `[Route]` avec uniquement une directive `Gateway=`.
- `IPv6DuplicateAddressDetection=`: Active la détection de conflits d'adressage avec IPv6 (booléen). Le comportement par défaut dépend de la configuration du noyau (`sysctl net.ipv6.conf.default.accept_dad`).
- `NTP=`: Serveur NTP à utiliser lorsque ce réseau est up. Uniquement utile si `systemd-timesyncd` est utilisé.
- `IPForward=`: Permet d'activer le forwarding IP. **Le paramêtre manipulé est global et `systemd-networkd` ne le manipule que pour l'activé.**
- `IPMasquerade=`: Permet de configurer le NAT en mode masquerade pour cette interface. Peut être activé uniquement pour IPv4 en utilisant la valeur `ipv4`. Peut être activé uniquement pour IPv6 avec la valeur `ipv6`. Ou peut être activé pour les deux avec la valeur `both`. Le défaut est de ne pas configuré le NAT en mode masquerade (valeur `no`).
- `IPv6PrivacyExtensions=`: Active la configuration et l'utilisation d'adresses IPv6 temporaire changeant sur le temps (RFC 4941). Peut avoir les valeurs `yes` (pour les configurer et les préférer pour la sortie), `prefer-public` (pour les configurer, mais préférer l'adresse stable), `kernel` (pour utiliser ce qui est configuré par défaut au niveau du noyau (`sysctl net.ipv6.conf.default.use_tempaddr`)) ou `no` (pour ne pas les configurées).
- `LinkLocalAddressing=`: Active les adresses link-local pour IPv6 ou IPv4 (uniquement quand DHCP est activé et échoue pour IPv4). Peut avoir les valeurs `yes` (active les deux), `no` (désactive les deux), `ipv4` (n'active que pour IPv4) et `ipv6` (n'active que pour IPv6). Par défaut, si l'interface n'est pas contrôlée par un bridge (ou avec une interface macvlan/macvtap en mode `passthru`), seul l'adressage link-local IPv6 est activé. Il faut laisser l'adressage link-local activé pour IPv6 si IPv6 est configuré.
- `IPv6LinkLocalAddressGenerationMode=`: Modifié le mode d'adressage link-local pour IPv6, entre `eui64` (basé sur l'adresse MAC de l'interface), `stable-privacy` (RFC 7217, basé sur `IPv6StableSecretAddress=`), `random` et `none` (autre moyen pour désactiver l'adressage link-local IPv6, ne pas utiliser si IPv6 est utilisé).
- `IPv6StableSecretAddress=`: Prend une "adresse IPv6", utilisé comme secret pour générer l'adresse IPv6 link-local de l'interface. **Ce n'est pas l'adresse utilisée, juste une valeur utilisée pour la génération.**

Cette section permet aussi de configuré les interfaces virtuelles (bridge, bond, vlan, ...) à attacher sur cette interface :

- `Bridge=`: Interface bridge sur laquelle attachée cette interface.
- `Bond=`: Interface bond sur laquelle attachée cette interface.
- `VLAN=`: Nom de l'interface de type vlan à attachée à cette interface.

### Configuration détaillée d'adresses statiques

Il est possible de configurer des adresses statiques avec plus de détail que ce qui est possible dans la section `[Network]` dans la section `[Address]`, cette section peut être présente plusieurs fois avec chaque occurrence configurant une seule adresse statique.

Les directives possibles sont les suivantes :

- `Address=`: Adresse statique à configurer avec son préfixe. (eg. `Address=192.0.2.42/24` et `Address=3fff:7e83:e2c7:54::42/64`)
- `Peer=`: Adresse du pair pour une connexion point à point. Même format que pour `Address=`
- `Broadcast=`: Une adresse ou un booléen. Permet de définir l'adresse de broadcast. Si `yes` (le défaut), la valeur est dérivée de l'adresse configurée.
- `Label=`: Un label pour l'adresse IPv4. Il s'agit d'une chaine de caractères ASCII d'une longueur de 1 à 15 caractères.
- `PreferredLifetime=`: Temps après lequel l'adresse devrait expirer. Ne peut avoir que trois valeurs : `forever` et `infinity` qui déclare que l'adresse n'expire jamais (le défaut), et `0` qui indique que l'adresse doit être directement expirée.
- `Scope=`: Défini le scope de l'adresse. Peut être `global` indiquant qu'elle est valide sur tout le réseau, même après passage par une gateway, `link` indiquant qu'elle n'est valide que sur le lien et ne peut donc pas passer une gateway ou `host` indiquant qu'elle n'est valide que pour la machine elle même (comme `127.0.0.1` par exemple). Le défaut est `global`.
- `DuplicateAddressDetection=`: Active la détection de conflit, peut avoir les valeurs suivantes : `ipv4` pour activer uniquement la détection de conflit d'adressage IPv4 (RFC 5227), `ipv6` pour activer uniquement la détection d'adresses dupliquées IPv6 (définie dans RFC 4862 pour SLAAC), `both` pour activer les deux et `none` pour les désactiver. Le défaut est `ipv6` pour les adresses IPv6, `ipv4` pour les adresses IPv4 link-local et `none` pour le reste (donc IPv4). Si un conflit est détecté l'adresse n'est pas configurée, ce qui permet de limiter l'effet d'une erreur de configuration.
- `ManageTemporaryAddress=`: Permet d'activer RFC 3041 (les adresses IPv6 temporaires changeantes) pour une IP statique comme si SLAAC était actif. Nécessite que l'adresse configurée a une longueur de préfixe de 64.
- `AddPrefixRoute=`: Ajoute la route pour le préfixe de l'adresse automatiquement. Booléen, le défaut est `yes`.
- `AutoJoin=`: Permet de déclarer les adresses multicasts sur lesquelles l'interface écoute à un switch faisant du IGMP snooping.

### Configuration de routes

Il est possible de configurer des routes à ajouter à la table de routage dans une section `[Route]`. Cette section peut être présente plusieurs fois et chaque section correspond à une seule entrée dans la table de routage.

Les directives possibles sont les suivantes :

- `Gateway=`: Gateway à utiliser pour cette route. (partie `via <address>` pour `ip route`)
- `GatewayOnLink=`: Permet d'indiquer au kernel que la gateway est bien sur l'interface au lieu qu'il cherche à vérifier lui-même.
- `Destination=`: Réseau de destination pour cette route.
- `Metric=`: Métrique de la route. Le défaut est la métrique par défaut du noyau.
- `Scope=`: Scope de la route IPv4, peut être `global` (si la route permet d'atteindre des hôtes au-delà du premier saut), `site` (si la route ne permet d'atteindre que des hôtes dans le même AS local), `link` (si la route ne permet d'atteindre que des hôtes sur le même réseau local (un seul saut)), `host` (si la route ne quitte pas la machine) et `nowhere` (si la destination n'existe pas).
- `PreferredSource=`: L'adresse source préférée pour cette route.
- `Table=`: La table de routage dans laquelle la route doit être ajouté. Les tables définies par défaut sont : `default`, `main` et `local`.
- `Type=`: Le type de la route. Peut être `unicast`, `local`, `broadcast`, `anycast`, `multicast`, `blackhole`, `unreachable`, `prohibit`, `throw`, `nat` et `xresolve`. La valeur `unicast` est celle pour une route standard. La valeur `blackhole` indique que les paquets qui utiliseraient cette route seront drop. La valeur `unreachable` indique que les paquets qui utiliseraient cette route seront rejetés avec un message ICMP "Host Unreachable". La valeur `prohibit` indique que les paquets utilisant cette route seront rejetés avec un message ICMP "Communication Administratively Prohibited". La valeur `throw` indique que le lookup va échouer et que le process va retourner à la Routing Policy Database.
- `MTUBytes=`: Défini le MTU pour cette route (au cas où le MTU est connu et est différent de celle de l'interface).

### Configuration détaillée de DHCPv4

La configuration du client DHCP (pour IPv4) se fait dans une section `[DHCPv4]`. Pour que DHCP soit utilisé, il faut que la directive `DHCP=` de la section `[Network]` soit à `ipv4` ou `yes`.

- `SendHostname=`: Booléen, si à `yes` (le défaut) le nom d'hôte de la machine sera envoyé au serveur DHCP.
- `Hostname=`: Nom d'hôte envoyé au serveur DHCP. Le défaut est le nom d'hôte de la machine.
- `ClientIdentifier=`: L'identifieur de client DHCP à envoyer au serveur DHCP. Peut être `mac`, `duid` ou `duid-only`. La valeur `mac` indique d'envoyer l'adresse MAC de la machine. La valeur `duid` indique d'envoyer un identifieur respectant la RFC 4361, cet identifieur est dépendant du IAID (Identity Association Identifier) et DUID (DHCP Unique Identifier, paramètre généralement global à la machine). `duid-only` n'envoie que le DUID.
- `IAID=`: Configure l'IAID pour l'interface. Il s'agit d'un entier non signé à 32 bit (0-4294967295).
- `DUIDType=` et `DUIDRawData=`: Permet d'override la valeur globale de ces paramètres. Les valeurs de `DUIDType=` peuvent être `vendor` (utilisant l'identifiant vendeur `43793` (pour systemd) et le `machine-id`, c'est la valeur par défaut), `uuid` (qui utilise un `machine-id` spécifique à DHCP (qui se base sur le `machine-id` global)) et `link-layer-time` (ou `link-layer`) qui utilise l'adresse MAC et une valeur temporaire. `DUIDRawData=` est une chaine de caractère hexadécimale avec chaque octet séparé par ':', cette chaine devrait suivre les spécifications dans RFC 3315 et RFC 6355.
- `UseDNS=`: Indique si il faut utiliser le DNS fourni par DHCP. Les valeurs sont `yes`, `no` ou `route-only` (qui indique que le serveur DNS fourni ne devrait être utilisé que pour le domaine fourni par DHCP). Le défaut est de ne pas utiliser le serveur DNS fourni par DHCP. **N'est utile que si `systemd-resolved` est utilisé.**
- `UseNTP=`: Booléen, indique si il faut utiliser le serveur NTP fourni par DHCP. **N'est utile que si `systemd-timesyncd` est utilisé.**
- `UseMTU=`: Booléen, indique si le MTU indiqué par le serveur DHCP devrait être utilisé sur l'interface. Ignoré si `MTUBytes=` est défini.
- `UseHostname=`: Indique si le nom d'hôte fourni par le serveur DHCP devrait être utilisé.
- `UseDomains=`: Indique si le nom de domaine indiqué par le serveur DHCP devrait être utilisé pour la résolution DNS. ** Utile uniquement avec `systemd-resolved`.**
- `UseRoutes=`: Indique si les routes doivent être demandées au serveur DHCP. Le défaut est de demander les routes.
- `UseGateway=`: Indique si la gateway doit être demandée à DHCP (et être utilisée). Le défaut est d'utiliser la valeur de `UseRoutes=`.
- `DenyList=` et `AllowList=`: Permet de filtrer les IP autorisées à être configurées par DHCP. Peut prendre une liste de réseau et adresses IP.
- `SendRelease=`: Indique si le client DHCP doit envoyer un paquet DHCP "release" quand il s'arrête. Booléen, le défaut est `yes`.
- `SendDecline=`: Indique si le client DHCP doit faire une vérification de conflit d'adresse IP avant d'accepter l'adresse et envoyé un paquet DHCPDECLINE si il y a conflit. La détection de conflit suit la RFC 5227. Cette détection est recommandée par la RFC 2131 définissant DHCP. Le défaut est à `no`.

### Configuration de la configuration automatique sans état d'IPv6 (SLAAC)

La configuration de SLAAC se fait dans la section `[IPv6AcceptRA]`. Il faut que la directive `IPv6AcceptRA=` de la section `[Network]` soit à `yes` pour que SLAAC soit utilisé.

Quelques directives utiles :

- `Token=`: Spécifie un mode de génération pour les adresses IPv6. Peut avoir les valeurs suivantes :
    - `eui64`: utilise l'algorithme EUI-64 qui se base sur l'adresse MAC de la machine. Supporté uniquement pour les interfaces Ethernet et Infiniband (donc aussi celles se basant sur ces interfaces). Il s'agit de la valeur par défaut.
    - `static`: Prend une "adresse IPv6". Les bits bas sont combinés au préfix fourni par les RA pour généré l'adresse finale. Si un conflit est détecté, ce mode échouera à généré une adresse IP.
    - `prefixstable`: Utilise l'algorithme défini dans la RFC 7217. Par défaut utilise un UUID généré à partir du machine-id. L'utilisation de ce mode peut être limité à certains préfix. Le format de cette option est `prefixstable[:ADDRESS][,UUID]`.

    Plusieurs modes peuvent être indiqués.
  
    Exemples :

    ```
    Token=eui64
    Token=static:::1a:2b:3c:4d
    Token=prefixstable
    Token=prefixstable:2002:da8:1::
    ```

- `UseDNS=`
- `UseDomains=`
- `UseMTU=`
- `UseGateway=`
- `UseRoutePrefix=`
- `UseAutonomousPrefix=`
- `UseOnLinkPrefix=`
