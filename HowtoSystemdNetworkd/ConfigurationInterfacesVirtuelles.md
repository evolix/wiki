---
categories: system sysadmin systemd
title: Howto systemd-networkd - Configuration des interfaces virtuelles
...

# Configuration des interfaces virtuelles avec systemd-networkd

* [Documentation officielle](https://www.freedesktop.org/software/systemd/man/latest/systemd.netdev.html)

La configuration des interfaces virtuelles (bridge, vlan, bond, tunnels…) avec systemd-networkd se fait dans des fichiers `.netdev` dans `/etc/systemd/network/`. Ces fichiers devraient idéalement être préfixés par un numéro. Les fichiers sont appliqués dans l’ordre alphanumérique.

## Définition d'un switch virtuel (Bridge)

Pour créer un bridge (switch virtuel) :

```
# /etc/systemd/network/10-br0.netdev
[NetDev]
Name=br0
Kind=bridge
```

L'ajout d'interfaces sur le bridge se fait dans un fichier `.network` lié à ces interfaces (directive `Bridge=` de la section `[Network]`).

Par exemple si les interfaces `dummy0` et `dummy1` doivent appartenir à ce bridge, la configuration suivante peut être utilisée :

```
[Match]
Name=dummy0 dummy1

[Network]
Bridge=br0
```

## VLAN

Pour créer une interface virtuelle `dummy0.42` lié au VLAN 42 :

```
# /etc/systemd/network/10-dummy0_vlan42.netdev
[NetDev]
Name=dummy0.42
Kind=vlan

[VLAN]
Id=42
```

```
# /etc/systemd/network/10-dummy0.network
[Match]
Name=dummy0

[Network]
VLAN=dummy0.42
# [...]
```

## Bonding

Dans tous les cas, la configuration des interfaces sur lesquels le bonding se fait est à faire dans un fichier `.network` tel que :

```
# /etc/systemd/network/10-bond0_slaves.network
[Match]
# Interfaces sous jacentes
Name=dummy0 dummy1

[Network]
Bond=bond0
```

### Mode 802.3ad (LACP)

```
# /etc/systemd/network/10-bond0.netdev
[NetDev]
Name=bond0
Kind=bond

[Bond]
Mode=802.3ad
MIIMonitorSec=100ms
UpDelaySec=200ms
DownDelaySec=200ms
```

### Mode ALB

```
# /etc/systemd/network/10-bond0.netdev
[NetDev]
Name=bond0
Kind=bond

[Bond]
Mode=balance-alb
MIIMonitorSec=100ms
UpDelaySec=200ms
DownDelaySec=200ms
```

### Mode active-backup

```
# /etc/systemd/network/10-bond0.netdev
[NetDev]
Name=bond0
Kind=bond

[Bond]
Mode=active-backup
MIIMonitorSec=100ms
UpDelaySec=200ms
DownDelaySec=200ms
```

## Détails

### Conditionnement de l'application de la configuration

Il est possible de conditionner l'application d'un fichier `.netdev` par le biais d'une section `[Match]`. Toutes les conditions doivent être validées pour que la configuration s'applique.

Les directives possibles pour cette section sont :

- `Host=`: Vérifie le nom d'hôte ou le [machine-id][machine-id] de la machine.
- `Virtualization=`: Peut être un booléen (auquel cas cela vérifier si l'environnement est virtualisé) ou une technologie détectable par systemd (cf. [`ConditionVirtualization=` dans `systemd.unit(5)`][conditionVirtualization]).
- `KernelCommandLine=`: Vérifie si une option est fournie dans la ligne de commande du kernel (cf. [`ConditionKernelCommandLine=` dans `systemd.unit(5)`][conditionKernelCommandLine]).
- `KernelVersion`: Vérifie la version kernel contre une expression (cf. [`ConditionKernelVersion=` dans `systemd.unit(5)`][conditionKernelVersion]).
- `Architecture=`: Vérifie l'architecture du système.
- `Credential=`: Vérifie si un certain ["credential"][credentials] est passé à `systemd-networkd`.
- `Firmware=`: Vérifie si la machine utilise un certain firmware (cf. [`ConditionFirmware=` dans `systemd.unit(5)`][conditionFirmware]).

[machine-id]: https://www.freedesktop.org/software/systemd/man/latest/machine-id.html
[conditionVirtualization]: https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionVirtualization=
[conditionKernelCommandLine]: https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionKernelCommandLine=
[conditionKernelVersion]: https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionKernelVersion=
[credentials]: https://systemd.io/CREDENTIALS/
[conditionFirmware]: https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html#ConditionFirmware=


### Configurations générales

Les configurations communes à tous les types d'interfaces virtuelles se trouvent dans la section `[NetDev]`.

Directives utiles dans cette section :

- `Name=`: Le nom de cette interface, cette directive est obligatoire.
- `Kind=`: Le type d'interface virtuelle (bridge, vlan, bond, veth, macvlan, macvtap, ...)
- `MACAddress=`: Définie l'adresse MAC de l'interface. Inutile et ignoré pour les interfaces "tun", "tap" et "l2tp". Si cette directive n'est pas spécifiée alors les interfaces de type "vlan" ont la même adresse MAC que leur interface parente et les autres types ont une adresse MAC générée à partir de leur nom et du [`machine-id`][machine-id].
- `MTUBytes=`: MTU appliqué à l'interface (à la création). Ignoré pour les interfaces "tun" et "tap".

### Configuration d'un bridge (switch virtuel)

La configuration spécifique à une interface bridge (switch virtuel) se fait dans une section `[Bridge]`. La majorité des configurations correspondent à STP.

Directives :

- `HelloTimeSec=`: Nombre de secondes entre deux packets hello (utilisés pour communiquer la topologie du réseau local entre plusieurs switchs).
- `MaxAgeSec=`: Temps maximum en secondes avant de considérer le switch racine (STP) comme down.
- `ForwardDelaySec=`: Temps en secondes à passer dans l'état d'apprentissage de STP.
- `AgeingTimeSec=`: Temps en secondes pendant lequel une adresse MAC est conservée dans la table de forwarding du switch.
- `Priority=`: Priorité du switch pour STP.
- `DefaultPVID=`: VLAN par défaut pour les nouvelles interfaces attachées au switch. Peut être désactivé avec la valeur "none".
- `MulticastSnooping=`: Booléen, contrôle le multicast snooping.
- `VLANFiltering=`: Booléen, défini si le switch considère le tag des VLAN quand il traite les paquets.
- `VLANProtocol=`: Protocole à utiliser quand `VLANFiltering=` est activé. Peut être `802.1q` ou `802.1ad`.
- `STP`: Booléen, défini si le switch utilise STP.
- `MulticastIGMPVersion=`: Version du protocole IGMP à utiliser.

### Configuration d'une interface VLAN

La configuration spécifique aux interfaces VLAN se fait dans une section `[VLAN]`.

Directives utiles :

- `Id=`: VLAN ID à utiliser. Obligatoire.
- `Protocol=`: Protocole à utiliser. Peut être `802.1q` ou `802.1ad`. Le défaut dépend de la configuration du noyau.
- `GVRP=`: Booléen, défini si la VLAN devrait être déclarer par GVRP.
- `MVRP=`: Booléen, défini si la VLAN devrait être déclarer par le [protocole MVRP (IEEE 802.1ak)][MVRP]

[MVRP]: https://en.wikipedia.org/wiki/Multiple_Registration_Protocol

### Configuration d'un bond

La configuration spécifique aux interfaces bond se fait dans une section `[Bond]`.

Directives utiles :

- `Mode=`: Politique de bonding. Peut être `balance-rr` (le défaut), `active-backup`, `balance-xor`, `broadcast`, `802.3ad`, `balance-tlb` et `balance-alb`.
- `TransmitHashPolicy=`: La politique de hash utilisée pour la sélection de l'interface sous-jacente pour les modes `balance-xor`, `802.3ad` et `balance-tlb`. Peut avoir les valeurs suivantes :
  - `layer2`: XOR des adresses MAC et du packet ID, peut être utilisé avec `802.3ad`,
  - `layer2+3`: XOR des adresses MAC et des adresses IP, peut être utilisé avec `802.3ad`,
  - `layer3+4`: utilise, quand possible, les informations de port source et destination en plus des adresses IP, ne suis pas vraiment les règles pour `802.3ad`,
  - `encap2+3`: pareil que `layer2+3` mais utilisant une fonction du kernel qui fait que les entêtes internes à un tunnel peuvent être utilisées, ce qui peut améliorer les performances pour les utilisateurs du tunnel,
  - `encap3+4`: pareil que `layer3+4` mais utilisant une fonction du kernel qui fait que les entêtes internes à un tunnel peuvent être utilisées, ce qui peut améliorer les performances pour les utilisateurs du tunnel,
- `LACPTransmitRate=`: Défini la fréquence de transmission des données LACP quand le mode `802.3ad` est utilisé. Peut être `slow` (le défaut), auquel cas les LACPDU sont envoyées toutes les 30s, ou `fast`, auquel cas les LACPDU sont envoyées toutes les secondes.
- `MIIMonitorSec=`: Fréquence de vérification de l'état des liens sous-jacent. La valeur `0` (la valeur par défaut) désactive la surveillance.
- `UpDelaySec=`: Délai avant qu'un lien soit considéré comme UP après avoir été vu comme sain. La valeur est arrondie vers le bas à un multiple de `MIIMonitorSec=`.
- `DownDelaySec=`: Délai avant qu'un lien soit considéré comme DOWN après avoir été vu comme non sain. La valeur est arrondie vers le bas à un multiple de `MIIMonitorSec=`.
- `LearnPacketIntervalSec=`: Fréquence à laquelle le driver de bonding evoi des paquets d'apprentissage aux switch distants. N'est utile que dans les modes `balance-tlb` et `balance-alb`.
- `AdSelect=`: Logique de sélection d'agrégation 802.3ad à utiliser. Peut avoir les valeurs suivantes :
  - `stable`: L'agrégat avec la plus forte bande passante est sélectionnée et une nouvelle sélection n'est faîte qu'après que tous les liens de l'aggrégat sélectionné sont vues comme down (ou si l'agrégat n'a plus de lien),
  - `bandwidth`: L'agrégat avec la plus forte bande passante est sélectionnée et une nouvelle sélection est faîte si :
    - Un lien est ajouté ou enlevé du bond
    - L'état d'un lien change
    - L'état d'association LACP d'un lien change
    - Le bond passe d'éteint à allumé
  - `count`: L'agrégat avec le plus de liens est choisi. Une nouvelle sélection est faîte dans les mêmes cas que pour `bandwidth`.
- `AdActorSystemPriority=`
- `AdUserPortKey=`
- `AdActorSystem=`
- `PrimaryReselectPolicy=`: Spécifie la politique de changement du lien primaire. Peut être `always`, `better` ou `failure`.
- `ResendIGMP=`: Nombre d'annonces de participation IGMP à envoyer dans le cas d'un failover. Une première est envoyée dès le failover et les autres sont espacés de 200ms. La valeur par défaut est `1`.
- `GratuitousARP=`: Nombre d'annonces ARP et IPv6 NA à faire après un failover. Une première annonce est faite dès le failover et les autres sont séparés de `MIIMonitorSec` ou `ARPIntervalSec`.
- `ARPIntervalSec=`: Fréquence du monitoring ARP. La valeur `0` (le défaut) désactive ce monitoring.
- `AllSlavesActive=`: Défini si les frames reçues sur un lien autre que ceux utilisés doivent être acceptées ou non. La valeur par défaut est `false`.
- `MinLinks=`: Nombre minimum de liens qui doivent être actifs avant de considérer le bond comme connecté.
