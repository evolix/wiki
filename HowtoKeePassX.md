---
categories: desktop sécurité
title: Howto KeepassX
...

* Code source : <https://github.com/keepassx/keepassx>

[KeePassX](https://www.keepassx.org/) est un logiciel de gestion des mots de passe pour Linux/Windows/Mac. Il permet de générer, stocker et organiser ses mots de passe tout en les protégeant avec un chiffrement AES (clé de 256 bits).

KeePassX est un fork du logiciel [KeePass](http://keepass.info/) avec lequel son format de stockage (.kdb/.kdbx) est compatible. Il existe aussi [KeePassXC](https://keepassxc.org/) un fork de… KeePassX.
 
* KeePassX 0.4 utilise le format **.kdb** (compatible avec KeePass 1)
* KeePassX 2 utilise le format **.kdbx** (compatible avec KeePass 2) et peut lire le format **.kdb** (lecture seule et conversion au format .kdbx)

**Chez Evolix nous utilisons désormais KeePassXC et le format .kdbx**

## Installation

~~~
# apt install keepassx kpcli libcapture-tiny-perl libclipboard-perl

$ keepassx -v
keepassx 2.0.3
~~~

> *Note* : Pour Debian 8, on peut faire :
>
> ~~~
> # echo "deb http://mirror.evolix.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list
> # apt install keepassx/jessie-backports kpcli libcapture-tiny-perl
> ~~~

Les paquets `libcapture-tiny-perl` et `libclipboard-perl` (disponibles uniquement sous *Stretch*) permettent d'avoir accès aux commandes xu/xp/xw/xx de _kpcli_ pour placer le login ou mot de passe directement dans le presse-papier.


## Utilisation

Vous pouvez créer un fichier .kdbx protégé par mot de passer et/ou clé pour stocker vos mots de passe :

~~~
Base de données> Nouvelle base de données> Mot de passe: PASSWORD > OK > Enregistrer la base de données
~~~

Vous pourrez ultérieurement l'utiliser via la commande :

~~~
$ keepassx db.kdbx
~~~

### Raccourcis

* `Ctrl+u` : ouvrir l'URL dans votre navigateur
* `Ctrl+v` : tente de remplir directement les champs dans votre navigateur (à utiliser avec précaution)
* `Ctrl+B` : copier le login dans le presse-papier
* `Ctrl+C` : copier le mot de passe dans le presse-papier
* `Ctrl+Alt+u` : copier l'URL dans le presse-papier

### Verrouillage

Nous conseillons de configurer un verrouillage automatique de votre KeePassX via :

~~~
Outils> Paramètres> Sécurité
~~~

### Convertir un fichier .kdb en .kdbx

~~~
Base de données> Importer une base de données KeePass
~~~

il faut ensuite sauvegarder la nouvelle base de données importée du fichier .kdb


## kpcli

[kpcli](http://kpcli.sourceforge.net/) est un utilitaire permettant la manipulation de fichier .kdbx (ou .kbd) depuis la console. Il couvre les fonctions principales de KeePassX (copie du login/mot de passe dans le presse-papier, arborescence conservée, création de nouvelles entrées etc.)

> *Note* : la gestion du format Keepass n'est pas strictement identique entre kpcli et KeePassX, par exemple kpcli gère les tags mais pas KeePassX. Si vous en ajoutez avec kpcli, ils ne seront donc pas visibles avec KeePassX !

### Créer une nouvelle base

Il suffit de lancer `kpcli` sans argument, puis de sauvegarder : 

~~~
$ kpcli
KeePass CLI (kpcli) v3.1 is ready for operation.

kpcli:/> saveas foo.kdbx
Please provide the master password: *************************
Retype to verify: *************************
~~~

### Utilisation courante 

kpcli a quelques options au démarrage. Les plus utiles étant : 

* `--kdb FICHIER` - Spécifie le fichier .kdbx à ouvrir. 
* `--readonly` - Ouverture de la base de données en lecture seule

~~~
$ kpcli --readonly --kdb foo.kdbx

Please provide the master password: *************************

Type 'help' for a description of available commands.
Type 'help <command>' for details on individual commands.

kpcli:/> help
  attach -- Manage attachments: attach <path to entry|entry number>
      cd -- Change directory (path to a group)
      cl -- Change directory and list entries (cd+ls)
   clone -- Clone an entry: clone <path to entry> <path to new entry>
   close -- Close the currently opened database
     cls -- Clear screen ("clear" command also works)
    copy -- Copy an entry: copy <path to entry> <path to new entry>
    edit -- Edit an entry: edit <path to entry|entry number>
  export -- Export entries to a new KeePass DB (export <file.kdbx> [<file.key>])
    find -- Finds entries by Title
    help -- Print helpful information
 history -- Prints the command history
   icons -- Change group or entry icons in the database
  import -- Import another KeePass DB (import <file.kdbx> <path> [<file.key>])
      ls -- Lists items in the pwd or a specified path ("dir" also works)
   mkdir -- Create a new group (mkdir <group_name>)
      mv -- Move an item: mv <path to group|entry> <path to group>
     new -- Create a new entry: new <optional path&|title>
    open -- Open a KeePass database file (open <file.kdbx> [<file.key>])
    pwck -- Check password quality: pwck <entry|group>
     pwd -- Print the current working directory
    quit -- Quit this program (EOF and exit also work)
  rename -- Rename a group: rename <path to group>
      rm -- Remove an entry: rm <path to entry|entry number>
   rmdir -- Delete a group (rmdir <group_name>)
    save -- Save the database to disk
  saveas -- Save to a specific filename (saveas <file.kdbx> [<file.key>])
    show -- Show an entry: show [-f] [-a] <entry path|entry number>
   stats -- Prints statistics about the open KeePass file
     ver -- Print the version of this program
    vers -- Same as "ver -v"
      xp -- Copy password to clipboard: xp <entry path|number>
      xu -- Copy username to clipboard: xu <entry path|number>
      xw -- Copy URL (www) to clipboard: xw <entry path|number>
      xx -- Clear the clipboard: xx

Type "help <command>" for more detailed help on a command.

kpcli:/> ls

=== Groups ===
eMail/
Internet/

kpcli:/> cd Internet/
kpcli:/Internet> ls

=== Entries ===
0. example.com                                                 secure.example.com
1. example.net                                                 intra.example.net
2. example.org                                                 magic.example.org

kpcli:/Internet> show 2

 Path: /Internet
Title: example.org
Uname: baz
 Pass: 
  URL: magic.example.net
Notes: Tarpin secure thing
~~~

Note : le champ « Pass » est invisible mais l'on peut le copier/coller avec la souris.

### Ajouter une entrée

~~~
$ kpcli:/Internet> new

Adding new entry to "/Internet"
Title: example
Username: root
Password:                ("g" or "w" to auto-generate, "i" for interactive)
Retype to verify: 
URL: 
Tags: 
Strings: (a)dd/(e)dit/(d)elete/(c)ancel/(F)inish? 
Notes/Comments (""): 
(end multi-line input with a single "." on a line)
| 
Database was modified. Do you want to save it now? [y/N]: 
Saved to foo.kdbx
~~~

### Modifier une entrée

~~~
$ kpcli:/Internet> edit 0
~~~

## FAQ

### Multi-utilisateurs

Malheureusement l'utilisation d'un même fichier à plusieurs utilisateurs est mal géré.
L'ouverture d'une base de données avec KeePassX provoque la création d'un fichier .lock empêchant l'ouverture en écriture par un autre utilisateur. Pour détecter cet utilisateur, voir le propriétaire du fichier .lock…
