**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Philesight est déprécié au profit de Duc, du même auteur. [<http://trac.evolix.net/infogerance/wiki/HowtoDUC>]

# HowtoPhilesight

Philesight permet de faire un graphique à la « filelight » mais côté serveur. Il s'agit d'un script cgi en ruby. On peut le configurer de sorte à ce que chaque nuit l'index soit mis à jour ainsi que les graphiques, et le consulter à tout moment.[[BR]]

[[Image(philesight.png)]][[BR]]

Une démo est disponible [ici](http://goo.gl/NLfpL)

# Installation

Télécharger l'archive sur le site de [Zevv](https://zevv.nl/play/code/philesight/).

Installer les dépendances :

~~~
# aptitude install ruby libcairo-ruby libdb-ruby1.8
~~~

Configurer votre serveur web de façon à gérer les cgi. On supposera que les cgi sont à placer dans /usr/lib/cgi-bin, copier les fichiers philesight* dans le répertoire cgi-bin.
Exécutez la commande `/usr/lib/cgi-bin/philesight --db /tmp/ps.db --index / --skip /proc/`.

Si vous souhaitez modifier le chemin du fichier d'index, modifier le chemin dans philesight.cgi.

À partir de maintenant vous pouvez accéder au graphique via l'url de votre cgi, du genre <http://monserveur/cgi-bin/philesight.cgi>
L'idéal et de lancer la commande pour générer l'index chaque nuit avec un cron par exemple.