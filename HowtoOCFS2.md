# Howto OCFS2

Il est notamment nécessaire de disposer d'un FS clusterisé quand FS est monté à deux endroits différents, ce qui n'est pas possible avec un FS standard tel que ext4.

### Installation

~~~
# apt install ocfs2-tools
~~~

Configurer OCFS dans /etc/ocfs2/cluster.conf

~~~
node:
    ip_port = 7777
    ip_address = 192.0.2.1
    number = 0
    name = kvm1
    cluster = ocfs2

node:
    ip_port = 7777
    ip_address = 192.0.2.2
    number = 1
    name = kvm2
    cluster = ocfs2

cluster:
    node_count = 2
    name = ocfs2
~~~

Formater le device drbd0 sur une seule machine.

~~~
# mkfs.ocfs2 /dev/drbd/by-res/r0
~~~

Monter la partition sur les deux machines.

~~~
# /etc/init.d/o2cb online
# mount /dev/drbd/by-res/r0 /virtualmachines
~~~





### Erreur "Unable to access cluster service while trying initialize cluster"

Si vous avez une erreur "Starting Oracle Cluster File System (OCFS2) mount.ocfs2: Unable to access cluster service while trying initialize cluster" :

~~~
# /etc/init.d/o2cb online
# /etc/init.d/ocfs2 restart
~~~
