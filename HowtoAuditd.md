---
categories: system security
title: Howto Auditd
...

* Documentation : <https://github.com/linux-audit/audit-documentation>
* Statut de cette page : test / bullseye

Le Linux Auditing System est un framework d’audit des évènements systèmes intégré au noyau Linux.
Son composant en userspace est fournit par le paquet Debian **auditd** qui fournit un service du même nom.
Il permet, par exemple, de surveiller et loguer l’exécution de processus, la création ou suppression de fichiers…

## Installation

~~~
# apt install auditd

# auditctl -v
auditctl version 3.0

# systemctl status auditd
● auditd.service - Security Auditing Service
     Loaded: loaded (/lib/systemd/system/auditd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2023-08-17 12:45:14 CEST; 5min ago
       Docs: man:auditd(8)
             https://github.com/linux-audit/audit-documentation
    Process: 458337 ExecStart=/sbin/auditd (code=exited, status=0/SUCCESS)
    Process: 458341 ExecStartPost=/sbin/augenrules --load (code=exited, status=0/SUCCESS)
   Main PID: 458338 (auditd)
      Tasks: 2 (limit: 19002)
     Memory: 1.2M
        CPU: 267ms
     CGroup: /system.slice/auditd.service
             └─458338 /sbin/auditd

~~~

## Configuration

Auditd se configure avec des règles, qui vont loguer les évènements demandés.

Les règles doivent se placer dans des fichiers `/etc/audit/rules.d/*.rules`.

> *Attention* : Selon des types d’évènement logués, les logs peuvent devenir très volumineux.

### Loguer les commandes root

Ajouter dans `/etc/audit/rules.d/audit.rules` :

~~~
-a exit,always -F arch=b64 -S execve -k root-commands
-a exit,always -F arch=b32 -S execve -k root-commands
~~~

Puis :

~~~
# /etc/init.d/auditd restart
# auditctl -l
-a always,exit -F arch=b64 -S execve -F key=root-commands
-a always,exit -F arch=b32 -S execve -F key=root-commands
~~~

On a ainsi une trace de (toutes?) les actions effectuées dans `/var/log/audit/audit.log` (exemple pour une commande `uptime` lancée en **root**) :

~~~
type=EXECVE msg=audit(1692269576.808:624): argc=1 a0="uptime"
type=CWD msg=audit(1692269576.808:624): cwd="/root"
type=PATH msg=audit(1692269576.808:624): item=0 name="/usr/bin/uptime" inode=14569387 dev=08:04 mode=0100755 ouid=0 ogid=0 rdev=00:00 nametype=NORMAL cap_fp=0 cap_fi=0 cap_fe=0 cap_fver=0 cap_frootid=0^]OUID="root" OGID="root"
type=PATH msg=audit(1692269576.808:624): item=1 name="/usr/bin/uptime" inode=14569387 dev=08:04 mode=0100755 ouid=0 ogid=0 rdev=00:00 nametype=NORMAL cap_fp=0 cap_fi=0 cap_fe=0 cap_fver=0 cap_frootid=0^]OUID="root" OGID="root"
type=PATH msg=audit(1692269576.808:624): item=2 name="/lib64/ld-linux-x86-64.so.2" inode=13107268 dev=08:04 mode=0100755 ouid=0 ogid=0 rdev=00:00 nametype=NORMAL cap_fp=0 cap_fi=0 cap_fe=0 cap_fver=0 cap_frootid=0^]OUID="root" OGID="root"
type=PROCTITLE msg=audit(1692269576.808:624): proctitle="uptime"
type=SYSCALL msg=audit(1692269580.712:625): arch=c000003e syscall=59 success=yes exit=0 a0=5558a0880e00 a1=5558a088bb00 a2=5558a0854d40 a3=fffffffffffffd8e items=3 ppid=458386 pid=460417 auid=0 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=pts7 ses=1536 comm="vim" exe="/usr/bin/vim.basic" subj=unconfined key="root-commands"^]ARCH=x86_64 SYSCALL=execve AUID="root" UID="root" GID="root" EUID="root" SUID="root" FSUID="root" EGID="root" SGID="root" FSGID="root"
~~~

### Surveiller les renommages et suppressions de fichiers

Créer un fichier `/etc/audit/rules.d/rm.rules` contenant :

~~~
## First rule - delete all
-D

## Increase the buffers to survive stress events.
## Make this bigger for busy systems
-b 8192

## This determine how long to wait in burst of events
--backlog_wait_time 0

## Set failure mode to syslog
-f 1

# Monitor rename and deletion of files in directory <PATH>
-a always,exit -F dir=<PATH> -S unlink -S unlinkat -S rename -S renameat -S rmdir -k <MY_CUSTOM_VAR>
~~~

Puis recharger le service.


## Logs

Les logs se trouvent dans `/var/log/audit/audit.log`.

On peut utiliser `ausearch` qui fournit des options de recherche avancées. Par exemple, si on a définit un mot clé dans la règle :

~~~
# ausearch -k <MY_CUSTOM_VAR>
~~~

La rotation des logs est gérée par auditd lui-même dans `/etc/audit/auditd.conf`, il n'est donc pas nécessaire d'ajouter de configuration pour logrotate.

## Envoi avec rsyslog

Renvoi vers un serveur rsyslog distant via une config type (non testée) :

~~~
$InputFileName /var/log/audit/audit.log
$InputFileTag tag_audit_log:
$InputFileStateFile audit_log
$InputFileSeverity info
$InputFileFacility local0
$InputRunFileMonitor

local0.* @192.0.2.42:514
~~~

