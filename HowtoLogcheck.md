---
categories: sécurité sysadmin
title: Howto Logcheck
...


[Logcheck](http://logcheck.org/) est un script Bash qui permet d'envoyer par email les termes inconnus dans des logs. Il est lancé toutes les heures et surveille par défaut `/var/log/syslog` et `/var/log/auth.log` (ainsi que journald depuis Debian 12). C'est un complément idéal de [Log2mail](HowtoLog2mail) car il va détecter de nouveaux termes dans les logs, que l'on considèrera comme normaux (on les ajoutera alors à la liste des termes connus) ou anormaux (que l'on ajoutera à _Log2mail_ pour avoir une alerte immédiate).


## Installation

~~~
# apt install logcheck logcheck-database
~~~


## Configuration

Fichiers de configuration :

~~~
/etc/logcheck/
├── logcheck.conf
├── logcheck.logfiles.d
│   ├── journal.logfiles
│   ├── [...]
│   └── syslog.logfiles
├── header.txt
├── cracking.d/
│   ├── kernel
│   ├── [...]
│   └── uucico
├── cracking.ignore.d/
├── ignore.d.paranoid/
│   ├── bind
│   ├── [...]
│   └── xinetd
├── ignore.d.workstation
│   ├── automount
│   ├── [...]
│   └── xlockmore
├── logcheck.logfiles.d/
├── violations.d/
│   ├── kernel
│   ├── [...]
│   └── sudo
└── violations.ignore.d
    ├── logcheck-su
    └── logcheck-sudo
~~~

Le fichier de configuration principal est `/etc/logcheck/logcheck.conf` où l'on précisera notamment l'option `REPORTLEVEL` pour choisir quel jeu d'exceptions l'on veut utiliser et `SENDMAILTO` pour la destination de l'email :

~~~
REPORTLEVEL="server"
SENDMAILTO="monitoring@example.com"
MAILASATTACH=0
FQDN=1
TMP="/tmp"
~~~

La liste des fichiers de journaux à surveiller se trouve dans le fichier `/etc/logcheck/logcheck.logfiles` :

~~~
/var/log/syslog
/var/log/auth.log
/var/log/user.log
~~~


## Utilisation

Par défaut Logcheck s'exécute toutes les heures et à chaque reboot comme indiqué dans le fichier `/etc/cron.d/logcheck`. On est parfois surpris par la quantité de lignes reçues. Pourtant, le but est bien de ne **rien** recevoir sauf exception ! Pour cela, il est important de passer par une « phase de test » où l'on ajoutera des règles d'exception pour prendre en compte les particularités de son système.

Si vous utilisez le `REPORTLEVEL="server"` vous ajouterez des expressions régulières dans un fichier situé dans le répertoire `/etc/logcheck/ignore.d.server/`. Voici par exemple, quelques règles que l'on a pu ajouter pour diverses raisons :

~~~
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ kernel: \[IPTABLES DROP\] : IN=eth0 OUT= MAC=.*
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ log2mail\[[0-9]+\]: Logfile [.[:alnum:]/]+ rotated. Listening to new file.$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ nrpe\[[0-9]+\]: Could not read request from client, bailing out...$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ nrpe\[[0-9]+\]: INFO: SSL Socket Shutdown.$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ ntpd\[[0-9]+\]: clock is now [[:alnum:]]+$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ suhosin\[[0-9]+\]: ALERT - Include filename \([^)]+\) is an URL that is not allowed \(attacker.+$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ suhosin\[[0-9]+\]: ALERT - tried to register forbidden variable '_REQUEST' through POST variables \(attacker.+$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ suhosin\[[0-9]+\]: ALERT - tried to register forbidden variable '_GET' through POST variables \(attacker.+$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ suhosin\[[0-9]+\]: ALERT - tried to register forbidden variable '_SERVER\[\w+\]' through POST variables \(attacker.+$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ suhosin\[[0-9]+\]: ALERT - ASCII-NUL chars not allowed within request variables.+$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ proftpd\[[0-9]+\]: [._[:alnum:]-]+ - ProFTPD killed \(signal 15\)$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ proftpd\[[0-9]+\]: [._[:alnum:]-]+ - ProFTPD 1.3.1 standalone mode SHUTDOWN$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ proftpd\[[0-9]+\]: [._[:alnum:]-]+ - ProFTPD 1.3.1 \(stable\) \(built Tue Oct 27 10:09:08 UTC 2009\) standalone mode STARTUP$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ rsyncd\[[0-9]+\]: connect from [._[:alnum:]-]+ \([.[0-9]]+\)$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ rsyncd\[[0-9]+\]: rsync allowed access on module [a-z]+ from [._[:alnum:]-]+ \([.[0-9]]+\)$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ rsyslogd: -- MARK --$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ rsyslogd: \[origin software="rsyslogd" swVersion="3.18.6" x-pid="[0-9]+" x-info="<http://www.rsyslog.com"\>] restart$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ sshd\[[0-9]+\]: Received disconnect from [.[0-9]]+: 11:.*$
~~~

Le programme [logcheck-test](https://manpages.debian.org/testing/logcheck/logcheck-test.1) permet de facilement tester une règle ou un fichier de règles.

~~~
$ logcheck-test -r /etc/logcheck/ignore.d.server/foo -l /var/log/syslog
~~~

Pour tester une expression régulière que l'on écrit, on utilisera `grep -E` ainsi :

~~~
$ sed -e 's/[[:space:]]*$//' /var/log/syslog | grep -E 'MON-EXPRESSION-REGULIERE'
~~~

On peut aussi faire tout simplement :

~~~
$ echo "Oct  8 23:59:33 foo sshd[24123]: Received disconnect from 192.0.2.51: 11: disconnected by user" | grep -E 'MA-SUPER-EXPRESSION-REGULIERE'
~~~

On peut tester l'exécution de logcheck avec toutes les règles et afficher en sortie standard dans le terminal :

~~~
# sudo -u logcheck /usr/sbin/logcheck -o
~~~

## FAQ

### Comment apprendre les expressions régulières pour écrire de nouvelles règles ?

Le plus simple est de repartir des règles existantes, et de s'aider de la nombreuse documentation existante comme le site <http://rubular.com/>.


### Je ne reçois pas certains emails envoyés par Logcheck ?

Quand beaucoup de logs anormaux sont générés, il est possible que l'email envoyé dépasse la taille autorisé par votre MTA (sous Postfix, c'est 10 Mo par défaut).

Dans ce cas, si on ne peut pas réduire la taille des logs envoyés (en corrigeant l'applicatif responsable), on peut réduire la taille en compressant les logs en pièce jointe :

~~~
# Send the results as attachment or not.
# 0=not as attachment; 1=as attachment; 2=as gzip attachment
# Default is 0
MAILASATTACH=2
~~~
