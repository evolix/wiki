---
title: Howto nut
---

<http://networkupstools.org/documentation.html>

## Installation

    # apt install nut

## Configuration upsd

upsd est un démon qui gère la connexion avec un onduleur branché en port série ou USB à la machine.
Il permet à des clients (*upsc*, *upsmon*, *check_ups*) de consulter l'état de l'onduleur.

### Onduleur branché en USB

    $ lsusb

/etc/nut/ups.conf :

~~~{.ini}
[onduleur]
    driver = usbhid-ups
    port = auto
~~~

### Onduleur branché sur port série

/etc/nut/ups.conf :

~~~{.ini}
[onduleur]
    driver = apcsmart
    port = /dev/ttyS0
~~~

### Ajouter un utilisateur

Obligatoire si vous voulez gérer une extinction automatique via [upsmon](#configuration-upsmon).

/etc/nut/upsd.users :

~~~{.ini}
[upsmon]
   password = XXXX
   upsmon master
~~~

### Vérification

Vous devez avoir deux process : upsd (qui écoute par défaut sur TCP/3493) et le process qui correspond à votre driver.

    # /etc/init.d/nut-server restart
    # ps auwx | egrep '(ups|nut)' | grep -v grep
    2921  0.0  0.0   9032   684 ?        Ss   01:30   0:00 /lib/nut/usbhid-ups -a onduleur
    2924  0.0  0.0  10960   712 ?        Ss   01:30   0:00 /sbin/upsd
    # netstat -taupen | grep upsd | grep LISTEN
    tcp        0      0 127.0.0.1:3493          0.0.0.0:*               LISTEN      0          28828609    2924/upsd       
    tcp6       0      0 ::1:3493                :::*                    LISTEN      0          28828611    2924/upsd       

Pour accéder aux infos de l'onduleur en ligne de commande, on peut utiliser *upsc* :

    $ upsc -l
    onduleur
    $ upsc onduleur@localhost
    battery.charge: 100
    battery.charge.low: 10
    battery.charge.warning: 50
    [...]


## Configuration upsmon

*upsmon* est un démon qui va surveiller en permanence l'état de l'onduleur en se connectant à upsd.
Il va permettre l'arrêt automatique de la machine si l'état de l'onduleur est en *battery.charge.low*

*upsmon* peut tourner sur la même machine que upsd ou sur une machine distante (on peut alors avoir plusieurs machines connectées à un onduleur qui vont s'éteindre automatiquement).

/etc/nut/upsmon.conf :

~~~
MONITOR onduleur@localhost 1 upsmon XXXX master
~~~

On peut le lancer, on doit avoir deux process : l'un en root et l'autre en *nut* (connecté en réseau à *upsd*) :

    # /etc/init.d/ups-monitor start
    [ ok ] Starting NUT - power device monitor and shutdown controller: nut-client.
    $ ps auwx | egrep upsmon | grep -v grep
    root      3003  0.0  0.0   8568   432 ?        Ss   01:31   0:00 /sbin/upsmon
    nut       3005  0.0  0.0   8992   596 ?        S    01:31   0:00 /sbin/upsmon
    # netstat -taupen | grep upsmon
    tcp        0      0 127.0.0.1:54108         127.0.0.1:3493          ESTABLISHED 115        28832727    3005/upsmon     


## Check Nagios


Un check Nagios est inclus dans les plugins de base :

    $ /usr/lib/nagios/plugins/check_ups -u onduleur
    UPS OK - Status=Online Utility=241.0V Batt=100.0% Load=21.0% |voltage=241000mV;;;0 battery=100%;;;0;100 load=21%;;;0;100
