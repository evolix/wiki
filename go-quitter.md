---
title: Howto Go-quitter
...

# Go-quitter

Go-quitter est un projet de client GNU Social (statusnet) en CLI écrit en langage Go : [dépôt github](https://github.com/aerth/go-quitter)

## Installation

~~~
# apt install golang git
~~~ 

~~~{.bash}
$ GOPATH=/tmp/go GOBIN=$HOME/bin/ CGO_ENABLED=0 go get -v -u -x -ldflags='-s -w' github.com/aerth/go-quitter/cmd/go-quitter
~~~

## Utilisation

~~~
$ go-quitter config
$ #Voir dernière notifs
$ watch -n 60 -d "go-quitter home 2>/dev/null | grep '@<jdoe>' | grep -v '^@<jdoe>'"
~~~
