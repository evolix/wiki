**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto LSyncD

<http://code.google.com/p/lsyncd/>

## Installation

A partir de Debian Squeeze.

~~~
# aptitude install lsyncd
~~~

## Utilisation en ligne de commande

~~~
lsyncd --no-daemon /repertoire/a/syncroniser user@host:/repertoire/dans/lequel/syncroniser
~~~

## Lancement en tant que daemon

Il suffit d'appeler lsyncd avec le paramètre --conf et le chemin du fichier de configuration que l'on souhaite utiliser :

~~~
lsyncd --conf /home/user/blah/fichier
~~~

Le fichier de configuration :

~~~
<lsyncd version="1">
    <settings>
            <logfile      filename="/tmp/lsyncd"/>
            <binary       filename="/usr/bin/rsync"/>
    </settings>
    <directory>
            <source path="/home/source"/>
            <target path="host:target/"/>
    </directory>
</lsyncd>
~~~