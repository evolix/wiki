**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Choses à respecter pour éviter les problèmes


## Ajout de mémoire RAM

Les barrettes de RAM se positionne suivant un ordre précis et non linéaire. Il est indiqué au dos du couvercle du boitiers.

Si l'ordre n'est pas respecté, certaines barrettes peuvent ne pas être détectées.

## Accès au BIOS des périphériques

Pour accéder au firmware des périphériques branchés sur le serveur il faut d'abord passer par le BIOS.
Puis aller dans System Settings ? Adapters and UEFI drivers ? Please refresh this page on the first visit qui affichera la liste des périphériques et permettra de les configurer.

## Mise à jour du BIOS

On ira sur le site d'[IBM](http://www.ibm.com/support/) en rentrant le modèle de la machine (affiché sur une étiquette ou dans le BIOS).

L'outil pour flasher le BIOS est s'exécute à chaud sur GNU/Linux, c'est un script shell.

Exemple d'utilisation :

~~~
./ibm_fw_bios_gfe149b-1.17_linux_i386.sh -s
~~~

Il se peut que ça « bug », comme c'est une archive auto-extractible, on l'extrait et on lance lfash à la main.


~~~
./ibm_fw_bios_gfe149b-1.17_linux_i386.sh -x /tmp
cd /tmp/linwrap
./lflash64
~~~



## Configuraton du BIOS

Options recommandées :

[[Image(0.jpg)]]

[[Image(1.jpg)]]

[[Image(2.jpg)]]

[[Image(3.jpg)]]

[[Image(4.jpg)]]

[[Image(5.jpg)]]

[[Image(6.jpg)]]

[[Image(7.jpg)]]

[[Image(8.jpg)]]

[[Image(9.jpg)]]

[[Image(10.jpg)]]

[[Image(11.jpg)]]

