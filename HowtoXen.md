**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Xen

<http://www.joachim-breitner.de/blog/archives/265-Xen-Server-Sharing-The-Setup.html>

## dom0

~~~
# aptitude install xen-tools xen-hypervisor-3.0.3-1-amd64 
xen-linux-system-2.6.18-6-xen-amd64 bridge-utils
~~~

## domU

### Préparer les "images" des partitions

~~~
# dd if=/dev/zero of=debian.img bs=1 count=0 seek=8G
# mkfs.ext3 debian.img
# dd if=/dev/zero of=debian_swap.img bs=1M count=50
# mkswap debian_swap.img
~~~

Note : on pourra aussi gérer les partitions directement en LVM

### Installation du système de base

On monte la (ou les) partitions en local. Dans le cas d'une image :

~~~
# mkdir /mnt/xen
# mount -o loop debian.img /mnt/xen/
~~~

Puis on deboostrap :

~~~
# debootstrap squeeze /mnt/xen <http://mirror.evolix.org/debian>
~~~

Et enfin, on réalise les opérations de base :

~~~
# mount -t proc /proc/ /mnt/xen/proc/
# sudo chroot /mnt/xen /bin/bash
# passwd
# dpkg-reconfigure tzdata
# aptitude install linux-image-2.6.32-5-xen-amd64_2.6.32-30_amd64.deb
# hostname
~~~

On ajuste le fichier _/etc/fstab_ :

~~~
/dev/xvda1     /     ext3     errors=remount-ro          0     1
proc          /proc proc     rw,nodev,nosuid,noexec     0     0
/dev/xvda2     none  swap     sw                         0     0
~~~

Le fichier _/etc/network/interfaces_ :

~~~
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address IP
    netmask MASQUE
    gateway ROUTEUR
    # post-up  ethtool -K eth0 tx off
~~~

Le fichier _/etc/hosts_

Le fichier _/etc/hostname_


Et enfin, il faudra aussi modifier le fichier _/etc/inittab_ ainsi :

~~~
#1:2345:respawn:/sbin/getty 38400 tty1
1:2345:respawn:/sbin/getty 38400 hvc0
~~~

Attention, à la fin, ne pas oublier de démonter toutes les partitions !!!

~~~
# exit
# sudo umount /mnt/xen/proc
# sudo umount /mnt/xen
~~~



### Configuration de la machine virtuelle

On configure la machine virtuelle/etc/xen/debian :

~~~
kernel = "/boot/vmlinuz-2.6.32-5-xen-amd64"
ramdisk = '/boot/initrd.img-2.6.32-5-xen-amd64'
memory = 128
name = "debian"
root = "/dev/xvda1 ro"
disk = [ 'file:/srv/xen/debian.img,xvda1,w' ]
vif  = [ '' ]
~~~


Creation/démarrage du domU (option -c => attacher le système) 

~~~
# xm create debian
~~~

On pourra utiliser l'option _-c_ pour voir directement le système démarrer

S'attacher à un terminal :

~~~
# xm console debian
~~~

Sortir :

~~~
Ctrl+AltGr+]
~~~

Pour que la machine virtuelle démarre automatiquement lors d'un démarrage du dom0 :

~~~
# mkdir -p /etc/xen/auto
# cd /etc/xen/auto
# ln -s ../debian debian
~~~
