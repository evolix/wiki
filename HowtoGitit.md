---
categories: git wiki
title: Howto GitIt
...

* Site/dépôt officiel : <https://github.com/jgm/gitit/tree/master/>

**Gitit** est un wiki basé sur [Git](HowtoGit) et écrit en [Haskell](https://fr.wikipedia.org/wiki/Haskell). Il permet l'édition de pages en utilisant la syntaxe [Markdown](https://fr.wikipedia.org/wiki/Markdown), directement via [Git](HowtoGit) ou via une interface web. Au niveau rendu, il génère des pages HTML avec un style personnalisable via des templates, et permet aussi l'export en TXT/ODT/etc ; il intègre également un moteur de recherche (regardez en haut à droite !). <https://wiki.evolix.org> utilise fièrement Gitit !

## Installation

### Paquet Debian

~~~
# apt install gitit

$ gitit --version
gitit version 0.10.4 +plugins
Copyright (C) 2008 John MacFarlane
This is free software; see the source for copying conditions.  There is no
warranty, not even for merchantability or fitness for a particular purpose.
~~~


### Compilation classique

Attention, la compilation est assez longue.

~~~
# apt install cabal-install ghc zlib1g-dev
# adduser --disabled-password gitit
# sudo -u gitit -i
~~~

**Note : `/tmp` doit être en EXEC pour l'installation**

~~~
$ cabal update
$ cabal --reorder-goals install gitit
$ echo PATH="$HOME/.cabal/bin:$PATH" > ~/.profile

$ gitit --version
gitit version 0.12.3 -plugins
~~~



### Compilation avec patch pour la recherche de mots partiels

Par défaut, Gitit ne recherche que les mots entiers.

Voici un patch pour pouvoir rechercher aussi les mots partiels.

Attention, la compilation est assez longue.

~~~
# adduser --disabled-password gitit
# sudo -u gitit -i
~~~

**Note : `/tmp` doit être en EXEC pour l'installation**

~~~
$ git clone https://github.com/jgm/gitit.git src/gitit
$ cd src/gitit
~~~

Mettre le patch suivant dans un fichier `patch` :

~~~{.diff}
--- src/Network/Gitit/Handlers.hs→  2024-10-10 17:38:54.599750889 +0200
+++ src/Network/Gitit/Handlers.hs→  2024-10-10 17:21:30.964162724 +0200
@@ -274,5 +274,5 @@
                    else liftIO $ E.catch (search fs SearchQuery{
                                                   queryPatterns = patterns
-                                                , queryWholeWords = True
+                                                , queryWholeWords = False
                                                 , queryMatchAll = True
                                                 , queryIgnoreCase = True })
--- data/static/js/search.js→   2024-10-10 17:38:54.599750889 +0200
+++ data/static/js/search.js→   2024-10-10 17:21:30.960162606 +0200
@@ -1,14 +1,4 @@
 jQuery.fn.highlightPattern = function (patt, className)
 {
-    // check if patt starts or ends with non-word character
-    // and set regex boundary accordingly.
-    var start = '\\b(';
-    var end = ')\\b';
-    if (/\W/.test(patt.charAt(0))) {
-       var start = '\(?=\\W\)(';
-    };
-    if (/\W/.test(patt.charAt(patt.length - 1))) {
-       var end = ')\(?!\\w\)';
-    }
     // escape regex metacharacters that may be in the patt
     var epatt = patt.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")
@@ -16,5 +6,5 @@
     // patt is a space separated list of strings - we want to highlight
     // an occurrence of any of these strings as a separate word.
-    var regex = new RegExp(start + epatt.replace(/ /g, '| ') + end, 'gi');
+    var regex = new RegExp('(' + epatt.replace(/ /g, '| ') + ')', 'gi');

     return this.each(function ()
~~~

Et appliquer le patch :

~~~
$ patch -p0 < patch
patching file src/Network/Gitit/Handlers.hs
patching file data/static/js/search.js
~~~

Puis compiler GitIt :

~~~
$ nice -n19 cabal install exe:gitit
$ ~/.cabal/bin/gitit  --version
~~~

Faire le ménage :

~~~
$ cd ~
$ rm -r src
~~~


## Configuration

### Configuration de Gitit

~~~
$ gitit --print-default-config > gitit.conf
~~~

Exemple de configuration pour `gitit.conf` :

~~~
address: 127.0.0.1
repository-type: Git
repository-path: git
session-timeout: 36000
default-extension: md
default-page-type: Markdown
front-page: index
no-delete: index, Help
use-cache: yes
~~~

Pour voir la liste des langages supportés pour le _highlighting_ :

~~~
$ pandoc -v
~~~


### Configuration du reverse proxy Nginx

Exemple de configuration [Nginx](HowtoNginx) `/etc/nginx/sites-available/gitit.conf` :

~~~
server {
    server_name     gitit.example.com;
    listen          0.0.0.0:80;
    listen          [::]:80;
    location /_register {
        allow 192.0.2.42;
        deny all;
        proxy_pass        http://127.0.0.1:5001/_register;
        proxy_set_header  X-Real-IP  $remote_addr;
        proxy_redirect off;
    }
    location / {
        proxy_pass        http://127.0.0.1:5001/;
        proxy_set_header  X-Real-IP  $remote_addr;
        proxy_redirect off;
    }
    access_log      /var/log/nginx/gitit/access.log;
    error_log       /var/log/nginx/gitit/error.log;
}
~~~


## Lancement

On peut lancer manuellement :

~~~
$ gitit -f gitit.conf
~~~


### Exemple d'unité systemd mono-instance


`/etc/systemd/system/gitit.service` avec `gitit` installé [manuellement](#manuellement) dans `$HOME/.cabal` :

~~~
[Unit]
Description=Wiki gitit
After=network.target

[Service]
User=gitit
Group=gitit
UMask=0027

WorkingDirectory=/home/gitit
PrivateTmp=true

ExecStart=/home/gitit/.cabal/bin/gitit -f /home/gitit/gitit.conf
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
Alias=wiki.service
~~~


### Exemple d'unité systemd multi-instances

Installer Gitit manuellement comme indiqué [ci-dessus](#manuellement) pour l'utilisateur `USER` (nommé `gitit` dans la section mono-instance).

`/etc/systemd/system/gitit@.service` avec `gitit` installé manuellement dans `/home/USER/.cabal` :

~~~
[Unit]
Description=Gitit %i
After=network.target

[Service]
WorkingDirectory=/home/%i/
ExecStart=/home/%i/.cabal/bin/gitit -f /home/%i/gitit.conf
Type=simple
User=%i
Group=%i
UMask=0027
PrivateTmp=true
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
~~~

Pour activer et démarrer l'instance de USER :

~~~
# systemctl enable gitit@USER.service
# systemctl start gitit@USER.service
~~~

### Configuration pour la connection via SSO

Si on souhaite utiliser un SSO pour l'authentication à gitit, on peux utiliser l'option `authentication-method: generic`

En activant cela, gitit s'attendra à ce qu'on lui passe le header `REMOTE_USER` pour indiquer l'utilisateur connecté.

Voici un exemple de configuration NGINX pour utiliser [oauth2-proxy](/HowtoOauth2-Proxy) :

~~~
            auth_request /oauth2/auth;
            error_page 401 = /oauth2/sign_in;

            auth_request_set $email  $upstream_http_x_auth_request_email;
            auth_request_set $preferred_username $upstream_http_x_auth_request_preferred_username;
            proxy_set_header REMOTE_USER "$preferred_username \<$email\>";
~~~

Si on souhaite utiliser l'authentification SSO *uniquement* pour l'édition ( `require-authentication: modify` ) alors il faut que le header `REMOTE_USER` soit défini pour :

* les chemins en `/_edit*` , on peut simplement créer une `location  ^~ /_edit` avec le `auth_request`

* les requetes POST ; pour cela, il faut utiliser un `if` vers une location "placeholder" qui servira uniquement à faire le `auth_request` avant d'envoyer le POST 

~~~
        location / {
            proxy_pass        http://127.0.0.1:5003;
            proxy_set_header  X-Real-IP  $remote_addr;
            proxy_redirect off;
            if ($request_method = POST) {
                    rewrite (.*) /_batman$1;
            }
        }
        location ^~ /_batman {
            #rewrite .* /_batman break;
            auth_request /oauth2/auth;
            error_page 401 = /oauth2/sign_in;
            auth_request_set $email  $upstream_http_x_auth_request_email;
            auth_request_set $preferred_username $upstream_http_x_auth_request_preferred_username;
            proxy_set_header REMOTE_USER "$preferred_username \<$email\>";
            proxy_pass        http://127.0.0.1:5003;
            proxy_set_header  X-Real-IP  $remote_addr;
            proxy_redirect off;
            rewrite /_batman(.*) $1 break;
        }
~~~

> Le `rewrite ... break;` est important, il permet au rewrite de ne pas changer le location de la requete ( et donc de rester dans le location `_batman` avec le `auth_request` )

## Mise-à-jour

Pour la mise-à-jour d'une installation manuelle, on fera simplement :

~~~
$ cabal update
$ cabal install gitit
# systemctl restart gitit
~~~


## Troubleshooting

### Page en markdown non interprétée

Si Gitit affiche certaines pages en markdown sans l’interpréter, il faut supprimer le contenu du répertoire `cache` dans son répertoire racine :

~~~
# rm -rf <GITIT_ROOT>/cache/*
~~~


### Hooks Git non exécutés

Attentions, les hooks `./.git/hooks/` du dépôt doivent être exécutables.


