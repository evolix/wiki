---
categories: hardware
title: Howto Master switch APC
...

Documentation AP89XX :

* Firmware version 6.8.0 : <https://download.schneider-electric.com/files?p_File_Name=990-5569M_EN.pdf&p_Doc_Ref=990-5569_EN&p_enDocType=User+guide>
* Firmware version 5.X.X : <https://download.schneider-electric.com/files?p_File_Name=JSAI-862KZR_R2_EN.pdf&p_Doc_Ref=SPD_JSAI-862KZR_EN&p_enDocType=User+guide>

## Installation

Deux solutions : via un [câble console](#port-série), ou via telnet/ssh. Attention : telnet est actif par défaut (ssh inactif) jusqu'à la version 6.8, à partir de laquelle c'est ssh qui devient actif par défaut (telnet inactif).

Via telnet ou ssh, en résumé :

- Relever l'adresse MAC de l'APC
- Depuis une autre machine : arp -s <future_IP_APC> <MAC_APC> (Note : cela doit apparemment être fait sur une machine avec future_IP_APC comprise dans le réseau d'une *vraie* interface (par exemple, cela ne semble pas marcher avec eth0:0... même en faisant arp -i eth0:0 -s <future_IP_APC> <MAC_APC>))
- Depuis cette autre machine, faire un : ping -s 113 <future_IP_APC>
- Puis : telnet/ssh <future_IP_APC> avec le login/passwd apc/apc.
- Enfin, bien penser à lui fixer son adresse IP définitive !!

## Manipulations de base

- Définir le nom et domaine de l'APC
- Positionner les paramètres réseaux (IP/Netmask/Gateway/DNS)
- Changer le login/passwd
- Configurer SNMP
- Configurer un serveur NTP, et se mettre en timezone GMT
- Configurer les paramètres mails (serveur SMTP/from/to) pour recevoir des notifications par mail
- Relever les machines branchées à ses ports

Selon la version du firmware de l'APC, les commandes sont différentes. Une fois connecté à un APC, la version de son firmware est visible en haut à droite, ainsi qu'avec la commande `about` sous "APC OS(AOS)" sauf pour la version interactives où il n'y a justement pas de commande, mais des menus à suivre.

## Commandes CLI pour le firmware version interactif

### Contrôle des ports

~~~
# Lister tous les ports :
"1- Device Manager" > "2- Outlet Management" > "1- Outlet Control/Configuration"

# À partir de ce menu, redémarrer le port n°3 :
"3- XXXXX                    ON" > "1- Control Outlet" > "3- Immediate Reboot" OU "2- Immediate Off" puis "1- Immediate On"
~~~


## Commandes CLI pour le firmware version 5.X.X

### Commandes générales

~~~
apc> help tcpip # gestion du réseau
Usage: tcpip  -- Configure and display TCP/IP v4 parameters
    tcpip [-S <enable | disable>]
          [-i <ipv4 address>]
          [-s <subnet mask>]
          [-g <gateway>]
          [-d <domain name>]
          [-h <host name>]    # hostname “réseau”, différent du hostname “système” (system -n)

user -an root -ap PASSWORD # changer le mot de passe du compte admin "root", l'utilisateur est créé si nécessaire
userDelete apc # supprimer l'utilisateur par défaut "apc"
system -n APC42 # changer le nom de l'APC
snmp -S enable # activer le SNMP
dns -p X.X.X.X # configurer le serveur dns primaire
ntp -p X.X.X.X # configurer le serveur ntp primaire
console -S telnet # activer le telnet ET désactive le SSH (défaut sur cette version)
reboot # redémarrer le firmware (ne redémarre pas les prises)
system # lister des infos de l'APC
prodInfo # lister des infos de l'APC
~~~

Les paramètres mail et de DST peuvent se régler depuis l'interface web.

### Contrôle des ports

~~~
olStatus all # lister tous les ports avec leur status ON ou OFF
olStatus 3 # afficher le status du port 3
olName 3 salut # renommer le port 3 en "salut"
olReboot 3 # reboot le port 3 (salut, donc)
olOff 3 # éteintre le port salut
olOn 3 # allumer le port salut

appcli # passe en mode CLI spécial pour gérer les outlets
~~~


## Commandes CLI pour le firmware version 6.X.X


### Commandes générales

~~~
apc>help tcpip # gestion du réseau
Usage: tcpip  -- Configure and display TCP/IP v4 parameters
    tcpip [-S <enable | disable>]
          [-i <ipv4 address>]
          [-s <subnet mask>]
          [-g <gateway>]
          [-d <domain name>]
          [-h <host name>]    # hostname “réseau”, différent du hostname “système” (system -n)

user -n root -pw PASSWORD -pe Administrator -e enable # changer le mot de passe du compte admin "root", l'utilisateur est créé si nécessaire
user -n apc -cp apc -e disable # désactiver l'utilisateur par défaut "apc"
system -n APC42 # changer le nom de l'APC
console -t enable # activer le telnet ET désactive le SSH (telnet par défaut jusqu'à 6.8, SSH par défaut à partir de 6.8)
snmp -S enable -c1 public -a1 read -n1 Y.Y.Y.Y # activer le snmp sur la communauté "public" en RO, avec autorisation pour l'IP "Y.Y.Y.Y" uniquement
dns -p X.X.X.X # configurer le serveur dns primaire
ntp -p X.X.X.X # configurer le serveur ntp primaire
email -g1 enable -s1 X.X.X.X -t1 foo@example.com -r1 custom -f1 bar@example.com # configurer l'envoi d'alertes mail
reboot # redémarrer le firmware (ne redémarre pas les prises)
system # lister des infos de l'APC
prodInfo # lister des infos de l'APC
~~~

Le paramètre de DST peut se régler depuis l'interface web.

### Contrôle des ports

~~~
olStatus all # lister tous les ports avec leur status ON ou OFF
olStatus 3 # afficher le status du port 3
olName 3 salut # renommer le port 3 en "salut"
olReboot 3 # reboot le port 3 (salut, donc)
olOff 3 # éteintre le port salut
olOn 3 # allumer le port salut

appcli # passe en mode CLI spécial pour gérer les outlets
~~~

## Cas d'une machine avec double alimentation

Lorsqu'une machine a 2 alimentations (que ce soit via 2 APCs différents, ou sur 2 prises du même APC) et que l'on souhaite éteindre ou redémarrer cette machine, il faut bien penser à éteindre les ports d'alimentation des **2 prises**, sinon la machine restera alimentée par au moins une autre prise :

* On **éteint** l'alimentation de la **1e prise**
* On **éteint** l'alimentation de la **2e prise**
* On **rallume** l'alimentation de la **2e prise**
* On **rallume** l'alimentation de la **1e prise**

## APCs avec compteurs par prises

Les modèles AP86XX ont un compteur par prise, permettant de savoir quelle machine consomme combien (intensité en A, puissance en W, et énergie consommée en Wh).

Cependant comme indiqué sur la [FAQ](https://www.apc.com/us/en/faqs/FA156074/#PDUG2), les valeurs en dessous de 0.5A et 50W pour chaque prise ne sont pas mesurées et sont indiqués comme 0A/0W. Cela perd souvent en intérêt, surtout lorsqu'une machine est double alimentée : sa charge se répartit plus ou moins entre les 2 alimentations, qui est donc trop faible pour être mesurée.

## Monitoring via SNMP

Les OID `.1.3.6.1.4.1.318.1.1.12` (rPDU) et `.1.3.6.1.4.1.318.1.1.26` (rPDU2, à préférer) permettent d'intérroger les APCs sur leur état, notamment l'intensité utilisée sur le moment.

Voir la page [HowtoCollectd#plugin-snmp]() pour des exemples de configuration avec Collectd.

## Mise à jour du firmware

**Voir si l'on possède les anciens binaires... certains binaires récents semblent incompatibles avec du vieux matériel**

Pour un AP8959EU3 :

Documentation : <https://download.schneider-electric.com/files?p_File_Name=990-5569M_EN.pdf&p_Doc_Ref=990-5569_EN&p_enDocType=User+guide> page 169/178, titre "Use a USB drive to transfer and upgrade the files".

Nous faisons la mise à jour via clé USB.

* Télécharger le firmware .exe depuis le site APC : <https://www.apc.com/shop/fr/fr/products/APC-Rack-PDU-2G-Switched-16A-230V-21-C13-3-C19-IEC309-Cord/P-AP8959EU3> dans "Téléchargement de logiciels"
* Extraire le fichier exécutable : `unar apc_hw0X_aosXXX_rpdu2gXXX_bootmonXXX.exe`
* Si ce n'est pas déjà le cas, formater sa clé USB en FAT32 : `mkfs.vfat -F 32 /dev/sdX1`
* Dans la clé USB, créer un dossier "apcfirm"
* Dans ce dossier "apcfirm", placer les 3 binaires extraits depuis l'exécutable : apc_hw0X_aos_XXX.bin, apc_hw0X_bootmon_XXX.bin, apc_hw0X_rpdu2g_XXX.bin
* Dans ce même dossier "apcfirm", créer un fichier texte nommé "upload.rcf". Ce fichier texte doit contenir une ligne par binaire, pour indiquer leur noms, sous le format suivant :

~~~
BM=apc_hw0X_bootmon_XXX.bin
AOS=apc_hw0X_aos_XXX.bin
APP=apc_hw0X_rpdu2g_XXX.bin
~~~

* La clé USB doit donc contenir ces 3 binaires et le fichier "upload.rcf", tous à l'intérieur du dossier "apcfirm".
* Une fois la clé USB prête, il suffit de la brancher à l'APC à mettre à jour, puis d'appuyer (par exemple à l'aide d'un trombone) sur le bouton "reset" (sans rester appuyer).
* L'interface de management de l'APC redémarre et se met à jour ; l'opération dure environ 3 minutes. Les prises de courant ne se coupent pas.
* Une fois la mise à jour terminée, l'APC indique pendant quelques secondes sa version puis retourne dans son état habituel.

## Chainer les APC entre eux

Afin d'administrer jusqu'à 4 APCs à travers une seule IP, il est possible de les chainer entre eux.
Ils doivent pour cela être en version 6 minimum.

* Brancher l'APC 1 au réseau, et lui configurer une IP
* Brancher le port OUT de l'APC 1 au port IN de l'APC 2, et faire de même pour le nombre d'APCs souhaités (limité à 4)
* Chaque extrémité doit être bouché avec un bouchon réseau fourni : le port IN du premier APC et le port OUT du dernier APC

## Port série

### screen

~~~
# screen /dev/ttyUSB0 9600
~~~

### minicom

~~~
# minicom -b 9600 -D /dev/ttyUSB0
~~~

Il est aussi possible de créer un fichier de configuration et la commande `minicom` avec le nom de la configuration :

~~~
# cat /etc/minicom/minirc.apc
pu port /dev/ttyUSB0
pu baudrate 9600
pu bits 8
pu parity N
pu stopbits 1
pu rtscts No

# minicom apc
~~~

### cu

~~~
# cu -l /dev/cuaU0 -s 9600
~~~

Attention, parfois les caractères sont illisibles (problème matériel ?)
et il faut taper les commandes à l'aveugle...

## Reset des accès

Le reset des accès n'impacte pas les prises de courant.

Appuyer sur le bouton RESET. Après quelques secondes, la LED réseau clignote alternativement en orange et vert. Il faut à ce moment là appuyer à nouveau sur le bouton RESET. On peut ainsi accéder à l'APC via le port série avec le login **apc** et le mot de passe **apc**. Attention, il faut vite se connecter : l'accès est reset pendant uniquement 30 secondes.

## FAQ

### Mails : Switched Rack PDU: Communications lost/established.

Si l'APC envoie régulièrement des mails ayant ces objets, il est temps de le remplacer. En effet, le mail `Communications lost` signifie que l'interface de gestion du réseau dans le PDU a perdu la communication interne avec les autres composants du PDU, tel que les prises. Ainsi, si le PDU perd l'alimentation ou est redémarré lorsque la communication est perdue, les prises électriques pourraient redémarrer ou ne pas s'allumer puisque l'interface de gestion et les prises ne peuvent pas communiquer.

### Mails : Network Interface restarted.

Si l'APC ne reçoit rien sur sa carte réseau pendant 9min30s, alors l'APC suppose qu'il y a un problème réseau et redémarre sa carte réseau pour éviter/corriger un éventuel problème.

Source (et autres raisons possibles) : <https://www.apc.com/us/en/faqs/FA156063/>