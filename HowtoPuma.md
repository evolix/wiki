---
categories: web
title: Howto Puma
...

[Puma](http://puma.io/) est un serveur d'application Ruby on rails.

## Prérequis

Installation de ruby :

~~~
apt install ruby
~~~

Installation de Puma :

~~~
gem install puma
~~~

Création du dossier de configuration :

~~~
mkdir -m 0750 /etc/puma
~~~

Création d'un service SystemD en mode utilisateur :

~~~
cat > /etc/systemd/user/puma.service <<EOF
[Unit]
Description=Puma HTTP server for Ruby Apps : %u
After=network.target

[Service]
WorkingDirectory=%h/www
UMask=0027
PIDFile=%h/ruby.pid
ExecStartPre=/bin/mkdir -m 0750 %h/run
Environment="MALLOC_ARENA_MAX=2"  # workaround pour réduire les fuites mémoire
ExecStart=/usr/local/bin/puma --bind unix://%h/run/puma.sock?umask=0007 --pidfile %h/run/puma.pid --dir %h/www --config /etc/puma/%u.rb
ExecReload=/bin/kill -USR2 $MAINPID
KillMode=process
#Restart=on-failure

[Install]
WantedBy=multi-user.target
Alias=puma.service
EOF
~~~

Correction des droits du service SystemD

~~~
chmod 644 /etc/systemd/user/puma.service
~~~

## Configuration

Création du fichier de configuration de Puma pour l'utilisateur $USER :

~~~
cat > /etc/puma/$USER.rb <<EOF
environment 'production'
workers 2
threads 0, 4
tag 'Puma $USER'
EOF
~~~

Correction des droits du fichier de configuration

~~~
chmod 640 /etc/puma/$USER.rb
chown $USER: /etc/puma/$USER.rb
~~~

## Reverse proxy

Nous avons fait écouter Puma sur un socket Unix, maintenant il faut mettre en place un reverse proxy [Nginx](HowtoNginx) ou [Apache](HowtoApache#mod_proxy_http) vers :

~~~
unix:/home/$USER/run/puma.sock
~~~

## Gestion du service puma

**A lancer en mode utilisateur !**

Démarrer/éteindre l'application :

~~~
systemctl --user start/stop puma
~~~

Recharger la configuration après avoir modifier /etc/puma/$USER.rb (pas de coupure) :

~~~
systemctl --user reload puma
~~~

Redémarrer l'application :

~~~
systemctl --user restart puma
~~~

Activer/désactiver l'application au démarrage :

~~~
systemctl --user enable/disable puma
~~~


## Troubleshooting

### Problèmes de fuite mémoire

Comme workaround, il faut ajouter dans l'unité `systemd` de Puma :

~~~
Environment="MALLOC_ARENA_MAX=2"
~~~

Puis relancer :

~~~
systemctl --user daemon-reload
systemctl --user restart puma
~~~

