---
categories: network
title: Howto RIPE Atlas
...

* RIPE Atlas : <https://atlas.ripe.net/>
* Documentation : <https://atlas.ripe.net/docs/>

Les sondes [Atlas](https://atlas.ripe.net/) sont des petites machines connectées à Internet partout dans le monde. Une sonde peut être obtenue par n'importe qui en [s'adressant au RIPE](https://atlas.ripe.net/get-involved/become-a-host/). Elles permettent de mesurer la qualité de service d'Internet à l'aide de différents tests (ping, traceroute, DNS…) automatiquement et régulièrement exécutés vers différentes destinations.


## Mesures personnalisées

### Interface web

Des mesures aussi bien ponctuelles que régulières peuvent être créées depuis <https://atlas.ripe.net/measurements/form> (compte nécessaire).

### Outil en ligne de commande

* Documentation : <https://ripe-atlas-tools.readthedocs.io/en/latest/>

On utilise l'outil [ripe-atlas-tools](https://github.com/RIPE-NCC/ripe-atlas-tools) pour faire des mesures en ligne de commande. L'outil est packagé sous Debian depuis Stretch et sous OpenBSD (paquet `ripe.atlas.tools`), ou peut être installé depuis GitHub.

~~~
# apt install ripe-atlas-tools
~~~

Attention, pour utiliser ces outils vous devez avoir un compte et vous créer une clé d'API avec les autorisations "Schedule a new measurement" et "Get results from a non-public measurement".

Il faut ensuite l'ajouter dans la configuration de l'outil :

~~~
$ ripe-atlas configure --set authorisation.create=YOUR_API_KEY
~~~

#### Mesures rapides

~~~
$ aping --target 192.0.2.1
$ atraceroute --target 192.0.2.1
$ adig --query-argument www.example.com
$ antp --target 192.0.2.1
$ asslcert --target www.example.com
~~~

Ces commandes réalisent des pings/traceroutes/dig/ntp/tests SSL depuis 50 sondes Atlas différentes. La source géographique n'est pas indiquée, seule l'ID de la sonde utilisée pour la mesure est affichée.

Le résultat s'affiche sur le terminal. Il peut être revu en se rendant sur l'URL donnée, ou avec l'ID présent dans le lien :

~~~
$ ripe-atlas report ID
~~~

#### Mesures plus précises

Voir les AS par lesquels on passe (atraceroute uniquement) :

~~~
--traceroute-show-asns
~~~

Choisir le nombre de sondes sources :

~~~
--probes 2
~~~

Choisir la source :

~~~
--from-asn 65550
--from-country ca
--from-prefix 192.0.2.0/24
--from-probes 1234,5678
--from-area West                 # Options possibles : WW,West,North-Central,South-Central,North-East,South-East
--from-measurement 9876543210    # Permet de jouer une nouvelle mesure avec les mêmes paramètres que ceux utilisés par la mesure ayant cet ID
~~~

## Reachability test

Le [test d'accessibilité](https://atlas.ripe.net/lir-tools/reachability/) est un test très complet prenant 1 à 2 heures pour se compléter. Des traceroutes sont lancés depuis 1500 sondes à destination d'une IP afin de localiser un souci d'accessibilité de façon précise.

Dans ce test sont donnés :

* Le résultat des 1500 sondes sources avec leur temps d'accès à l'IP cible et le nombre de sauts ;
* Une carte de l'emplacement géographique des 1500 sondes sources ;
* La liste des AS observés dans les traceroutes atteignant la destination ;
* La liste des AS observés dans les traceroutes n'atteignant pas la destination ;
* La liste des AS observés à la fois dans des traceroutes atteignant et n'atteignant pas la destination ;
* Une carte de l'ensemble des AS atteignant la destination avec une vue arborescente ;
* Une carte de l'ensemble des AS n'atteignant pas la destination avec une vue arborescente.

## DomainMON

[DomainMON](https://atlas.ripe.net/domainmon) permet de surveiller les serveurs de noms d'un nom de domaine et d'en connaître la qualité.

## IXP Country Jedi

[IXP Country Jedi](https://www.ripe.net/analyse/internet-measurements/ixp-country-jedi) mesure si le trafic Internet local à un pays reste local.
Le service fournit des visualisations des chemins entre les sondes RIPE Atlas situées dans le même pays (traceroutes entre elles) sous différentes formes pour répondre aux questions suivantes :

* Est-ce que les chemins cibles prennent des détours à l’étranger ?
* Est-ce que nous voyons des IXP (Internet eXchange Point) dans les chemins ?

## RIPEstat

[RIPEstat](https://stat.ripe.net/) permet d'obtenir toutes sortes d'informations liées à n'importe quelle ressource Internet : adresse IP, préfixe IP, numéro d'AS, nom d'hôte, ou code d'un pays.

Les sondes Atlas ne sont pas la seule source d'informations de ces données, puisque le RIPE se sert également de sa base de données en tant que RIR (Registre Internet Régional), et de données depuis des RIS (Routing Information Service, un système collectant des données de routage à partir de route collectors, c'est-à-dire des routeurs connectés en BGP à de nombreux points d'échange à travers le monde).

À travers différents widgets, de nombreuses informations sont disponibles telles que :

* Les préfixes IP annoncées en BGP par un AS particulier dans le temps ;
* Les AS voisins d'un AS, et si ceux-ci sont des voisins "à gauche" (des fournisseurs de transit ou peering) ou "à droite" (des clients) ;
* La visiblité des préfixes d'un AS par les autres AS, permettant de vérifier qu'un préfixe est correctement annoncé par BGP ;
* Des infos sur les DNS d'un AS ou d'une IP particulière ;
* L'adresse email de contact d'un AS en cas d'abus (lien direct : <https://stat.ripe.net/special/abuse>).

Pour anecdote, le widget BGPlay a par exemple permis de voir le [hijack d'un préfixe appartenant à YouTube par Pakistan Telecom](https://www.youtube.com/watch?v=IzLPKuAOe50).

## RIPE Atlas Status Checks

Documentation : <https://labs.ripe.net/Members/suzanne_taylor_muzzin/introducing-ripe-atlas-status-checks>

Il est possible de monitorer le résultat d'une mesure continue à partir de son ID.

Leur résultat étant disponibles sur l'URL <https://atlas.ripe.net/api/v2/measurements/ID_MESURE/status-check>, on peut parser sa sortie de plusieurs façons, par exemple avec le plugin NRPE check_http :

~~~
$ /usr/lib/nagios/plugins/check_http -H atlas.ripe.net -I atlas.ripe.net -r 'global_alert":false' --ssl=1 -u "/api/v2/measurements/<ID_MESURE>/status-check?permitted_total_alerts=1"
~~~

Dans ce cas, si `global_alert":false` est trouvé dans la sortie, cela signifie que l'ensemble de la mesure a abouti : le check renvoie OK.

## FAQ

### Gestion des crédits

Des crédits sont nécessaires pour utiliser les sondes Atlas. Ces crédits se gagnent en adoptant une ou plusieurs sondes Atlas (à demander au RIPE). En étant [LIR](HowtoRIPE), vous pouvez obtenir 1 million de crédits par mois : <https://atlas.ripe.net/lir-tools/credits/>.

