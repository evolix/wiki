# Howto Nagios

* <https://www.nagios.org/documentation/>

## Faire tous les tests d'un fichier de conf

~~~{.bash}
(while read line; do if [[ "$line" =~ ^command ]]; then echo $line; eval $(echo "$line" | cut -d'=' -f2); fi; done) < /etc/nagios/nrpe.d/tests.cfg 
~~~

## FAQ

### Priorité dans nrpe.d/

Depuis [NRPE 3.1](https://github.com/NagiosEnterprises/nrpe/commit/25f49109ecad8acbc88de1077d3ba32e18b66814) (soit Debian Buster) la directive `include_dir` charge les fichiers par ordre alphabétique. On peut donc remplacer une directive définie dans `a.cfg` en la redéfinissant dans le fichier `zzz.cfg`.

Attention, dans les versions précédentes, les fichiers inclus le sont aléatoirement !

