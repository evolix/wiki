---
title: Howto atop
---

# Installation

~~~
apt install atop
~~~

Les logs d'atop peuvent être assez volumineux, pour passer la rétention à 10 jour (4 semaines par défaut), configurer le cron suivant :

~~~
@daily find /var/log/atop/ -name 'atop_*' -mtime +10 -exec rm {} \;
~~~


# Loguer

~~~
atop -w <file>
~~~

# Lire le log

~~~
# Tri des processus par consommation CPU
atop -r <file>
# Tri des processus par consommation mémoire
atop -r <file> -m
# Tri des processus par consommation d'IO disque
atop -r <file> -d
~~~

Raccourcis :

* t : 10 minutes plus tard
* T : 10 minutes précédente

# Fonctionement

Bien que `atop` récupère ses statistiques par interval (10s ou 10min par défaut) il est capable d'identifier les proccessus qui se sont executé et terminé même entre deux échantillons. Ça se fait grace à une fonctionnalité du noyau "[Per-task statistics interface](https://docs.kernel.org/accounting/taskstats.html)", qui est distincte de /proc. Atop accède à cette API via son service `atopacctd`.


# Déprécié

## Accounting kernel ad-hoc

Dans d'ancienne version d'atop, l'accounting kernel n'était pas intégré dans atop, il était géré avec `psacct` ou `acct`. Ces processus séparés écrivaient dans le fichier `/var/log/account/pacct` sans avoir de rotation qui s'accumulait sur le disque et dont l'espace disque n'était pas facilement libérable, car géré par le noyau.

Maintenant l'accoutning kernel est géré par atop lui-même via le démon `atopacctd`, donc le fichier `/var/log/account/pacct` problématique n'est plus utilisé par défaut. Et la documentation suivante n'est plus a utiliser.

### Activer l'accounting kernel ad-hoc

~~~
# apt install acct
Turning on process accounting, file set to '/var/log/account/pacct'.
~~~

### Lancer atop avec un le chemin du fichier d'accounting ad-hoc

~~~
# ATOPACCT="/var/log/account/pacct" atop -w atop.file
~~~

### Stopper l'accounting kernel ad-hoc

~~~
accton off
~~~