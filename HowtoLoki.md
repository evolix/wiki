---
categories: sysadmin
title: HowtoLoki
...

* Documentation : <https://grafana.com/docs/loki/latest/>

Loki est un système d'agrégation de logs "multitenants" Hautement Disponible et performant, car il indexe seulement certains champs des logs (metadatas). 

## Installation

Documentation : <https://grafana.com/docs/loki/latest/setup/install/>

## Configuration

Documentation : <https://grafana.com/docs/loki/latest/configure/>

## Administration

## Optimisation

## Monitoring

### Nagios

### Munin

## Plomberie

## FAQ
