---
title: Howto i3, a tiling window manager
category: window-manager
---

**Documentation officielle** : <http://i3wm.org/docs/>

## Installation

### Sous Debian

En installant le méta-paquet **i3** on obtient **i3-wm** lui-même et plusieurs outils utiles (**i3lock** , **i3status** , **dunst**…).

~~~
# apt install i3
~~~

On peut aussi se contenter du paquet **i3-wm** pour ne pas installer les outils complémentaires.

Pour démarrer automatiquement i3 au lancement de la commande `startx`, il suffit d'ajouter la ligne suivante dans `~/.xinitrc`:

~~~
exec /usr/bin/i3
~~~

### Sous OpenBSD

On peut installer le paquet **i3** qui va aussi installer le paquet **i3status** qui permet d'avoir la barre d'information

~~~
# pkg_add i3
~~~

À noter qu'i3 utilise [pledge(2)](https://man.openbsd.org/pledge) afin de minimiser la portée d'une
faille de sécurité qui serait présente dans i3.

## Barre d'état

Une barre d'état est proposée par i3 via la commande `i3status` qui affiche une ligne de texte d'information, mise à jour en continu.

i3 affiche par défaut cette barre sur tous les écrans, en dessous des "workspaces".

Via la configuration, il est possible de modifier ce comportement. Par exemple la placer en haut, seulement sur l'écran principal :

~~~
bar {
  position top
  tray_output primary
  status_command i3status
}
~~~

## Utile

### Verrouillage de session rapide

i3 propose un verrou de session avec la commande `i3lock`.
On peut créer un raccourci pour le faire facilement :

~~~
bindsym $mod+exclam exec i3lock --dpms
~~~

### Verrouillage automatique de la session

Au lancement de i3, il est possible de lancer également un programme qui va verrouiller la session après une certaine inactivité. Par exemple avec `xautolock` :

~~~
xautolock -time 10 -locker i3lock
~~~

### Déplacer des "workspaces" sur un autre écran

Pour déplacer le "workspace" courant sur un autre écran (à placer dans la config de i3) :

~~~
bindsym $mod+Control+Shift+Right move workspace to output right
bindsym $mod+Control+Shift+Left move workspace to output left
~~~

Si on branche/débranche régulièrement des écrans, il peut être utile d'automatiser le retour de certains "workspaces" sur un écran ajouté :

~~~{.bash}
# list all worspaces on the internal display, except "worspace 1" (using "jq" for json parsing)
i3_workspaces=$(i3-msg -t get_workspaces | jq '.[]  | select(.output == "eDP1" and .num != 1) | .num')
for i in $i3_workspaces; do
  i3-msg -q "workspace ${i}; move workspace to output right"
done
~~~

### Faire une capture d'écran

~~~
bindsym Print exec scrot -e 'mv $f ~/tmp/ && sxiv ~/tmp/$f'
~~~

La commande exécutée est `scrot […]` et il est évidemment possible de la personnaliser ou la remplacer par une autre.


## Definir des noms pour chaque workspace

On peut définir des noms pour certains workspace à la place des numéros par défaut, pour ce faire on définit d'abord les noms dans des variables comme ceci :

~~~
set $workspace1 "1: Term"
set $workspace2 "2: Web"
~~~

Ensuite, on définit nos variables dans les raccourcis qui permettent de switcher d'un workspace à l'autre avec les touches mod+numero :

~~~
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
~~~

Et aussi dans les raccourcis qui permettent de déplacer une fenêtre d'un workspace à un autre :

~~~
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
~~~

## Assigner une application à un workspace

Pour assigner une application à un workspace, on utilise la fonction *assign* en précisant la *class* de l'application que l'on souhaite utiliser, suivi du numéro du workspace ou du nom de la variable du workspace, si on a définit ces workspaces avec des variables, par exemple :

~~~
assign [class="Firefox-esr"] $workspace2
~~~

Pour obtenir la class de l'application on peut utiliser la commande *xprop* et de cliquer sur la fenêtre de l'application, on obtient en console un résultat comme ceci :

~~~
$ xprop

WM_CLASS(STRING) = "Navigator", "Firefox-esr"
~~~

## Définir une application en mode fenêtre flottante

Si vous voulez avoir certaines applications en mode fenêtre flottante et pas en mode tilling dans un conteneur de fenêtre, il faut utiliser le mode "floating", exemple avec pavucontrol :

~~~
for_window [class="Pavucontrol"] floating enable
~~~

On peut redimensionner une fenêtre en mode floating avec le click droit de la souris.

## Customisation de l'apparence de i3

* Changer la couleur des fenêtres :

Pour changer la couleur des fenêtres il faut d'abord définir chaque couleur de chaque variable :

~~~
set $bg-color            #2f343f
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935
~~~

Pour obtenir les numéros de chaque couleur, on peut utiliser gimp ou inkscape, ou alors sur ce site : [color-picker](https://www.webpagefx.com/web-design/color-picker/)

Définition des variables :

        * $bg-color : couleur d'arrière-plan de la fenêtre
        * $inactive-bg-color : couleur d'arrière-plan d'une fenêtre inactive
        * $text-color: couleur du texte de la fenêtre
        * $inactive-text-color : couleur du texte d'une fenêtre inactive
        * $urgent-bg-color : couleur d'une fenetre qui signale une notification

Ensuite, on définit nos variables pour chaque "état" des fenêtres :

~~~
#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          #00ff00
client.unfocused        $inactive-bg-color $inactive-bg-color $inactive-text-color #00ff00
client.focused_inactive $inactive-bg-color $inactive-bg-color $inactive-text-color #00ff00
client.urgent $urgent-bg-color $urgent-bg-color $text-color #00ff00
~~~

Pour chaque "état" on définit une couleur pour la bordure, le fond, le texte et l'indicateur horizontal / vertical.
L'indicateur Horizontal / Vertical est une barre très fine qui permet de savoir dans quel sens la prochaine fenêtre va être dispose.

Voici la définition de chaque "état" de fenêtre :

        * client.focused : fenêtre sur laquelle on a le focus
        * client.unfocused : fenêtre sur laquelle on n'a pas le focus
        * client.focused_inactive: fenêtre qui est dans un conteneur, mais qui n'a pas de focus pour le moment.
        * client.urgent : fenêtre avec une notification

* Changer la couleur de la barre d'état

Pour changer la couleur de la barre, que l'on utilise i3status ou i3blocks, on peut utiliser les mêmes variables que pour les fenêtres, mais avec des fonctions différentes, il faut en premier lieu créer une section *colors* dans la section *bar* du fichier de configuration de i3 :

~~~
bar {
        colors {
                background $bg-color
                separator #757575
                #                  border             background         text
                focused_workspace  $bg-color          $bg-color          $text-color
                inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
                urgent_workspace   $urgent-bg-color   $urgent-bg-color   $text-color
        }
}
~~~

On utilise toujours les mêmes options pour les bordures, le fond et le texte, mais avec des options différentes :

        * focused_workspace : workspace actif
        * inactive_workspace : workspace inactif
        * urgent_workspace : workspace avec une notification

## Utiliser des icônes pour les noms des workspaces ou pour des notifications de la barre d'état

On peut afficher des icônes dans les noms des workspaces ou pour les notifications de la barre d'état.

Il faut utiliser la police de caractères *Awesome* disponible ici : [Font-Awesome](https://github.com/FortAwesome/Font-Awesome/releases) et la placer dans le dossier /usr/local/share/fonts/

Pour connaître toutes les icônes disponibles dans cette police et pouvoir les copier facilement sans connaître leurs codes, on peut s'aider de ce site [fontawesome-cheatsheet](http://fontawesome.io/cheatsheet/)

Il suffit, ensuite, juste de *copier / coller* l'icône qui nous intéresse.  

## Utiliser i3blocks à la place de i3status

On peut changer la barre d'état par défaut, par la barre alternative i3blocks.

Elle permet d'afficher, par défaut, plus d'information, comme l'occupation mémoire ou le trafic réseau.

Pour l'installer dans Debian :

~~~
apt install i3blocks
~~~

La configuration par défaut se fait dans */etc/i3blocks.conf,* mais on peut indiquer un autre fichier de configuration, en espace utilisateur par exemple, en modifiant le fichier *~/i3/config* :

~~~
bar {
        status_command i3blocks -c ~/.config/i3/i3blocks.conf

}
~~~

## Démarrer une application au démarrage, dans un workspace prédéfini

Pour démarrer  une application au démarrage, dans un workspace prédéfini, on utilisera cette commande dans le fichier de configuration de i3
:

~~~
exec --no-startup-id i3-msg 'workspace $workspace2; exec /usr/bin/firefox-esr'
~~~

On utilise la commande i3-msg, avec le nom de la variable du workspace ou son numéro, suivi de l'exécution de l'application.

## Assigner automatiquement un workspace a un écran :

Pour assigner automatiquement un workspace a un écran il faut dans son *.i3/config* :

~~~
workspace 1 output HDMI1
~~~

Ou avec le nom de la variable du workspace ou le nom du workspace :

~~~
workspace $workspace1 output HDMI1

workspace "1: Term" output HDMI1
~~~

## Sauvegarder la disposition d'un workspace

* Positionner votre workspace avec la disposition des fenêtres que vous souhaitez.

* Exécuter cette commande, qui va enregistrer la disposition dans un fichier .json :

~~~
i3-save-tree --workspace N > ~/.i3/workspace_N.json
~~~

N est le numéro, ou le nom du workspace souhaité.


## Restaurer la disposition d'un workspace

Pour restaurer la disposition d'un workspace qui a été sauvegardé avec la méthode précédente, il faut, en premier, créé un script *load_layout.sh* comme ceci :

~~~
#!/bin/bash
i3-msg "workspace M; append_layout ~/.i3/workspace_N.json"
~~~

Où M est le workspace dans lequel vous souhaitez chargé la disposition précédemment enregistrée et N est le nombre d'espace de travail enregistré dans la section précédente.

Il y a plus qu'as exécuté le script pour que le workspace soit chargé au bon endroit.

On peut créer un raccourci qui exécute le script, à mettre dans le fichier *~/.i3/config* :

~~~
bindsym $mod+g exec ~/load_layout.sh
~~~