---
categories: video audio stream flux
title: Howto FFmpeg
...

[FFmpeg](http://ffmpeg.org) est un outil en CLI permettant d'enregistrer, convertir et streamer des flux vidéos et audio.

* Statut de la page : nouvelle.
* [Documentation officielle](https://ffmpeg.org/ffmpeg.html)
* [Wiki officiel](https://trac.ffmpeg.org/wiki)


# Installer FFmpeg

~~~
# apt install ffmpeg
~~~


# Utiliser FFmpeg

## Syntaxe générale

**/!\\ Attention : l'ordre des arguments et des options est important !**

La syntaxe de la commande est la suivante :

~~~
$ ffmpeg [global_options] {[input_file_options] -i input_url} ... {[output_file_options] output_url} ...
~~~

On passe en option plusieurs flux d'entrée et de sortie. Les flux d'entrée sont ré-encodés et redirigés vers les sorties (en fonction des options).

Chaque flux d'entrée est déclaré d'abord avec ses options et un `-i <INPUT_URL>` final (« URL » est à prendre au sens large).

Idem pour chaque flux de sortie, mais sans le `-i`.


## Lister les formats et les codecs supportés

~~~
$ ffmpeg -formats
~~~

On peut aussi lister les codecs pour l'encodage et le décodage avec :

~~~
$ ffmpeg -decoders
$ ffmpeg -encoders
~~~


## Enregistrer

### Enregistrer du son à partir d'un micro

Pour la capture audio, on utilise les flux fournis par le serveur de son [PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/), qui fait le lien avec les pilotes ALSA des périphériques audio.

Lister les périphériques d'entrée de PulseAudio :

~~~
$ pactl list short sources | grep input
~~~

Il est possible de fournir à FFmpeg le numéro du périphérique audio (colonne 1, par exemple : `1`) ou bien son nom complet (colonne 2, par exemple : `alsa_input.pci-0000_00_1f.3.analog-stereo`) :

Puis lancer un enregistrement (voir [Lister les formats et les codecs supportés](https://wiki.evolix.org/HowtoFFmpeg#lister-les-formats-et-les-codecs-support%C3%A9s) pour les formats supportés en sortie) :

~~~
$ ffmpeg -f pulse -i <SOURCE_NUMBER|NAME> <OUTPUT_FILE>

$ # Par exemple :
$ ffmpeg -f pulse -i 1 output.wav
~~~

Utiliser `Ctrl+c` pour stopper l'enregistrement.

Les blocs d'arguments indiqués dans la section [Syntaxe générale](#syntaxe-générale) sont les suivants :

~~~
         (Entrée audio ) (Flux de sortie)
$ ffmpeg (-f pulse -i 1) (output.wav)
~~~


### Enregistrer une discussion audio

En plus de la source du micro, on ajoute la sortie audio comme seconde source avec un autre bloc d'arguments `-i`.

Lister les périphériques d'entrée et de sortie de PulseAudio :

~~~
$ pactl list short sources | grep -E "(in|out)put"
~~~

Il est possible de fournir à FFmpeg le numéro des périphériques audio (colonne 1) ou bien leur nom complet (colonne 2). Le nom du périphérique de sortie audio de sortie à utiliser contient normalement `.monitor` (par exemple : `alsa_output.pci-0000_00_1f.3.analog-stereo.monitor`) :

Il faut également indiquer comme option de sortie `-filter_complex amix=inputs=2` pour dire à FFmpeg de fusionner les deux entrées audio.

~~~
$ ffmpeg -f pulse -i <INPUT_SOURCE_NUMBER|NAME> -f pulse -i <OUTPUT_SOURCE_NUMBER|NAME> -filter_complex amix=inputs=2 <OUTPUT_FILE>

$ # Par exemple :
$ ffmpeg -f pulse -i 1 -f pulse -i 2 -filter_complex amix=inputs=2 output.wav
~~~

Utiliser `Ctrl+c` pour stopper l'enregistrement.

Les blocs d'arguments indiqués dans la section [Syntaxe générale](#syntaxe-générale) sont les suivants :

~~~
         (Entrée audio ) (Sortie audio ) (              Flux de sortie            )
$ ffmpeg (-f pulse -i 1) (-f pulse -i 2) (-filter_complex amix=inputs=2 output.wav)
~~~


### Enregistrer une vidéo à partir d'un affichage Xorg

Pour la capture vidéo (image seulement, sans le son), on utilise les flux fournis fournis par le serveur graphique [Xorg](https://www.x.org/wiki/).

Lister les écrans connectés et identifier la résolution (par exemple `2560x1080`) et le décalage (par exemple `+2560+0`) de l'écran à enregistrer :

~~~
$ xrandr | grep -E "[[:space:]]connected"
~~~

Puis lancer un enregistrement (voir [Lister les formats et les codecs supportés](https://wiki.evolix.org/HowtoFFmpeg#lister-les-formats-et-les-codecs-support%C3%A9s) pour les formats supportés en sortie) :

~~~
$ ffmpeg -f x11grab -video_size <SCREEN_RESOLUTION> -i :0.0<OFFSET> <OUTPUT_FILE>
$ # Par exemple :
$ ffmpeg -f x11grab -video_size 2560x1080 -i :0.0+2560+0 output.webm
~~~

Utiliser `Ctrl+c` pour stopper l'enregistrement (la clôture prend un peu de temps pour l'encodage, ne pas forcer l'arrêt en faisant 2 fois `Ctrl+c`).


### Enregistrer une visioconférence (audio + vidéo)

Combiner les options des sections [Enregistrer une discussion audio](#enregistrer-une-discussion-audio) et [Enregistrer une vidéo à partir d’un affichage Xorg](#enregistrer-une-vidéo-à-partir-dun-affichage-xorg) (voir ces sections pour trouver les bons arguments) :

~~~
$ ffmpeg -f pulse -i <INPUT_SOURCE_NUMBER|NAME> -f pulse -i <OUTPUT_SOURCE_NUMBER|NAME>  # flux audio en entrée \
         -f x11grab -video_size <SCREEN_RESOLUTION> -i :0.0<OFFSET>  # flux vidéo en entrée  \
         -filter_complex amix=inputs=2 <OUTPUT_FILE>  # flux audio+vidéo en sortie
$ # Par exemple :
$ ffmpeg -f pulse -i 1 -f pulse -i 2 -f x11grab -video_size 2560x1080 -i :0.0+2560+0 -filter_complex amix=inputs=2 output.webm
~~~

Utiliser `Ctrl+c` pour stopper l'enregistrement (la clôture prend un peu de temps pour l'encodage, ne pas forcer l'arrêt en faisant 2 fois `Ctrl+c`).

Les blocs d'arguments indiqués dans la section [Syntaxe générale](#syntaxe-générale) sont les suivants :

~~~
         (Entrée audio ) (Sortie audio ) (                 Vidéo                         ) (              Flux de sortie             )
$ ffmpeg (-f pulse -i 1) (-f pulse -i 2) (-f x11grab -video_size 2560x1080 -i :0.0+2560+0) (-filter_complex amix=inputs=2 output.webm)
~~~


## Éditer

### Couper un son ou une vidéo

Couper le début :

~~~
ffmpeg -i <INPUT_FILE> -ss <NEW_START_TIME> -c copy <OUTPUT_FILE>
~~~

L'option `-c copy` indique à FFmpeg de copier le flux entrant sans le ré-encoder.
Le format de `NEW_START_TIME` est : `[<HH>:]<MM>:<SS>[.<ms>]`.

Couper la fin :

~~~
ffmpeg -sseof -<NEW_END_TIME> -i <INPUT_FILE> -c copy <OUTPUT_FILE>
~~~

Noter le `-` devant `NEW_END_TIME`, car on part de la fin (EOF = end of file).

Extraire une partie spécifique :

~~~
ffmpeg -i <INPUT_FILE> -ss <START_TIME> -to <END_TIME> -c copy <OUTPUT_FILE>
~~~

On peut profiter de la commande de découpe pour faire aussi une conversion. Par exemple d'un fichier `.wav` en `.ogg` (Vorbis), auquel cas l'option `-c copy` n'est plus pertinente :

~~~
ffmpeg -i <INPUT_FILE> -ss <START_TIME> -to <END_TIME> -c:a libvorbis <OUTPUT_FILE>
~~~


# Troubleshooting

## Problème de lecture de vidéos avec Firefox

Il semble que certaines versions de Firefox ne supportent pas l'espace de couleurs par défaut `GBRP` pour le codec VP9 du format `WEBM`.

A la place, utiliser l'espace de couleurs `YUV420P` en ajoutant l'option `-pix_fmt yuv420p` au flux de sortie :

~~~
         (Flux d'entrée...) (         Flux de sortie         )
$ ffmpeg (...             ) (... -pix_fmt yuv420p <OUTPUT_FILE>)
~~~

De la même manière, pour corriger l'espace de couleurs d'une vidéo existante :

~~~
$ ffmpeg -i <INPUT_FILE> -pix_fmt yuv420p <OUTPUT_FILE>
~~~


