**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**


## Installation d'une Debian dans une VM Hyper-V

#### Carte réseau non détectée

Si Debian ne détecte pas la carte réseau lors de l'installation, c'est que la VM n'a pas été configuré avec la bonne carte. Lors de l'ajout d'une interface réseau, il faut bien sélectionner _Legacy Network Adapter_.
Voir ici pour la procédure en image : <http://wiki.debian.org/WindowsServerHyperV> (§ Configure Networking)