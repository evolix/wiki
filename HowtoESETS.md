---
categories: system antivirus eset esets
title: Howto ESET Antivirus
...

Les solutions proposées par ESET sont propriétaires. Les informations ci-dessous sont indiquées seulement pour aider les utilisateurs, et ne sont en aucun cas une incitation à utiliser ces solutions.


# ESET Server Security, EFS (anciennement ESET File Security)

## Version 8

Attention : ne pas confondre avec ESET Endpoint Antivirus for Linux qui est la version desktop.

Documentation : <https://help.eset.com/efs/8/fr-FR/>


### Installation


Il faut d'abord installer les dépendances :

```
# apt install gcc make linux-headers-amd64 libelf-dev
```

Télécharger le binaire d'installation :

~~~
cd /tmp  # important pour les permissions
wget https://download.eset.com/com/eset/apps/business/efs/linux/latest/efs.x86_64.bin
sh efs.x86_64.bin
 
rm efs.x86_64.bin
rm efs-8.1.685.0.x86_64.deb

~~~

Si vous avez oublié ou si vous avez un échec pendant l'installation, pour pouvoir la relancer ensuite il faut d'abord faire le ménage :

```
# apt purge esets
# apt purge efs
```

Vous pourrez ensuite ré-exécuter le binaire d'installation.

L'installateur extrait un fichier `.deb` puis l'installe automatiquement avec `apt` (ne pas l'installer manuellement).

Notez bien les informations de connexion à la fin du processus d'installation.

EFS installe tous ses fichiers dans `/opt/eset/efs/` et démarre un service systemd nommé `efs`.

L'interface web est ensuite disponible sur le port 9443.


#### Impossible d'exécuter l'installateur

Solution : vérifiez que le binaire a bien les droits d'exécution (`u+x`) et que la partition n'est pas montée avec l'option `noexec`.


#### Erreur "Download is performed unsandboxed as root as file couldn't be accessed by user _apt (13: Permission denied)"

Apt n'a pas les droits pour accéder au `.deb` téléchargé.

Solution : copiez le binaire d'installation et exécutez-le dans `/tmp`.


#### Interface web inaccessible sur le port 9443

Si après une installation ou réinstallation, l'interface web n'est pas accessible sur le port 9443, il est possible qu'elle n'ait pas été activée.

Elle peut être administrée avec l'utilitaire `setgui` :

~~~
# /opt/eset/efs/sbin/setgui -h
Usage: setgui [OPTIONS..]
ESET Server Security GUI setup utility

Options:
  -g, --gen-password            Generate new password
  -p, --password=PASSWORD       Set new password
  -f, --passfile=FILE           Set new password from file
  -r, --gen-cert                Generate new private key and certificate
  -a, --cert-password=PASSWORD  Set certificate password
  -l, --cert-passfile=FILE      Set certificate password from file
  -i, --ip-address=IP:PORT      Server address
  -c, --cert=FILE               Import certificate
  -k, --key=FILE                Import private key
  -d, --disable                 Disable GUI
  -e, --enable                  Enable GUI
~~~

Par exemple, pour activer l'interface web et générer mot de passe, avec un certificat SSL dédié :

~~~
# /opt/eset/efs/sbin/setgui -e -g -c <SSL_CERT> -k <PRIV_KEY>
~~~


### Mise-à-jour de la licence

La mise-à-jour peut se faire via l'interface web.

Alternativement, uploader le fichier de licence sur le serveur, puis exécuter :

```
# /opt/eset/efs/sbin/lic -f $path/nod32.lic
```

Pour vérifier que la licence a bien été importée :

```
# /opt/eset/efs/sbin/lic
```


## [Obsolète] Version 4

* Documentation : <https://help.eset.com/efs/4/en-US/>

~~~
To COMPLETE INSTALLATION or UPDATE the Product:
   * Import the license file: /opt/eset/esets/sbin/esets_lic --import file.lic
   * Enter acquired username/password information into the global section
     of main configuration file /etc/opt/eset/esets/esets.cfg
   * Start main daemon service: /etc/init.d/esets start

To UNINSTALL the Product:
   * Uninstall the package: dpkg --purge esets

To KEEP your KNOWLEDGE Up-To-Date:
   * Read the User's Guide in /opt/eset/esets/share/doc.
   * Read manual page esets.cfg(5) (use 'man esets.cfg').

To REPORT Bugs or Problems:
   * Please, visit: www.eset.com/support
~~~

Pour lancer un scan :

~~~
/opt/eset/esets/sbin/esets_scan --no-symlink --no-quarantine --clean-mode=none /home
~~~



# [Obsolète] Antivirus Mail (Mail Security)

* Documentation : <https://help.eset.com/ems_linux/4/en-US/>
* Téléchargement : <https://www.eset.com/us/business/server-antivirus/mail-security-linux/download/>.

L'installation consiste a exécuter une archive auto-extractible qui contient un .deb. Une fois installé, le logiciel ESETS réside dans `/opt/esets`.

Pour activer la mise à jour des signatures antivirus il faut spécifier le `av_update_username/password` dans `/etc/opt/eset/esets/esets.cfg`.

Si vous avez un proxy ou un pare-feu il faut autoriser `update.eset.com`.

Pour démarrer le démon :

~~~
# systemctl start esets
~~~

Mise à jour manuelle des signatures antivirus :

~~~
# /opt/eset/esets/sbin/esets_update --username="$LOGIN" --password="$PASS"
~~~

Pour mettre à jour la version de Antivirus Mail, il faut récupérer la dernière version depuis l'interface web, Home > Product version > Check for new version.

Puis l’exécuter comme ceci :

~~~
sh ./esets.amd64.deb.bin
~~~

Cela redémarre esets.service, il conserve la configuration courante et la licence associé.


## Utilisation en content_filter postfix

Exemple de configuration :

~~~
[smtp]
agent_enabled = yes
num_proc = 1
num_thrd = 2
listen_addr = "localhost"
listen_port = 2526
server_addr = "localhost"
# Port classique 10025 pack mail Evolix, second serveur SMTP post amavis
# Ou port 10024 pour passer à Amavis
server_port = 10025
timeout_client = 30
add_header_xvirus = yes
add_header_received = yes
action_av = "scan"
action_as = "scan"
av_eml_subject_modification_mask = "as_spam"
av_eml_header_modification_mask = ""
av_eml_footnote_modification_mask = ""
av_eml_header_template = "%avstatus%"
#av_eml_subject_template = "[Virus %avstatus%]"
as_eml_subject_template = "[%asstatus%]"
as_eml_header_modification = yes
av_mail_notified_users = "postmaster@example.com"
av_scan_obj_archives = yes
av_scan_obj_mime = yes
av_scan_obj_sfx = yes
av_scan_obj_rtp = yes
av_scan_app_adware = yes
av_scan_app_unsafe = yes
av_scan_app_unwanted = yes
av_scan_pattern = yes
av_scan_heur = yes
av_scan_adv_heur = yes
av_scan_smart = yes
action_av_infected = "accept"
action_av_notscanned = "accept"
action_av_deleted = "accept"
action_as_spam = "accept"
av_eml_footnote_log_all = yes
av_clean_mode = "standard"
av_scan_obj_max_size = 0
av_scan_archive_max_level = 10
av_scan_archive_timeout = 30
av_scan_archive_max_size = 0
av_quarantine_enabled = no
syslog_facility = "mail"
syslog_class = "error:warning:summall:summ:partall:part:info:debug"

~~~


## Interface web

Exemple de configuration :

~~~
[wwwi]
agent_enabled = yes
listen_addr = "127.0.0.1"
listen_port = 9090
username = "admin"
password = "password"
~~~


## Voir les logs

~~~
journalctl -f -u esets
~~~


## Mise-à-jour de la licence

Uploader le fichier de licence sur le serveur, puis exécuter :

```bash
# /opt/eset/esets/sbin/esets_lic --import=$path/nod32.lic
```

Pour vérifier que la licence a bien été importée :

```bash
# /opt/eset/esets/sbin/esets_lic
```
