**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Ubuntu sous KVM

## Ubuntu 12.04 LTS server

Après installation, Ubuntu 12.04 LTS server lance un démarrage + ou - graphique (bootsplash & co).
Cela empêche d'avoir un accès "console" via KVM.

La solution est de désactiver le démarrage "graphique".

Dans le fichier /etc/default/grub décommenter la ligne :

~~~
GRUB_TERMINAL=console
~~~

Puis

~~~
# update-grub2 
Generating grub.cfg ...
Found linux image: /boot/vmlinuz-3.8.0-34-generic
Found initrd image: /boot/initrd.img-3.8.0-34-generic
Found linux image: /boot/vmlinuz-3.8.0-29-generic
Found initrd image: /boot/initrd.img-3.8.0-29-generic
Found memtest86+ image: /boot/memtest86+.bin
done
# reboot
The system is going down for reboot NOW!
~~~
