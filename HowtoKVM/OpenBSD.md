**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto OpenBSD sous KVM

## Installation

~~~
$ kvm-img create -f qcow2 routeur0.qcow2 3G
Formatting 'routeur0.qcow2', fmt=qcow2 size=3221225472 encryption=off cluster_size=0
$ wget <http://mirror.evolix.org/pub/OpenBSD/4.8/amd64/cd48.iso>
# kvm -hda routeur0.qcow2 -cdrom cd48.iso -boot d -m 384 -k fr -net nic,model=e1000 -net tap,vlan=0,ifname=tap0
~~~

Ensuite on redémarre ainsi :

~~~
# kvm -hda routeur0.qcow2  -m 384 -k fr -net nic,model=e1000 -net tap,vlan=0,ifname=tap0
~~~

Attention, avant le démarrage d'OpenBSD, on doit désactiver _mpbios_ :

~~~
>> OpenBSD/adm64 BOOT 3.15
bsd> bsd -c
UKS> disable mpbios
 52 mpbios0 disabled
UKS> quit
~~~

...puis, une fois démarré, on désactive définitivement :

~~~
# config -ef /bsd
disable mpbios
quit
~~~

## Avec libvirt

Dans cet exemple, on utilise les drivers virtio supportés dans OpenBSD depuis la 5.3 ( <http://www.openbsd.org/cgi-bin/man.cgi?query=virtio&manpath=OpenBSD%20Current&sektion=4&format=html> ).

On n'oublie pas de spécifier la disposition clavier, car elle peut poser problème lorsque l'on se connecte en VNC.

~~~
virt-install -r 1024 --vcpus=1 --os-variant=openbsd4 --accelerate -v -c install54.iso -w bridge:lan0,model=virtio --vnc --disk path=routeur0.img,bus=virtio --name routeur0  -k fr
~~~

Une fois l'installation terminée, la commande virsh start routeur0 permettra de démarrer la nouvelle VM.