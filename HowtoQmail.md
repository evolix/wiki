**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowtoQmail

## Rediriger vers un autre serveur SMTP

~~~
echo ":le-serveur-smtp.net" > /var/qmail/control/smtproutes
~~~

## Vider une mailqueue

En cas de catastrophe (un serveur OVH par exemple) :

~~~
# /etc/init.d/qmail stop
# cd /home/qmail/queue/mess
# for file in `find . -type f` ; do echo $file ; rm -f $file ; rm -f ../info/$file ; rm -f ../remote/$file ; done
# /etc/init.d/qmail start
~~~

Ou pour vider avec pattern : <http://www.redwoodsoft.com/~dru/mailRemove/>

Exemple, supprime les mails qui contiennent dans les en-têtes : 

~~~
# python mailRemove.py --real anonymous
~~~

Pour reconstruire l'aborescence de _queue_ : <http://guide.ovh.com/OEGqueue#link3>
