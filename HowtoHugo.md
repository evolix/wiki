# Howto Hugo

* Documentation : <https://gohugo.io/documentation/>

[Hugo](https://gohugo.io) est un générateur de sites web statiques publié sous licence Apache 2.0. Comme son nom le suggère, il est écrit en langage Go. Il utilise le système de modèles (*templates*) de Go pour traiter le texte et le html.

## Installation

Il est possible d'installer Hugo à partir des dépôts standards de Debian 10 (Buster) ou Debian 11 (Bullseye) :

~~~
# apt install hugo
~~~

Cela dit, le développement rapide de Hugo fait que ces versions (0.55.x pour Buster et 0.80.x pour Bullseye) sont plutôt anciennes. Il est possible de télécharger le .deb de la [dernière version stable](https://github.com/gohugoio/hugo/releases) directement sur GitHub et de l'installer sur son ordinateur avec `dpkg -i <nom-du-paquet.deb>` (en tant que *root*).

~~~
$ hugo env
hugo v0.92.2+extended linux/amd64 BuildDate=2022-02-23T16:47:50Z VendorInfo=debian:0.92.2-1
GOOS="linux"
GOARCH="amd64"
GOVERSION="go1.17.3"
~~~

## Premiers pas

Pour se familiariser avec Hugo on peut démarrer un nouveau projet de site de cette façon :

~~~
$ hugo new site un-site-en-exemple
cd un-site-en-exemple
git init
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke themes/ananke
cp -r themes/ananke/exampleSite/{config.toml,content/,static/} .
sed -i -e 's/\[\"github\.com\/theNewDynamic\/gohugo-theme-ananke\"\]/\x27ananke\x27/' config.toml
hugo server
~~~

La dernière commande, `hugo server`, lance un micro serveur web sur `http://localhost:1313`. Tout changement au projet est immédatement détecté et le rendu est mis à jour. Les principaux méta-éléments du site (URL, titre, langue, thème) sont lus par le serveur dans le fichier `config.toml`.

Si on est prêt à générer le site statique (au lieu de simplement le prévisualiser sur `http://localhost:1313`), il suffit de lancer la commande `hugo` sans aucun paramètre et les fichiers finaux (à téléverser sur le serveur de production) seront placés dans le dossier `public`.

## Structure d'un projet

L'exemple de site ci-haut aura cette arborescence :

~~~
un-site-en-exemple/
├── archetypes/
├── content/                     # fichiers .md
├── data/                        # listes, dictionnaires .yml
├── layouts/                     # modèles .html avec syntaxe Hugo
├── public/                      # fichier du site final
├── static/                      # .css, .js, polices, images
├── themes/
│   └── ananke/
│       └── ...                  # arborescence semblable à celle d'un site
└── config.toml
~~~

## Modèles (*templates*)

La [documentation officielle](https://gohugo.io/templates/introduction/) permet de comprendre la syntaxe utilisée par Hugo pour manipuler des variables et des fonctions.

En plus des variables que l'on peut définir soi-même, il y en a plusieurs très pratiques qui font parties de Hugo comme `.Site.Author`, `.Site.BaseURL`, `.Site.Copyright` (site), `.Content`, `.Data`, `.Date`, `.Permalink`, `.TableOfContents` (page), etc.

## Fonctionalités avancées

Hugo est très riche en fonctionnalités de [gestion de contenus](https://gohugo.io/hugo-modules/) (pages, billets, taxonomies, sommaires, menus, diagrams, codes abrégés, multilinguisme) et est extensible grâce à un système de modules ([fonctionalités](https://gohugo.io/hugo-modules/), [thèmes](https://themes.gohugo.io/)), des [tuyaux (*pipes*)](https://gohugo.io/hugo-pipes/) pour le traitement de ressources, des outils de développement, etc.

Pour les adeptes de la ligne de commandes, Hugo est capable de [générer un script d'autocomplétion](https://gohugo.io/commands/) pour bash, zsh et autres interpréteurs.
