**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Serveur Dedibox

Interventions Online : <https://console.online.net/fr/assistance/status>

## Installation

À installer sous Debian évidemment ;-)

Bien avoir en tête que l'outil de partitionnement en ligne de Dédibox est nul : dès qu'il y a un partitionnement
digne de ce nom, cela échoue. Il faut donc faire un partitionnement léger avec seulement 3 partitions
primaires et repasser en mode "secours" et finir le partitionnement à la main.

Autre particularité, il semble conseillé d'installé un noyau backporté (2.6.39).
Voici ce que l'on nous a dit sur l'IRC #online :

~~~
15:29 <@philou> reg_: en debian 6 fait mettre kernel 2.6.39 si ca down trop souvent l'ipmi ou prob 
                reseau. kernel d'avant son pas top pour la gestion carte resau
[...]
15:34 <@philou> reg_: si tu as pas de prob reste en .32 
15:35 <@philou> reg_: mais au cas ou 
~~~

Les infos d'Online pour le noyau :

<http://documentation.online.net/fr/serveur-dedie/systemes-d_exploitation/distribution-debian>

### Installation via KVM iLO

<http://documentation.online.net/fr/serveur-dedie/systemes-d_exploitation/installation-depuis-hp-ilo>

## IPv4

Les nouvelles machines sont configurés en DHCP. On pourra sans problème désactiver le DHCP et passer en mode statique.

## IPv6

L'IPv6 n'est pas activé par défaut en auto-configuration (pas de RA) et il faut demander un « bloc IPv6 » dans la console Online.
Si vous avez un bloc IPv6 qui commence par 2a01, il faut d'abord le résilier dans « Mon compte ». Puis commander un nouveau bloc IPv6 qui commence par 2001.

Il y a un bloc par client, mais il est possible de créer 1 sous-réseau par machine.

/etc/network/interfaces

~~~
iface eth0 inet6 static
    address 2001:0e0b:2368:100::115/56 #exemple
    pre-up dhclient -cf /etc/dhcp/dhclient6.conf -6 -P eth0
    pre-down dhclient -x -pf /var/run/dhclient6.pid
~~~

Dans /etc/dhcp/dhclient6.conf, mettre le DUID fourni dans la console Online.

~~~
interface "eth0" {        
    send dhcp6.client-id DUID;
    request;
}
~~~

Autoriser le trafic DHCPv6 dans minifirewall

~~~
#dhclient6 - obligatoire chez online
/sbin/ip6tables -t filter -A INPUT -i $INT -p udp --dport 546 -d fe80::/64 -j ACCEPT
/sbin/ip6tables -t filter -A OUTPUT -o $INT -p udp --dport 547 -j ACCEPT
~~~

S'assurer qu'il n'y a pas d'autoconf sur l'infterface eth0 :

~~~
# sysctl -w net.ipv6.conf.eth0.autoconf=0
# echo "net.ipv6.conf.eth0.autoconf=0" >> /etc/sysctl.conf
~~~

Puis relancer le réseau :

~~~
ifdown eth0 && ifup eth0
~~~

## IP FailOver

### Pour adresse secondaire

Online propose l'utilisation d'adresses IP supplémentaires sur un serveur et potentiellement "transportables" sur un autre serveur, d'où leur nom IP FailOver.

Pour en ajouter une, il faut le gérer via la Console Online, puis on ajoute une configuration de ce type sous Debian (attention, le masque est bien un /32 !!) :

~~~
auto eth0:0
iface eth0:0 inet static
        address 192.0.43.2/32
~~~

### Pour adresse principale d'une VM

Lorsqu'on gère des VM sur un serveur Online, il est possible d'utiliser une IP Failover comme adresse principale de la VM en suivant ces quelques étapes :

1. Vérifier dans l'interface d'Online/Scaleway que l'IP est bien associée au KVM.

2. Récupérer l'adresse MAC virtuelle indiquée afin de l'associer à l'interface virtuelle de la VM (en utilisant `virsh edit <domain>` pour spécifier la MAC.)

3. Configurer `/etc/network/interfaces` dans la VM (exemple sur Debian 9) : 

```
auto ens3
iface ens3 inet static
    address 62.210.XX.YY/32
    post-up ip route add 62.210.0.1 dev ens3
    post-up ip route add default via 62.210.0.1
    post-down ip route del 62.210.0.1 dev ens3
```

## Ajout de disques

*Attention ! *

Si vous commandez des nouveaux disques, la création d'un nouvel array doit se faire en vKVM. La console Online nécessite de reset tout les volumes RAID… !


~~~
Cela doit être fait manuellement depuis votre KVM et sur le BIOS. Lancer cette opération depuis votre console entraînerait un formatage complet de vos données sur l'Array 1.
~~~


## Gestion de la carte RAID

Les serveurs Dedibox Pro sont équipés d'une carte RAID "DELL H200", qui utilise le chipset SAS2008 de LSI Logic.
Le module noyau pour son support se nomme "mtp2sas".

Un lspci donne : 
~~~
[...]
03:00.0 Serial Attached SCSI controller: LSI Logic / Symbios Logic SAS2008 PCI-Express Fusion-MPT SAS-2 [Falcon] (rev 03)
[...]
~~~ 

### Récupération de l'utilitaire

La version la plus récente de l'utilitaire sas2ircu se trouve (étrangement) chez IBM : <http://www-947.ibm.com/support/entry/portal/docdisplay?lndocid=MIGR-5084943> [[BR]]
Le binaire fonctionne indifféremment en 32 ou 64 bits.

### Interrogation de la carte RAID

Lancer sas2ircu sans argument(s) affiche la liste des options :

~~~
LSI Corporation SAS2 IR Configuration Utility.
Version 4.00.00.00 (2009.10.12) 
Copyright (c) 2009 LSI Corporation. All rights reserved. 

SAS2IRCU: No command specified.
  sas2ircu <controller #> <command> <parameters> ...
    where <controller #> is:
      Number between 0 and 255
    where <command> is:
      DISPLAY    - display controller, volume and physical device info
      LIST       - Lists all available LSI adapters (does not need ctlr #>
      CREATE     - create an IR volume
      DELETE     - set controller configuration to factory defaults
      HOTSPARE   - make drive a hot spare
      STATUS     - display current volume status info
      CONSTCHK   - Start Consistency Check operation on the specified IR Volume
      ACTIVATE   - Activate an Inactive IR volume
      LOCATE     - Locate a disk drive on an enclosure
      LOGIR      - Upload or Clear IR Log data
      BOOTIR     - Select an IR Boot Volume as primary boot device
      BOOTENCL   - Select an Enclosure/Bay as primary boot device
    where <parameters> are:
      Command specific values; enter "sas2ircu <controller #> <command>"
      to get command specific help

~~~

Pour afficher l'état du premier volume RAID :

~~~
# ./sas2ircu 0 DISPLAY
~~~

~~~
LSI Corporation SAS2 IR Configuration Utility.
Version 4.00.00.00 (2009.10.12) 
Copyright (c) 2009 LSI Corporation. All rights reserved. 

Read configuration has been initiated for controller 0
------------------------------------------------------------------------
Controller information
------------------------------------------------------------------------
  Controller type                         : SAS2008
  BIOS version                            : 7.01.09.00
  Firmware version                        : 2.15.63.00
  Channel description                     : 1 Serial Attached SCSI
  Initiator ID                            : 0
  Maximum physical devices                : 39
  Concurrent commands supported           : 3439
  Slot                                    : 1
  Segment                                 : 0
  Bus                                     : 3
  Device                                  : 0
  Function                                : 0
  RAID Support                            : Yes
------------------------------------------------------------------------
IR Volume information
------------------------------------------------------------------------
IR volume 1
  Volume ID                               : 79
  Status of volume                        : Okay (OKY)
  RAID level                              : RAID1
  Size (in MB)                            : 1907200
  Physical hard disks                     :
  PHY[0] Enclosure#/Slot#                 : 1:0
  PHY[1] Enclosure#/Slot#                 : 1:1
------------------------------------------------------------------------
Physical device information
------------------------------------------------------------------------
Initiator at ID #0

Device is a Hard disk
  Enclosure #                             : 1
  Slot #                                  : 0
  State                                   : Optimal (OPT)
  Size (in MB)/(in sectors)               : 1907729/3907029167
  Manufacturer                            : SEAGATE 
  Model Number                            : ST32000444SS    
  Firmware Revision                       : KS68
  Serial No                               : 9WM3AX59
  Protocol                                : SAS
  Drive Type                              : SAS_HDD

Device is a Hard disk
  Enclosure #                             : 1
  Slot #                                  : 1
  State                                   : Optimal (OPT)
  Size (in MB)/(in sectors)               : 1907729/3907029167
  Manufacturer                            : SEAGATE 
  Model Number                            : ST32000444SS    
  Firmware Revision                       : KS68
  Serial No                               : 9WM3AT71
  Protocol                                : SAS
  Drive Type                              : SAS_HDD

Device is a Enclosure services device
  Enclosure #                             : 1
  Slot #                                  : 8
  State                                   : Standby (SBY)
  Manufacturer                            : DP      
  Model Number                            : BACKPLANE       
  Firmware Revision                       : 1.07
  Serial No                               : 0BL00BH
  Protocol                                : SAS
  Drive Type                              : SAS_HDD
------------------------------------------------------------------------
Enclosure information
------------------------------------------------------------------------
  Enclosure#                              : 1
  Logical ID                              : 5782bcb0:09870a00
  Numslots                                : 9
  StartSlot                               : 0
  Primary Boot Slot                       : 0
------------------------------------------------------------------------
SAS2IRCU: Command DISPLAY Completed Successfully.
SAS2IRCU: Utility Completed Successfully.

~~~

### Monitoring du RAID

On peut ainsi écrire un petit script qui pourra être placé en cron pour avertir en cas de défaillance d'un disque ou du contrôleur :

~~~
#!/bin/sh

# CONFIGURATION
BINARY=/opt/sas2ircu
TMPFILE=/var/tmp/raid-status
MAIL=admin@example.com
HOST=`hostname`

NBVOL=1
NBDISKS=2

# TESTING BINARY
if [ ! -e ${BINARY} ]; then
    echo "Check RAID impossible" | mail -s "[warn] Probleme RAID sur ${HOST}" ${MAIL}
    exit 3
fi

# TESTING RAID STATE
${BINARY} 0 DISPLAY > ${TMPFILE}
STATE=$?

NBVOLOK=$(grep 'Okay (OKY)' ${TMPFILE} | wc -l)
NBDISKSOK=$(grep 'Optimal (OPT)' ${TMPFILE} | wc -l)

if [ $NBVOLOK -eq $NBVOL ] && [ $NBDISKSOK -eq $NBDISKS ]; then
    exit 0
else
    cat ${TMPFILE} | mail -s "[warn] Probleme RAID sur ${HOST}" ${MAIL}
    exit 2
fi
~~~

Il est aussi possible d'utiliser ce script avec Nagios, ses codes de sortie correspondent aux niveaux d'erreur :

* Exit code 3 - UNKNOWN : lorsque le binaire "sas2ircu" est absent
* Exit code 2 - CRITICAL : lorsqu'un disque/contrôleur est en erreur
* Exit code 0 - OK : lorsqu'on peut dormir sur nos deux oreilles

## Backup avec RPN Rsync

Documentation Online :
<https://documentation.online.net/fr/serveur-dedie/tutoriel/rpn-rsync?s>[]=rsync (Notamment procédure pour commander un espace de stockage).

Note, bien que conseillé par Online, <https://documentation.online.net/fr/serveur-dedie/tutoriel/rpn-by-online.net#activation_et_dhcp,> l'utilisation du DHCP n'est pas obligatoire. Il suffit de lancer une fois un dhclient pour récupérer l'adressage et on pourra le positionner en dur dans /etc/network/interfaces pour éviter de faire tourner un client DHCP.

Avec cet espace de stockage on pourra faire des sauvegardes incrémentielles avec rsync en utilisant les hardlinks.

Exemple en CLI.

Synchro :

~~~
# RSYNC_PASSWORD=XXX rsync -av --delete --link-dest=/current/ /etc/ rsync://evolix-xxx@evolix-xxx.backup.rpn-rsync.online.net/evolix-xxx_backup/2014-03-17
# RSYNC_PASSWORD=XXX rsync -av --delete --link-dest=/current/ /etc/ rsync://evolix-xxx@evolix-xxx.backup.rpn-rsync.online.net/evolix-xxx_backup/2014-03-18
~~~

Effacement vieux backups :

~~~
# mkdir /tmp/empty
# RSYNC_PASSWORD=XXX rsync -av --delete /tmp/empty/ rsync://evolix-xxx@evolix-xxx.backup.rpn-rsync.online.net/evolix-xxx_backup/2014-03-17
~~~

Lister le contenu, pour par exemple chercher ce qu'on peut restaurer :

~~~
# RSYNC_PASSWORD=XXX rsync --list-only rsync://evolix-xxx@evolix-xxx.backup.rpn-rsync.online.net/evolix-xxx_backup/
~~~

Ou via evobackup-linkdest : <https://github.com/benpro/evobackup/tree/link-dest>

## Migration adresses IP

Online a modifié son réseau en 2014 en utilisant des nouvelles adresses IP (IPv4 et IPv6) afin d'avoir un routage indépendant d'Iliad Entreprises.
En conséquence, toutes les anciennes Dedibox ont du migrer vers un nouvel adressage avant septembre 2014.

<http://documentation.online.net/fr/serveur-dedie/tutoriel/migration-network>

### Procédure

<http://documentation.online.net/fr/serveur-dedie/tutoriel/migration-network/serveur/debian_ubuntu>

1/ Pré-migration : ajouter la nouvelle adresse IP en alias dans /etc/network/interfaces et cliquer sur "Pré-migrer"

2/ Migration finale : inverser l'ancienne et la nouvelle adresse IP (l'ancienne adresse IP devient un alias) : la gateway est donc modifiée ! [[BR]]
Il faut cliquer sur "Migrer" puis envoyer au moins un paquet DHCP : via dhclient (dhclient -d eth0 + Ctrl+C ou reboot car il y a un boot PXE chez Online)[[BR]]
Il faut bien sûr penser aux différents détails : firewall, application, DNS / reverse DNS, etc. qu'implique un changement d'adresse IP.

## RPN v2

« La technologie du RPN v2 (VXLAN) vous permet de créer votre réseau niveau 2 à travers vos serveurs éligibles au RPN v2. »

Documentation : <https://documentation.online.net/fr/dedicated-server/network/rpn-v2>

On crée un groupe sur la page [RPN v2](https://console.online.net/fr/server/rpn/v2), en ajoutant les serveurs concernés. Il faut veiller à ce que les serveurs qui doivent se voir aient bien le même VLANID.

La page d'état des serveurs, dans la section "Adresses principales > rpn v2 member", indique la MAC de l'interface réseau sur laquelle le RPN est utilisable. Sur le serveur on retrouve l'interface réseau grâce à cette MAC.

Sur tous les serveurs du groupe il faut installer le paquet **vlan** : `# apt install vlan`.

On peut choisir son plan d'adressage réseau au sein du groupeRPNv2. Nous prendrons par exemple `192.0.2.X`.

### configuration réseau simple

Dans cet exemple, l'interface `eno3` porte l'IP publique.
C'est sur l'interface `enp2s0f0` que l'on monte le RPN, avec le VLAN 3000.

~~~
# cat /etc/network/interfaces

auto eno3
iface eno3 inet dhcp

auto enp2s0f0
iface enp2s0f0 inet dhcp

auto enp2s0f0.3000
iface enp2s0f0.3000 inet static
    address 192.0.2.10/24
~~~

### configuration réseau avec bridge

Exemple en créant une interface eth1.3000 et un bridge derrière (tout ce qui sera dans le bridge sera donc tagué dans le VLAN 3000).


~~~
# cat /etc/network/interfaces

auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1.3000 inet manual

auto br1
iface br1 inet static
    bridge_ports eth1.3000
    up echo 0 > /sys/class/net/br1/bridge/multicast_snooping
    address 192.0.2.10/24
~~~

### Eteindre serveur via IDRAC

<https://documentation.online.net/fr/dedicated-server/hardware/configure-ipmi-server/dell-idrac-controller?s[]=idrac&s[]=8>

## Mode rescue et boot UEFI

Les nouveaux serveurs (octobre 2018) semblent amorcer en mode UEFI et le mode rescue ne fonctionne pas, le démarrage est fait sur le disque dans tout les cas. Il est donc impossible d'accéder au mode rescue en boot UEFI…   
Mais il y a une astuce qui est d'accéder à iLO ou iDRAC (ou autre vKVM) et d'appuyer sur la touche F12 (ou autre) pour faire un démarrage en mode iPXE. En sélectionnant iPXE cela va amorcer sur l'image de rescue (si vous avez mis le serveur en mode rescue dans la console Online).

## Suivi des abuse

Lorsque Online/Scaleway reçoit une plainte à propos de spams envoyés par un serveur, un dossier est créé.

**Une notification est envoyée, mais seulement au propriétaire du serveur, pas à l'infogérant.**

En cas de non résolution de cette plainte dans un délai de 48h, le serveur peut-être suspendu.
Dans ce cas, l'infogérant est notifié.