---
categories: storage
title: Howto NFS
...

* Documentation : <http://nfs.sourceforge.net/nfs-howto>

NFS (Network File System) est un protocole réseau permettant le partage de fichiers entre un serveur et des clients.

# Serveur NFS

## Installation

On utilise généralement le serveur NFS natif du kernel Linux

~~~
# apt install nfs-kernel-server
~~~

Il est aussi possible d'utiliser [Ganesha](/HowtoNFSGanesha), une implémentation orientée « haute disponibilité ».

## Configuration

On peut configurer certaines options via le fichier `/etc/default/nfs-kernel-server` :

~~~
RPCNFSDCOUNT=8
RPCMOUNTDOPTS="--no-nfs-version 4 --debug all"
~~~

> **Note : ** Si vous n'avez pas mis `--no-nfs-version 4`, le montage se fera par défaut en version 4. Il faudra penser à spécifier l'option de montage `nfsvers=3`.

Les partages se configurent dans le fichier `/etc/exports` :

~~~
/srv/nfs 192.0.2.4(rw,root_squash,sync,no_subtree_check)
/srv/nfs2 192.0.2.0/27(ro) 192.0.242(rw) 
~~~

Quelques options possibles :

* `rw` : active le mode read-write (par défaut c'est en read-only)
* `async` : autorise le serveur à répondre aux clients même si les fichiers ne sont pas encore écrits, cela améliore les performances mais c'est plus dangereux (possibilité de perdre des fichiers dans certains cas)
* `no_subtree_check` : désactive certaines vérifications qu'un fichier accédé via NFS appartient bien à un répertoire exporté. Cette option est conseillée en général (c'est même devenu le comportement par défaut)
* `insecure` : désactive la nécessité d'utiliser des ports inférieurs à 1024 pour les clients NFS
* `no_root_squash`/`all_squash` : options pour la gestion du mapping UID/GID

### Mapping uid/gid

Par défaut, les requêtes faites depuis un client avec UID/GID à 0 sont mappées en _anonymous_ : c'est le `root_squash`. Si l'on éviter ce comportement, il faut spécifier l'option `no_root_squash`.

On peut aussi mapper toutes les requêtes vers un _anonymous_ avec l'option `all_squash`, et l'on peut préciser UID/GID avec les options `anonuid` et `anongid`.

Sans l'option `all_squash` les UID/GID sont conservés entre le serveur et tous les clients NFS, il faut donc s'assurer d'avoir une cohérence des UID/GID sur l'ensemble des serveurs concernés.

### État des exports

On peut lister les exports actifs :

~~~
# showmount -e localhost
~~~

# Client NFS

## Installation

~~~
# apt install nfs-common
~~~

## Utilisation

On peut vérifier la connexion vers un serveur NFS ainsi :

~~~
# rpcinfo -p 192.0.2.1
~~~

Pour activer un partage NFS :

~~~
# mount -t nfs -o nfsvers=3 192.0.2.1:/srv/nfs /mnt
~~~

`/etc/fstab` :

~~~
192.0.2.1:/srv/nfs /mnt nfs nfsvers=3 0 0
192.0.2.1:/srv/nfs /mnt nfs auto,rw,_netdev,mountproto=tcp,comment=systemd.automount 0 0
192.0.2.1:/srv/nfs /mnt nfs defaults,_netdev,vers=3,x-systemd.device-timeout=42 0 0
~~~

Voici un exemple de montage NFS :

~~~
# mount | grep nfs
192.0.2.1:/srv/nfs on /mnt type nfs4 (rw,relatime,vers=4,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,port=0,timeo=600,retrans=2,sec=sys,clientaddr=192.0.2.4,minorversion=0,local_lock=none,addr=192.0.2.1)
~~~

Quelques options possibles :

* `nfsvers=3` : force le protocole NFSv3 à la place de NFSv4 
* `soft` : si le serveur NFS ne répond, stoppe les requêtes au bout d'un certain temps (par défaut c'est infini)
* `timeo=n` : temps d'attente (en dixièmes de seconde) avant d'effectuer une nouvelle tentative vers le serveur NFS
* `retry=n` : temps d'attente (en minutes) lors de la commande mount… par défaut c'est environ une semaine !
* `noac` : empêche tout mise en cache des attributs des fichiers
* `rsize=n,wsize=n` : forcer le nombre maximal d'octets pour une requête NFS (en général, cela s'adapte entre le client et le serveur)


# Réseau

NFS est un protocole complexe qui utilise de nombreux ports TCP et UDP.
En général, on autorise tous les accès réseau entre un client et un serveur.


# NFSv3 ou NFSv4 ?

NFSv4 est un protocole très différent de NFSv3. On peut avoir une certaine compatibilité en utilisant l'option `sec=sys` mais pour des cas simples il est souvent plus simple d'utiliser NFSv3. NFSv4 a un grand intérêt d'un point de vue sécurité en l'utilisant avec Kerberos (`sec=krb5`) : la sécurité ne repose plus sur les UID/GID pour identifier les utilisateurs à  l'origine des requêtes mais sur des tickets d'authentification Kerberos.

## Service `rpc-statd`

En cas d’une mise à niveau vers Debian 10 du serveur NFS, il est possible que le service `rpc-statd` soit désactivé au démarrage. Ce service peut être nécessaire pour que des clients se connectent en NFSv3. Il faudra activer et démarrer ce service si on a des erreurs de cette forme dans les logs (`/var/log/syslog`) sur le serveur :

~~~
Jan 01 12:34:56 server.host.name kernel: [ 1234.123456] lockd: cannot monitor client.host.name
~~~

Sur les clients, on aura des erreurs de cette forme :

~~~
Jan 01 12:34:56 client.host.name kernel: [ 1234.123456] lockd: server 10.1.2.3 not responding, still trying
~~~

# Plomberie

* NFSv3 : <https://tools.ietf.org/html/rfc1813>
* NFSv4 : <https://tools.ietf.org/html/rfc7530>

Sous Linux, le répertoire `/var/lib/nfs/` contient pas mal d'informations, notamment `/var/lib/nfs/etab` contient la liste des partages et `/var/lib/nfs/rmtab` la liste des clients NFS. Pour de la haute-disponibilité, on pourra partager ce répertoire entre plusieurs serveurs afin d'assurer une tolérance de panne entre les clients NFS sans nécessité de remonter les volumes.


# FAQ

## Serveur NFS sous Debian 6

Sous Debian Squeeze, il est recommandé de désactiver l'option `RPCMOUNTDOPTS` dans le fichier `/etc/default/nfs-kernel-server` sinon cela pose [des problèmes avec les groupes secondaires](http://bugs.debian.org/585085).

## NFS sous Debian 7

Sous Debian Wheezy, pour conserver les UID/GID il faut spécifier un domaine similaire sur le client et serveur dans `/etc/idmapd.conf`

## Serveur NFS sous Debian 7, client NFS sous Debian 6

Si le serveur NFS est sous Debian Wheezy et le client sous Debian Squeeze, il faudra mettre `NEED_IDMAPD=yes` dans `/etc/default/nfs-common` sur le client.

## Le client NFS ne monte par le répertoire au démarrage

Pour diverses raisons il peut y avoir un délai entre la configuration réseau et la disponibilité du serveur NFS au démarrage. On peut tester de mettre quelques dizaines de secondes d'attente dans la configuration fstab via le paramètre `x-systemd.device-timeout`, par exemple :

~~~
192.0.2.1:/home /home nfs defaults,_netdev,vers=3,x-systemd.device-timeout=42 0 0
~~~

