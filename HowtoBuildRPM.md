**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# How to create an RPM package

Inspiré de <http://fedoraproject.org/wiki/How_to_create_an_RPM_package>

Installation de tous les outils liés à la compilation.

~~~
# yum install @development-tools fedora-packager
~~~

Création d'un utilisateur spécifié, dédié à la création de paquet.

~~~
# /usr/sbin/useradd makerpm
# passwd makerpm
~~~

Une fois loggué avec cet utilisateur, mettre en place de l'environnement de création de paquets : 

~~~
$ rpmdev-setuptree
~~~

Arbo :


~~~
.
|-- BUILD
|-- RPMS
|-- SOURCES
|-- SPECS
`-- SRPMS

~~~


Fichier SPECS d'exemple :


~~~
Summary:            A program that ejects removable media using software control
Name:               eject
Version:            2.1.5
Release:            21%{?dist}
License:            GPLv2+
Group:              System Environment/Base
Source:             %{name}-%{version}.tar.gz
Patch1:             eject-2.1.1-verbose.patch
Patch2:             eject-timeout.patch
Patch3:             eject-2.1.5-opendevice.patch
Patch4:             eject-2.1.5-spaces.patch
Patch5:             eject-2.1.5-lock.patch
Patch6:             eject-2.1.5-umount.patch
URL:                <http://www.pobox.com/~tranter>
ExcludeArch:        s390 s390x
BuildRequires:      gettext
BuildRequires:      libtool

%description
The eject program allows the user to eject removable media (typically
CD-ROMs, floppy disks or Iomega Jaz or Zip disks) using software
control. Eject can also control some multi-disk CD changers and even
some devices' auto-eject features.

Install eject if you'd like to eject removable media using software
control.

%prep
%setup -q -n %{name}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1

%build
%configure
make %{?_smp_mflags}

%install
make DESTDIR=%{buildroot} install

install -m 755 -d %{buildroot}/%{_sbindir}
ln -s ../bin/eject %{buildroot}/%{_sbindir}

%find_lang %{name}

%files -f %{name}.lang
%doc README TODO COPYING ChangeLog
%{_bindir}/*
%{_sbindir}/*
%{_mandir}/man1/*

%changelog

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.5-21
- Rebuilt for <https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild>

* Fri Jul 02 2010 Kamil Dudka <kdudka@redhat.com> 2.1.5-20
- handle multi-partition devices with spaces in mount points properly (#608502)
~~~

On peut aussi générer un pseudo template en indiquant le nom du soft : 


~~~
cd ~/rpmbuild/SPECS
rpmdev-newspec python-antigravity
vi python-antigravity.spec
~~~

Une fois le fichier de spec créer, le tester :


~~~
rpmlint program.spec
~~~

Si tout est OK, copier le fichier source (souvent un tar.gz en upstream) dans ~/rpmbuild/SOURCES/

Enfin pour créer le paqet :


~~~
rpmbuild -ba program.spec
~~~


Exemple avec nfdump pour le projet nagios-rt : 


~~~
Name:           nfdump
Version:        1.6.6
Release:        1%{?dist}
Summary:        nfdump is a set of tools to collect and process netflow data

Group:          Applications/Internet
License:        BSD License
URL:            <http://sourceforge.net/projects/nfdump/>
Source0:        <http://downloads.sourceforge.net/project/nfdump/stable/nfdump-1.6.6/nfdump-1.6.6.tar.gz>
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  flex,bison,byacc,rrdtool-devel

%description
nfdump is a set of tools to collect and process netflow data. It's fast and has
a powerful filter pcap like syntax. It supports netflow versions v1, v5, v7 and
v9 as well as a limited set of sflow and is IPv6 compatible. IPFIX is supported
in beta state.  For CISCO ASA devices, which export Netflow Security Event
Loging (NSEL) records, please use nfdump-1.5.8-2-NSEL.


%prep
%setup -q


%build
./configure --prefix=/usr --enable-nfprofile
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%{_bindir}/*
%{_mandir}/*
%defattr(-,root,root,-)
%doc



%changelog
~~~


~~~
[makerpm@centos58 SPECS]$ rpmbuild -ba nfdump.spec 
Exécution_de(%prep): /bin/sh -e /var/tmp/rpm-tmp.19407
+ umask 022
+ cd /home/makerpm/rpmbuild/BUILD
+ LANG=C
+ export LANG
+ unset DISPLAY
+ cd /home/makerpm/rpmbuild/BUILD
+ rm -rf nfdump-1.6.6
+ /bin/gzip -dc /home/makerpm/rpmbuild/SOURCES/nfdump-1.6.6.tar.gz
+ tar -xf -
+ STATUS=0
+ '[' 0 -ne 0 ']'
+ cd nfdump-1.6.6
++ /usr/bin/id -u
+ '[' 501 = 0 ']'
++ /usr/bin/id -u
+ '[' 501 = 0 ']'
+ /bin/chmod -Rf a+rX,u+w,g-w,o-w .
+ exit 0
Exécution_de(%build): /bin/sh -e /var/tmp/rpm-tmp.19407
+ umask 022
+ cd /home/makerpm/rpmbuild/BUILD
+ cd nfdump-1.6.6
+ LANG=C
+ export LANG
+ unset DISPLAY
+ ./configure --prefix=/usr --enable-nfprofile
checking for a BSD-compatible install... /usr/bin/install -c
[...]
configure: creating ./config.status
config.status: creating Makefile
config.status: creating bin/Makefile
config.status: creating man/Makefile
config.status: creating config.h
config.status: executing depfiles commands

* Many thanks for using nfdump tools
* You may want to subscribe to the nfdump-discuss and/or
* nfsen-discuss mailing list:
* <http://lists.sourceforge.net/lists/listinfo/nfdump-discuss>
* <http://lists.sourceforge.net/lists/listinfo/nfsen-discuss>
* Please send bug reports back to me: phaag@users.sourceforge.net
* or to one of the lists.
+ make -j3
make  all-recursive
[...]
make[1]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6'
+ exit 0
Exécution_de(%install): /bin/sh -e /var/tmp/rpm-tmp.4807
+ umask 022
+ cd /home/makerpm/rpmbuild/BUILD
+ cd nfdump-1.6.6
+ LANG=C
+ export LANG
+ unset DISPLAY
+ rm -rf /var/tmp/nfdump-1.6.6-1-root-makerpm
+ make install DESTDIR=/var/tmp/nfdump-1.6.6-1-root-makerpm
Making install in .
make[1]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6'
make[2]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6'
make[2]: Nothing to be done for `install-exec-am'.
make[2]: Nothing to be done for `install-data-am'.
make[2]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6'
make[1]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6'
Making install in bin
make[1]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/bin'
make  install-am
make[2]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/bin'
make[3]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/bin'
test -z "/usr/bin" || /bin/mkdir -p "/var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin"
  /usr/bin/install -c nfcapd nfdump nfreplay nfexpire nfanon nfprofile '/var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin'
make[3]: Nothing to be done for `install-data-am'.
make[3]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/bin'
make[2]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/bin'
make[1]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/bin'
Making install in man
make[1]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/man'
make[2]: Entering directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/man'
make[2]: Nothing to be done for `install-exec-am'.
test -z "/usr/share/man/man1" || /bin/mkdir -p "/var/tmp/nfdump-1.6.6-1-root-makerpm/usr/share/man/man1"
 /usr/bin/install -c -m 644 ft2nfdump.1 nfcapd.1 nfdump.1 nfexpire.1 nfprofile.1 nfreplay.1 nfanon.1 sfcapd.1 '/var/tmp/nfdump-1.6.6-1-root-makerpm/usr/share/man/man1'
make[2]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/man'
make[1]: Leaving directory `/home/makerpm/rpmbuild/BUILD/nfdump-1.6.6/man'
+ /usr/lib/rpm/find-debuginfo.sh /home/makerpm/rpmbuild/BUILD/nfdump-1.6.6
extracting debug info from /var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin/nfanon
extracting debug info from /var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin/nfcapd
extracting debug info from /var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin/nfdump
extracting debug info from /var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin/nfprofile
extracting debug info from /var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin/nfexpire
extracting debug info from /var/tmp/nfdump-1.6.6-1-root-makerpm/usr/bin/nfreplay
2695 blocks
+ /usr/lib/rpm/check-rpaths /usr/lib/rpm/check-buildroot
+ /usr/lib/rpm/redhat/brp-compress
+ /usr/lib/rpm/redhat/brp-strip-static-archive /usr/bin/strip
+ /usr/lib/rpm/redhat/brp-strip-comment-note /usr/bin/strip /usr/bin/objdump
+ /usr/lib/rpm/brp-python-bytecompile
+ /usr/lib/rpm/redhat/brp-java-repack-jars
Traitement des fichiers: nfdump-1.6.6-1
Requires(rpmlib): rpmlib(CompressedFileNames) <= 3.0.4-1 rpmlib(PayloadFilesHavePrefix) <= 4.0-1
Requires: libc.so.6()(64bit) libc.so.6(GLIBC_2.2.5)(64bit) libc.so.6(GLIBC_2.3)(64bit) libresolv.so.2()(64bit) librrd.so.4()(64bit) rtld(GNU_HASH)
Traitement des fichiers: nfdump-debuginfo-1.6.6-1
Requires(rpmlib): rpmlib(CompressedFileNames) <= 3.0.4-1 rpmlib(PayloadFilesHavePrefix) <= 4.0-1
Vérification des fichiers non empaquetés: /usr/lib/rpm/check-files /var/tmp/nfdump-1.6.6-1-root-makerpm
Ecrit: /home/makerpm/rpmbuild/SRPMS/nfdump-1.6.6-1.src.rpm
Ecrit: /home/makerpm/rpmbuild/RPMS/x86_64/nfdump-1.6.6-1.x86_64.rpm
Ecrit: /home/makerpm/rpmbuild/RPMS/x86_64/nfdump-debuginfo-1.6.6-1.x86_64.rpm
Exécution_de(%clean): /bin/sh -e /var/tmp/rpm-tmp.76791
+ umask 022
+ cd /home/makerpm/rpmbuild/BUILD
+ cd nfdump-1.6.6
+ rm -rf /var/tmp/nfdump-1.6.6-1-root-makerpm
+ exit 0
[makerpm@centos58 SPECS]$ 

~~~


