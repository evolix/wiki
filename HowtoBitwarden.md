---
categories: utilities web
title: Howto Bitwarden : installation et utilisation courante.
---

* Documentation Bitwarden : <https://help.bitwarden.com/article/install-on-premise/>

## Installation

Sous Debian 9, nous installons [HowtoDocker](Docker), [HowtoDockerCompose](Docker-compose) puis bitwarden.

~~~
# adduser --disabled-password bitwarden
# adduser bitwarden docker
# sudo -iu bitwarden
$ curl -s -o bitwarden.sh \
  https://raw.githubusercontent.com/bitwarden/core/master/scripts/bitwarden.sh
$ chmod u+x bitwarden.sh
$ ./bitwarden.sh install
# chown -R bitwarden:docker /home/bitwarden/bwdata
~~~

Il suffira de répondre aux questions de l'assistant puis d'éditer `/home/bitwarden/bwdata/env/global.override.env`.  
On conseille de le mettre derrière HAProxy et de le faire écouter sur les port 8090 et 4443 car l'image docker utiliser un userland proxy pouvant entrer en conflit avec les ports de l'hôte.

Pour le démarrer : `./bitwarden.sh start`, cela va déployer les containers nécessaires et les démarrer.

On pourra éditer les fichier de configuration de base dans `bwdata/`. Par exemple pour activer seulement le HTTP sur le vhost nginx, on éditera `bwdata/nginx/default.conf` et on fait un `./bitwarden.sh restart`.

## upgrade

<https://help.bitwarden.com/article/updating-on-premise/>

## FAQ

### erreur "host not found in upstream web"

En cas d'erreur :

~~~
nginx: [emerg] host not found in upstream "web" in /etc/nginx/conf.d/default.conf:30
~~~

redémarrer `./bitwarden.sh restart`