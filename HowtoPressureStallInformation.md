---
categories: psi pressure stall information load
title: Pressure Stall Information (PSI)
...

À partir du noyau Linux 4.20, de nouvelles métriques de pression sur le CPU, les I/O et la mémoire sont disponibles dans `/proc/pressure/`.

Elles sont donc disponibles à partir de Debian 11 (Bullseye).

Documentation du noyau : <https://docs.kernel.org/accounting/psi.html>


## Explications

Pour chaque type de ressource (CPU, I/O ou mémoire), le noyau indique trois moyennes `avgXX` en pourcentage, ainsi qu'une valeur absolue `total` en microsecondes (1 sec = 1 000 000 microsecs).

~~~
some avg10=0.00 avg60=0.00 avg300=0.00 total=0
~~~

Si certains processus actifs ont attendu une ressource, les moyennes `avgXX` de la ligne `some` représentent le pourcentage de temps pendant lequel il y a eu de l'attente, sur une fenêtre de temps glissante (10 secs, 1 min ou 5 mins).

La valeur absolue `total` indique la somme du temps d'attente actuel de chaque processus. Elle est surtout utilisée pour du [monitoring](#nagios), en la soustrayant avec sa valeur à un moment antérieur, ce qui permet de savoir combien de temps des processus ont attendu sur une fenêtre de temps donnée.

~~~
full avg10=0.00 avg60=0.00 avg300=0.00 total=0
~~~

C'est le même principe pour les valeurs de la ligne `full`, sauf que le temps compté n'est pas quand *certains* processus attendent, mais quand *tous* les processus *actifs* attendent une ressource. Elles sont donc inférieures ou égales à celles de la ligne `some`.

Attention, les processus non-actifs ne sont pas pris en compte dans le calcul de `full`. Ainsi, même si on a une ressource lente et une autre rapide, les pressions `some` et `full` peuvent être très proches, par exemple dans le cas où les processus ayant besoin de la ressource rapide ont été satisfaits et sont repassés idle.

Les valeurs `full` ne sont pas pertinentes pour le CPU, car tous les processus actifs ne peuvent pas être en attente du CPU, car il est forcément actif sur certains d'entre eux.


### CPU

Par exemple :

~~~
$ cat /proc/pressure/cpu 
some avg10=10.19 avg60=11.69 avg300=12.32 total=10917307090
~~~


### I/O et mémoire

Par exemple :

~~~
$ cat /proc/pressure/io
some avg10=1.13 avg60=1.77 avg300=2.23 total=2690169666
full avg10=1.03 avg60=1.24 avg300=1.68 total=2268981698
~~~


### Rapport avec le load

Le load indique le nombre moyen de processus en attente sur 1 min, 5 min et 15 min.

Il n'indique pas pendant combien de temps des processus ont attendu, ni quel est le pourcentage du temps pendant lequel il y a de l'attente.

Contrairement au load, la pression n'indique par quand un serveur est très actif, mais seulement quand les ressources se font rares. 

Mais un serveur peut très bien avoir du load (être très actif donc), sans pour autant qu'il y ait de pression. C'est un cas d'utilisation optimale des ressources.


## Munin

A partir de Debian 11, on peut utiliser le plugin Munin [linux_psi](https://github.com/munin-monitoring/contrib/blob/master/plugins/system/linux_psi) pour grapher ces valeurs :

~~~
wget -O /usr/share/munin/plugins/linux_psi https://raw.githubusercontent.com/munin-monitoring/contrib/master/plugins/system/linux_psi
chmod 755 /usr/share/munin/plugins/linux_psi
ln -s /usr/share/munin/plugins/linux_psi /etc/munin/plugins/linux_psi
systemctl restart munin-node
~~~

Le graphe principal (sur la page d'index) est notamment utile lors d'une augmentation du load, pour identifier rapidement quelles ressources les processus attendent (CPU, I/O disque…).


## Nagios

A partir de Debian 12, le paquet `monitoring-plugins-contrib` fournit l'utilitaire `check_pressure`.

Le check surveille le temps d'attente des processus sur une seconde (obtenu en faisant la différence entre le temps total d'attente donné par `/proc/pressure/` entre t et t+1 sec).

Par exemple, pour surveiller la pression CPU à partir des seuils de 0,1 secs (warning) et 0,5 secs (critique) :

~~~
/usr/lib/nagios/plugins/check_pressure --cpu -w 100000 -c 500000
~~~

Pour les I/O et la mémoire, on peut utiliser l'option `--full`, utilisera les valeurs `full` au lieu de `some` (le check sera alors **moins** sensible) :

~~~
/usr/lib/nagios/plugins/check_pressure --io --full -w 500000 -c 1000000
/usr/lib/nagios/plugins/check_pressure --mem --full -w 500000 -c 1000000
~~~




