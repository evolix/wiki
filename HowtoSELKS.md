---
title: SELKS
---

* Documentation: <https://github.com/StamusNetworks/SELKS/wiki>

SELKS est une plateforme IDS/IPS basée sur Debian et constituée de Suricata, [Elasticsearch, Logstash, Kibana](HowtoELK) et Scirius.

## Interface web

L'interface web, gérée par Scirius, est protégée par mot de passe.

Une fois connecté il y a le choix entre plusieurs vues présentées ci-dessous.

### Administration

Il s'agit de Scirius en vue administrative. Elle offre un dashboard qui permet d'avoir la liste des 20 alertes les plus communes dans un laps de temps donné (1h, 6h, 24h, 2 jours, 7 jours ou 30 jours). Cette vue permet de modifier les règles que Suricata utilise pour détecter de potentiels problèmes et de gérer l'administration de l'ensemble de SELKS.

- Un clic sur le logo ouvre un menu permettant l'accès à des options d'administration et de gestion de SELKS. Ces options sont:
    - `System settings`: Administration de SELKS, permet d'utiliser un autre serveur elasticsearch, de supprimer toutes les donnés d'ES et de réinitialiser Kibana,
    - `Action history`: Il s'agit d'un historique global des actions prises dans Scirius,
    - `Manage accounts`: Gestion des comptes utilisateur (détails dans l'aide en ligne)
        - `Staff Status`: peut gérer les alertes
        - `Superuser Status`: peut gérer les utilisateur (+ tous les droits)
        - `Active`: peut se connecter
- `Sources`: Permet de contrôler d'où viennent les alertes et de modifier des catégories entières.
- `Rulesets`: Permet de contrôler les ruleset (groupes de sources), il n'y en a qu'un à l'heure actuelle: `Default Rulesets`
- `Suricata`: Voir le status détaillé de Suricata, la mémoire utilisée pour décoder les paquets, les "problèmes" de décodage de paquets (paquet décodé non valide, trou dans une session tcp, ...), le nombre de paquets capturés et perdus par le kernel, et une vision très rapide de l'activité des règles Suricata (nombre global d'alertes, catégories générales des règles les plus actives et règles les plus actives).

Pour modifier une règle suricata, il faut cliquer sur son Sid. Nous avons ensuite deux possibilités :

1. Cliquer sur l'une des actions dans la boite "Action" à gauche (disable, enable ou edit rule). Depuis l'un de ces menus, nous avons accès à de nouveaux menus : transform, disable, enable, threshold, ou suppress rule.
    1. `Transform rule` permet de définir une action que SELKS doit effectuer lorsqu'une règle est déclenchée (par exemple, bloquer un paquet). C'est une fonction utilisé en mode IPS.
    2. `Disable rule` permet de désactiver totalement la règle.
    3. `Enable rule` permet de réactiver totalement la régle précédemment désactivée.
    4. `Threshold rule` permet de modifier le seuil d'alerte de la règle : le nombre de fois où la règle doit être déclenchée en une période de temps donnée avant d'afficher une alerte.
    5. `Suppress rule` permet de ne plus avoir d'alerte concernant une IP ou un préfixe IP, en choisissant par source ou destination.
2. Ou, à droite dans les tableaux "Source IP" et "Destination IP", puis colonne "Actions", cliquer sur la flèche vers le bas ou sur la croix pour une IP spécifique.
    1. La flèche vers le bas permet de modifier le seuil d'alerte de la règle : le nombre de fois où la règle doit être déclenchée en une période de temps donnée avant d'afficher une alerte.
    2. La croix permet de ne plus avoir d'alerte concernant cette IP source ou destination.

### Hunt

Il s'agit de Scirius en vue d'utilisation, permettant d'avoir une vision d'ensemble des alertes et d'obtenir rapidement des informations sur une alerte précise ou une IP.

- `Dashboard`: Vue par défaut, infos rassemblées sur toutes les alertes dans un laps de temps défini
- `Signatures`: Liste des signatures triées selon, au choix: Created (date de création de la règle dans sa source), Alerts (nombre total d'alertes correspondant à la signature dans ce laps de temps), Message (nom de la signature), Updated (dernière fois que la règle a été modifiée)
- `Alerts`: Liste des dernières alertes correspondants aux filtres, il est possible de cliquer dessus pour obtenir des informations sur cette alerte (IPs, payload, ...)
- `Action`: Cet onglet montre les actions prises par Suricata lorsqu'il est utilisé en tant qu'IPS. Il n'est donc pas utile lorsque SELKS est configuré en tant qu'IDS.

> Il est possible de filtrer les alertes représentées en cliquant sur les loupes dans les différents onglets (il peut être nécessaire de survoler les informations pour que la loupe apparaisse).

### Dashboard

Il s'agit de Kibana, les seules parties vraiment utiles sont "Dashboard" et potentiellement "Discovery", il permet aussi de gérer Elasticsearch. Pour visualisation rapide des alertes, il y a un Dashboard dans "Dashboard" nommé "SN-ALERT" ; les alertes peuvent ensuite être triées en cliquant sur ce qu'on veut (sauf la carte).

> Le champ "FPC" dans `SN-ALERT-EventsList` est lié à Moloch.

### Event Viewer

Ouvre EveBox, permet d'obtenir les paquets correspondants à une alerte (d'après Suricata) sans passer par Moloch, un peu moins long.

> Une alerte dans Suricata est produite lorsqu'une règle Suricata est atteinte par un paquet (ou un ensemble de paquets).

## FAQ

Pour plus de pistes pour résoudre des problèmes, voir <https://github.com/StamusNetworks/SELKS/wiki#troubleshooting-and-getting-help>.

La commande `selks-health-check_stamus` peut être lancée pour voir l'état de santé de tous les services nécessaires au bon fonctionnement de SELKS.

### Réinitialiser Kibana

Parfois Kibana est bloqué dans son démarrage, dans ce cas là, il faut le réinitialiser.

Dans l'interface web, il faut aller sur <https://ip.du.serveur.selks/rules/settings/> > Kibana > Reset SN dashboards.

Ou par SSH, il faut faire en tant que root:

```
cd /usr/share/python/scirius/ && . bin/activate && python bin/manage.py kibana_reset && deactivate
```

Voire, ci cela ne suffit pas:

```
curl -XDELETE 'http://localhost:9200/.kibana*/' && \
sleep 20 ; cd /usr/share/python/scirius/ && \
. bin/activate && python bin/manage.py kibana_reset && \
deactivate
```

### Des indices sont passés en read-only

Voir <https://www.elastic.co/guide/en/elasticsearch/reference/6.5/disk-allocator.html>

Une fois un certain pourcentage de disque utilisé, Elasticsearch passera certains indices en read-only afin d'éviter de saturer le disque.

Pour repasser tous les indices en read-write, il faut aller sur la machine SELKS et utiliser la commande suivante:

```
curl -XPUT "http://localhost:9200/_all/_settings" -H 'Content-Type: application/json' -d'
{
  "index.blocks.read_only_allow_delete": null
}'
```
