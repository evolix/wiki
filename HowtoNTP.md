---
categories: system
title: Howto NTP
...

* Documentation : <http://support.ntp.org/bin/view/Main/DocumentationIndex>
* Rôle Ansible : <https://forge.evolix.org/projects/ansible-roles/repository/revisions/stable/show/ntpd>

Le protocole [NTP (Network Time Protocol)](https://fr.wikipedia.org/wiki/Network_Time_Protocol) permet de maintenir à l'heure un système d'exploitation : il s'agit d'éviter les déviations d'heure (et **pas** de gérer les changements de fuseaux horaires ou les décalages d'heure été/hiver qui sont gérés directement par le système).
On utilise en général le protocole NTP version 4 (NTPv4) publié en juin 2010. Sur des petits équipements, on utilise parfois une version simplifiée de NTP appelée [SNTP (Simple Network Time Protocol)](https://tools.ietf.org/html/rfc4330).

Sur un serveur, nous recommandons d'être synchronisé avec les serveurs de temps de référence [pool.ntp.org](http://www.pool.ntp.org/).

A partir de Debian 12, le programme NTP dans Debian est remplacé par NTPsec, une version dérivée et plus sécurisée du programme NTP original. 

## Installation

### Debian 11 et avant (NTP)

Il existe des clients NTP en ligne de commande comme _ntpdate_ mais sur un
serveur nous conseillons d'utiliser un démon NTP qui maintiendra le système
synchronisé en permanence avec un temps de référence :

~~~
# apt install ntp

# ntpd --help | head -1
ntpd - NTP daemon program - Ver. 4.2.8p10
~~~

### Debian 12 et suivant (NTPsec)

~~~
# apt install ntpsec 

# ntpd --version
ntpd ntpsec-1.2.2
~~~

## Configuration de NTP

### Configuration en tant que client NTP 

L'objectif est de maintenir le serveur synchronisé en permanence avec des serveurs NTP de référence.

La configuration se fait via le fichier `/etc/ntp.conf` (pour ntp) ou `/etc/ntpsec/ntp.conf` pour ntpsec

~~~

# Le ou les serveurs sur lesquels on veut se synchroniser
server ntp.evolix.net iburst

# Si on souhaite utiliser les serveurs du pool ntp.org
pool 0.pool.ntp.org iburst
pool 1.pool.ntp.org iburst
pool 2.pool.ntp.org iburst
pool 3.pool.ntp.org iburst

# Cette ligne sert à autoriser le contrôle TOTAL de l'horloge pour la machine locale ce qui est le but recherché. 
# On autorise toute requête de synchronisation DEPUIS notre serveur
restrict 127.0.0.1
restrict ::1
restrict source nomodify noquery notrap

# On interdit toute requête de synchronisation VERS notre serveur
restrict -4 default ignore
restrict -6 default ignore
~~~

> Note: Pour Debian Jessie, la directive `restrict source nomodify noquery notrap` n'étant pas supportée avec la version distribuée de ntp, cette configuration ne fonctionnera pas car le client va rejeter la réponse du serveur à cause du `restrict -4 default ignore`.
> Il est éventuellement possible de contourner cela avec la directive suivante `restrict ntp.example.net nomodify notrap nopeer noquery`


On peut vérifier le bon fonctionnement via la commande :

~~~
# ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 ntp.example.com 213.251.153.35   3 u   50   64    3   19.404  -10.832   8.768
~~~


### Configuration en tant que serveur NTP

Les serveurs NTP sont organisés en strates, se synchronisant entre eux et avec la strate supérieure.
La strate 1 est la plus haute, elle regroupe les machines reliées directement à des horloges atomiques ou récepteurs GPS/grandes ondes.

Pour un serveur sur lequel viendrait se synchroniser d'autres machines, on se synchronise avec plusieurs serveurs de strate 2, pour la France on peut par exemple les choisir via <https://services.renater.fr/ntp/serveurs_francais>.

On autorise aussi l'accès en lecture depuis d'autres machines.

Voici le fichier `/etc/ntp.conf` correspondant :

~~~
# Si plusieurs IPs sur le serveur, écouter sur 192.0.2.1 uniquement
interface listen 192.0.2.1

# Plusieurs serveurs (de strate 2 a priori)
# Voir http://support.ntp.org/bin/view/Servers/StratumTwoTimeServers
server ntp1.example.com
server ntp2.example.com
server ntp3.example.com
server ntp4.example.com

# Autoriser l'accès en lecture uniquement depuis partout
restrict -4 default kod notrap nomodify nopeer noquery
restrict -6 default kod notrap nomodify nopeer noquery

# On autorise toujours notre propre machine à modifier son horloge
restrict 127.0.0.1
restrict ::1
~~~

On peut voir la liste des clients qui se connectent au serveur via :

~~~
# ntpq -c mrulist
lstint avgint rstr r m v  count rport remote address
==============================================================================
     2     27  1d0 . 3 4   6404 41342 server1.example.com
     2     27  1d0 . 3 4   6412 34764 server2.example.com
    10     25  1d0 . 3 4   6970 44168 server3.example.com
    16     70  1d0 . 3 4   2495 58051 server4.example.com
    30     66  1d0 . 3 4   2640   123 server5.example.com
~~~


## NTP sous OpenBSD

Sous OpenBSD on a le démon très léger [OpenNTPD](http://www.openntpd.org/).
Il est installé et lancé par défaut avec une configuration minimale via le fichier `/etc/ntpd.conf` :

~~~
servers pool.ntp.org
#constraints from "https://www.google.com/"
~~~

Le démon d'OpenBSD a une fonctionnalité supplémentaire : il sait demander la date à un serveur https via TLS. Cela ne permet pas d'améliorer la précision mais rajoute une contrainte sur la date synchronisée et réduit ainsi la possibilité d'une attaque man in the middle (MITM) sur le protocole NTP lui-même. Les paquets NTP n'étant pas en accord avec la contrainte sont ignorés et le serveur NTP dont ils proviennent est marqué comme invalide. Néanmoins on préfère éviter de signifier à google la présence d'une machine OpenBSD et donc on commente cette directive.

À noter que par défaut, la synchronisation NTP est lente, il faudra plusieurs minutes voire heures
si votre horloge a un fort décalage. 

Pour forcer une synchronisation immédiate :

~~~
# rdate -cv ntp.evolix.net
~~~

On conseille de l'activer au démarrage :

~~~
# rcctl set ntpd flags "-s"
~~~

## Outils

### ntpdate

L'outil _ntpdate_ (package à installer) permet de lancer une synchronisation en ligne de commande. C'est notamment utile en mode debug :

~~~
# ntpdate ntp.evolix.net
31 May 12:24:07 ntpdate[29352]: adjust time server 31.170.8.123 offset 0.000484 sec

# ntpdate -d ntp.evolix.net
31 May 12:37:40 ntpdate[29382]: ntpdate 4.2.8p10@1.3728-o Sun May  7 22:26:53 UTC 2017 (1)
transmit(31.170.8.123)
receive(31.170.8.123)
transmit(31.170.8.123)
receive(31.170.8.123)
transmit(31.170.8.123)
receive(31.170.8.123)
transmit(31.170.8.123)
receive(31.170.8.123)
server 31.170.8.123, port 123
stratum 2, precision -22, leap 00, trust 000
refid [31.170.8.123], delay 0.02649, dispersion 0.00002
transmitted 4, in filter 4
reference time:    dcd9190c.fda602f8  Wed, May 31 2017 12:27:24.990
originate timestamp: dcd91b7a.34519aeb  Wed, May 31 2017 12:37:46.204
transmit timestamp:  dcd91b7a.342bf15c  Wed, May 31 2017 12:37:46.203
filter delay:  0.02669  0.02670  0.02649  0.02676 
         0.00000  0.00000  0.00000  0.00000 
filter offset: 0.000082 0.000017 0.000060 -0.00002
         0.000000 0.000000 0.000000 0.000000
delay 0.02649, dispersion 0.00002
offset 0.000060

31 May 12:37:46 ntpdate[29382]: adjust time server 31.170.8.123 offset 0.000060 sec
~~~

Pour forcer synchro et heure ntp:

~~~{}
# /etc/init.d/ntp stop
# ntpdate ntp.evolix.net
# /etc/init.d/ntp start
~~~


### ntpstat

L'outil _ntpstat_ (package à installer) permet vérifier le bon fonctionnement d'un démon NTP lancé en local :

~~~
$ ntpstat 
unsynchronised
   polling server every 8 s
 
$ ntpstat 
synchronised to NTP server (163.172.177.158) at stratum 4 
   time correct to within 984 ms
   polling server every 64 s
~~~

### timedatectl

La commande _timedatectl_ (qui fait partie de [systemd](HowtoSystemd)) donne une vue d'ensemble des paramètres d'horloge d'un système :

~~~
$ timedatectl
      Local time: Wed 2017-05-31 12:53:11 CEST
  Universal time: Wed 2017-05-31 10:53:11 UTC
        RTC time: Wed 2017-05-31 10:53:10
       Time zone: Europe/Paris (CEST, +0200)
     NTP enabled: no
NTP synchronized: yes
 RTC in local TZ: no
      DST active: yes
 Last DST change: DST began at
                  Sun 2017-03-26 01:59:59 CET
                  Sun 2017-03-26 03:00:00 CEST
 Next DST change: DST ends (the clock jumps one hour backwards) at
                  Sun 2017-10-29 02:59:59 CEST
                  Sun 2017-10-29 02:00:00 CET
~~~

On peut également activer la synchronisation NTP directement avec [systemd](HowtoSystemd). Il faut s'assurer d'avoir installé le paquet debian `systemd-timesyncd`, avant de le configurer via le fichier `/etc/systemd/timesyncd.conf` puis `timedatectl set-ntp true`.

Il est possible de surcharger la configuration par défaut dans d’autres fichiers.

~~~
$ cat /etc/systemd/timesyncd.conf.d/example.conf
[Time]
NTP=ntp.example.net
~~~

### `nmap --script ntp-info`

Si on veut s'assurer qu’on peut obtenir l’heure à partir d’un serveur NTP, on peut utiliser le script `ntp-info` de `nmap` : 

~~~(sh)
# nmap -sU -p 123 --script ntp-info pool.ntp.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-06-07 15:26 CEST
Nmap scan report for pool.ntp.org (82.65.248.56)
Host is up (0.015s latency).
Other addresses for pool.ntp.org (not scanned): 91.194.60.128 5.39.80.51 95.81.173.74
rDNS record for 82.65.248.56: 82-65-248-56.subs.proxad.net

PORT    STATE SERVICE
123/udp open  ntp
| ntp-info:
|_  receive time stamp: 2024-06-07T13:26:56

Nmap done: 1 IP address (1 host up) scanned in 10.34 seconds
~~~

## Munin

Pour les serveurs utilisant un serveur NTP uniquement pour leurs besoins, Munin
intègre par défaut des scripts pour surveiller le décalage par rapport à un/des serveur(s) de
temps. Il ne reste qu'à les activer.

Pour les serveurs NTP, des plugins non officiels existent : _ntp_queries_ etc.

## FAQ

### ntpdate: the NTP socket is in use, exiting

Si vous avez une erreur du type :

~~~
# ntpdate ntp.evolix.net
31 May 12:19:23 ntpdate[29290]: the NTP socket is in use, exiting
~~~

C'est que votre socket réseau udp/123 est occupée par un autre programme, probablement un démon NTP !

### Horloge hardware

Une machine intègre une horloge machine, pas forcément synchronisée avec l'horloge système.

Pour accéder à l'horloge hardware :

~~~
# hwclock --show
Wed 31 May 2017 12:59:18 PM CEST  -0.213258 seconds
~~~

Pour forcer l'horloge hardware à prendre la même valeur que l'horloge système :

~~~
# hwclock --systohc
~~~
