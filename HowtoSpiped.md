---
title: Howto Spiped
categories: tunneling
...

* Documentation : <http://www.tarsnap.com/spiped.html>

Spiped est un utilitaire pour créer des tunnels chiffrés et authentifiés.

# Installation

~~~
# apt install spiped
~~~

# Utilisation 

## En mode écoute/serveur

Pour exposer un service foo écoutant en local sur le port 1337 via une connexion chiffrée par la clé /etc/spiped/foo.key sur le port 33306:

~~~
# mkdir /etc/spiped
# dd if=/dev/urandom of=/etc/spiped/foo.key bs=32 count=1

# cat /etc/systemd/system/spiped-foo.service
[Unit]
Description=Spiped receive for FOO
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/usr/bin/spiped -F -d -s 0.0.0.0:33306 -t [127.0.0.1]:3306 -k /etc/spiped/foo.key

[Install]
WantedBy=multi-user.target

# systemctl daemon-reload
~~~

On autorisera l'ouverture du port 33306 pour les IPs autorisées à y accéder.

## En mode client

On importe la clé /etc/spiped/foo.key sur le client où le nouveau service écoutera sur le port 33306 en local:

~~~
# mkdir /etc/spiped
# vim /etc/spiped/foo.key

# cat /etc/systemd/system/spiped-foo.service
[Unit]
Description=Spiped receive for FOO
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/usr/bin/spiped -F -e -s 127.0.0.1:33306 -t [$IP_SERVEUR_DISTANT]:33306 -k /etc/spiped/foo.key

[Install]
WantedBy=multi-user.target

# systemctl daemon-reload
~~~