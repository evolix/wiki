---
title: Howto MTR
categories: tips network
...

* Manpage <https://manpages.debian.org/stable/mtr-tiny/mtr.8.en.html>
* Code source : <https://github.com/traviscross/mtr>

[MTR (My traceroute)](http://www.bitwizard.nl/mtr/) est un outil de diagnostic réseau combinant les fonctionnalités des programmes _traceroute_ et _ping_.

## Principe

Pour décrire l'itinéraire emprunté, MTR utilise les paquets ICMP _Time Exceeded_ renvoyés par les routeurs intermédiaires et les paquets ICMP _Echo Reply_ renvoyés par l'hôte de destination. Les paquets ICMP envoyés ont des TTLs augmentant progressivement jusqu'à la destination. Le TTL (Time To Live) contrôle le nombre de sauts qu'un paquet fera avant d'expirer. De cette façon après un saut (TTL=1), le premier routeur décrémentera le TTL de 1, le passant à 0. Ainsi le paquet expirera et un paquet ICMP _Time Exceeded_ sera transmis à l'émetteur contenant l'IP du routeur. S'en suivra un paquet avec un TTL à deux, puis trois, et ainsi de suite jusqu'à ce que l'hôte de destination renvoie un paquet ICMP _Echo Reply_. Parallèlement, MTR collecte des informations supplémentaires tel que le temps d'aller-retour et la perte de paquets éventuelle pour chacun des équipements traversés jusqu'à la destination. Cela en fait un outil idéal pour diagnostiquer des problèmes réseau.

## Installation

Sous Debian

~~~
# apt install mtr
~~~

> *Note* : pour installer la version sans couche graphique, utiliser le paquet **mtr-tiny**

Sous OpenBSD :

~~~
# pkg_add mtr
~~~

## Utilisation de base

Utiliser l'interface "ncurses" (`-t` ou `--ncurses`) :

~~~
$ mtr example.com --curses
~~~

Sans résolution DNS (`-n` ou `--no-dns`) :

~~~
$ mtr example.com --curses -n
~~~

> *Note* : on peut désactiver/activer la résolution DNS en live en appuyant sur la touche `n`

Lancer en mode rapport (`-r` ou `--report`) :

~~~
$ mtr example.com -r
~~~

Modifier l'intervalle d'envoi (défaut 1 seconde) :

~~~
# mtr example.com --curses -i 0.1
~~~

On peut également lancer MTR en mode TCP sur un port donné :

~~~
# mtr example.com --curses --tcp -P 80
~~~

Autres options utiles :

* `-u` passe en UDP
* `-w` (--report-wide) remplace `-r` en fournissant les hostnames complets
* `-4` force en IPv4
* `-6` force en IPv6

~~~
Start: Fri Mar 31 14:04:36 2017
HOST: foo.example.net            Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- routeur1.example.net      0.0%    10    0.9   0.8   0.5   1.0   0.0
  2.|-- routeur2.example.net      0.0%    10    1.0   1.1   0.8   1.4   0.0
  3.|-- routeur3.example.net      0.0%    10    2.4   9.4   1.8  21.2   7.3
  4.|-- routeur4.example.net      0.0%    10   11.6  25.9  11.1 139.2  40.2
  5.|-- routeur5.example.net      0.0%    10    5.4   7.3   5.3  21.5   5.0
  6.|-- routeur6.example.net      0.0%    10   12.0  11.8  11.4  12.1   0.0
  7.|-- routeur7.example.net      0.0%    10   11.5  11.7  11.4  12.1   0.0
  8.|-- routeur8.example.net      0.0%    10   15.4  15.8  11.7  19.7   2.6
  9.|-- routeur9.example.net      0.0%    10   12.2  12.4  11.9  12.9   0.0
 10.|-- routeur10.example.net     0.0%    10   12.0  12.2  11.8  13.6   0.3
 11.|-- example.com               0.0%    10   11.7  13.0  11.7  23.1   3.5
                                    ↑      ↑     ↑     ↑     ↑     ↑     ↑
                                    |      |     |     |     |     |     |
                                    |      |     |     |     |     |      −− Écart type des latences : différence entre les mesures de la
                                    |      |     |     |     |     |         latence.
                                    |      |     |     |     |     |
                                    |      |     |     |     |      −− Moins bonne latence
                                    |      |     |     |     |    
                                    |      |     |     |      −− Meilleur latence
                                    |      |     |     |
                                    |      |     |      −− Latence moyenne de tous les paquets envoyés
                                    |      |     |  
                                    |      |      −− Latence du dernier paquet envoyé
                                    |      |
                                    |       −− Nombre de paquets envoyés
                                    |
                                     −− Pourcentage de perte de paquets à chaque saut
~~~

## Diagnostics réseau

Dans cette section nous allons voir comment interpréter et tirer des conclusions en fonction des données fournies par MTR.

### La perte de paquets

Lorsque l'on souhaite faire une analyse avec MTR, on observe deux paramètres importants, la perte de paquets et la latence. Le première chose à regarder concerne la perte de paquets. Une perte observée sur le chemin n'est pas toujours pertinente. Une pratique commune consiste à faire du rate limiting sur l'ICMP. Vous pouvez alors observer une perte à un saut particulier. Il est facile de déterminer si la perte est à considérer ou non. Si les sauts suivants rapportent 0.0% de perte alors il s'agit de rate limiting ICMP, pas d'inquétude à avoir.

Voici un exemple :

~~~
Start: Sat Apr  1 19:52:13 2017
HOST: foo.example.com         Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- routeur1.example.net   0.0%    10    0.8   0.8   0.7   1.0   0.0
  2.|-- routeur2.example.net   0.0%    10   25.2  26.2  24.7  28.2   1.1
  3.|-- routeur3.example.net   0.0%    10   25.6  26.5  25.6  27.4   0.0
  4.|-- routeur4.example.net  70.0%    10   38.2  39.3  36.8  42.6   2.0
  5.|-- routeur5.example.net   0.0%    10   36.3  36.4  35.9  37.3   0.0
  6.|-- routeur6.example.net   0.0%    10   38.8  42.0  37.4  77.9  12.6
  7.|-- routeur7.example.net   0.0%    10   36.7  36.8  36.2  38.7   0.7
  8.|-- routeur8.example.net   0.0%    10   36.1  36.6  35.4  38.1   1.2
  9.|-- routeur9.example.net   0.0%    10   36.1  36.1  36.0  36.1   0.0
 10.|-- routeur10.example.net  0.0%    10   37.2  35.9  35.2  37.2   0.5
~~~
On peut voir ici que la perte ne concerne que le quatrième saut et nous avons bien 0% de perte à la destination. On est bien ici dans un cas de rate limiting ICMP.

Voici un exemple de perte à considérer :

~~~
Start: Sat Apr  1 19:52:13 2017
HOST: foo.example.com         Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- routeur1.example.net   0.0%    10    0.8   0.8   0.7   1.0   0.0
  2.|-- routeur2.example.net   0.0%    10   25.2  26.2  24.7  28.2   1.1
  3.|-- routeur3.example.net   0.0%    10   25.6  26.5  25.6  27.4   0.0
  4.|-- routeur4.example.net   0.0%    10   38.2  39.3  36.8  42.6   2.0
  5.|-- routeur5.example.net   0.0%    10   36.3  36.4  35.9  37.3   0.0
  6.|-- routeur6.example.net  40.0%    10   38.8  42.0  37.4  77.9  12.6
  7.|-- routeur7.example.net  40.0%    10   36.7  36.8  36.2  38.7   0.7
  8.|-- routeur8.example.net  40.0%    10   36.1  36.6  35.4  38.1   1.2
  9.|-- routeur9.example.net  40.0%    10   36.1  36.1  36.0  36.1   0.0
 10.|-- routeur10.example.net 40.0%    10   37.2  35.9  35.2  37.2   0.5
~~~

Si la perte rapportée se poursuit jusqu'à la destination alors la perte est probablement réelle.

On peut aussi observer des cas où rate limiting et perte réelle sont présents :

~~~
Start: Sat Apr  1 19:52:13 2017
HOST: foo.example.com         Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- routeur1.example.net   0.0%    10    0.8   0.8   0.7   1.0   0.0
  2.|-- routeur2.example.net   0.0%    10   25.2  26.2  24.7  28.2   1.1
  3.|-- routeur3.example.net   0.0%    10   25.6  26.5  25.6  27.4   0.0
  4.|-- routeur4.example.net   0.0%    10   38.2  39.3  36.8  42.6   2.0
  5.|-- routeur5.example.net  70.0%    10   36.3  36.4  35.9  37.3   0.0
  6.|-- routeur6.example.net  40.0%    10   38.8  42.0  37.4  77.9  12.6
  7.|-- routeur7.example.net  40.0%    10   36.7  36.8  36.2  38.7   0.7
  8.|-- routeur8.example.net  40.0%    10   36.1  36.6  35.4  38.1   1.2
  9.|-- routeur9.example.net  40.0%    10   36.1  36.1  36.0  36.1   0.0
 10.|-- routeur10.example.net 40.0%    10   37.2  35.9  35.2  37.2   0.5
~~~
 
Ici le cinquième saut est concerné par une perte liée à du rate limiting ICMP car les sauts suivants ne sont concernés que par une perte de 40%. La perte réelle à considérer est toujours celle observée sur les sauts suivants.

Il est important d'avoir en tête que parfois la perte est due à un problème sur le chemin du retour. Les paquets peuvent arriver correctement à destination mais se perdre sur le retour ce pourquoi il est toujours conseillé de lancer un MTR dans les deux sens.

### La latence réseau

Outre la perte de paquets, MTR permet la mise en évidence de la latence. Elle est bien sûr liée avant tout à des contraintes physiques et augmente avec le nombre de sauts. Cette augmentation doit idéalement être constante. La nature de la connexion affectera la latence que vous observez, les connexions ADSL auront une latence beaucoup plus élevée que les connexions par fibre par exemple.

## FAQ

**J'ai 100% de perte sur un saut mais 0% à la destination.**

Tout va bien, cela signifie simplement qu'un équipement sur le trajet refuse l'ICMP.

**J'ai plus au moins de perte sur un saut mais 0% à la destination.**

Un équipement sur le chemin doit certainement faire du rate limiting ICMP, ce qui explique ces pertes. Ici encore pas d'inquietude.

**J'observe des ??? sur un saut.**

Il s'agit de time out. Cela ne signifie pas qu'il y a perte de paquet mais que l'équipement drop le trafic ICMP. Tout va bien.

**J'ai 100% de perte à la destination.**

Soit l'hôte de destination est injoignable, soit celui-ci reçoit les paquets mais n'est pas en mesure d'y répondre ce qui probablement alarmant (défaut de configuration).

## Liens

* [A Practical Guide to (Correctly) Troubleshooting with Traceroute Troubleshooting with Traceroute](https://www.nanog.org/meetings/nanog47/presentations/Sunday/RAS_Traceroute_N47_Sun.pdf)
