---
title: Howto LVM
category: linux disk
...

* <http://www.tldp.org/HOWTO/LVM-HOWTO/>

LVM (Logical Volume Manager) permet de gérer des « logical volumes » indépendamment des disques. On obtient ainsi un système de partitionnement plus souple, cela facilite notamment l'augmentation (future) de la taille d'un volume logique. Sous Linux, cela s'appuye sur le module noyau [Device Mapper](https://en.wikipedia.org/wiki/Device_mapper) et les [outils LVM2](http://www.sourceware.org/lvm2/).


## Installation

~~~
# apt install lvm2

# lvm version
  LVM version:     2.02.168(2) (2016-11-30)
  Library version: 1.02.137 (2016-11-30)
  Driver version:  4.35.0

$ /sbin/modinfo dm_mod
filename:       /lib/modules/4.9.0-3-amd64/kernel/drivers/md/dm-mod.ko
license:        GPL
author:         Joe Thornber <dm-devel@redhat.com>
description:    device-mapper driver
alias:          devname:mapper/control
[…]
~~~

### PV : les partitions LVM

On doit ensuite créer des partitions de type « Linux LVM » (code `8E`).
Puis on initialise les partitions pour LVM :

~~~
# pvcreate /dev/hda1
# pvcreate /dev/hdb1
~~~

**Note** : dans certains cas, on devra utiliser l'option `-ff` (par exemple pour réinitialiser une partition)

On pourra ainsi lister les partitions LVM du système avec `pvdisplay` ou `pvs` :

~~~
# pvdisplay
--- Physical volume ---
PV Name               /dev/hda1
VG Name               group1
PV Size               124.84 GB / not usable 1.52 MB
Allocatable           yes
PE Size (KByte)       4096
Total PE              31959
Free PE               7639
Allocated PE          24320
PV UUID               T12qj5-SEkv-zNrB-QUdG-tFua-b6ok-p1za3e

--- Physical volume ---
PV Name               /dev/hdb1
VG Name               group1
PV Size               13.08 GB / not usable 2.08 MB
Allocatable           yes
PE Size (KByte)       4096
Total PE              3347
Free PE               3347
Allocated PE          0
PV UUID               CQEeDw-TYNK-n0nh-G7ti-3U3J-4zgk-a7xg2S

# pvs
PV         VG     Fmt  Attr PSize   PFree
/dev/hda1 group1 lvm2 a-    13.07G 13.07G
/dev/hdb1 group1 lvm2 a-   124.84G 34.84G

# pvs -o pv_mda_count,pv_mda_free /dev/hda1
#PMda #PMdaFree
    1    91.50K

# pvscan
PV /dev/sda9    VG group1   lvm2 [124.84 GB / 29.84 GB free]
PV /dev/sda12   VG group1   lvm2 [13.07 GB / 13.07 GB free]
Total: 2 [137.91 GB] / in use: 2 [137.91 GB] / in no VG: 0 [0   ]
~~~

Si la partition est redimensionnée, on peut augmenter la taille du PV :

~~~
# pvresize /dev/hda1
~~~

### VG : les groupes de volumes

Une fois nos PV initialisés, on créé un ou plusieurs groupes de volumes (VG) dans lesquels on découpera les volumes logiques (LV).

~~~
# vgcreate group1 /dev/hda1 /dev/hdb1
  Volume group "mylvmtest" successfully created
~~~

On peut ainsi les lister avec les commandes `vgdisplay` ou `vgs` :

~~~
# vgdisplay
  --- Volume group ---
  VG Name               group1
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  28
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                5
  Open LV               4
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               137.91 GB
  PE Size               4.00 MB
  Total PE              35306
  Alloc PE / Size       24320 / 95.00 GB
  Free  PE / Size       10986 / 42.91 GB
  VG UUID               zwApn7-SCSx-ju4h-6Y1R-x6ie-3wl0-uSE1DE

# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  group1   2   5   0 wz--n- 137.91G 42.91G

# vgscan
  Reading all physical volumes.  This may take a while...
  Found volume group "group1" using metadata type lvm2
~~~

### LV : les volumes logiques

On peut maintenant découper nos volumes finaux :

~~~
# lvcreate -L5G -n firstlvmvol group1
  Logical volume "firstlvmvol" created

# lvcreate -L10G -n secondlvmvol group1
  Logical volume "secondlvmvol" created
~~~

On a ainsi des périphériques de stockage utilisables (accessibles via `/dev/mapper/<VG>-<LV>` ou `/dev/<VG>/<LV>`) que l'on peut formater :

~~~
# mkfs.ext4 /dev/mapper/group1-firstlvmvol
# mkfs.ext4 /dev/group1/secondlvmvol
~~~

On peut lister les LV avec `lvdisplay` ou `lvs` :

~~~
# lvdisplay
  --- Logical volume ---
  LV Name                /dev/group1/firstlvmvol
  VG Name                group1
  LV UUID                iHCvHy-ow0G-Idf2-hNOi-TRFe-BqvW-tmowLj
  LV Write Access        read/write
  LV Status              available
  # open                 1
  LV Size                5.00 GB
  Current LE             2560
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0

  --- Logical volume ---
  LV Name                /dev/group1/secondlvmvol
  VG Name                group1
  LV UUID                S5GPY7-7q6n-1FCy-ydKA-Js2e-BAOy-wlgYQO
  LV Write Access        read/write
  LV Status              available
  # open                 1
  LV Size                10.00 GB
  Current LE             12800
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1

# lvs
  LV           VG     Attr   LSize  Origin Snap%  Move Log Copy%  Convert
  firstlvmvol  group1 -wi-ao  5.00G
  secondlvmvol group1 -wi-ao 10.00G

# lvscan
  ACTIVE            '/dev/group1/firstlvmvol'   [5.00 GB] inherit
  ACTIVE            '/dev/group1/secondlvmvol' [10.00 GB] inherit
~~~

### LV : le thin provisioning

Le thin provisioning est utilisé pour créer des disques virtuels à l'intérieur d'un volume logique. Quel intéret ? Supposons que nous ayons une capacité de stockage de 30 Go. Nous disposons de trois volumes logiques de 10 Go de stockage chacun. L'inconvénient peut être finalement de n'utiliser qu'une fine partie de cet espace alloué, ainsi si la nécessité d'un quatrième volume se présente à nous, impossible de le créer sans disque supplémentaire.

La méthode du thin provisioning permet justement de pallier cette problématique. Nos trois volumes logiques de 10 Go ne représentent alors, du point de vue du pool parent, que l'espace qu'ils occupent réellement. Si nos trois volumes ne représentent que 16 Go d'occupation réelle, on a alors 14 Go d'espace libre. En mode `thinpool` on pourra créer un quatrième volume. Cette méthode est pratique mais attention à l'overprovisioning !

~~~
# lvcreate -l 95%FREE --type thin-pool --thinpool lxc lxc
# lvcreate -V 3G --thin -n test1 lxc/lxc
# lvcreate -V 3G --thin -n test2 lxc/lxc
# lvcreate -V 3G --thin -n test3 lxc/lxc
# lvcreate -V 35G --thin -n test4 lxc/lxc
# lvcreate -V 10G --thin -n test5 lxc/lxc
# lvcreate -V 20G --thin -n test6 lxc/lxc
~~~

~~~
# lvs
  LV                          VG   Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lxc                         lxc  twi-a-tz-- 47,40g             10,46  5,27
  test1                       lxc  Vwi-a-tz--  3,00g lxc         62,69
  test2                       lxc  Vwi-a-tz--  3,00g lxc         52,14
  test3                       lxc  Vwi-a-tz--  3,00g lxc         50,51
  test4                       lxc  Vwi-a-tz-- 35,00g lxc         0,00
  test5                       lxc  Vwi-a-tz-- 10,00g lxc         0,00
  test6                       lxc  Vwi-a-tz-- 20,00g lxc         0,00
~~~

Note : Pensez à installer le paquet `thin-provisioning-tools`

### Les snapshots LVM

Un snapshot LVM sert à « figer » une partition à chaud. Cela permettra de faire une vraie sauvegarde « tranquillement » par la suite.

Exemple typique, une base SQL stocke ses fichiers dans `/srv/sql` qui est en LVM :

* On arrête la base SQL (ou on la « lock »)
* On déclenche un snapshot LVM de `/srv/sql`
* On redémarre (ou « délock ») la base SQL : elle n'aura été arrêtée que quelques secondes !
* Ensuite, on peut monter le snapshot et faire tranquillement son backup (tar, dd, rsync, etc.)
* On peut enfin supprimer le snapshot vu qu'il ne sera plus présent au prochain boot

**Note** : Il est possible de créer plusieurs snapshots. Attention, si il n'y a plus de place dans le LV du snapshot, celui-ci sera désactivé et inutilisable ! Il est donc conseillé de créer un snapshot avec une taille approprié. (En estimant la taille de différence entre l'origine et le snapshot)

~~~
# lvcreate -L100M -s -n snap /dev/mylvmtest/firstlvmvol
  Logical volume "snap" created

# lvdisplay
  --- Logical volume ---
  LV Name                /dev/mylvmtest/firstlvmvol
  VG Name                mylvmtest
  LV UUID                4vOXer-YH8x-AB9T-3MoP-BESB-7fyn-ce0Rho
  LV Write Access        read/write
  LV snapshot status     source of
                         /dev/mylvmtest/snap [active]
  LV Status              available
  # open                 0
  LV Size                500.00 MB
  Current LE             125
  Segments               1
  Allocation             inherit
  Read ahead sectors     0
  Block device           253:0

  --- Logical volume ---
  LV Name                /dev/mylvmtest/snap
  VG Name                mylvmtest
  LV UUID                lF0wn9-7O3A-FacC-gnVM-SPwE-fCnI-5jb9wz
  LV Write Access        read/write
  LV snapshot status     active destination for /dev/mylvmtest/firstlvmvol
  LV Status              available
  # open                 0
  LV Size                500.00 MB
  Current LE             125
  COW-table size         100.00 MB
  COW-table LE           25
  Allocated to snapshot  0.02%
  Snapshot chunk size    8.00 KB
  Segments               1
  Allocation             inherit
  Read ahead sectors     0
  Block device           253:3

# mkdir /tmp/snap
# mount /dev/mylvmtest/snap /tmp/snap/
# rsync -a /tmp/snap /tmp/demo
# umount /tmp/snap
# lvremove /dev/mylvmtest/snap
Do you really want to remove active logical volume "snap"? [y/n]: y
  Logical volume "snap" successfully removed
~~~

### LVM mirror : du RAID1 avec LVM

Une fonctionnalité peu connue de LVM est de permettre de faire du RAID1.
Peu d'intérêt à part peut-être gérer du RAID1 « retaillable » sans gérer une couche « MDADM + LVM ».

~~~
# pvcreate /dev/sda7
  Physical volume "/dev/sda7" successfully created

# pvcreate /dev/sdb5
  Physical volume "/dev/sdb5" successfully created

# lvcreate -L180G -m1 -nlvmirror --corelog vg00 /dev/sda7 /dev/sdb5
  Logical volume "lvmirror" created

# lvs -a
  LV                  VG   Attr   LSize   Origin Snap%  Move Log Copy%
  lvmirror            vg00 mwi-ao 180.00G                          8.60
  [lvmirror_mimage_0] vg00 iwi-ao 180.00G
  [lvmirror_mimage_1] vg00 iwi-ao 180.00G
~~~

Pour étendre un miroir LVM, on ajoute des PV au VG :

~~~
# pvcreate /dev/sda8
  Physical volume "/dev/sda8" successfully created

# pvcreate /dev/sdb6
  Physical volume "/dev/sdb6" successfully created

# vgextend vg00 /dev/sda8 /dev/sdb6
  Volume group "vg00" successfully extended
~~~

On désactive le miroir, on retaille, on réactive :

~~~
# lvextend -L+25G  /dev/vg00/lvmirror
  Extending 2 mirror images.
  Mirrors cannot be resized while active yeta.

# umount /dev/vg00/lvmirror

# lvchange -an /dev/vg00/lvmirror

# lvextend  -L+25G /dev/vg00/lvmirror
  Extending 2 mirror images.
  Extending logical volume lvmirror to 205.00 GB
  Logical volume lvmirror successfully resized

# lvchange -ay /dev/vg00/lvmirror
~~~

Enfin on retaille le filesystem :

~~~
# e2fsck -f /dev/vg00/lvmirror
e2fsck 1.40-WIP (14-Nov-2006)
Pass 1: Checking inodes, blocks, and sizes
Pass 2: Checking directory structure
Pass 3: Checking directory connectivity
Pass 4: Checking reference counts
Pass 5: Checking group summary information
/dev/vg00/lvmirror: 216749/23592960 files (13.5% non-contiguous), 44825506/47185920 blocks

# resize2fs /dev/vg00/lvmirror
resize2fs 1.40-WIP (14-Nov-2006)
Resizing the filesystem on /dev/vg00/lvmirror to 53739520 (4k) blocks.
The filesystem on /dev/vg00/lvmirror is now 53739520 blocks long.
~~~

## FAQ

### Supprimer une partition LVM d'un VG

~~~
# pvmove -v /dev/hde1
    Finding volume group "mylvmtest"
    Archiving volume group "mylvmtest" metadata.
    Creating logical volume pvmove0
  mirror: Required device-mapper target(s) not detected in your kernel

# vgreduce mylvmtest /dev/hde1
  Removed "/dev/hde1" from volume group "mylvmtest"
~~~

### Supprimer un LV `testlv` dans le VG `vg00`

~~~
# lvremove -v /dev/vg00/testlv
    Using logical volume(s) on command line
Do you really want to remove active logical volume "testlv"? [y/n]: y
    Archiving volume group "vg00" metadata.
    Found volume group "vg00"
    Removing vg00-testlv
    Found volume group "vg00"
    Releasing logical volume "testlv"
    Creating volume group backup "/etc/lvm/backup/vg00"
  Logical volume "testlv" successfully removed
~~~

### Renommer un LV `lvold` en `lvnew` au sein du VG `vg00`

~~~
# lvrename vg00 lvold lvnew
~~~


### Augmenter la taille d'un LV

C'est la commande `lvextend` qu permet de redimensionner un volume logique.
On peut indiquer la valeur en relatif (ajouter 10Go : `-L+10G`) ou en absolu (redimensionner à 500G : `-L500G`).

L'option `-r` permet de redimensionner automatiquement le filesystem dans le volume. Dans certains cas il ne faut pas le faire (avec du DRBD par exemple).

~~~
# umount /dev/mylvmtest/thirdlvmvol

# #-l +100%FREE
# lvextend -r -L+10G  /dev/mylvmtest/thirdlvmvol
fsck from util-linux 2.25.2
BACKUP: clean, 11/704854016 files, 89036065/2819416064 blocks
  Size of logical volume mylvmtest/thirdlvmvol changed from 2.63 TiB (688334 extents) to 2.64 TiB (690894 extents).
  Logical volume backup successfully resized
resize2fs 1.42.12 (29-Aug-2014)
Resizing the filesystem on /dev/mapper/myvmtest-thirdlvmvol to 2829901824 (1k) blocks.

# mount /dev/mylvmtest/thirdlvmvol
~~~

> **Note** : On peut aussi utiliser la commande `lvresize`.

~~~
# lvresize -r -l+100%FREE /dev/mylvmtest/thirdlvmvol
~~~

> Pour un système de fichier XFS, on utilisera `xfs_growfs` au lieu de `resize2fs` ou `lvresize -r` :
> 
> ~~~ { .sh }
> lvextend -L+10G /dev/mapper/vg0-data
> xfs_growfs /srv/data
> ~~~

### Réduire la taille d'un LV

`lvreduce` peut se charger de réduire le système de fichier sous-jacent
(avec `-r`). On peut indiquer la réduction en relatif ou en absolu
(`-L-10G` ou `-L50G`).

**Attention** : il n’est pas possible de réduire un volume formatté en `ext4` sans le démonter !

~~~
# umount /dev/mylvmtest/secondlvmvol

# lvreduce -L-300G /dev/mylvmtest/secondlvmvol
fsck from util-linux 2.25.2
BACKUP: 11/783497216 files (0.0% non-contiguous), 98946865/3133988864 blocks
resize2fs 1.42.12 (29-Aug-2014)
Resizing the filesystem on /dev/mapper/mylvmtest-secondlvmvol to 2819416064 (1k) blocks.

  Size of logical volume mylvmtest/secondlvmvol changed from 2.92 TiB (765134 extents) to 2.63 TiB (688334 extents).
  Logical volume backup successfully resized

# mount /dev/mylvmtest/secondlvmvol
~~~

### Augmenter la taille d'un PV

Cas d'une VM ou l'on a augmenté la taille de l'image disque, ou que l'on a augmenté la taille du disque via VMWare.

Il faut supprimer puis recréer la partition LVM concernée, il faut que cette partition soit la dernière sur le disque, il faut bien recréer la partition avec comme type « Linux LVM » (code `8E`).


On affiche les volumes actifs :

~~~
# lvscan
  ACTIVE            '/dev/vg0/HOME' [20.00 GiB] inherit
~~~

Il faut désactiver le volume :

~~~
# lvchange -a n /dev/vg0/HOME
~~~

Si le VG est sur la partition 2 du disque sda par exemple :

~~~
fdisk /dev/sda
~~~

puis "d 2" pour supprimer la partiton, "n" pour en créer une nouvelle en veillant à conserver le même "First sector" que la partition d'origine.

Vous souhaitez également conserver la signature existante :

~~~
Created a new partition 2 of type 'Linux' and of size 50 GiB.
Partition #2 contains a LVM2_member signature.

Do you want to remove the signature? [Y]es/[N]o: N
~~~

Enfin "t" puis "8e" et "w".

Ensuite il faut recharger la table de partition avec partprobe :

~~~
# partprobe
~~~

**/!\\ Si on a une erreur avec partprobe il faut redémarrer la VM. /!\\**

Puis on étend le PV :

~~~
# pvresize /dev/sda2
~~~

On vérifie de le PV et le VG ont bien l'espace de libre :

~~~
# pvs
PV         VG   Fmt  Attr PSize   PFree
/dev/sda2  vg0  lvm2 a--  50g    78g 

# vgs
 VG   #PV #LV #SN Attr   VSize   VFree
 vg0    1   6   0 wz--n- 50g    78g 
~~~

On peut ensuite étendre le volume logique que l'on souhaite.

### LVM et les tailles

Les tailles reportées par LVM sont très peu fiables.

Un exemple concret avec un VG qui annonce :

~~~
  VG Size               137.91 GB
  PE Size               4.00 MB
  Total PE              35306
  Alloc PE / Size       23040 / 90.00 GB
  Free  PE / Size       12266 / 47.91 GB
~~~

On a tendance à croire qu'il reste de la place… Pourtant un `lvextend` ou `lvcreate` échoue.

Par exemple :

~~~
# lvextend -L+10G /dev/group1/data
  Extending logical volume data to 30.00 GB
  device-mapper: resume ioctl failed: Invalid argument
  Unable to resume group1-data (253:3)
  Logical volume data successfully resized

# lvcreate -L5G -ntest group1
  device-mapper: resume ioctl failed: Invalid argument
  Unable to resume group1-test (253:4)
  /dev/group1/test: write failed after 0 of 4096 at 0: No space left on device
  Logical volume "test" created
~~~

### Restauration

**/!\\ à manipuler avec beaucoup de précautions /!\\**

LVM sauvegarde ses métadatas dans `/etc/lvm/backup` et `/etc/lvm/archive`.
On peut éventuellement les restaurer via la commande `vgcfgrestore`.

#### Cas du thin provisioning

Au reboot, disparition du vg dans /dev                                                   

~~~
# vgcfgrestore lxc --force
  WARNING: Forced restore of Volume Group lxc with thin volumes.
  Restored volume group lxc
# vgchange -ay lxc
  4 logical volume(s) in volume group "lxc" now active
~~~

Note : La présence de `/usr/sbin/thin_check: execvp failed: Aucun fichier ou dossier de ce type` dans le retour commande signifie qu'il manque le paquet `thin-provisioning-tools`

### Infos sur les volumes

~~~
# dmsetup info -c
# dmsetup info
~~~

### Réduire la taille d'un VG+PV

TODO.
Très bonne info à adapter : <http://unix.stackexchange.com/a/193971>

### Filter la détection des VG

Par défaut, au démarrage de la machine, LVM scanne tous les block devices pour y repérer les signatures LVM et activer les VG.
C'est un comportement que l'on ne veut pas forcément. On pourra donc ajouter un filtre dans `/etc/lvm/lvm.conf`. Voici un exemple :

~~~
filter = [ "a|^/dev/sd[a-zA-Z]+[0-9]*$|", "a|^/dev/nvme[0-9]+(n[0-9]+)?(p[0-9]+)?$|", "a|^/dev/md[0-9]+$|", "r|.*/|" ]
global_filter = [ "a|^/dev/sd[a-zA-Z]+[0-9]*$|", "a|^/dev/nvme[0-9]+(n[0-9]+)?(p[0-9]+)?$|", "a|^/dev/md[0-9]+$|", "r|.*/|" ]
~~~

**Afin que ce changement soit pris en compte au redémarrage il faut aussi mettre à jour l'initramfs** (udev/lvmetad étant démarrés durant l'initramfs) :

~~~
update-initramfs -k all -u
~~~

> *Note* : le cas classique est sur un hyperviseur, il faut exclure les périphériques qui peuvent contenir des VG à l'intérieur des VMs.. il faut alors bien exclure les VGs de l'hyperviseur (par exemple `/dev/SSD.*|`, `/dev/SATA.*|` etc.) et non les VGs à l'intérieur des VMs !

> *Note 2* : Attention à la syntaxe des expressions régulières, il faut que cela soir `r|/dev/foo.*|` et non `r|/dev/foo/.*|`

> *Note 3* : global_filter est nécessaire pour exclure le scan au démarrage car filter n'est pas pris en compte par udev et lvmetad.

> *Note 4* : Quand possible, préférez un liste blanche au lieu d'exclusions. (ex. `filter = [ "a|^/dev/sda[0-9]*$|", "a|^/dev/sdb[0-9]*$|", "r|.*/|" ]` pour n'autorisé que les PV qui sont directement des partitions de /dev/sda et /dev/sdb (+ les disques eux même))

Pour réinitialisé le cache (généré par lvmetad/udev au démarrage) :

~~~
# pvscan --cache
# vgscan --cache
~~~

Afin de vérifier le bon fonctionnement des filtres, il est possible d'utiliser `pvs --config` :

~~~
# pvs --config 'devices {
  filter = [ "a|^/dev/sda[0-9]*$|", "a|^/dev/sdb[0-9]*$|", "r|.*/|" ]
  global_filter = [ "a|^/dev/sda|", "a|^/dev/sdb|", "r|.*/|" ]
}'
~~~

### Désactiver un VG (pour rendre le disque sous-jacent utilisable par le système)

Par défaut LVM active tous les VG trouvés au démarrage, cela rend les PV sous-jacent inutilisable, ce qui peut être problématique. Pour désactiver le VG (rendant les LV l'utilisant inaccessibles mais rendant les PV sous-jacent utilisable), faire la commande suivante :

~~~
# vgchange -a n "${vg:?}"
~~~

### Savoir si un LV est ouvert

Il faut regarder la sixième colonne du champs "Attr" dans la sortie de [`lvs(8)`](https://manpages.debian.org/bookworm/lvm2/lvs.8.en.html#NOTES).
On aura "o" si le volume est ouvert et "-" sinon.
Dans l'exemple suivant, le volume `baz/foo` est ouvert et le volume `baz/bar` ne l'est pas.
```
# lvs
  LV  VG   Attr
  foo baz -wi-ao----
  bar baz -wi-a-----
               ↑
```

La commande lvdisplay(8) affiche aussi une ligne "# open".
