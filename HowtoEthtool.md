---
title: Howto Ethtool
categories: sysadmin network
...

* Manpage : <https://manpages.debian.org/stretch/ethtool/ethtool.8.en.html>

[ethtool](http://www.kernel.org/pub/software/network/ethtool/) est un l'outil standard sous Linux permettant de voir et contrôler les paramètres d'une carte réseau (vitesse, half/full duplex, etc.)

## Installation


~~~
# apt install ethtool

# ethtool --version
ethtool version 4.8
~~~

## Utilisation

### Consulter l'état d'une interface

~~~
# ethtool eth0
Settings for eth0:
    Supported ports: [ TP MII ]
    Supported link modes:   10baseT/Half 10baseT/Full 
                            100baseT/Half 100baseT/Full 
                            1000baseT/Half 1000baseT/Full 
    Supported pause frame use: No
    Supports auto-negotiation: Yes
    Advertised link modes:  10baseT/Half 10baseT/Full 
                            100baseT/Half 100baseT/Full 
                            1000baseT/Half 1000baseT/Full 
    Advertised pause frame use: Symmetric Receive-only
    Advertised auto-negotiation: Yes
    Link partner advertised link modes:  10baseT/Half 10baseT/Full 
                                         100baseT/Half 100baseT/Full 
    Link partner advertised pause frame use: Symmetric
    Link partner advertised auto-negotiation: Yes
    Speed: 100Mb/s
    Duplex: Full
    Port: MII
    PHYAD: 0
    Transceiver: internal
    Auto-negotiation: on
    Supports Wake-on: pumbg
    Wake-on: g
    Current message level: 0x00000033 (51)
                   drv probe ifdown ifup
    Link detected: yes
~~~

### Changer son état

Par exemple, configurer la vitesse à 10Mb/s, le duplex half, et désactiver l'auto-négotiation :

~~~
# ethtool -s eth0 speed 10 duplex half autoneg off
~~~

Pour changer l'adresse mac :

~~~
# ethtool -s eth0 phyad 01:23:45:67:89:0a
~~~
