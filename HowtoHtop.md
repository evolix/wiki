---
categories: sysadmin
title: Howto Htop
...


[Htop](http://hisham.hm/htop/) est un outil permettant de visualiser les ressources systèmes ainsi que les processus et leurs caractéristiques (PID, CPU, mémoire, I/O…) de façon interactive sur un système Linux/UNIX. Il est écrit en C avec la bibliothèque _ncurses_. Htop s'inspire de `top`, outil historique, en apportant plusieurs améliorations (couleurs, scroll vertical, interactions avec la souris, etc.). 


## Installation

~~~
# apt install htop lsof strace

$ htop -v
htop 2.0.2 - (C) 2004-2016 Hisham Muhammad
Released under the GNU GPL.
~~~

## Utilisation basique

~~~
$ htop
~~~

Options utiles :

* `-u www-data` : afficher uniquement les processus d'un utilisateur ;
* `-d 10` : changer le délai de rafraichissement en dixièmes de seconde (par défaut 20) ;
* `-p 21,42` : afficher uniquement les processus avec le PID 21 et 42 ;
* `-s PERCENT_CPU` : trier les processus par PERCENT_CPU, PERCENT_MEM, IO_RATE… (liste complète avec `htop --sort-key help`) ;
* `-C` : forcer l'affichage en monochrome.

![*Exemple d'une sortie htop sur un poste utilisateur.*](/htop.png)

En appuyant sur `F1` on peut voir les options disponibles :

![*L'aide de Htop.*](/htop_F1.png)


## Configuration

La configuration par défaut se trouve dans `/etc/htoprc`.

Nous mettons par défaut dans ce fichier :

~~~
# Force the SWAP column to the right of the CPU one
fields=0 48 17 18 38 39 40 119 2 46 47 49 1
~~~


On peut configurer Htop « dynamiquement » en appuyant sur `F2`.

On peut notamment :

* choisir le thème de couleur (`color_scheme`) ;
* choisir les métriques à afficher en haut ;
* choisir les colonnes à afficher et leur ordre (`fields`) ;
* voir ou non les processus kernel (`hide_kernel_threads`) ;
* voir le chemin complet des processus (`show_program_path`).
 
Les choix sont persistants, ils sont conservés dans le fichier `~/.config/htop/htoprc`.

Voici un exemple de fichier `~/.config/htop/htoprc` :

~~~
fields=109 110 111 0 48 17 18 38 39 40 2 46 47 49 1
sort_key=46
sort_direction=1
hide_threads=0
hide_kernel_threads=0
hide_userland_threads=0
shadow_other_users=0
show_thread_names=0
show_program_path=1
highlight_base_name=0
highlight_megabytes=1
highlight_threads=1
tree_view=1
header_margin=1
detailed_cpu_time=1
cpu_count_from_zero=1
update_process_names=0
account_guest_in_cpu_meter=0
color_scheme=5
delay=15
left_meters=CPU AllCPUs
left_meter_modes=1 1
right_meters=Hostname Clock Tasks LoadAverage Uptime Memory Swap
right_meter_modes=2 2 2 2 2 1 1
~~~

## Astuces

### Voir les I/O disque

Htop permet d'afficher les valeurs de lecture, écriture et performance sur les I/O disque depuis Debian 6.
Pour activer ces colonnes :

`F2` > Columns > Available Columns > `F5` ("Ajouter les IO_*", en bas de la liste)

### Filtre de processus

Avec `F4`, on peut entrer le nom d'un processus (ou une partie du nom) et afficher uniquement les processus correspondants.

### Informations sur un processus

On peut sélectionner un processus avec les flèches haut/bas ou la souris.

On peut alors avoir les actions suivantes sur ce processus :

* `e` : lister ses variables d'environnement ;
* `l` : lister ses fichiers ouverts (avec `lsof`) ;
* `s` : lister ses appels système (avec `strace`) ;
* `i` : ajuster sa priorité I/O (avec `ionice`) ;
* `F8` : diminuer sa priorité (incrémente sa valeur `nice`) ;
* `F7` : augmenter sa priorité (décrémente sa valeur `nice`, réservé à _root_) ;
* `F9` : envoyer un signal KILL (par défaut SIGTERM).

### Kill de plusieurs processus

On peut marquer des processus avec la touche `Espace` (ou la touche `c` pour les processus enfant),
puis lancer un signal KILL à tous les processus marqués en faisant `F9`.

On peut « démarquer » tous les processus avec `U`.

### Voir la moyenne d'utilisation CPU sur tous les cores

Si vous voulez visualiser la moyenne d'utilisation CPU sur l'ensemble des cores, vous pouvez l'activer ainsi :

`F2` > Meters > Available meters > CPU AVerage (appuyer sur Entrée et sélectionner où vous voulez l'afficher)

Vous pouvez aussi supprimer la visualisation pour chaque core en supprimant `CPUs (1/1) [Bar]` dans _Left Column_


## Plomberie

Où/comment Htop va chercher ses informations ? Que veulent-elles dire ?
Pēteris Ņikiforovs explique tout cela en détails sur <https://peteris.rocks/blog/htop/>
Une traduction partielle se trouve dans une série d'articles sur <https://carlchenet.com/category/htop-explique/>


## FAQ

### Ma mémoire est remplie en permanence, est-ce grave ?

La barre de mémoire de Htop se décompose en : "mémoire utilisée" (vert) + "buffers" (bleu) + "mémoire cache" (jaune).
Pour vulgariser, la mémoire cache peut être ignorée car Linux ne libère pas tout seul cette mémoire.
Vous pouvez forcer la libération de la mémoire cache avec `echo 3 > /proc/sys/vm/drop_caches` mais c'est déconseillé en général.

### Ma Swap est remplie en permanence, est-ce grave ?

Si la Swap est utilisée, cela signifie que votre système a eu besoin dans le passé de s'en servir, par exemple car il manquait de mémoire.
Cela ne signifie par forcément que la Swap est en cours d'utilisation.
Si vous avez de la mémoire libre, vous pouvez forcer une libération de votre Swap via `swapoff -a && swapon -a`.

### Que veulent dire les colonnes _VIRT_, _RES_ et _SHR_ ? 

_VIRT_ est la mémoire virtuelle d'un processus. Elle inclut notamment les fichiers mappés en mémoire, la mémoire de la carte vidéo et les allocations mémoire (fonctions _malloc()_ en C) qui ne sont pas forcément utilisées en pratique.

_RES_ est la mémoire résidente d'un processus, c'est un bon indicateur de la mémoire réellement utilisée même si une partie de cette mémoire peut être partagée avec d'autres processus. Il faut noter aussi qu'en cas de fork d'un processus, la mémoire résidente sera identique pour le second process mais pas réellement utilisée car Linux utilise une méthode de copy-on-write.

_SHR_ est la mémoire partagée d'un processus, par exemple les bibliothèques partagées.

En résumé, la mémoire réellement utilisée par un processus au moment présent se situe entre _RES_-_SHR_ et _VIRT_, et l'on peut considérer que _RES_ est une bonne approximation.


