# Howto Gphoto

~~~
# aptitude install gphoto2
~~~


Lister les fichiers :

~~~
$ gphoto2 -L
~~~

Obtenir un fichier :

~~~
$ gphoto2 -p <numéro>
~~~

Obtenir une série de fichiers :

~~~
$ gphoto2 -p <numéro>-<numéro>
~~~

Obtenir tous les fichiers :

~~~
$ gphoto2 --get-all-files
~~~

