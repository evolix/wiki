---
categories: WIP fonts
title: How to Fontconfig
...

* Documentation: <https://www.freedesktop.org/software/fontconfig/fontconfig-user.html>

Fontconfig est le système permettant aux applications de trouver les polices d'écritures installées sur un serveur.

## Installation

```
sudo apt install fontconfig
```

## Ajouter une police d'écriture

Pour ajouter une police d'écriture il faut ajouter ses fichiers dans un sous-dossier de `/usr/share/fonts/` puis lancer `fc-cache`, par exemple :

~~~
# mkdir /usr/share/fonts/truetype/foo/ /usr/share/fonts/truetype/foo/
# cp *ttf
# chmod -R g+rX,o+rX /usr/share/fonts/truetype/foo/
# fc-cache
# chmod -R g+rX,o+rX /var/cache/fontconfig/
~~~

On peut ensuite lister en tant qu'utilisateurs les polices disponibles :

~~~
$ fc-list

/usr/share/fonts/truetype/dejavu/DejaVuSerif-Bold.ttf: DejaVu Serif:style=Bold
...
~~~

