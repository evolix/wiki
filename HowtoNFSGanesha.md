---
categories: storage haute-disponibilite
title: How-To NFS/Ganesha
...

* Documentation : <https://github.com/nfs-ganesha/nfs-ganesha/wiki>

NFS/Ganesha est un serveur NFS et 9P en userspace pouvant à la fois exporter une partie de la hiérarchie de fichier du serveur et exporter des volumes provenant de technologies telles que [GlusterFS](https://wiki.evolix.org/HowtoGlusterFS) et [Ceph](https://wiki.evolix.org/HowtoCeph).

## Installation

Il y a (au moins) deux paquets à installer : le paquet `nfs-ganesha` fournissant le serveur NFS et un paquet fournissant le support du backend à fournir (`nfs-ganesha-vfs` pour exporter une partie de la hiérarchie de fichier, `nfs-ganesha-gluster` pour GlusterFS, `nfs-ganesha-ceph` pour Ceph, ...):

```
apt install nfs-ganesha nfs-ganesha-vfs
```

## Configuration

La configuration de NFS-Ganesha se fait à travers de `/etc/ganesha/ganesha.conf`. Ce fichier est constitué de plusieurs blocs.

### Bloc NFS_CORE_PARAM

Ce bloc défini la configuration globale du serveur.

#### Directives

* `NFS_Port`: Le port sur lequel le serveur écoute pour le protocol NFS. (défaut: `2049`)
* `Rquota_Port`: Le port sur lequel le serveur écoute pour le protocol RQUOTA. (défaut: `875`)
* `Bind_addr`: L'IP sur laquelle le serveur écoute. (défaut: `0.0.0.0`)
* `Protocols`: Les protocoles autorisés à être autorisées dans les exports. (défaut: `[3, 4, 9P]`, donc NFSv3, NFSv4 et 9P)
* `Clustered`: Si le serveur fait partie d'un cluster ou non. (défaut: `true`)
* `mount_path_pseudo`: Si le serveur devrait utiliser l'entrée `Pseudo` (true) ou `Path` (false) pour le chemin utilisable par les clients pour NFSv3 et 9P, fortement recommandé d'être mis à `true` afin de simplifier la gestion étant donné que NFSv4 utilise toujours `Pseudo`. (défaut: `false`)

### Blocs EXPORT

Ces blocs permettent de définir un export NFS/9P.

#### Directives

* `Export_Id`: Ce champ **obligatoire** est un nombre entre 0 (obligatoirement utilisé pour le pseudo-root) et 65535 qui doit être unique à l'export en cours de définition.
* `Path`: Chemin à exporter, la signification de ce champ dépend du backend utilisé, dans le cas du backends `vfs` il s'agit du chemin dans la hiérarchie de fichier.
* `Pseudo`: Chemin utilisé pour le montage sous NFSv4 (et optionnellement NFSv3 et 9P, cf. plus haut).
* `Protocols`: Protocoles pour lesquels cet export existe.
* `Access_Type`: Actions pouvant être faites par les clients. (Correspond essentiellement à une option de montage `rw` ou `ro`.) Peut être l'une des valeurs suivantes :
    * `RW`: Peut tout faire.
    * `RO`: Accès en lecture seule uniquement.
    * `MDONLY`: Ne permet pas les opérations `READ` et `WRITE` mais autorise tout le reste.
    * `MDONLY_RO`: Ne permet pas les opérations `READ`, `WRITE` et toutes opérations qui modifieraient les attributs de fichiers ou le contenu de répertoires.
    * `NONE`: Aucun accès.
* `Squash`: Le type d'écrasage d'user id à utiliser. (défaut: `Root_Squash`) Peut être :
    * `No_Root_Squash`, `NoIdSquash`, `None`: Aucun écrasage
    * `RootId`, `Root_Id_Squash`, `RootIdSquash`: uid 0 et gid 0 sont remplacés par `Anonymous_Uid` et `Anonymous_Gid`. Le gid 0 est aussi enlevé de la liste des groupes alternatifs.
    * `Root`, `Root_Squash`, `RootSquash`: uid 0 combiné à n'importe quel groupe est remplacé par `Anonymous_Uid` et `Anonymous_Gid`. La liste des groupes secondaire n'est pas prise en compte.
    * `All`, `All_Squash`, `AllSquash`, `All_Anonymous`, `AllAnonymous`: Tous les utilisateurs et groupes sont remplacés par `Anonymous_Uid` et `Anonymous_Gid`.
* `Anonymous_Uid`: L'uid par lequel remplacer un uid écrasé. Peut être un chiffre entre -2147483648 et 4294967295. (défaut: `-2`)
* `Anonymous_Gid`: Le gid par lequel remplacer un gid écrasé. Peut être un chiffre entre -2147483648 et 4294967295. (défaut: `-2`)
* `SecType`: Le type de sécurité RPC utilisée. (défaut: `none, sys`) Peut être :
    * `none`
    * `sys`: Utilisation des uid/gid du client.
    * `krb5`: Utilisation d'authentification avec Kerberos
    * `krb5i`: Utilisation d'authentification et de garantie d'intégrité avec Kerberos
    * `krb5p`: Utilisation d'authentification et de chiffrement avec Kerberos
* `PrivilegedPort`: Autorisation des ports inférieure à 1024 pour le montage par les clients. (défaut: `false`)
* `Manage_Gids`: Mettre à `true` pour que la liste des groupes secondaire soit générée du côté du serveur au lieu de se baser sur `AUTH_SYS` (permet d'avoir plus de 16 groupes). (défaut: `false`)
* Des blocs `CLIENT`: Permet de gérer les permissions différemment pour certains clients, peut contenir les directives `Access_Type`, `Protocols`, `Anonymous_uid`, `Anonymous_gid`, `SecType`, `PrivilegedPort`, `Manage_Gids` et `Squash`. Il doit contenir une directive `Clients` contenant les clients auxquels ces directives s'appliquent, cette directive accepte les valeurs suivantes, plusieurs valeurs peuvent être utilisées en séparant par des virgules (`,`):
    * Un nom d'hôte
    * Un wildcard (`*`)
    * Une adresse IP
    * Une adresse d'un réseau IP (`x.x.x.x/y`)
* Un **unique** bloc `FSAL` définissant le backend utilisé pour cet export. Les directives utilisées dépendent du backend. Doit **obligatoirement** contenir une directive `Name` contenant le nom du backend.

### Exemple

Voici un exemple minimal pour exporter en NFSv3 et NFSv4 le `/srv` du serveur en tant que `/foo` en lecture seule, sauf pour 192.0.2.3 et 198.51.100.0/25 qui ont le droit de modifier le contenu des fichiers et 203.0.113.0/24 qui n'a aucun accès.

~~~
NFS_CORE_PARAM {
    mount_path_pseudo = true;
}

EXPORT {
    Export_Id = 1;

    Path = /srv;

    Pseudo = /foo;

    Protocols = 3,4;

    Access_Type = RO;

    Squash = root_squash;

    Sectype = sys;

    FSAL {
        Name = VFS;
    }

    CLIENT {
        Clients = 192.0.2.3, 198.51.100.0/25;

        Access_Type = RW;
    }

    CLIENT {
        Clients = 203.0.113.0/24;

        Access_Type = None;
    }
}
~~~

## Client NFS

Les exports NFS s'utilisent par les clients de la même façon que pour ceux du kernel Linux (cf. [HowtoNFS](/HowtoNFS#clients-nfs)).