**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto proxy avec Squid

On peut donc installer Squid pour faire office de proxy HTTP/HTTPS.
Voici un exemple de configuration :

~~~
# System
##########

# Squid normally listens to port 3128
#<http_port> 3128
<http_port> 3128 transparent

# By default, ICP queries are disabled
#icp_port 0

# Boost max FD (TODO : need ulimit ?)
max_filedescriptors 4096

# Logs
#emulate_<httpd_log> on
logformat combined   %>a %[ui %[un [%tl] "%rm %ru HTTP/%rv" %>Hs %<st "%{Referer}>h" "%{User-Agent}>h" %Ss:%Sh
access_log /var/log/squid3/access.log combined
#cache_log /var/log/squid3/cache.log
cache_store_log /var/log/squid3/store.log
#debug_options ALL,3

visible_hostname proxy.example.com

# Hint : custom error pages
# cp -r fr custom
# <http://www.squid-cache.org/Doc/config/deny_info/>
error_directory /usr/share/squid3/errors/custom
email_err_data on
#err_html_text

cache_mgr proxy@example.com

# Cache
#############

# Don't cache errors
negative_ttl 0
acl badreq http_status 400
cache deny badreq

# Gestion du cache
refresh_pattern ^ftp:           1440    20%     10080
refresh_pattern ^gopher:        1440    0%      1440
refresh_pattern .               0       20%     4320

hierarchy_stoplist cgi-bin ?
acl QUERY urlpath_regex cgi-bin \?
cache deny QUERY

coredump_dir /var/spool/squid3
cache_dir ufs /var/spool/squid3 800 16 256
cache_mem 128 MB
maximum_object_size_in_memory 512 KB
maximum_object_size 64 MB

# ACL
##############
# <http://wiki.squid-cache.org/SquidFaq/SquidAcl>

#acl all src 0.0.0.0/0
acl localhost src 127.0.0.0/8
acl localnet src 192.168.0.0/16

acl SSL_ports port 443

acl Safe_ports port 80
acl Safe_ports port 443

acl purge method PURGE
acl CONNECT method CONNECT

acl manager proto cache_object

acl ExceptionIpList src "/etc/squid3/exceptioniplist.conf"
acl ExceptionSiteList dstdom_regex "/etc/squid3/exceptionsitelist.conf"
acl BannedDestination dstdom_regex "/etc/squid3/banneddestination.conf"
acl BannedURL urlpath_regex "/etc/squid3/bannedURL.conf"

acl allowed_sites dstdomain .squid-cache.org
acl allowed_sites dstdomain .evolix.net

acl nocache_sites dstdomain .evolix.fr
acl nocache_sites dstdomain .evolix.com

# Deny requests to certain unsafe ports
<http_access> deny !Safe_ports

# Deny CONNECT to other than secure SSL ports
<http_access> deny CONNECT !SSL_ports

# admin
<http_access> allow manager localhost
<http_access> deny manager
<http_access> allow purge localhost
<http_access> deny purge

<http_access> allow allowed_sites

cache deny nocache_sites
<http_access> allow nocache_sites

<http_access> allow ExceptionSiteList
<http_access> deny BannedDestination
<http_access> deny BannedURL
<http_access> deny ExceptionIpList
<http_access> allow localnet

<http_access> deny all

### # rate limiting
### ####################
### # <http://www.squid-cache.org/Doc/config/delay_parameters/>
### # <http://www.squid-cache.org/Doc/config/delay_access/>
### 
### delay_pools 3
### 
### # Debit non limite
### delay_class 1 2
### delay_parameters 1 -1/-1 -1/-1
### 
### # Debit limite a 8000 bytes/s, soit 64 Kbps par poste
### delay_class 2 2
### delay_parameters 2 -1/-1 8000/8000
### 
### # Debit global limite a 120000 bytes/s, soit 960 Kbit/s
### # Debit postes limite a  40000 bytes/s, soit 320 Kbit/s
### delay_class 3 2
### delay_parameters 3 120000/120000 40000/40000
### 
### #delay_access 1 deny all
### #delay_access 2 deny all
### #delay_access 3 allow localnet
~~~

Créer les backlist et whitelist (vides) :

~~~
# cd /etc/squid3/
# for i in banneddestination.conf bannedURL.conf exceptioniplist.conf exceptionsitelist.conf; do touch $i; done
~~~

banneddestination.conf : Liste de sites bloqués, vous pouvez utiliser votre propre liste noire ou en récupérer une communautaire ou commerciale. Exemple :

~~~
^(.*\.)?007guard.com$
^(.*\.)?008i.com$
^(.*\.)?008k.com$
~~~

bannedURL.conf : Liste d'URLs bloqués, toute partie après le nom de domaine.
exceptioniplist.conf : Liste d'adresses IP à passer en mode blacklist. Par défaut on est en mode whitelist.
exceptionsitelist.conf : Sites autorisés pour le mode whitelist.

Configurer les pages d'erreurs en copiant le modèle Français. (Et éventuellement faire des modifications).

~~~
# cd /usr/share/squid3/errors
# cp -r fr custom
~~~


## Blacklists

<http://dsi.ut-capitole.fr/blacklists/>
<http://www.squidblacklist.org/> (payant)
<http://freecode.com/projects/sleezeball>
<http://www.urlfilterdb.com/> (payant)
<http://www.stearns.org/noads/> (obsolète)

## Ne pas cacher

Voici un exemple d'entête pour forcer un proxy à ne jamais cacher :

~~~
Cache-Control: max-age=0, no-cache, no-store, must-revalidate
Pragma: no-cache
Expires: Thu, 01 Jan 1970 00:00:42 GMT
~~~

Sur un serveur web Apache, on peut ainsi mettre en place un fichier .htaccess du type :

~~~
<Files foo.bar>
FileETag None
<ifModule mod_headers.c>
Header unset ETag
Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
Header set Pragma "no-cache"
Header set Expires "Thu, 01 Jan 1970 00:00:42 GMT"
</ifModule>
</Files>
~~~

En PHP, on pourra ainsi écrire le code suivant :

~~~
header("Pragma: no-cache");
header("Cache-Control: max-age=0, no-cache, no-store, must-revalidate");
header("Expires: Thu, 01 Jan 1970 00:00:42 GMT");
~~~
