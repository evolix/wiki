**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Reverse Proxy avec Squid

[wiki:HowtoSquid Squid] peut être utilisé comme un puissant _reverse proxy_ permettant une mise en cache de contenu avec de grandes performances.[[BR]]
Son utilisation en amont d'un load-balancer comme [wiki:HowtoHaproxy HAProxy] s'utilise dans les infrastructures haute-disponibilité.


~~~
                                                      _________port 80: Frontal 1
                                                     /
Clients ---> port 80: Squid ---> port 8080: HAProxy /__________port 80: Frontal 2
                                                    \
                                                     \_________port 80: Frontal 3
~~~

Pour la mise en cache du contenu, Squid se comporte un peu comme un navigateur : il met en cache (ou pas !) selon les entêtes HTTP
envoyés par les différents contenus. Il permet également la gestion des réponses "304 - Not modified" qui évitent aux navigateurs
de télécharger plusieurs fois le même contenu si il n'a pas changé.

## Installation

Voir [wiki:HowtoSquid]

## Configuration

*squid.conf* pour une configuration basique :

~~~
#acl all src 0.0.0.0/0.0.0.0
acl localhost src 127.0.0.1/255.255.255.255
acl purge method PURGE

<http_access> allow purge localhost
<http_access> allow all

logformat combined %>a %ui %un [%tl] "%rm %ru HTTP/%rv" %Hs %<st "%{Referer}>h" "%{User-Agent}>h" %Ss:%Sh
access_log /var/log/squid3/access.log combined
emulate_<httpd_log> on
logfile_rotate 365

<http_port> 80 vhost
cache_peer 127.0.0.1 parent 8080 0 no-query originserver

refresh_pattern .               1       20%     4320 ignore-reload
coredump_dir /var/spool/squid3

<httpd_suppress_version_string> on
strip_query_terms off

cache_dir ufs /var/spool/squid3 15000 16 256
cache_mem 1000 MB
maximum_object_size_in_memory 1000 KB
~~~

## Pages d'erreur

Il semble évident que les pages d'erreur applicatives doivent s'assurer de ne pas être mises en cache (ça c'est le boulot des développeurs). Pour certains types d'erreur (connexions refusées, 404, etc.), elles sont suceptibles d'être mises en "cache négatif" (TCP_NEGATIVE_HIT). Pour éviter cela, on ajoutera l'option :

~~~
negative_ttl 0
~~~

Pour ne pas cacher certains codes d'erreur spécifiques, on fera par exemple :

~~~
acl badreq <http_status> 400
cache deny badreq
~~~

## Purge du cache

Pour purger une page précise :

~~~
squidclient -p80 -m PURGE <http://www.example.com/foo/bar>
~~~

Pour purger tout le cache, une méthode douce (les connexions en cours ne sont pas coupées) mais lente (les nouvelles connexions vont échouer pendant ce temps) :

~~~
/etc/init.d/squid3 stop
echo "" > /var/spool/squid3/swap.state
rm -rf /var/spool/squid3/0*
/usr/sbin/squid3 -z
/etc/init.d/squid3 start
~~~

Une méthode quasi-instantanée et violente (coupure des connexions en cours) :

~~~
kill -9 `cat /var/run/squid3.pid`
echo "Deleting data file..."
# rm -f /var/spool/squid3/swap.state
rm -rf /var/spool/squid3/*
/etc/init.d/squid3 start
~~~

## Divers

### Authentification HTTP avec Squid

Si une authentification HTTP est présente sur les frontaux web, il est nécessaire d'ajouter l'option *login=PASS* à la directe _cache_peer_' :

~~~
cache_peer 127.0.0.1 parent 8080 0 no-query originserver login=PASS
~~~

### Apache et Squid sur le port 80

Pour ajouter de façon souple un reverse-proxy Squid sur un serveur web existant, il suffit de faire écouter Squid sur le port 80
mais avec une adresse IP distincte d'Apache (ou Nginx) :

~~~
<http_port> 192.0.43.10:80 vhost
cache_peer 192.0.43.7 parent 80 0 no-query originserver login=PASS
~~~

Attention, il faudra aussi modifier le serveur web pour qu'il n'écoute pas sur toutes les IPs de la machine.
Pour Apache, on modifiera par exemple :

~~~
#Listen 80
Listen 127.0.0.1:80
Listen 192.0.43.7:80
Listen 192.0.43.8:80
~~~

On pourra donc tester/activer Squid simplement en changeant l'enregistrement DNS du site concerné !
