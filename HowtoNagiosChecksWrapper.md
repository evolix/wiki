---
title: Howto Nagios alerts wrapper.
...

Cet article documente la façon dont on peut temporairement désactiver les alertes de monitoring (Nagios, Icinga…) grace à un "wrapper" autour du check réalisé sur le serveur.

Le principe est de faire croire au monitoring que tout va bien, même si ça n'est pas forcément le cas, lorsque certaines conditions sont satisfaites.

Plusieurs variations sont possibles, mais nous allons documenter une approche très simple, basée sur la présence d'un fichier temporaire.

## Gestion du fichier temporaire

Un script `alerts_switch` s'occupera de gérer le fichier temporaire.

~~~
# /usr/share/script/alerts_switch disable tomcat
~~~

Cette action va créer un fichier temporaire `/var/lib/misc/tomcat_alerts_disabled` et inscrire une entrée datée dans le fichier `/var/log/tomcat_alerts.log`.

~~~
# /usr/share/script/alerts_switch enable tomcat
~~~

Cette action va renommer le fichier en `/var/lib/misc/tomcat_alerts_enabled` s'il existe ou le créer (pour indiquer clairement qu'on a souhaité que les alertes soient activées).

NB : on ne se contente pas de supprimer le fichier "disabled" pour ne pas risquer de supprimer un fichier important qui serait là au préalable, portant par malchance le même nom.

Détail du script `alerts_switch` :

~~~{.bash}
#!/bin/bash

# https://forge.evolix.org/projects/evolix-private/repository
#
# You should not alter this file.
# If you need to, create and customize a copy.

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

usage() {
    echo "$PROGNAME action prefix"
}

disable_alerts () {
    disabled_file="$1_disabled"
    enabled_file="$1_enabled"

    if [ -e "${enabled_file}" ]; then
        mv "${enabled_file}" "${disabled_file}"
    else
        touch "${disabled_file}"
        chmod 0644 "${disabled_file}"
    fi
}

enable_alerts () {
    disabled_file="$1_disabled"
    enabled_file="$1_enabled"

    if [ -e "${disabled_file}" ]; then
        mv "${disabled_file}" "${enabled_file}"
    else
        touch "${enabled_file}"
        chmod 0644 "${enabled_file}"
    fi
}

now () {
  date --iso-8601=seconds
}

log_disable () {
    echo "$(now) - alerts disabled by $(logname || echo unknown)" >> $1
}

log_enable () {
    echo "$(now) - alerts enabled by $(logname || echo unknown)" >> $1
}

main () {
    local action=$1
    local prefix=$2

    local base_dir="/var/lib/misc"
    mkdir -p "${base_dir}"

    local file_path="${base_dir}/${prefix}_alerts"
    local log_file="/var/log/${prefix}_alerts.log"

    case "$action" in
    enable)
        enable_alerts ${file_path}
        log_enable ${log_file}
        ;;
    disable)
        disable_alerts ${file_path}
        log_disable ${log_file}
        ;;
    help)
        usage
        ;;
    *)
        >&2 echo "Unknown action '$action'"
        exit 1
        ;;
    esac
}

main $ARGS
~~~

## (Dés)activation des alertes

L'activation/désactivation des alertes peut se faire manuellement par exécution du script, mais elle peut aussi se faire automatiquement depuis le script d'initialisation d'un service.

Par exemple, avec Tomcat on peut ajouter les commandes citées plus haut dans les fonctions start/stop, juste après l'appel à `log_daemon_msg`

## Wrapper pour le check

Il reste à tenir compte de la présence ou pas du fichier temporaire au niveau des checks Nagios/NRPE.

Pour un check `check_tcp -p 8080`, on va jouter une commande et un argument juste avant :

~~~
$ alerts_wrapper tomcat check_tcp -p 8080
~~~

On peut ainsi invoquer n'importe quel check NRPE en le précédant du wrapper et du prefix voulu.

Détail du script `alerts_wrapper` :

~~~{.bash}
#!/bin/bash

# https://forge.evolix.org/projects/evolix-private/repository
#
# You should not alter this file.
# If you need to, create and customize a copy.

readonly check_name="${1}"
readonly check_file="/var/lib/misc/${check_name}_alerts_disabled"
readonly check_stdout=$(mktemp --tmpdir=/tmp "${check_name}_stdout.XXXX")

trap "rm ${check_stdout}" EXIT

shift

readonly check_command="$@"

$check_command > $check_stdout
readonly check_rc=$?

delay=0

if [ -e "${check_file}" ]; then
    readonly last_change=$(stat -c %Z "${check_file}")
    readonly limit=$(date --date '24 hours ago' +'%s')

    delay=$((${last_change} - ${limit}))

    if [ "${delay}" -le "0" ]; then
        sudo /usr/share/scripts/alerts_switch enable ${check_name}
    fi
fi

if [ -e "${check_file}" ]; then
    readonly formatted_last_change=$(date --date "@$(stat -c %Z "${check_file}")" +'%c')

    echo "ALERTS DISABLED for ${check_name} (since ${formatted_last_change}, delay: ${delay} sec) - ${check_stdout}"
    if [ ${check_rc} = 0 ]; then
        # Nagios OK
        exit 0
    else
        # Nagios WARNING
        exit 1
    fi
else
    cat ${check_stdout}
    exit ${check_rc}
fi
~~~

Le wrapper exécute le check et en met de côté la sortie standard et le code de sortie.

Il cherche ensuite le fichier `/var/lib/misc/tomcat_alerts_disabled`.

S'il est présent et vieux de plus de 24h, il sera détruit pour ne pas risquer d'oubli.

Si après ça il ets toujours présent (donc valide), le wrapper informe le monitoring que les alertes sont désactivées (depuis quand) et lui donne également le résultat du check réel. L'état du check est au pire "warning" si le check a échoué.

NB: le scripts doit bien être exécutable par l'utilisateur du monitoring. Dans notre cas, un bon emplacement est `/usr/local/lib/nagios/plugins/`, avec des droits en `0755`.
