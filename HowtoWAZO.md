---
title: Howto WAZO
categories: SIP
...

Wazo peut être utilisé pour créer une application PABX basée sur plusieurs composants gratuits existants, dont Asterisk.

Wazo est un logiciel gratuit. La plupart de ses composants distinctifs, et Wazo Platform dans son ensemble, sont distribués sous licence GPLv3.

Wazo est un fork de XiVO, créé en 2005 en France par Sylvain Boily et la société Proformatique. 
En 2010, Proformatique a fusionné avec Avencall et Avencall a acquis les droits d'auteur et la marque de XiVO.

* Documentation : <https://wazo-platform.org/uc-doc/>
* Statut de cette page : WIP

## Installation

Wazo préconise les ressources suivantes sur le serveur, pour une installation en production d'environ 50 utilisateurs :

* 2 CPU
* 4 Gio de Ram
* 50 Gio d'espace disque

L'installation de la dernière version doit se faire a partir d'une Debian 11 minimum, voici les prérequis suivants avant de lancer l'installation :

~~~
# apt install git ansible curl
~~~

* On clone de dépôt Ansible de Wazo :

~~~
# git clone https://github.com/wazo-platform/wazo-ansible.git
# cd wazo-ansible
~~~

* Par défaut le playbook Ansible installe la version de dévelloppement, pour installer la version stable, il faut se mette dans la branche `stable` :

~~~
# ansible_tag=wazo-$(curl https://mirror.wazo.community/version/stable)
# git checkout $ansible_tag
~~~ 

* Installer les dependences de Wazo avec le playbook `requirements-postgresql.yml` :

~~~
# ansible-galaxy install -r requirements-postgresql.yml
~~~

* Editer le fichier `inventories/uc-engine` pour ajouter les préférences et les mot de passe, ou pour installer la derniere version stable :

~~~
[uc_engine:vars]
wazo_distribution = pelican-bullseye
wazo_distribution_upgrade = pelican-bullseye
~~~ 

* Pour definir les identifiants de l'accès root à l'API :

~~~
[uc_engine:vars]
engine_api_configure_wizard = true
engine_api_root_password = <YOUR_ROOT_PASSWORD>
api_client_name = <YOUR_API_USERNAME>
api_client_password = <YOUR_API_PASSWORD>
~~~

* Lancer le playbook pour l'installation de Wazo :

~~~
# ansible-playbook -i inventories/uc-engine uc-engine.yml
~~~

* Un fois l'installation terminé vérifié que tout les services soit fonctionnel avec la commande :

~~~
# wazo-service status
~~~

## Importer un backup

Par défaut les backups sont fait tout les jours à 6h25, et il y a une retention de 7 jours.

Les taches de backup sont fait dans `/usr/sbin/wazo-backup` et `/etc/logrotate.d/wazo-backup` et il sont stockés dans `/var/backups/wazo`

Voici la liste des repertoires et fichiers qui sont backupés par Wazo :

~~~
    /etc/asterisk/
    /etc/crontab
    /etc/dhcp/
    /etc/hostname
    /etc/hosts
    /etc/ldap/
    /etc/network/if-up.d/xivo-routes
    /etc/network/interfaces
    /etc/ntp.conf
    /etc/profile.d/xivo_uuid.sh
    /etc/resolv.conf
    /etc/ssl/
    /etc/systemd/
    /etc/wanpipe/
    /etc/wazo-agentd/
    /etc/wazo-agid/
    /etc/wazo-amid/
    /etc/wazo-auth/
    /etc/wazo-call-logd/
    /etc/wazo-calld/
    /etc/wazo-chatd/
    /etc/wazo-confd/
    /etc/wazo-confgend-client/
    /etc/wazo-dird/
    /etc/wazo-dxtora/
    /etc/wazo-phoned/
    /etc/wazo-plugind/
    /etc/wazo-purge-db/
    /etc/wazo-webhookd/
    /etc/wazo-websocketd/
    /etc/xivo/
    /root/.config/wazo-auth-cli/
    /usr/local/bin/
    /usr/local/sbin/
    /usr/local/share/
    /usr/share/wazo/WAZO-VERSION
    /var/lib/asterisk/
    /var/lib/wazo-auth-keys/
    /var/lib/wazo-provd/
    /var/lib/wazo/
    /var/log/asterisk/
    /var/spool/asterisk/
    /var/spool/cron/crontabs/
~~~

* Pour importer un backup il faut que la version de Wazo soit de la **même version** que la version où à été fait le backup.

* Au préallable il faut arrêter les services :

~~~
# wazo-service stop 
~~~

* On restaure les fichiers :

~~~
# tar xvfp /var/backups/wazo/data.tgz -C /
~~~

* On restaure la base de donnée :

~~~
# tar xvf db.tgz -C /var/tmp
# cd /var/tmp/pg-backup
# sudo -u postgres dropdb asterisk
# sudo -u postgres pg_restore -C -d postgres asterisk-*.dump
~~~

* On supprime les fichiers de cache généré par la précédentes base de donnée :

~~~
# rm -rf /var/cache/wazo-confgend/*
~~~ 

* Une fois les backups restaurés, il faut restauré l'UUID su serveur :

~~~
# XIVO_UUID=$(sudo -u postgres psql -d asterisk -tA -c 'select uuid from infos')
# echo "export XIVO_UUID=$XIVO_UUID" > /etc/profile.d/xivo_uuid.sh
~~~

Ensuite éditer le fichier `/etc/systemd/system.conf` pour mettre à jours la variable `DefaultEnvironment` avec la valeur de `XIVO_UUID`

* A ce stade vous devez soit redémarré la machine, soit executer ces commande pour redémarré les services :

~~~
# source /etc/profile.d/xivo_uuid.sh
# systemctl set-environment XIVO_UUID=$XIVO_UUID
# systemctl daemon-reload
# wazo-service start
~~~


