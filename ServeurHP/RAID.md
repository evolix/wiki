---
categories: bios raid
title: Serveur HP / RAID
...

Des informations plus générales sur les serveurs HP sont disponible sur cette page : [ServeurHP]().

Tuto : <https://kallesplayground.wordpress.com/useful-stuff/hp-smart-array-cli-commands-under-esxi/>

## Accéder au BIOS

Appuyer sur F9 au bon moment

## Installer l'utilitaire

~~~
# apt install apt-transport-https
# echo "deb https://downloads.linux.hpe.com/SDR/repo/mcp/ buster/current non-free" > /etc/apt/sources.list.d/hp.list
# curl http://downloads.linux.hpe.com/SDR/hpPublicKey1024.pub > /etc/apt/trusted.gpg.d/hpPublicKey1024.asc
# curl http://downloads.linux.hpe.com/SDR/hpPublicKey2048.pub > /etc/apt/trusted.gpg.d/hpPublicKey2048.asc
# curl http://downloads.linux.hpe.com/SDR/hpPublicKey2048_key1.pub > /etc/apt/trusted.gpg.d/hpPublicKey2048_key1.asc
# curl http://downloads.linux.hpe.com/SDR/hpePublicKey2048_key1.pub > /etc/apt/trusted.gpg.d/hpePublicKey2048_key1.asc
# chmod 644 /etc/apt/trusted.gpg.d/hp*

# apt update
# apt install ssacli
~~~

> `ssacli` remplace `hpssacli`

## Récupérer les informations des disques

Récapitulatif des disques virtuel et physiques :

~~~
# ssacli ctrl all show config [detail]

Smart Array P440ar in Slot 0 (Embedded)   (sn: PDNLH0BRH8B0XM)

   Port Name: 1I

   Port Name: 2I

   Internal Drive Cage at Port 1I, Box 1, OK

   Internal Drive Cage at Port 2I, Box 0, OK
   array A (SAS, Unused Space: 0  MB)

      logicaldrive 1 (558.7 GB, RAID 1+0, OK)

      physicaldrive 1I:1:1 (port 1I:box 1:bay 1, SAS, 300 GB, OK)
      physicaldrive 1I:1:2 (port 1I:box 1:bay 2, SAS, 300 GB, Failed)
      physicaldrive 1I:1:3 (port 1I:box 1:bay 3, SAS, 300 GB, OK)
      physicaldrive 1I:1:4 (port 1I:box 1:bay 4, SAS, 300 GB, OK)
~~~

Pour récupérer les informations sur le tableau RAID, on commence d'abord par connaître le n° de tableau dans lequel se trouve notre disque

On voit bien le tabeau RAID avec l'ID 0, et deux disques physiques qui composent notre disque logique, dont l'un des deux est noté *Failed*

Ensuite, pour trouver le n° de série du disque, on prend sont ID de disque physique (1I:1:2) et on demande ses informations dans le tabeau RAID 0

~~~
# ssacli controller slot=0 physicaldrive 1I:1:2 show

Smart Array P440ar in Slot 0 (Embedded)

   array A

      physicaldrive 1I:1:2
         Port: 1I
         Box: 1
         Bay: 2
         Status: OK
         Drive Type: Data Drive
         Interface Type: SAS
         Size: 300 GB
         Drive exposed to OS: False
         Native Block Size: 512
         Rotational Speed: 15000
         Firmware Revision: HPD2
         Serial Number: 0TG8T4GP
         Model: HP      EH0300JDYTH
         Current Temperature (C): 41
         Maximum Temperature (C): 52
         PHY Count: 2
         PHY Transfer Rate: 12.0Gbps, Unknown
         Drive Authentication Status: OK
         Carrier Application Version: 11
         Carrier Bootloader Version: 6
         Sanitize Erase Supported: False
~~~

L'outil `ssacli` est le résultat du renommage de `hpacucli`, qui fonctionne avec les mêmes options.

## CCISS ?

Sur les serveurs HP, il y a souvent le démon `cciss-vol-statusd` qui tourne et qui envoie des notifications lorsque le RAID est défaillant.

## Créer un nouveau RAID

Si on ajoute des nouveaux disques à chaud, il est possible de créer un ensemble RAID sans redémarrer le serveur.

D'abord on liste l'état des disques. Dans notre exemple il y a déjà un RAID1 de 2x 480 Go et un RAID5 de 4x 300 Go. On a rajouté 2 disques de 1,8 To pour former un nouveau RAID1.

~~~
# ssacli ctrl all show config

Smart Array P440ar in Slot 0 (Embedded)   (sn: PDNLH0BRH839G7)

   Internal Drive Cage at Port 1I, Box 1, OK

   Internal Drive Cage at Port 2I, Box 1, OK

   Port Name: 1I

   Port Name: 2I

   Array A (Solid State SATA, Unused Space: 0  MB)

      logicaldrive 1 (447.10 GB, RAID 1, OK)

      physicaldrive 1I:1:1 (port 1I:box 1:bay 1, SATA SSD, 480 GB, OK)
      physicaldrive 1I:1:2 (port 1I:box 1:bay 2, SATA SSD, 480 GB, OK)

   Array B (SAS, Unused Space: 0  MB)

      logicaldrive 2 (838.10 GB, RAID 5, OK)

      physicaldrive 1I:1:3 (port 1I:box 1:bay 3, SAS HDD, 300 GB, OK)
      physicaldrive 1I:1:4 (port 1I:box 1:bay 4, SAS HDD, 300 GB, OK)
      physicaldrive 2I:1:5 (port 2I:box 1:bay 5, SAS HDD, 300 GB, OK)
      physicaldrive 2I:1:6 (port 2I:box 1:bay 6, SAS HDD, 300 GB, OK)

   Unassigned

      physicaldrive 2I:1:7 (port 2I:box 1:bay 7, SAS HDD, 1.8 TB, OK)
      physicaldrive 2I:1:8 (port 2I:box 1:bay 8, SAS HDD, 1.8 TB, OK)
~~~

On voit bien nos 2 disques non assignés.

On crée ensuite un RAID1 en ciblant spécifiquement ces disques :

~~~
# ssacli ctrl slot=0 create type=ld drives=2I:1:7,2I:1:8 raid=1
~~~

On peut vérifier ensuite que tout est OK :

~~~
# ssacli ctrl all show config

Smart Array P440ar in Slot 0 (Embedded)   (sn: PDNLH0BRH839G7)

   Internal Drive Cage at Port 1I, Box 1, OK

   Internal Drive Cage at Port 2I, Box 1, OK

   Port Name: 1I

   Port Name: 2I

   Array A (Solid State SATA, Unused Space: 0  MB)

      logicaldrive 1 (447.10 GB, RAID 1, OK)

      physicaldrive 1I:1:1 (port 1I:box 1:bay 1, SATA SSD, 480 GB, OK)
      physicaldrive 1I:1:2 (port 1I:box 1:bay 2, SATA SSD, 480 GB, OK)

   Array B (SAS, Unused Space: 0  MB)

      logicaldrive 2 (838.10 GB, RAID 5, OK)

      physicaldrive 1I:1:3 (port 1I:box 1:bay 3, SAS HDD, 300 GB, OK)
      physicaldrive 1I:1:4 (port 1I:box 1:bay 4, SAS HDD, 300 GB, OK)
      physicaldrive 2I:1:5 (port 2I:box 1:bay 5, SAS HDD, 300 GB, OK)
      physicaldrive 2I:1:6 (port 2I:box 1:bay 6, SAS HDD, 300 GB, OK)

   Array C (SAS, Unused Space: 0  MB)

      logicaldrive 3 (1.64 TB, RAID 1, OK)

      physicaldrive 2I:1:7 (port 2I:box 1:bay 7, SAS HDD, 1.8 TB, OK)
      physicaldrive 2I:1:8 (port 2I:box 1:bay 8, SAS HDD, 1.8 TB, OK)
~~~