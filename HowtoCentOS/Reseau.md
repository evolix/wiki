**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Réseau sous CentOS

<http://www.centos.org/docs/5/html/Deployment_Guide-en-US/pt-network-related-config.html>

## Mode simple

Avec une seule interface _eth0_, cela se passe dans le fichier _/etc/sysconfig/network-scripts/ifcfg-eth0_ :

~~~
DEVICE=eth0
TYPE=Ethernet
BOOTPROTO=none
ONBOOT=yes
IPADDR=192.0.2.51
NETMASK=255.255.255.0
GATEWAY=192.0.2.254
ETHTOOL_OPTS="autoneg off speed 100 duplex full"
~~~

Certaines directives globales comme le gateway, le hostname peuvent être mise dans _/etc/sysconfig/network_

~~~
NETWORKING=yes
NETWORKING_IPV6=yes
HOSTNAME=foo.example.com
GATEWAY=192.0.2.254
~~~

Pour gérer l'activation de l'interface :

~~~
# ifdown eth0
# ifup eth0
~~~

/!\ Attention il semblerait que CentOS garde l'ancienne IP si vous faites un changement d'adresse IP /!\

Vérifiez avec ` ip addr ` la présence de l'ancienne adresse IP et supprimer-la avec ` ip del @IP `.
