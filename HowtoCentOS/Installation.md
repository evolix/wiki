## Gestion des packages

Mise-à-jour des paquets :

~~~
# yum update
~~~

Ajouter le repository _rpmforge_ pour avoir des paquets supplémentaires (Munin, Nagios, etc.) :

~~~
# wget <http://packages.sw.be/rpmforge-release/rpmforge-release-0.5.1-1.el5.rf.x86_64.rpm>
# rpm -Uvh rpmforge-release-0.5.1-1.el5.rf.x86_64.rpm
~~~

Pour builder des paquets sources RPM :

~~~
# yum install rpm-build
# rpmbuild --rebuild *.src.rpm
~~~

Voir <http://wiki.centos.org/HowTos/SetupRpmBuildEnvironment>

## Ajout du dépôt Software Collections

Depuis Mars 2018, RedHat a mis a disposition un nouveau dépôt pour CentOS, qui reprend une sélection de paquets du dépôt COPR, les informations sur les paquets et sur ce dépôt sont accessibles a cette adresse : <https://www.softwarecollections.org/en/>

Pour avoir la liste des paquets disponibles et faire une recherche : <https://www.softwarecollections.org/en/scls/>

Ce dépôt permet d'installer des paquets plus récents que ce que les dépôts de CentOS proposent par défaut.

Pour installer ce dépôt, il faut installer le paquet *centos-release-scl* 

~~~
# yum install centos-release-scl
~~~

Installer aussi le paquet *yum-plugin-priorities* qui permet de gérer les priorités des différents dépôts, si ce n'est pas déjà fait 

~~~
# yum install yum-plugin-priorities
~~~

Pour rappel, plus le chiffre de priorité est haut, plus la priorité du dépôt est faible.

Le paquet *centos-release-scl* ajoute deux fichiers qu'il faudra activer en passant la directive *enabled=1*

* /etc/yum.repos.d/CentOS-SCLo-scl.repo
* /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo

Voici le contenu, pour exemple, du fichier */etc/yum.repos.d/CentOS-SCLo-scl.repo* :

~~~
#additional packages that may be useful
[extras]
name=CentOS-$releasever - Extras
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=extras&infra=$infra
#baseurl=http://mirror.centos.org/centos/$releasever/extras/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
enabled=1
priority=12
~~~

### Installation d'un paquet depuis Software Collections

Pour l'exemple, nous allons installer la dernière version de mysql, à savoir MySQL 5.7

Le nom des paquets en provenance de Software Collection commence toujours par rh-*, suivi du nom du paquet, pour MySQL 5.7 :

~~~
# yum install rh-mysql57
~~~

Une fois le paquet installé, il n'est pas encore *actif*, il n'est pas vu par le système, comme si le paquet n'était pas installé, il faut l'activer avec la commande  *scl enable* :

~~~
# scl enable rh-mysql57 bash
~~~

**A ce moment de l'installation, MySQL 5.7 est utilisable comme n'importe quelle autre application du système**

Pour la démarrer :

~~~
# systemctl start rh-mysql57-mysqld
~~~

Pour voir la liste de toutes les dépendances et des composants du paquet que l'on vient d'installé :

~~~
# yum list rh-mysql57\*
~~~