CrowdSec est une solution de remédiation ayant pour objectif la détection et le blocage  d'actes malicieux sur le réseau. L'agent permet typiquement la détection et remediation d'attaques telles que DDOS, Bruteforce détectable au niveau 3 mais aussi également d'attaques de niveau 7 telles que le bruteforce de la console admin WordPress par exemple.
Globalement l'utilisation de CrowdSec est possible dès lors que des logs sont générés pour l'application concernée.

[Documentation Officielle](https://docs.crowdsec.net/docs/intro/)

# Installation 

CrowdSec ne maintient la documentation remontant à la version du paquet Debian. Il faut donc installer une version au moins 3 mineures derrière la version actuelle. _(1.6.0 actuelle => Minimum supporté par la documentation: 1.3.x latest bugfix release)_

Pour importer les repos CrowdSec propose un handy script à utiliser comme suit : 

```bash
### dépendance gpg curl

# apt-get install -y debian-archive-keyring
# apt-get update


# pour apt -v supérieure à 1.1

# install -d -m 0755 "/etc/apt/keyrings"
# curl -fsSL "https://packagecloud.io/crowdsec/crowdsec/gpgkey" | gpg --dearmor >"/etc/apt/keyrings/crowdsec_crowdsec-archive-keyring.gpg"
# chmod 0644 "/etc/apt/keyrings/crowdsec_crowdsec-archive-keyring.gpg"

# echo "deb [signed-by=/etc/apt/keyrings/crowdsec_crowdsec-archive-keyring.gpg] https://packagecloud.io/crowdsec/crowdsec/any/ any main" >"/etc/apt/sources.list.d/crowdsec_crowdsec.list"
# echo "deb-src [signed-by=/etc/apt/keyrings/crowdsec_crowdsec-archive-keyring.gpg] https://packagecloud.io/crowdsec/crowdsec/any/ any main" >>"/etc/apt/sources.list.d/crowdsec_crowdsec.list"
# apt-get update

```

Dans le cas d'une vieille machine : 

```bash
# si apt -v inférieure à 1.1
# curl -fsSL "https://packagecloud.io/crowdsec/crowdsec/gpgkey" | gpg --dearmor >"/etc/apt/trusted.gpg.d/crowdsec_crowdsec.gpg"
# chmod 0644 "/etc/apt/trusted.gpg.d/crowdsec_crowdsec.gpg"
```

Vérification de l'installation :

```bash
# cscli version
```

Activation du service : 

```bash
# systemctl enable --now crowdsec
# systemctl status crowdsec

● crowdsec.service - Crowdsec agent
     Loaded: loaded (/lib/systemd/system/crowdsec.service; enabled; preset: enabled)
     Active: active (running) since Mon 2025-01-20 12:06:53 CET; 2h 5min ago
    Process: 2139517 ExecStartPre=/usr/bin/crowdsec -c /etc/crowdsec/config.yaml -t -error (code=exited, status=0/SUCCESS)
   Main PID: 2139525 (crowdsec)
      Tasks: 9 (limit: 4683)
     Memory: 40.0M
        CPU: 4.261s
     CGroup: /system.slice/crowdsec.service
             └─2139525 /usr/bin/crowdsec -c /etc/crowdsec/config.yaml

Jan 20 12:06:49 netbox-sandbox systemd[1]: Starting crowdsec.service - Crowdsec agent...
Jan 20 12:06:53 netbox-sandbox systemd[1]: Started crowdsec.service - Crowdsec agent.

```
# Configuration et mise en place 

Par défaut Crowdsec inclus quelques fichiers de configuration garantissant un fonctionnement basique. 
On y retrouve notamment trois scénarios de reconnaissance d'attaque par brute-force ssh :

```bash
# cscli scenarios list

SCENARIOS
───────────────────────────────────────────────────────────────────────────────────────────────────────
 Name                                                  📦 Status    Version       Local Path
───────────────────────────────────────────────────────────────────────────────────────────────────────
 crowdsecurity/ssh-bf                         ✔️   enabled  0.3             /etc/crowdsec/scenarios/ssh-bf.yaml
 crowdsecurity/ssh-cve-2024-6387  ✔️   enabled  0.2             /etc/crowdsec/scenarios/ssh-cve-2024-6387.yaml
 crowdsecurity/ssh-slow-bf                ✔️   enabled  0.4             /etc/crowdsec/scenarios/ssh-slow-bf.yaml
───────────────────────────────────────────────────────────────────────────────────────────────────────

```

Ainsi que les parsers nécessaires à la lecture des fichiers de logs `sshd` et `syslog` et de whitelisting : 

```bash
# cscli parsers list

PARSERS
───────────────────────────────────────────────────────────────────────────────────────────────────────
 Name                                                 📦 Status    Version  Local Path
───────────────────────────────────────────────────────────────────────────────────────────────────────
 crowdsecurity/dateparse-enrich      ✔️   enabled  0.2      /etc/crowdsec/parsers/s02-enrich/dateparse-enrich.yaml
 crowdsecurity/geoip-enrich              ✔️   enabled  0.5      /etc/crowdsec/parsers/s02-enrich/geoip-enrich.yaml
 crowdsecurity/sshd-logs                   ✔️   enabled  2.8      /etc/crowdsec/parsers/s01-parse/sshd-logs.yaml
 crowdsecurity/syslog-logs                ✔️   enabled  0.8      /etc/crowdsec/parsers/s00-raw/syslog-logs.yaml
 crowdsecurity/whitelists                    ✔️   enabled  0.2      /etc/crowdsec/parsers/s02-enrich/whitelists.yaml
───────────────────────────────────────────────────────────────────────────────────────────────────────

```

## Installation d'une collection

Une collection est un ensemble de parsers et scénarios visant à la détections d'événements spécifiques identifiés comme pouvant représenter un risque pour l'infrastructure.

Crowdsec maintient un [hub](https://app.crowdsec.net/hub?filters=search%3Dapache) libre d'accès et nourri par la communauté, depuis lequel il est facile d'installer de nouveaux modules de détection/prévention directement depuis le terminal :  

```bash 
### exemple d'installation d'une collection ciblant les logs Apache2

# cscli collection install apache2
[...]
# systemctl reload crowdsec

### ---
# cscli  collection list 
# cscli  scenario list 
```

**Attention à bien noter que les scénarios installés sont tous actifs par défaut, si besoin de désactiver un scénario il faut le supprimer :**

```bash
# cscli scenario remove <provider/scenario>
# systemctl reload crowdsec
```

Si le scénario en question appartient à une collection, il faut forcer la suppréssion :

```bash
# cscli scenario remove <provider/scenario> --force
# systemctl reload crowdsec
```

Si besoin de désactiver la remédiation sur les Alertes levées par un scénario spécifique (typiquement debug de scénario). Pour se faire, passer à `false` le contenu de la variable `labels.remediation` dans le fichier `.yaml` descriptif du scénario en question (de path `/etc/crowdsec/scenarios/<scenario_name>`).

## Installation d'un module de remédiation

Aussi appelés "Bouncers", les modules de remédiation sont des exécutables (scripts, binaires etc), prenant action directement depuis la liste de décisions prises de Crowdsec; typiquement, l'ajout d'adresses IPs dans des IPsets, de règles firewall etc.

Les Bouncers sont maintenus et packagés directement par CrowdSec, pour les installer : 

```cscli
# apt install  crowdsec-firewall-bouncer-iptables
# apt install  crowdsec-firewall-bouncer-nftables
# apt install  crowdsec-cloudflare-bouncer
# apt install  crowdsec-nginx-bouncer
```

Une fois l'installation effectuée, le bouncer est automatiquement enrôlé dans crowdsec et sa clé d'API locale est générée et ajoutée automatiquement dans le fichier `/etc/crowdsec/bouncers/<nom_bouncer>.yaml` dans la variable `api_key`.

Activer les bouncers installés :

```bash
# systemctl enable --now crowdsec-firewall-bouncer-nftables
# systemctl status crowdsec-firewall-bouncer-nftables
● crowdsec-firewall-bouncer.service - The firewall bouncer for CrowdSec
     Loaded: loaded (/etc/systemd/system/crowdsec-firewall-bouncer.service; enabled; preset: enabled)
     Active: active (running) since Mon 2025-01-20 15:23:41 CET; 20min ago
    Process: 2155699 ExecStartPre=/usr/bin/crowdsec-firewall-bouncer -c /etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml -t (code=exited, status=0/SUCCESS)
    Process: 2155714 ExecStartPost=/bin/sleep 0.1 (code=exited, status=0/SUCCESS)
   Main PID: 2155706 (crowdsec-firewa)
      Tasks: 10 (limit: 4683)
     Memory: 23.7M
        CPU: 355ms
     CGroup: /system.slice/crowdsec-firewall-bouncer.service
             └─2155706 /usr/bin/crowdsec-firewall-bouncer -c /etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml

Jan 20 15:23:40 netbox-sandbox systemd[1]: Starting crowdsec-firewall-bouncer.service - The firewall bouncer for CrowdSec...
Jan 20 15:23:41 netbox-sandbox systemd[1]: Started crowdsec-firewall-bouncer.service - The firewall bouncer for CrowdSec.

```

## Configuration de crowdsec en mode centralisé

Pour que l'API locale de crowdsec soit centralisée, il faut plusieurs chose: 

1. Configurer l'API pour écouter sur l'IP concernée et non `127.0.0.1`
2. Configurer la console pour pointer correctement vers l'API locale
3. Configurer le Bouncer distant pour que ce dernier se connecte à l'API locale


**1.**  Pour configurer l'IP d'écoute de l'API, modifier `/etc/crowdsec/config.yaml`, particulièrement la variable `api.server.listen_uri ` au format `<IP>:<PORT>`.

**2.**  Pour configurer la console et faire pointer celle-ci vers l'API locale, modifier `/etc/crowdsec/local_api_credentials.yaml`, particulièrement la variable `url` au format `http://<IP>:PORT/`.

**3.**  Pour configurer l'IP d'écoute de l'API, **sur la machine Satellite** modifier `/etc/crowdsec/bouncers/<bouncer_name>.yaml`, particulièrement la variable `api_url`, au format `http://<IP>:<PORT>/`

## Configuration d'un bouncer en mode satellite

De la même façon que 

## Configuration d'une machine en mode satellite
# Usage commun

## Gestion des décisions et alertes

Lister les décisions et prises 

```bash
$ sudo cscli decisions list
$ sudo cscli alerts list
```

Ajouter un décision manuellement concernant une adresse IP spécifique pour une durée de 2h avec le commentaire "DDOS atempt"

```bash
$ sudo cscli decisions add --ip <IP_> --duration 2h --reason "DDOS atempt"
```

Ajouter un décision manuellement concernant un subnet (notation CIDR) spécifique pour une durée de 2h avec le commentaire "DDOS atempt"

```bash
$ sudo cscli decisions add --range <CIDR_RANGE> --duration 2h --reason "DDOS atempt"
```

Supprimer une décision 

```bash
$ sudo cscli decisions delete --{ip|range} <{IP|CIDR_RANGE}>
```
# Configuration avancée
## Les Parsers

Le parser est un module dont la tâche est de lire et interpreter les logs des services à surveiller, puis remplir une structure de donnée dite traversante en faisant suivre toutes cette dernière au prochain module d'execution.

Les parsers s'organisent selon la structure en place dans /etc/crowdsec/parsers

```
/etc/crowdsec/parsers/
├── s00-raw
│   └── syslog-logs.yaml -> /etc/crowdsec/hub/parsers/s00-raw/crowdsecurity/syslog-logs.yaml
├── s01-parse
│   ├── nginx-logs.yaml -> /etc/crowdsec/hub/parsers/s01-parse/crowdsecurity/nginx-logs.yaml
│   └── sshd-logs.yaml -> /etc/crowdsec/hub/parsers/s01-parse/crowdsecurity/sshd-logs.yaml
└── s02-enrich
    ├── dateparse-enrich.yaml -> /etc/crowdsec/hub/parsers/s02-enrich/crowdsecurity/dateparse-enrich.yaml
    ├── geoip-enrich.yaml -> /etc/crowdsec/hub/parsers/s02-enrich/crowdsecurity/geoip-enrich.yaml
    └── whitelists.yaml -> /etc/crowdsec/hub/parsers/s02-enrich/crowdsecurity/whitelists.yaml
```

La chaîne d'execution suit un ordre décroissant de priorité.

> *ici s00-raw en premier puis s01-parse etc*

### s00-raw

Premier stage de parsing, ce niveau est destiné au premier filtrage des logs et particulièrement à determiner s'il s'agit de logs gérés par syslog ou non.
Si oui alors les premières donnés sont remplies dans la structure de donnée traversante.

Le parser commence par effectuer une recherche/matching à l'aide de chaînes *GROK*.
GROK est un superset des chaines REGEXP que l'on connaît, ajoutant simplement un niveau d'abstraction en nommant certaines structures connues : 

Un filtre GROK : 
```grok
^<\%{NUMBER:stuff1}>\%{NUMBER:stuff2} \%{SYSLOGBASE2} \%{DATA:program} \%{NUMBER:pid}'
```

C'est donc simplement l'attribution à des variables le contenu des matchs d'une macro REGEX : 

```grok
%{<REGEX_MACRO>:variable}
```

Des variables sont donc remplies  à l'aide des données récupérées par les filtres, puis sont stockées dans la structure de donnée traversante à l'aide du sous-dictionnaire `static`:


``` yaml
#/etc/crowdsec/parsers/s00-raw/syslog-logs.yaml
#---------------------------------------------------
[...]
statics:
  - meta: machine
    expression: evt.Parsed.logsource
  - parsed: "logsource"
    value: "syslog"
# syslog date can be in two different fields (one of hte assignment will fail)
  - target: evt.StrTime
    expression: evt.Parsed.timestamp
  - target: evt.StrTime
    expression: evt.Parsed.timestamp8601
  - meta: datasource_path
    expression: evt.Line.Src
  - meta: datasource_type
    expression: evt.Line.Module
[...]
```

Ici on peut voir l'attribution de champs de la structure YAML à travers l'objet `evt`
## Les Scenarios
TODO
## Les Bouncers
### Bouncer firewall

Le _bouncer-firewall_ manipule directement les règles firewall de son instance. Il existe deux modes de fonctionnement et deux _backends_ possibles : 

* **IPtables** : Permet la gestion des regles pare-feu d'une machine. Ce mode existe principalement dans le but de maintenir un compatibilité "legacy" avec l'outil iptables.
* **NFtables**: Permet l'utilisation du logiciel _nftables_ successeur directe de _iptables_.

Parallèlement à cela, les modes de fonctionnement sont les suivants : 

* **Management** : Le fonctionnement par défaut du bouncer; ce mode donne carte blanche au bouncer en ce qui concerne la gestion des règles et des chaines.  De fait, des opérations telles que le flush de toutes les regles présentes dans une chaine sont éffectuées au démarrage du bouncer. Ce mode permet un usage basique et le bouncer est ainsi fonctionnel sans configuration particulière préalable.

* **IPSet**: Les _IPset_ sont un moyen de lister un certain nombre d'objets dans une collection. Ils permettent notamment la déclaration d'une seule règles valable pour tous les objets du set (utile dans le cas d'un ban sur plusieurs IPs ou ports par exemple). IPset est initialement un outil permettant une extension de fonctionnalités d'iptables. Le concept de sets est également présent nativement dans nftables. Dans ce mode de fonctionnement, le bouncer ajoute les IPs à bannir dans l'IPset `crowdsec-blacklists` et **l'utilisateur doit créer les règles correspondantes manuellement**. De plus, les sets `crowdsec-blacklists` et `crowdsec6-blacklists` doivent être créés préalablement : 
	* `# ipset create crowdsec-blacklists hash:ip timeout 0`
	* `# ipset create crowdsec6-blacklists hash:ip timeout 0`
	
	Pour lister les IPs présentes dans un set : 
	* `# ipset list crowdsec-blacklists`
	
La configurations de ces aspects du bouncer se fait dans les fichiers :
* `/etc/crowdsec/bouncer/crowdsec-firewall-bouncer.yaml`
* `/etc/crowdsec/bouncer/crowdsec-firewall-bouncer.yaml.local`

### Création d'un Bouncer custom

Il est possible de mettre en place un module de remédiation dont les actions sont pilotées par un script ou un binaire spécifique. Pour se faire il faut mettre en place un bouncer custom : 

1. Télécharger la dernière release du binaire bouncer : [ici](https://github.com/crowdsecurity/cs-custom-bouncer/releases)
2. `$ tar xvf crowdsec-custom-bouncer.tgz && cd crowdsec-custom-bouncer-vXX.XX/ `
3. ` # ./install.sh` (à exécuter en root). 
		Le script d'installation suit ces étapes : 
			1. Génère une clé d'API pour le bouncer
			2. Génère le fichier de configuration du bouncer
			3. Remplis les informations de clé et du binaire de remédiation dans la configuration
			4. Crée une unité systemd
			5. Copie tous ces fichiers aux emplacements reconnus
1. Spécifier l'emplacement absolu du script à exécuter pour remédiation.
2. `# systemctl daemon-reload && systemctl enable --now crowdsec-custom-bouncer.service`

La configuration d'un bouncer custom vit dans `/etc/crowdsec/bouncers/crowdsec-custom-bouncer.yaml`
Il est conseillé de changer cela, et de maintenir la configuration dans `/etc/crowdsec/bouncers/crowdsec-custom-bouncer.yaml.local` car ce fichier pourrait être écrasé en cas de mise à jour.

Le script ou binaire spécifié dans la configuration sera invoqué comme suit : 

```bash
<my_custom_binary> add <value> <duration> <reason> <json_object> # to add an IP address
<my_custom_binary> del <value> <duration> <reason> <json_object> # to del an IP address
```

## Rapports réguliers via email

Il est possible d'envoyer des rapports concernant les dernières décisions prises, les alertes et les différentes métriques de CrowdSec : 

TODO:

`/etc/crowdsec/profiles.yaml`+ `/etc/crowdsec/notifications/email.yaml`

# FAQ
## Bouncer non enrôlé par apt (cscli bouncer list vide)

Si l'installation n'enrôle pas le bouncers automatiquement, il faut générer une clé api : 

```bash
# cscli bouncer add nom_bouncers
API key for 'nom_bouncers':

   Ezh61tUJZmAEVMsE0U9KiGzSD2PlKVNnkHEQyaNA7Vc

Please keep this key since you will not be able to retrieve it!
```

Puis configurer cette clé d'API dans le fichier de configuration du bouncer `/etc/crowdsec/bouncers/nom_bouncer.yaml` dans la variable `api_key`.
