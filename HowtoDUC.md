---
categories: utilities sysadmin storage
title: Howto Duc
...

* Documentation : <https://rawgit.com/zevv/duc/master/doc/duc.1.html>

[Duc](http://duc.zevv.nl) est un outil qui permet d'analyser l'espace disque en créant un index, que l'on pourra ensuite visualiser avec une interface texte ou ncurses, un graphique, une GUI ou en CGI.

# Installation

Un paquet est disponible à partir de Debian Stretch :

~~~
# apt install duc
~~~


# Utilisation

## Création de l'index

La première chose à faire est de créer un index, c'est la partie qui analyse l'espace disque pour ensuite l'afficher de différentes manières. Par défaut l'index sera créé dans `$HOME/.duc.db`, si l'on veut changer le chemin de l'index il faudra utiliser la variable d'environnement `DUC_DATABASE` ou spécifier le chemin avec `-d`.

L'index n'est pas forcément rattaché à un seul chemin, on peut en indexer autant qu'on veut :

~~~
$ duc index -Hpx -d /tmp/duc.idx /var/
$ duc index -Hpx -d /tmp/duc.idx /usr/
~~~

> *Note* : La génération de l'index peut être très longue et peut prendre de la place si vous avez beaucoup de données. Il est peut être intéressant de :
>
> * rajouter le paramètre `-p` pour suivre la progression,
> * vérifier que la partition qui héberge l’index (`/tmp` dans l’exemple ci-dessus) soit suffisamment grande,
> * exécuter la commande dans une session [Screen](/HowtoScreen) ou [Tmux](/HowtoTmux).


## Consulter l'index

### Voir les informations de l'index

~~~
$ duc info -d /tmp/duc.idx 
Date       Time       Files    Dirs    Size Path
2018-03-02 09:49:38   22.5K    2.4K    2.2G /var
2018-03-02 09:49:42  268.0K   24.1K    8.2G /usr
~~~


### Lister le contenu de l'index

~~~
$ duc ls -d /tmp/duc.idx /usr     
  4.3G lib
  3.0G share
606.2M bin
210.5M src
 66.4M include
 35.3M sbin
 20.1M local
  6.4M lib32
  2.2M libexec
  1.7M games
  4.0K etc

$ duc ls -d /tmp/duc.idx /usr/bin/ | head
 29.9M hkt
 28.3M hokey
 24.2M node
 19.6M hot
 17.0M shellcheck
 15.7M mysql_embedded
 14.7M fwbuilder
 11.6M audacity
 11.1M rawtherapee
 10.4M qemu-system-x86_64

$ duc ls -d /tmp/duc.idx /var/log | head  
197.2M atop
 30.4M daemon.log.1
 24.1M daemon.log
 17.1M installer
  5.4M syslog.1
  2.7M messages.1
  2.4M vbox-install.log
  2.1M syslog
  1.9M kern.log.1
  1.7M ufw.log.1
~~~

Pour avoir une liste basée sur le nombre d'inode plutôt que la quantité d'octet utilisé, il faut utiliser l'option `--count`.


### Lister le contenu de l'index avec une interface ncurses

L'interface ncurses est semblable à celle de l'outil [ncdu](https://dev.yorhel.nl/ncdu). La touche `h` permettra d'afficher l'aide pour comprendre comment naviguer et interagir avec cette interface.

~~~
# duc ui -d /tmp/duc.idx /usr
~~~

![Interfaces ncurses](/duc.png)


### Lister le contenu de l'index avec une interface graphique

Si on est sur un poste ayant un serveur X (ou en `ssh -X`), on pourra utiliser cette interface et utiliser la souris. Contrairement à l'interface ncurses on n'a pas d'aide en appuyant sur `h`, il faudra voir la liste des raccourcis dans le manuel.

~~~
$ duc gui -d /tmp/duc.idx /usr
~~~

![Le GUI de Duc](/duc2.png)


### Lister le contenu de l'index en générant une image

On peut générer une image PNG, utile pour l'envoyer à quelqu'un d'autre par exemple :

~~~
$ duc graph -d /tmp/duc.idx -o /tmp/duc.png -l8 -s 1920 /usr
~~~

Les paramètres intéressants sont :

- `-l` nombre de sous-répertoires à afficher ;
- `-o` chemin de sortie de l'image ;
- `-s` largeur de l'image.


### Naviguer dans les graphes d'un index Duc dans un navigateur

On appelle Duc dans un script CGI.

Créer `/usr/lib/cgi-bin/duc.cgi` contenant :

~~~
#!/bin/sh
/usr/bin/duc cgi -d /var/cache/duc.idx --list
~~~

Ajuster les droits :

~~~
# chmod 755 /usr/lib/cgi-bin/duc.cgi
# chown www-data: /usr/lib/cgi-bin/duc.cgi
# chmod 640 /var/cache/duc.idx
# chgrp www-data /var/cache/duc.idx
~~~

Configurer le vhost dans lequel on veut avoir accès aux graphes :

Cas [Apache](HowtoApache) :

Vérifier l'activation des script CGI, avec une protection d'accès :

~~~
ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory /usr/lib/cgi-bin>
    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
    Require all denied
    Include /etc/apache2/ipaddr_whitelist.conf
</Directory>
~~~

Puis à la suite, ajouter un alias pour le CGI :

~~~
ScriptAlias /duc /usr/lib/cgi-bin/duc.cgi
~~~

Cas [Nginx](HowToNginx) :

~~~
    location /duc {
        fastcgi_pass unix:/var/run/fcgiwrap.socket;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME /usr/lib/cgi-bin/duc.cgi;
    }
~~~
**TODO :** accès sécurisé (whitelist dans les snippets).

Puis, mettre en place un cron pour indexer le contenu, par exemple de `/home` :

~~~
# cat /etc/cron.d/duc
30 6  * * * root /usr/bin/ionice -c3 /usr/bin/duc index -q -d /var/cache/duc.idx /home && /bin/chmod 640 /var/cache/duc.idx && /bin/chgrp www-data /var/cache/duc.idx
~~~

Il y aura besoin du paquet `fcgiwrap` pour le socket.

## Plomberie

Quelques options utiles :

- `-H` : Compter les mêmes inodes une seule fois ;
- `-e` <regex> : Exclure les fichiers correspondant (ne correspond que au nom du fichier et non de son PATH complet) ;
- `-p` : Affiche la progression de l'indexation.
- `-x` : Comme pour la commande *du*, limite au FS analysé

~~~
# duc index -p -H -e "*.log*" -d /backup/duc.idx /backup/jails
# duc index -p -H -d /backup/duc.idx /backup/jails 
~~~

Pour exclure un répertoire (par exemple `backup/` contenant de nombreux fichiers), se placer dessus et faire :

~~~
# duc index -p -e "backup" -d duc.idx .
~~~


### Cron

Il peut être judicieux de créer un cron pour faire une indexation journalière, très pratique quand couplé avec l'interface CGI.

~~~
30 6  * * * root /usr/bin/ionice -c3 /usr/bin/duc index -q -d /var/cache/duc.idx / && /bin/chmod 640 /var/cache/duc.idx && /bin/chgrp www-data /var/cache/duc.idx
~~~
