**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowtoGitWeb

## Prérequis

Installation de gitweb et highlight :

~~~
# aptitude install gitweb highlight
~~~

### Configuration Nginx

Installation de fcgiwrapper :

~~~
# aptitude install fcgiwrap
~~~

Modifier /etc/gitweb.conf et remplacer les lignes suivantes :

~~~
$projectroot = $ENV{'GITWEB_PROJECTROOT'} || "/pub/git";
$projects_list = $ENV{'GITWEB_PROJECTLIST'} || $projectroot;
~~~

Rajout de la conf highlight :

~~~
cat >> /etc/gitweb.conf <<CONF
\$feature{'highlight'}{'default'} = [1];
CONF
~~~

## Configuration

Choix de l'utilisateur $GIT :

* $GIT : utilisateur propriétaire des dépots
* dépôts présent dans /home/$GIT/repositories
* accès git:// depuis $GIT@votre-domaine.tld

~~~
GIT='git'
~~~

Configuration du vhost :

~~~
cat > /etc/nginx/sites-available/gitweb_$GIT <<VHOST
server {
        server_name $GIT.$(hostname -d);
        listen      0.0.0.0:80;
        listen      [::]:80;
        root        /usr/share/gitweb;
        location / {
            index gitweb.cgi;
            include fastcgi_params;
            gzip off;
            fastcgi_param   GITWEB_CONFIG   /etc/gitweb.conf;
            fastcgi_param   GITWEB_PROJECTROOT  /home/$GIT/repositories;
            fastcgi_param   GITWEB_PROJECTLIST  /home/$GIT/projects.list;
            if (\$uri ~ "/gitweb.cgi") {
                fastcgi_pass    unix:/var/run/fcgiwrap.socket;
            }
        }
        access_log      /var/log/nginx/gitweb.access.log;
        error_log       /var/log/nginx/gitweb.error.log;
}
VHOST
~~~

Activation du vhost :

~~~
ln -s /etc/nginx/sites-available/gitweb_$GIT /etc/nginx/sites-enabled/gitweb_$GIT
~~~

Donne l'accès en lecture au dépôts à l'utilisateur www-data :

~~~
addgroup www-data $GIT
~~~

Redémarre le démon fcgiwrap :

~~~
service fcgiwrap restart
~~~
