---
categories: tests http
title: Howto Hurl
...

* Site/dépôt officiel : <https://hurl.dev/>

**Hurl** est un outil en ligne de commande qui exécute des requêtes HTTP définies dans un format simple et lisible.
Il peut enchaîner les requêtes, capturer les valeurs d'en-têtes et du corps de la réponse.
Il peut récupérer des données, tester des sessions HTTP ainsi que des API JSON ou XML.
Il utilise la `libcurl` comme moteur de requêtes HTTP.

Hurl est développé en Rust, par [l'équipe « Open Source » de l'opérateur Orange](https://github.com/Orange-OpenSource/hurl/).

## Installation

Il n'y a pas de paquet dans les dépôts officiels de Debian.

On peut télécharger manuellement un paquet ou bien récupérer un binaire pré-compilé.

### Paquet Debian

~~~
# curl -LO https://github.com/Orange-OpenSource/hurl/releases/download/1.8.0/hurl_1.8.0_amd64.deb
# dpkg -i hurl_1.8.0_amd64.deb

$ hurl --version
hurl 1.8.0 libcurl/7.74.0 OpenSSL/1.1.1n zlib/1.2.11 brotli/1.0.9 libidn2/2.3.0 libssh2/1.9.0 nghttp2/1.43.0
Features (libcurl):  alt-svc AsynchDNS brotli HTTP2 IDN IPv6 Largefile libz NTLM NTLM_WB SPNEGO SSL TLS-SRP UnixSockets
Features (built-in): brotli
~~~

## Utilisation

On crée un fichier par site dans `tests/monsite.hurl` :

~~~
# Redirects

GET http://example.com
HTTP/* 301
Location: https://example.com/

GET http://www.example.com
HTTP/* 301
Location: https://example.com/

GET http://example.com/somewhere
HTTP/* *
Location: https://example.com/somewhere-else
[Asserts]
status >= 300
status < 400

# Cache headers

## 1st request, warm-up

GET https://example.com
HTTP/2 200
x-cacheable: TRUE
strict-transport-security: max-age=63072000

## 2nd request, cache should hit

GET https://example.com
HTTP/2 200

[Asserts]
header "x-cache" == "HIT"
header "age" exists
header "age" != "0"
~~~

NB : des simplifications de syntaxe sont [en cours de préparation](https://github.com/Orange-OpenSource/hurl/issues?q=is%3Aissue+author%3Ajlecour+).

On joue la commande `$ hurl --test tests/monsite.hurl` pour un seul site ou `$ hurl --test tests/*.hurl` pour tous les sites à la suite.

Codes de retour possibles :

* `0` : toutes les assertions sont satisfaites ;
* `1` : erreur d'option sur la ligne de commande ;
* `2` : erreur de lecture/analyse d'un fichier d'entrée ;
* `3` : erreur d'exécution (timeout de connexion…) ;
* `4` : erreur sur une ou plusieurs assertions non satisfaites.

Avec l'option `--verbose` on peut avoir une sortie détaillée.