---
title: Howto mtree
...

* Documentation : <http://cdn.netbsd.org/pub/pkgsrc/current/pkgsrc/pkgtools/mtree/README.html>

L'utilitaire `mtree` compare une hiérarchie de fichier (sur la base d'un emplacement) avec une spécification fournie (entrée standard ou fichier). Il est souvent utilisé pour vérifier l'intégrité d'un système de fichiers.

## Installation

~~~
# apt install mtree-netbsd
~~~

## Usage

Pour analyser /home et stocker le résultat dans un fichier :

~~~
# mtree -x -c -p /home > /tmp/home.mtree-spec
~~~

Pour comparer plus tard :

~~~
# mtree -f /tmp/home.mtree-spec -p /home
~~~

Il est possible d'indiquer des meta-données à ignorer au moment de la comparaison.