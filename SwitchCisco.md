---
categories: network
title: Howto SwitchCisco
...

Nous utilisons principalement des [switchs Cisco](https://www.cisco.com/c/fr_fr/products/switches/index.html) Catalyst 2960/3750, des Cisco Nexus 3000, et quelques Cisco Small Business. Cette documentation sera orientée pour ces modèles.

**ℹ️ Note : historiquement, nous utilisions principalement des Catalyst 2960, c'est donc le modèle contenant la documentation la plus complète. Les commandes non indiquées pour les autres modèles sont généralement les mêmes que celles indiquées pour les Catalyst 2960. ℹ️**

Ressources diverses :

* [Cisco Software Checker : informations de sécurité sur les logiciels Cisco](https://sec.cloudapps.cisco.com/security/center/softwarechecker.x)
* [Liste des firmwares IOS](https://www.cisco.com/c/en/us/support/ios-nx-os-software/index.html) ;
* [Centre de téléchargement cisco](https://software.cisco.com/download/home) ;
* [Manuels Catalyst 2960/2960-S](https://www.cisco.com/c/en/us/support/switches/catalyst-2960-series-switches/products-installation-and-configuration-guides-list.html) ;
* [Data Sheet Catalyst 3750](http://www.cisco.com/en/US/prod/collateral/switches/ps5718/ps5023/product_data_sheet0900aecd80371991.html) ;
* [Manuels Nexus 3000](https://www.cisco.com/c/en/us/support/switches/nexus-3000-series-switches/products-installation-and-configuration-guides-list.html) ;
* [Troubleshooting](http://www.cisco.com/en/US/products/hw/switches/ps708/products_tech_note09186a008015bfd6.shtml) ;
* [LED 2960S](https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst2960/hardware/installation/guide/2960_hg/higover.html#39834).


## Cisco Catalyst 2960

Exemples de numéros de modèles : `2960`, `2960X`…

### Configuration initiale

#### Avec un port série (old-school)

* Raccorder le port d'administration du switch au port série d'un poste Linux à l'aide du câble fourni (couleur bleu ciel)
* Installer `minicom` et créer le fichier de configuration `/etc/minicom/minirc.cisco` avec un contenu du type :

~~~
pu port             /dev/ttyS0
pu baudrate         9600
pu bits             8
pu parity           N
pu stopbits         1
pu rtscts           No
~~~

> *Note* : Avec un adaptateur USB, le device est /dev/ttyUSB0 (ou autre numéro).

* Exécuter la commande `minicom cisco` et observer l'initialisation du switch. À la question `Would you like to enter the initial configuration dialog?`, répondre `no`
* Passer en mode administrateur avec la commande `enable`, et afficher la configuration par défaut avec `show running-config`


#### Avec un câble Ethernet

Pour un switch récent, une fois allumé pour la 1ère fois et les voyants _SYST_, _MAST_ et _STAT_ verts, on appuie 3 à 4 secondes sur le bouton _MODE_ ce qui le fait passer en mode _Express Setup_.
Note : cela ne fonctionne que si le switch n'a jamais été configuré.

On branche un câble sur n'importe quel port et on lance un client DHCP sur un ordinateur :

~~~
# dhclient -d eth0
Internet Systems Consortium DHCP Client 4.1.1-P1
Copyright 2004-2010 Internet Systems Consortium.
All rights reserved.
For info, please visit <https://www.isc.org/software/dhcp/>

Listening on LPF/eth0/f0:de:f1:12:c9:d2
Sending on   LPF/eth0/f0:de:f1:12:c9:d2
Sending on   Socket/fallback
DHCPREQUEST on eth0 to 255.255.255.255 port 67
DHCPNAK from 10.0.2.3
DHCPDISCOVER on eth0 to 255.255.255.255 port 67 interval 5
DHCPOFFER from 10.0.2.3
DHCPREQUEST on eth0 to 255.255.255.255 port 67
DHCPACK from 10.0.2.3
bound to 10.0.2.2 -- renewal in 271 seconds.
~~~

On obtient ainsi l'adresse IP actuelle du switch : 10.0.2.3 et l'on peut se connecter en telnet sans mot de passe, ou en HTTP avec cisco/cisco.
Le plus simple est a priori de le faire en HTTP où l'on renseigne une configuration IP de base et les mots de passe, et l'on sort du mode
_Express Setup_ une fois terminé.


### Modes d'exécution des commandes

Il existe différents niveaux de mode dans lesquels différentes commandes peuvent s'exécuter. Parmi les plus communs :

~~~
Switch>
Switch#
Switch(config)#
Switch(config-if)#
~~~

Le premier est le mode d'exécution avec des droits minimums où l'on ne peut obtenir aucune information sensible.
On passe dans le mode d'exécution privilégié avec `Switch> enable`. Dans ce mode, on ne peut rien configurer, mais c'est dans celui-ci que l'on pourra exécuter les commandes `show` permettant d'examiner les configurations en place. Avec `Switch# configure terminal`, on arrive dans le mode de configuration global. Enfin, `Switch(config)# interface XXX` nous mettra dans le mode de configuration d'interface.

On peut descendre d'un niveau avec la commande `exit`, ou redescendre directement vers le mode d'exécution privilégié avec la commande `end`.

Les commandes `show` ne peuvent s'exécuter que dans le mode d'exécution privilégié, mais peuvent être exécutés depuis un mode plus élevé en débutant la commande avec `do` :

~~~
Switch(config)# do show running-config
~~~


### Lister les commandes disponibles

Toutes les commandes ne peuvent pas être exécutées dans n’importe quel mode.

À tout moment, si on veut savoir quelle commande peut être exécutée ou quel mot-clé peut compléter la commande courante, il suffit d’écrire `?`, et les différentes possibilités s’affichent.

~~~
SW# ?
  attach                 Connect to a specific linecard
  bcm-shell              Bcm shell/cmd
  blink                  Blink locator led
  bloggerd               Blogger commands
  callhome               Callhome commands
  cd                     Change current directory
[…]
~~~

Cela permet aussi de lister les sous-commandes :

~~~
SW# configure ?
  <CR>             
  config-template  Configuration-template
  maintenance      Maintenance profile mode
  profile          Configure profile
  replace          Perform a replace of the running-config
  session          Configure the system in a session
  sync             Configure the system in config-sync mode
  terminal         Configure the system from terminal input
~~~


### Simple Network Management Protocol  (SNMP)

Les switchs peuvent aussi être configurés de manière non-interactive avec le [protocole SNMP](https://fr.wikipedia.org/wiki/Simple_Network_Management_Protocol).

Voir les sous-sections dédiés à chaque switch.


### Commandes de base


#### Commandes de bases sous IOS

~~~
Switch# show version
Switch# show mem
Switch# show processes
Switch# show process cpu
Switch# show flash
Switch# show clock
Switch# show history
Switch# show logging
Switch# show inventory
Switch# show interface
Switch# show interface status
Switch# show interface st
Switch# show interface counter
Switch# show interface counter errors
Switch# show interface Gi0/11
Switch# show interface Gi0/11 status
Switch# show interface Gi0/11 counter
Switch# show interface Gi0/11 counter errors 
Switch# show interface trunk
Switch# show interface description
~~~

Note : Pour les Catalyst, la commande d'origine est `show interfaces` avec un *s* et non `show interface`. Mais étant donné que la commande pour les Nexus est `show interface` sans *s* et que la commande sans *s* est comprise par les Catalyst, on la documente ainsi pour simplifier le copié/collé.

Supprimer une configuration ou un état :

~~~
Switch# no <COMMAND>
~~~


#### Gestion de base de la configuration

Voir la configuration en Flash :

~~~
Switch# show startup-config
~~~

Voir la configuration actuelle :

~~~
Switch# show running-config
~~~

Configuration actuelle d'une interface particulière :

~~~
Switch# show running-config interface GigabitEthernet1/0/1
~~~

Ecrire/sauvegarder/enregistrer la configuration actuelle en Flash :

~~~
Switch# copy running-config startup-config
Destination filename [startup-config]? 
Building configuration...
[OK]
0 bytes copied in 1.394 secs (0 bytes/sec)
~~~

Note : la commande `write` est également possible, mais dépréciée.


#### Changer le nom d'hôte

~~~
Switch# configure terminal
Switch(config)# hostname sw-test
sw-test(config)# end
~~~

#### Changer le nom de domaine

~~~
Switch# conf t
Switch(config)# ip domain-name test.com
Switch(config)# end
~~~

#### Chiffrer les mots de passe

~~~
Switch# configure terminal
Switch(config)# service password-encryption
~~~

Avec cette commande, les mots de passes définis avec le mot-clef `password` ne seront pas en clair.

Attention : ces mots de passe ne sont pas sécurisés et sont déchiffrables par [Cisco Password Cracker](http://www.ifm.net.nz/cookbooks/passwordcracker.html). Il ne faut donc pas utiliser le mot clef `password` et préférer `secret` lorsque cela est possible : `enable secret PASSWORD` au lieu de `enable password PASSWORD`, et ne pas utiliser `password PASSWORD` pour les connexions telnet, ssh ou console. Voir [SwitchCisco#mise-en-place-dun-compte-utilisateur]()

Il faut également utiliser un mot de passe fort, puisque les mots de passes chiffrés par secret sont hashés en MD5 et déchiffrables avec [Cisco IOS Enable Secret Type 5 Password Cracker](http://www.ifm.net.nz/cookbooks/cisco-ios-enable-secret-password-cracker.html)

#### Mise en place d'un compte utilisateur

~~~
Switch(config)# username jdoe privilege 15 secret PASSWORD
~~~

Les privilèges vont de 0 à 15.

Par défaut, les privilèges 0 et 1 connectent l'utilisateur en mode user (prompt `Switch>`) où `enable` est nécessaire pour passer dans le mode supérieur. Les privilèges 2 à 15 connectent directement l'utilisateur dans le mode supérieur (prompt `Switch#`).

#### Mise en place d'un mot de passe "enable"

~~~
Switch# configure terminal
Switch(config)# enable secret PASSWORD
Switch(config)# end
~~~

Note : si besoin de désactiver un ancien mot de passe, il peut être nécessaire de faire `no enable secret`

#### Mise en place d'un mot de passe console

Important : Mettre en place le [compte utilisateur](SwitchCisco#mise-en-place-dun-compte-utilisateur).

~~~
Switch# configure terminal
Switch(config)# line console 0
Switch(config-line)# login local
Switch(config-line)# end
~~~

Avec `login local`, on utilisera un compte local créé par la commande `username`, alors qu'avec `login`, aucun compte n'est utilisé et le mot de passe doit être défini avec la commande `password PASSWORD`. Ces dernières ne doivent pas être utilisées, étant donné que le mot de passe ne serait pas sécurisé.

#### Infos/gestion d'une interface


Voir l'état et la vitesse de tous les ports :

~~~
Switch# show interface status
~~~

Statut d'une interface :

~~~
Switch# show interface GigabitEthernet1/0/48
~~~

Infos détaillées sur la config d'un port :

~~~
Switch# show interface GigabitEthernet1/0/48 switchport
~~~

Désactiver/activer une interface :

~~~
Switch# conf t
Switch# interface GigabitEthernet1/0/48
Switch# shutdown
Switch# no shutdown
Switch# exit
~~~

Forcer la vitesse du port :

~~~
Switch# conf t
Switch# interface GigabitEthernet1/0/48
Switch# speed {10,100,1000,auto}
Switch# exit
~~~

**Attention** : si l'on change la vitesse d'un port pour lequel portfast n'est pas activé, celui-ci sera désactivé par le STP pendant 30s.

Affecter un nom / une description à l'interface :

~~~
Switch# conf t
Switch# interface GigabitEthernet1/0/48
Switch# description Machine XYZ
~~~

Gestion du MTU pour toutes les interfaces Gigabits :

~~~
Switch(config)# system mtu jumbo 9000
~~~

#### Gérer les fichiers/répertoires

~~~
Switch# cd flash:
Switch# cd rep
Switch# dir
Switch# copy foo bar
Switch# delete bar
Switch# mkdir rep
Switch# rm rep
Switch# more info.txt
Switch# verify image.bin
~~~

Commandes à manipuler avec précaution :

~~~
Switch# fsck
Switch# erase
Switch# format
~~~

#### Reboot complet du switch

~~~
Switch# reload
~~~

#### Reset complet du switch

##### Avec le bouton _MODE_

Si l'on appuie plus de 8 secondes sur le bouton _MODE_, le switch redémarre et sera remis en configuration d'usine !

##### Autre méthode sans mot de passe

Sur certains modèles, cette méthode ne fonctionne pas. Après être raccordé au switch, il faut le redémarrer en maintenant le bouton MODE enfoncé.

Ensuite, initialiser le file system flash :

~~~
switch: flash_init
switch: load_helper
~~~

On peut sauvegarder ou supprimer l'ancienne configuration :

~~~
switch: dir_flash:
switch: rename flash:config.text flash:config.old
~~~

Enfin, on boot :

~~~
switch: boot
~~~

### Sauvegardes

Voir la liste des protocoles disponibles :

~~~
Switch# show file systems
File Systems:

     Size(b)     Free(b)      Type  Flags  Prefixes
           -           -    opaque     ro   bs:
*   57931776    42733568     flash     rw   flash: flash1:
           -           -    opaque     rw   system:
           -           -    opaque     rw   tmpsys:
      524288      518420     nvram     rw   nvram:
           -           -    opaque     rw   null:
           -           -    opaque     ro   tar:
           -           -   network     rw   tftp:
           -           -    opaque     ro   xmodem:
           -           -    opaque     ro   ymodem:
           -           -   network     rw   rcp:
           -           -   network     rw   <http:>
           -           -   network     rw   ftp:
           -           -   network     rw   scp:
           -           -   network     rw   <https:>
           -           -    opaque     ro   cns:

Switch# show flash 

Directory of flash:/

  581  -rwx           5  Mar 21 2012 19:19:25 +01:00  private-config.text
  582  -rwx        3096  Mar 21 2012 19:19:25 +01:00  multiple-fs
  583  -rwx        2739  Mar 21 2012 19:19:25 +01:00  config.text
    2  drwx         512   Mar 1 1993 01:14:39 +01:00  c2960s-universalk9-mz.122-55.SE3

57931776 bytes total (42733568 bytes free)
~~~

Copier la configuration actuelle dans un fichier nommé `sauvegarde` :

~~~
Switch# copy running-config sauvegarde
Destination filename [sauvegarde]? 

2739 bytes copied in 2.076 secs (1319 bytes/sec)

Switch# show flash                    

Directory of flash:/

  580  -rwx        2739  Mar 21 2012 19:29:32 +01:00  sauvegarde
  581  -rwx           5  Mar 21 2012 19:19:25 +01:00  private-config.text
  582  -rwx        3096  Mar 21 2012 19:19:25 +01:00  multiple-fs
  583  -rwx        2739  Mar 21 2012 19:19:25 +01:00  config.text
    2  drwx         512   Mar 1 1993 01:14:39 +01:00  c2960s-universalk9-mz.122-55.SE3

57931776 bytes total (42729984 bytes free)


Switch# delete sauvegarde
Delete filename [sauvegarde]? 
Delete flash:/sauvegarde? [confirm]
~~~

On peut aussi envoyer la configuration sur un serveur distant :

~~~
Switch# copy running-config ftp://ftp.example.com/rep/sauvegarde_29fevrier2012.txt
~~~

Si le serveur FTP nécessite des identifiants de connexion, on peut les configurer :

~~~
Switch(config)# ip ftp username jdoe
Switch(config)# ip ftp password PASSWORD
~~~

De même, on peut sauvegarder le firmware du switch sur un serveur distant : 

~~~
Switch# cd flash:/
Switch# cd c2960s-universalk9-mz.122-55.SE3
Switch# dir

Directory of flash:/c2960s-universalk9-mz.122-55.SE3/

    3  drwx        5632   Mar 1 1993 01:08:54 +01:00  html
  578  -rwx    10907578   Mar 1 1993 01:10:24 +01:00  c2960s-universalk9-mz.122-55.SE3.bin
  579  -rwx         484   Mar 1 1993 01:13:44 +01:00  info

Switch# copy flash:/c2960s-universalk9-mz.122-55.SE3/c2960s-universalk9-mz.122-55.SE3.bin ftp://ftp.example.com/rep/sauvegarde_firmware.bin
~~~

Pour sauvegarder la liste des VLANs, c'est le fichier `vlan.dat` qui nous intéresse :

~~~
Switch# copy flash:/vlan.dat ftp://ftp.example.com/rep/sauvegarde_vlan.dat
~~~

### Mettre à jour IOS

Le software IOS est téléchargeable gratuitement sur <https://software.cisco.com/download/home>. Il suffit d'avoir un compte Cisco, créable par n'importe qui.

Il faut chercher le modèle du switch que l'on possède. Ce modèle est visible à la fin de la commande `show version`, auquel on enlève "WS-C" :

~~~
Switch# sh version         
[…]
Switch Ports Model              SW Version            SW Image
------ ----- -----              ----------            ----------
*    1 26    WS-C2960-24TT-L    12.2(55)SE1           C2960-LANBASEK9-M
~~~

Le modèle est `WS-C2960-24TT-L`, on cherche alors `2960-24TT-L`.

On clique ensuite sur "IOS Software", et toutes les versions apparaissent, dont la version recommandée. Une version `.tar` et une version `.bin` sont proposées, nous téléchargeons plutôt la version `.bin`.

Ensuite il y a 2 possibilités, selon que le switch a un port USB ou non.

#### Switch avec un port USB

La clé sera formatée, il ne faut pas avoir des données importantes dessus.

On commence par brancher une clé USB sur le switch.

~~~
*Mar  1 01:19:12.778: %USBFLASH-5-CHANGE: usbflash0 has been inserted!
*Mar  1 01:19:12.935: %USBFLASH-4-FORMAT: usbflash0 contains unexpected values in partition table or boot sector.  Device needs formatting before use!
~~~

Ensuite on formate cette clé USB pour que le switch puisse la lire :

~~~
Switch# format usbflash0:
Format operation may take a while. Continue? [confirm]
Format operation will destroy all data in "usbflash0:".  Continue? [confirm]

Primary Partition created...Size 3680 MB

Drive communication & 1st Sector Write OK...

Format: All system sectors written. OK...

Format: Total sectors in formatted partition: 7536608
Format: Total bytes in formatted partition: 3858743296
Format: Operation completed successfully.

Format of usbflash0 complete
~~~

On vérifie la version IOS actuelle :

~~~
Switch# sh version
[…]
System image file is "flash:/c2960-lanbasek9-mz.122-55.SE1.bin"
[…]
Switch Ports Model              SW Version            SW Image
------ ----- -----              ----------            ----------
*    1 26    WS-C2960-24TT-L    12.2(55)SE1           C2960-LANBASEK9-M
~~~

Si souhaité, on peut sauvegarder le binaire actuel vers la clé USB :

~~~
Switch# copy flash:c2960-lanbasek9-mz.122-55.SE1.bin usbflash0:
Destination filename [c2960-lanbasek9-mz.122-55.SE1.bin]?
Copy in progress...CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
9771282 bytes copied in 22.498 secs (434318 bytes/sec)
~~~

Ensuite, on branche la clé USB sur le PC où le nouveau binaire a été téléchargé pour le copier sur cette clé USB.

Une fois que la copie du binaire est faite sur la clé USB, on la rebranche sur le switch, et on le copie dans sa mémoire flash :

~~~
Switch# copy usbflash0:c2960-lanbasek9-mz.122-55.SE12.bin flash:
Destination filename [c2960-lanbasek9-mz.122-55.SE12.bin]?
Copy in progress...CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
9827106 bytes copied in 125.502 secs (78302 bytes/sec)
~~~

Puis on indique au switch de booter sur cette nouvelle version :

~~~
Switch# conf t
Switch(config)#boot system flash:c2960-lanbasek9-mz.122-55.SE12.bin
Switch(config)#end
~~~

On vérifie :

~~~
Switch# sh boot
BOOT path-list      : flash:c2960-lanbasek9-mz.122-55.SE12.bin
[…]
~~~

**Important :** on débranche la clé USB avant redémarrage, sinon il se met en erreur.

On enregistre, et on redémarre le switch :

~~~
Switch# copy running-config startup-config 
Destination filename [startup-config]?
Building configuration...
[OK]

Switch# reload
Proceed with reload? [confirm]
~~~

Après redémarrage, on vérifie que la version a bien été mise à jour :

~~~
Switch# sh version
[…]
System image file is "flash:c2960-lanbasek9-mz.122-55.SE12.bin"
[…]
Switch Ports Model              SW Version            SW Image
------ ----- -----              ----------            ----------
*    1 26    WS-C2960-24TT-L    12.2(55)SE12          C2960-LANBASEK9-M
~~~

On est bien passé de la version `12.2(55)SE1 ` à la version `12.2(55)SE12`.

L'ancien binaire est toujours présent dans la mémoire flash du switch, et peut être supprimé s'il manque de la place :

~~~
Switch# dir flash:
Directory of flash:/
             
    2  -rwx        4120   Mar 1 1993 01:02:38 +01:00  multiple-fs 
    3  -rwx        1816   Mar 1 1993 01:01:20 +01:00  vlan.dat
    4  -rwx        1414   Mar 1 1993 06:28:42 +01:00  config.old
    5  -rwx        3858   Mar 1 1993 01:01:49 +01:00  config.text
    6  -rwx     9771282   Mar 1 1993 01:03:00 +01:00  c2960-lanbasek9-mz.122-55.SE1.bin
    7  -rwx     9827106   Mar 1 1993 01:26:57 +01:00  c2960-lanbasek9-mz.122-55.SE12.bin
   10  -rwx        1920   Mar 1 1993 01:01:49 +01:00  private-config.text
    
32514048 bytes total (12899840 bytes free)
    
Switch# delete flash:c2960-lanbasek9-mz.122-55.SE1.bin 

Delete filename [c2960-lanbasek9-mz.122-55.SE1.bin]?
Delete flash:c2960-lanbasek9-mz.122-55.SE1.bin? [confirm]
~~~

#### Switch sans port USB

1. **Pré-requis**

Le switch doit tout d'abord avoir une IP configurée. On peut par exemple configurer une IP dans le VLAN 1 comme indiqué [ici](#affecter-une-adresse-ip-%C3%A0-un-vlan), puis affecter l'interface qui sera branchée au serveur TFTP à ce VLAN 1 en s'aidant de [cette partie](#affecter-des-interfaces-ports-%C3%A0-un-vlan).

2. **Installer un serveur TFTP**

Un serveur TFTP est nécessaire pour transférer le binaire vers le switch.

On utilise `tftpd-hpa` :

~~~
# apt install tftpd-hpa
# vim /etc/default/tftpd-hpa

TFTP_USERNAME="nobody"
TFTP_DIRECTORY="/srv/tftp"
TFTP_ADDRESS="0.0.0.0:69"
TFTP_OPTIONS="--secure -c"

# /etc/init.d/tftpd-hpa restart
# mv c2960-lanbasek9-mz.122-55.SE12.bin /srv/tftp/
# chown -R nobody:nogroup /srv/tftp/
~~~

On configure le serveur sur l'utilisateur `nobody`, et on ajoute l'option `-c` pour autoriser la création de nouveaux fichiers sur le serveur. On place ensuite le binaire IOS dans le répertoire TFTP.

3. **Copier le binaire vers le switch**

On commence par vérifier la version actuelle :

~~~
Switch# sh version
[…]
System image file is "flash:/c2960-lanbasek9-mz.122-55.SE1.bin"
[…]
Switch Ports Model              SW Version            SW Image
------ ----- -----              ----------            ----------
*    1 26    WS-C2960-24TT-L    12.2(55)SE1           C2960-LANBASEK9-M
~~~

Si souhaité, on peut sauvegarder le binaire actuel vers le serveur TFTP :

~~~
Switch# copy flash:c2960-lanbasek9-mz.122-55.SE1.bin tftp:
Address or name of remote host []? 192.0.2.20       
Destination filename [c2960-lanbasek9-mz.122-55.SE1.bin]?
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
9771282 bytes copied in 22.498 secs (434318 bytes/sec)
~~~

Ensuite, on récupère la version téléchargée du binaire sur le switch :

~~~
Switch# copy tftp://192.0.2.20/c2960-lanbasek9-mz.122-55.SE12.bin flash:
Destination filename [c2960-lanbasek9-mz.122-55.SE12.bin]?
Accessing tftp://192.0.2.20/c2960-lanbasek9-mz.122-55.SE12.bin...
Loading c2960-lanbasek9-mz.122-55.SE12.bin from 192.0.2.20 (via Vlan1): !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[OK - 9827106 bytes]

9827106 bytes copied in 125.502 secs (78302 bytes/sec)
~~~

Puis on indique au switch de booter sur cette nouvelle version :

~~~
Switch# conf t
Switch(config)#boot system flash:c2960-lanbasek9-mz.122-55.SE12.bin
Switch(config)#end
~~~

On vérifie :

~~~
Switch# sh boot
BOOT path-list      : flash:c2960-lanbasek9-mz.122-55.SE12.bin
[…]
~~~

On enregistre, et on redémarre le switch :

~~~
Switch# copy running-config startup-config 
Destination filename [startup-config]?
Building configuration...
[OK]

Switch# reload
Proceed with reload? [confirm]
~~~

Après redémarrage, on vérifie que la version a bien été mise à jour :

~~~
Switch# sh version
[…]
System image file is "flash:c2960-lanbasek9-mz.122-55.SE12.bin"
[…]
Switch Ports Model              SW Version            SW Image
------ ----- -----              ----------            ----------
*    1 26    WS-C2960-24TT-L    12.2(55)SE12          C2960-LANBASEK9-M
~~~

On est bien passé de la version `12.2(55)SE1 ` à la version `12.2(55)SE12`.

L'ancien binaire est toujours présent dans la mémoire flash du switch, et peut être supprimé s'il manque de la place :

~~~
Switch# dir flash:
Directory of flash:/
             
    2  -rwx        4120   Mar 1 1993 01:02:38 +01:00  multiple-fs 
    3  -rwx        1816   Mar 1 1993 01:01:20 +01:00  vlan.dat
    4  -rwx        1414   Mar 1 1993 06:28:42 +01:00  config.old
    5  -rwx        3858   Mar 1 1993 01:01:49 +01:00  config.text
    6  -rwx     9771282   Mar 1 1993 01:03:00 +01:00  c2960-lanbasek9-mz.122-55.SE1.bin
    7  -rwx     9827106   Mar 1 1993 01:26:57 +01:00  c2960-lanbasek9-mz.122-55.SE12.bin
   10  -rwx        1920   Mar 1 1993 01:01:49 +01:00  private-config.text
    
32514048 bytes total (12899840 bytes free)
    
Switch# delete flash:c2960-lanbasek9-mz.122-55.SE1.bin 

Delete filename [c2960-lanbasek9-mz.122-55.SE1.bin]?
Delete flash:c2960-lanbasek9-mz.122-55.SE1.bin? [confirm]
~~~

### Gestion des VLANs

#### Affecter une adresse IP à un VLAN

~~~
Switch# configure terminal
Switch(config)# interface Vlan 1
Switch(config-if)# ip address 192.0.2.10 255.255.255.0
~~~

#### Créer un VLAN

Dans cet exemple, ce sera le VLAN d'ID 2 nommé « bob ».

~~~
Switch# configure terminal
Switch(config)# vlan 2
Switch(config-vlan)# name bob
Switch(config-vlan)# end
~~~

#### Supprimer un VLAN

~~~
Switch# configure terminal
Switch(config)# no vlan 2
Switch(config)# end
~~~

#### Affecter des interfaces (ports) à un VLAN

Dans cet exemple on affecte l'interface `GigabitEthernet 0/13` au VLAN 2

~~~
Switch# configure terminal 
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)# interface gigabitethernet0/13
Switch(config-if)# switchport mode access 
Switch(config-if)# switchport access vlan 2
Switch(config-if)# end 
~~~

On peut affecter un range de ports à un VLAN :

~~~
Switch# conf t
Switch(config)# interface range GigabitEthernet1/0/1-24,GigabitEthernet1/0/30-35
Switch(config-if-range)# switchport mode access
Switch(config-if-range)# switchport access vlan 13
Switch(config-if-range)# exit
~~~

Attention : on ne peut pas spécifier plus de 5 groupes de range séparés par des virgules à la fois (maximum 4 virgules).

#### Voir la configuration des VLAN

##### D'un VLAN en particulier

~~~
Switch# show vlan <ID>
Switch# show interface vlan <ID>
~~~

##### De tous les VLAN

Voir un résumé de la configuration :

~~~
Switch# show vlan brief
~~~

Voir la configuration de tous les VLAN :

~~~
Switch# show vlan
~~~

#### Faire un port trunk

Dans cet exemple l'interface _trunké_ est l'interface `GigabitEthernet0/24`

Sur un switch niveau 3 :

~~~
Switch# configure terminal 
Enter configuration commandsss, one per line.  End with CNTL/Z.
Switch(config)# interface gigabitethernet0/24 
Switch(config-if)# switchport trunk encapsulation dot1q
Switch(config-if)# switchport mode trunk
Switch(config-if)# end 
~~~

Un switch niveau 2 ne supporte que le dot1q. On aura juste à basculer le port en mode trunk :

~~~
Switch(config-if)# switchport mode trunk
~~~

Il est possible de spécifier le ou les vlans transportés par le trunk :

~~~
Switch(config-if)# switchport trunk allowed vlan 11,13
~~~

Astuce : pour ajouter un VLAN sur un trunk sans reprendre toute la liste de ceux déjà autorisés on peut utiliser la syntaxe :

~~~
Switch(config-if)# switchport trunk allowed vlan add 42
~~~

Pour le supprimer :

~~~
Switch(config-if)# switchport trunk allowed vlan remove 42
~~~

Une façon de contrôler si le trunk est bien mis en place des 2 côtés est de consulter la sortie de la commande `show vlan brief`. Si le port est toujours dans le vlan 1, c'est que le trunk n'est pas opérationnel (interface non montée, ou port distant non configuré en trunk). Si tout fonctionne bien, on ne doit le voir dans aucun vlan, mais on le verra en trunk dans un `show interface status`.

Pour afficher les VLAN autorisés sur un trunk :

~~~
Switch# show interface Gi1/0/50 trunk
~~~

Pour afficher la configuration de tous les trunks :

~~~
Switch# show interface trunk
~~~

ATTENTION : si l'on configure un port trunk, il est indispensable de créer le VLAN sur le switch, sinon cela ne marche pas !

### Administration à distance

#### Configuration IP

Configuration IP de base :

~~~
Switch# conf t
(config)# interface Vlan1
(config-if)# ip address 192.0.2.1 255.255.255.0
(config-if)# no shutdown
~~~

La configuration IP de base permet un accès au switch via telnet

#### Par telnet

Important : Mettre en place le [compte utilisateur](SwitchCisco#mise-en-place-dun-compte-utilisateur).

~~~
Switch# configure terminal
Switch(config)# line vty 0 15
Switch(config-line)# login local
Switch(config-line)# end
~~~

Note : `line vty 0 4` autorise 5 connexions simultanées tandis que `line vty 0 15` en autorise 16. Les plus anciens switchs ne permettaient pas plus de 5 connexions simultanées, c'est pourquoi les vty 0 4 et 5 15 sont souvent séparés.

#### Par SSH

Important : Mettre en place le [nom d'hôte](SwitchCisco#changer-le-nom-dh%C3%B4te), le [nom de domaine](SwitchCisco#changer-le-nom-de-domaine), et le [compte utilisateur](SwitchCisco#mise-en-place-dun-compte-utilisateur).

Activation de SSH, et désactivation de telnet :

~~~
Switch# configure terminal
Switch(config)# crypto key generate rsa
Switch(config)# line vty 0 15
Switch(config-line)# login local
Switch(config-line)# transport input ssh
Switch(config-line)# exit
Switch(config)# ip ssh version 2
~~~

Si on veut avoir à la fois telnet et SSH, remplacer `transport input ssh` par `transport input all`.

#### Désactiver les services web

~~~
Switch(config)# no ip http server
Switch(config)# no ip http secure-server
~~~

### Configurer l'heure

* Synchronisation NTP :

~~~
Switch# configure terminal
Switch(config)# ntp server 31.170.8.123
Switch(config)# end
Switch# show ntp status
Switch# show ntp associations 
~~~

* Gestion Daylight Savings Time (DST) :

~~~
Switch# configure terminal
Switch(config)# clock timezone UTC 1
Switch(config)# clock summer-time UTC recurring last Sun Mar 2:00 last Sun Oct 3:00
~~~

### Gérer les logs

* Synchroniser les logs :

~~~
Switch(config)# line console 0
Switch(config-line)# logging synchronous
~~~

Cette commande permet de ne pas interrompre le prompt de l'utilisateur lorsque des logs s'affichent sur le switch. Ils s'afficheront sur la ligne du dessus plutôt que sur la ligne courante.

* Augmenter la taille du buffer de log :

~~~
Switch# conf t
Switch(config)# logging buffered 102400
~~~

La commande `show logging` gardera un historique de logs plus importants.

* Générer des logs lors d'une connexion utilisateur :

~~~
Switch# conf t
Switch(config)# login on-failure log
Switch(config)# login on-success log
~~~

**Attention !** La commande `login on-success log` provoque un bug sous la version IOS `15.0(2a)EX5`. Si le switch est sous cette version, il ne faut **pas** configurer cette commande, qui provoquera un reboot du switch à chaque connexion d'un utilisateur.

* Afficher l'heure locale sur les logs, avec une présicion en millisecondes :

~~~
Switch# conf t
Switch(config)# service timestamps debug datetime msec localtime
Switch(config)# service timestamps log datetime msec localtime
~~~

* Envoyer les logs à un serveur syslog :

~~~
Switch# conf t
Switch(config)# logging trap debugging
Switch(config)# logging facility local6
Switch(config)# logging host 192.0.2.10
~~~

Du côté du serveur syslog, par exemple avec rsyslogd, on peut le configurer pour avoir un fichier de log par switch, selon son IP. Dans `/etc/rsyslog.conf` :

~~~
if $fromhost-ip=='192.0.2.5' then /var/log/switchs/switch-192.0.2.5.log
& stop
if $fromhost-ip=='192.0.2.6' then /var/log/switchs/switch-192.0.2.6.log
& stop
~~~

Attention à bien mettre ces règles **avant** les règles de facilités.

Vu qu'on a configuré le switch pour utiliser la facilité "local6", on peut également rajouter une règle supplémentaire pour tous les switchs sans règle spécifique :

~~~
local6.*                        -/var/log/switchs/unknown.log
~~~

Puis on restart le service :

~~~
# systemctl restart rsyslog
~~~

* Rate-limiter les messages de log

Dans le cas où on a un message qui s'écrit plusieurs fois à la suite, et qu'on veut éviter que celui-ci remplisse les logs, on peut rate-limiter ce log spécifiquement :

~~~
Switch# conf t
Switch(config)# logging discriminator SNMP severity drops 5 facility drops SYS mnemonics drops CONFIG_I msg-body drops "Configured from 192.0.2.10 by snmp"
~~~

Ici, il le message ne s'affichera pas plus de 5 fois à la suite.

Une fois le discriminator "SNMP" crée, on l'applique aux commandes syslog précédemment activées :

~~~
# Taille du buffer :
Switch(config)# logging buffered discriminator SNMP 102400
# Envoi à un serveur syslog :
Switch(config)# logging host 192.0.2.10 discriminator SNMP
~~~

### Configurer SNMP

* Activer SNMP en lecture :

~~~
Switch# conf t
Switch(config)# snmp-server community public RO 
~~~

On pourra ainsi faire des requêtes du type :

~~~
$ snmpwalk -v2c -c public <IP switch> .1
~~~

* Activer SNMP en écriture :

Si on veut limiter l'écriture SNMP par une IP source, et sur certains OID uniquement, il faut créer une `view` et une `access-list`, puis l'indiquer à SNMP :

~~~
Switch# conf t
Switch(config)# snmp-server view ifXTable 1.3.6.1.2.1.31.1.1.* included
Switch(config)# access-list 1 permit 192.0.2.10
Switch(config)# snmp-server community private view ifXTable RW 1
~~~

* Envoyer des traps SNMP :

Le terme "trap" englobe 2 types de trap SNMP : les requêtes "trap" et les requêtes "inform". La différence est que pour une requête "trap", l'agent SNMP qui l'envoie n'a aucun moyen de savoir si le manager SNMP l'a bien reçue ou non, alors qu'une requête "inform" sera ré-émise par l'agent SNMP tant qu'il n'a pas reçu d'acknowledgement de la part du manager SNMP lui indiquant que la requête inform a bien été reçue.

Si on veut envoyer des traps SNMP de type `inform` à l'hôte `192.0.2.10`, en utilisant la version SNMP `2c` et la communauté SNMP `private`, et en activant toutes les traps :

~~~
Switch# conf t
Switch(config)# snmp-server host 192.0.2.10 informs version 2c private
Switch(config)# snmp-server enable traps
~~~

Si on souhaite que les traps de type "link up" et "link down" soient au format standard IETF et indiquent notamment les status `ifAdminStatus` et `ifOperStatus` :

~~~
Switch(config)# snmp-server trap link ietf
~~~

En activant toutes les traps, certaines sont trop verbeuses et inutiles, et peuvent être désactivées :

~~~
Switch(config)# no snmp-server enable traps config
Switch(config)# no snmp-server enable traps syslog
~~~

La première désactive l'envoi de trap à chaque exécution d'une commande sur le switch, la deuxième désactive l'envoi de trap SNMP avec le contenu du log pour chaque syslog généré.

### Modifier le temps d'inactivité avant déconnexion

Pour être déconnecté après 1h d'inactivité :

~~~
Switch# conf t
Switch(config)# line vty 0 15
Switch(config-line)# exec-timeout 60
Switch(config-line)# line con 0
Switch(config-line)# exec-timeout 60
~~~

### Désactiver la traduction DNS

~~~
Switch# conf t
Switch(config)# no ip domain-lookup
~~~

Dans le cas où il n'y a pas de serveur DNS configuré, cette commande permet de ne pas avoir à attendre quelques secondes avant timeout qu'un nom soit tenté d'être traduit. Très utile en cas d'erreur :

~~~
Switch# exiit
Translating "exiit"...domain server (255.255.255.255)
% Unknown command or computer name, or unable to find computer address
~~~

### Gestion des adresses MAC

Lister toutes les adresses MAC connues :

~~~
Switch# show mac address-table
Switch# show mac address-table int Gi0/11
~~~

Gestion des adresses MAC dynamiques, notamment forcer la suppression :

~~~
Switch# clear mac address-table dynamic
Switch# clear mac address-table dynamic address <mac>
Switch# clear mac address-table dynamic interface <if>
Switch# clear mac address-table dynamic vlan <id>
~~~

Configurer des alertes lors de modifications :

~~~
Switch(config)# snmp-server enable traps mac-notification change
Switch(config)# snmp-server enable traps mac-notification move
~~~

Forcer des adresses MAC de façon statique :

~~~
Switch(config)# mac address-table static <mac> vlan <id> interface <if>
~~~

### CDP : Cisco Discovery Protocol et LLDP : Link Layer Discovery Protocol

<https://fr.wikipedia.org/wiki/Cisco_Discovery_Protocol>  
<https://fr.wikipedia.org/wiki/Link_Layer_Discovery_Protocol>

Ces deux protocoles de niveau 2 permettent la même chose : découvrir ses voisins réseau directement connectés, à l'aide d'annonces émises de façon régulière. CDP est propriétaire Cisco (et donc uniquement compatible avec d'autres équipements Cisco) tandis que LLDP est un protocole normé.

Si ces protocoles sont activés sur l'ensemble des équipements d'un réseau, ils permettent d'en découvrir la topologie en vérifiant les voisins de chacun des équipements. Attention cependant : si trois switchs sont interconnectés (switch1<--->switch2<--->switch3) mais que switch2 a ses protocoles LLDP et CDP désactivés, alors les paquets de découvertes de switch1 pour chacun des protocoles seront retransmis par switch2 à switch3. Si on ne fait pas attention, on peut alors penser que switch1 et switch3 sont directement connectés.

Il peut également être intéressant de capturer ces annonces depuis une machine afin de savoir sur quel appareil elle est connectée.

Pour capturer une annonce CDP :

~~~
# tcpdump -v -c 1 -i eth0 'ether[20:2] == 0x2000'
~~~

Pour capturer une annonce LLDP :

~~~
# tcpdump -v -c 1 -i eth0 ether proto 0x88cc
~~~

Pour chaque voisin est indiqué : son nom réseau, le type ou modèle d'appareil, son adresse IP, l'interface du switch local à laquelle l'appareil est connecté, l'interface du switch distant à laquelle l'appareil est connecté, ainsi que le VLAN utilisé sur ce lien.

Ces protocoles peuvent également servir à automatiquement configurer certains appareils sur le réseau à l'aide des informations partagées. Ce n'est généralement pas une bonne idée de l'utiliser à cette fin, de mauvaises configurations peuvent se faire et une configuration réseau qui fonctionnait peut devenir non fonctionnelle.

### STP : Spanning Tree Protocol

<http://en.wikipedia.org/wiki/Spanning_Tree_Protocol>

Le STP est un protocole permettant de détecter et désactiver automatiquement des boucles sur un segment Ethernet.
Cela permet donc d'éviter une boucle faite par erreur (ce qui en découle sur un Packet Storm et un réseau très dégradé)
…ou de créer des boucles volontairement pour assurer de la redondance !

Le principe de fonctionnement est qu'un des switchs est élu ROOT (racine de l'arbre STP), et qu'un coût est associé à chaque lien vers le ROOT.
Ce coût est calculé automatiquement à partir du nombre de connexions et du type de ces connexions (un lien 10Mb/s coûte 100, un lien 100Mb/s coûte 19, un lien 1Gb/s coûte 4, etc.).
Ce coût peut aussi être forcé manuellement si l'on veut influencer le calcul du STP.
Ensuite, grâce à ces coûts, si une boucle est détectée certains ports peuvent être bloqués.
Des vérifications sont réalisées régulièrement pour détecter un changement et adapter les blocages si nécessaire.

/!\\ Attention : le simple branchement d'un switch peut provoquer un recalcul de l'arbre et de la topologie par STP, selon la topologie présente. Le branchement d'un nouveau switch peut faire changer le chemin pour permettre de joindre ce nouveau switch en moins de saut.

/!\\ Le _keepalive_ — élement essentiel pour STP — n'est pas activé par défaut sur les ports *SFP* d'un switch Cisco : il faut absolument l'activer si vos segments Ethernet sont propagés sur les ports SFP !

~~~
Switch# conf t
(config)# int Gi0/49
(config-if)# keepalive
~~~

Sur les Cisco 2960-2970, le STP est géré par VLAN on parle de « per-VLAN spanning-tree plus (PVST+) » et si le mode « Rapid » est activé de « rapid per-VLAN spanning-tree plus (rapid-PVST+) ».
La valeur par défaut de la priorité du root switch est 3276.
Le root ID est calculé avec cette priorité + une dérivation de l'adresse MAC. C'est le plus petit ID qui l'emporte.
Il peut être intéressant de changer la priorité pour choisir le switch root.
On peut aussi changer la priorité des ports (128 par défaut + coût du lien [10MBps = 100, 100Mbps = 19, 1Gbps = 4]) pour influencer le calcul STP et les connexions à désactiver.
La configuration par défaut des timers respecte les recommandations de la norme 802.1d.

~~~
Hello time: 2 seconds.
Forward-delay time: 15 seconds.
Maximum-aging time: 20 seconds.
Transmit hold count: 6 BPDUs 
~~~

Il faut avoir les mêmes valeurs si d'autres équipements interviennent sur le réseau, comme des machines OpenBSD ou Linux.

#### Voir les informations du STP sur le switch.

~~~
Switch# show spanning-tree summary
Switch# show spanning-tree
Switch# show spanning-tree detail
~~~

#### Activer le mode rapid STP 802.1w.

~~~
Switch# conf t
Switch(config)# spanning-tree mode rapid-pvst
~~~

#### Forcer un switch en root (la priorité sera calculée automatiquement).

~~~
Switch# conf t
Switch(config)# spanning-tree vlan 1-4096 root primary
~~~

#### Changer le coût d'un port.

~~~
Switch# conf t
Switch(config)# interface gigabitEthernet 0/50
Switch(config-if)# spanning-tree cost 65536
~~~

#### Changer le coût uniquement pour un ou plusieurs VLANs.

~~~
Switch(config-if)# spanning-tree vlan 1-4096 cost 1
~~~

#### Différence entre Spanning-Tree et Rapid Spanning-Tree

En Spanning-Tree, le rajout d'un lien créant une boucle et ayant un coût plus bas que le lien déjà en place (donc nouveau lien qui sera utilisé à la place de l'actuel) provoquera une coupure de 30s de ces deux liens. Si le lien rajouté a un coût plus élevé et qu'il ne remplacera donc aucun lien, alors seul ce nouveau lien sera coupé 30s. Le changement du switch ROOT provoque également une coupure des ports 30s.

En Rapid Spanning-Tree, le rajout d'un lien et le changement du switch ROOT ne provoquent pas de coupure, seuls des liens redondants déjà désactivés peuvent se couper 30s.

Le passage du mode Spanning-Tree au mode Rapid Spanning-Tree ou inversement provoque également la coupure de tous les ports du switch pour 30s, sauf les ports en mode portfast.

#### Synchro immédiate : Spanning Tree Portfast

Lorsque l’on se branche sur un port, il faut 30s pour qu’il soit utilisable à cause du Spanning Tree.
Le Spanning Tree Portfast permet de passer un port dans l'état forwarding de façon immédiate, en lui faisant sauter les états listening et learning.
On utilisera cette commande uniquement si l'on est sûr de ne pas avoir besoin du Spanning Tree (serveur non virtuel connecté directement au port, etc.). Le portfast ne désactive pas le spanning-tree puisque le port continue de recevoir et d'envoyer des trames BPDU : il sera bien désactivé par STP si une boucle est créée. Mais dans ce cas, le port pourra mettre jusqu'à 2s pour se désactiver (correspondant à la valeur hello time), et donc des duplications de trames pourront avoir lieu pendant cet instant.

Activer le portfast :

~~~
Switch(config)# interface GigabitEthernet0/28
Switch(config-if)# spanning-tree portfast
~~~

Désactiver le portfast :

~~~
Switch(config)# interface GigabitEthernet0/28
Switch(config-if)# no spanning-tree portfast
~~~

### VTP : VLAN Trunking Protocol

<https://www.cisco.com/c/fr_ca/support/docs/lan-switching/vtp/10558-21.html>

Le VTP est un protocole de niveau 2 propriétaire Cisco et permettant de configurer et administrer les VLAN des switchs dans un même réseau local.
Attention, son utilisation peut être dangereuse si le protocole n'est pas maîtrisé.

Les différents switchs doivent être dans le même domaine VTP pour que la configuration se partage entre eux. Par défaut, aucun domaine n'est configuré, et celui-ci est configuré automatiquement sur tous les switchs dès que sa configuration est faite sur un premier switch.

~~~
Switch(config)# vtp domain NAME
~~~

VTP fonctionne selon plusieurs modes :

* Serveur (mode par défaut) : Création, modification et suppression de VLAN possible. La version VTP peut également être modifiée pour l'ensemble du domaine VTP. Les serveurs annoncent leur configuration VLAN aux autres switchs du domaine et la synchronisent selon les paquets VTP reçus au travers des liens trunks.
* Client : Les clients synchronisent leur configuration VLAN d'après les paquets VTP reçus, et les transférent au travers des liens trunks 
* Transparent : Un switch en mode transparent ne participe pas à VTP. Il ne partage ni ne synchronise pas sa configuration, mais transfère les paquets VTP reçus sur ses liens trunks.
* Off : Aucune participation à VTP, et les paquets reçus ne sont pas transférés.

En mode transparent ou off, la configuration VLAN s'écrit dans la NVRAM avec le reste de la configuration.

~~~
Switch(config)# vtp mode (server|client|transparent|off)
~~~

Les switchs ont un numéro de révision VTP, incrémenté lors de chaque modification d'une configuration VTP ou VLAN. Si le numéro de révision reçu par un switch serveur ou client est plus grand que celui en cours, la nouvelle configuration est appliquée. Sinon, elle est ignorée. 

~~~
Switch# sh vtp status
VTP Version capable             : 1 to 3
VTP version running             : 1
VTP Domain Name                 : 
VTP Pruning Mode                : Disabled
VTP Traps Generation            : Disabled
Device ID                       : a456.309a.d200
Configuration last modified by 0.0.0.0 at 0-0-00 00:00:00
Local updater ID is 192.0.2.1 on interface Fa0 (first layer3 interface found)

Feature VLAN:
--------------
VTP Operating Mode                : Server
Maximum VLANs supported locally   : 255
Number of existing VLANs          : 5
Configuration Revision            : 0
~~~

### Rate-limiting

On peut forcer un port à rate-limiter à 10, 20, 30.... ou 90% de sa capacité.

Par exemple pour rate-limiter à du 8 Mb/s :

~~~
Switch(config)# int Gi0/1
Switch(config-if)# speed 10
Switch(config-if)# srr-queue bandwidth limit 80
~~~

La négociation de la vitesse (commande `speed`) ne fonctionne que sur une interface ethernet, et non fibre.

Infos sur le rate-limiting :

~~~
Switch# show mls qos interface GigabitEthernet0/1 queueing
GigabitEthernet0/1
QoS is disabled. When QoS is enabled, following settings will be applied
Egress Priority Queue : disabled
Shaped queue weights (absolute) :  25 0 0 0
Shared queue weights  :  25 25 25 25
The port bandwidth limit : 10  (Operational Bandwidth:11.12)
The port is mapped to qset : 1 
~~~

### SPAN : Switch Port Analyzer (Port mirroring)

Activer le port mirroring, en copiant la sortie des ports G0/1 à G0/3 vers le port G0/5, et en gardant la même encapsulation :

~~~
Switch(config)# monitor session 1 source interface g0/1 - 3 tx
Switch(config)# monitor session 1 destination interface g0/5 encapsulation replicate
~~~

On peut aussi mirrorer tout ce qui passe dans un vlan particulier :

~~~
Switch(config)# monitor session 1 source vlan 2 , 5
Switch(config)# monitor session 1 destination interface g0/5 encapsulation replicate
~~~

Désactiver le port mirroring :

~~~
Switch(config)# no monitor session 1
~~~

#### Copier la transmission ou la réception

Sans aucun des deux mots clefs `rx` ou `tx`, la réception et la transmission sont tous les deux mirrorés. Il faut préciser `tx` si on ne veut mirrorer que ce qui sort du port, ou `rx` si on ne veut mirrorer que ce qui rentre dans le port.

Si on veut monitorer à la fois un port en tx et un autre en rx :

~~~
Switch(config)# monitor session 1 source interface g0/1 tx
Switch(config)# monitor session 1 source interface g0/2 rx
~~~

La deuxième commande n'écrase pas la première, mais la configuration s'additionne.

#### Encapsulation replicate

Par défaut, le port mirroring renvoie tout le trafic vers l'interface de destination de façon non étiqueté (sans aucun marquage de VLAN), et sans les protocoles de niveau 2 : CDP (Cisco Discovery Protocol), VTP (VLAN Trunking Protocol), DTP (Dynamic Trunk Protocol), STP (Spanning Tree Protocol) et PAgP (Port Aggregation Protocol).

En rajoutant le mot clef `encapsulation replicate`, les paquets sont envoyés en gardant le même étiquetage VLAN qu'à la source, et les protocoles de niveau 2 sont également copiés.

#### Filtrer les VLANs

Si un port source est un port trunk dans lequel passent les VLANs 2, 3, 4, 5 et 6, mais que l'on ne veut mirrorer que les VLANs 3 et 5 :

~~~
Switch(config)# monitor session 1 filter 3 , 5
~~~

Si parmi les ports source, l'un est en mode trunk et l'autre est en mode access, seul la source en trunk est affectée par ce filtre.

#### Autoriser un IDS/IPS à agir sur ce qu'il voit

L'IDS/IPS derrière la destination du port mirroring peut être configuré pour agir sur ce qu'il reçoit, et être ainsi capable d'envoyer par exemple un reset d'une connexion TCP qu'il a detecté comme étant indésirable. Le port de destination doit alors être autorisé à recevoir des paquets de l'IDS/IPS :

~~~
Switch(config)# monitor session 1 source interface g0/1 , g0/5
Switch(config)# monitor session 1 destination interface g0/15 ingress vlan 15
~~~

### LACP : Link Aggregation Control Protocol (Port-Channel)

Le Port-Channel est une technologie qui permet d’agréger plusieurs interfaces ensemble. Le trafic est ensuite réparti sur chacune des interfaces. Cette technologie permet une meilleure redondance, ainsi qu’une augmentation de la bande passante. L'utilisation de l'un ou l'autre lien ne dépend pas de la charge sur les liens mais uniquement de l'algorithme du protocole : même si un lien est surchargé, l'algorithme continuera à pouvoir envoyer du trafic dessus.

Cisco propose le protocole propriétaire PaGP, mais nous utilisons le protocole standardisé [LACP](https://fr.wikipedia.org/wiki/IEEE_802.3ad).

Voici un schéma d'exemple :

~~~
┌────────┐Gi0/1   Gi0/1┌────────┐
│        ├─────────────┤        │
│  SW01  │             │  SW02  │
│        ├─────────────┤        │
└────────┘Gi0/2   Gi0/2└────────┘
~~~

On veut agréger les interfaces Gi0/1 et Gi0/2 entre SW01 et SW02.

Sur SW01 et sur SW02, on active LACP sur les ports souhaités :

~~~
Switch(config)# interface range GigabitEthernet0/1-2
Switch(config-if)# channel-protocol lacp
Switch(config-if)# channel-group 1 mode active
~~~

La configuration des ports devra ensuite se faire sur l'interface port-channel nouvellement créé. Les interfaces physiques membres du port-channel hériteront automatiquement de la configuration.

~~~
Switch(config)# interface Port-channel1
Switch(config-if)# switchport mode trunk
Switch(config-if)# switchport trunk allowed vlan X
Switch(config-if)# …
~~~

### Cron / tâches planifiées

Voir <http://www.tmartin.io/articles/2010/sauvegarder-la-configuration-de-cisco-ios-vers-un-serveur-distant-avec-kron/>.

Exemple pour sauvegarder périodiquement la configuration du switch :

On commence par définir la tâche planifiée :

~~~
switch(config)# kron policy-list backupConfig
switch(config-kron-policy)# cli show running-config | redirect ftp://<host>/<path>/cisco.txt
switch(config-kron-policy)# exit
~~~

Puis on définit la période, par exemple tous les jours à 01h15 :

~~~
switch(config)# kron occurrence backupConfig_occurence at 01:15 recurring
switch(config-kron-occurrence)# policy-list backupConfig
switch(config-kron-occurrence)# exit
switch(config)# exit
~~~

### Désactiver la vérification des modules GBIC

Par défaut, CISCO n'autorise pas les modules GBIC non agréés, il faut donc désactiver la vérification des checksum des modules GBIC pour pouvoir les connecter :

~~~
Switch# conf t
Switch(config)# service unsupported-transceiver
~~~

On peut ensuite les lister via :

~~~
Switch# show inventory
~~~

### Ré-activer un port désactivé par errdisable

Un port est désactivé dans divers cas, par exemple la non-autorisation des modules GBIC tiers.

Pour le ré-activer manuellement :

~~~
Switch# conf t
Switch(config)# interface GigabitEthernet0/28
Switch(config-if)# shutdown
Switch(config-if)# no shutdown
~~~

Pour qu'il soit ré-activé automatiquement au bout d'un certain temps :

~~~
Switch# conf t
Switch(config)# errdisable recovery interval 600
Switch(config)# errdisable recovery cause gbic-invalid
~~~

En remplaçant `gbic-invalid` par la cause souhaitée.

La durée en secondes indiquée par la commande `interval` est globale à toutes les causes, et est la durée après laquelle le port sera automatiquement ré-activé après être passé en errdisable.

### Consulter les informations DOM d'un SFP

Pour surveiller la température, ou le voltage :

~~~
Switch# show interface transceiver
~~~

### Passer un port SFP en "speed nonegotiate"

On ne peut pas forcer la vitesse d'un port SFP… mais il faut parfois passer en "speed nonegotiate" :

~~~
Switch(config-if)# speed nonegotiate
Switch(config-if)# shutdown
Switch(config-if)# no shutdown
~~~

Lire <http://herdingpackets.net/2013/03/21/disabling-gigabit-link-negotiation-on-fiber-interfaces/>


### DTP : Dynamic Trunk Protocol

Le Dynamic Trunk Protocol (DTP) est un protocole permettant de négocier la création automatique de trunk sur une liaison. Pour en profiter, il faut passer l'interface soit en mode dynamic auto, soit en mode dynamic desirable :

~~~
Switch(config-if)# switchport mode dynamic auto
Switch(config-if)# switchport mode dynamic desirable
~~~

Selon l'état de l'interface de l'autre côté, la liaison sera en mode access ou en mode trunk :

                       Dynamic Auto    Dynamic Desirable   Trunk                 Access
---------------------  -------------   -----------------   --------------------  --------
**Dynamic Auto**       Access          Trunk               Trunk                 Access
**Dynamic Desirable**  Trunk           Trunk               Trunk                 Access
**Trunk**              Trunk           Trunk               Trunk                 ?
**Access**             Access          Access              ?                     Access

L'utilisation de DTP (et donc du mode dynamic) est risquée car un serveur en face pourrait négocier un trunk et ainsi avoir accès à tous les vlans. Il est donc conseillé de ne jamais utiliser le mode dynamic.

Le DTP peut également être désactivé :

~~~
Switch(config-if)# switchport nonegotiate
~~~

Mais cette commande n'a pas d'intérêt dans le cas où le mode dynamic n'est pas utilisé. Attention cependant, désactiver cette option (`no switchport nonegotiate`) entraînera une coupure de 3 secondes de la liaison.

### Scripting avec tclsh

Les appareils Cisco incluent tclsh, un shell contenant l'interpréteur Tcl.

Il peut permettre par exemple de rapidement exécuter des pings à de multiples destinations :

~~~
Switch# tclsh 
Switch(tcl)# foreach address {                  
+> 192.0.2.3      
+> 192.0.2.4      
+> 192.0.2.5      
+> } { ping $address repeat 3 }          

Type escape sequence to abort.
Sending 3, 100-byte ICMP Echos to 192.0.2.3, timeout is 2 seconds:
!!!
Success rate is 100 percent (3/3), round-trip min/avg/max = 5/10/21 ms
Type escape sequence to abort.
Sending 3, 100-byte ICMP Echos to 192.0.2.4, timeout is 2 seconds:
!!!
Success rate is 100 percent (3/3), round-trip min/avg/max = 1/7/15 ms
Type escape sequence to abort.
Sending 3, 100-byte ICMP Echos to 192.0.2.5, timeout is 2 seconds:
!!!
Success rate is 100 percent (3/3), round-trip min/avg/max = 1/2/5 ms
~~~

### Limiter l'impact d'une tempête de paquet avec storm-control

Lors d'une tempête de paquet, le LAN peut être fortement dégradé, voire devenir inaccessible. Pour empêcher ça, l'option `storm-control` peut être utilisée afin de limiter le trafic unicast inconnu, multicast, ainsi que broadcast entrant (depuis l'interface vers le switch). Par défaut, le storm-control est désactivé.

**Attention** : configurer le storm-control peut provoquer une coupure de quelques secondes sur l'interface où c'est configuré. Supprimer la configuration ne provoque pas de coupure.

Le `storm-control` se configure au niveau de l'interface sur laquelle la limitation doit être faite, peut utiliser 3 méthodes de détection différentes, et peut également définir 2 actions lors d'une détection :

~~~
Switch# conf t
Switch(config)# interface Gi0/1
Switch(config-if)# storm-control broadcast level 60.26 49.37
Switch(config-if)# storm-control multicast level bps 500m 400m
Switch(config-if)# storm-control unicast level pps 1m 500k
Switch(config-if)# storm-control action trap
Switch(config-if)# storm-control action shutdown
~~~

Ici, on a défini le seuil haut du trafic broadcast à 60.26% de la bande passante disponible sur l'interface et le seuil bas à 49.37%. On a défini le seuil haut du trafic multicast à 500Mbps et le seuil bas à 400Mbps. On a défini le seuil haut du trafic unicast inconnu à 1Mpps et le seuil bas à 500kpps. On a également défini les actions `shutdown` et `trap` : lorsqu'un des 3 seuils est atteint, l'interface passe en `errdisable` (voir [#ré-activer-un-port-désactivé-par-errdisable]()) et une trap SNMP est envoyée.

Le seuil haut indique le seuil à partir duquel le trafic choisi doit être bloqué. Le seuil bas est facultatif et indique le seuil en dessous duquel le trafic choisi ne sera plus bloqué ; s'il n'est pas indiqué, alors le trafic ne sera plus bloqué dès qu'il sera en dessous du seuil haut. Seul le trafic du type en question est bloqué, pas le reste.

Par exemple, `broadcast level 60.26 49.37` indique que tout le trafic broadcast sera bloqué dès qu'il atteint 60.26% de la bande passante disponible sur l'interface, et le trafic broadcast ne sera plus bloqué dès qu'il sera en dessous de 49.37% de la bande passante disponible sur l'interface.


## Cisco Catalyst 3750

### DHCP relay sur plusieurs VLANs

* [Exemple de mise en oeuvre de DHCP Relay sur 2 VLANs](http://www.cisco.com/en/US/tech/tk648/tk361/technologies_tech_note09186a0080114aee.shtml)

#### Côté Serveur DHCP

Côté serveur DHCP (serveur linux), configurer un subnet par VLAN. Penser aux routes permettant d'accéder à chaque réseau de chaque VLAN.
Le serveur DHCP pourra être dans un VLAN dédié.

#### Côté Switch

Pour activer le DHCP Relay, sur chaque interface VLAN, rajouter la directive `ip address helper <ip-server-dhcp>`.

Voici un exemple de création d'un VLAN avec l'adresse 192.0.2.50/24, faisant office de DHCP relay pour le serveur 198.51.100.2 :

~~~
Switch(config)# interface vlan 200
Switch(config-if)# ip address 192.0.2.50 255.255.255.0
Switch(config-if)# ip helper-address 198.51.100.2
Switch(config-if)# end
~~~

On répètera cette manipulation pour chaque VLANs et chaque subnet déclaré sur le serveur DHCP que l'on souhaite activer.

### Routage Inter-VLANs

L'activation du routage inter-VLANs se fait de la manière suivante :

~~~
Switch(config)# ip routing
~~~

Tous les VLANs seront routés entre eux. Il sera possible si besoin de limiter le routage inter-VLANs grâce aux ACLs.

### Définir une route par défaut

~~~
Switch(config)# ip default-gateway IP_routeur
~~~

ou

~~~
Switch(config)# ip route 0.0.0.0 0.0.0.0 IP_routeur
~~~

La première commande est à utiliser si `ip routing` n'est pas configuré. Si c'est configuré, il faut utiliser la deuxième commande.


## Cisco Nexus 3000

Exemples de numéros de modèles : `C3064PQ`, `C3172PQ`…

**Beaucoup de commandes sont similaires entre les Catalyst (IOS) et les Nexus (NX-OS). Seules les commandes qui diffèrent sont indiquées.**

### Changer le mode d'utilisation des ports QSFP

Pour un C3172PQ par exemple. Par défaut, les 6 ports QSFP sont en modes 4x10G.

Pour tous les passer en mode 1x40G :

~~~
Switch# conf t
Switch(config)# hardware profile portmode 48x10g+6x40g
~~~

Un redémarrage du switch est nécessaire pour que ce soit pris en compte.

### Infos d'une interface

L'obtention des infos d'une interface se fait de la même façon que sur les Nexus. La seule différence est l'orthographe du mot "interface", qui ne prend pas le "s" à la fin, contrairement aux Catalyst :

~~~
Switch# show interface
Switch# show interface status
Switch# show interface description
Switch# show interface trunk
Switch# show interface vlan <ID>
Switch# show interface e1/1
Switch# show interface e1/1 status
Switch# show interface e1/1 counter
Switch# show interface e1/1 counter errors   
Switch# show interface e1/1 switchport
Switch# show interface e1/1 trunk
~~~

### Gestion des VLANs

* Affecter une adresse IP à un VLAN :

~~~
Switch# conf t
Switch(config)# feature interface-vlan
Switch(config)# interface Vlan 1
Switch(config-if)# ip address 192.0.2.10/24
Switch(config-if)# no shutdown
~~~

Contrairement à IOS, sous NX-OS il faut d'abord activer la fonctionnalité "interface-vlan" avec la commande `feature interface-vlan` pour pouvoir configurer une SVI (Switch Virtual Interface).

### Configurer l'heure

* Gestion Daylight Savings Time (DST) :

~~~
Switch# conf t
Switch(config)# clock timezone UTC 1 0
Switch(config)# clock summer-time UTC 5 Sun Mar 02:00 5 Sun Oct 03:00
~~~

### Gérer les logs

* Avoir une présicion en microsecondes sur l’heure des logs :

~~~
Switch# conf t
Switch(config)# logging timestamp microseconds
~~~

* Ajouter la description de l'interface aux logs concernant une interface :

~~~
Switch# conf t
Switch(config)# logging message interface type ethernet description
~~~

* Envoyer les logs à un serveur syslog :

~~~
Switch# conf t
Switch(config)# logging server 192.0.2.10 7 facility local6
~~~

### Configurer SNMP

* Activer SNMP en écriture :

Si on veut limiter l’écriture SNMP par une IP source, et sur certains OID uniquement, il faut créer une `access-list` et un `rôle`, puis les indiquer à SNMP :

~~~
Switch# conf t
Switch(config)# ip access-list snmp-rw
Switch(config-acl)# 10 permit ip host 192.0.2.4 192.0.2.0/24 vlan 1 log
Switch(config-acl)# exit
Switch(config)# role name ifXTable_only
Switch(config-role)# rule 1 permit read-write feature snmp
Switch(config-role)# rule 2 permit read-write oid 1.3.6.1.2.1.31.1.1.*
Switch(config-role)# exit
Switch(config)# snmp-server community private rw
Switch(config)# snmp-server community private use-ipv4acl snmp-rw
Switch(config)# snmp-server community private group ifXTable_only
~~~

* Envoyer des traps SNMP :

Le terme "trap" englobe 2 types de trap SNMP : les requêtes "trap" et les requêtes "inform". La différence est que pour une requête "trap", l'agent SNMP qui l'envoie n'a aucun moyen de savoir si le manager SNMP l'a bien reçue ou non, alors qu'une requête "inform" sera ré-émise par l'agent SNMP tant qu'il n'a pas reçu d'acknowledgement de la part du manager SNMP lui indiquant que la requête inform a bien été reçue.

Si on veut envoyer des traps SNMP de type `inform` à l'hôte `192.0.2.10`, en utilisant la version SNMP `2c` et la communauté SNMP `private`, et en activant toutes les traps :

~~~
Switch# conf t
Switch(config)# snmp-server host 192.0.2.10 informs version 2c private
Switch(config)# snmp-server host 192.0.2.10 use-vrf default
Switch(config)# snmp-server enable traps
~~~

En activant toutes les traps, certaines sont trop verbeuses et inutiles, et peuvent être désactivées :

~~~
Switch(config)# no snmp-server enable traps config
Switch(config)# no snmp-server enable traps syslog
Switch(config)# no snmp-server enable traps link cieLinkDown
Switch(config)# no snmp-server enable traps link cieLinkUp
~~~

La première désactive l'envoi de trap à chaque changement de configuration sur le switch, la deuxième désactive l'envoi de trap SNMP avec le contenu du log pour chaque syslog généré. Les troisième et quatrième désactivent l'envoi de trap SNMP au format étendu Cisco (OID `SNMPv2-SMI::enterprises.9.9.276.0.*`) lorsqu'une interface passe down ou up, pour ne garder que celles au format IETF (`IF-MIB`) plus claires.


### STP : Spanning Tree Protocol

* Synchro immédiate : Spanning Tree Portfast => Spanning Tree Port Type Edge

Le nom "portfast" n'existe pas sous Nexus, c'est une notion de "port type" qui est utilisé. Lorsque le port est connecté à une machine finale, c'est un port de type `edge` (de bord, d'extrémité). 

Activer le Port Type Edge :

~~~
Switch(config)# interface Eth1/28
Switch(config-if)# spanning-tree port type edge
~~~

Désactiver le Port Type Edge :

~~~
Switch(config)# interface Eth1/28
Switch(config-if)# no spanning-tree port type edge
~~~

### Rate-limiting

Si on veut par exemple limiter un port fibre 1Gbps en 100Mbps :

On crée une policy de type `queuing` en sortie :

~~~
Switch(config)# policy-map type queuing 100M-output
Switch(config-pmap-que)# class type queuing class-default
Switch(config-pmap-c-que)# shape kbps 102400
~~~

On crée une policy de type `qos` en entrée :

~~~
Switch(config)# policy-map type qos 100M-input
Switch(config-pmap-qos)# class type qos class-default
Switch(config-pmap-c-qos)# police cir 102400 kbps
~~~

Puis on applique les deux policy sur l'interface souhaitée :

~~~
Switch(config)# int eth1/1
Switch(config-if)# service-policy type queuing output 100M-output
Switch(config-if)# service-policy type qos input 100M-input
~~~

Le `queuing` correspond à du traffic shaping avec lequel les paquets qui dépassent le seuil configuré sont placés en buffer : la latence peut augmenter, mais les paquets ne sont pas perdus (tant qu'ils ne dépassent pas la taille du buffer). Quant à lui, le `qos` correspond à du policing, avec lequel les paquets qui dépassent le seuil configuré sont perdus. Le traffic shaping n'est possible qu'en sortie, on est donc obligé d'utiliser du policing en entrée.

> Note : "sortie" et "entrée" sont définis du point de vue du switch. Le trafic en entrée est celui émis par le client vers le switch (upload par le client), alors que le trafic en sortie est celui émis depuis le switch vers le client (download par le client).

Voir les configurations appliquées :

~~~
Switch# show running-config ipqos 

policy-map type qos 100M-input
  class class-default
    police cir 102400 kbps bc 200 ms conform transmit violate drop
policy-map type queuing 100M-output
  class type queuing class-default
    shape kbps 102400


interface Ethernet1/1
  service-policy type qos input 100M-input
  service-policy type queuing output 100M-output
~~~

### SPAN : Switch Port Analyzer (Port mirroring)

Activer le port mirroring, en copiant la sortie des ports Eth1/1 à Eth1/3 vers le port Eth1/5 :

~~~
Switch(config)# interface ethernet 1/5
Switch(config-if)# switchport monitor
Switch(config-if)# exit
Switch(config)# monitor session 1 type local
Switch(config-monitor)# source interface ethernet 1/1-3 tx
Switch(config-monitor)# destination interface ethernet 1/5
Switch(config-monitor)# no shut
~~~

Désactiver le port mirroring en gardant la configuration :

~~~
Switch(config)# monitor session 1 type local
Switch(config-monitor)# shut
~~~

### Cron / tâches planifiées

Exemple pour sauvegarder périodiquement la configuration du switch :

On commence par activer la fonctionnalité "scheduler" :

~~~
Switch(config)# feature scheduler
~~~

On définit la tâche planifiée :

~~~
Switch(config)# scheduler job name backupConfig
Switch(config-job)# copy running-config ftp://<user>:<password>@<host>/<path>/cisco.txt vrf default
Switch(config-job)# exit
~~~

L'user et le password sont obligatoires. Pour un ftp anonyme, indiquer `ftp://anonymous:anonymous@…`

Puis on définit la période, par exemple tous les jours à 01h15 :

~~~
Switch(config)# scheduler schedule name backupConfig
Switch(config-schedule)# job name backupConfig
Switch(config-schedule)# time daily 01:15
~~~

### VPC : Virtual Port-Channel

Ressources : <https://www.packetcoders.io/what-is-cisco-vpc-virtual-port-channel/>, <https://www.networklife.net/images/sheets/Nexus-VPC.pdf>

* Explication globale :

Le Port-Channel est une technologie qui permet d'agréger plusieurs interfaces ensemble. Le trafic est ensuite réparti sur chacune des interfaces. Cette technologie permet une meilleure redondance, ainsi qu'une augmentation de la bande passante.

Cependant un Port-Channel doit se faire depuis un seul serveur vers un seul même switch.

Le Virtual Port-Channel permet de dépasser cette limitation en faisant un Port-Channel virtuel depuis un serveur vers 2 switchs différents. Les deux switchs continuent à être gérés et configurés indépendamment.

Pour fonctionner, le VPC est constitué de plusieurs composants :

* **Switchs pairs VPC** : switchs faisant partie du groupe VPC.
* **Ports membres VPC** : machines (switch ou serveur) branchées à des pairs VPC de façon redondante.
* **Lien VPC peer-link** : lien physique permettant de connecter les 2 switchs pairs VPC et de transpoter les BPDUs et adresses MAC entre eux. Il sert également à transporter le trafic de niveau 2 entre les switchs pairs VPC, soit en cas de défaillance d'une liaison vers un port membre, soit lors de communications entre machines non membres VPC (fonction d'un lien inter-switch classique). Il doit donc être suffisament dimensionné pour supporter ce potentiel trafic.
* **Lien VPC keepalive** : lien physique permettant, dans le cas où le VPC peer-link est down et que les switchs ne pourraient donc plus communiquer à travers celui-ci, de détecter les pairs VPC en mode "tous actifs"/"tous primaires". Dans cette situation, on peut avoir des paquets dupliqués ou d'autres problèmes. Il est donc important de le détecter afin de réagir de façon appropriée. Les ports de management peuvent être utilisés pour créer ce lien.
* **Ports orphelins** : un port qui utilise un des VLANs transportés sur le VPC peer-link mais qui n'est pas un membre VPC. C'est le cas d'une machine branchée de façon volontaire sur un seul switch, sans Port-Channel.

Dans le cas où le lien **VPC peer-link** est down, le lien **VPC keepalive** le détecte, et le **pair VPC** secondary coupe tous ses ports allant vers des **membres VPC** afin d'éviter des boucles réseau. Par défaut, les **ports orphelins** ne seront pas coupés, mais peuvent l'être automatiquement si on le souhaite (voir configuration ci-dessous). Les pairs VPC primaire et secondaire sont déterminés par défaut d'après l'adresse MAC, ou par une priorité configurée manuellement.

Le serveur ne sait pas qu'il est branché sur 2 switchs différents, et croit être sur un Port-Channel classique. Une configuration en [LACP](HowtoDebian/Reseau#mode-802.3ad-lacp) est donc suffisante sur celui-ci.

Voici un schéma avec l'ensemble de ces composants :

~~~
   ┌────────┐VPC peer-link┌────────┐          ┌───────────┐
   │        ├─────────────┤        │          │           │
   │  SW01  │VPC keepalive│  SW02  ├──────────┤  Serveur3 │
   │        ├─────────────┤        │   Port   │           │
   └──┬───┬─┘             └─┬───┬──┘ orphelin └───────────┘
      │   │                 │   │
      │   │                 │   │
      │   │  ┌───────────┐  │   │
      │   └──┤           ├──┘   │
      │   VPC│  Serveur1 │VPC   │
      │membre│           │membre│
      │      └───────────┘      │
      │                         │
   VPC│                         │VPC
membre│      ┌───────────┐      │membre
      └──────┤           ├──────┘
             │  Serveur2 │
             │           │
             └───────────┘
~~~

* Cas de défaillance d'une liaison vers un port membre :

Imaginons que Serveur1 veuille communiquer avec Serveur2, et que l'algorithme de LACP sur Serveur1 décide d'envoyer le paquet sur l'interface allant vers SW01.
Si tout fonctionne correctement, alors le paquet partira de Serveur1 pour passer par SW01, et arriver depuis SW01 vers Serveur2.

Cependant, dans le cas où le lien entre Serveur2 et SW01 est down, alors le paquet partira de Serveur1 pour passer par SW01, puis passera par le VPC peer-link pour arriver sur SW02, et enfin arriver depuis SW02 vers Serveur2.

* Configuration :

Sur SW01 et SW02, prenons :

* Eth1/48 comme lien VPC keepalive
* Eth1/49 comme lien VPC peer-link
* Eth1/10 comme port membre VPC pour Serveur1
* Eth1/11 comme port membre VPC pour Serveur2
* Eth1/12 comme port orphelin pour Serveur3

Et on utilisera des Port-Channel ayant les mêmes numéros que les interfaces physiques auxquelles elle sont rattachées.

On commence par activer LACP et VPC, sur SW01 et SW02 :

~~~
Switch# conf t
Switch(config)# feature vpc
Switch(config)# feature lacp
~~~

On crée une VRF pour le VPC keepalive, sur SW01 et SW02 :

~~~
Switch(config)# vrf context VPC_KEEPALIVE
Switch(config-vrf)# exit
~~~

On crée le domaine VPC et on indique les IPs des 2 pairs, qu'on configure plus tard sur l'interface dédiée au keepalive :

Sur SW01 :

~~~
Switch(config)# vpc domain 1
Switch(config-vpc-domain)# peer-switch
Switch(config-vpc-domain)# peer-keepalive destination 198.51.100.2 source 198.51.100.1 vrf VPC_KEEPALIVE
Switch(config-vpc-domain)# role priority 8192
Switch(config-vpc-domain)# exit
~~~

Sur SW02 :

~~~
Switch(config)# vpc domain 1
Switch(config-vpc-domain)# peer-switch
Switch(config-vpc-domain)# peer-keepalive destination 198.51.100.1 source 198.51.100.2 vrf VPC_KEEPALIVE
Switch(config-vpc-domain)# role priority 16384
Switch(config-vpc-domain)# exit
~~~

On a configuré une priorité plus faible sur SW01 que sur SW02, permettant de rendre SW01 primary, et SW02 secondary : le plus faible est prioritaire.

La commande `peer-switch` permet aux 2 switchs d'apparaître comme un seul même switch au niveau du protocole Spanning Tree, en leur faisant avoir le même bridge ID. En cas de perte de l'un des 2 switchs, cela permettra de n'avoir aucun recalcul de topologie Spanning Tree.

On crée le VPC peer-link, sur SW01 et SW02 :

~~~
Switch(config)# interface Ethernet1/49
Switch(config-if)# description Lien vers SWXX VPC peer-link
Switch(config-if)# channel-group 49 mode active

Switch(config)# interface port-channel49
Switch(config-if)# switchport mode trunk
Switch(config-if)# switchport trunk allowed vlan <1-4094>
Switch(config-if)# vpc peer-link
~~~

On crée le VPC keepalive, sur SW01 et SW02 :

~~~
Switch(config)# interface Ethernet1/48
Switch(config-if)# description Lien vers SWXX VPC keepalive
Switch(config-if)# channel-group 48 mode active

Switch(config)# interface port-channel48
Switch(config-if)# no switchport
Switch(config-if)# vrf member VPC_KEEPALIVE
Switch(config-if)# ip address 198.51.100.1/24  # IP sur SW01
Switch(config-if)# ip address 198.51.100.2/24  # IP sur SW02
~~~

**Attention** : Lors du branchement si le VPC est déjà configuré, il faut brancher en premier le lien `peer-link`, et ensuite le lien `keepalive`, pour éviter que le VPC secondary ne coupe ses ports membres VPC le temps du branchement complet. 

**Attention** : Si pour une maintenance on doit débrancher le lien `keepalive` entre les 2 switchs VPC, et notamment éteindre 1 des 2 switchs, il est **très important** de reconfigurer les priorités pour que le switch qu'on veut éteindre soit secondary. Sinon, si le switch restant allumé est secondary, il coupera tous ses ports membres VPC jusqu'à ce que le switch primary et le lien `keepalive` soient revenus, et donc tout ce qui est branché sur ce switch sera down et n'aura plus de redondance (puisque l'autre switch est éteint).

La configuration jusque là est une configuration de base, qu'il ne faudra plus toucher une fois en place.
À partir d'ici, la suite est une configuration qui peut être répétée autant de fois que nécessaire pour de multiples serveurs.

<a name="VPC-server"></a>

Enfin, pour chacun des serveurs, on configure les ports membres VPC. Pour faciliter la gestion, on utilise le numéro de port comme numéro de channel-group/port-channel et comme numéro de vpc :

Pour Serveur1, sur SW01 et SW02 :

~~~
Switch(config)# interface Eth1/10
Switch(config-if)# channel-group 10 mode active

Switch(config)# interface port-channel10
Switch(config-if)# vpc 10
Switch(config-if)# switchport access vlan <1-4094>
~~~

Pour Serveur2, sur SW01 et SW02 :

~~~
Switch(config)# interface Eth1/11
Switch(config-if)# channel-group 11 mode active

Switch(config)# interface port-channel11
Switch(config-if)# vpc 11
Switch(config-if)# switchport access vlan <1-4094>
~~~

La configuration des ports devra ensuite se faire sur les interfaces `port-channel` ; l'interface physique (ou les interfaces physiques, s'il y en a plusieurs) membre du port-channel héritera automatiquement de la configuration.

* Ports orphelins :

Dans notre cas, si le VPC peer-link est down, alors SW02 (qui est VPC secondary) coupera tous ses ports allant vers des membres VPC, soit Eth1/10 et Eth1/11. Serveur3 sera alors isolé et ne pourra communiquer qu'avec les autres éventuels ports orphelins.

Cependant si Serveur3 était configuré par exemple pour du bonding en mode active/backup ou en mode [ALB](HowtoDebian/Reseau#mode-alb) en étant branché à la fois sur SW01 et SW02, alors le failover ne se ferait pas pour Serveur3 et des problèmes de communications aurraient lieux. Dans ce cas, on souhaiterait que le lien entre Serveur3 et SW02 se coupe pour que la communication entre Serveur1, Serveur2 et Serveur3 fonctionne par SW01.

Pour ça, sur le port où est branché Serveur3, il faut configurer la commande suivante :

~~~
Switch(config)# interface Eth1/12
Switch(config-if)# vpc orphan-port suspend
~~~

Ainsi, le port sera automatiquement coupé quand le VPC peer-link sera down, et sera automatiquement rétabli lorsque le VPC peer-link sera de nouveau up.

### Limiter l'impact d'une tempête de paquet avec storm-control

Lors d'une tempête de paquet, le LAN peut être fortement dégradé, voire devenir inaccessible. Pour empêcher ça, l'option `storm-control` peut être utilisée afin de limiter le trafic unicast inconnu, multicast, ainsi que broadcast entrant (depuis l'interface vers le switch). Par défaut, le storm-control est désactivé.

Le `storm-control` se configure au niveau de l'interface sur laquelle la limitation doit être faite et peut également définir 2 actions lors d'une détection :

~~~
Switch# conf t
Switch(config)# interface Eth1/20
Switch(config-if)# storm-control broadcast level 43.62
Switch(config-if)# storm-control multicast level 45.3
Switch(config-if)# storm-control unicast level 42
Switch(config-if)# storm-control action trap
Switch(config-if)# storm-control action shutdown
~~~

Ici, on a défini le seuil du trafic broadcast à 43.62% de la bande passante disponible sur l'interface. On a défini le seuil du trafic multicast à 45.3% de la bande passante disponible sur l'interface. On a défini le seuil du trafic unicast inconnu à 42% de la bande passante disponible sur l'interface. On a également défini les actions `shutdown` et `trap` : lorsqu'un des 3 seuils est atteint, l'interface passe en `errdisable` (voir [#ré-activer-un-port-désactivé-par-errdisable]()) et une trap SNMP est envoyée.

Tout le trafic au-delà du seuil configuré pour le type de trafic choisi sera bloqué, mais celui en-dessous du seuil continuera à passer. Par exemple, `broadcast level 43.62` indique que seul 43.62% de la bande passante disponible sur l'interface peut être utilisée par du trafic broadcast, et le trafic broadcast au-dessus de ce seuil sera bloqué.

### Interdire l'exécution d'une commande donnée

On peut utiliser le gestionnaire d'évènement pour interdire l'exécution d'une commande ou d'un type de commande en particulier. Le wildcard `*` peut être utilisé (et est la seule expression régulière autorisée).

On peut par exemple vouloir interdire l'exécution de la commande `switchport trunk allowed vlan` suivi d'un numéro de vlan sans `add` ni `remove`, qui écrasera tous les VLANs du trunk.
Pour ça, il faut créer 2 règles autorisant chacune la commande `switchport trunk allowed vlan add *` et `switchport trunk allowed vlan remove *`, et une troisième qui bloque `switchport trunk allowed vlan *`. Sans l'action `event-default`, la commande sera bloquée. On peut également écrire un message de log lorsqu'un évènement se produit.

~~~
Switch# conf t

Switch(config)# event manager applet Trunk_AuthorizeAdd
Switch(config-applet)# event cli match "conf t ; int * ; switchport trunk allowed vlan add *"
Switch(config-applet)# action 1.0 event-default

Switch(config)# event manager applet Trunk_AuthorizeRemove
Switch(config-applet)# event cli match "conf t ; int * ; switchport trunk allowed vlan remove *"
Switch(config-applet)# action 1.0 event-default

Switch(config)# event manager applet Trunk_DoNotOverwrite
Switch(config-applet)# event cli match "conf t ; int * ; switchport trunk allowed vlan *"
Switch(config-applet)# action 1.0 syslog priority notifications msg Trunk_DoNotOverwrite event triggered
~~~

Si l'on sait ce que l'on fait, on peut vouloir contourner ponctuellement le blocage :

~~~
Switch# terminal event-manager bypass
~~~

Puis le réactiver une fois la commande souhaitée exécutée :

~~~
Switch# terminal no event-manager bypass
~~~

## Cisco Small Business (SMB)

Exemples de numéros de modèles : `SX550X`, `SG300`, `SG220`…

**Beaucoup de commandes sont similaires entre les Catalyst (IOS) et les Small Business. Si les commandes ne sont pas indiquées pour les Small Business, il est probable que ce soit les mêmes que celles des Catalyst.**

Les switchs Cisco Small Business Pro (par exemple, le modèle Cisco ESW 500) sont en fait d'anciens switchs Linksys. Ils n'ont pas de système IOS habituel.
Selon le modèle, la connexion par le port console ou par telnet donne seulement accès à un menu interactif permettant d'effectuer seulement quelques opérations de base. Dans ce cas, on préférera donc l'utilisation de l'interface HTTP.

Pour l'initialiser, suivre les instructions du Quick Start Guide. En résumé :

* Brancher un câble RJ45 sur un port quelconque
* Se connecter à l'adresse IP par défaut 192.168.10.2
* S'identifier avec cisco/cisco

### Mettre à jour le firmware

Le firmware est téléchargeable gratuitement sur <https://software.cisco.com/download/home>.

Il faut chercher le modèle du switch que l'on possède. Ce modèle est visible avec la commande `show system` :

~~~
Switch# show system
System Description:                       SX550X-24F 24-Port 10G SFP+ Stackable Managed Switch
[…]
~~~

On cherche alors `SX550X-24F 24-Port 10G SFP+ Stackable Managed Switch`.

On clique ensuite sur `Switch Firmware`, et toutes les versions apparaissent.

On vérifie la version IOS actuelle :

~~~
Switch# show version
Active-image: flash://system/images/image1.bin
  Version: 2.5.0.83
  MD5 Digest: 07968d912499cff5e8b07fdc24779854
~~~

On choisit la version souhaitée, et on télécharge le binaire.

Ensuite, on peut mettre à jour le switch de 2 manières différentes.

#### À travers l'interface web

On se connecte à l'interface web du switch, puis on va dans le menu Administration > File Management > Firmware Operations.

Si souhaité, on peut sauvegarder le binaire actuel en sélectionnant "Backup Firmware" pour Operation Type", et "HTTP/HTTPS" pour "Copy Method", puis le téléchargement débute.

Pour la mise à jour, on sélectionne "Update Firmware" pour "Operation Type", et "HTTP/HTTPS" pour "Copy Method".

On clique sur "Browse...", on sélectionne le binaire téléchargé précédemment, et on clique sur "Apply".

À la fin, l'interface web indique que l'opération a été terminée avec succès.

Il faut finir avec un redémarrage du switch pour que la mise à jour soit prise en compte. Il peut être fait depuis l'interface web dans le menu Administration > Reboot, ou en ligne de commande avec `reload`.

Après redémarrage, on vérifie que la version a bien été mise à jour :

~~~
Switch# show version
Active-image: flash://system/images/image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin
  Version: 2.5.9.54
  MD5 Digest: ca342eaaf641de230bc89ddf9783b513
~~~

#### En ligne de commande

1. **Pré-requis**

Le switch doit tout d'abord avoir une IP configurée. On peut par exemple configurer une IP dans le VLAN 1 comme indiqué [ici](#affecter-une-adresse-ip-%C3%A0-un-vlan), puis affecter l'interface qui sera branchée au serveur TFTP à ce VLAN 1 en s'aidant de [cette partie](#affecter-des-interfaces-ports-%C3%A0-un-vlan).

2. **Installer un serveur TFTP**

Un serveur TFTP est nécessaire pour transférer le binaire vers le switch.

On utilise `tftpd-hpa` :

~~~
# apt install tftpd-hpa
# vim /etc/default/tftpd-hpa

TFTP_USERNAME="nobody"
TFTP_DIRECTORY="/srv/tftp"
TFTP_ADDRESS="0.0.0.0:69"
TFTP_OPTIONS="--secure -c"

# /etc/init.d/tftpd-hpa restart
# mv image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin /srv/tftp/
# chown -R nobody:nogroup /srv/tftp/
~~~

On configure le serveur sur l'utilisateur `nobody`, et on ajoute l'option `-c` pour autoriser la création de nouveaux fichiers sur le serveur. On place ensuite le binaire dans le répertoire TFTP.

3. **Copier le binaire vers le switch**

Si souhaité, on peut sauvegarder le binaire actuel vers le serveur TFTP :

~~~
Switch# copy flash://system/images/image1.bin tftp://192.0.2.20/old-binary.bin
%COPY-I-FILECPY: Files Copy - source URL flash://system/images/image1.bin destination URL tftp://192.0.2.20/old-binary.bin
%COPY-W-TRAP: The copy operation was completed successfully

Copy: XXX bytes copied in 00:04:27 [hh:mm:ss]
~~~

Ensuite, on récupère la version téléchargée du binaire sur le switch :

~~~
Switch# boot system tftp://192.0.2.20/image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin
%COPY-I-FILECPY: Files Copy - source URL tftp://192.0.2.20/image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin destination URL flash://system/images/image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin
%COPY-W-TRAP: The copy operation was completed successfully

Copy: XXX bytes copied in 00:04:27 [hh:mm:ss]
~~~

On vérifie que la nouvelle version a bien été chargée. Elle ne sera active qu'après reboot :

~~~
Switch# show version
Active-image: flash://system/images/image1.bin
  Version: 2.5.0.83
  MD5 Digest: 07968d912499cff5e8b07fdc24779854
  Inactive after reboot
Inactive-image: flash://system/images/image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin
  Version: 2.5.9.54
  MD5 Digest: ca342eaaf641de230bc89ddf9783b513
  Active after reboot
~~~

Il faut finir avec un redémarrage du switch :

~~~
Switch# reload
~~~

À nouveau, on vérifie que la version a bien été mise à jour après redémarrage :

~~~
Switch# show version
Active-image: flash://system/images/image_tesla_hybrid_2.5.9.54_release_cisco_signed.bin
  Version: 2.5.9.54
  MD5 Digest: ca342eaaf641de230bc89ddf9783b513
~~~

### Changer le mode (trunk, access…etc) d'une plage de ports

Avec la WebUI ce n'est pas évident, mais il est bien possible d'appliquer une configuration à plusieurs ports d'un coup :
Dans la section « VLAN Management > Interface Settings » il suffit de configurer un port, puis en cliquant sur « Copy settings » en bas de la page, de renseigner la plage de ports à laquelle appliquer la même configuration.

### Changer le VLAN d'une plage de ports

Probablement dans un souci de cohérence, c'est également possible mais avec une méthode totalement différente…
Dans la section « VLAN Management > Port to VLAN » sélectionner le VLAN ID désiré, et cliquer sur Go. Puis cocher « Untagged » au lieu de « Excluded » pour tous les ports désirés, et valider avec « Apply ». 
L'application des modifications est visible dans « VLAN Management > Port VLAN Membership »

### CLI

Commandes similaires aux Catalyst :

~~~
Switch# copy running-config startup-config
Switch# sh vlan
Switch# sh interface gi1
Switch# sh interface status gi1
Switch# show interface switchport gi1

Switch# show fiber-ports optical-transceiver
Switch# show fiber-ports optical-transceiver detailed

Switch(config)# vlan 2
Switch(config-vlan)# name bob
Switch(config-vlan)# end

Switch(config)# no vlan 2
Switch(config)# end

Switch(config)# interface gigabitethernet0/13

Switch(config)# interface gi1
Switch(config-if)# switchport mode access 
Switch(config-if)# switchport access vlan 2
Switch(config-if)# end

Switch(config)# interface gi1
Switch(config-if)# switchport mode trunk
Switch(config-if)# switchport trunk allowed vlan add all
Switch(config-if)# end

Switch(config)# no lldp run
Switch(config)# no bonjour enable
Switch(config)# jumbo-frame 10000
Switch(config)# hostname foo
~~~

### Rate-limiting

Rate-limiter à 10 Mb/s :

~~~
Switch(config)# interface gi1
Switch(config-if)# traffic-shape 10000
Switch(config-if)# rate-limit 10000
Switch(config-if)# end
~~~

`traffic-shape` rate-limite en sortie, et `rate-limite` en entrée.

### Configurer SNMP

* Envoyer des traps SNMP :

Le terme "trap" englobe 2 types de trap SNMP : les requêtes "trap" et les requêtes "inform". La différence est que pour une requête "trap", l'agent SNMP qui l'envoie n'a aucun moyen de savoir si le manager SNMP l'a bien reçue ou non, alors qu'une requête "inform" sera ré-émise par l'agent SNMP tant qu'il n'a pas reçu d'acknowledgement de la part du manager SNMP lui indiquant que la requête inform a bien été reçue.

Si on veut envoyer des traps SNMP de type `inform` à l'hôte `192.0.2.10`, en utilisant la version SNMP `2c` et la communauté SNMP `private`, et en activant toutes les traps :

~~~
Switch# conf t
Switch(config)# snmp-server host 192.0.2.10 informs version 2c private
Switch(config)# snmp-server enable traps
~~~

Pour les Cisco SMB, il n'est pas possible de choisir ce qui doit envoyer ou pas une trap SNMP.

## Ansible

Voir [HowtoAnsible/Cisco](HowtoAnsible/Cisco)

## FAQ

### Switchs stackables (séries S)

Les switchs _-S Series_ permettent de stacker plusieurs switchs : c'est-à-dire que plusieurs switchs seront vus comme un seul,
offrant ainsi des facilités en termes d'administration (mais pas forcément en termes de sécurité). Cela se fait par exemple avec
plusieurs switch 2960-S et des modules Cisco FlexStack : ces modules s'ajoutent à l'arrière de chaque switch, il suffit ensuite
de les relier avec des câbles Cisco FlexStack.

Astuce : un switch stackable (S Series) est parfois moins cher qu'un non-stackable… et il peut pourtant très bien être utilisé tout seul !

### SSH Unable to negotiate

Lorsque ce type de message est reçu quand on tente de se ssh à un switch :

~~~
$ ssh root@192.0.2.1
Unable to negotiate with 192.0.2.1 port 22: no matching key exchange method found. Their offer: diffie-hellman-group1-sha1
~~~

Il faut se connecter en précisant que l'on veut utiliser l'offre proposée diffie-hellman-group1-sha1 :

~~~
$ ssh root@192.0.2.1 -o "KexAlgorithms diffie-hellman-group1-sha1"
~~~

## Liste des bugs connus

Pour avoir une liste des bugs connus, il faut chercher le modèle du switch et sa version actuelle sur <https://software.cisco.com/download/home>.

Une fois la version sélectionnée, un lien "Release Notes for XXX" est présent sous "Related Links and Documentation". Dans ces releases notes peuvent être indiqués les bugs connus sous le nom "Open Caveats" ou "Known Issues".

Plus d'infos sur un bug peuvent être trouvées sur <https://bst.cisco.com/bugsearch/> en indiquant son numéro

### Bug avec la commande "login on-success log"

Sur les Cisco Catalyst sous la version IOS `15.0(2a)EX5`, la commande `login on-success log` provoque un reboot du switch à chaque connexion d'un utilisateur.

### Bug lors de l'ajout d'un VLAN dans un trunk sur un peer-link entre 2 switchs en VPC

Ce bug a été constaté avec des switchs Nexus sous la version NXOS `7.0(3)I7(9)`, mais nous ne savons pas s'il est aussi présent sous d'autres versions ou pas.

Le bug se produit avec 4 switchs configurés en 2 paires VPC de 2 switchs, dans 2 domaines VPC différents :

~~~
┌───────────────────────────────────┐
│           VPC domain 1            │
│                                   │
│                                   │
│ ┌────────┐VPC peer-link┌────────┐ │
│ │        ├─────────────┤        │ │
│ │  SW01  │VPC keepalive│  SW02  │ │
│ │        ├─────────────┤        │ │
│ └───┬────┘             └────┬───┘ │
│     │                       │     │
└─────┼───────────────────────┼─────┘
      │                       │
      │                       │
      │                       │
      │                       │
┌─────┼───────────────────────┼─────┐
│     │                       │     │
│ ┌───┴────┐VPC peer-link┌────┴───┐ │
│ │        ├─────────────┤        │ │
│ │  SW03  │VPC keepalive│  SW04  │ │
│ │        ├─────────────┤        │ │
│ └────────┘             └────────┘ │
│                                   │
│                                   │
│            VPC domain 2           │
└───────────────────────────────────┘
~~~

Lorsque l'un des 2 liens entre `SW01-SW03` ou `SW02-SW04` est coupé (port `shut`, fibre coupée, fibre non présente, …) et que l'on ajoute un VLAN autorisé dans le trunk sur le VPC peer-link d'une paire, alors certaines destinations deviennent inaccessibles par certaines sources.

Par exemple : 

Le lien entre `SW01-SW03` est coupé pour une raison quelconque. Les machines `serveur1` et `serveur2` sont branchés en LACP à l'aide du VPC à la fois sur `SW01` et `SW02`. Les machines `serveur3` et `serveur4` sont branchés en LACP à l'aide du VPC à la fois sur `SW03` et `SW04`.

Les liens peer-link entre `SW01-SW02` et `SW03-SW04` sont configurés en trunk, avec seulement certains VLANs autorisés. Sur le lien peer-link `SW01-SW02`, je souhaite autoriser un nouveau VLAN :

~~~
SW01(config-if)# switchport trunk allowed vlan add 10
~~~

À partir de ce moment-là, le bug apparaît (il n'y a pas besoin de faire la même commande sur SW02 pour que le bug apparaîsse, et il apparaît peu importe le switch sur lequel la commande est faite) : les 4 machines n'arriveront plus à communiquer correctement. Par exemple, `serveur1` n'arrivera plus à parler à `serveur3` mais arrivera à parler à `serveur4` (et inversement), et `serveur2` n'arrivera plus à parler à `serveur4` mais arrivera à parler à `serveur3` (et inversement).

Le fait de revenir en arrière en levant le VLAN 10 autorisé n'y changera rien.

Le bug disparaîtra soit après un reboot de `SW02` (switch paire de celui sur lequel la configuration a été modifiée, et sur lequel la liaison vers l'autre domaine VPC est toujours UP), soit dès que le lien `SW01-SW03` sera de nouveau opérationel, et ne ré-apparaîtra pas si le lien est de nouveau coupé. Il n'y a aucun problème lorsque l'on ajoute un VLAN autorisé dans le trunk et qu'aucun des 2 liens n'est coupé. Le bug ré-apparaîtra seulement si un VLAN est ajouté aux autorisations lorsqu'un des liens `SW01-SW03` ou `SW02-SW04` est coupé.
