**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Tokyo Tyrant

Tokyo Cabinet est une base de données noSQL clé-valeur qui stocke ses données avec une méthode très simple (tirée de [DBM](http://en.wikipedia.org/wiki/Dbm) implémentation de Ken Thompson !). Sa dernière implémentation a été renommée en *Kyoto Cabinet*. Une surcouche réseau a été implémentée, nommée *Tokyo Tyrant* puis renommée en *Kyoto Tycoon*.

<http://fallabs.com/kyotocabinet/> [[BR]]
<http://fallabs.com/kyototycoon/>

## Installation

Sous Debian Squeeze, on trouve encore Tokyo Tyrant :

~~~
# aptitude install tokyotyrant
~~~

## Configuration

<http://fallabs.com/tokyotyrant/spex.html>

La configuration se trouve dans le fichier _/etc/default/tokyotyrant_.

Les données se trouvent dans _/var/lib/tokyotyrant/data/tokyotyrant.tch_

Par défaut sous Debian, Tokyo Tyrant n'est accessible que via une socket : _/var/run/tokyotyrant/tokyotyrant.sock_

Il est lancé ainsi :

~~~
/usr/sbin/ttserver -port 0 -dmn -pid /var/run/tokyotyrant/tokyotyrant.pid -log /var/log/tokyotyrant/tokyotyrant.log -host /var/run/tokyotyrant/tokyotyrant.sock /var/lib/tokyotyrant/data/tokyotyrant.tch#bnum=1000000
~~~

On peut également le rendre accessible via un port réseau :

~~~
SERVERPORT=1978
~~~

## Client

~~~
$ tcrmgr version
Tokyo Tyrant version 1.1.40 (323:0.91) for Linux
Copyright (C) 2007-2010 Mikio Hirabayashi
$ tcrmgr put -port 0 /var/run/tokyotyrant/tokyotyrant.sock foo bar
$ tcrmgr get -port 0 /var/run/tokyotyrant/tokyotyrant.sock foo
bar
$ tcrmgr list -port 0 /var/run/tokyotyrant/tokyotyrant.sock
foo
~~~

## PHP

~~~
# aptitude install php5-tokyo-tyrant
~~~

## Sessions PHP avec Tokyo Tyrant

~~~
tokyo_tyrant.session_salt="randomlongstring"
session.save_handler=tokyo_tyrant
session.save_path="tcp://127.0.0.1:1978"
~~~

## Sauvegardes

TODO

## Réplication

TODO

## Monitoring

TODO

