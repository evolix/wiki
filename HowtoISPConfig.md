# FAQ

## Changez le mot de passe d'un compte mail

On vérifie l'algoritme de hashage utilisé dans le mot de passe actuel du compte a changer le mot de passe :

~~~ (sh)
# mysql dbispconfig
[...]
MariaDB [dbispconfig]> SELECT * FROM mail_user WHERE email='toto@example.com'\G
[...]
                   email: toto@example.com
                password: $1$OmT7m5BF$lu/Wtf.NTjT.geWv7Rd42/
[...]
~~~

On sait que `$1` correspond à `md5`, donc on génère un nouveau mot de passe, puis on génère une empreinte (ici avec md5), on change le mot de passe et finalement on vérifie que le changement a bien été effectué.

~~~ (sh)
$ apg -n 1 -m 16                      # génération du mot de passe
ayredUmBeaHagHum
$ mkpasswd --method=md5
Password:
$1$JBPToAhF$8tkkeNjBEewMlTdqdBli6.
# mysql dbispconfig
[...]
MariaDB [dbispconfig]> UPDATE mail_user SET password='$1$JBPToAhF$8tkkeNjBEewMlTdqdBli6.' WHERE email='toto@example.com';
MariaDB [dbispconfig]> SELECT * FROM mail_user WHERE email='toto@example.com'\G
[...]
                   email: toto@example.com
                password: $1$JBPToAhF$8tkkeNjBEewMlTdqdBli6.
[...]
~~~