# Howto Ring

## Installation

<https://ring.cx/en/download/gnu-linux>

~~~
# apt install apt-transport-https
# echo "deb http://dl.ring.cx/ring-nightly/debian_9/ ring main" > /etc/apt/sources.list.d/ring.list
# cd /etc/apt/trusted.gpg.d
# wget 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3a9ea1193a97b548be1457d48919f6bd2b48d754' -O ring.asc
# chmod 644 ring.asc
# apt update
# apt install ring
~~~

## Configuration

* Créer un compte "random" pour avoir accès aux settings (roues crantées en haut à droite)
* Via Settings, aller dans Comptes et désactiver le compte "random"
* Créer un compte de type SIP avec adresse IP pour nom d'hôte + login + pass
* Dans Options avancées, changer le port local si l'on est derrière du NAT pour qu'il soit unique

## Téléphoner en SIP

Entrer le numéro dans le champ "recherche" puis valider... il devient disponible pour un appel



