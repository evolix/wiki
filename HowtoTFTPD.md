**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

Installation de tftpd-hpa : `sudo aptitude install tftpd-hpa.`[[BR]]

Ensuite il faut configurer le fichier de configuration `/etc/default/tftpd-hpa` et mettre « yes » à la
ligne _RUN_DEAMON_, et modifier la ligne _OPTIONS_ en ajoutant _-c_.
`OPTIONS="-c -l -s /var/lib/tftpboot"`[[BR]]
Le paramètre _-c_ permet d'autoriser la création de fichiers (indépendamment des droits du système de fichiers).

Démarrer le serveur.[[BR]]

` /etc/init.d/tftpd-hpa start `

Par défaut le répertoire de stockage est `/var/lib/tftpboot/` et il est accessible en lecture/écriture par _nobody_

