---
categories: virtualisation
title: Howto LXC
...

* Documentation : <https://linuxcontainers.org/lxc/manpages/>
* Statut de cette page : test / bookworm

[LXC](https://linuxcontainers.org/lxc/introduction/) est une interface en espace utilisateur pour piloter les fonctionnalités d'isolation du noyau Linux, permettant de créer des containers.

# Installation

~~~
# apt install lxc bridge-utils debootstrap

$ lxc-info --version
5.0.2
~~~

> Note: Avec Debian Buster, il faut rajouter le paquet lxc-templates

# Mise en place du réseau virtuel

Obligatoire si vous voulez un accès Internet dans vos conteneurs. Une des façons de faire et de monter un bridge qui contiendra tout les conteneurs. Ensuite via iptables, on fera du NAT pour router le réseau privé du bridge via l'interface principale (souvent eth0).

~~~
# cat /etc/network/interfaces
[...]
auto br0
iface br0 inet static
        address 192.0.2.254/24
~~~

~~~
# echo 1 > /proc/sys/net/ipv4/ip_forward
# /sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
~~~

# Installation d'un container

En choisissant de créer un conteneur avec `-t download`, cela vous permettra via un prompt de choisir quel type de distribution à installer.

~~~
# lxc-create -t download -n foo
~~~

On peut directement choisir quel type de distribution on veut.

~~~
# lxc-create -t debian -n foo
~~~

## Déclarer le réseau virtuel dans la configuration du conteneur

Exemple avec la machine `foo`, via le fichier `/var/lib/lxc/foo/config`

~~~
lxc.network.type = veth
lxc.network.link = br0
lxc.network.ipv4 = 192.0.2.10/24
lxc.network.ipv4.gateway = 192.0.2.254
lxc.network.name = eth0
lxc.network.flags = up
~~~

Pas de réseau (sauf lo) :

~~~
lxc.network.type = empty
~~~

Partage du réseau avec l'hôte

~~~
lxc.network.type = none
~~~


## Monter des répertoires dans le conteneur

Il faut utiliser l'option `lxc.mount.entry` dans `/var/lib/lxc/<CONTAINER>/config` avec la syntaxe `fstab`.

Par exemple :

~~~
lxc.mount.entry = /home home none bind 0 0
lxc.mount.entry = /srv srv none bind 0 0
~~~

On ne mets pas le `/` pour `home` et `srv` pour la destination. Ces chemins seront complétés avec le chemin de la racine du conteneur (`/var/lib/lxc/<CONTAINER>/rootfs`).


# Utilisation basique

Lister les conteneurs :

~~~
# lxc-ls
foo bar baz
~~~

On peut obtenir plus de détails avec l'option `--fancy`

~~~
# lxc-ls --fancy
NAME  STATE   AUTOSTART GROUPS IPV4          IPV6              
foo   STOPPED 0         -      -             -
bar   RUNNING 1         -      192.0.2.73    2001:DB8::73
~~~

Démarrer un conteneur :

~~~
# lxc-start -n foo -d
~~~

Entrer dans un conteneur :

~~~
# lxc-attach -n foo
~~~

Arrêter un conteneur :

~~~
# lxc-stop -n foo
~~~

Autres commandes utiles :

~~~
# lxc-info -n foo
# lxc-console -n foo
# lxc-halt -n foo
# lxc-destroy -n foo
~~~

Pour quitter un `lxc-console`. Il faut faire `<Ctrl+a q>`. Il semblerait cependant que cela ne fonctionne pas !

Pour exécuter une commande dans un conteneur, sans y rester attaché :

~~~
# lxc-attach -n foo -- command --with --arguments
~~~

# Divers / FAQ / Diagnostic

## systemd

Si systemd dans un container pose souci, on peut revenir à sysvinit.

~~~
# apt install sysvinit-core
~~~

## Erreur accès /dev

~~~
Couldn't open /dev/null: Permission denied
~~~

C'est parce que /dev est crée en 700 au lieu de 755. Pour fixer :

~~~
# chmod 755 /dev
~~~

## Failed to load config for foo

~~~
# lxc-ls 
Failed to load config for foo
~~~

Il faut mettre à jour la config :

~~~
# lxc-update-config -c /var/lib/lxc/foo/config
~~~


## Erreur de démarrage d'un conteneur LXC

### wait_on_daemonized_start

```
# lxc-start -n $container_name
lxc-start: $container_name: lxccontainer.c: wait_on_daemonized_start: 842 Received container state "ABORTING" instead of "RUNNING"
lxc-start: $container_name: tools/lxc_start.c: main: 330 The container failed to start
lxc-start: $container_name: tools/lxc_start.c: main: 333 To get more details, run the container in foreground mode
lxc-start: $container_name: tools/lxc_start.c: main: 336 Additional information can be obtained by setting the --logfile and --logpriority options
```

S'il y a un nouveau point de montage : vérifiez que le dossier de destination existe!

### Failed to set AppArmor label

Si un conteneur est stoppé et qu'il retourne ces erreurs :

```
# lxc-start -n $container_name
lxc-start: $container_name: lsm/lsm.c: lsm_process_label_set_at: 174 No such file or directory - Failed to set AppArmor label "lxc-container-default-cgns"
lxc-start: $container_name: lsm/apparmor.c: apparmor_process_label_set: 1097 Failed to change AppArmor profile to lxc-container-default-cgns
lxc-start: $container_name: sync.c: __sync_wait: 62 An error occurred in another process (expected sequence number 5)
lxc-start: $container_name: start.c: __lxc_start: 1951 Failed to spawn container "php73"
lxc-start: $container_name: tools/lxc_start.c: main: 330 The container failed to start
lxc-start: $container_name: tools/lxc_start.c: main: 336 Additional information can be obtained by setting the --logfile and --logpriority options
```

Alors, il faut réinstaller le paquet et apparmor et relancer le service systemd :

```
# apt reinstall apparmor
# systemctl restart lxc
# lxc-start -n $container_name
```

Note : le redémarrage du service LXC n'affecte pas le statut des conteneurs.

Pour éviter d'autres problèmes à l'avenir avec AppArmor, si le conteneur n'a pas vocation de sécurité, on peut le déconfiner dans sa configuration et le redémarrer :

```
# vim /var/lib/lxc/$container_name/config
+ lxc.apparmor.profile = unconfined
# lxc-stop -n $container_name && lxc-start -n $container_name
```

