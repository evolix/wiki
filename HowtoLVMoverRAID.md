---
title: Howto LVM
...

* Voir [HowtoLVM]()
* Voir [HowtoRAIDLogiciel]()

Pendant des années nous avons été réticents à utiliser du LVM sur des volumes RAID, effrayés par des rumeurs de grosses pertes de données… mais surtout prudents de ne pas empiler 2 technologies complexes pour quelque chose d'aussi critique que du stockage.

Puis, petit à petit, en utilisant le RAID logiciel (indispensable si l'on n'a pas de RAID hardware, pas si mal que ça) et le LVM (pratique dans pas mal de cas) indépendamment, on y vient.

## Création du RAID

On crée deux partitions `/dev/sda9` et `/dev/sdb9` de type "Linux raid autodetect" (FD).

~~~
# mdadm --create /dev/md8 --chunk=64 --level=raid1 --raid-devices=2 /dev/sda9 /dev/sdb9
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md8 started.

# mdadm --query /dev/md8
/dev/md8: 37.25GiB raid1 2 devices, 0 spares. Use mdadm --detail for more detail.

# mdadm --detail /dev/md8
/dev/md8:
        Version : 1.2
  Creation Time : Sat Nov 20 22:23:28 2010
     Raid Level : raid1
     Array Size : 39060920 (37.25 GiB 40.00 GB)
  Used Dev Size : 39060920 (37.25 GiB 40.00 GB)
   Raid Devices : 2
  Total Devices : 2
    Persistence : Superblock is persistent

    Update Time : Sat Nov 20 22:23:28 2010
          State : clean, resyncing
 Active Devices : 2
Working Devices : 2
 Failed Devices : 0
  Spare Devices : 0

 Rebuild Status : 2% complete

           Name : cap:8  (local to host cap)
           UUID : 66e00042:c24c606d:a276cb64:bdb476cc
         Events : 0

    Number   Major   Minor   RaidDevice State
       0       8        9        0      active sync   /dev/sda9
       1       8       25        1      active sync   /dev/sdb9

# cat /proc/mdstat
Personalities : [linear] [raid0] [raid1] [raid10] [raid6] [raid5] [raid4] [multipath] [faulty]
md8 : active raid1 sdb9[1] sda9[0]
      39060920 blocks super 1.2 [2/2] [UU]
      [==>..................]  resync = 14.7% (5757312/39060920) finish=7.2min speed=75992K/sec
~~~

## Création du LVM

~~~
# pvcreate /dev/md8
  Physical volume "/dev/md8" successfully created

# vgcreate vg-over-raid /dev/md8
  Volume group "vg-over-raid" successfully created

# lvcreate -L37G -nsrv vg-over-raid
  Logical volume "srv" created
~~~

## Utilisation

~~~
# mkfs.ext3 /dev/vg-over-raid/srv
# mount /dev/vg-over-raid/srv /srv
~~~
