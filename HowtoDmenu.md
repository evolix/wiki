---
title: Howto dmenu
categories: desktop menu suckless x
---

**Site officiel** : <https://tools.suckless.org/dmenu/>

`dmenu` est logiciel graphique (sur X) écrit en C pour générer un menu à partir d'une liste.

Certains gestionnaires de fenêtre peuvent utiliser `dmenu` pour lister l'ensemble les programmes dans la variable `PATH` et permettre à l'utilisateur de choisir le programme à exécuter. C'est une façon d'utiliser `dmenu`.

`dmenu` affiche un menu à partir de son entrée standard. Une ligne est un élément du menu. L'utilisateur peut sélectionner un ou plusieurs éléments. Les éléments choisis sont écrits sur la sortie standard.

## Installation

### Sous Debian

Le logiciel se trouve dans le paquet `suckless-tools`

~~~
# apt install suckless-tools
~~~

## Utilisation

Pour s'entrainer, on peut utiliser cette commande :

~~~ 
$ printf 'élément 1\nélément 2\nélément 3\nitem 1\nitem 2\nitem 3' | dmenu
~~~

Une barre apparaîtra en haut de l'écran avec une zone d'entrée de texte à gauche pour filter suivie des différents éléments. **`dmenu` capture toutes les entrées du clavier**. On peut alors entrer du texte pour filtrer les éléments.

`dmenu` est contrôlé uniquement avec le clavier. Quelques raccourcis :

* `Bas` ou `Droite` ou `Ctrl+n` : sélectionner l'élément affiché suivant ;
* `Haut` ou `Gauche` ou `Ctrl+p` : sélectionner l'élément affiché précédent ;
* `Début` ou `Ctrl+a` : sélectionner le premier élément affiché ;
* `Fin` ou `Ctrl+e` : sélectionner le dernier élément affiché ;
* `Entrée` : valider l'élément sélectionné et quitter ;
* `Échap` : quitter avec une erreur ;
* `Ctrl+Entrée` : valider l'élément sélectionné.

> Lorsque `dmenu` est invoqué en mode vertical, avec l'option `-l <n>`, les touches `Droite` et `Gauche` déplacent le curseur au lieu de changer l'élément sélectionné.

Options couramment utilisées :

* `-i` : recherche insensible à la case ;
* `-l <n>` : afficher le menu verticalement, en affichant `<n>` lignes en même temps ;
* `-p <str>` : définir un _prompt_.

> L'espace après les options `-l` et `-p` est nécessaire.

### Exemple : ouvrir des fichiers

~~~
$ find ~/Documents ~/Musiques | dmenu -i -l 22 | xargs xdg-open
~~~

### Exemple : parcourir une hiérarchie de fichiers

Fonction récursive en [Bash]() pour parcourir une hiérarchie de fichiers.

> `dmenu` peut être embarqué dans une fenêtre avec l'option `-w <window_id>`

~~~
dmenu_browser() {
    sel="$(ls -FL | grep '/$' | dmenu -i -l 11 -p "$(pwd)" -w "${WINDOWID}")"
    if [[ -d "${sel}" ]]
    then
        if cd "${sel}"
        then
            dmenu_browser
        fi
    fi
}
~~~

### Exemple : exécuter des commandes prédéfinies

Le fichier texte `~/.my_commands` contient :

~~~
xscreensaver-command -lock
xterm -title 'calculator' -e 'bc -l'
xterm -title 'irc' -e 'ssh -t my.host -- screen -r my_irc_session'
xclip -o -sel clip > ~/clipboard_"$(date -Isec)"
~~~

Choisir une commande à exécuter :

~~~
$ dmenu -i -l 11 -p 'run:' < ~/.my_commands | sh
~~~

> On peut _binder_ cette commande sur un raccourci clavier qui va bien dans son gestionnaire de fenêtre.
