---
categories: network web ha
title: HowtoTraefik
...

* Documentation : <https://doc.traefik.io/traefik/>

[Traefik](https://traefik.io/traefik/) est un proxy inverse écrit en Go qui supporte la grande majorité des technologies de cluster ([Swarm](/HowtoDocker.md#swarm), [Kubernetes](https://kubernetes.io/), [etc](https://doc.traefik.io/traefik/providers/overview/#supported-providers)).Cela lui permet de trouver automatiquement la configuration des services qui s'exécutent sur un cluster ou "Provider" pour leur transmettre les requêtes qu'il reçoit.

Traefik est une application [Stateless](/Glossaire.md#) qui peut fonctionner en HA (haute disponibilité) en lançant plusieurs instances sur un cluster.

## Installation

Comme Traefik est souvent utilisé pour rendre accessible les services d'un cluster, il est généralement installé sur le cluster lui aussi. pour cela il faudra configurer une stack pour docker swarm ou chart helm pour kubernetes (Voir <https://doc.traefik.io/traefik/getting-started/install-traefik/>).

On peut aussi lancer Traefik directement avec son binaire ou via un conteneur

~~~{.bash}
$ git ls-remote --tags https://github.com/traefik/traefik 
...
0a7964300166d167f68d5502bc245b3b9c8842b4	refs/tags/v2.10.7
$ wget https://github.com/traefik/traefik/releases/download/v2.10.7/traefik_v2.10.7_linux_amd64.tar.gz
# Wip / extraire et lancer ....
~~~

## Configuration

On peut configurer Traefik au démarrage avec la configuration statique ou pendant son exécution avec la configuration dynamique...

### Configuration statique

Documentation : <https://doc.traefik.io/traefik/getting-started/configuration-overview/#the-static-configuration>

On peut donner à Traefik sa configuration statique avec...

* Un fichier de configuration dans `/etc/traefik/traefik.yml` ou `$HOME/.config/traefik.yml`
* Des arguments
* Des variables d'environnement ENVs (liste complète <https://doc.traefik.io/traefik/reference/static-configuration/env/>)

La configuration est évaluée dans cet ordre (un ENV peut écraser un argument par exemple).

Sans rentrer dans les détails dans cette configuration, nous allons préciser...

* sur quels ports écoute Traefik avec des [EntryPoints](https://doc.traefik.io/traefik/routing/entrypoints/#configuration-examples)
* quels providers peut utiliser Traefik, exemple avec [docker](https://doc.traefik.io/traefik/providers/docker/#configuration-examples) 
* le [métriques](https://doc.traefik.io/traefik/migration/v1-to-v2/#metrics) pour les services et entryPoints 
* la gestion des [logs](https://doc.traefik.io/traefik/observability/logs/#logs)

Par exemple...

~~~{.yaml}
# Configuration Statique
entryPoints:
  web:
    address: ":80"
  websecure:
    address: ":443"
providers:
  docker:
    endpoint: unix:///var/run/docker.sock
    watch: true
  file:
    directory: /etc/traefik/dynamic-configuration
    watch: true
log:
  filePath: "/var/log/traefik.log"
~~~

### Configuration dynamique

Documentation : <https://doc.traefik.io/traefik/getting-started/configuration-overview/#the-dynamic-configuration>

Traefik récupère [périodiquement](https://doc.traefik.io/traefik/providers/overview/#configuration-reload-frequency) sa configuration (toute les 2 secondes par defaut) depuis le ou les providers configurés.

Liste des providers : https://doc.traefik.io/traefik/providers/overview/

> Remarque : le provider File permet de passer directement de la configuration via un fichier ou un dossier contenant des fichiers, https://doc.traefik.io/traefik/providers/file/#configuration-examples

**Attention** : Il faut configurer les providers dans la configuration statique (voir [Configuration statique](#configuration-statique) )

* Des [Routers](https://doc.traefik.io/traefik/routing/routers/#configuration-example) qui permette de connecter les requêtes aux services 
* Des [Services](https://doc.traefik.io/traefik/routing/services/#services) qui permettent de joindre les services...

Par exemple dans un provider de type file :

~~~{.yaml}
# Configuration dynamique 
http:
  routers:
    Router-1:
      # By default, routers listen to every entry points
      rule: "Host(`example.com`)"
      service: "my-service"
  services:
    my-service:
      loadBalancer:
        servers:
        - url: "http://<private-ip-server-1>:<private-port-server-1>/"
        - url: "http://<private-ip-server-2>:<private-port-server-2>/"
~~~

Dans le cas des providers comme docker et kubernetes, Treafik connaitra déjà les services, on aura seulement à configurer des routers directement dans la définitions des service dans swarm ou kubernetes...

Par exemple dans une stack docker compose :

~~~{.yaml}
services:
  nextcloud:
    image: nextcloud
    labels:
      - "traefik.http.routers.nextcloud.rule=Host(`nextcloud.example.com`)"
      - "traefik.http.routers.nextcloud.tls.certResolver=letsencrypt"
      - "traefik.http.services.nextcloud.loadbalancer.server.port=80"
~~~

Il faut spécifier le port du conteneur.

> Pour un service swarm, labels se trouve dans `deploy:` :

~~~{.yaml}
services:
  nextcloud:
    deploy:
      labels:
~~~

## Configuration avancée

### Logging

Documentation Logs : <https://doc.traefik.io/traefik/observability/logs/>
Documentation Access Logs : <https://doc.traefik.io/traefik/observability/access-logs/>
Pour configurer les logs de Traefik, il faut lui indiquer dans sa configration statique...

~~~{.yaml}
log:
  # ERROR par défaut : DEBUG > INFO > WARN > ERROR > FATAL > PANIC
  level: DEBUG
  filePath: "/var/log/traefik.log"
  
accessLog:
  filePath: "/path/to/access.log"
~~~

### Whitelist

On peut définir un middleware pour n'autoriser que certaines Ips

~~~{.toml}
# Configuration dynamique
    [http.middlewares.evolix-whitelist.ipWhiteList]
        sourceRange = ["31.170.8.0/21", "185.236.224.0/22"]
~~~

On peut ensuite utiliser ce middleware directement sur un entryPoint ou sur des services (TODO à venir)...

~~~{.toml}
# Configuration statique
  [entryPoints.websecure.http]
    middlewares = ["evolix-whitelist@file"]
~~~

### HTTPS et gestion des certificats TLS

Documentation : <https://doc.traefik.io/traefik/routing/routers/#tls>

dès le moment ou le router contient la section `tls:`, Traefik suppose que celui-ci est uniquement utilisé pour des requetes HTTPS uniquement.

~~~{.yaml}
## Configuration dynamique dans un fichier ( file provider )
http:
  routers:
    my-https-router:
      rule: "Host(`example.com`)"
      service: service-id
      # Configure le tls, tls: {} active le tls avec un cert autosigné généré par traefik
      tls:
        certResolver: letsencrypt
        domains:
          - main: "example.com"
            sans: 
              - "www.example.com"
~~~

> Remarque: Si on veut aussi repondre en HTTP, il faut configurer un autre router

Traefik peut s'occuper de la gestion des certificats TLS, il faut definir un "certResolver", par exemple pour letsencrypt :

~~~{.yaml}
# Configuration Statique
certificatesResolvers:
  letsencrypt:
    acme:
      email: batman@example.com
      storage: acme.json
      caServer = "https://acme-v02.api.letsencrypt.org/directory"
      httpChallenge:
        # entrypoint utilisé, doit etre sur le port 80 !
        entryPoint: web
  letsencrypt-staging:
    acme:
      email: batman@example.com
      storage: acme.json
      caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"
      httpChallenge:
        entryPoint: web   
~~~

> Remarque : fonctionne seulement pour une instance seule. Pour de la gérer les certificats sur un cluster HA, il faut utiliser la version entreprise ou bien utiliser laisser la gestion des certificats à un autre outils comme Cert manager( sur Kubernetes ) et donner directement le certificat à .

### Redirection vers HTTPS

(documentation à venir)

## Administration

## Optimisation

## Monitoring

### Nagios

### Munin

## Plomberie

## FAQ
