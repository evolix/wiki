---
title: Howto NSD
categories: network openbsd
...

## Documentation

* <https://www.nlnetlabs.nl/projects/nsd/>
* <http://www.openbsd.org/cgi-bin/man.cgi?query=nsd.conf>
* <http://www.openbsd.org/cgi-bin/man.cgi?query=nsd>
* <http://www.openbsd.org/cgi-bin/man.cgi?query=nsd-control>

NSD est un serveur DNS faisant autorité. Il ne dispose pas de fonctionnalité de récursion, en d'autres termes il peut servir des zones DNS, mais ne peut pas interroger d'autres serveurs. Il utilise des fichiers de zone dans un format similaire à celui de Bind ce qui a l'avantage de faciliter une migration.

Sous OpenBSD 6.3, la version de NSD est la 4.1.20
Sous Debian Stretch, la version de NSD est la 4.1.14

### Liens utiles

* <https://calomel.org/nsd_dns.html>
* <https://www.digitalocean.com/community/tutorials/how-to-set-up-dnssec-on-an-nsd-nameserver-on-ubuntu-14-04>

## Installation

Sous OpenBSD, nsd(8) fait partie du système de base.

Sous Debian, on l'installera via :

~~~
# apt-get install nsd
~~~

## Configuration

On configure le démon via le fichier `/var/nsd/etc/nsd.conf`(OpenBSD) ou `/etc/nsd/nsd.conf` (Debian).

Pour lancer nsd(8) :

OpenBSD

~~~
# rcctl start nsd
~~~

Debian

~~~
# systemctl start nsd
~~~

Pour vérifier la configuration :

~~~
# nsd-checkzone example.com /chemin/vers/zone/db.example.com
# nsd-checkconf /chemin/vers/configuration/nsd.conf
~~~

## Exemple de configuration master/slave simple

**La configuration est ici faite sous OpenBSD. Bien que globalement similaire sous Debian, la configuration tiendra compte de l'arborescence sous OpenBSD.**

Prenons 2 machines, que l'on définit comme étant 2 serveurs DNS, l'un master, l'autre slave.

### Master

La première chose à faire est de générer tous les certificats afin de permettre un contrôle distant via nsd-control(8) :

~~~
# nsd-control-setup
~~~

Voici le fichier `nsd.conf` sur le serveur avec l'adresse IP 192.0.2.53 (master) :

~~~
# vi /var/nsd/etc/nsd.conf
~~~

~~~
server:
    hide-version: yes
    verbosity: 1
    database: "" # disable database
    logfile: "/var/log/nsd.log"
    pidfile: "/var/nsd/run/nsd.pid"
    xfrdfile: "/var/nsd/run/xfrd.state"
    zonelistfile: "/var/nsd/db/zone.list"

## bind to a specific address/port
    ip-address: 192.0.2.53

remote-control:
    control-enable: yes

## tsig key example
key:
    name: "key.example.com."
    algorithm: hmac-sha256
    secret: "rMZVA3oOLyrk9Xn+aKe19aCqOf3xYv9kVw8M3crGkFE="

pattern:
    name: "talktoslave"
    notify: 192.0.2.54 key
    provide-xfr: 192.0.2.54 key

## master zone example.com
zone:
    name: "example.com"
    zonefile: "/master/db.example.com"
    include-pattern: "talktoslave"
~~~

Sur le serveur master on devra également définir la zone :

~~~
# vi /var/nsd/zones/master/db.example.com
~~~

~~~

$ORIGIN example.com.
$TTL 1800
@       IN      SOA     ns1.example.com.    dns.example.com. (
                        2016112700
                        2h
                        1h
                        5w
                        10m
                        )
@       IN      NS      ns1.example.com.
@       IN      NS      ns2.example.com.
ns1     IN      A       192.0.2.53
dns     IN      A       192.0.2.53
ns2     IN      A       192.0.2.54
@       IN      A       192.0.2.80
www     IN      CNAME   example.com.
@       IN      MX      10 mx1.foo.com.
@       IN      MX      20 mx2.foo.com.
~~~

On vérifie la configration ainsi que la zone :

~~~
# nsd-checkconf /var/nsd/etc/nsd.conf
# nsd-checkzone example.com /var/nsd/zones/master/db.example.com
zone example.com is ok
~~~

On active et démarre nsd :

~~~
# rcctl enable nsd
# rcctl start nsd
~~~

On peut désormais tester que tout fonctionne :

~~~
# dig ANY foo.com. @192.0.2.53
~~~

### Slave

Générons donc tous les certificats pour nsd-control(8) :

~~~
# nsd-control-setup
~~~

Et voici le fichier `nsd.conf` sur le serveur avec l'adresse IP 192.0.2.54 (slave) :

~~~
# vi /var/nsd/etc/nsd.conf
~~~

~~~
server:
    hide-version: yes          
    verbosity: 1
    database: "" # disable database 
    logfile: "/var/log/nsd.log"
    pidfile: "/var/nsd/run/nsd.pid" 
    xfrdfile: "/var/nsd/run/xfrd.state"
    zonelistfile: "/var/nsd/db/zone.list"

## bind to a specific address/port
    ip-address: 192.0.2.54

remote-control:
    control-enable: yes

## tsig key example
key:
    name: "key.example.com."
    algorithm: hmac-sha256
    secret: "rMZVA3oOLyrk9Xn+aKe19aCqOf3xYv9kVw8M3crGkFE="
  
pattern:
    name: "listentomaster" 
    allow-notify: 192.0.2.53 key.example.com.
    request-xfr: AXFR 192.0.2.53 key.example.com.

## master zone example.com
zone:
    name: "example.com"
    zonefile: "slave/db.example.com"
    include-pattern: "listentomaster"
~~~

On vérifie la configration :

~~~
# nsd-checkconf /var/nsd/etc/nsd.conf
~~~

On force maintenant le transfert de la zone :

~~~
# nsd-control transfer example.com
ok
~~~

Et enfin on teste le bon fonctionnement du slave :

~~~
# dig ANY example.com. @192.0.2.54
~~~

### Commandes utiles

Recharger une zone :

~~~
nsd-control reload example.com
~~~

Notifier un slave d'un changement sur la zone :

~~~
nsd-control notify example.com
~~~

Obtenir des informations sur l'état d'une zone

~~~
nsd-control zonestatus example.com
~~~
