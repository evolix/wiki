---
categories: web webapp saas
title: Howto Nextcloud
...

* [Documentation](https://docs.nextcloud.com/)
* [Notes de publication de versions](https://nextcloud.com/changelog/)
* [Dates de fin de vie des versions](https://github.com/nextcloud/server/wiki/Maintenance-and-Release-Schedule)

[Nextcloud](https://nextcloud.com/) est un Logiciel Libre qui permet de stocker des fichiers sur un serveur distant, communément appelé un « cloud ».
Ses fonctionnalités étendues et les nombreux plugins (nommés « Apps ») disponibles font de Nextcloud une plateforme collaborative modulaire et une alternative libre face aux logiciels privateurs comme Google Drive ou Dropbox.
On y trouve, entre autres, des fonctionnalités comme le partage de fichiers, l'édition collaborative, un calendrier, des contacts...

## Installation

[Documentation officielle d'installation](https://docs.nextcloud.com/server/latest/admin_manual/installation/)

Nous préconisons d'utiliser Nextcloud sur une machine en GNU/Linux Debian 11 (bullseye) avec [Apache](HowtoApache) + PHP, [MariaDB](HowtoMySQL) et [Redis](HowtoRedis).

Note : Certaines extensions PHP sont nécessaires pour le bon fonctionnement de Nextcloud. On peut les installer avec la commande suivante : 

~~~
# apt install bzip2 php-gd php-json php-xml php-mbstring php-zip php-curl php-bz2 php-intl php-gmp php-apcu php-redis php-bcmath php-imagick
~~~

On peut ensuite télécharger une archive sur <https://download.nextcloud.com/server/releases/>.

Pour l'installation, nous utilisons l'utilitaire en ligne de commande `occ` fourni par Nextcloud. Mais il est aussi possible de passer par l'interface web pour procéder à l'installation et la configuration de l'instance

Pour l'utiliser il faut se déplacer dans le répertoire racine de Nextcloud puis :

~~~
$ php occ -V
Nextcloud 24.0.3

$ php ./occ status --output json | grep -v 'Nextcloud is not installed'"

$ php ./occ maintenance:install --database mysql --database-name db_foo --database-user foo --database-pass PASSWORD --admin-user admin --admin-pass PASSWORD --data-dir /home/foo/data"

$ php ./occ config:system:set trusted_domains nextcloud.example.com
~~~

On configure ensuite Apache, PHP-FPM, MariaDB et Redis.


### Apache 

Pour [Apache-ITK](HowtoApache) on désactive la gestion des droits pour faciliter les mises à jour :

~~~
AssignUserID example example
~~~

On peut utiliser le vhost suivant :

~~~
<VirtualHost *:80 *:443>
  ServerName nextcloud.example.org
  
  # SSL
  SSLEngine on
  SSLCertificateFile    /etc/letsencrypt/live/foo/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/foo/privkey.pem
  
  # HSTS
  # Header always set Strict-Transport-Security "max-age=15552000"

  DocumentRoot /home/foo/nextcloud/

  <Directory /home/foo/nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>



  # SSL Redirect
  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteCond %{HTTP:X-Forwarded-Proto} !=https
  RewriteRule ^ https://%{HTTP:Host}%{REQUEST_URI} [L,R=permanent]

  # ITK
  AssignUserID foo foo

  # LOG
  CustomLog /var/log/apache2/access.log vhost_combined
  ErrorLog  /var/log/apache2/error.log

  # PHP
  php_admin_value sendmail_path "/usr/sbin/sendmail -t -i -f foo"
  php_admin_value open_basedir "/usr/share/php:/home/foo:/tmp"
    
</VirtualHost>
~~~


### Redis

Il ne faut pas oublier de [configurer la politique de gestion de la mémoire de Redis](/HowtoRedis#politique-sur-la-m%C3%A9moire) (directives `maxmemory` et `maxmemory-policy`), sinon, par défaut, Redis peut croître jusqu'à occuper toute la mémoire.

Pour que Nextcloud puisse accéder à Redis, on peut ajouter la configurations suivante dans `<DocumentRoot>/config/config.php` :

```
$CONFIG = array (
  (...)
  'filelocking.enabled' => true,
  'memcache.local' => '\OC\Memcache\APCu',
  'memcache.distributed' => '\OC\Memcache\Redis',
  'memcache.locking' => '\OC\Memcache\Redis',
  'redis' => array(
       'host' => 'localhost',
       'port' => 6379,
  ),
);
```


## Mise-à-jour

Dans Nextcloud, les mises-à-jour se font de version en version. S'il y a plusieurs versions de retard, il faut le mettre à jour autant de fois qu'il y a de versions de retard.

Les mises-à-jour peuvent être faites via l'interface web, ou en ligne de commande.

### Via l'interface web

Pour cela : cliquez sur le bouton rond en haut à droite > Paramètres > Vue d'ensemble > Ouvrir le système de mise-à-jour.
Puis suivez les instructions.

Attention, il ne faut pas fermer la page pendant la mise-à-jour, sinon vous resterez bloqué en mode maintenance.

A la fin des opérations de mise-à-jour, l'interface propose de quitter le mode maintenance.

En cas d'oubli, votre interface web sera [bloquée en mode maintenance](HowtoNextcloud#interface-web-bloquee-en-mode-maintenance).

### Via la ligne de commande

L'opération de mise à jour est réalisable en CLI via un outil dédié. Il va procéder aux mêmes opérations que via l'interface web.
Il faut appeller avec php le script `updater/updater.phar` à la racine du dossier de Nextcloud et se laisser guider par l'opération.
En fin de téléchargement de la nouvelle version, l'updater proposera d'exécuter des commandes de maintenances, puis de lever le mode maintenance.

~~~
$ php updater/updater.phar 
Nextcloud Updater - version: v24.0.0beta3-1-g67bf13b dirty

Current version is 24.0.3.

Update to Nextcloud 24.0.5 available. (channel: "stable")
Following file will be downloaded automatically: https://download.nextcloud.com/server/releases/nextcloud-24.0.5.zip
Open changelog ↗

Steps that will be executed:
[ ] Check for expected files
[ ] Check for write permissions
[ ] Create backup
[ ] Downloading
[ ] Verify integrity
[ ] Extracting
[ ] Enable maintenance mode
[ ] Replace entry points
[ ] Delete old files
[ ] Move new files in place
[ ] Done

Start update? [y/N] 
~~~


## MÀJ / Mise-à-jour spécifiques :

### Mise à jour de Nextcloud 25 à 26

La mise à jour de nextcloud 25 à 26 est un peu difficile sur Debian : Nextcloud 26 ne supporte pas PHP 7.4 (la version par défaut sur Debian Bullseye (11)).
On va donc devoir passer à PHP 8.1 ou 8.2.

Aussi, lorsqu'il se trouve dans un conteneur, PHP 8 semble plus restrictif sur les manières de se connecter à MySQL, ce qui amène à une erreur 500.

Voici la procédure pour mettre à jour vers 26 :

* Vérifier que la fonction PHP `system()` n'est pas dans le champ `disable_functions` de la configuration du PHP CLI utilisée `/etc/php/<version>/cli/`
* Vérifier [comment Nextcloud se connecte à MySQL](https://wiki.evolix.org/HowtoNextCloud#erreur-de-connexion-%C3%A0-mysql)
* **IMPORTANT** : Mettre à jour Nextcloud à la dernière version mineure de 25.x en CLI `$ php <DocumentRoot>/updater/updater.phar` (avec l'utilisateur Nextcloud)
* Faire l'[upgrade de Debian](/HowtoDebian/MigrationBullseyeBookworm) Bullseye vers Bookworm (11 -> 12) , ou la montée de version PHP de 7.4 vers 8.1 ou 8.2
* Mettre à jour Nextcloud 25 vers 26 `$ php <DocumentRoot>/updater/updater.phar` (avec l'utilisateur Nextcloud)
* Désactiver le mode maintenance.

La doc officielle : https://docs.nextcloud.com/server/25/admin_manual/maintenance/upgrade.html?highlight=upgrade


### Mise à jour de Nextcloud 22 à 23

La màj de Nextcloud 22 à 23 pu échouer avec l'erreur suivante :

~~~
Updating database schema
Doctrine\DBAL\Schema\SchemaException: The table with name 'COMPTE.oc_authorized_groups' already exists.
Update failed
~~~

COMPTE correspond ici au compte client.
La solution est de renommer la table en question après avoir confirmé qu'elle est bien vide :

~~~
$ mysql
~~~

Dans l'invite de mysql/mariadb :

~~~
SELECT * FROM COMPTE.oc_authorized_groups;
Empty set (0.001 sec)

RENAME TABLE COMPTE.oc_authorized_groups TO COMPTE.oc_authorized_groups_old;
Query OK, 0 rows affected (0.007 sec)
~~~

Une fois sorti de mysql/mariadb (avec la commande `exit`), il suffit de relancer le script `updater/updater.phar`.


### Mise à jour des applications foo et bar

~~~
$ php nextcloud/occ maintenance:mode --on
$ php nextcloud/occ app:update foo bar
$ php nextcloud/occ maintenance:mode --off
~~~


## Administration en ligne de commande (CLI)

Documentation officielle : <https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html>

NextCloud fournit une interface d'administration en CLI assez riche appelée `occ`. C'est un script PHP.

Pour l'utiliser, il faut l'exécuter en tant que l'utilisateur propriétaire de l'application Nextcloud, et se placer dans le répertoire d'installation de Nextcloud.

La syntaxe générale est la suivante :

~~~
<NEXTCLOUD-USER>:<NEXTCLOUD_PATH>$ php occ <MODULE>:<COMMAND> [OPTION ...]
~~~

Ou avec `sudo` :

~~~
<NEXTCLOUD_PATH>$ sudo -u <NEXTCLOUD-USER> php occ <MODULE>:<COMMAND> [OPTION ...]
~~~

**Note :** Pour une meilleure lisibilité, dans les sections suivantes, **on ne re-précisera pas le chemin et l'utilisateur Nextcloud**.

On peut lister les options d'une commande avec le module `help` :

~~~
php occ help <MODULE>:<COMMAND>
~~~


### Gestion des utilisateurs

#### Ajouter un compte

~~~
php occ user:add --display-name "<WEB_UI_NAME>" <USER>
~~~


#### Avoir les informations sur un compte

~~~
php occ user:info <USER>
~~~


#### Changer le mot de passe d'un compte

~~~
php occ user:resetpassword <USER>
~~~


#### Désactiver/Activer un compte

~~~
php occ user:disable <USER>
php occ user:enable <USER>
~~~

Pour savoir si le compte est activé ou non, voir [les informations sur ce compte](#avoir-les-informations-sur-un-compte).


#### Supprimer un compte

~~~
php occ user:delete <USER>
~~~


#### Lister les compte et les groupes

Lister les comptes :

~~~
php occ user:list
~~~

Lister les groupes et les comptes associés :

~~~
php occ group:list
~~~


#### Donner ou retirer les droits administrateur d'un compte

Pour donner les droits d'administrateur à un compte `foo`, l'ajouter dans le groupe `admin` :

~~~
$ php occ group:adduser admin foo
~~~

Pour retirer les droits, il suffit de faire l'opération inverse :

~~~
$ php occ group:removeuser admin foo
~~~

### Gestion des fichiers

Scanner les nouveaux fichiers :

~~~
$ php occ files:scan --all
$ php occ files:scan --path=/home/foo/data/bar
~~~

Scanner un "groupfolder" :

~~~
$ php nextcloud/occ groupfolders:scan 2
~~~

#### Vider **toute** la corbeille d'un compte 

~~~
$ php occ trashbin:cleanup admin foo
~~~

Si besoin de vider automatiquement les fichiers plus vieux de 20 jours, à insérer dans `config/config.php` :

~~~
  'trashbin_retention_obligation' => 'auto, 20',
~~~

## Utilisation

Les fichiers stockés sont accessibles :

* Via une interface web accessible à partir de n'importe quel navigateur (depuis un PC ou un smartphone) ;
* Dans un dossier local, grâce à l'application de synchronisation Nextcloud (disponible sur Linux, Mac et Windows) ;
* Sur un lecteur réseau avec le [protocole WebDAV](https://fr.wikipedia.org/wiki/WebDAV) (disponibles sur Linux, Mac et Windows) ;
* Via une application mobile [Android](https://f-droid.org/fr/packages/com.nextcloud.client/) ou [iPhone](https://apps.apple.com/fr/app/nextcloud/id1125420102).

Le choix des modes d'accès dépend de vos cas d'usage :

* Si vous utilisez Nextcloud pour stocker des données très volumineuses que vous ne voulez pas stocker localement, il est plus intéressant d'utiliser l'interface web et/ou un lecteur réseau WebDAV.
* Si vous utilisez Nextcloud comme solution de sauvegarde de vos fichiers locaux, vous aurez besoin de l'application de synchronisation.
* Si vous utilisez beaucoup les Apps Nextcloud, comme le calendrier ou l'éditeur collaboratif, vous utiliserez plutôt l'interface web.

Dans la plupart les cas d'usage, sauf stockage volumineux, nous conseillons d'installer l'application de synchronisation et d'utiliser l'interface web pour tout usage autre que du stockage ou du partage (collaboration, calendrier...).



### Synchronisation automatique

L'application de synchronisation copie et maintient à jour vos fichiers Nextcloud sur votre ordinateur dans un dossier local de votre choix, accessible comme d'habitude avec votre gestionnaire de fichiers.

Les fichiers synchronisés seront accessibles sans latence. Cependant, contrairement au lecteur réseau WebDAV, ils prendront de l'espace sur votre disque dur.

L'application s'exécute en tâche de fond et ajoute une icône dans la barre des tâches, à partir de laquelle on peut configurer la synchronisation.


#### Installation de l'application de synchronisation sur Debian

Note : Le nom des paquets peut varier selon votre distribution.

Sur **Debian**, le paquet à installer est [nextcloud-desktop](https://packages.debian.org/stable/nextcloud-desktop).

S'il n'est pas visible dans les icônes de la barre des tâches après l'installation, il faut le lancer manuellement via le menu des programmes et vérifier dans ses paramètres que l'option « Lancer au démarrage du système » est bien cochée.

Par exemple, pour le lancer sur XFCE : `Menu > Accessoires > Client de synchronisation Nextcloud`.

S'il ne démarre pas automatiquement au prochain redémarrage, vous devez l'ajouter manuellement aux applications démarrées automatiquement par votre environnement de bureau (KDE, Gnome, XFCE, Windows...).

Par exemple, pour XFCE :

* `Menu > Paramètres > Session et démarrage > Démarrage automatique d'application > +`
* Commande à exécuter : `/usr/bin/nextcloud --background`


#### Installation de l'application de synchronisation sur d'autres systèmes

Si votre système d'exploitation ne fournit pas de paquet pour installer l'application de synchronisation (**Windows, MacOS, distributions Linux n'ayant pas de paquet Nextcloud**), vous pouvez utiliser :

* Les binaires pré-compilés sur le [GitHub de Nextcloud](https://github.com/nextcloud/desktop).
* Le fichier d'installation ou le binaire sur le [site de Nextcloud](https://nextcloud.com/install/#install-clients).

Cependant, avec ce type d'installation, vous devez effectuer les mises-à-jour de l'application manuellement.


### Lecteur réseau (via le protocole WebDAV)

Cette solution permet de rendre les fichiers accessibles dans votre gestionnaire de fichiers via un lecteur réseau, avec un temps de latence qui dépendra de votre connexion à internet et de la taille des fichiers. Contrairement au client de bureau, ils ne prendront pas de place sur votre disque dur.

L'avantage du protocole WebDAV est d'être indépendant du système d'exploitation que vous utilisez.
En revanche, en cas de coupure d'accès à internet, vous ne pourrez pas accéder à vos fichiers.

L'url à renseignée est `https://nextcloud-demo.evolix.org/remote.php/dav/files/$login`.


#### Ajout du lecteur réseau WebDAV à votre gestionnaire de fichiers Linux

La plupart des gestionnaires de fichiers prennent en charge le protocole WebDAV nativement.
Cette solution a l'avantage de ne pas nécessiter l'installation de paquets supplémentaires.

En général, il suffit d'entrer l'URL suivante dans la barre de chemin de votre gestionnaire de fichiers, en adaptant `$login` :

* Thunar, Nautilus, Caja, Nemo : `davs://nextcloud-demo.evolix.org/remote.php/dav/files/$login`
* Dolphin : `webdavs://nextcloud-demo.evolix.org/remote.php/dav/files/$login`

Selon votre configuration ou la version de votre gestionnaire de fichiers, il faut parfois cliquer sur la barre de chemins pour pouvoir entrer l'URL (Dolphin), cliquer sur une icône à côté (Nemo, Caja), ou ajouter explicitement un nouveau lecteur réseau (Nautilus).

Sur Nautilus, il n'y a pas de barre de chemin, il faut ajouter un lecteur réseau en cliquant sur `+ Autres emplacements` dans le panneau latéral de gauche, puis renseigner l'URL WebDAV de l'instance Nextcloud dans « Connexion à un serveur ».


#### Intégration dans votre environnement de bureau Linux

Certains environnements de bureau proposent une intégration native de Nextcloud :

* Gnome (Nautilus) : [nautilus-nextcloud](https://packages.debian.org/stable/nautilus-nextcloud), (documentation)[https://docs.nextcloud.com/server/19/user_manual/pim/sync_gnome.html]
* KDE (Dolphin) : [dolphin-nextcloud](https://packages.debian.org/stable/dolphin-nextcloud)
* Cinnamon, Unity (Nemo) : [nemo-nextcloud](https://packages.debian.org/stable/nemo-nextcloud)
* Mate (Caja) : [caja-nextcloud](https://packages.debian.org/stable/caja-nextcloud)

Généralement, vous pouvez ensuite configurer votre compte Nextcloud dans les paramètres de votre bureau.

Par exemple, pour Gnome, cela se passe dans `Settings > Online accounts > Nextcloud`.

Ces intégrations ajoutent un lecteur réseau nextcloudavec le protocole WebDAV et permettent de synchroniser votre calendrier et vos contacts avec les applications du bureau.


#### Ajout du lecteur réseau WebDAV sur Windows

Dans l'explorateur de fichiers : `Clic droit sur PC > Connecter un lecteur réseau`.

Dans `Dossier`, indiquez `A COMPLETER`.


## Applications

Ce qui rend attractif Nextcloud est son côté modulaire à pouvoir intégrer des fonctionnalitées supplémentaires au stockage de fichiers grâce aux applications. C'est à dire qu'il est possible d'activer les commentaires, journal des modifications, liens de partages, etc.

Avec un compte d'administrateur de l'instance, la liste est présente à l'adresse `https://nextcloud-demo.evolix.org/settings/apps`

Il est également possible de les manipuler via des commandes en CLI grâce à `php occ [options] [arguments]`

Par exemple :

- Voir les applications installées : `php occ app:list`
- Désactiver une application : `php occ app:disable monitoring`
- Supprimer une application : `php occ app:remove serverinfo`


### Group folders

Pour gérer les partages, nous préconisons l'utilisation du plugin [Group folders](https://apps.nextcloud.com/apps/groupfolders).

Le plugin « Group folders » permet de créer des groupes de partage et de configurer les droits d'accès par groupe dans l'interface (bouton « Dossiers du groupe » dans l'Administration de l'interface web).

Sans ce plugin, on peut créer un répertoire et le partager avec certains droits. Cependant, ce n'est pas pratique : les droits sont gérés par une seule personne qui pourrait les retirer par erreur, et l'on sera embêté si l'on doit supprimer cet utilisateur dans le futur.

~~~
php occ app:enable groupfolders
~~~

### Agenda

L'application [Nextcloud Calendar](https://apps.nextcloud.com/apps/calendar) permet de gérer un agenda via l'interface web ou en synchronisant les évènements avec le protocole [CalDAV](https://fr.wikipedia.org/wiki/CalDAV).

* [Documentation utilisateur](https://docs.nextcloud.com/server/latest/user_manual/fr/groupware/calendar.html)
* [Nextcloud Calendar sur Github](https://github.com/nextcloud/calendar)

Voici un certain nombre de fonctionnalités :

- Gestion de plusieurs agendas
- Partage de calendriers au sein de Nextcloud ou externes via WebCal
- Invitation par email aux évènements
- Rappels par email ou dans l'interface web
- Gestion des (in)disponibilités

Son installation en mode CLI :

~~~
php occ app:enable calendar
~~~

#### Gestion des invitations par email

On peut évidemment inviter des contacts internes à Nextcloud, un email d'invitation est envoyé et l'on peut accepter l'invitation via le lien Nextcloud permettant une intégration de la réponse directement dans son agenda et de la réponse dans l'agenda de l'organisateur. Si l'on utilise l'application "Nextcloud Mail" on pourra répondre à l'invitation directement en affichant l'email, et de même avec d'autres clients email comme Thunderbird qui gèrent les invitations grâce au protocole iMIP (iCalendar Message-Based Interoperability Protocol) défini dans la [RFC 4047](https://www.rfc-editor.org/rfc/rfc6047.txt).

##### Envoi d'une invitation

Par défaut Nextcloud envoie des emails contenant des liens vers Nextcloud pour répondre directement, ainsi qu'une pièce jointe `event.ics` au format iCal défini dans la [RFC 5545](https://www.rfc-editor.org/rfc/rfc5545.txt).

Si l’instance n’est pas publique, il est possible de supprimer les liens vers Nextcloud ainsi :

~~~
php occ config:app:set dav invitation_link_recipients --value="no"
~~~

Il est aussi possible de n'autoriser les liens que pour certains domaines ou même adresse :

~~~
php occ config:app:set dav invitation_link_recipients --value="example.com,example.org,email@sub.example.org"
~~~

##### Réception d'une invitation

L'email d'invitation reçu doit donc contenir un attachement au format iCal.

Si on utilise l'application "Nextcloud Mail" on devrait pouvoir accepter/décliner l'invitation directement en affichant l'email.

Cela affiche : «  Vous avez été invité à un événement » avec des boutons « Accepter », « Décliner », « Accepter provisoirement » et « Plus d'options ».

Quelques remarques ou bugs :

* Attention, cela ne sera possible qu'une seule fois car la réponse est apparemment enregistrée par Nextcloud Mail : au lieu d'afficher une demande de réponse, cela affichera par exemple «  Vous avez accepté cette invitation » ou «  Vous avez décliné cette invitation ». Si l'on change d'avis, il faudra ouvrir l'évènement dans son agenda (voir plus bas)
* Si l'évènement est dans le passé, il ne sera plus possible d'y répondre, cela affiche : «  Cet événement est dans le passé. »
* À confirmer : si votre email n'est pas dans le champ `ATTENDEE` du fichier iCal, la demande d'invitation ne sera pas affichée, cf <https://github.com/nextcloud/calendar/issues/6304>
* La notification de réponse à une invitation externe ne semble pas envoyée, ce qui est clairement un bug cf l'[issue 6803](https://github.com/nextcloud/maim/issues/6803) : c'est désormais corrigé pour Calendar depuis juillet 2024 via les dernières versions de Nextcloud (29.04, 28.08) et pour Mail 3.7.8 (août 2024).

Si l'on utilise un autre client mail, il faut s'assurer que l'évènement est bien envoyé dans l'agenda Nextcloud et qu'une notification de réponse est bien envoyée à l'organisateur.

##### Réception d'une réponse à une invitation

La réponse à une invitation doit donc contenir un attachement au format iCal.

Si on utilise l'application "Nextcloud Mail", celle-ci va afficher suivant la réponse :

* « Foo a accepté votre invitation »
* « Foo a décliné votre invitation »
* « Foo a accepté provisoirement votre invitation »

La réponse s'intègre bien dans l'agenda Nextcloud mais après un certain délai.
Si un invité change d'avis, un nouvel email avec attachement iCal doit être reçu, elle s'intègrera aussi après un certain délai.

Le traitement des réponses est a priori effectué par le BackgroundJob « IMipMessageJob » lancé grâce aux crons.
Par contre, un intervalle d'1h est codé en dur, ce qui explique le « certain délai ».
Un hack est de modifier le fichier `apps/mail/lib/BackgroundJob/IMipMessageJob.php` et mettre `>setInterval(60 * 5)` pour que cela soit traité tous les 5 minutes.

Attention, si l'évènement est dans le passé, les réponses ne semblent plus prises en compte.

Si l'on utilise un autre client mail, il faut s'assurer que la réponse remonte bien dans l'agenda Nextcloud.
Et cela pose la question du traitement des réponses pendant que le client email n'est pas ouvert.

##### Modification de l'évènement

Si l'on modifie un évènement déjà créé, en tant qu'organisateur, cela renvoie une nouvelle invitation : « Invitation mise à jour ».

Par contre cela garde le statut précédent des participations ce qui peut être trompeur, cf <https://github.com/nextcloud/calendar/issues/6305>

##### Si je change d'avis

On parle d'un évènement organisé par une personne externe à Nextcloud.

On a vu qu'il n'est plus possible de le faire via l'email d'invitation reçu, il faut aller dans le Calendrier.

Ce changement d'avis envoie bien un email : l'organisateur externe n'est pas notifié. Attention cela fonctionne uniquement depuis juillet 2024 via les dernières versions de Nextcloud (29.04, 28.08).

##### Si mon invité propose une autre date

Cela semble non traité (test avec Google), simplement sa réponse est prise en compte et seul le texte de l'email peut indiquer la nouvelle proposition (ou il faut lire « manuellement » l'attachement iCal). Cf <https://github.com/nextcloud/mail/issues/10077>

### Contacts

Utilisant le protocole CardDAV avec un seul carnet d'adresse, ses principales fonctionnalités sont :

- Partage d'un contact au format vCard
- Synchronisation possible avec DavX
- Intégration avec d'autres applications issue de nextcloud : mail, calendrier, ...
- Intégration avec d'autres applications tierces : rainloop
- Créer des groupes de contacts
- Importer des contacts au format vCard
- Rappel des dates d'anniversaires
- Pleins d'informations de contacts disponible (nom, prénom, adresse mail, téléphone, adresse postale, photo, métier, type de travail, etc)
- Ajout d'informations personnalisées (étiquettes, année, ...)

Son installation en mode CLI :

~~~
php occ app:enable contacts
~~~



## FAQ

### Interface web bloquée en mode maintenance

C'est une erreur courante lorsque l'on oublie ou que l'on se trompe lors de la dernière étape de la mise-à-jour via l'interface web.

Pour être sûr que la mise-à-jour s'est bien déroulée, on peut la relancer :

~~~
$ php occ upgrade
~~~

Puis, on sort du mode maintenance :

~~~
$ php occ maintenance:mode --off
~~~


### Erreur de connexion à MySQL

Vérifier `dbhost` dans `<DocumentRoot>/config/config.php` :

~~~
<?php
$CONFIG = array (
(...).
'dbhost' => 'localhost',
~~~

* La valeur `localhost` indique à Nextcloud de se connecter à MySQL via le socket. Si on utilise PHP dans un conteneur, il faut vérifier que le socket est bien « bind mounté » dans le conteneur.
* Sinon, on peut mettre `127.0.0.1`, qui indique à Nextcloud de se connecter à MySQL en TCP (un peu plus lent).


### Connecteur ONLYOFFICE

On utilisera un serveur ONLYOFFICE DOC, si besoin de support voir <https://www.onlyoffice.com/en/docs-enterprise-prices.aspx>

### Synchronisation automatique pour smartphone

C'est possible de le faire pour les applications calendar et contacts dont l'application [DAVx5](https://www.davx5.com/faq/what-does-davx5-stand-for) est disponible sur [F-droid](https://f-droid.org/fr/packages/at.bitfire.davdroid/) et après l'avoir lancé en acceptant les autorisations, toucher le bouton rond " + " pour ajouter un compte.

Plusieurs choix sont possibles où seulement le 2ème point "Connexion avec une URL et un nom d'utilisateur" est intéressante
Il y aura besoin de renseigner ces informations dans les champs respectifs :

- URL de base : https://nextcloud.example.org/remote.php/dav/
- Nom d'utilisateur : BAR
- Mot de passe : PASSWORD

Enfin, acceptez les propositions des pages suivantes.
Par défaut, rien n'est sélectionné alors choisissez ce que vous souhaitez autoriser.
Pensez aussi à sélectionner les agendas ou calendriers à synchroniser.

Si la synchronisation n'est pas immédiate, soit il y a besoin d'attendre la prochaine heure de synchronisation, soit que des autorisations sont manquantes.


### Autoriser la synchronisation de fichiers .htaccess

Par défaut, Nextcloud met en liste noire les fichiers .htaccess de la synchronisation car ce n'est pas recommandé de synchroniser ce genre de fichiers.

Néanmoins, il est possible de lever cette limitation en ajoutant la directive suivante au fichier `config/config.php`

~~~
'blacklisted_files' => [''],
~~~


### Si bloqué sur l'erreur `Step 4 is currently in process. Please call this command later.`

Débloquer la situation avec `occ maintenance:repair` et relancer la mise à jour

~~~
$ php nextcloud/occ maintenance:repair
$ php nextcloud/updater/updater.phar
~~~

### Langue par défaut 

À insérer dans `config/config.php` :

`'default_language' => 'fr',`

### Fichiers données en exemple

Il y a par défaut des fichiers donnés en exemple qui sont copiés dans le compte de chaque nouvel utilisateur. On peut vouloir copier d'autres fichiers à la place ou même n'en copier aucun. Pour ne rien copier, il faut insérer cette ligne dans `config/config.php` :

`'skeletondirectory' => '',`

### Microsoft Office

Avec les logiciels Microsoft, on peut être amené à avoir ce genre de message d'erreur qui empêche la sauvegarde d'un fichier dans un partage Nextcloud via WebDAV :
`Microsoft Office a bloqué l'accès aux $URL, car la source utilise une méthode de connexion qui peut être non sécurisée.`

Microsoft a décidé de bloquer les connections (même chiffrées) qui utilisent le [basic access authentication](https://en.wikipedia.org/wiki/Basic_access_authentication) car ils considèrent qu'utiliser un identifiant avec [un mot de passe n'est plus assez sécurisé](https://learn.microsoft.com/fr-fr/microsoft-365-apps/security/basic-authentication-prompts-blocked) pour connecter une application à un service web.

Un [rapport de bug](https://github.com/nextcloud/server/issues/41192) est ouvert chez Nextcloud. Actuellement la seule solution est de contourner cette sécurité au niveau de Windows en rajoutant le domaine de l'instance Nextcloud dans la liste blanche des domaines autorisés à faire de l'authentification basique, comme [documenté par Microsoft](https://learn.microsoft.com/fr-fr/microsoft-365-apps/security/basic-authentication-prompts-blocked). C'est une [solution testée avec succès](https://github.com/nextcloud/server/issues/41192#issuecomment-1992884882) par d'autres utilisateurs de Nextcloud.
