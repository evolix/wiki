**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Gratuitous ARP

## Compilation de send_arp.c

send_arp.c est le code source de l'utilitaire fourni dans HeartBeat pour générer des Gratuitous ARP.

~~~
$ wget <http://hg.linux-ha.org/dev/raw-file/7ea7c05d3ed3/tools/send_arp.linux.c>
~~~

Commentez la ligne 348 et 366, puis compiler avec gcc (vous devez avoir le paquet build-essential d'installé) : 

~~~
gcc send_arp.linux.c -o send_arp
~~~

Pour générer un gratuitous ARP : 

~~~
send_arp -U -c 3 -I eth3 IP && send_arp -A -c 3 -I eth3 IP
~~~