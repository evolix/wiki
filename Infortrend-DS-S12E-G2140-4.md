**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Infortrend DS S12E-G2140-4

Méthode d'administration possible : port console, telnet, ssh, <http(s)>

## Informations utiles

Le mot de passe par défaut est vide, quand on vous demande un mot de passe, appuyer juste sur « entrer ».[[BR]]

[Manuel](http://viproom.infortrend.com.tw/Datatree/EonStor%20DS/ESDS%20B12E-R_G2140/Operation%20Manual/ESDS_B12E_HMN_v1.0.zip)

# Configuration basique étape par étape

Le but de ce petit « tutoriel » consiste à créer un device iSCSI qui sera accessible pour un serveur.

## Connexion avec câble série

Paramétrage minicom : 

~~~
# Fichier généré automatiquement - utilisez « minicom -s »
# pour changer les paramètres
pu port             /dev/ttyUSB0
pu baudrate         38400
pu bits             8
pu parity           N
pu stopbits         1
pu rtscts           No
~~~

Ce qu'il faut retenir, par défaut le port COM est configuré à une vitesse de 38400 bauds/sec. À une vitesse inférieur ça ne fonctionne pas !

## Connexion en telnet

Par défaut toutes les interfaces sont en DHCP, connectez l'interface d'administration à votre réseau, et regarder quelle IP votre serveur DHCP lui a affecté.
Il suffit ensuite de se connecter en telnet et sélectionner le mode VT100 : 

~~~
telnet adresse_ip
~~~

## Création d'un disque logique

Allez dans la section « view and edit Logical drives » et créer le RAID voulu (type de RAID, disque en SPARE, etc ...)

## Création d'un volume logique

Allez dans la section « view and edit logical Volume » et créer le volume logique composé du RAID précédemment crée. Enfin partionnez-le comme voulu (une partition minimum).

## Affecter un volume logique à un LUN

Allez dans la section « view and edit Host luns » et affecter sur un channel choisi, un volume logique. Le volume sera ainsi accessible sur ce channel.

## Connaître l'adresse IP d'un channel / Configuration statique

Comme les interfaces sont configurées en DHCP, on peut savoir quelle adresse IP est affectée en allant dans :

1. view and edit Configuration parameters
1. Communication Parameters
1. Internet Protocol (TCP/IP)   
1. Noter l'adresse IP du channel.

On peut aussi éditer l'interface de façon à passer en statique au lieu de DHCP.

## Monter le device iSCSI sous GNU/Linux

Cf. [wiki:HowtoISCSI]

# Interface Web

Le SAN dispose d'une interface Web pour l'administrer. Cette interface est très lente à utiliser, préférez telnet ! La seule chose intéressante est qu'elle indique l'état de santé du SAN (état des ventilateurs, température CPU, ...), mais on peut aussi le voir avec telnet ;-)

[[Image(webinterface.png, 33%)]]

# Divers paramètres 

## Changer le mot de passe

1. system Functions
1. change Password