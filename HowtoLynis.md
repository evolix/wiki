Lynis est un outil d'audit et de durcissement de sécurité.

Attention, certaines fonctionnalités n'existent que dans la version Entreprise payante (<https://cisofy.com/compare/lynis-and-lynis-enterprise/>). Toutes les fonctionnalités décrites sur cette page sont disponibles dans la version libre.

Liens externes :

* Présentation : <https://cisofy.com/lynis/>
* GitHub : <https://github.com/CISOfy/lynis>
* Documentation :
    * <https://cisofy.com/documentation/lynis/>
    * <https://cisofy.com/documentation/lynis/configuration/>
* Role Ansible : <https://github.com/CISOfy/lynis-ansible>
* Développement de plugins : <https://github.com/CISOfy/lynis-sdk>


# Installation

~~~
# apt install lynis
# cp /etc/lynis/default.prf /etc/lynis/custom.prf
# systemctl stop lynis.timer && systemctl disable lynis.timer
~~~

La configuration se trouve dans des profiles. On pourra la customiser via `/etc/lynis/custom.prf`.

> *Attention* : l'option `--profile` semble ne pas fonctionner, on est donc limités aux fichiers de configuration `default.prf` et `custom.prf`.

A partir de Debian 11, Lynis installe automatiquement un timer systemd, on préfère le désactiver pour le lancer à la main ou via cron.

# Utilisation

## Mode interactif

~~~
# lynis audit system --auditor $USER
# lynis audit system | aha > /var/www/lynis.html
~~~

Options possibles :

* `--quiet` et `--warnings-only` semblent produire la même sortie.
* `--warnings-only` : attention, tous les warnings ne semblent pas affichés.

On trouvera le log dans `/var/log/lynis.log` et le rapport d'audit dans `/var/log/lynis-report.dat`.

Attention, ces fichiers sont écrasés à chaque lancement.


## Mode Cron

On peut créer un répertoire `/var/log/lynis` et mettre en place un cron `/etc/cron.weekly/lynis` :

~~~
#!/bin/sh

set -u
MAILTO="admin@mydomain.com"
DATE=$(date +%Y%m%d-%H%M%S)
LOG_DIR="/var/log/lynis"
LOG="$LOG_DIR/lynis-${DATE}.log"
DATA="$LOG_DIR/report-data-${DATE}.dat"

lynis audit system --quiet --auditor "Cron" --cronjob --logfile $LOG --report-file $DATA --warnings-only
~~~


## Désactiver des tests

Dans `/etc/lynis/custom.prf`, on indiquera le nom des tests à désactiver (noms tels qu'indiqués dans le rapport d'audit) :

~~~
skip-test=$test1
skip-test=$test2
(...)
~~~

