# Howto GParted

<http://gparted.org>

## Créer un Live USB

Prendre une clé USB vierge, par exemple /dev/sdz

Télécharger le dernier fichier .iso pour amd64 sur <https://sourceforge.net/projects/gparted/files/gparted-live-stable/> puis :

~~~
# dd if=/dev/zero of=/dev/sdz
# cp gparted-live-1.0.0-5-amd64.iso /dev/sdz
# sync
~~~

## FAQ

Que faire pour faire « oublier » une clé USB en type iso9660 ?

formater en fat32, puis supprimer la partition, puis dd /dev/zero
