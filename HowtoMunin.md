---
categories: web
title: Howto Munin
...

* [Documentation générale](http://guide.munin-monitoring.org/en/latest/index.html)
* [Détails sur les légendes](http://demo.munin-monitoring.org/munin-monitoring.org/demo.munin-monitoring.org/)  

[Munin](https://munin-monitoring.org/) est un outil de monitoring de resources système.

# Installation

## Debian

~~~
## Pour faire à la fois un munin-master et un munin-node (par défaut)
# apt install munin

## Seulement un noeud munin-node
# apt install munin-node
~~~

## OpenBSD

~~~
# pkg_add munin-node munin-server
~~~

# Fonctionnement

Munin fonctionne sur une [architecture Master/Node](http://guide.munin-monitoring.org/en/latest/architecture/index.html). 

Une machine munin-master va se connecter à un ou plusieurs service munin-node afin de collecter les métriques des plugins configurés

Le cron `/etc/cron.d/munin` s'exécute toutes les 5 minutes sur le master. Il interroge les nodes listés dans `/etc/munin/munin.conf` et `/etc/munin/munin-conf.d/*.conf` (par défaut uniquement localhost.localdomain) sur leur port `4949` et récupère les données d'état du node. Les nodes ne stockent aucune donnée.

Puis, il met à jour ses fichiers de données RRD stockés dans `/var/lib/munin`, et génère des graphiques (images PNG) et pages HTML de présentation.

Les graphiques PNG et les pages générées sont stockés dans `/var/cache/munin/www`.

Le service munin-node peut être sur la même machine que le master, dans ce cas l'installation est purement locale (réglage par défaut).

Note : D'un point de vue réseau, ce sont les nodes qui ont le statut de serveur et le master qui se connecte en client sur leur port `4949` pour collecter les métriques.


## Fichiers RRD

Les fichiers RRD de munin sont les plus important car ce sont eux qui enregistrent l'historique des métriques collectées.

À chaque plugin est associé un ou plusieurs fichiers RRD.

~~~
# cd /var/lib/munin/localdomain/
# ls *load-*
localhost.localdomain-load-load-g.rrd
# ls *cpu-*
localhost.localdomain-cpu-idle-d.rrd localhost.localdomain-cpu-iowait-d.rrd localhost.localdomain-cpu-irq-d.rrd localhost.localdomain-cpu-nice-d.rrd
localhost.localdomain-cpu-softirq-d.rrd localhost.localdomain-cpu-steal-d.rrd localhost.localdomain-cpu-system-d.rrd localhost.localdomain-cpu-user-d.rrd
~~~

Si vous n'avez que les fichiers RRD (cas d'une sauvegarde par exemple), vous pouvez recréer des graphes via _rrdtool_.
Voici un exemple pour tracer un graphe de load sur un intervalle passé :

~~~
$ rrdtool graph foo.png -a PNG --start -15d --end -4d --width 800 --height 800 'DEF:load=localhost.localdomain-load-load-g.rrd:42:AVERAGE' 'LINE1:load#ff0000:Load' -l 0 -u 1
~~~


# Configuration

## Configurer le master

Pour rappel, le munin-master est celui, par l'exécution d'un cron, collecte les métriques sur les munin-nodes. Les fichier `/etc/munin/munin.conf` et `/etc/munin/munin-conf.d/*.conf` permettent de le configurer.

Par défaut les graphiques sont générés dans `/var/cache/munin/www`.

Pour changer le nom d'hôte, on changera son nom entre les crochets. Exemple avec le node `monserveur.example.net` :

~~~
[monserveur.example.net]
    address 127.0.0.1
    use_node_name yes
~~~


## Configurer le node

La configuration est située dans `/etc/munin/munin-node.conf`. Les plugins sont listés dans `/etc/munin/plugins` et leur configuration dans `/etc/munin/plugin-conf.d`

C'est dans ce cadre là qu'on va notamment ajouter des plugins avec leur configuration pour collecter des métriques. Voir [la section sur les plugins](./HowtoMunin#plugins)

Après une modification d'un réglage ou d'un plugin, il faut redémarrer le service munin-node

~~~
# systemctl restart munin-node
~~~


## Ajouter un node dans un master

### Configuration à faire sur le munin-node

Le fichier `/etc/munin/munin-node.conf` permet de configurer un node.

D'abord, il faut autoriser l'IP du master à ce connecter au munin-node dans `/etc/munin/munin-node.conf`. Par défaut, on a seulement l'IP locale (`127.0.0.1` et `::1`) qui est autorisée :

~~~
allow ^127\.0\.0\.1$
allow ^::1$
~~~

Il est préférable d'utiliser la directive `cidr_allow` à place de `allow` comme expliqué dans les commentaires du fichier par défaut. En effet, `allow` fonctionne avec des regex alors que `cidr_allow` utilise la notation réseau CIDR.

Exemple pour autoriser `192.0.2.42` avec les deux directives : 

~~~
cidr_allow 192.0.2.42/32    # <=== Directive de configuration préférée
allow ^192\.0\.2\.42$      
~~~

> **Remarque** : Si `cidr_allow` ne marche pas, il vous manque le paquet Debian `libnet-cidr-perl`

Puis, redémarrer le service `munin-node` :

~~~
# systemctl restart munin-node
~~~

Dans le firewall du node, il faut ajouter l'IP du master sur le port 4949 en entrée :

~~~
/sbin/iptables -A INPUT -p tcp --dport 4949 -s 192.0.2.42/32 -j ACCEPT 
~~~


### Configuration à faire sur le munin-master 

Ajouter les directives suivantes à la configuration du master :

~~~
# vim /etc/munin/munin.conf
[node1.example.com]          # <-- nom du node dans Munin
    address 192.0.2.33/32    # <-- IP du node
    use_node_name yes
~~~

Il n'y a pas de service à redémarrer. Il faut attendre 5 minutes ou lancer le cron manuellement en tant qu'utilisateur munin :

~~~
sudo -u munin -- /usr/bin/munin-cron
~~~

Dans le firewall du master, il faut ouvrir le port 4949 en sortie :

~~~
/sbin/iptables -A INPUT -p tcp --sport 4949 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
~~~

## Configuration sous OpenBSD

**Attention, aucun plugin n'est installé par défaut. Voir [#ajouter-un-plugin]() pour ajouter des plugins avant toute autre action.**

Activer et démarrer munin_node :

~~~
# rcctl enable munin_node
# rcctl start munin_node
~~~

Sous OpenBSD, le cron n'est pas installé par défaut et il faut l'ajouter manuellement :

~~~
# crontab -l -u _munin
*/5 * * * * /usr/local/bin/munin-cron > /dev/null
~~~

Après l'avoir ajouté, le lancer une première fois manuellement pour s'assurer que c'est fonctionnel :

~~~
# doas -u _munin /usr/local/bin/munin-cron
~~~

## Réglages de performance côté master

Quand un munin-master supporte de nombreux munin-node, celà peut-être assez consomateur de ressources.

Il y a plusieurs moyens pour optimiser les performances :

* Augmenter le nombre de processus de travail avec la directive `max_processes` dans `/etc/munin/munin.conf`
* Désactiver la génération des graphes/pages pour qu'elle soit faire a la demande seulement
* Utiliser rrdcached pour réduire les écritures disque


### Augmentation de la valeur de `max_processes`

C'est le nombre de noeuds/plugins qui sont traités en parallèle côté master. La valeur par défaut est à *16* mais elle peut facilement être augmentée.

### Désactivation de la génération des images (et pages html)

Il est possible d'économiser du temps CPU en demandant à munin-master de ne pas générer les images. Elles seront générées à la demande quand une personne visitera les pages de l'interface de munin. Il faut que la génération des images en CGI (utilisé aussi pour le zoom) soit bien réglé côté service web.

On changera alors la valeur de `graph_strategy` de **cron** à **cgi** dans `/etc/munin/munin.conf`

Il est aussi possible d'aller plus loin et de ne pas générer le HTML. Mais il faudra une configuration supplémentaire pour que les pages soient générées à la visite.


### Utilisation de rrdcached

Les écritures dans les fichiers RRD génèrent de nombreuses petites écritures dans de multiples fichiers. En grand nombre, celà peut être assez consomateur de ressources sur les accès disque. Une alternative est de conserver les écritures de côté avec rrdcached qui va les enregistrer dans son journal avant les appliquer uniquement passé un certain délai, par bloc.

Cette opération réduit les accès disques nécessaires.


~~~
# apt install rrdcached
~~~

Puis on configure un service rrdcached pour munin dans `/etc/systemd/system/munin-rrdcached.service` : 

~~~
[Unit]
Description=Munin rrdcached

[Service]
Restart=always
User=munin
PermissionsStartOnly=yes
ExecStartPre=/usr/bin/install -d -o munin -g munin -m 0755 /var/lib/munin/rrdcached-journal /run/munin
ExecStart=/usr/bin/rrdcached \
  -g -B -b /var/lib/munin/ \
  -p /run/munin/munin-rrdcached.pid \
  -F -j /var/lib/munin/rrdcached-journal/ \
  -m 0660 -l unix:/run/munin/rrdcached.sock \
  -w 1800 -z 1800 -f 3600
ExecStartPost=/bin/sleep 1 ; chgrp www-data /run/munin/rrdcached.sock

[Install]
WantedBy=multi-user.target
~~~ 

> *Note* : Si vous avez déplacé /var/lib/munin ailleurs, et remplacé celui-ci par un lien symbolique, changez les chemins dans l'unité, car rrdcached ne veux pas passer par des liens symboliques

Dans la configuration ci-dessus : 

* `-w 1800` > Attendre 1800 secondes (30min) avant d'écrire les données
* `-z 1800` > Délayer l'écriure dans chaque fichiers individuel par un nombre aléatoire jusqu'à 1800 secondes (30min)
* `-f 3600` > Forcér l'écriture de toutes les données accumulées passé 3600 secondes (1 heure)

~~~
# systemctl daemon-reload
# systemctl enable --now munin-rrdcached.service
~~~

Il faut maintenant signifier a munin-master que rrdcache est disponible avec la directive `rrdcached_socket` dans `/etc/munin/munin.conf`

~~~
rrdcached_socket /run/munin/rrdcached.sock
~~~

# Intégration avec un serveur web

Munin 2 permet de zoomer sur les graphiques.

Pour que cela fonctionne il faut faire tourner un démon FastCGI et le script munin-cgi-graph.


## Intégration avec Nginx

Adapter les droits de `/var/log/munin/munin-cgi` et vérifier s'il y a des fichiers manquants pour les zoom :

~~~ { .bash }
touch /var/log/munin/munin-cgi-graph.log
chown www-data:munin /var/log/munin/munin-cgi-*
chmod 660 /var/log/munin/munin-cgi-*
~~~

De même pour `/etc/logrotate.d/munin`, section `/var/log/munin/munin-cgi-graph.log` :

~~~
create 660 www-data munin
~~~

Installer les dépendances pour démarrer le démon :

~~~ { .bash }
apt install libcgi-fast-perl spawn-fcgi
~~~

Créer une unité systemd pour gérer le démon :

~~~ { .bash }
tee > /etc/systemd/system/spawn-fcgi-munin-graph.service << 'eof'
[Unit]
Description=Munin zoom for nginx.
After=network.target

[Service]
ExecStart=/usr/bin/spawn-fcgi -s /var/run/munin/spawn-fcgi-munin-graph.sock -U www-data -u munin -g munin /usr/lib/munin/cgi/munin-cgi-graph
Type=forking

[Install]
WantedBy=default.target
eof
~~~

Puis on charge l'unité pour la démarrer par la suite :

~~~
systemctl daemon-reload
systemctl start spawn-fcgi-munin-graph
~~~

Pour vérifier si le démon tourne :

~~~ { .bash }
systemctl status spawn-fcgi-munin-graph
lsof /var/run/munin/spawn-fcgi-munin-graph.sock
~~~

> Si spawn-fcgi ne démarre pas mais renvoie `spawn-fcgi: child exited with: 2`, il se peut que ce soit car le paquet `libcgi-fast-perl` manque.

Enfin dans le vhost concerné d'Nginx, ajoutez-ceci :

~~~{.nginx}
location /munin/ {
    alias /var/cache/munin/www/;
}

location ^~ /munin-cgi/munin-cgi-graph/ {
    fastcgi_split_path_info ^(/munin-cgi/munin-cgi-graph)(.*);
    fastcgi_param PATH_INFO $fastcgi_path_info;
    fastcgi_pass unix:/var/run/munin/spawn-fcgi-munin-graph.sock;
    include fastcgi_params;
}
~~~


## Intégration avec Apache

Installer `libapache2-mod-fcgid` (attention apt redémarre Apache !), puis ajouter au vhost :

~~~{.apache}
# Munin. We need to set Directory directive as Alias take precedence.
Alias /munin /var/cache/munin/www
<Directory /var/cache/munin/>
    Require all denied
    Include /etc/apache2/ipaddr_whitelist.conf
</Directory>
# Munin cgi
# Ensure we can run (fast)cgi scripts
ScriptAlias /munin-cgi/munin-cgi-graph /usr/lib/munin/cgi/munin-cgi-graph
<Location /munin-cgi/munin-cgi-graph>
    Options +ExecCGI
    <IfModule mod_fcgid.c>
        SetHandler fcgid-script
    </IfModule>
    <IfModule mod_fastcgi.c>
        SetHandler fastcgi-script
    </IfModule>
    <IfModule !mod_fastcgi.c>
        <IfModule !mod_fcgid.c>
            SetHandler cgi-script
        </IfModule>
    </IfModule>
    Require all granted
</Location>
~~~

Si on a une erreur de cette forme dans les logs d’erreur :

~~~
<h1>Software error:</h1>
<pre>Can't open /var/log/munin/munin-cgi-graph.log (Permission denied) at /usr/share/perl5/Log/Log4perl/Appender/File.pm line 151.
</pre>
~~~

Il peut être nécessaire de créer le fichier `/var/log/munin/munin-cgi-graph.log` comme pour Nginx.

~~~ { .bash }
touch /var/log/munin/munin-cgi-graph.log
chown www-data:munin /var/log/munin/munin-cgi-*
chmod 660 /var/log/munin/munin-cgi-*
vi /etc/logrotate.d/munin
~~~

# Notifications

Munin est capable d'envoyer des mails quand un plugin atteint son seuil de warning ou critical. Toute la configuration se fait dans `munin.conf`, on indique un mail de contact, et éventuellement on ajuste les seuils (ou on les définit) d'un ou plusieurs plugins.

Exemple :

~~~
contact.someuser.command mail -s "Munin notification" foo@example.com

[example.foo.bar]
    address 127.0.0.1
    use_node_name yes
    postfix_mailqueue.deferred.critical 200
~~~

On peut aussi configurer les seuils de warning ou critical dans le fichier de configuration `/etc/munin/plugin-conf.d/munin-node`, voici un exemple de seuils sur le check du retard sur la réplication mysql :

~~~
[mysql*]
user root
env.mysqlopts --defaults-file=/root/.my.cnf
env.mysqluser root
env.mysqlconnection DBI:mysql:mysql;mysql_read_default_file=/root/.my.cnf
env.seconds_behind_master_warning 120
env.seconds_behind_master_critical 800
~~~

# Plugins

Les plugins sont stockés dans `/usr/share/munin/plugins`.

Les plugins activés sont mis en place via un lien symbolique dans `/etc/munin/plugins/`.

Exemple :

~~~
$ ls -lha /etc/munin/plugins/
[…]
lrwxrwxrwx 1 root root   40  3 juin  10:31 apache_accesses -> /usr/share/munin/plugins/apache_accesses
[…]
~~~


## Ajouter un plugin

Pour ajouter un plugin à grapher il suffit de mettre en place un lien symbolique.
Il faut ensuite redémarrer munin-node : `systemctl restart munin-node.service`

Munin peut suggérer des configurations selon ce qui est installé sur le serveur avec la commande `munin-node-configure --suggest --shell`


## Tester un plugin

Il peut être utile, et recommandé de tester le plugin avant de le mettre en place.

Pour afficher les valeurs des graphes d'un plugin :

~~~
$ munin-run --debug <PLUGIN_NAME>  # nom du symlink dans /etc/munin/plugins/
~~~

Pour afficher la config des graphes du plugin :

~~~
$ munin-run --debug <PLUGIN_NAME> config
~~~

Exemple :

~~~
$ munin-run --debug swap
# Processing plugin configuration from /etc/munin/plugin-conf.d/munin-node
# Set /rgid/ruid/egid/euid/ to /111/65534/111 111 /65534/
# Setting up environment
# About to run '/etc/munin/plugins/swap'
swap_in.value 19
swap_out.value 53
~~~


## Configurer un plugin

Documentation officielle : <http://guide.munin-monitoring.org/en/latest/plugin/use.html>

La configuration des plugins se fait dans `/etc/munin/plugin-conf.d`.
La configuration par défaut se trouve dans le fichier `/etc/munin/plugin-conf.d/munin-node`. **Il ne faut pas le modifier.**

Si on souhaite ajouter ou modifier la configuration d'un plugin, le mieux est de créer un fichier `/etc/munin/plugin-conf.d/zzz-<PLUGIN_NAME>`.
Avoir un fichier de configuration par plugin permet une gestion plus fine, et facilite la gestion via Ansible par exemple.


### Changer l'utilisateur et le groupe avec lesquels le plugin est lancé

Selon les besoins, il peut être utile d'exécuter un plugin avec un utilisateur et un groupe particuliers.

Pour cela, créer un fichier `/etc/munin/plugin-conf.d/zzz-<PLUGIN_NAME>` :

~~~
[<PLUGIN_NAME>]
user munin
group munin
~~~

### Passer une variable d'environnement à un plugin

On peut aussi avoir besoin de passer une variable d'environnement à un plugin (respecter la casse dans le nom de la variable) :

~~~
[<PLUGIN_NAME>]
(...)
env.<ENV_VAR_NAME> <VALUE>
~~~


## Utiliser plusieurs fois un plugin

Il est possible d'utiliser plusieurs fois un même plugin mais avec des paramètres différents. C'est très utile pour le plugin MySQL par exemple.

~~~
/etc/munin/plugins# for type in bytes queries slowqueries threads; do for instance in 3307 3309; do ln -s /usr/share/munin/plugins/mysql_${type} mysql_${instance}_${type}; done; done
/etc/munin/plugins# ls -lha mysql_330*
lrwxrwxrwx 1 root root 36 Jul 22 11:23 mysql_3307_bytes -> /usr/share/munin/plugins/mysql_bytes
lrwxrwxrwx 1 root root 38 Jul 22 11:23 mysql_3307_queries -> /usr/share/munin/plugins/mysql_queries
lrwxrwxrwx 1 root root 42 Jul 22 11:23 mysql_3307_slowqueries -> /usr/share/munin/plugins/mysql_slowqueries
lrwxrwxrwx 1 root root 38 Jul 22 11:23 mysql_3307_threads -> /usr/share/munin/plugins/mysql_threads
lrwxrwxrwx 1 root root 36 Jul 22 11:23 mysql_3309_bytes -> /usr/share/munin/plugins/mysql_bytes
lrwxrwxrwx 1 root root 38 Jul 22 11:23 mysql_3309_queries -> /usr/share/munin/plugins/mysql_queries
lrwxrwxrwx 1 root root 42 Jul 22 11:23 mysql_3309_slowqueries -> /usr/share/munin/plugins/mysql_slowqueries
lrwxrwxrwx 1 root root 38 Jul 22 11:23 mysql_3309_threads -> /usr/share/munin/plugins/mysql_threads
~~~

Puis dans le fichier /etc/munin/plugin-conf.d/mysql_multi :

~~~
[mysql_3307_*]
user root
env.mysqlopts --defaults-extra-file=/root/.my.cnf -h127.0.0.1 -P3307

[mysql_3309_*]
user root
env.mysqlopts --defaults-extra-file=/root/.my.cnf -h127.0.0.1 -P3309
~~~


## Le plugin postgresql

Installer `libdbd-pg-perl`

~~~
# aptitude install libdbd-pg-perl
~~~

Linker les plugins généraux :

~~~
ln -s /usr/share/munin/plugins/postgres_bgwriter /etc/munin/plugins/
ln -s /usr/share/munin/plugins/postgres_checkpoints /etc/munin/plugins/
ln -s /usr/share/munin/plugins/postgres_connections_db /etc/munin/plugins/
ln -s /usr/share/munin/plugins/postgres_users /etc/munin/plugins/
ln -s /usr/share/munin/plugins/postgres_xlog /etc/munin/plugins/
~~~

Pour les plugins wildcard finissant par _ ajoutez `ALL` pour monitorer toutes les BDD :

~~~
ln -s /usr/share/munin/plugins/postgres_cache_ /etc/munin/plugins/postgres_cache_ALL
ln -s /usr/share/munin/plugins/postgres_connections_ /etc/munin/plugins/postgres_connections_ALL
ln -s /usr/share/munin/plugins/postgres_locks_ /etc/munin/plugins/postgres_locks_ALL
ln -s /usr/share/munin/plugins/postgres_querylength_ /etc/munin/plugins/postgres_querylength_ALL
ln -s /usr/share/munin/plugins/postgres_size_ /etc/munin/plugins/postgres_size_ALL
ln -s /usr/share/munin/plugins/postgres_transactions_ /etc/munin/plugins/postgres_transactions_ALL
~~~

sauf les suivants qui ne peuvent analyser qu'une seule base de données :

~~~
ln -s /usr/share/munin/plugins/postgres_scans_ /etc/munin/plugins/postgres_scans_NOMBDD
ln -s /usr/share/munin/plugins/postgres_tuples_ /etc/munin/plugins/postgres_tuples_NOMBDD
~~~

Attention avec **Squeeze LTS**, la version du serveur contient lts (ex: PostgreSQL 8.4.22lts5) et le plugin ne fonctionne plus.
Il faut modifier le fichier `/usr/share/perl5/Munin/Plugin/Pgsql.pm` et remplacer la ligne :

~~~
unless ($v =~ /^PostgreSQL (\d+)\.(\d+)\.(\d+) on/);
~~~

par :

~~~
unless ($v =~ /^PostgreSQL (\d+)\.(\d+)\.(\d+)lts(\d) on/);
~~~

## Le plugin mysql

Activons-les :

~~~
# cd /etc/munin/plugins
# ln -s /usr/share/munin/plugins/mysql_bytes
# ln -s /usr/share/munin/plugins/mysql_queries
# ln -s /usr/share/munin/plugins/mysql_slowqueries
# ln -s /usr/share/munin/plugins/mysql_threads
~~~

On pourra aussi activer le plugin *mysql_* avec des liens symboliques nommés connections, files_tables, innodb_bpool, innodb_bpool_act, innodb_io, innodb_log, innodb_rows, innodb_semaphores, myisam_indexes, qcache, qcache_mem, sorts ou tmp_tables.

On mettra en place cette configuration dans `/etc/munin/plugin-conf.d/munin-node` :

~~~
[mysql*]
user root
env.mysqlopts --defaults-file=/root/.my.cnf
env.mysqluser root
env.mysqlconnection DBI:mysql:mysql;mysql_read_default_file=/root/.my.cnf
~~~

Pour tester le bon fonctionnement des plugins MySQL pour Munin :

~~~
# munin-run mysql_bytes
~~~

Si on a plusieurs instances MySQL sur un serveur, il est possible d'[utiliser plusieurs fois un plugin Munin](https://wiki.evolix.org/HowtoMunin#utiliser-plusieurs-fois-un-plugin).

### Réplication

On recommande d'activer ceux-ci :

~~~
ln -s '/usr/share/munin/plugins/mysql_' '/etc/munin/plugins/mysql_bin_relay_log'
ln -s '/usr/share/munin/plugins/mysql_' '/etc/munin/plugins/mysql_replication'
~~~

## Le plugin dnsresponsetime

Il faut éditer le fichier `/etc/munin/plugin-conf.d/munin-node` et ajouter la section du plugin pour spécifier un nom à résoudre et un ou des serveurs DNS :

~~~
[dnsresponsetime]
env.queries evolix.fr@ns2.evolix.net evolix.fr@romario.evolix.net
~~~

## Le plugin bind_rndc

Ajouter les lignes suivante dans `/etc/munin/plugin-conf.d/munin-node` :

~~~
[bind9_rndc]
user root
env.querystats /var/chroot-bind/var/cache/bind/named.stats
~~~

Assurez-vous d'avoir configuré le fichier `named.stats`, voir [HowtoBind#Graphsdesstatsviarndc].

## Le plugin bind

Assurez-vous d'avoir configurer un « channel de log » pour les requêtes DNS. Cf. [HowtoBind#Graphsdesacc%C3%A8sviaunfichierdelog].

Ajouter les lignes suivante dans `/etc/munin/plugin-conf.d/munin-node`

~~~
[bind9]
user root
env.logfile /var/chroot-bind/var/log/queries.log
~~~

Attention au chemin si bind est chrooté !

Un bug nécessite de créer le fichier des infos munin à la main…

~~~
# cd /var/lib/munin/plugin-state/
# touch bind9.state
# chown munin:munin bind9.state
# chmod 664 bind9.state
~~~

## Le plugin tomcat

~~~
# ln -s /usr/share/munin/plugins/tomcat_ tomcat_jvm
# ln -s /usr/share/munin/plugins/tomcat_ tomcat_threads
# ln -s /usr/share/munin/plugins/tomcat_ tomcat_volume
# ln -s /usr/share/munin/plugins/tomcat_ tomcat_access
# ln -s /usr/share/munin/plugins/tomcat_ tomcat_avgtime
# ln -s /usr/share/munin/plugins/tomcat_ tomcat_maxtime
~~~

~~~
[tomcat_*]
env.host 127.0.0.1
env.port 8080
env.request /manager/status?XML=true
env.user admin
env.password mon_pass
env.timeout 30
~~~

## Le plugin squid

~~~
ln -s /usr/share/munin/plugins/squid_cache /etc/munin/plugins/
ln -s /usr/share/munin/plugins/squid_icp /etc/munin/plugins/
ln -s /usr/share/munin/plugins/squid_objectsize /etc/munin/plugins/
ln -s /usr/share/munin/plugins/squid_requests /etc/munin/plugins/
ln -s /usr/share/munin/plugins/squid_traffic /etc/munin/plugins/
~~~

Ajuster le port si nécessaire :

~~~
[squid*]
env.squidhost 127.0.0.1
env.squidport 80
~~~

S'assurer que les directives suivantes sont bien présentes dans `/etc/squid3/squid.conf` :

~~~
# munin
acl manager proto cache_object
<http_access> allow manager localhost
<http_access> deny manager
~~~

Vérification :

~~~
squidclient -p 80 cache_object://127.0.0.1/storedir
squidclient -p 80 cache_object://127.0.0.1/server_list
squidclient -p 80 cache_object://127.0.0.1/info
squidclient -p 80 cache_object://127.0.0.1/counters
~~~


# Troubleshooting

## Souci de droits sur les images

Si munin n'affiche pas les images, c'est peut-être un souci de droits. Vérifier les droits dans `/var/cache/munin/www/*/*/`. Si les .png sont en 600, faire ceci :

~~~
# echo "umask 022" >> /etc/default/cron
# /etc/init.d/cron restart
# chmod 644 /var/cache/munin/www/*/*/*.png
~~~

On peut lancer la generation des graphs à la main pour avoir des informations plus précises :

~~~
su - munin --shell=/bin/bash
/usr/share/munin/munin-update --debug
~~~


## Problème de graph, munin ne graphe plus

Vérifier la conf du plugin qui ne graphe pas, dans /etc/munin/plugin-conf.d/munin-node

Il faut regarder si la variable env.url a bien le nom du serveur en
question et pas 127.0.0.1. Par exemple avec le plugin apache :

~~~
[apache_*]
env.url   http://nomduserveur.evolix.net:%d/server-status-XXXXX?auto
env.ports 8080
~~~

On peut aussi vérifier que la requête vers l'url fonctionne en faisant un GET vers l'url:

~~~
GET http://nomduserveur.evolix.net:8080/server-status-XXXXX?auto
~~~

## Regarder dans la doc

~~~
# munindoc <plugin>
~~~


## Il manque `/home` dans le *plugin* `df`

Attention, le *plugin* `df` est différent du *plugin* `diskstats`.

~~~{ .bash }
mkdir -p /etc/systemd/system/munin-node.service.d
printf '[Service]\nProtectHome=read-only\n' | tee -a /etc/systemd/system/munin-node.service.d/override.conf
systemctl daemon-reload
systemctl restart munin-node.service
~~~

Puis attendre la prochaine exécution du *cron* ou la lancer soi-même :`sudo -u munin /usr/bin/munin-cron`.

> On peut remplacer les commandes `mkdir` et `printf | tee` par `systemctl edit munin-node.service`.


## Le titre des graphiques est absent

Note : correction à faire sur le master.

A la ligne 12, il faut remplacer `<tr><td>` dans `/etc/munin/templates/munin-nodeview.tmpl` par :

~~~
<tr><td><TMPL_VAR ESCAPE="HTML" NAME="NAME"></td></tr><tr><td>
~~~

Pour voir la correction sans attendre la régénération automatique, lancer :

~~~
$ sudo -u munin munin-cron
~~~

**Attention**, il semble que cette commande casse les permissions si l'umask de l'utilisateur est `0077`.


## Impossible de zoomer sur les graphes

Si malgré la procédure donnés pour Apache et Nginx on ne peut toujours pas faire de zoom sur les graphes, il y a probablement un souci de permission sur les fichiers et répertoires sous `/var/lib/munin/cgi-tmp`. Exemple d’erreur dans `/var/log/munin/munin-cgi-graph.log` :

~~~
2023/01/23 12:34:56 [WARNING] Could not draw graph "/var/lib/munin/cgi-tmp/munin-cgi-graph/example.com/<hostname>.example.com/…
~~~

On peut vérifier le propriétaire avec `namei -l /var/lib/munin/cgi-tmp/munin-cgi-graph/example.com/<hostname>.example.com/`. À partir de `/var/lib/munin/`, le propriétaire devrait être `munin`. Le groupe peut varier.
