# OverlayFS

Documentation : <https://docs.kernel.org/filesystems/overlayfs.html>

Un système de fichier OverlayFS fusionne deux systèmes de fichier, un système de fichier supérieur `upper` et un système de fichier inférieur `lower`. Quand un nom existe dans les deux systèmes de fichiers, l'objet dans le système de fichier supérieur est visible tandis que l'objet dans le système de fichier inférieur et soit caché ou dans le cas de dossiers, fusionnés avec l'objet supérieur.

## Mount bind RO du pauvre

Un `bind --mount` read-only à l'intérieur du même système de fichier est considéré comme un même système de fichier, donc les commandes comme `cp --one-file-system` traverserons ce point de montage. Pour éviter ça on peut utiliser un OverlayFS.

Il suffit de créer un OverlayFS composé uniquement de systèmes de fichiers inférieurs afin que le résultat soit read-only et vu qu'on ne veut pas modifier le contenu du dossier à monter, on le met en dernier dans la liste des FS inférieurs.

1. Soit `/foo` le point de montage d'un système de fichier (ext4 par exemple)
2. Soit `/foo/bar/` le dossier qu'on veut monter autre part dans le système de fichier `/foo`
3. Soit `/foo/buz/mnt` notre futur point de montage

On crée ce point de montage comme ceci :

~~~(sh)
empty_dir=$(mktemp --directory --tmpdir)
mount -t overlay overlay -o "lowerdir=${empty_dir:?}:/foo/bar" "/foo/buz/mnt"
~~~