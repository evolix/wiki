---
categories: monitoring
title: Howto Grafana
...

* Documentation : <http://docs.grafana.org/>

[Grafana](https://grafana.com/) permet d'effectuer des requêtes sur des bases de données, pour en afficher des graphes personnalisés. Nous l'utilisons en combinaison avec [collectd](HowtoCollectd) et [InfluxDB](HowtoInfluxDB).


## Installation

Grafana n'étant pas disponible sous Debian Bullseye, nous utilisons les dépôts fournis par GrafanaLabs.

~~~
# wget -O /etc/apt/keyrings/grafana.asc https://packages.grafana.com/gpg.key
# dos2unix /etc/apt/keyrings/grafana.asc
# chmod 644 /etc/apt/keyrings/grafana.asc
# echo "deb [signed-by=/etc/apt/keyrings/grafana.asc] https://packages.grafana.com/oss/deb stable main" > /etc/apt/sources.list.d/grafana.list
# apt update
# apt install grafana
# systemctl daemon-reload
# systemctl enable grafana-server.service
# systemctl start grafana-server.service

# grafana-server -v
Version 10.4.2 (commit: 22809dea50455ae875624827dd277b17621b4ed4, branch: HEAD)

# systemctl status grafana-server.service
● grafana-server.service - Grafana instance
     Loaded: loaded (/lib/systemd/system/grafana-server.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-04-30 15:37:26 CEST; 21s ago
       Docs: http://docs.grafana.org
   Main PID: 13827 (grafana)
      Tasks: 15 (limit: 2352)
     Memory: 54.1M
        CPU: 2.276s
     CGroup: /system.slice/grafana-server.service
             └─13827 /usr/share/grafana/bin/grafana server --config=/etc/grafana/grafana.ini --pidfile=/run/grafana/grafana-server.pid --packaging=deb cfg:default.paths.logs=/var/log/grafana cfg:default.paths.data=/var/lib/grafana cfg:default.paths.plugins=/var/lib/grafana/plugins cfg:default.paths.provisioning=/etc/grafana/provisioning
~~~

## Configuration

On modifie le fichier de configuration `/etc/grafana/grafana.ini` afin de n'écouter qu'en local, pour ensuite utiliser un reverse proxy vers 127.0.0.1:3000. On cache également l'affichage de la version utilisée de Grafana.

~~~
[server]
http_addr = 127.0.0.1

[auth.anonymous]
hide_version = true
~~~

### Authentification LDAP

Modifier la configuration `/etc/grafana/grafana.ini` :

~~~
[auth.ldap]
enabled = true
config_file = /etc/grafana/ldap.toml
allow_sign_up = true
~~~

Puis configurer le fichier `/etc/grafana/ldap.toml` en conséquence.

### Reverse proxy avec sous dossier

Si grafana est dans un sous-dossier de la forme `example.com/grafana/` plutôt que `grafana.example.com`, il faut alors l'indiquer dans la configuration (laisser les paramètres `%(nom)s` tels quels) :

~~~
[server]
root_url = %(protocol)s://%(domain)s:%(http_port)s/grafana/
~~~

Exemple de configuration avec [Nginx](/HowtoNginx#proxy_pass) :

~~~
    location = /grafana {
        return 301 grafana/;
    }
    location /grafana/ {
        proxy_pass http://127.0.0.1:3000/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        client_max_body_size 10m; # Nécessaire pour Grafana https://github.com/grafana/grafana/issues/3176
    }
~~~

### Emplacement des données

Les données de Grafana (sources, dashboards, graphes, utilisateurs, …) sont stockées par défaut dans `/var/lib/grafana`.

### Configuration SMTP

Pour pouvoir utiliser la fonction "mot de passe oublié", le serveur SMTP doit être configurée :

~~~
[smtp]
enabled = true
host = localhost:25
skip_verify = true
from_address = grafana@example.com
from_name = Grafana Example
~~~

## Utilisation

Les identifiants par défaut sont `admin/admin`, et doivent être changés à la première connexion à l'interface. 

### Ajouter une source

Pour ajouter par exemple une source [InfluxDB](HowtoInfluxDB), avec les données dans une base [collectd](HowtoCollectd) :

* Connections > Data Sources > Add data source
* Choisir InfluxDB
* Name : Collectd (pour indiquer la source des données et non le nom de la base)
* Query Language : InfluxQL
* URL : http://127.0.0.1:8086
* Database : collectd

Avec l'authentification configurée ou non selon la configuration d'[InfluxDB](HowtoInfluxDB#authentification) :

* User : \<username\>
* Password : \<password\>

### Créer un dashboard

* Dashboard > Create Dashboard
* Configurer le dashboard en cliquant sur la roue dentée en haut à droite

Un dashboard hérite des permissions de son dossier parent. Pour ne pas avoir les permissions par défaut `every viewers can view` et `every editors can edit`, étant donné que les permissions du dossier par défaut _Général_ ne sont pas modifiables, il faut créer un nouveau dossier n'ayant pas ces permissions et y placer les dashboards. Ainsi, les utilisateurs n'auront par défaut pas accès à tous les dashboards.

Des dashboards créés par la communauté peuvent être importés depuis <https://grafana.com/dashboards> 

### Créer un graphe

* Se rendre dans le dashboard précédemment créé > En haut à droite, choisir Add > Visualization > Choisir le type de graphe tout en haut à droite (défaut sur "Time series")
* Parcourir les onglets pour choisir la source et configurer le graphe

### Changer le dashboard par défaut

Pour changer le dashboard par défaut pour tout le monde (celui qui s'affiche à la connexion, sur la page d'accueil) :

* Administration > General > Default preferences > Home Dashboard > Sélectionner celui souhaité

Pour ne le changer que pour l'utilisateur courant :

* Cliquer tout en haut à droite sur l'icône représentant son utilisateur > Profile > Home Dashboard > Sélectionner celui souhaité