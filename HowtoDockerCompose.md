---
title: Howto Docker Compose
categories: docker
---

Compose est un outil qui permet de définir et de lancer des applications
Docker multi-conteneurs.

## Installation

~~~
# apt install docker-compose

$ docker-compose --version
docker-compose version 1.21.0, build unknown
~~~

### Manuellement

~~~
$ curl -L "https://github.com/docker/compose/releases/download/1.18.0/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
$ chmod +x /usr/local/bin/docker-compose
~~~

> **Note** : Assurez-vous de récupérer la dernière [release](https://github.com/docker/compose/releases).

Si vous avez l'erreur suivante :

~~~
docker-compose: error while loading shared libraries: libz.so.1: failed to map segment from shared object
~~~

Vous devez changer de TMPDIR ou s'assurer d'avoir monté `/tmp` en exec.


### pip

~~~
$ pip install docker-compose
~~~

## Utilisation de base

### Lancer l'application

~~~
$ docker-compose up [options]
~~~

Options courantes :

~~~
-d      : Lancer en mode détaché
--build : Construire les images avant de lancer les conteneurs
~~~

### Fermer ou forcer la fermeture de l'application

~~~
$ docker-compose down|kill [options]
~~~

Options :

~~~
-v : Supprimer les volumes déclarée dans le fichier docker-compose
~~~

### *Scaler* un composant de l'application

~~~
$ docker-compose scale <appname>=<## de conteneurs> ...
~~~

### Construire ou reconstruire un les images

~~~
$ docker-compose build [options]
~~~

Options courantes :

~~~
--force-rm : Supprimer les conteneurs intermédiaires
--no-cache : Ne pas utiliser le cache lors de la construction
--pull     : Toujours tenter de télécharger une version plus récente de l'image
~~~

## Fichier de configuration

Le fichier qui définit l'application Docker Compose au format YAML est nommé **docker-compose.yml**.

En voici un exemple :

~~~
version: '2'
services:
  web:
    build: .
    ports:
     - "5000:5000"
    volumes:
     - .:/code
    depends_on:
     - redis
  redis:
    image: redis
~~~
