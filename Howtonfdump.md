---
title: Howto nfdump
---

* Documentation nfdump : <http://nfdump.sourceforge.net/>
* Documentation NfSen : <http://nfsen.sourceforge.net/>
* manpage nfdump(1) : <https://manpages.debian.org/jessie-backports/nfdump/nfdump.1.html>

**nfdump** est un outil pour gérer des données [Netflow](HowtoOpenBSD/Netflow). On l'associe en général à **nfcapd** un démon chargé de collecter les _Netflow_ et parfois à **NfSen** qui permet une utilisation via une interface web.

## Installation

~~~
# apt install nfdump

$ nfdump -V
nfdump: Version: 1.6.15
~~~

## nfcapd pour collecter les flows

Il faut au préalable [avoir configuré l'export des flows sur vos routeurs](HowtoOpenBSD/Netflow).

On peut ainsi lancer le démon **nfcapd** en configurant via `/etc/default/nfdump` :

~~~
nfcapd_start=yes
~~~

Par défaut, il écoute en UDP/9995 et stocke les données _Netflow_ dans `/var/cache/nfdump`

On peut bien sûr configurer d'autres options, exemple d'une façon de le lancer :

~~~
$ nfcapd -w -D -p 9996 -u foo -g www-data -B 200000 -S 1 -P /var/run/nfcapd-p9996.pid -z -I routeur -l /srv/nfdump/routeur
~~~


## nfdump pour analyser les flows

On peut utilise **nfdump** en ligne de commande :

~~~
$ nfdump -M /var/cache/dump [-R file1:file2] [FILTRE] [options]
~~~

Options utiles pour **lister** les flows :

* `-c N` : limite à N flows
* `-b` : agrège les flows aller/retour (bi-directional)
* `-A <LISTE>` : agrège les flows, exemples : **-A proto,srcip,dstip**, **-A proto,srcip,dstport**
* `-t <intervalle>` : limite à un intervalle précis, exemple _-t 2011/03/26.22:35:10-2011/03/26.22:36:03_
* `-o line6` : afficher les adresses IPv6 entièrement (au lieu d'une forme tronquée telle que 2001:db8..000::58)
* `-n 10` : limiter le résultat au top 10, mettre à 0 pour avoir tous les résultats

Options utiles pour **trier** les flows :

* `-s ip/flows -n 10` : top 10 des adresses IP qui sont concernés par le + de flows
* `-s ip/packets -n 10` : top 10 des adresses IP qui sont concernés par le + de paquets
* `-s ip/bytes -n 10` : top 10 des adresses IP qui sont concernés par le + d'octets

> *Note* : on peut remplacer `ip` par srcip,dstip,port,srcport,dstport…

Filtres _tcpdump_ que l'on peut ajouter pour limiter les flows listés/triés :

* `'proto tcp'` : limite au protocole TCP
* `'[src|dst] host <IP>'` : limite à une adresse IP
* `'[src|dst] net <net>'` : limite à un sous-réseau
* `'[src|dst] port <port>'` : limite à un port

> *Note* : ces filtres se combinent avec AND, OR et des parenthèses pour les priorités

Quelques exemples utiles :

~~~
## Lister tous les flows
# nfdump -M /var/cache/nfdump -R .

## Lister tous les flows issus de plusieurs répertoires
# nfdump -M /srv/nfdump/routeur1:routeur2:routeur3 -R .

## Lister les flows d'une date à une autre
# nfdump -M /var/cache/nfdump -R 2017/02/21/nfcapd.201702211030:2017/03/10/nfcapd.201703101030
## Sur OpenBSD il semble qu'on ne puisse pas utiliser l'option -M dans cette commande
# nfdump -R /var/cache/nfdump/2017/02/21/nfcapd.201702211030:2017/03/10/nfcapd.201703101030

## Lister les flows concernant l'UDP et l'IP 8.8.8.8 avec agrégation bi-directionnel
# nfdump -M /var/cache/nfdump -R . 'proto udp and host 8.8.8.8' -b

## Lister les flows en agrégeant par proto,srcip,dstip,dstport :
# nfdump -M /var/cache/nfdump -R . -A proto,srcip,dstip,dstport

## top 10 des ports utilisés entre deux machines en fonction du nombre
## de flows
# nfdump -M /var/cache/nfdump -R . 'host 192.0.2.1 and host 192.0.2.2' -s port -n 10
## Sur OpenBSD il semble que l'ordre des arguments soit important
# nfdump -M /var/cache/nfdump -R . -s port -n 10 'host 192.0.2.1 and host 192.0.2.2'

## top 10 des IPs source avec le plus de trafic
# nfdump -M /var/cache/nfdump -R . -s srcip/bytes -n 10

## top 10 des IPs ayant générés le plus de flow vers TCP/80 d'une machine
# nfdump -M /var/cache/nfdump -R . 'proto tcp and dst host 192.0.2.1 and dst port 80' -s srcip/flows
~~~

## NfSen

**NfSen** est un outil pour gérer les _Netflow_ via une interface web, il permet notamment :

* de lancer plusieurs **nfcapd**  pour collecter les _Netflow_ de plusieurs routeurs
* de lancer la commande _nfdump_ avec des options de manière « graphique »
* d'avoir des graphes par flows/paquets/octets par seconde avec des filtres tcpdump
* de créer des alertes en fonction de seuils / filtres tcpdump

### Installation NfSen

On installe NfSen **et** nfdump à partir des sources :

~~~
# perl -MCPAN -e 'install Socket6'

# wget https://github.com/phaag/nfdump/archive/v1.6.23.tar.gz
# tar zxvf v1.6.23.tar.gz
# cd nfdump-1.6.23
# echo 'AC_CONFIG_MACRO_DIRS([m4])' >> configure.ac
# apt install libcurl4-gnutls-dev libboost-regex-dev libboost-filesystem-dev libboost-system-dev libbz2-1.0 libbz2-dev libbz2-ocaml libbz2-ocaml-dev byacc libtool m4 automake pkg-config flex librrd-dev make
# ./autogen.sh
# ./configure --enable-nfprofile --enable-nftrack --enable-influxdb
# make
# make install

# wget https://sourceforge.net/projects/nfsen/files/stable/nfsen-1.3.8/nfsen-1.3.8.tar.gz/download -O nfsen-1.3.8.tar.gz
# tar zxvf nfsen-1.3.8.tar.gz
# cd ./nfsen-1.3.8
# cp etc/nfsen-dist.conf /etc/nfsen.conf
# vim /etc/nfsen.conf
# mkdir -p /opt/nfsen/data/nfsen
# mkdir -p /opt/nfsen/www
# adduser nfsen
# adduser nfsen www-data
# chown -R nfsen:www-data /opt/nfsen
# chmod -R 750 /opt/nfsen
# apt install libmailtools-perl
# ldconfig
# ./install.pl /etc/nfsen.conf
# ln -s /opt/nfsen/data/nfsen/bin/nfsen /etc/init.d/nfsen
# /etc/init.d/nfsen start
~~~

On peut créer une unité systemd dans `/etc/systemd/system/nfsen.service`:

~~~
[Unit]
Description=NFSen
After=network.target

[Service]
ExecStart=/opt/nfsen/data/nfsen/bin/nfsen start
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process

[Install]
WantedBy=multi-user.target
~~~

Puis on l'active :

~~~
# systemctl enable nfsen.service
~~~

### Rétention des données

Par défaut, les données sont gardées sur une durée illimitée.

La rétention peut être configurée depuis l'interface web, dans l'onglet `Stats`. Plusieurs valeurs sont intéressantes ici :

* Start : date des données les plus anciennes
* End : date des données les plus récentes
* Size : taille actuellement occupée
* Max. Size : taillée souhaitée maximale
* Expire : durée de rétention souhaitée en heures (sans unité, ou unité "h", "hour", "hours") ou en jours (unités "d", "day", "days")

Si "Max. Size" et "Expire" sont tous les deux définies, la première valeur atteinte fera expirer les données.

### Export des données vers InfluxDB

Note : Nfdump doit avoir été compilé avec l'option `--enable-influxdb`.

Les statistiques du profile "live" peuvent être directement exportées vers [InfluxDB](HowtoInfluxDB).
Il faut pour cela appliquer [un patch fourni par le projet nfdump](https://github.com/phaag/nfdump/blob/master/extra/nfsen/nfsen-1.6-stats-influxdb.patch), puis fournir la variable `$influxdb_url="http://mydbhost.local:8086/write?db=nfsen";` dans le fichier de configuration `nfsen.conf`, et redémarrer nfsen.

Pour exporter les statistiques des autres profiles, il suffit d'ajouter `$NFPROFILEOPTS="-i http://mydbhost.local:8086/write?db=nfsen";` dans le fichier de configuration `nfsen.conf`, et redémarrer nfsen.

Un [dashbord Grafana](https://github.com/phaag/nfdump/blob/master/extra/grafana/Nfsen_Stats.json) préconfiguré est fourni comme exemple.

## FAQ

### Les alertes ne fonctionnent plus, et aucun profil ne graphe sauf live

Ce problème peut arriver après un upgrade de l'OS. Il faut dans ce cas [recompiler nfdump](Howtonfdump#installation-nfsen).

### Les AS ne font pas partie des flows

Les AS sont populés dans les flows par les routeurs ayant une full table BGP. Si l'on réccupère des flows depuis une machine n'ayant pas de full table BGP, les AS ne seront pas renseignés :

~~~
Date first seen          Duration Proto                AS      Flows(%)     Packets(%)       Bytes(%)      pps      bps   bpp
2019-06-03 11:01:53.000 86883.000 any                   0    66706(100)    2.3 M(100)     1.9 G(100)       26   177729   847
~~~

Il existe pour cela le module Perl [Net::NfDump](https://metacpan.org/pod/Net::NfDump/) qui permet de réécrire les fichiers créés par nfcapd en y ajoutant les informations d'AS :

~~~
# nfasnupd nfcapd.201906041110
Checking BGP database for new version.
Loading AS database.
Updating records.
Processed 66706 flows in 8 secs.
~~~

### Nfdump ne fonctionne plus après upgrade

Un message d'erreur est obtenu lors du lancement d'une commande nfdump :

~~~
# nfdump -V
nfdump: error while loading shared libraries: "libnfdump-1.6.18.so": cannot open shared object file: No such file or directory
~~~

Il faut lancer `ldconfig` pour refaire le lien symbolique entre libnfdump.so et libnfdump-1.6.18.so :

~~~
# ldconfig
# nfdump -V
nfdump: Version: 1.6.18
~~~

### NfSen ne démarre plus : RRD version '1.7002' not yet supported!

On obtient l'erreur suivante lors du démarrage de NfSen, ou la même erreur avec un numéro de version différent :

~~~
Starting nfsendPANIC nfsend dies: RRD version '1.7002' not yet supported!

RRD version '1.7002' not yet supported!
~~~

Il faut éditer `/opt/nfsen/data/nfsen/libexec/NfSenRRD.pm` (adapter le chemin selon où est installé nfsen) en augmentant la version max supportée :

~~~
    if ( $rrd_version >= 1.2 && $rrd_version < 1.8 ) {
~~~