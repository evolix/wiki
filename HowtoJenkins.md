---
categories: dev
title: Howto Jenkins
...

* Documentation : <https://jenkins.io/doc/>
* Ansible : <https://gitea.evolix.org/evolix/ansible-roles/src/branch/stable/jenkins>
* Statut de cette page : test / bookworm


[Jenkins](https://www.jenkins.io/) est un outil open source d'intégration continue écrit
en Java. Jenkins peut fonctionner dans un conteneur de servlets tel
qu’Apache Tomcat, ou en mode autonome avec son propre serveur Web
embarqué.


## Compatibilité des versions

On recommande en :

 * Debian 8 avec du Java 7 et une version inférieure à 2.54 de Jenkins
 * Debian 9 avec du Java 8 et une version supérieure à 2.54 de Jenkins
 * Debian 10 avec du Java 11 et une version supérieure à 2.164.1 de Jenkins
 * Debian 11 avec du Java 11 et une version supérieure à 2.332.1 de Jenkins
 * Debian 12 avec du Java 17 et une version supérieure à 2.361.1 de Jenkins

Notons qu'il n'est pas compatible avec Java 9 et 10.

## Installation

Jenkins n’est pas disponible dans les dépôts Debian, il faut donc ajouter le dépôt comme suit :


~~~
# curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | tee \
/etc/apt/keyrings/jenkins-keyring.asc > /dev/null

# echo deb [signed-by=/etc/apt/keyrings/jenkins-keyring.asc] \
https://pkg.jenkins.io/debian-stable binary/ | tee \
/etc/apt/sources.list.d/jenkins.list > /dev/null
~~~

Ensuite on procède à l’installation:

~~~
# apt update
# apt install fontconfig openjdk-17-jre
# apt install jenkins
~~~


## Configuration

Par défaut Jenkins utilise le port 8080, pour modifier cela, il faut modifier le fichier /etc/default/jenkins et modifier la ligne suivante :

~~~
HTTP_PORT=8080
~~~

Le */home* de jenkins se trouve dans :

~~~
/var/lib/jenkins/
~~~

Il faut créer par exemple la clé SSH de jenkins dans ce répertoire, en se connectant avec l'utilisateur jenkins.

~~~
su - jenkins
~~~

Puis générer la clé SSH.

~~~
ssh-keygen -t ed25519
~~~

### Modifier le répertoire temporaire de Jenkins

Souvent la partition /tmp est en noexec, ce qui pose des problèmes a Jenkins lors d'un clonage de dépôt où il y a besoin d’exécuter un script sh

On peut modifier le dossier tmp dans */var/lib/jenkins/tmp* et bien donner comme propriétaire *jenkins:jenkins* au dossier *tmp*

Il faut ensuite mettre la configuration suivant dans */etc/default/jenkins* :

~~~
# use a different tmpdir for jenkins
JAVA_ARGS="$JAVA_ARGS -Djava.io.tmpdir=/var/lib/jenkins/tmp/"
~~~

Pour le que dossier *tmp* soit vidé au redémarrage de la machine, comme un vrai dossier /tmp, il faut le monter en *tmpfs* dans */etc/fstab* :

~~~
tmpfs                   /var/lib/jenkins/tmp        tmpfs    defaults,nosuid,nodev,size=1024m  0       0
~~~

### Configuration Reverse proxy Apache en HTTPS

Voici une configuration d'un VirtualHost Apache pour proxyfier Jenkins en HTTPS (nécessite d’activer les modules ``proxy`` et ``proxy_http`` d’Apache) :

~~~ apache
<VirtualHost *:80>
    ServerName jenkins.domaine.com
    Redirect permanent / https://jenkins.domaine.com/
</VirtualHost>
<VirtualHost *:443>
    ServerName jenkins.domaine.com
    ServerAdmin webmaster@localhost

    <Directory proxy:http://localhost:8080/*>
	Require all granted
    </Directory>

    ProxyRequests     Off
    ProxyPreserveHost On
    AllowEncodedSlashes NoDecode

    <Location />
        ProxyPass http://localhost:8080/ nocanon retry=0
        ProxyPassReverse http://localhost:8080/
        RequestHeader set X-Forwarded-Proto "https"
        RequestHeader set X-Forwarded-Port "443"
    </Location>

   CustomLog /var/log/apache2/access_jenkins.log combined
   ErrorLog /var/log/apache2/error_jenkins.log

    SSLEngine on
    SSLProtocol all -SSLv2 -SSLv3
    SSLCertificateFile /etc/ssl/certs/foo.crt
    SSLCertificateKeyFile /etc/ssl/private/bar.key
    SSLCertificateChainFile /etc/ssl/certs/DigiCertCA.crt

</VirtualHost>
~~~


## Dépannage

### Problème de configuration de Reverse Proxy

Si sur la page d'administration de Jenkins l'erreur suivante apparaîtra :

~~~
La configuration de votre proxy inverse n'est pas bonne
~~~

Il faut configurer le ProxyPass avec l'option _nocanon_ comme ceci :

~~~ apache
<Location />
        ProxyPass http://localhost:8080/ nocanon
        ProxyPassReverse http://localhost:8080/
        RequestHeader set X-Forwarded-Proto "https"
        RequestHeader set X-Forwarded-Port "443"
 </Location>
~~~

Si une réponse 403 est renvoyée lors de l’envoi d’un formulaire, activer
l’option de compatibilité (« Enable proxy compatibility ») de protection
CSRF dans « Manage Jenkins > Security > Configure Global Security > CSRF
Protection » (ou « Administrer Jenkins -> Sécurité -> Configurer la
sécurité globale -> CSRF Protection -> Activer la compatibilité proxy »)
peut aider.

## Agents

L’exécution des tests ne devrait pas être réalisée sur le nœud intégré,
il faut donc paramétrer des agents, qui seront connectés au contrôleur
dont l’installation vient d’être décrite.
C’est sur ces nœuds que s’exécuterons effectivement les tests.

### Installation

D’après le [projet amont](https://www.jenkins.io/doc/book/using/using-agents/#environment),
Java est nécessaire pour faire fonctionner les agents (mais il y a moyen
de s’en passer comme décrit par
[Debian](https://salsa.debian.org/qa/jenkins.debian.net/-/blob/master/INSTALL)).

Docker est pratique aussi pour certaines constructions. Cela nécessite
l’installation des plugins
[Docker](https://plugins.jenkins.io/docker-plugin/) et
[Docker Pipeline](https://plugins.jenkins.io/docker-workflow/) sur le
contrôleur.

~~~
# apt install default-jre-headless docker.io
~~~

### Configuration

Création de l’utilisateur que l’instance principale contactera pour
exécuter les tests.

~~~
# adduser --disabled-password jenkins
# adduser jenkins docker
~~~

Ajouter la partie publique de la clef configurée précédemment à
`~jenkins/.ssh/authorized_key`. Il peut être intéressant de vérifier
que l’on arrive bien à se connecter depuis le contrôleur à l’agent
par SSH dès maintenant.

#### Déclarer l’agent dans le contrôleur

Le nœud peut maintenant être ajouté depuis l’interface web de Jenkins.

~~~
Build Executor Status (en bas à gauche)
> New node (en haut à gauche)
  > Node name (configurer un nom explicite, par exemple celui de la machine)
  > Type > Permanent Agent
  > Create
    > Remote root directory > /home/jenkins
    > Launch method > Launch agents via SSH
    > Host > nom de la machine (FQDN)
    > Crédentials > Add
      > Kind > SSH Username with private key
      > Username > jenkins
      > Private key > Enter directly > Add > La clef privée déjà créé sur la machine
      > Save
~~~

## Lien avec une forge

Maintenant que l’agent est connecté, il est possible de configurer une
forge contenant les dépôts que l’on souhaite tester.

Pour se connecte à Gitea (par exemple), l’installation d’un
[plugin Gitea](https://plugins.jenkins.io/gitea/) est
nécessaire. Cela peut être réalisé dans l’interface web de Jenkins.

~~~
Manage Jenkins
> Manage Plugins
  > Available
    > Search > Gitea
      > Gitea Plugin (cocher la case de la colonne « Install » initiale)
      > Install without restart
        > Restart Jenkins when installation is complete and no jobs are running
~~~

Dans Gitea, il faut créer un utilisateur (par exemple « jenkins ») et
lui donner accès aux organisations et dépôts que l’on souhait tester
(par exemple en l’ajoutant aux équipes existantes, ou en créant une
nouvelle équipe, par exemple « ci »).
Une fois connecté en tant que ce nouvel utilisateur dans Gitea, il faut
créer un jeton.

~~~
Profiles and Settings… (tout en haut à droite)
> Settings
  > Applications
    > Manage Access Tokens > Generate Token (noter le jeton)
~~~

Dans Jenkins, l’organisation correspondante peut être créée et associée.

~~~
New Item
> Enter an item name (par exemple l’URL de la forge)
  > Organisation Folder
  > OK
    > Projects
      > Repository Sources
        > Add
          > Gitea Organization
            > Credentials
              > Add
                > Kind
                  > Gitea Personnal Access Token
                    > Token (choisir le jeton qui vient d’être défini)
                    > Add
                  > Owner (indiquer le nom d’utilisateur correspondant à l’espace qui sera surveillé, exécuté…)
                  > Save
~~~

Dans Jenkins, tous les dépôts de l’utilisateur précédent peuvent être surveillés, et tout ceux contenant un fichier `.Jenkinsfile` (`Jenkinsfile` par défaut, le choix ici est de rendre le fichier caché comme dans de nombreux autres outils de CI) seront automatiquement testés. Il faut préalablement sélectionner l’organisation précédente.
[Basic Branch Build
Strategies](https://plugins.jenkins.io/basic-branch-build-strategies/)
permet de déclencher les constructions à base de tags.

~~~
Configure
  > Projects
    > Repository Sources
      > Gitea Organization
        > Behaviours > Add > Discover Tags
        > Project Recognizers > Pipeline Jenkinsfile > Script Path > .Jenkinsfile
        > Build strategies > Add > Tags
        > Build strategies > Add > Regular branches (sinon on ne construit plus grand chose…)
~~~

Si nécessaire (par exemple si les évènements capturés sont insuffisant), il est possible d’augmenter la fréquence de de découverte des dépôts, branches et fichiers `.Jenkinsfile` dans le même menu.

~~~
Configure
   > Scan Gitea Organization Triggers
     > 5 minutes
   > Child Scan Triggers
     > 5 minutes
~~~
