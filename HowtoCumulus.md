**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Cumulus

Cumulus Linux est une distribution de Linux axée sur le réseau et basée sur Debian.
C'est un OS qui peut être installé sur un "Bare Metal Switch", c'est-à-dire un switch vendu sans OS.
Voir la liste du matériel compatible : <https://cumulusnetworks.com/support/linux-hardware-compatibility-list/>

Équivalence des commandes Cisco : <https://support.cumulusnetworks.com/hc/en-us/sections/200718008-Cumulus-Linux-Interoperability-and-Conversion-Guides>

Documentations :

<https://docs.cumulusnetworks.com/display/DOCS>

<https://support.cumulusnetworks.com/hc/en-us/>

## Topologie

Pour les exemples, je me baserai sur cette topologie, où une machine sera connectée à leaf1 et une autre à leaf2 : <https://docs.cumulusnetworks.com/display/VX/Using+Cumulus+VX+with+KVM>

## Installation et démarrage des machines virtuelles sous KVM

Téléchargement de l'image disque de la VM : <https://cumulusnetworks.com/cumulus-vx/download/>

Démarrage des machines avec des modifications des commandes données sur le site :

leaf1 :

~~~
sudo /usr/bin/kvm   -curses                             \
                    -name leaf1                       \
                    -pidfile leaf1.pid                \
                    -smp 1                              \
                    -m 256                              \
                    -net nic,macaddr=00:01:00:00:01:00,model=virtio \
                    -net user,hostfwd=tcp::1401-:22 \
                    -netdev socket,udp=127.0.0.1:1602,localaddr=127.0.0.1:1601,id=dev0 \
                    -device virtio-net-pci,mac=00:02:00:00:00:01,addr=6.0,multifunction=on,netdev=dev0,id=swp1 \
                    -netdev socket,udp=127.0.0.1:1606,localaddr=127.0.0.1:1605,id=dev1 \
                    -device virtio-net-pci,mac=00:02:00:00:00:02,addr=6.1,multifunction=off,netdev=dev1,id=swp2 \
                    -netdev socket,udp=127.0.0.1:1611,localaddr=127.0.0.1:1609,id=dev2 \
                    -device virtio-net-pci,mac=00:02:00:00:00:09,addr=6.2,multifunction=off,netdev=dev2,id=swp3 \
                    leaf1.qcow2
~~~

leaf2 :

~~~
sudo /usr/bin/kvm   -curses                             \
                    -name leaf2                       \
                    -pidfile leaf2.pid                \
                    -smp 1                              \
                    -m 256                              \
                    -net nic,macaddr=00:01:00:00:02:00,model=virtio \
                    -net user,hostfwd=tcp::1402-:22 \
                    -netdev socket,udp=127.0.0.1:1604,localaddr=127.0.0.1:1603,id=dev0 \
                    -device virtio-net-pci,mac=00:02:00:00:00:03,addr=6.0,multifunction=on,netdev=dev0,id=swp1 \
                    -netdev socket,udp=127.0.0.1:1608,localaddr=127.0.0.1:1607,id=dev1 \
                    -device virtio-net-pci,mac=00:02:00:00:00:04,addr=6.1,multifunction=off,netdev=dev1,id=swp2 \
                    -netdev socket,udp=127.0.0.1:1612,localaddr=127.0.0.1:1610,id=dev2 \
                    -device virtio-net-pci,mac=00:02:00:00:00:0A,addr=6.2,multifunction=off,netdev=dev2,id=swp3 \
                    leaf2.qcow2
~~~

spine1 :

~~~
sudo /usr/bin/kvm   -curses                             \
                    -name spine1                       \
                    -pidfile spine1.pid                \
                    -smp 1                              \
                    -m 256                              \
                    -net nic,macaddr=00:01:00:00:03:00,model=virtio \
                    -net user,hostfwd=tcp::1403-:22 \
                    -netdev socket,udp=127.0.0.1:1601,localaddr=127.0.0.1:1602,id=dev0 \
                    -device virtio-net-pci,mac=00:02:00:00:00:05,addr=6.0,multifunction=on,netdev=dev0,id=swp1 \
                    -netdev socket,udp=127.0.0.1:1603,localaddr=127.0.0.1:1604,id=dev1 \
                    -device virtio-net-pci,mac=00:02:00:00:00:06,addr=6.1,multifunction=off,netdev=dev1,id=swp2 \
                    spine1.qcow2
~~~

spine2 :

~~~
sudo /usr/bin/kvm   -curses                             \
                    -name spine2                       \
                    -pidfile spine2.pid                \
                    -smp 1                              \
                    -m 256                              \
                    -net nic,macaddr=00:01:00:00:04:00,model=virtio \
                    -net user,hostfwd=tcp::1404-:22 \
                    -netdev socket,udp=127.0.0.1:1605,localaddr=127.0.0.1:1606,id=dev0 \
                    -device virtio-net-pci,mac=00:02:00:00:00:07,addr=6.0,multifunction=on,netdev=dev0,id=swp1 \
                    -netdev socket,udp=127.0.0.1:1607,localaddr=127.0.0.1:1608,id=dev1 \
                    -device virtio-net-pci,mac=00:02:00:00:00:08,addr=6.1,multifunction=off,netdev=dev1,id=swp2 \
                    spine2.qcow2
~~~

Ajout de deux machines :

machine1, connectée à swp3 de leaf1 :

~~~
sudo /usr/bin/kvm   -curses                             \
                    -name machine1                       \
                    -pidfile machine1.pid                \
                    -smp 1                              \
                    -m 256                              \
                    -net nic,macaddr=00:01:00:00:05:00,model=virtio \
                    -net user,hostfwd=tcp::1405-:22 \
                    -netdev socket,udp=127.0.0.1:1609,localaddr=127.0.0.1:1611,id=dev0 \
                    -device virtio-net-pci,mac=00:02:00:00:00:0B,addr=6.0,multifunction=off,netdev=dev0,id=eth1 \
                    machine1.qcow2
~~~

machine2, connectée à swp3 de leaf2 :

~~~
sudo /usr/bin/kvm   -curses                             \
                    -name machine2                       \
                    -pidfile machine2.pid                \
                    -smp 1                              \
                    -m 256                              \
                    -net nic,macaddr=00:01:00:00:06:00,model=virtio \
                    -net user,hostfwd=tcp::1406-:22 \
                    -netdev socket,udp=127.0.0.1:1610,localaddr=127.0.0.1:1612,id=dev0 \
                    -device virtio-net-pci,mac=00:02:00:00:00:0C,addr=6.0,multifunction=off,netdev=dev0,id=eth1 \
                    machine2.qcow2
~~~

Nous pouvons ensuite nous connecter en SSH sur chacune des machines sur les ports 1401 à 1406.

## Configuration de couche 3

Configuration avec du routage entre chaque switch.

### Configuration dans /etc/network/interfaces

Pour leaf1 et leaf2 :

~~~
auto swp1
  iface swp1
  address 10.2.1.X/32

auto swp2
  iface swp2
  address 10.2.1.X/32

auto swp3
  iface swp3
  address 10.4.1.X/24
~~~

Pour spine1 et spine2 :

~~~
auto swp1
  iface swp1
  address 10.2.1.X/32

auto swp2
  iface swp2
  address 10.2.1.X/32
~~~

Avec X = 1 pour leaf1, 2 pour leaf2, 3 pour spine1 et 4 pour spine2

### Quagga

La configuration du routage se fait sous [wiki:HowtoQuagga Quagga].

Activer zebra et ospf dans /etc/quagga/daemons :

~~~
zebra=yes
ospfd=yes
~~~

Configurer ospf dans /etc/quagga/Quagga.conf :

Pour leaf1 et leaf2 :

~~~
service integrated-vtysh-config

interface swp1
  ip ospf network point-to-point

interface swp2
  ip ospf network point-to-point

router-id 10.2.1.X

router ospf
  ospf router-id 10.2.1.X
  network 10.2.1.X/32 area 0.0.0.0
  network 10.4.1.0/24 area 0.0.0.0
~~~

Pour spine1 et spine2 :

~~~
service integrated-vtysh-config

interface swp1
  ip ospf network point-to-point

interface swp2
  ip ospf network point-to-point

router-id 10.2.1.X

router ospf
  ospf router-id 10.2.1.X
  network 10.2.1.X/32 area 0.0.0.0
~~~

## Configuration de couche 2

Configuration avec des VLAN et du Spanning-Tree. Trois VLAN : le 1 est le natif, le 5 où chaque switch a son IP, et le 10 où seuls les deux PCs ont leur IP.

### Configuration dans /etc/network/interfaces

Pour leaf1 et leaf2 :

~~~
auto swp1
  iface swp1

auto swp2
  iface swp2

auto swp3
  iface swp3
  bridge-access 10
  mstpctl-portadminedge yes
  mstpctl-bpduguard yes

auto bridge
  iface bridge
  bridge-vlan-aware yes
  bridge-ports glob swp1-3
  bridge-stp on
  bridge-vids 5 10
  bridge-pvid 1

auto bridge.5
  iface bridge.5
  address 10.0.5.X/24
~~~

Pour spine1 et spine2 :

~~~
auto swp1
  iface swp1

auto swp2
  iface swp2

auto bridge
  iface bridge
  bridge-vlan-aware yes
  bridge-ports glob swp1-2
  bridge-stp on
  bridge-vids 5 10
  bridge-pvid 1

auto bridge.5
  iface bridge.5
  address 10.0.5.X/24
~~~

Les ports swp1 et swp2 sont en mode trunk pour les vlan 1, 5 et 10 grâce à la configuration de bridge dont ils héritent.
Le port swp3 n'hérite plus de la configuration VLAN de bridge puisqu'il est configuré en mode access sur le vlan 10, le stp portfast et le bpduguard sont activés.
Le vlan 5 est configuré pour le réseau 10.0.5.0/24.

### Vérifications

Interfaces :

~~~
leaf1# netshow interface bridge
--------------------------------------------------------------------
To view the legend,  rerun "netshow" cmd with the  "--legend" option
--------------------------------------------------------------------
    Name    Mac                Speed      MTU  Mode
--  ------  -----------------  -------  -----  ---------
UP  bridge  00:02:00:00:00:01  N/A       1500  Bridge/L2


-------------------------  ----------------------
STP Mode:                  RSTP / Single Instance
Root Port:                 RootSwitch
Ports in Designated Role:  swp1-3
Ports in Alternate Role:   None
Root Priority:             32768
Bridge Priority:           32768
Last TCN:                  swp1 (0:00:31)
Bridge Type                Vlan Aware Bridge


Ports in Forwarding State
---------------------------
swp1-3


Ports in Oper Edge Port State
-------------------------------
swp3
~~~

~~~
leaf1# netshow interface swp1
--------------------------------------------------------------------
To view the legend,  rerun "netshow" cmd with the  "--legend" option
--------------------------------------------------------------------
    Name    Mac                Speed      MTU  Mode
--  ------  -----------------  -------  -----  --------
UP  swp1    00:02:00:00:00:01  10G       1500  Trunk/L2


Vlan List
-----------
1, 5, 10

Untagged Vlans
----------------
1

Vlans In Designated State
---------------------------
1, 5, 10

Vlans In Forwarding State
---------------------------
1, 5, 10


LLDP
------  ----  -------------
swp1    ====  swp1(cumulus)
~~~

STP :

~~~
leaf1# mstpctl showbridge bridge
bridge CIST info
  enabled         yes
  bridge id       8.000.00:02:00:00:00:01
  designated root 8.000.00:02:00:00:00:01
  regional root   8.000.00:02:00:00:00:01
  root port       none
  path cost     0          internal path cost   0
  max age       20         bridge max age       20
  forward delay 15         bridge forward delay 15
  tx hold count 6          max hops             20
  hello time    2          ageing time          300
  force protocol version     rstp
  time since topology change 68666s
  topology change count      11
  topology change            no
  topology change port       swp3
  last topology change port  swp2
~~~

~~~
leaf1# mstpctl showport bridge
   swp1  8.003 forw 8.000.00:02:00:00:00:01 8.000.00:02:00:00:00:01 8.003 Desg
   swp2  8.001 forw 8.000.00:02:00:00:00:01 8.000.00:02:00:00:00:01 8.001 Desg
   swp3  8.002 forw 8.000.00:02:00:00:00:01 8.000.00:02:00:00:00:01 8.002 Desg

~~~

VLAN :

~~~
leaf1# bridge vlan show
port	vlan ids
swp1	 1 PVID Egress Untagged
	 5
	 10

swp2	 1 PVID Egress Untagged
	 5
	 10

swp3	 10 PVID Egress Untagged

bridge	 5
~~~

### Explications des options et autres possibilités

* _bridge-vlan-aware yes_ : activer les VLAN
* _bridge-stp on_ : activer STP
* _bridge-vids 5 10_ : trunk pour les vlan 5 et 10
* _bridge-pvid 1_ : vlan natif
* _bridge-access 10_ : port d'accès pour le vlan 10
* _mstpctl-portadminedge yes_ : activer le portfast
* _mstpctl-bpduguard yes_ : activer le BPDUguard
* _mstpctl-treeprio 32768_ : modifier la priorité STP du switch
* _mstpctl-treeportprio swp3=128_ : modifier la priorité STP d'un port
* _link-speed 100_ : régler la vitesse du port à 100M
* _link-duplex full_ : régler le mode full duplex

Voir toutes les options disponibles avec la commande _ifquery --syntax-help_

### Boucle/Intervalle

Possibilité d'utiliser des boucles directement dans /etc/network/interfaces :

~~~
%for v in [5,10,30]:
    auto bridge.${v}
      iface bridge.${v}
      address 10.0.${v}.1/24
%endfor
~~~

Ou

~~~
%for port, vlanid in zip(range(2, 20), range(2004, 2022)) :
    auto swp${port}
      iface swp${port}
      bridge-access ${vlanid}
%endfor
~~~

Ou même un intervalle :

~~~
auto bridge.[1-2000]
  iface bridge.[1-2000]
  ATTRIBUT
~~~

### Modification à chaud

Créer un bridge et y ajouter trois ports :

~~~
# brctl addbr bridge
# brctl addbr bridge.5
# brctl addif bridge swp1 swp2 swp3
# ip link set up dev bridge
# for I in {1..3}; do  ip link set up dev swp$I; done
# brctl show
~~~

Ajouter une adresse IP :

~~~
# ip addr add 10.0.5.1/24 dev bridge.5
~~~

Activer STP :

~~~
# brctl stp bridge on
~~~

Mettre un port en STP portfast et activer le BPDUguard :

~~~
# mstpctl setportadminedge bridge swp3 yes
# mstpctl setbpduguard bridge swp3 yes
~~~

Cependant, il n'y a pas de commandes pour modifier à chaud la configuration VLAN. Pour cela, il faut modifier le fichier /etc/network/interfaces puis prendre en compte les modifications avec _ifreload_ ou _ifup nom_interface_. Dans ce cas là, les précédentes modifications faites à chaud ne restent pas.