---
categories: databases
title: Howto Percona Monitoring and Management (PMM)
...

- Documentation : [https://docs.percona.com/percona-monitoring-and-management/index.html](https://docs.percona.com/percona-monitoring-and-management/index.html)
- Docker Hub : [https://hub.docker.com/r/percona/pmm-server](https://hub.docker.com/r/percona/pmm-server)
- Code : [https://github.com/percona/pmm](https://github.com/percona/pmm)
- Licence : [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/#AGPL)

*PMM* est une application client/serveur qui permet de monitorer et de manager des instances MariaDB, Mysql, PostgreSQL, MongoDB et ProxySQL.
PMM vous aide à améliorer les performances des bases de données, à simplifier leur gestion et à renforcer leur sécurité.

# Installation avec l'image Docker

PMM Serveur peut s'installer de différente manière, on va détailler l'installation dans un conteneur Docker, avec l'image fournie par Percona.

## Prérequis

* Sur la machine hôte, il faut Docker en version 1.12.6 ou plus récent.

On utilise l'image Docker de Percona sur [hub.docker.com](https://hub.docker.com/r/percona/pmm-server/tags)

* Si on démarre le conteneur seul, on copie l'image comme ceci :

~~~
$ docker pull percona/pmm-server:2.31.0
~~~

* On utilise un volume ext4 monté dans */srv*, par exemple, et on démarre l'image comme ceci :

~~~
$ docker run --detach --restart always \
--publish 8333:443 \
-v pmm-data:/srv \
--name pmm-server \
percona/pmm-server:2.31.0
~~~  

* Si on utilise Docker Swarm pour déployer la stack, on peut utiliser une configuration similaire dans son fichier .yml :

~~~{.yaml}
pmms:
    image: percona/pmm-server:2.31.0
    deploy:
      replicas: 1
      restart_policy:
          condition: on-failure
    ports:
      - 8333:443
    volumes:
      - /mnt/pmm_server/srv/:/srv/
~~~

* Une fois le conteneur démarré, la première chose à faire est de changer le mot de passe de l'utilisateur `admin` :

~~~
$ docker exec -t pmm-server change-admin-password <nouveau_mot_de_passe>
~~~

On peut ensuite se connecter sur https://{IP_SERVEUR}:8333/ pour se connecter à l'interface de PMM.

# Installation des clients pour Percona Monitoring and Management (PMM)

PMM a besoin de `client` sur les serveur SQL, qui collecte les metriques de l'instance SQL, et qui le renvoie au serveur PMM.

* En premier lieu, il faut créer l'utilisateur SQL dédié à PMM sur l'instance que l'on veut monitorer :

~~~{.sql}
CREATE USER 'pmm'@'%' IDENTIFIED BY '{Password}' WITH MAX_USER_CONNECTIONS 10;
GRANT SELECT, PROCESS, REPLICATION CLIENT, RELOAD ON *.* TO 'pmm'@'%';
~~~

Si l'on utilise MySQL 8.0 il faut rajouter le droit `BACKUP_ADMIN` :

~~~{.sql}
GRANT SELECT, PROCESS, REPLICATION CLIENT, RELOAD, BACKUP_ADMIN ON *.* TO 'pmm'@'%';
~~~

* Puis sur le serveur SQL, on lance la commande qui permet d'ajouter l'instance SQL au serveur PMM :

~~~
$ pmm-admin config --server-url "https://admin:{Password}@IP_SERVEUR:8333" --server-insecure-tls
$ pmm-admin add mysql --host=IP_SERVEUR -port=3306 --username=pmm --password={Password}
~~~

Si on a plusieurs serveurs SQL ou plusieurs instances à monitorer, il faut répéter l'opération sur chaque serveur et/ou ports.

## Activation de différentes métriques sur les serveurs MySQL / MariaDB.

En fonction de ce que vous souhaitez comme métriques sur chaque source, il est nécessaire d'activer certaines fonctions.

En général les choix se portent sur les `Slow query log` et `Performance Schema`.

Bien que vous puissiez utiliser les deux en même temps, nous vous recommandons d'en utiliser qu'un seul à la fois.
Il y a certains chevauchements dans les données rapportées, et chacun entraîne une légère perte de performance.

Le choix dépend de la variante de votre instance MySQL / MariaDB, et de la quantité de détails que vous souhaitez voir.

* Pour MySQL 5.1 et antérieur il est préférable d'activer que les `Slow query log`

* Pour MySQL 5.6 et suprérieur ou MariaDB 10.0 et supérieur, il est préférable d'activer `Performance Schema`

### Activation de `Slow query log` :

On active le `Slow query log` comme ceci dans la configuration :

~~~
slow_query_log=ON
long_query_time=2
log_slow_admin_statements=ON
log_slow_slave_statements=ON
~~~

Où comme ceci dans le Shell :

~~~{.sql}
SET GLOBAL slow_query_log = 1;
SET GLOBAL long_query_time = 0;
SET GLOBAL log_slow_admin_statements = 1;
SET GLOBAL log_slow_slave_statements = 1;
~~~

### Activation de `Performance Schema` :

On active le `Performance Schema` comme ceci dans la configuration :

~~~
performance_schema=ON
performance-schema-instrument='statement/%=ON'
performance-schema-consumer-statements-digest=ON
innodb_monitor_enable=all
~~~

Où comme ceci dans le Shell :

~~~{.sql}
UPDATE performance_schema.setup_consumers
SET ENABLED = 'YES' WHERE NAME LIKE '%statements%';
SET GLOBAL innodb_monitor_enable = all;
~~~

### Activation de Query response time

On peut activer un plugin que mesure le temps de distribution d'une requête, montrant la proportion du temps consacré a diverses activités.

Activation du plugin dans MariaDB 10.3+, directement sans redémarrage :

~~~{.sql}
INSTALL PLUGIN QUERY_RESPONSE_TIME_AUDIT SONAME 'query_response_time.so';
INSTALL PLUGIN QUERY_RESPONSE_TIME SONAME 'query_response_time.so';
SET GLOBAL query_response_time_stats = ON;
~~~

Ou dans le fichier de configuration, un rédémarrage est nécessaire :

~~~
[mariadb]
...
plugin_load_add = query_response_time
~~~

### Activation de User statistics

On peut activer également les statistiques sur l'activé de l'utilisateur, les détails d'accès aux tables individuelles et aux indexs.

Dans le fichier de configuration :

~~~
userstat=ON
~~~

Dans le shell :

~~~{.sql}
SET GLOBAL userstat = ON;
~~~
