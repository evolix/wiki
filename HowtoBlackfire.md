https://blackfire.io/

# Installation

Source : https://blackfire.io/docs/up-and-running/installation?action=install&mode=full&version=latest&mode=full&location=server&os=debian&language=php

On installe la clé de signature des paquets de blackfire, on rajoute le dépôt blackfire au dépôt que la machine utilise puis on installe les paquets nécessaires :

``` sh
(umask 022; curl -o /etc/apt/trusted.gpg.d/blackfire.asc https://packages.blackfire.io/gpg.key)
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/trusted.gpg.d/blackfire.asc] http://packages.blackfire.io/debian any main" > /etc/apt/sources.list.d/blackfire.list
apt update

apt install blackfire blackfire-php
```

# Configuration

Il faut a minima définir `server-id` et `server-token` dans le fichier de configuration de l'agent `/etc/blackfire/agent`, puis le redémarrer :

``` sh
vim /etc/blackfire/agent
systemctl restart blackfire-agent.service
blackfire agent:healthcheck
```

Finalement, il faut redémarrer le serveur PHP pour qu'il charge le module `blackfire`, par exemple avec :
``` sh
systemctl restart php7.4-fpm.service
```