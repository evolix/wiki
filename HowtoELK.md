---
title: Howto Stack Elastic
categories: nosql pack
---

Cette documentation décrit l'installation et la configuration de la suite Elastic, anciennement nommée **ELK** (Elasticsearch + Logstash + Kibana).

## Pré-requis

Les composants de la suite doivent être installés à partir du dépôt APT de la société Elastic (éditrice de la suite Elastic)

~~~
# echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" >> /etc/apt/sources.list.d/elastic.list
# cd /etc/apt/trusted.gpg.d
# wget https://artifacts.elastic.co/GPG-KEY-elasticsearch -O GPG-KEY-elasticsearch.asc
# chmod 644 GPG-KEY-elasticsearch.asc
~~~

Pour Elasticsearch et Logstash, il faut avoir Java 1.8, disponible depuis les `jessie-backports`.

~~~
# apt install openjdk-8-jre
# update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
~~~

## Elasticsearch

Il existe un guide détaillé d'installation et configuation pour Elasticsearch : [HowtoElasticsearch#installation]().

### Installation

~~~
# apt install elasticsearch
~~~

### Démarrage

~~~
# systemctl enable elasticsearch
# systemctl start elasticsearch
~~~

### Configuration

Préciser *node.name* dans le fichier `/etc/elasticsearch/elasticsearch.yml` :

~~~{.yaml}
node.name: bar
~~~

## Logstash

Logstash est un service qui récupère des données depuis des sources variées (ports TCP/UDP, fichiers…), les transforme et les renvois vers des sorties variées (fichiers, Elasticsearch, syslog…).
C'est un logiciel écrit en Ruby, qui tourne sur une JVM (grace à JRuby).

Documentation officielle : <https://www.elastic.co/guide/en/logstash/5.0/index.html>

### Installation

~~~
# apt install logstash
# systemctl enable logstash
# systemctl start logstash
~~~

Dans `/etc/default/logstash` il faut penser à ajuster la ram allouée.

Logstash fait lui-même la rotation et la compression des logs, mais ne les supprime jamais. On peut ajouter ce script en cron :

~~~{.bash}
#!/bin/sh

LOG_DIR=/var/log/logstash
MAX_AGE=365

find ${LOG_DIR} -type f -user logstash -name "logstash.log.*.gz" -mtime +${MAX_AGE} -delete
find ${LOG_DIR} -type f -user root -name "logstash.err.*.gz" -mtime +${MAX_AGE} -delete
find ${LOG_DIR} -type f -user root -name "logstash.stdout.*.gz" -mtime +${MAX_AGE} -delete
~~~

### Configuration

À ce stade, aucun "pipeline" Logstash n'est configuré.
Voir <https://www.elastic.co/guide/en/logstash/5.0/configuration.html>

### Ajout de plugins

Il est possible d'installer des plugins logstash via la commande `logstash-plugin`.

Pour installer par exemple le plugin « logstash-input-jdbc » :

~~~
# cd /usr/share/logstash/
/usr/share/logstash# bin/logstash-plugin install logstash-input-jdbc
Validating logstash-input-jdbc
Installing logstash-input-jdbcio/console on JRuby shells out to stty for most operations
Installation successful
~~~

## Kibana

Kibana est une interface de visualisation des donnes stockées dans Elasticsearch.
C'est un logiciel écrit principalement en NodeJS, où tout est embarqué dans un processus principal.

Documentation officielle : <https://www.elastic.co/guide/en/kibana/5.0/index.html>

### Installation

~~~
# apt install kibana
~~~

~~~
# vim /etc/logrotate.d/kibana
/var/log/kibana/*.log {
        daily
        rotate 7
        copytruncate
        compress
        delaycompress
        missingok
        notifempty
}
~~~

### Démarrage

~~~
# systemctl enable kibana
# systemctl start kibana
~~~

L'interface principale est disponible sur <http://127.0.0.1:5601/>

### Proxy

Kibana ne gère pas d'authentification. Pour le rendre accessible de manière sécurisée, il est conseillé de le placer derrière un reverse proxy, par exemple Nginx.

~~~
upstream kibana {
  server 127.0.0.1:5601 fail_timeout=0;
}
server {
  charset utf-8;

  # ajouter les règles d'authentification

  listen         _IP_PUBLIQUE_:80;
  server_name    _HOSTNAME_;

  location / {
    proxy_redirect off;
    proxy_pass http://kibana/;
    proxy_set_header    X-Real-IP   $remote_addr;
    proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto  $scheme;
    proxy_set_header    X-Forwarded-Server  $host;
    proxy_set_header    X-Forwarded-Host  $host;
    proxy_set_header    Host  $host;
  }
}
~~~

## Conseils

* utiliser des certificats SSL (nativement géré par les logiciels) pour que les données circulent de manière chiffrée (surtout entre filebeat et logstash, car ça passe par Internet).

## Support

Une grille officielle de support est disponible sur <https://www.elastic.co/support/matrix> Elle couvre la compatibilité avec les OS, JVM, navigateurs…

La politique de durée de support des versions est disponible sur <https://www.elastic.co/support/eol>
