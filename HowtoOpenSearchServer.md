**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto OpenSearchServer

## Installation OpenSearchServer

### Installer Java

~~~
# apt-get install openjdk-7-jdk
~~~

### Installer OpenSearchServer

Télécharger la dernière version sur le site <http://www.opensearchserver.com/>

~~~
dpkg -i opensearchserver-X.X.X-bXXX.deb
~~~

Une fois installé voici les dossiers et fichiers importants :

/var/lib/opensearchserver: Contains all your data (indexes)
/usr/share/opensearchserver: Contains all your OSS binaries
/etc/opensearchserver: The configuration file of your OpenSearchServer instance
/etc/init.d/opensearchserver: The script used by the system to start and stop the OpenSearchServer instance.

~~~
/etc/init.d/opensearchserver stop
mkdir -p /home/opensearchserver/share
mkdir /home/opensearchserver/lib
mv /usr/share/opensearchserver /home/opensearchserver/share
mv /var/lib/opensearchserver /home/opensearchserver/lib
ln -s /home/opensearchserver/share/opensearchserver /usr/share/opensearchserver
ln -s /home/opensearchserver/lib/opensearchserver /var/lib/opensearchserver
service opensearchserver start
~~~