---
title: DRBD – cas pratique : empilement de ressources (avec méta-données externes)
---

## Migration à chaud d'une .img via volumes DRBD empilés

> **Attention : ce « retour d'expérience » est incomplet et invalide !** ; on ne peut pas « empiler » un volume drbd sur un autre, sans utiliser les directives prévues à cet effet. Il faut donc forcément utiliser la méthode officielle : <http://www.drbd.org/en/doc/users-guide-83/s-three-nodes>.

Dans ce scénario, on dispose de VMs avec leur stockage sur un fichier ".img". L'idée est de migrer le disque .img à chaud sur un autre hyperviseur en utilisant DRBD en mode « stacked » (empilé).

Nous utilisons 3 hyperviseurs : **foo**, **bar** et **baz** : fichier .img → loop device → DRBD foo - bar → DRBD bar - baz

On monte l'image sur un _loop device_.

~~~
foo# losetup /dev/loop0 qux.img
~~~

On monte un DRBD entre `foo:/dev/loop0` et `bar:/dev/drbd0` (`bar:/dev/drbd0` étant entre **bar** et **baz**).
Il faudra stocker les meta-données en externe. On calcule la taille nécessaire.

<http://serverfault.com/questions/433999/calculating-drbd-meta-size>

metacalc.sh :

~~~{.bash}
#!/bin/bash

which bc >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    echo "Error: bc is not installed"
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "Error: Please supply block device path"
    echo "Eg. /dev/vg1/backups"
    exit 1
fi

DEVICE=$1

SECTOR_SIZE=$( blockdev --getss $DEVICE )
SECTORS=$( blockdev --getsz $DEVICE )
MD_SIZE=$( echo "((($SECTORS + (2^18)-1) / 262144 * 8) + 72)" | bc )
FS_SIZE=$( echo "$SECTORS - $MD_SIZE" | bc )

MD_SIZE_MB=$( echo "($MD_SIZE / 4 / $SECTOR_SIZE) + 1" | bc )
FS_SIZE_MB=$( echo "($FS_SIZE / 4 / $SECTOR_SIZE)" | bc )

echo "Filesystem: $FS_SIZE_MB MiB"
echo "Filesystem: $FS_SIZE Sectors"
echo "Meta Data:  $MD_SIZE_MB MiB"
echo "Meta Data:  $MD_SIZE Sectors"
echo "--"
echo "Resize commands: resize2fs -p "$DEVICE $FS_SIZE_MB"M; drbdadm create-md res"
~~~

~~~
foo# bash metacalc.sh /dev/loop0
Meta Data:  1 MiB
~~~

On pourra ajouter une marge au cas où…
Si on n'a pas la possibilité d'utiliser LVM, on utilisera un fichier plus un loop device :

~~~
foo# dd if=/dev/zero of=./qux.img.drbd.metadata bs=1M count=2
foo# losetup /dev/loop1 qux.img.drbd.metadata
~~~

Si l'on peut utiliser LVM, il suffira de créer un volume dédié.

Maintenant que l'on a un _device_ pour les meta-données, on créer la ressource DRBD.

Sur **foo** :

~~~
resource "qux" {
    net {
        cram-hmac-alg "sha1";
        shared-secret "YOURSHAREDSECRET";
        # Dans le cadre d'une migration se mettre en mode A ?
        protocol C;
        allow-two-primaries;
        # Tuning perf.
        max-buffers 8000;
        max-epoch-size 8000;
        sndbuf-size 0;
    }
    # A utiliser si RAID HW avec cache + batterie
    disk {
        disk-barrier no;
        disk-flushes no;
    }
    volume 0 {
        device minor 126;
        disk /dev/loop0;
        meta-disk /dev/loop1;
    }
    on foo {
        address 192.0.2.10:7900;
    }
    on bar {
        address 192.0.2.11:7900;
    }
}
~~~

Attention si vous n'utilisez pas la même version de DRBD, la config est différente. Exemple avec la version 8.3 :

~~~
resource "qux" {
  protocol C;
    net {
        cram-hmac-alg "sha1";
        shared-secret "YOURSHAREDSECRET";
        allow-two-primaries;
        # Tuning perf.
        max-buffers 8000;
        max-epoch-size 8000;
        sndbuf-size 0;
    }
    on foo {
        device /dev/drbd126;
        disk /dev/loop0;
        flexible-meta-disk /dev/loop1;
        address 192.0.2.10:7900;
    }
    on bar {
        device /dev/drbd126;
        disk /dev/drbd0;
        meta-disk /dev/hdd/zedmel2-drbd-metadata[0];
        address 192.0.2.11:7900;
    }
}
~~~

Sur **bar** :

~~~
resource "qux" {
    net {
        cram-hmac-alg "sha1";
        shared-secret "YOURSHAREDSECRET";
        # Dans le cadre d'une migration se mettre en mode A ?
        protocol C;
        allow-two-primaries;
        # Tuning perf.
        max-buffers 8000;
        max-epoch-size 8000;
        sndbuf-size 0;
    }
    # A utiliser si RAID HW avec cache + batterie
    disk {
        disk-barrier no;
        disk-flushes no;
    }
    volume 0 {
        device minor 126;
        disk /dev/drbd0;
        meta-disk /dev/vg0/lv0-qux-drbd-metadata;
    }
    on foo {
        address 192.0.2.10:7900;
    }
    on bar {
        address 192.0.2.11:7900;
    }
}
~~~

On arrête la VM **qux**.

~~~
foo# virsh stop qux
~~~

On active la nouvelle ressource DRBD.

~~~
foo# drbdadm create-md qux
bar# drbdadm create-md qux
foo# /etc/init.d/drbd reload
bar# /etc/init.d/drbd reload
foo# drbdadm attach qux
bar# foo# drbdadm attach qux
foo# drbdadm -- --overwrite-data-of-peer primary qux
~~~

On édite sa config pour être mettre `/dev/drbdX` en tant que disque. Et on démarre

~~~
foo# virsh edit
foo# vrish start
~~~

Une fois la synchro terminé. On pourra éteindre la VM et la démarrer sur **bar**.

On pourra supprimer la ressources DRBD qui a servi à la migration ainsi que le disque de méta-données.