---
categories: network security sysadmin
title: Kerberos
---

* Documentation : <https://web.mit.edu/Kerberos/krb5-1.12/doc/>

## Installation

Pour un serveur maître (celui qui s'occupe des comptes)

~~~
# apt install krb5-admin-server krb5-kdc
~~~

Il faut ensuite créer la base de donnée KDC (après la configuration du fichier `/etc/krb5kdc/kdc.conf`) pour cela il suffit d'utiliser la commande suivante (il faudra choisir un mot de passe pour la clé maître qui sera sauvegardé dans `/etc/krb5kdc/kadm5.keytab`):

~~~
# kdb5_util create -s
Loading random data
Initializing database '/var/lib/krb5kdc/principal' for realm 'EXAMPLE.COM',
master key name 'K/M@TEST.EVOLIX.NET'
You will be prompted for the database Master Password.
It is important that you NOT FORGET this password.
Enter KDC database master key:
Re-enter KDC database master key to verify:
~~~

Pour serveur esclave (suit les mise à jour et permet connection)

~~~
# apt install krb5-kpropd krb5-kdc
~~~

Pour client (serveur ssh + machine de connexion)

~~~
# apt install krb5-user
~~~

## Configuration

> **Note :** Les serveurs ont besoin d'un "principal" correspondant au résultat d'un requête DNS inverse sur leur IP pour les connections par adresses IP (NFS ou SSH sans DNS).

Par défaut l'installation sous Debian demande à la fois le royaume Kerberos auquel l'ordinateur appartient (en général le nom de domaine en majuscule est utilisé), l'adresse des serveurs de clés (KDC, donc l'ensemble serveurs maîtres et serveurs esclaves) et les adresses des serveurs maître (pour changement de mot de passe entre autre).

Il peut être préférable de remplacer le fichier `/etc/krb5.conf` générer à l'installation par Debian car celui ci contient de royaumes prédéfinis (exemple pour example.com avec le serveur Kerberos sous kerberos.example.com):

~~~{.conf}
[libdefaults]
    default_realm = EXAMPLE.COM

    kdc_timesync = 1
    ccache_type = 4
    forwardable = true
    proxiable = true

[realms]
    EXAMPLE.COM = {
        kdc = kerberos.example.com
        admin_server = kerberos.example.com
    }

[domain_realm]
    .example.com = EXAMPLE.COM
    example.com = EXAMPLE.COM

~~~

### Configuration des serveurs

> Note: Le nom de royaume (realm) est par convention le nom de domaine DNS en majuscule.

Les serveurs sont de plus configurés par le serveur `/etc/krb5kdc/kdc.conf` qui doit être identique entre serveurs maître et serveurs esclaves.

~~~{.conf}
[kdcdefaults]
    kdc_ports = 750,88

[realms]
    EXAMPLE.COM = {
        database_name = /var/lib/krb5kdc/principal
        admin_keytab = FILE:/etc/krb5kdc/kadm5.keytab
        acl_file = /etc/krb5kdc/kadm5.acl
        key_stash_file = /etc/krb5kdc/stash
        kdc_ports = 750,88
        max_life = 10h 0m 0s
        max_renewable_life = 7d 0h 0m 0s
        master_key_type = aes256-sha2
        #supported_enctypes = aes256-cts:normal aes128-cts:normal
        default_principal_flags = +preauth
    }
~~~
Il est aussi nécessaire de créer le fichier `/etc/krb5kdc/kadm5.acl` pour définir les accès à l'interface d'administration par principal (il n'est pas possible de démarré le serveur d'administration, permettant les changements de mot de passes et la mise à jour des serveurs esclave sans ce fichier) au moins un principal a besoin d'avoir un accès administrateur pour pouvoir administrer le serveur lorsqu'il est en fonctionnement.

<!-- Insérer description de la configuration du fichier ici -->

Enfin, il est possible d'administrer le serveur localement **lorsque le serveur d'administration est éteint** par le biais de la commande `kadmin.local` (qui ne fonctionne que si l'utilisateur a accès en écriture à la base de donnée et en lecture à `/etc/krb5kdc/kadm5.keytab`).

## Administration de la base de données Kerberos

Pour administrer la base de données Kerberos il faut utiliser la commande `kadmin` sur un poste correctement configurer (ou lui passé au minimum l'argument `-s <ip_du_serveur>:<port>`) cette commande nécessite une authentification par Kerberos et utilise par défaut le principal `<utilisateur>/admin`.

### Créer un utilisateur ("principal")

~~~
kadmin> add_principal <utilisateur>[/role]
~~~

pour une machine serveur SSH:

~~~
kadmin> add_prinipal -randkey host/<domaine_de_la_machine>
~~~

pour une machine utilisant NFS (client ou serveur):

~~~
kadmin> add_principal -randkey nfs/<domaine_de_la_machine>
~~~

par example :

~~~
kadmin> add_principal -randkey host/machine.example.com
~~~

### Lister les utilisateurs

~~~
kadmin> list_principals
~~~

### Obtenir des informations sur un utilisateur

~~~
kadmin> get_principal <nom_d'utilisateur>
~~~

### Permettre aux machines de s'identifier

Cela est nécessaire pour le bon fonctionnement des services utilisant Kerberos.

La version ci-dessous assume que la machine utilise Kerberos pour SSH et NFS et que l'utilisateur est connecté sur cette machine.

~~~
$ kadmin
Authenticating as principal admin/admin@EXAMPLE.COM with password.
Password for admin/admin@EXAMPLE.COM:
kadmin> ktadd -keytab machine.keytab host/<domaine_de_la_machine> nfs/<domaine_de_la_machine>
kadmin> quit
# cp machine.keytab /etc/krb5.keytab
~~~

> Note: Il est aussi possible d'utiliser kadmin directement en tant que root ce qui permet d'ajouter une principal dans le fichier keytab directement sans avoir à lister l'ensemble des principal y étant déjà.

### Ajouter une politique de sécurité

~~~
kadmin> add_policy [options] <policy>
~~~

les options étant:

~~~
-maxlife <temp_de_vie_maximum_du_mot_de_passe> \
-minlife <temp_de_vie_minimum_du_mot_de_passe> \
-minlength <longeur_minimal_du_mot_de_passe> \
-minclasses <classes_de_char_minimal> \
-maxfailures <nombre> \
-failurecountinterval <temps> \
-lockoutduration <temps> \
~~~

Il faut ensuite ajouter les "principal" à cette politique par le biais de l'argument `-policy` des commandes `add_principal` et `modify_principal`.

## Obtention d'un ticket Kerberos

~~~
$ kinit [nom_utilisateur_Kerberos]
~~~

Par défaut le nom d'utilisateur Kerberos utilisé est le nom d'utilisateur système.

## Utilisation avec SSH

Pour utiliser Kerberos avec SSH il faut que la machine soit configurée pour utiliser Kerberos et ai la possibilité d'utiliser le principal `host/<nom_DNS_de_la_machine>` (exemple: `host/ssh.example.com`). Ensuite il faut ajouter les options suivantes au fichier `/etc/ssh/sshd_config`:

~~~{sshd_config}
GSSAPIAuthentication yes
GSSAPICleanupCredentials yes
GSSAPIStrictAcceptorCheck yes
GSSAPIKeyExchange no
~~~

Il est ensuite possible de se connecté en SSH sur cette machine en utilisant Kerberos simplement en ayant un ticket Kerberos et en utilisant `ssh -K`.

## Utilisation avec NFSv4

Pour utiliser Kerberos avec NFSv4 il faut que les machines utilisant NFS (serveur et client) soient configurées pour utiliser Kerberos et aient la possibilité d'utiliser le principal `nfs/<nom_DNS_de_la_machine>` **et** que tout utilisateur utilisant NFS doivent avoir un principal Kerberos associé pour pouvoir accéder aux fichiers.

Une fois cela fait il suffit d'ajouter `sec=krb5`, `sec=krb5i` ou `sec=krb5p` dans les options dans `/etc/exports`. Par exemple, pour un chemin exporté dans `/srv/nfs` vers un réseau `192.0.2.0/24`:

~~~{.exports}
/srv/nfs 192.0.2.0/24(rw,sync,no_subtree_check,sec=krb5p)
~~~

Pour les machines clients le montage ce fait exactement de la même manière que sans Kerberos même si il est possible de précisé la sécurisation dans les options de montage par `sec=krb5/krb5i/krb5p`.

## Plomberie

* Kerberos v5: <https://tools.ietf.org/html/rfc4120>

