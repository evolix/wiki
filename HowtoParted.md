---
categories: system filesystem
title: Howto Parted
...

* Documentation : <https://www.gnu.org/software/parted/manual/parted.html>

[Parted](https://www.gnu.org/software/parted/) est un gestionnaire de partitions disque, constitué de la bibliothèque _libparted_ et de la commande _parted_. C'était l'un des premiers outils à gérer correctement les partitionnements de type GPT, raison pour laquelle nous l'utilisons depuis longtemps. Il est également très utile via la version [Live-CD de GParted](https://gparted.org/livecd.php). 

## Installation

~~~
# apt install parted

$ /sbin/parted -v
parted (GNU parted) 3.2
Copyright (C) 2014 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by <http://git.debian.org/?p=parted/parted.git;a=blob_plain;f=AUTHORS>.
~~~

## Utilisation de base

Commandes disponibles :

~~~
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) help                                                             
  align-check TYPE N                       check partition N for TYPE(min|opt) alignment
  help [COMMAND]                           print general help, or help on COMMAND
  mklabel,mktable LABEL-TYPE               create a new disklabel (partition table)
  mkpart PART-TYPE [FS-TYPE] START END     make a partition
  name NUMBER NAME                         name partition NUMBER as NAME
  print [devices|free|list,all|NUMBER]     display the partition table, available devices, free space,
                                           all found partitions, or a particular partition
  quit                                     exit program
  rescue START END                         rescue a lost partition near START and END
  resizepart NUMBER END                    resize partition NUMBER
  rm NUMBER                                delete partition NUMBER
  select DEVICE                            choose the device to edit
  disk_set FLAG STATE                      change the FLAG on selected device
  disk_toggle [FLAG]                       toggle the state of FLAG on selected device
  set NUMBER FLAG STATE                    change the FLAG on partition NUMBER
  toggle [NUMBER [FLAG]]                   toggle the state of FLAG on partition NUMBER
  unit UNIT                                set the default unit to UNIT
  version                                  display the version number and copyright information of GNU Parted
~~~

### Créer une table de partitions

Exemple avec un table de partitions GPT sur un volume tout neuf :

~~~
# parted /dev/sdz

(parted) p                                                                
Error: /dev/sdz: unrecognised disk label                                  

(parted) mklabel gpt                                                      

(parted) p                                                                
Model: IFT DS S12E-G2140-4 (scsi)
Disk /dev/sdz: 7998GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
~~~

### Créer une partition

Attention : le comportement de la commande `mkpart` n’est pas le même si la table de partition est en GPT ou MBR. En MBR, le premier paramètre indique si la partition est primaire ou logique. En GPT, le premier paramètre donne le label de la partition, donc on entrera plutôt `EFI system partition` ou `Home partition` au lieu de `primary`.

Exemple avec une partition de type _ext4_ qui occupe tout le volume :

~~~
# parted /dev/sdz

(parted) mkpart primary ext4 0% 100%
(parted) p
Number  Start   End     Size    File system  Name     Flags
 1      1049kB  2000GB  2000GB  ntfs         primary
(parted) align-check optimal 1
1 aligned

(parted) q

# mkfs.ext4 /dev/sdz1
~~~

Exemple avec un périphérique boucle `/dev/loop0` :

~~~
# parted /dev/loop0

(parted) mkpart primary ext2 0% 95%
(parted) mkpart primary linux-swap 95% 96%                             
(parted) mkpart primary linux-swap 96% 97%                               
(parted) p                                                                
Model:  (file)
Disk /dev/loop0: 85.9GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  81.6GB  81.6GB  primary  ext4         boot
 2      81.6GB  82.5GB  859MB   primary
 3      82.5GB  83.3GB  859MB   primary

(parted) q
~~~

Exemple d'ajout d'une partition :

~~~
(parted) mkpart test ext4 209GB 210GB

(parted) p                                                                
 5      209GB   210GB   727MB   ext4         test
~~~

## Utilisation avancée

### Infos diverses

Avec la commande `print` :

~~~
(parted) print devices                                                    
/dev/nvme0n1 (256GB)
/dev/mapper/nvme0n1p4_crypt (7997MB)
/dev/mapper/nvme0n1p3_crypt (200GB)

(parted) print free
[...]
        210GB   256GB   46,1GB  Free Space
~~~

### Flag boot

Pour positionner le flag _boot_ sur une partition (ici la numéro 1) :

~~~
(parted) set 1 boot on
~~~

### Créer une partition de boot pour système UEFI

~~~
# parted /dev/sdX
[…]
(parted) mkpart "EFI system partition" fat32 1MiB 513MiB                  
(parted) set 1 esp on                                                     
(parted) p                                                                
[…]
Disk Flags: 

Number  Start   End    Size   File system  Name                  Flags
 1      1049kB  538MB  537MB  fat32        EFI system partition  boot, esp
~~~

### Changement de la taille d'une partition

Pour changer la taille d'une partition, on préfère utiliser cfdisk/fdisk/sfdisk et supprimer/recréer la partition,
on peut néanmoins le faire avec _parted_ :

~~~
(parted) p
 5      209GB   210GB   727MB   ext4         test

(parted) resizepart 5 211GB

(parted) p
 5      209GB   211GB   1726MB  ext4         test
~~~

On n'oubliera pas ensuite de faire un `resize2fs` pour prise en compte par le système de fichiers :

~~~
# resize2fs /dev/nvme0n1p5
resize2fs 1.43.4 (31-Jan-2017)
Le système de fichiers de /dev/nvme0n1p5 est monté sur /mnt ; le changement de taille doit être effectué en ligne
old_desc_blocks = 1, new_desc_blocks = 1
Le système de fichiers sur /dev/nvme0n1p5 a maintenant une taille de 421448 blocs (4k).
~~~

### Exemple de partitionnement

#### UEFI +GPT + `/home` séparé

On veut partitionner le volume `/dev/vda` de 40 Gio avec :

* une partition EFI de 500 Mio ;
* une partition de _swap_ de 500 Mio ;
* une partition pour la racine de 10 Gio ;
* une partition pour `/home` sur le reste du volume.

~~~
# parted /dev/vda
(parted) mklabel gpt
(parted) mkpart "EFI system partition" fat32 1MiB 501MiB
(parted) set 1 esp on
(parted) mkpart "swap partition" linux-swap 501MiB 1001MiB
(parted) mkpart "root partition" ext4 1001MiB 11001MiB
(parted) mkpart "home partition" ext4 11001MiB 100%
~~~

## Créer des volumes RAID

Après avoir créé la partition qui va accueillir le RAID, comme une partition conventionnelle, il suffit d'activer le flag RAID sur cette dernière.
Par exemple, pour la première partition d'un disque :

~~~
(parted) set 1 raid on
~~~

Puis initialiser le RAID avec [mdadm](HowtoRAIDLogiciel) comme d'habitude :

~~~
# mdadm --create /dev/mdX --chunk=64 --level=raid1 --raid-devices=2 /dev/sdX1 /dev/sdY1
~~~


## FAQ

### Quels sont les avantages d'une table de partitions GPT ?

Une table de partitions de type GPT (GUID Partition Table) a plusieurs avantages :

* jusqu'à 128 partitions primaires (plus besoin de partitions étendues/logiques)
* prise en charge des disques de taille supérieure à 1.8 To
* la table des partitions est écrite au début à la fin du disque


### Quand doit-on utiliser GPT ?

Une table de partitions de type GPT est obligatoire pour les disques de plus de 1.8 To, mais cela peut également être utilisé pour les disques de plus petite taille sous réserve que cela soit supporté par le BIOS/UEFI.
