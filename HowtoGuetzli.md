---
categories: web
title: Howto Guetzli
...

* Documentation: <https://github.com/google/guetzli>

[Guetzli](https://github.com/google/guetzli) est un outil pour compresser des images de façon très efficace sans altérer la qualité.

Il n'est disponible que dans les dépôt de Debian Buster, on peut le backporter manuellement pour Stretch ainsi :

/etc/apt/sources.list.d/buster.list

~~~
deb http://mirror.evolix.org/debian buster main
~~~

/etc/apt/preferences.d/guetzli

~~~
Package: *
Pin: release n=buster
Pin-Priority: 50

Package: guetzli
Pin:  release n=buster
Pin-Priority: 999
~~~

En Jessie il est possible de l'installer en « one-shot » :

~~~
# wget https://github.com/google/guetzli/releases/download/v1.0.1/guetzli_linux_x86-64
# install -m 755 guetzli_linux_x86-64 /usr/local/bin/guetzli
~~~