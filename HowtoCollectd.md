---
categories: monitoring
title: Howto Collectd
...

* Documentation : <https://collectd.org/documentation.shtml>

[Collectd](https://collectd.org/) est un démon collectant des métriques systèmes et applications à intervalles réguliers. Il peut ensuite stocker ces métriques de différentes manières. Nous l'utilisons en combinaison avec [InfluxDB](HowtoInfluxDB) et [Grafana](HowtoGrafana).

Nous préférons collectd à [telegraf](https://www.influxdata.com/time-series-platform/telegraf/), ce dernier n'ayant pas de paquet disponible sous OpenBSD lorsque nous l'avions mis en place. Depuis, un paquet est disponible pour telegraf sous OpenBSD, mais nous restons sous collectd que nous connaissons et utilisons déjà.

## Installation

### Debian

~~~
# apt install collectd

$ apt list collectd
Listing... Done
collectd/stable,now 5.12.0-14 amd64 [installed]

$ systemctl status collectd
● collectd.service - Statistics collection and monitoring daemon
     Loaded: loaded (/lib/systemd/system/collectd.service; enabled; preset: enabled)
     Active: active (running) since Tue 2024-04-30 15:31:54 CEST; 14s ago
       Docs: man:collectd(1)
             man:collectd.conf(5)
             https://collectd.org
    Process: 12843 ExecStartPre=/usr/sbin/collectd -t (code=exited, status=0/SUCCESS)
   Main PID: 12844 (collectd)
      Tasks: 12 (limit: 2352)
     Memory: 35.9M
        CPU: 140ms
     CGroup: /system.slice/collectd.service
             └─12844 /usr/sbin/collectd
~~~

### OpenBSD

~~~
# pkg_add collectd

$ pkg_info -Q collectd
collectd-5.12.0p0 (installed)
~~~

## Configuration

Collectd fonctionne à l'aide de plugins à activer selon ses besoins.

~~~
Interval 60
Timeout 2
Retries 3

LoadPlugin foo

<Plugin foo>
  Option value
</Plugin>
~~~

### Plugin network

Le plugin [network](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#plugin_network) est indispensable et permet de spécifier vers où envoyer les métriques collectées.

~~~
LoadPlugin network

<Plugin network>
  Server "127.0.0.1" "25826"
</Plugin>
~~~

Si l'authentification est configurée du côté de la base de données (par exemple sur [InfluxDB](/HowtoInfluxDB#authentification-1)), on peut la configurer ainsi :

~~~
<Plugin network>
  <Server "127.0.0.1" "25826">
    SecurityLevel "sign"
    Username "username"
    Password "password"
  </Server>
</Plugin>
~~~

Attention à ce que le fichier de configuration ne soit lisible que par `root`, vu que le mot de passe est en clair.

### Plugin syslog

Le plugin [syslog](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#plugin_syslog) permet d'écrire des logs dans syslog, en y choisissant le log-level du démon. S'il n'est pas activé, aucun log ne sera présent dans syslog.

~~~
LoadPlugin syslog

<Plugin syslog>
  LogLevel warning
</Plugin>
~~~

### Plugin logfile

Le plugin [logfile](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#plugin_logfile) permet d'écrire des logs dans un fichier texte défini, en y choisissant le log-level du démon.

~~~
LoadPlugin logfile

<Plugin "logfile">
  LogLevel "warning"
  File "/var/log/collectd.log"
  Timestamp true
  PrintSeverity true
</Plugin>
~~~

### Plugin exec

Le plugin [exec](https://collectd.org/documentation/manpages/collectd-exec.5.shtml) est intéressant puisqu'il permet d'exécuter n'importe quel script afin de récupérer toutes sortes de métriques non prises en charge par un autre plugin.

~~~
<LoadPlugin exec>
  Interval 300
</LoadPlugin>

<Plugin exec>
  Exec "user" "/path/to/script" "arg0" "arg1"
</Plugin>
~~~

La sortie du script doit respecter un format particulier :

~~~
PUTVAL host/plugin-instance/type-instance date:value
~~~

Avec :

* `host` : le nom de la machine associée à la mesure ;
* `plugin` : le nom de la mesure ;
* `instance` (facultatif) : un tag pouvant caractériser la mesure ;
* `type` : le type de mesure, parmi ceux présents dans le fichier `/usr/share/collectd/types.db` (ou `/usr/local/share/collectd/types.db` sous OpenBSD), et définissant le format de `value` ;
* `instance` (facultatif) : un tag pouvant caractériser la mesure ;
* `date` : la date à laquelle la mesure est récupérée, utiliser `N` pour remplacer par la date actuelle ;
* `value` : la valeur à récupérer.

Par exemple, pour récupérer le nombre de préfixes BGP partagés par un voisin :

~~~
PUTVAL $(hostname)/bgp_prefixes/count-${PEER} N:${CURRENT_PREFIXES}
~~~

### Plugin snmp

Le plugin [snmp](https://collectd.org/documentation/manpages/collectd-snmp.5.shtml) permet de faire des requêtes SNMP à des machines spécifiques.
Un block `Data` définit l'OID et sous quel format stocker les valeurs récupérées, et un block `Host` définit quelles machines interroger ainsi que la configuration SNMP.

Sous debian, le paquet `snmp` est nécessaire.

~~~
# apt install snmp
~~~

Exemple pour récupérer : 

* Pour un switch (exemple avec un Cisco Nexus) : le débit, le nombre de paquets unicast/multicast/broadcast, le nombre d'erreurs, les données des capteurs des SFP, la charge CPU, la RAM utilisée, l'uptime

~~~
LoadPlugin snmp

<Plugin snmp>
# Switch
  <Data "ifmib_if_octets64">
    Type "if_octets"
    Table true
    TypeInstanceOID "1.3.6.1.2.1.31.1.1.1.18"
    Values "1.3.6.1.2.1.31.1.1.1.6" "1.3.6.1.2.1.31.1.1.1.10"
  </Data>
  <Data "ifmib_if_packets64">
    Type "if_packets"
    Table true
    TypeInstanceOID "1.3.6.1.2.1.31.1.1.1.18"
    Values "1.3.6.1.2.1.31.1.1.1.7" "1.3.6.1.2.1.31.1.1.1.11"
  </Data>
  <Data "ifmib_if_multicast_packets64">
    Type "if_packets"
    Table true
    TypeInstanceOID "1.3.6.1.2.1.31.1.1.1.18"
    TypeInstancePrefix "multicast-"
    Values "1.3.6.1.2.1.31.1.1.1.8" "1.3.6.1.2.1.31.1.1.1.12"
  </Data>
  <Data "ifmib_if_broadcast_packets64">
    Type "if_packets"
    Table true
    TypeInstanceOID "1.3.6.1.2.1.31.1.1.1.18"
    TypeInstancePrefix "broadcast-"
    Values "1.3.6.1.2.1.31.1.1.1.9" "1.3.6.1.2.1.31.1.1.1.13"
  </Data>
  <Data "ifmib_if_errors">
    Type "if_errors"
    Table true
    TypeInstanceOID "1.3.6.1.2.1.31.1.1.1.1"
    Values "1.3.6.1.2.1.2.2.1.14" "1.3.6.1.2.1.2.2.1.20"
  </Data>
  <Data "ifmib_if_dropped">
    Type "if_dropped"
    Table true
    TypeInstanceOID "1.3.6.1.2.1.31.1.1.1.1"
    Values "1.3.6.1.2.1.2.2.1.13" "1.3.6.1.2.1.2.2.1.19"
  </Data>
  # Capteurs des SFP fibre pour Cisco Nexus
  <Data "sfp_sensors">
    Type "gauge"
    Scale 0.001
    Table true
    TypeInstanceOID ".1.3.6.1.2.1.47.1.1.1.1.2"
    Values ".1.3.6.1.4.1.9.9.91.1.1.1.1.4"
  </Data>
  # https://www.cisco.com/c/en/us/support/docs/ip/simple-network-management-protocol-snmp/15215-collect-cpu-util-snmp.html
  <Data "cpu_5s">
    Type "cpufreq"
    TypeInstance "cpu_5s"
    Values "1.3.6.1.4.1.9.9.109.1.1.1.1.6.1"
  </Data>
  <Data "cpu_1m">
    Type "cpufreq"
    TypeInstance "cpu_1m"
    Values "1.3.6.1.4.1.9.9.109.1.1.1.1.7.1"
  </Data>
  <Data "cpu_5m">
    Type "cpufreq"
    TypeInstance "cpu_5m"
    Values "1.3.6.1.4.1.9.9.109.1.1.1.1.8.1"
  </Data>
  # https://community.ipswitch.com/s/article/What-SNMP-OID-does-WhatsUp-Use-for-Cisco-Memory-and-CPU
  <Data "memory_used">
    Type "memory"
    Table true
    TypeInstanceOID "1.3.6.1.4.1.9.9.48.1.1.1.2"
    TypeInstancePrefix "Used - "
    Values "1.3.6.1.4.1.9.9.48.1.1.1.5"
  </Data>
  <Data "memory_free">
    Type "memory"
    Table true
    TypeInstanceOID "1.3.6.1.4.1.9.9.48.1.1.1.2"
    Type InstancePrefix "Free - "
    Values "1.3.6.1.4.1.9.9.48.1.1.1.6"
  </Data>
  # https://community.cisco.com/t5/switching/snmp-uptime/td-p/939932
  <Data "uptime">
    Type "uptime"
    TypeInstance "uptime"
    Values ".1.3.6.1.6.3.10.2.1.3.0"
  </Data>

  <Host "Switch">
    Address "192.0.2.1"
    Version 2
    Community "public"
    Collect "ifmib_if_octets64" "ifmib_if_packets64" "ifmib_if_multicast_packets64" "ifmib_if_broadcast_packets64" "ifmib_if_errors" "ifmib_if_dropped" "sfp_sensors" "cpu_5s" "cpu_1m" "cpu_5m" "memory_used" "memory_free" "uptime"
  </Host>
</Plugin>
~~~

Si on veut identifier chaque interfaces avec son nom (par exemple Gi0/1) plutôt que sa description (définie avec la commande "description" dans la configuration de l'interface), il faut modifier les lignes de configuration `Instance "1.3.6.1.2.1.31.1.1.1.18"` par `Instance "1.3.6.1.2.1.31.1.1.1.1"` pour les blocs `ifmib_if_octets64`, `ifmib_if_packets64`, `ifmib_if_errors`, et `ifmib_if_dropped`.

Dans ce cas, au lieu que seules les interfaces ayant une description remontent des données, toutes les interfaces en remonteront (à 0 si rien n'est branché).

* Pour un PDU de marque APC : la charge

~~~
LoadPlugin snmp

<Plugin snmp>
# APC
  <Data "apc_current_load">
    Type "current"
    TypeInstance "apc_current_load"
    Scale 0.1
    Values ".1.3.6.1.4.1.318.1.1.12.2.3.1.1.2.1"
  </Data>
  <Data "apc_current_low">
    Type "current"
    TypeInstance "apc_current_low"
    Values ".1.3.6.1.4.1.318.1.1.12.2.2.1.1.2.1"
  </Data>
  <Data "apc_current_near">
    Type "current"
    TypeInstance "apc_current_near"
    Values ".1.3.6.1.4.1.318.1.1.12.2.2.1.1.3.1"
  </Data>
  <Data "apc_current_overload">
    Type "current"
    TypeInstance "apc_current_overload"
    Values ".1.3.6.1.4.1.318.1.1.12.2.2.1.1.4.1"
  </Data>
  # Pour les APCs avec compteurs par prises
  <Data "apc_outlet_current">
    Type "current"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.318.1.1.26.9.4.3.1.1"
    Scale 0.1
    Values ".1.3.6.1.4.1.318.1.1.26.9.4.3.1.6"
  </Data>
  <Data "apc_outlet_watt">
    Type "energy"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.318.1.1.26.9.4.3.1.1"
    Values ".1.3.6.1.4.1.318.1.1.26.9.4.3.1.7"
  </Data>
  <Data "apc_outlet_total_energy">
    Type "energy_wh"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.318.1.1.26.9.4.3.1.1"
    Scale 0.1
    Values ".1.3.6.1.4.1.318.1.1.26.9.4.3.1.11"
  </Data>

  <Host "APC classique">
    Address "192.0.2.2"
    Version 2
    Community "public"
    Collect "apc_current_load" "apc_current_low" "apc_current_near" "apc_current_overload"
    Interval 1800
  </Host>
  <Host "APC avec compteurs par prises">
    Address "192.0.2.2"
    Version 2
    Community "public"
    Collect "apc_current_load" "apc_current_low" "apc_current_near" "apc_current_overload" "apc_outlet_current" "apc_outlet_watt" "apc_outlet_total_energy"
    Interval 1800
  </Host>
</Plugin>
~~~

* Pour un ATS de marque APC : la fréquence, la tension, l'intensité, la source préférée et le statut

~~~
LoadPlugin snmp

<Plugin snmp>
# ATS
  <Data "ats_input_freq_a">
    Type "frequency"
    TypeInstance "ats_input_freq_a"
    Values ".1.3.6.1.4.1.318.1.1.8.5.3.2.1.4.1"
  </Data>
  <Data "ats_input_volt_a">
    Type "voltage"
    TypeInstance "ats_input_volt_a"
    Values ".1.3.6.1.4.1.318.1.1.8.5.3.3.1.3.1.1.1"
  </Data>
  <Data "ats_input_freq_b">
    Type "frequency"
    TypeInstance "ats_input_freq_b"
    Values ".1.3.6.1.4.1.318.1.1.8.5.3.2.1.4.2"
  </Data>
  <Data "ats_input_volt_b">
    Type "voltage"
    TypeInstance "ats_input_volt_b"
    Values ".1.3.6.1.4.1.318.1.1.8.5.3.3.1.3.2.1.1"
  </Data>

  <Data "ats_output_volt">
    Type "voltage"
    TypeInstance "ats_output_volt"
    Values ".1.3.6.1.4.1.318.1.1.8.5.4.3.1.3.1.1.1"
  </Data>
  <Data "ats_output_current">
    Type "current"
    TypeInstance "ats_output_current"
    Scale 0.1
    Values ".1.3.6.1.4.1.318.1.1.8.5.4.3.1.4.1.1.1"
  </Data>
  <Data "ats_output_freq">
    Type "frequency"
    TypeInstance "ats_output_freq"
    Values ".1.3.6.1.4.1.318.1.1.8.5.4.2.1.4.1"
  </Data>

  <Data "ats_source_preferred">
    Type "count"
    TypeInstance "ats_source_preferred"
    Values ".1.3.6.1.4.1.318.1.1.8.4.2.0"
  </Data>
  <Data "ats_source_status">
    Type "count"
    TypeInstance "ats_source_status"
    Values ".1.3.6.1.4.1.318.1.1.8.5.1.2.0"
  </Data>

  <Data "ats_status_redundancy">
    Type "count"
    TypeInstance "ats_status_redundancy"
    Values ".1.3.6.1.4.1.318.1.1.8.5.1.3.0"
  </Data>
  <Data "ats_status_current_state">
    Type "current"
    TypeInstance "ats_status_current_state"
    Values ".1.3.6.1.4.1.318.1.1.8.5.1.4.0"
  </Data>
  <Data "ats_status_current_limit">
    Type "current"
    TypeInstance "ats_status_current_limit"
    Values ".1.3.6.1.4.1.318.1.1.8.4.6.0"
  </Data>

  <Host "ATS">
    Address "192.0.2.3"
    Version 2
    Community "public"
    Collect "ats_input_freq_a" "ats_input_volt_a" "ats_input_freq_b" "ats_input_volt_b" "ats_output_volt" "ats_output_current" "ats_output_freq" "ats_source_preferred" "ats_source_status" "ats_status_redundancy" "ats_status_current_state" "ats_status_current_limit"
    Interval 1800
  </Host>
</Plugin>
~~~

* Pour un PDU de marque Eaton : l'uptime, la charge (tension, intensité et puissance ; globale, par banques et par prises), l'énergie consommée (globale et par prises), le statut on/off des banques et des prises, les mesures des sondes de température et d'humidité

~~~
LoadPlugin snmp

<Plugin snmp>
# PDU Eaton
## Uptime
  <Data "eaton_uptime">
    Type "uptime"
    TypeInstance "eaton_uptime"
    Scale 100
    Values ".1.3.6.1.6.3.10.2.1.3.0"
  </Data>
## Entrée du PDU
  <Data "eaton_inputVoltage"> # Tension d'entrée en mV
    Type "voltage"
    TypeInstance "eaton_inputVoltage"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.3.2.1.3.0.1.1"
  </Data>
  <Data "eaton_inputCurrentCapacity"> # Capacité d'intensité en mA
    Type "current"
    TypeInstance "eaton_inputCurrentCapacity"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.3.3.1.3.0.1.1"
  </Data>
  <Data "eaton_inputCurrent"> # Intensité d'entrée en mA
    Type "current"
    TypeInstance "eaton_inputCurrent"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.3.3.1.4.0.1.1"
  </Data>
  <Data "eaton_inputCurrentPercentLoad"> # Pourcentage d'intensité utilisée par rapport à la capacité
    Type "percent"
    TypeInstance "eaton_inputCurrentPercentLoad"
    Values ".1.3.6.1.4.1.534.6.6.7.3.3.1.11.0.1.1"
  </Data>
  <Data "eaton_inputVA"> # Puissance d'entrée en VA
    Type "power"
    TypeInstance "eaton_inputVA"
    Values ".1.3.6.1.4.1.534.6.6.7.3.4.1.3.0.1.1"
  </Data>
  <Data "eaton_inputWatts"> # Puissance d'entrée en W
    Type "power"
    TypeInstance "eaton_inputWatts"
    Values ".1.3.6.1.4.1.534.6.6.7.3.4.1.4.0.1.1"
  </Data>
  <Data "eaton_inputWh"> # Énergie consommée en entrée en Wh depuis le dernier reset
    Type "energy_wh"
    TypeInstance "eaton_inputWh"
    Values ".1.3.6.1.4.1.534.6.6.7.3.4.1.5.0.1.1"
  </Data>
  <Data "eaton_inputWhTimer"> # Date (UnixTimeStamp) du dernier reset du compteur précédent
    Type "timestamp"
    TypeInstance "eaton_inputWhTimer"
    Values ".1.3.6.1.4.1.534.6.6.7.3.4.1.6.0.1.1"
  </Data>
## Banques
  <Data "eaton_groupBreakerStatus"> # Statut des disjoncteur des banques (1 = On, 2 = Off)
    Type "count"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.5.1.1.3"
    Values ".1.3.6.1.4.1.534.6.6.7.5.1.1.5"
  </Data>
  <Data "eaton_groupVoltage"> # Tension en mV pour chaque banque
    Type "voltage"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.5.1.1.3"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.5.3.1.3"
  </Data>
  <Data "eaton_groupCurrentCapacity"> # Capacité d'intensité en mA pour chaque banque
    Type "current"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.5.1.1.3"
    TypeInstancePrefix "Capacity-"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.5.4.1.2"
  </Data>
  <Data "eaton_groupCurrent"> # Intensité en mA utilisée par chaque banque
    Type "current"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.5.1.1.3"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.5.4.1.3"
  </Data>
  <Data "eaton_groupCurrentPercentLoad"> # Pourcentage d'intensité utilisée par chaque banque par rapport à leur capacité
    Type "percent"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.5.1.1.3"
    Values ".1.3.6.1.4.1.534.6.6.7.5.4.1.10"
  </Data>
## Prises
  <Data "eaton_outletCurrentCapacity"> # Capacité d'intensité en mA pour chaque prise
    Type "current"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    TypeInstancePrefix "Capacity-"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.6.4.1.2"
  </Data>
  <Data "eaton_outletCurrent"> # Intensité en mA utilisée par chaque prise
    Type "current"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    Scale 0.001
    Values ".1.3.6.1.4.1.534.6.6.7.6.4.1.3"
  </Data>
  <Data "eaton_outletVA"> # Puissance consommée par chaque prise en VA
    Type "power"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    TypeInstancePrefix "VA-"
    Values ".1.3.6.1.4.1.534.6.6.7.6.5.1.2"
  </Data>
  <Data "eaton_outletWatts"> # Puissance consommée par chaque prise en W
    Type "power"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    Values ".1.3.6.1.4.1.534.6.6.7.6.5.1.3"
  </Data>
  <Data "eaton_outletWh"> # Énergie consommée par chaque prise en Wh depuis le dernier reset
    Type "energy_wh"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    Values ".1.3.6.1.4.1.534.6.6.7.6.5.1.4"
  </Data>
  <Data "eaton_outletWhTimer"> # Date (UnixTimeStamp) du dernier reset du compteur précédent
    Type "timestamp"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    Values ".1.3.6.1.4.1.534.6.6.7.6.5.1.5"
  </Data>
  <Data "eaton_outletControlStatus"> # Statut de chaque prise (0 = Off, 1 = On, 2 = pendingOff, 3 = pendingOn)
    Type "count"
    Table true
    TypeInstanceOID ".1.3.6.1.4.1.534.6.6.7.6.1.1.3"
    Values ".1.3.6.1.4.1.534.6.6.7.6.6.1.2"
  </Data>
## Sondes
  <Data "eaton_sensorCount"> # Nombre de sondes branchées sur le PDU
    Type "count"
    TypeInstance "eaton_sensorCount"
    Values ".1.3.6.1.4.1.534.6.8.1.1.1.0"
  </Data>
## Température
### Sonde 1
  <Data "eaton_temperatureValue_1"> # Température mesurée par chaque sonde en 1/10 de °C
    Type "temperature"
    TypeInstance "eaton_temperatureValue_1"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.2.3.1.3.1.1"
  </Data>
  <Data "eaton_temperatureMinValue_1"> # Température la plus basse atteinte pour chaque sonde en 1/10 de °C
    Type "temperature"
    TypeInstance "eaton_temperatureMinValue_1"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.1.1.1"
  </Data>
  <Data "eaton_temperatureMinValueSince_1"> # Date (UnixTimeStamp) à laquelle la température la plus basse a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_temperatureMinValueSince_1"
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.2.1.1"
  </Data>
  <Data "eaton_temperatureMaxValue_1"> # Température la plus haute atteinte pour chaque sonde en 1/10 de °C
    Type "temperature"
    TypeInstance "eaton_temperatureMaxValue_1"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.3.1.1"
  </Data>
  <Data "eaton_temperatureMaxValueSince_1"> # Date (UnixTimeStamp) à laquelle la température la plus haute a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_temperatureMaxValueSince_1"
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.4.1.1"
  </Data>
### Sonde 2
  <Data "eaton_temperatureValue_2"> # Température mesurée par chaque sonde en 1/10 de °C
    Type "temperature"
    TypeInstance "eaton_temperatureValue_2"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.2.3.1.3.2.1"
  </Data>
  <Data "eaton_temperatureMinValue_2"> # Température la plus basse atteinte pour chaque sonde en 1/10 de °C
    Type "temperature"
    TypeInstance "eaton_temperatureMinValue_2"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.1.2.1"
  </Data>
  <Data "eaton_temperatureMinValueSince_2"> # Date (UnixTimeStamp) à laquelle la température la plus basse a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_temperatureMinValueSince_2"
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.2.2.1"
  </Data>
  <Data "eaton_temperatureMaxValue_2"> # Température la plus haute atteinte pour chaque sonde en 1/10 de °C
    Type "temperature"
    TypeInstance "eaton_temperatureMaxValue_2"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.3.2.1"
  </Data>
  <Data "eaton_temperatureMaxValueSince_2"> # Date (UnixTimeStamp) à laquelle la température la plus haute a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_temperatureMaxValueSince_2"
    Values ".1.3.6.1.4.1.534.6.8.1.2.4.1.4.2.1"
  </Data>
## Humidité
### Sonde 1
  <Data "eaton_humidityValue_1"> # Humidité mesurée par chaque sonde en 1/10 de %
    Type "humidity"
    TypeInstance "eaton_humidityValue_1"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.3.3.1.3.1.1"
  </Data>
  <Data "eaton_humidityMinValue_1"> # Humidité la plus basse atteinte pour chaque sonde en 1/10 de %
    Type "humidity"
    TypeInstance "eaton_humidityMinValue_1"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.1.1.1"
  </Data>
  <Data "eaton_humidityMinValueSince_1"> # Date (UnixTimeStamp) à laquelle l'humidité la plus basse a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_humidityMinValueSince_1"
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.2.1.1"
  </Data>
  <Data "eaton_humidityMaxValue_1"> # Humidité la plus haute atteinte pour chaque sonde en 1/10 de %
    Type "humidity"
    TypeInstance "eaton_humidityMaxValue_1"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.3.1.1"
  </Data>
  <Data "eaton_humidityMaxValueSince_1"> # Date (UnixTimeStamp) à laquelle l'humidité la plus haute a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_humidityMaxValueSince_1"
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.4.1.1"
  </Data>
### Sonde 2
  <Data "eaton_humidityValue_2"> # Humidité mesurée par chaque sonde en 1/10 de %
    Type "humidity"
    TypeInstance "eaton_humidityValue_2"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.3.3.1.3.2.1"
  </Data>
  <Data "eaton_humidityMinValue_2"> # Humidité la plus basse atteinte pour chaque sonde en 1/10 de %
    Type "humidity"
    TypeInstance "eaton_humidityMinValue_2"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.1.2.1"
  </Data>
  <Data "eaton_humidityMinValueSince_2"> # Date (UnixTimeStamp) à laquelle l'humidité la plus basse a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_humidityMinValueSince_2"
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.2.2.1"
  </Data>
  <Data "eaton_humidityMaxValue_2"> # Humidité la plus haute atteinte pour chaque sonde en 1/10 de %
    Type "humidity"
    TypeInstance "eaton_humidityMaxValue_2"
    Scale 0.1
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.3.2.1"
  </Data>
  <Data "eaton_humidityMaxValueSince_2"> # Date (UnixTimeStamp) à laquelle l'humidité la plus haute a été atteinte pour chaque sonde
    Type "count"
    TypeInstance "eaton_humidityMaxValueSince_2"
    Values ".1.3.6.1.4.1.534.6.8.1.3.4.1.4.2.1"
  </Data>

  <Host "PDU Eaton">
    Address "192.0.2.2"
    Version 2
    Community "public"
    Collect "eaton_uptime" "eaton_inputVoltage" "eaton_inputCurrentCapacity" "eaton_inputCurrent" "eaton_inputCurrentPercentLoad" "eaton_inputVA" "eaton_inputWatts" "eaton_inputWh" "eaton_inputWhTimer" "eaton_groupBreakerStatus" "eaton_groupVoltage" "eaton_groupCurrentCapacity" "eaton_groupCurrent" "eaton_groupCurrentPercentLoad" "eaton_outletCurrentCapacity" "eaton_outletCurrent" "eaton_outletVA" "eaton_outletWatts" "eaton_outletWh" "eaton_outletWhTimer" "eaton_outletControlStatus" "eaton_sensorCount" "eaton_temperatureValue_1" "eaton_temperatureMinValue_1" "eaton_temperatureMinValueSince_1" "eaton_temperatureMaxValue_1" "eaton_temperatureMaxValueSince_1" "eaton_temperatureValue_2" "eaton_temperatureMinValue_2" "eaton_temperatureMinValueSince_2" "eaton_temperatureMaxValue_2" "eaton_temperatureMaxValueSince_2" "eaton_humidityValue_1" "eaton_humidityMinValue_1" "eaton_humidityMinValueSince_1" "eaton_humidityMaxValue_1" "eaton_humidityMaxValueSince_1" "eaton_humidityValue_2" "eaton_humidityMinValue_2" "eaton_humidityMinValueSince_2" "eaton_humidityMaxValue_2" "eaton_humidityMaxValueSince_2"
    Interval 1800
  </Host>
</Plugin>
~~~