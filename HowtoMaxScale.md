---
categories: databases
title: How to MaxScale: installation et utilisation
---

* Documentation : <https://mariadb.com/kb/en/maxscale/>

MariaDB MaxScale est un serveur proxy pour MySQL et MariaDB.

## Installation

Pour installer MariaDB MaxScale il faut soit ajouter le [dépot officiel](https://mariadb.com/kb/en/mariadb-package-repository-setup-and-usage/) soit [télécharger le paquet depuis le site officiel](https://mariadb.com/downloads/#mariadb_platform-mariadb_maxscale).

## Configuration

MariaDB MaxScale est configuré dans le fichier ini `/etc/maxscale.cnf`, ce fichier peut être séparé en 5 grandes parties: la configuration globale de MaxScale, la liste des serveurs que MaxScale surveille, la configuration du (ou des) service surveillant ces serveurs, la configuration des services par lesquelles MaxScale route les communications entre les applications clientes et les serveurs MySQL, et la manière par laquelle les clients se connectent à MaxScale.

La configuration globale de MaxScale se trouve dans un bloc `maxscale` et contient la configuration du nombre de threads utilisés par MaxScale, ses répertoires de stockages, les accès Rest-API (principalement pour l'administration à distance), …

Puisque MaxScale sert d'intermédiaire entre les applications clientes MySQL et des serveurs MySQL, il faut que le serveur MaxScale sache comment communiquer avec les serveurs MySQL, pour cela chaque serveur est configuré dans un bloc ayant la clé `type` à la valeur `server` et un nom qui sera le nom du serveur pour MaxScale, les principales clés pour ce type de bloc sont :

* `address` ayant pour valeur l'adresse du serveur MySQL,
* `port` ayant pour valeur le port sur lequel le serveur MySQL attend des connexions,
* `socket` qui correspond à la socket du serveur MySQL si celui-ci est un serveur local et
* `protocol` qui est le protocole que le serveur MaxScale va utiliser pour parler avec le serveur MySQL, les valeurs de cette clé sont `MariaDBBackend` pour les serveurs MySQL et `MariaDBClient` si le "serveur" est un service interne de MaxScale.

Exemples de configurations de serveurs

~~~{.ini}
[dbserver1]
type = server
address = 192.0.2.100
port = 3306
protocol = MariaDBBackend
~~~

~~~{.ini}
[dbserver2]
type = server
address = db2.example.com
port = 3306
protocol = MariaDBBackend
~~~

Afin de pouvoir surveiller l'état des serveurs MySQL, MaxScale a besoin d'avoir un moniteur configuré, cela se fait en configurant un bloc ayant la clé `type` à la valeur `monitor`, les autres clés principales sont:

* `module` définissant le type de moniteur (`mariadbmon` pour un cluster MySQL simple et `galeramon` pour un cluster Galera)
* `servers` qui contient les noms des serveurs surveillés par ce moniteur (séparés par des virgules)
* `user` configurant l'utilisateur MySQL utiliser pour surveiller le cluster
* `password` contenant le mot de passe de cet utilisateur MySQL.

Pour l'utilisateur, si vous utilisez le module `mariadbmon`, vous devez donner les permissions `REPLICATION CLIENT on *.*` jusqu'à MariaDB 10.3 (inclus), ou `SUPER, SLAVE MONITOR ON *.*` à partir de MariaDB 10.5. Si vous utilisez le module `galeramon`, aucun droit particulier n'est nécessaire.

Exemple :

~~~{.sql}
GRANT SUPER, SLAVE MONITOR ON *.* TO `maxscale_monitor`@`10.10.91.0/255.255.255.0` IDENTIFIED BY '*********';
~~~

D'autres clés de configurations sont disponibles pour chaques type de moniteur permettant de contrôler comment le moniteur interagi avec le cluster qu'il surveille.

Exemple de configuration de moniteur:

~~~{.ini}
[MariaDB-Monitor]
type = monitor
module = mariadbmon
servers = dbserver1, dbserver2
user = maxscale_monitor
password = PASSWORD1
~~~

MaxScale a ensuite besoin de savoir comment répartir la charge entre les serveurs, pour cela il faut configurer un bloc ayant la clé `type` à la valeur `service`, le nom du bloc étant le nom que MaxScale donnera à ce service. Les clés principales de ce type de bloc sont :

* `router` définissant le type de routage utiliser par MaxScale, les types principaux étant :
  * `readconnroute` qui se contente d'équilibrer les connexions entre les serveurs sans se préoccuper du type de requêtes, son utilisation principale étant de fournir un port pour les opérations d'écritures et un port pour les connexions de lectures.
  * `readwritesplit` qui sépare les opérations de lectures et d'écritures de manière automatique.
* `servers` définissant la liste des serveurs sur lesquelles faire du routage
* `cluster` (incompatible avec `servers`) le nom du moniteur surveillant les serveurs sur lesquels faire du routage
* `user` l'utilisateur MySQL que MaxScale utilise pour récupérer la liste des utilisateurs, table, bases de données…
* `password` mot de passe de cet utilisateur MySQL.

D'autres clés de configurations sont disponibles pour chaque type de routage.

Exemple de configuration de service:

~~~{.ini}
[MariaDB-Service]
type = service
router = readwritesplit
servers = dbserver1, dbserver2
user = maxscale_service
password = PASSWORD2
~~~

> Note: Il est possible de choisir un nom quelconque pour le bloc, il s'agira du nom que MaxScale donnera au moniteur.

L'utilisateur utilisé par le service a besoin des droits suivants:

~~~{.sql}
GRANT SHOW DATABASES ON *.* TO 'maxscale_service'@'%' IDENTIFIED BY '********';
GRANT SELECT ON mysql.user TO 'maxscale_service'@'%';
GRANT SELECT ON mysql.db TO 'maxscale_service'@'%';
GRANT SELECT ON mysql.tables_priv TO 'maxscale_service'@'%';
GRANT SELECT ON mysql.columns_priv TO 'maxscale_service'@'%';
GRANT SELECT ON mysql.proxies_priv TO 'maxscale_service'@'%';
GRANT SELECT ON mysql.roles_mapping TO 'maxscale_service'@'%';
~~~

Enfin, MaxScale doit être configuré pour attendre des connexions MySQL et les passer à un service, pour ce faire il faut configurer un bloc ayant la clé `type` à la valeur `listener`, la clé `service` au nom du service vers lequel passer la connection, la clé `protocol` à `MariaDBClient` et la clé `port` au port utilisé.

Exemple de configuration de listener:

~~~{.ini}
[Splitter-Listener]
type = listener
service = MariaDB-Service
protocol = MariaDBClient
port = 3306
~~~

> ⚠ : Les utilisateurs ont besoins de pouvoir se connecter aux serveurs MySQL depuis le serveur MaxScale, c'est à dire qu'il existe un utilisateur mysql `USER@<ip de MaxScale>` ou `USER@'%'`.

### Routers

MariaDB MaxScale à plusieurs types de routages possibles, les principaux sont:

* Readconnroute qui permet de faire du routage par connexions, avec toutes les requêtes allant vers un même serveur de backend, un des principaux cas d'usage de ce type de routage est une version légère de routage avec deux ports différents pour les commandes d'écritures et de lecture.
* Readwritesplit qui permet une séparation automatique des requêtes de lecture et d'écriture, ce routeur est plus lourd en ressources pour le serveur MaxScale mais est compatible avec la plupart des clients mysql sans modification majeure de configuration.
* Binlogrouter qui permet à MaxScale de récupérer les binlog d'un serveur MySQL ou MariaDB et d'être utilisé comme proxy au niveau de la réplication (MaxScale apparaissant comme étant le serveur maître).
* Avrorouter qui permet de transformer les binlog récupérés par un Binlogrouter en fichier Avro pour utilisation avec le protocol CDC (eg. Kafka)

Il y a d'autres router mais ils sont soit en béta soit très instables si non configurés avec une bonne connaissance du contenu des bases de données.

### Filtres

MaxScale peut faire passer les statements SQL par des filtres avant de les envoyer au routeur et donc aux serveurs MySQL, ces filtres permettent de bloquer certaines requêtes, de conseiller au routeur d'envoyer des requêtes sur un certain serveur, ou de modifier le retour des serveurs.

* `dbfwfilter`: Permet de bloquer des requêtes de manière plus souple que le système de GRANT traditionnel. Le filtre est considéré comme une solution par le meilleur effort et a pour but principal de protéger contre les erreurs et non les attaques.
* `insertstream`: (⚠ : Expérimental) Permet de convertir des `INSERT` en masse en une seule commande `LOAD DATA LOCAL INFILE` afin de réduire l'utilisation du trafic et augmenter la vitesse des insertions de masse.
* `cache`: (⚠ : N'est pas au courant des droits utilisateurs) Permet de cacher le retour d'un `SELECT` tant qu'il n'y a pas eu d'écriture afin d'augmenter la vitesse de retour d'un `SELECT` identique.
* `tee`: Permet de copier une requête et de l'envoyée vers un autre service.
* `throttle`: Permet de limiter la fréquence des requêtes.
* `binlogfilter`: Permet de faire de la réplication partielle avec `binlogrouter`.

### Validation

Il est possible de valider un fichier de configuration (où qu'il soit) avec la commande suivante :

~~~
# sudo -u maxscale maxscale --config-check --config /path/to/config.cnf
~~~

Il peut être nécessaire de spécifier l'utilisateur qui doit jouer la commande, avec l'option `--user=<USER>`.

## Utilisation

Pour utiliser MariaDB MaxScale il suffit de l'utiliser comme un serveur MySQL normal:

~~~
$ mysql --host <adresse du serveur maxscale> --user=USER --password base_de_donnée
~~~
## Configuration avancée

### Limiter le retard visible

Cela se fait au niveau du routeur et n'est possible que pour le routeur `Readwritesplit`, `readconnroute` étant trop léger pour permettre ce genre de configurations.

Pour que le routage ne se fasse pas du tout sur un serveur secondaire ayant un trop grand retard il faut préciser la valeur de l'option `max_slave_replication_lag` avec le nombre de secondes de lags autorisées. Pour que le routeur "attende" que le serveur secondaire puisse retourner une valeur consistente lors d'une lecture suivant des modifications de données, il faut définir l'option `causal_reads` à `true` et que les serveurs aient le paramètre `session_track_system_variables` défini à `last_gtid`.

### Configuration du mode proxy_protocol

Si on a plusieurs serveurs qui doivent se connecter vers plusieurs serveurs SQL, on peut activer le mode `proxy_protocol` pour deux raisons :

* De pouvoir se connecter avec un utilisateur SQL, sans devoir recréer des utilisateurs `user@ip_server_maxscale`
* De savoir quels hôtes est a l'origine d'une requête

Côté serveur MariaDB, on doit autoriser l'ip du serveur Maxscale, dans la variable `proxy_protocol_networks` :

~~~
proxy-protocol-networks=::1, 192.168.2.90, localhost
~~~

La variable `proxy_protocol_networks` peut prendre des ips séparé par des virgules, ou alors on peut autoriser tout un sous-réseau par exemple `192.168.0.0/16`
À partir de MariaDB 10.3.6, la variable est dynamique, on peut la modifier comme ceci :

~~~
MariaDB [(none)]> SET GLOBAL proxy_protocol_networks='::1, 192.168.2.90, localhost';
~~~

Côté Maxscale, on doit rajouter l'option `proxy_protocol=true` dans la définition de chaque serveur, dans `/etc/maxscale.cnf` :

~~~
[dbserver1]
type = server
address = 192.0.2.100
port = 3306
protocol = MariaDBBackend
proxy_protocol=true
~~~

Et on redémarre le service :

~~~
# systemctl restart maxscale.sercice
~~~

Si on se connecte directement depuis le serveur applicatif a Maxscale on voit que c'est bien l'ip d'origine qui est vu (192.0.2.50), et pas l'ip du serveur Maxscale (192.168.2.90) :

~~~
root@192.0.2.50:~# mysql -h 192.168.2.90 -P4006 -u evotest -p

MariaDB [dbtest]> select user(), current_user();
+---------------------------------------------------+------------------------------------+
| user()                                            | current_user()                     |
+---------------------------------------------------+------------------------------------+
| evotest@192.0.2.50                                | evotest@192.0.2.50                 |
+---------------------------------------------------+------------------------------------+
~~~

### Création d'un utilisateur pour l'API REST Maxscale

Si on veut créer un utilisateur "simple" dans maxscale on jouera la commande suivante :

~~~
# maxctrl create user maxscale_user maxscale_password
~~~

Si on veut créer un utilisateur de type admin :

~~~
# maxctrl create user maxscale_user maxscale_password --type=admin
~~~

Si on veut supprimer un utilisateur :

~~~
# maxctrl destroy user maxscale_user
~~~ 

Pour lister tous les utilisateurs de l'API :

~~~
# maxctrl list users
~~~

# Manipuler Maxscale via le cli Maxctrl

Dans ne nombreux cas, il est préférable de modifier la configuration de Maxscale via le cli.
Par exemple, si une synchro entre deux Maxscale est mise en place, il n'est plus possible de modifier la configuration directement dans le fichier _/etc/maxscale.cnf_

Toutes les commandes se font dans le shell maxsctrl

* Créer un service :

~~~
maxctrl create service <nom_service> <router> user=<maxscale_user> password=<userpassword> <params...>
~~~

Exemple de création de service _Slave-Service_ qui route vers un replica :

~~~
maxctrl create service Slave-Service readconnroute user=maxscale password=mypassword router_options=slave
~~~

* Link un service sur un serveur :

Lorsque l'on crée un service aucun serveur n'est associé, il faut donc link un ou des serveurs à ce service :

~~~
maxctrl link service <nom_service> <server>
~~~

Exemple :

~~~
maxctrl link service Slave-Service sql10,sql11
~~~

Tous les serveurs ajoutés aux services ne seront utilisés que par les nouvelles sessions. 
Les sessions existantes utiliseront les serveurs qui étaient disponibles lors de leur connexion.

* Créer un listener :

~~~
maxctrl create listener <nom_service> <nom_listener> <port>
~~~

Exemple :

~~~
maxctrl create listener Slave-Service Slave-Listener 4009
~~~

* Supprimer un listener :

~~~
maxctrl destroy listener <nom_listener>
~~~

Exemple :

~~~
maxctrl destroy listener Slave-Listener
~~~

La destruction d'un listener fermera le socket réseau et l'empêchera d'accepter de nouvelles connexions.
Les connexions existantes qui ont été créées via celui-ci continueront à l'afficher en tant que listener d'origine.

Les listeners ne peuvent pas être déplacés d'un service à un autre. Pour ce faire, le listener doit être détruit puis recréé avec le nouveau service.

* Unlink un serveur d'un service :

~~~
maxctrl unlink service <nom_service> <server>
~~~

Exemple :

~~~
maxctrl unlink service Slave-Service sql10,sql11
~~~

De la même manière que pour l'ajout de serveurs, la suppression de serveurs d'un service n'affectera que les nouvelles sessions. 
Les sessions existantes continuent d'utiliser les serveurs même si elles sont supprimées d'un service.

* Supprimer un service :

~~~
maxctrl destroy service <nom_service>
~~~

Exemple : 

~~~
maxctrl destroy service Slave-Service
~~~

Le service ne peut être détruit que s'il n'utilise aucun serveur ou cluster et qu'aucun _listener_ n'y est associé.
Pour forcer la destruction d'un service même s'il utilise des serveurs ou possède des _listener_, utilisez l'option --force.
Cela détruira également tous les _listeners_ associés au service.

* Assigner un filtre dans un service :

~~~
maxctrl alter service-filters <nom_service> <filtre>
~~~

Exemple, on assigne les filtres *Hint* et *foo* au service *Read-Write-Service* :

~~~
maxctrl alter service-filters Read-Write-Service Hint foo
~~~

Si on veut dé-assigner un filtre à un service, il faut juste faire un *alter service-filters* avec la liste des filtres que l'on veut garder, par exemple, si on veut supprimer le filtre foo du service *Read-Write-Service* :

~~~
maxctrl alter service-filters Read-Write-Service Hint
~~~

* Supprimer un filtre :

~~~
maxctrl destroy filter <nom_filtre>
~~~

Exemple :

~~~
maxctrl destroy filter foo_filter
~~~

Il faut que tout le filtre que l'on veut supprimer soit désasigné de tout les services avant de le supprimer.
On peut utiliser l'option *--force*, qui dé-assignera le filtre de tout les services avant de le supprimer.