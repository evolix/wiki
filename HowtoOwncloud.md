---
categories: ownCloud SAAS web file
title: Howto ownCloud
...

# Installation

## Prérequis

 - PHP 5.4 +
 - Apache 2.4 with mod_php
 - PHP module ctype
 - PHP module dom
 - PHP module GD
 - PHP module iconv
 - PHP module JSON
 - PHP module libxml (Linux package libxml2 must be >=2.7.0)
 - PHP module mb multibyte
 - PHP module posix
 - PHP module SimpleXML
 - PHP module XMLWriter
 - PHP module zip
 - PHP module zlib
 - PHP module pdo_mysql (si mysql)
 - PHP module ldap (si ldap)
 - PHP module smbclient (si smb)
 - PHP module ftp
 - PHP module imap
 - **Désactiver mod_webdav (ownCloud ayant son propre DAV)**
 - Apache mod_rewrite

### Recommendé

 - PHP module curl
 - PHP module fileinfo
 - PHP module bz2
 - PHP module intl
 - PHP module mcrypt
 - PHP module openssl
 - PHP module exif
 - PHP module gmp (sftp storage)
 - PHP module memcached (si memcached)
 - Apache mod_headers
 - Apache mod_env
 - Apache mod_dir
 - Apache mod_mime
 - Désactiver (toute) l'authentification basic

<https://doc.owncloud.org/server/9.0/admin_manual/installation/source_installation.html#prerequisites-label>

~~~
# apt install libphp5-embed php5-curl php5-intl php5-mcrypt php5-gmp php5-memcache php5-memcached php5-gd php5-json php5-imap php5-pclzip
# a2enmod php5 rewrite headers env dir mime
# a2dismod dav
~~~

libphp5-embed:
''
The following extensions are built in: bcmath bz2 calendar Core ctype date dba dom ereg exif fileinfo filter ftp gettext hash iconv libxml mbstring mhash openssl pcntl pcre Phar posix Reflection session shmop SimpleXML soap sockets SPL standard sysvmsg sysvsem sysvshm tokenizer wddx xml xmlreader xmlwriter zip zlib.
''

> *Note* : Ajouter dans la directive PHP openbasedir, le chemin /dev/urandom - utilisé pour les chaînes aléatoire sur ownCloud

## Procédure

 - Se connecter en tant que simple utilisateur (lié à l'utilisateur de l'instance du vhost concerné)
 - Télécharger archive ou dépôt
 - Appliquer un patch pour le **.htaccess** dans le répertoire principal car sinon erreur 503 pour directives non autorisés
 - Créer un répertoire data
 - Lancer la commande `occ` se trouvant dans le répertoire principal

~~~{ .bash }
$ php occ maintenance:install --no-interaction --database '{{ db }}' --database-name '{{ db_name }}' --database-host '{{ db_host }}' \
--database-user '{{ db_user }}' --database-pass "{{ db_pwd }}" --admin-user 'admin' --admin-pass '{{ admin_pwd }}' --data-dir '{{ dir_www }}/data'
~~~

* Si déjà installé, on peut éventuellement penser à une mise à jour ou upgrade :

Fais d'une 9.1.3 -> 10.0.8

Télécharger la nouvelle version de owncloud, et la placer dans son $HOME :
- $HOME/www : version actuelle
- $HOME/owncloud-NEW-VERSION : nouvelle version visé
 
~~~{ .bash }
~/www$ php occ maintenance:mode --on
~/www$ cp config/config.php $HOME/owncloud-NEW-VERSION/config/config.php
~/www$ cd $HOME/owncloud-NEW-VERSION/
~/owncloud-NEW-VERSION$ ln -s $HOME/www/data .
~/owncloud-NEW-VERSION$ php occ upgrade
~/owncloud-NEW-VERSION$ occ files:scan --all --repair (mais plus récement)
~/owncloud-NEW-VERSION$ php occ maintenance:repair
~/owncloud-NEW-VERSION$ php occ maintenance:mode --off
~~~

 - Dans `config/config.php`, on remplace la valeur du champs *trusted_domains* par le FQDN souhaité (et non localhost - sinon ne marchera pas de l'extérieur)
 - Dans `config/config.php`, on remplace aussi de la même manière la valeur du champs *overwrite.cli.url*
 - Dans `config/config.php`, on ajoute un champs *instanceid*, avec comme valeur une chaîne de caractères aléatoire
 - On ajoute les droits d'écriture pour le fichier `config/config.php`
 - On ajoute les droits d'écriture pour les répertoires (et en récursif) pour : `data`, `config`, `apps`, `lib/private`

Voilà c'est prêt!

## Maintenance

Se placer toujours à la racine du ownCloud en question, pour ainsi manipuler 'occ'.

[Doc occ](https://doc.owncloud.org/server/9.0/admin_manual/configuration_server/occ_command.html?highlight=occ)

### Lister les utilisateurs

On est obligé de se connecter à la base (MySQL) :

~~~{.mysql}
> select uid from oc_users\G
~~~

### Créer un utilisateur (effectuer des tests)

~~~ { .bash }
$ php occ user:add <usertest>
Enter password: 
Confirm password: 
~~~

Pour le supprimer (simplement) :

~~~{ .bash }
$ php occ user:delete <usertest>
~~~

### Reset password

~~~ { .bash }
$ php occ user:resetpassword <user>
~~~

### Nettoyer une instance

 - Supprimer les versions précédentes des fichiers :

~~~{ .bash }
$ php occ versions:cleanup
~~~

 - Supprimer les fichiers des corbeilles :

~~~{ .bash }
$ php occ trashbin:cleanup
~~~

 - Supprimer cache :

~~~{ .bash }
$ php occ files:cleanup
~~~


# Synchronisation

## Desktop Client

[Installation par package.](https://software.opensuse.org/download/package?project=isv:ownCloud:desktop&package=owncloud-client)

![Donner l'URL complète d'accès aux ownCloud voulu](/owncloud1.png)

![Donner les identifiants pour s'y connecter](/owncloud2.png)

![Choisir si l'on veut tout synchroniser ou choisir seulement certains répertoires](/owncloud3.png)

![Sélection des répertoires voulu - si choix choisis](/owncloud4.png)

![Visualiser les fichiers ainsi synchronisé (Web - FS)](/owncloud5.png)