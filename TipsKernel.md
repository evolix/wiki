---
categories: tips
title: Tips Kernel
...

# Tips Kernel

## Introduction

~~~
$ uname -a
~~~

Le noyau en lui même est un fichier de quelques Mo. Il est souvent nommé `/boot/vmlinuz-X.y.z-patch`. C'est ce fichier qui est lancé par le boot loader (LILO, GRUB, Yaboot, etc.). Les modules éventuels se retrouvent souvent sous le répertoire `/lib/modules/X.y.z-patch/`. Parlons aussi du fichier `System.map` (table des symboles du noyau) qui se trouve en général dans le répertoire `/boot/`. Enfin, parlons du fichier `initrd` qui se trouve sur certains systèmes Linux. Il s'agit d'un disque RAM initialisé au démarrage avant le noyau pour permettre un démarrage en deux phases (utile dans certains cas). Ce fichier sera également lancé par le boot loader.

## Arguments de la ligne de commande du noyau Linux

Les arguments passés au noyau Linux par le bootloader (Grub, systemd-boot, ...) sont nombreux. Ils sont utilisés aussi bien par le noyau que par différents processus afin d'ajuseter leur comportements dès le démarrage de l'OS.

La liste des arguments interprétés par Linux est [documenté ici](https://docs.kernel.org/admin-guide/kernel-parameters.html). Ceux utilisés par systemd le sont dans la page du manuel [kernel-command-line(7)](https://manned.org/man/debian-bookworm/kernel-command-line.7). Ces listes ne sont pas exhaustives car d'autres modules (ZFS, ...) ou processus peuvent documenter leur propores arguments dans leurs documentations respectives.

## timestamp dmesg

Depuis dmesg version 2.25 (Debian Jessie), on peut utiliser `dmesg --time-format iso` ou `dmesg -T` qui indiquera la date et l'heure. Exemple de sortie :

~~~
# dmesg -T
[…]
[Fri Jul  7 10:57:14 2017] usb 1-2.1.4: new low-speed USB device number 12 using xhci_hcd           
[Fri Jul  7 10:57:14 2017] usb 1-2.1.4: New USB device found, idVendor=1e54, idProduct=2030         
[Fri Jul  7 10:57:14 2017] usb 1-2.1.4: New USB device strings: Mfr=1, Product=2, SerialNumber=0    
[Fri Jul  7 10:57:14 2017] usb 1-2.1.4: Product: USB Keyboard                                       
[Fri Jul  7 10:57:14 2017] usb 1-2.1.4: Manufacturer: TypeMatrix.com                                
[…]
~~~

Pour les versions plus anciennes, quant on a une ligne du type :

~~~
[3412725.317750] entry->group=(null) inode=(null) wd=4096
~~~

On peut déduire depuis combien d'heure(s) cela s'est produit avec la commande :

~~~
$ echo "($(awk '{print $1}' /proc/uptime) - 3412725.317750 ) / 60 / 60" | bc
18
~~~

## Magic SysRq key

"Magic SysRq key" est un combo de touches qui peut varier en fonction du matériel, mais est généralement *Alt+SysRq+\<touche commande>**

### Avec le clavier

**Alt+SysRq+\<touche commande>**

*SysRq* est souvent la touche `Impr écran` (sur certains claviers, il faut *Fn*), donc **Alt+Impr écran+\<touche commande>**

### En ligne de commande / SSH

~~~
# sysctl -w kernel.sysrq=1
# echo <touche commande> > /proc/sysrq-trigger
~~~

Ou si sysctl ne fonctionne pas…

~~~
# sysctl -w kernel.sysrq=1
-bash: /sbin/sysctl: Input/output error
# echo "1" > /proc/sys/kernel/sysrq
~~~

Voici les principales touches commandes (dans un ordre croissant de violence) :

* `s` : Will attempt to sync all mounted filesystems.
* `u` : Will attempt to remount all mounted filesystems read-only.
* `b` : Will immediately reboot the system without syncing or unmounting your disks.

Pour la liste complète des touches commandes utilisables dans un combo Magic SysRq : <https://www.kernel.org/doc/Documentation/sysrq.txt>

## CPU hotplug

L'ajout de CPU à chaud peut être utile sur des machines virtuelles.

Activation :

~~~
# echo 1 > /sys/devices/system/cpu/cpu1/online
[2413287.667565] CPU1 has been hot-added
# echo 1 > /sys/devices/system/cpu/cpu2/online
[2413287.668239] CPU2 has been hot-added
~~~

Désactivation :

~~~
# echo 0 > /sys/devices/system/cpu/cpu2/online
~~~

## Compilation de noyau

### Préparation du noyau

Noyau [vanilla](http://kernel.org) :

~~~
$ tar -xf linux-X.y.z.tar.xz
~~~

### Patches

#### Patches rc ou X.y.z

~~~
$ cd linux-X.y
$ patch -p1 < ../patch
~~~

Ne pas utiliser de noyau X.y.**z** car les modifications sont déjà intégrées aux patches rc.

#### Patches grsecurity :

<https://grsecurity.net/>

~~~
$ gunzip < grsecurity.patch.gz |  patch -p0
~~~

#### Patches IPTables

<http://www.netfilter.org/documentation/HOWTO/fr/netfilter-extensions-HOWTO.html>

~~~
$ cd netfilter/patch-o-matic
$ ./runme extra
~~~

#### Patches Debian

Debian propose des paquets linux-source-* contenant les sources vanilla patchées.

## Choix des options

Pour le choix des options, plusieurs alternatives :

* `make menuconfig` (nécessite le paquet *libncurses5-dev*)
* `make xconfig` (nécessite le paquet *libqt4-dev*)
* `make config`

Pour récupérer les options d'un autre noyau (même plus ancien) :

~~~
$ cp /boot/config-X.y.z .config
$ make oldconfig
~~~

## Compilation classique

~~~
# apt install libssl-dev
$ make clean
$ make bzImage 
$ make modules
~~~

Pour l'installer :

~~~
# cp arch/i386/boot/bzImage /boot/vmlinuz-foo
# makes modules_install
~~~

## Compilation "à la sauce Debian"

~~~
# apt install kernel-package build-essential kmod xz-utils
~~~

Modifier éventuellement le fichier /etc/kernel-pkg.conf puis lancer la compilation :

~~~
$ make-kpkg clean
$ make-kpkg -j4 --append-to-version=-foo --initrd buildpackage
~~~

## sysctl

~~~
# systcl net.nf_conntrack_max # Lire la valeur
# sysctl -a #| Lire toutes les valeurs
# sysctl -w -w net.nf_conntrack_max=262144 # Changer la valeur
~~~

Appliquer la configuration que l'on a mise en dur :

~~~
# sysctl -p /etc/sysctl.conf
# sysctl -p /etc/sysctl.d/evolinux.conf
~~~

Attention, il faut spécifier explicitement le fichier où l'on a mis les valeurs, `sysctl -p /etc/sysctl.conf` n'appliquera pas les directives des fichiers dans `/etc/sysctl.d/`


## FAQ

### erreur "No filesystem could mount root"

En cas d'erreur :

~~~
No filesystem could mount root, tried:
Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(0,0)
~~~

Une solution possible est de vérifier que l'initrd est bien présent et/ou de le regénérer via `update-initramfs` ; par exemple avec un noyau _3.2.0-4-amd64_ :

~~~
# update-initramfs -c -v -k 3.2.0-4-amd64
# update-grub2
~~~
