---
categories: storage log utilities file filesystem
title: Howto Logrotate
...

* Documentation : 

- <https://linux.die.net/man/8/logrotate>
- <https://manpages.debian.org/cgi-bin/man.cgi?query=logrotate>

Logrotate est un programme qui permet de gérer la croissance et
l'historique des fichiers de logs. Le programme est exécuté
régulièrement, avec un fichier de configuration en paramètre.
Par défaut, c'est `/etc/logrotate.conf` qui charge toutes les
configurations présentes dans `/etc/logrotate.d/`. Les paquets des
programmes qui génèrent des logs (par exemple Apache, Postfix, ...)
fournissent généralement une configuration pour logrotate.

Ce programme est spécifique aux distributions Linux, *sous les systèmes
de la famille BSD, l'équivalent de logrotate est newsyslog*

# Installation

~~~
# apt install logrotate
# logrotate --version
logrotate 3.11.0
~~~

## Rotation et compression de logs

Exemple de configuration pour les serveurs d'application comme Puma, Unicorn, ... :

~~~
/home/USER/apps/production/*/shared/log/*.log {
    su USER GROUP
    weekly
    missingok
    rotate 52
    compress
    delaycompress
    notifempty
    dateext
    dateformat .%Y-%m-%d
    dateyesterday
    copytruncate
}
~~~

Cette stratégie va faire une rotation hebdomadaire, conserver 52 semaines (1 an), compresser les logs passés après les avoir datés.

L'instruction `copytruncate` est importante car le descripteur de fichier ouvert par l'application ne va pas changer.


## Compresser des logs sans faire la rotation

**Attention, il semble que cette configuration Logrotate ne se comporte pas comme attendu !** A la place, il vaut mieux déclencher une commande de compression via un cron, comme décrit dans le [HowtoElasticsearch](/HowtoElasticsearch#rotation-et-compression-des-logs).

Admettons que l'on ait une application qui fait sa rotation de log mais ne fait pas de compression :

~~~
/usr/local/apps/logs/*.log.*[!gz] {
    su USER GROUP
    daily
    missingok
    rotate 365
    compress
    dateext
    dateformat 
}
~~~

Cette stratégie va faire une rotation journalière, conserver 365 jours, compresser les logs ayant dans le nom de fichier $NOM__FICHIER.log.$DATE mais ne ne concernant pas les fichiers déjà compressé portant l'extention .gz.

## Tester et valider la configuration

* Pour vérifier qu'il n'y a pas d'erreurs globalement de syntaxe :

~~~
logrotate --debug /etc/logrotate.d/; echo $?
~~~

* Pour débugger le comportement d'un fichier de configuration :

~~~
logrotate --verbose --debug --force /etc/logrotate.d/fichier_configuration
~~~

## FAQ

### Augmenter la fréquence des logs

Si les logs prennent beaucoup d'espace disque, on peut augmenter la fréquence de rotation en effectuant ce changement :

~~~
-        weekly
+        daily
-        rotate 52
+        rotate 365
~~~

Attention que par défaut logrotate n'est exécuté qu'une fois par jour (par crons ou systemd-timers selon la version de Debian), donc il faut aussi modifier ce paramètre.

Dans le cas où `/etc/cron.daily/logrotate` s'interrompt si systemd-timers est présent, alors il faut éditer l'unité :

~~~
# systemctl edit logrotate.timer

[Timer]
OnCalendar=
OnCalendar=hourly
AccuracySec=1m

# systemctl reenable --now logrotate.timer
~~~

Dans le cas où c'est un simple cron, on peut le déplacer :

~~~
# mv /etc/cron.daily/logrotate /etc/cron.hourly/logrotate
~~~

### parent directory has insecure permissions

Si erreur du type :

` error: skipping "*.log" because parent directory has insecure permissions (It's world writable or writable by group which is not "root")`

Uniquement l'utilisateur concerné par la gestion des logs doit avoir un accès en écriture à ce dossier. Soit un chmod 750 marche bien.

### delaycompress

L'option `delaycompress` permet d'avoir un fichier .1 non compressé (à la place d'un .1.gz donc).
TODO expliquer dans quel cas on veut ça
