---
categories: video
title: Howto youtube-dl
...

* Documentation : <http://ytdl-org.github.io/youtube-dl/documentation.html>

[youtube-dl](https://youtube-dl.org/) est un outil pour télécharger les vidéos de Youtube mais pas seulement ! On peut télécharger des vidéos sur de nombreux sites spécifiques ou génériques.

Note préalable : le téléchargement de vidéos doit se faire en respectant les droits d'auteur, par exemple pour conserver une copie privée.

## Installation

On recommande d'avoir la dernière version car elle permet de prendre en compte les dernières cochonneries des sites spécifiques, à noter qu'il n'est pas nécessaire de l'installer en root, on peut simplement la mettre dans un répertoire temporaire en tant qu'utilisateur :

~~~
$ wget https://yt-dl.org/downloads/latest/youtube-dl
$ chmod +x youtube.dl
$ ./youtube-dl --version
2021.03.25
~~~

## Usage

### Youtube

Pour télécharger une vidéo Youtube, il suffit de mettre l'URL de la vidéo :

~~~
$ youtube-dl 'https://www.youtube.com/watch?v=ZoGH7d51bvc'

[youtube] ZoGH7d51bvc: Downloading webpage
[youtube] ZoGH7d51bvc: Downloading API JSON
[youtube] ZoGH7d51bvc: Downloading API JSON
WARNING: Requested formats are incompatible for merge and will be merged into mkv.
[download] Destination: Réformons l'élection présidentielle !-ZoGH7d51bvc.f248.webm
[download] 100% of 118.41MiB in 00:45
[download] Destination: Réformons l'élection présidentielle !-ZoGH7d51bvc.f140.m4a
[download] 100% of 17.35MiB in 00:06
[ffmpeg] Merging formats into "Réformons l'élection présidentielle !-ZoGH7d51bvc.mkv"
Deleting original file Réformons l'élection présidentielle !-ZoGH7d51bvc.f248.webm (pass -k to keep)
Deleting original file Réformons l'élection présidentielle !-ZoGH7d51bvc.f140.m4a (pass -k to keep)

$ ffprobe Réformons\ l\'élection\ présidentielle\ \!-ZoGH7d51bvc.mkv

[...]
Input #0, matroska,webm, from 'Réformons l'élection présidentielle !-ZoGH7d51bvc.mkv':
  Metadata:
    ENCODER         : Lavf58.20.100
  Duration: 00:19:05.44, start: 0.000000, bitrate: 995 kb/s
    Stream #0:0(eng): Video: vp9 (Profile 0), yuv420p(tv, bt709), 1920x1080, SAR 1:1 DAR 16:9, 29.97 fps, 29.97 tbr, 1k tbn, 1k tbc (default)
    Metadata:
      DURATION        : 00:19:05.377000000
    Stream #0:1: Audio: aac (LC), 44100 Hz, stereo, fltp (default)
    Metadata:
      HANDLER_NAME    : SoundHandler
      DURATION        : 00:19:05.440000000
~~~

On peut aussi télécharger uniquement l'audio (MP3) d'une vidéo Youtube :

~~~
$ youtube-dl -i -x --audio-quality 0 --audio-format mp3 -f bestaudio 'https://www.youtube.com/watch?v=ZoGH7d51bvc'
~~~

On peut aussi télécharger les fichiers audio de l'ensemble d'une playlist :

~~~
$ youtube-dl -i -x --audio-quality 0 --audio-format mp3 -f bestaudio 'https://www.youtube.com/watch?v=HsUjBw_auZw&list=PL2HZXed4ZqDpmoal0fwd8_m7w_5LqcJ49'
~~~

### Vidéo à partir de son manifest.mpd

Sur certains sites - comme France.tv (France2, France3, France5, etc.) - les vidéos sont diffusées en streaming HTML5. Il faut alors d'activer son module "Développement Web / Réseau" pour scruter les requêtes tout en regardant la vidéo dans Firefox : il suffit d'intercepter l'URL du fichier `manifest.mpd` et de le passer à youtube-dl :

~~~
$ youtube-dl 'https://cloudreplay.ftven.fr/ecf90c6681606/983914472_monde_TA.ism/ZXhwPTE2MTczMzM0OTB+YWNsPSUyZmVjZjkwYzY2ODE2MDYlMmY5ODM5MTQ0NzJfbW9uZGVfVEEuaXNtKn5obWFjPWI2MzVjMDJjYTczZTViOWQwN2FjNDdkNmEwYjZhOTI2NzUxN2Y1NWI4ZmM4NTIxYzllZjE3ZGViNWNjY2IxOGY=/manifest.mpd'
~~~


