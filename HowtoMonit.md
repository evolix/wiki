---
title: Howto Monit : installation de base
---

Documentation officielle : <https://mmonit.com/monit/documentation/monit.html>

## Installation

~~~
# apt install monit
~~~

## Interface CLI

Monit est utilisable en ligne de commande :

~~~
start all           - Start all services
start name          - Only start the named service
stop all            - Stop all services
stop name           - Only stop the named service
restart all         - Stop and start all services
restart name        - Only restart the named service
monitor all         - Enable monitoring of all services
monitor name        - Only enable monitoring of the named service
unmonitor all       - Disable monitoring of all services
unmonitor name      - Only disable monitoring of the named service
reload              - Reinitialize monit
status              - Print full status information for each service
summary             - Print short status information for each service
quit                - Kill monit daemon process
validate            - Check all services and start if not running
procmatch <pattern> - Test process matching pattern
~~~

En autorisant la commande Monit à certains utilisateurs non-administrateurs, il est possible de permettre la relance de processus lors de séquences de déploiement.

## Interface web

Il est aussi possible d'activer un serveur web intégré pour avoir une version web.
Cette interface dispose de son propre port et de ses mécanismes d'authentification.

## Pourquoi l'utiliser ?

Nous l'utilisons par exemple pour relancer des processus peu fiables, qui crashent souvent et qui doivent être relancés très réactivement (ou bien en cas de fuite mémoire).

Il peut être aussi utilisé pour déclencher des actions ou alertes en cas de changement d'état du système (présence de processus, niveau des ressources…) mais pour cela nous préférons Nagios qui est plus complet.

## Complément

Pour gérer la rotation des logs de Monit :

~~~
/var/log/monit.log {
    weekly
    missingok
    rotate 52
    compress
    notifempty
    create 640 root adm
    sharedscripts
    postrotate
        if [ -f /var/run/monit.pid ]; then
            /etc/init.d/monit force-reload > /dev/null
        fi
    endscript
}
~~~
