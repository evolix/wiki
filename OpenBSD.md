---
title: Howto OpenBSD
categories: openbsd misc
---

## Désactiver l'ACPI

*Soucis lié à l'ACPI sur Dell R220*

* Pour désactiver l'ACPI au démarage :

~~~
boot> boot -c
...
UKC> disable acpi
UKC> quit/
~~~

* Pour le désactiver (presque) définitivement :

La configuration sera perdue après un upgrade ou l'application d'un `syspatch`.

~~~
# config -ef /bsd  
Enter 'help' for information
ukc> disable acpi
ukc> quit
Saving modified kernel. 
~~~

* Pour le désactiver (vraiment) définitivement :

Disponible à partir d'OpenBSD 7.0

~~~
echo "disable acpi" > /etc/bsd.re-config
~~~

## ddb

Lors d'un crash d'OpenBSD, le serveur peut arriver dans un prompt `ddb>`. Dans ce cas, il faut suivre <https://www.openbsd.org/ddb.html> pour voir ce qui a causé le crash.

~~~
ddb> show panic
ddb> ps

# À répéter pour chaque CPU du serveur
ddb> machine ddbcpu 0
ddb{0}> trace

ddb{0}> machine ddbcpu 1
ddb{1}> trace
~~~

Le but est de récupérer un maximum d'info, pour pouvoir faire un bug report en suivant <https://www.openbsd.org/report.html>

## syspatch

[syspatch](https://man.openbsd.org/syspatch) permet de maintenir son noyau/système up-to-date (fonctionne uniquement pour les versions supportées, à savoir stable et stable-1) :

~~~
root:1# syspatch -c                                                            
004_libssl
005_ahopts
006_prevhdr
007_etherip

root:2# syspatch    
Get/Verify syspatch62-004_libssl.tgz 100% |*************|  2515 KB    00:02    
Installing patch 004_libssl
Get/Verify syspatch62-005_ahopts.tgz 100% |********************************************************|   703 KB    00:01    
Installing patch 005_ahopts
Get/Verify syspatch62-006_prevhdr... 100% |********************************************************|   783 KB    00:01    
Installing patch 006_prevhdr
Get/Verify syspatch62-007_etherip... 100% |********************************************************|  1030 KB    00:01    
Installing patch 007_etherip
Relinking to create unique kernel...  done.

root:4# syspatch -l 
001_tcb_invalid
002_fktrace
003_mpls
004_libssl
005_ahopts
006_prevhdr
007_etherip
~~~

rollback :

~~~
# syspatch -r
~~~

## FAQ


### ksh: ignoring old style history file

~~~
$ mv .histfile .histfile.old
~~~