# Tips Redmine

## Console Redmine

Il existe une « console interactive » permettant de passer des instructions directement à Redmine, c'est utile notamment pour effectuer des actions mis en place dans un script.

La console se trouve dans `script/console`.

Voici un exemple qui crée un utilisateur, qui crée un projet et ajoute l'utilisateur dedans.

~~~
$ script/console production <<< 'user = User.new({:firstname => "Test", :lastname=>"Dummy",:mail=>"test@testing.com"}); user.login = "toto"; user.auth_source_id=1; user.save;'
$ script/console production <<< 'project = Project.new({:name => "Projet de Toto", :description => "Un projet d'étudiant blabla.", :identifier => "projetdetoto"})'
$ script/console production <<< 'member = Member.new; member.user = User.find_by_login('toto'); member.project = Project.find_by_identifier('projetdetoto'); member.roles = [Role.find_by_name('Manager')]; member.save;'
~~~

## Visibilité des utilisateurs

Par défaut les informations sur un utilisateur sont visibles publiquement via  l'URL */users/NN* !
Et même si Redmine est protégé par une authentification, une fois logué on peut voir les informations de tous les utilisateurs.

Pour changer cela, il faut modifier les paramètres de chaque rôle et régler **Visibilité des utilisateurs**