**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Introduction

En [avril 2014](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=739981) Debian ajoute le support pour `/etc/subuid` et `/etc/subgid` au programme `shadow`, permettant ainsi l'utilisation des Linux Containers (LXC). Cette patch est disponible à travers le packet `passwd 1.4.2-1` et est donc en place par défaut à partir de Jessie.

## Que sont les LXC?

Wikipedia décris les LXC comme suit: 

_LXC, contraction de l’anglais Linux Containers est un système de virtualisation, utilisant l'isolation comme méthode de cloisonnement au niveau du système d'exploitation. Il est utilisé pour faire fonctionner des environnements Linux isolés les uns des autres dans des conteneurs partageant le même noyau et une plus ou moins grande partie du système hôte. Le conteneur apporte une virtualisation de l'environnement d'exécution (processeur, mémoire vive, réseau, système de fichier…) et non pas de la machine. Pour cette raison, on parle de « conteneur » et non de machine virtuelle_. -- [Wikipedia](https://fr.wikipedia.org/wiki/LXC)

Les conteneurs offerts par LXC ressemblent donc à ceux créés par OpenVZ. Si Docker utilisait LXC à ses débuts, ce n'est plus le cas depuis la version 0.9.

Comme LXD est basé sur LXC, `/etc/subuid` et `/etc/subgid` sont également nécessaire à sont bon fonctionnement.

# subuid, subgid

## Fonction

On utilise les subuids et les subgids avec LXC dans le but de permettre à un utilisateur qui n'est pas root de pouvoir créer de nouveaux conteneurs. Cela est normalement fait avec les deux commandes suivantes:

* `newuidmap`
* `newgidmap`

## Utilisation

Les deux fichiers `/etc/subuid` et `/etc/subgid` suivent la syntaxe suivante:

~~~
"login name":"numerical subordinate user ID":"numerical subordinate user ID count"
~~~

Ainsi, si on souhaite donner à l'utilisateur `roger` les UIDs de 10200 à 10300, on ajouterait la ligne suivante:


~~~
roger:10200:100
~~~

La même logique s'applique pour `/etc/subgid`.

Il n'est cependant pas nécessaire de faire ces configurations manuellement. En effet, il est possible de configuer `/etc/login.defs` pour que ces deux fichiers soient modifiés automatiquement lors de la création d'un nouvel utilisateur via `useradd` ou `newusers`.

Les paramètres en question dans `/etc/login.defs` sont:

* `SUB_GID_MIN`
* `SUB_GID_COUNT`
* `SUB_GID_MAX`
* `SUB_UID_MIN`
* `SUB_UID_MAX`
* `SUB_UID_COUNT`

La création automatique des subuids et de subgids est faite seulement si les fichiers `/etc/subuid` et `/etc/subgid` existent.
