---
categories: mail
title: Howto Exim
...

Nous préférons Postfix à Exim. Voici toute de même quelques informations de survie quand on rencontre un Exim.


# mailq

La commande `mailq` est très différente de celle de Postfix.
Voici un exemple de sortie.

~~~
# mailq
26h  824K 1cDs52-0006oK-8E <foo@example.com>
        D bar@example.com
5m  6.2K 1cEFxl-0003cL-JY <> *** frozen ***
          baz@example.com
0m  106K 1cEG2d-0004Ha-Mg <>
         qux@example.com
~~~

Dans cet exemple, il y a 3 mails dans la liste d'attente (« la mailq »).
La première ligne dispose de 5 éléments :  

- La durée du mail en mailq ;
- La taille du mail ;
- L'identifiant unique du mail ;
- L'expéditeur (from d'enveloppe) (peut être `<>` si c'est un bounce) ;
- Indicateur « frozen », indique si le mail est suspendu ou non (action manuelle).

La seconde ligne indique le ou les destinataires.

## Actions sur la mailq

Forcer la mailq à retraiter ses mails : 

    # exim -q -v

Forcer la mailq à retraiter seulement les mails locaux : 

    # exim -ql -v

Supprimer un mail : 

    # exim -Mrm <message-id> [ <message-id> ... ]

Freezer/suspendre un mail : 

    # exim -Mf <message-id> [ <message-id> ... ]

Dé-freezer/débloquer un mail suspendu : 

    # exim -Mt <message-id> [ <message-id> ... ]

Force un message à être délivré (même si suspendu) : 

    # exim -M <message-id> [ <message-id> ... ]

Force un mail à être délivré seulement si le temps de ré-essai a été atteint.

    # exim -Mc <message-id> [ <message-id> ... ]

Force un mail à échouer et le bounce avec « cancelled by administrator » : 

    # exim -Mg <message-id> [ <message-id> ... ]

Supprime tous les messages suspendus/frozen : 

    # exiqgrep -z -i | xargs exim -Mrm

Supprime tous les messages de plus de 5j (86400 * 5 = 432000 secondes) :

    # exiqgrep -o 432000 -i | xargs exim -Mrm

Suspend/freeze tous les mails d'un expéditeur en particulier : 

    # exiqgrep -i -f luser@example.tld | xargs exim -Mf

Voir les en-têtes d'un mail : 

    # exim -Mvh <message-id>

Voir le corps du message : 

    # exim -Mvb <message-id>

Voir les logs associés à un mail :

    # exim -Mvl <message-id>

Rajouter un destinataire à un mail : 

    # exim -Mar <message-id> <address> [ <address> ... ]

Éditer l'expéditeur d'un mail : 

    # exim -Mes <message-id> <address>
