---
categories: web sql
title: Howto Tableau
...

* Documentation : <https://www.tableau.com/fr-fr/resources/reference-materials>
* Statut de cette page : test / bullseye

[Tableau](https://www.tableau.com/fr-fr/) est un logiciel propriétaire permettant de piloter une base de données (par exemple [PostgreSQL](HowtoPostgresql)).

## Installation

Tableau supporte uniquement Ubuntu, nous installons donc un conteneur LXC :

~~~
# lxc-create -t ubuntu -n tableau -- -r bionic

# lxc-attach --name tableau

lxc# apt install ./tableau-server-2022-1-7_amd64.deb
[…]
Il est nécessaire de prendre 112 Mo/2 959 Mo dans les archives.
Après cette opération, 20,7 Go d'espace disque supplémentaires seront utilisés.
[…]
Paramétrage de tableau-server-20221.22.0916.0328 (20221-22.0916.0328) ...
sysctl: setting key "kernel.core_pattern": Read-only file system
sysctl: setting key "kernel.sem": Read-only file system
sysctl: setting key "vm.max_map_count": Read-only file system
sysctl: setting key "kernel.pid_max": Read-only file system

If this is a single node or initial node installation, run:

    sudo /opt/tableau/tableau_server/packages/scripts.20221.22.0916.0328/initialize-tsm --accepteula

to continue setting up Tableau Server. If this installation is part of a multi-node configuration,
see the online documentation for installing Tableau Server on additional nodes.

lxc# adduser tableau

lxc# /opt/tableau/tableau_server/packages/scripts.20221.22.0916.0328/initialize-tsm --accepteula --activation-service -a tableau
Creating environment file...
Creating directories and setting permissions...
Using '/var/opt/tableau/tableau_server' as the data directory.
Adding user 'tableau' to group 'tableau'...
Adding user 'tableau' to group 'tsmadmin'...
Added. Note: These group membership changes do not take effect in shells already open. For these to take effect, log out of the shell and log back in.
Job for fnplicenseservice_0.service failed because a timeout was exceeded.
See "systemctl --user status fnplicenseservice_0.service" and "journalctl --user -xe" for details.
lxc# /opt/tableau/tableau_server/packages/scripts.20221.22.0916.0328/initialize-tsm --accepteula --activation-service -a tableau
Creating environment file...
Creating directories and setting permissions...
Using '/var/opt/tableau/tableau_server' as the data directory.
Adding user 'tableau' to group 'tableau'...
Adding user 'tableau' to group 'tsmadmin'...
Added. Note: These group membership changes do not take effect in shells already open. For these to take effect, log out of the shell and log back in.
Starting TSM services...
Mise à jour de la version de référentiel dans le service de coordination Tableau Server.
TSM services started successfully
Use the 'tsm' command to continue setting up Tableau Server.
>> Tableau binary directory will be added to PATH for new shells. To get the
>> updated path, either start a new session, or for bash users run:
>> source /etc/profile.d/tableau_server.sh
The TSM administrative web interface (and REST API) is now available at
https://tableau:8850
You can continue the configuration and initialization of Tableau server using either the TSM command line interface,
or the web interface.
You will be prompted to authenticate, or can log in using the username 'tableau', with the same password you used to log into this session. You could also use any username, with its password, from the administrative group in the domain.
Done.
~~~

Pour installer le pilote pour PostgreSQL on doit le télécharger sur <https://www.tableau.com/fr-fr/support/drivers> :

~~~
# mkdir -p /opt/tableau/tableau_driver/jdbc
# chmod 755 /opt/tableau/tableau_driver /opt/tableau/tableau_driver/jdbc
# cd /opt/tableau/tableau_driver/jdbc
# wget https://downloads.tableau.com/drivers/linux/postgresql/postgresql-42.3.3.jar
# chmod 644 /opt/tableau/tableau_driver/jdbc/postgresql-42.3.3.jar
# tsm stop
# tsm start
~~~

## Administration

~~~
lxc# tsm status
lxc# tsm stop
lxc# tsm start (le démarrage prend 5 à 10 minutes!!)
~~~

La sortie de la commande `tsm status` est documentée [ici](https://help.tableau.com/current/server-linux/en-us/cli_status_tsm.htm).

## FAQ

### Problème avec le service user@1001

Il y a un problème avec le service user@1001. 1001 est l’UID de l’utilisateur tableau dans le conteneur.

~~~
-- L'unité (unit) user@1001.service a commencé à démarrer.
oct. 07 07:55:16 tableau systemd[318]: pam_unix(systemd-user:session): session opened for user tableau by (uid=0)
oct. 07 07:55:16 tableau systemd[318]: pam_systemd(systemd-user:session): pam-systemd initializing
oct. 07 07:55:16 tableau systemd[318]: Failed to create /user.slice/user-1001.slice/user@1001.service/init.scope control group: Permission denied
oct. 07 07:55:16 tableau systemd[318]: Failed to allocate manager object: Permission denied
oct. 07 07:55:16 tableau systemd[319]: pam_unix(systemd-user:session): session closed for user tableau
oct. 07 07:55:16 tableau systemd[1]: user@1001.service: Failed with result 'protocol'.
oct. 07 07:55:16 tableau systemd[1]: Failed to start User Manager for UID 1001.
-- Subject: L'unité (unit) user@1001.service a échoué
~~~

Voici l'astuce pour contourner :

~~~
diff --git a/default/grub b/default/grub
index b3a3467..c4ad2a6 100644
--- a/default/grub
+++ b/default/grub
@@ -6,7 +6,7 @@
 GRUB_DEFAULT=0
 GRUB_TIMEOUT=5
 GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
-GRUB_CMDLINE_LINUX_DEFAULT="quiet" 
+GRUB_CMDLINE_LINUX_DEFAULT="quiet systemd.legacy_systemd_cgroup_controller=1" 
 GRUB_CMDLINE_LINUX="" 

 # Uncomment to enable BadRAM filtering, modify to suit your needs
~~~

### Liens utiles

<https://jacksontwo.com/installing-tableau-server-on-linux-tableau-2021-1-edition>

