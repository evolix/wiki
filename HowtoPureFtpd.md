# How To pure-ftpd

## Changer mot de passe compte

~~~{.bash}
$ pure-pw passwd USER
~~~

### Si base MySQL

~~~{.bash}
$ mysql -e 'SELECT User,Password FROM BASEPURE.users WHERE Status="1" AND (Ipaddress = "*" OR Ipaddress LIKE "\R");' | cut -d' ' -f2 | grep USER
$ mysql -e "UPDATE BASEPURE.users SET Password=MD5('hawbOng\ocov6') WHERE User='USER';"
~~~

## Lister les sessions en cours

~~~{.bash}
pure-ftpwho -H
~~~

## Ajout d'un utilisateur virtuel
 
~~~
mkdir /home/ftp/foo/
pure-pw useradd foo -d /home/ftp/foo/ -u 110 -g 65534
pure-pw mkdb
~~~
