---
categories: desktop sécurité
title: Howto KeePassXC
...

* Code source : <https://github.com/keepassxreboot/keepassxc>

[KeePassXC](https://keepassxc.org/) est un logiciel libre de gestion des mots de passe pour Linux, Windows et Mac.

KeePassXC est un fork du logiciel [KeePassX](/HowtoKeePassX).
 
**Chez Evolix nous utilisons désormais KeePassXC et le format .kdbx au lieu de KeePassX.**

# Installation

~~~
# apt install keepassxc

$ keepassxc -v
KeePassXC 2.7.4
~~~


# Utilisation

## Créer une base de données

Vous pouvez créer un fichier .kdbx protégé par mot de passe et/ou clé pour stocker vos mots de passe. Depuis l'interface graphique :

1. `Base de données > Nouvelle base de données` puis continuer ;

2. entrer un nom et une description (facultative) puis continuer ;

3. pour les paramètres de chiffrement, on peut laisser puis continuer ;

4. saisir le mot de passe de la base de données puis continuer ;

5. choisir le nom et l'emplacement du fichier.

Vous pourrez ultérieurement l'utiliser via la commande :

~~~
$ keepassxc foo.kdbx
~~~

## Raccourcis

* `Ctrl+b` : copier le login dans le presse-papier
* `Ctrl+c` : copier le mot de passe dans le presse-papier
* `Ctrl+u` : copier l'URL dans le presse-papier
* `Ctrl+t` : copier jeton TOTP
* `Ctrl+l` : verrouille la base de données
* `Ctrl+Shift+u` : ouvrir l'URL dans votre navigateur
* `Ctrl+Shift+v` : _à utiliser avec précaution_, tente de remplir automatiquement le formulaire de connexion d'une autre fenêtre, comme un navigateur. KeePassXC va minimiser sa fenêtre puis entrer `<login>TAB<mot de passe>ENTRÉE`

## Verrouillage

Nous conseillons de configurer un verrouillage automatique de votre KeePassX sous `Outils > Paramètres > Sécurité`.


# keepassxc-cli

C'est la version CLI de KeePassXC, elle n'a pas été vraiment conçu pour un usage quotidien mais plutôt pour l'utiliser dans des scripts (bien que sa sortie ne soit [pas simplement parseable](https://github.com/keepassxreboot/keepassxc/issues/6011)).

## Créer une nouvelle base

~~~
$ keepassxc-cli db-create --set-password /tmp/foo.kdbx
Enter password to encrypt database (optional):
Repeat password:
Successfully created new database.
~~~

## Ajouter une entrée

~~~
$ keepassxc-cli add --username alice --password-prompt /tmp/foo.kdbx bar
Enter password to unlock /tmp/foo.kdbx:
Enter password for new entry:
Successfully added entry bar.
~~~

## Afficher une entrée

~~~
$ keepassxc-cli show --show-protected /tmp/foo.kdbx bar
Enter password to unlock /tmp/foo.kdbx:
Title: bar
UserName: alice
Password: bob
URL:
Notes:
Uuid: {0310de64-3f68-4a94-840d-6b35e6c1d8cf}
Tags:
~~~

## TOTP

### Ajouter un secret TOTP

Il n'est pas possible d'ajouter un secret TOTP en ligne de commande, car aucune des commandes `add` ou `edit` ne permet de spécifier un attribut. Dans KeepasXC les secrets TOTP sont stockés dans l'attribut `totp` sous la frome d'URL comme celle ci `otpauth://totp/totp@authenticationtest.com?secret=I65VU7K5ZQL7WB4`.

Il est prévu que la version 2.8 de KeePassXC supporte cette fonctionalité via la [PR #7462](https://github.com/keepassxreboot/keepassxc/issues/7462), cette demande de fonctionalité est suivi dans l'[issue #9212](https://github.com/keepassxreboot/keepassxc/issues/9212).

### Génerer un jeton TOTP

~~~
$ keepassxc-cli show --totp /tmp/foo.kdbx bar
Enter password to unlock /tmp/foo.kdbx bar:
123456
~~~

# Diceware - Générer une phrase secrète

_Diceware_ est une méthode de génération de phrases secrètes (_passphrase_).
Le but est de générer, non pas un mot de passe, mais une phrase secrètes
à l'aide de dés et d'une grande liste de mots.

Cette méthode a été inventée par Arnold Reinhold. La méthode est décrite, en
anglais, sur
[le site de Diceware](https://theworld.com/~reinhold/diceware.html).
La méthode est aussi décrite en français dans
[ce PDF](https://weber.fi.eu.org/software/diceware/francais.pdf?src).

Concernant le nombre de mots,
[la FAQ du site officiel](https://theworld.com/%7Ereinhold/dicewarefaq.html#howlong)
donne des recommandations. Une phrase de six mots offre encore une bonne
sécurité en 2025. Il est possible d'insérer un caractère non alphabétique
en suivant la méthode présentée sur
[la page d'accueil](https://theworld.com/~reinhold/diceware.html)
(chercher _insert one special character_).

KeePassXC peut générer des mots de passe, mais aussi des phrases secrètes !

## Générer une phrase secrète

Depuis l'interface graphique, `Outils > Générateur de mot de passe` puis
sélectionner l'onglet `Phrase de passe`. Il reste ensuite à ajuster le
nombre de mots.

Par défaut, la liste de mots utilisée est
[celle de l'EFF](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt),
mais on peut donner à KeePassXC d'autres listes de mots, comme
[cette liste de mots en français](http://world.std.com/~reinhold/wordlist_fr_8192.txt).

En ligne de commande :

    $ keepassxc-cli diceware -W 6 -w wordlist_fr_8192.txt

Il y a sinon la méthode manuelle, avec des dés. ☺
