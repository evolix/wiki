---
title: Astuces FileSystem Ext
...

## ext3/4

### Journalisation

La journalisation est présente dans ext3/4 elle permet de … blabla … Cela permet notamment de garantir une intégrité du système de fichiers alors que celui-ci à subit une opération brute, comme un hard reboot.

Il peut être intéressent de la désactiver ou de changer la méthode pour augmenter les performances du FS.

#### Changer la méthode de journalisation

##### data=ordered

Le mode ordered permet de…

Dans le fstab :

~~~
data=ordered,noatime,nodiratime
~~~

##### data=writeback

Le mode writeback permet de…

Dans le fstab :

~~~
data=writeback,noatime,nodiratime
~~~

~~~
# tune2fs -o journal_data_writeback /dev/sdXY
~~~

#### Réduire le nombres de commits

Le commit est la période à laquelle le kernel va effectuer l'écriture du journal.

Dans le fstab, la valeur par défaut est de 4 secondes.

~~~
commit=60
~~~

#### Désactiver la journalisation

**Pour ext4 seulement :**

~~~
# tune2fs -O ^has_journal /dev/sdXY
# e2fsck -f /dev/sdXY
~~~

**Pour ext3, désactiver la journalisation revient à monter le FS en ext2.**

### Redimensionner/Resize une partition

**Faites un backup de la table de partition avec sfdisk.**

Selon la position de votre partition (espace libre avant ou après), plusieurs cas.

#### Agrandir une partition

Supprimer la avec `cfdisk`, puis la recréer avec sa nouvelle taille.
Ensuite il faut faire un `fsck`, et lancer l'utilitaire `resize2fs`.

~~~
# cfdisk /dev/sdxY
# e2fsck -f /dev/sdxY
# resize2fs /dev/sdxY
~~~

On peut avoir une erreur de taille de table de partition ou si la taille de l'espace libre n'est pas reconnue dans *cfdisk* ou *cgdisk* il faut réparer la table de partition avec :

~~~
# sgdisk -e /dev/sdx
~~~

Puis redémarrer la machine. 

#### Rétrécir une partition

Pour rétrécir une partition, il faut d'abord rétrécir son système de fichier, avant de la supprimer puis de la recréer avec sa nouvelle taille.

~~~
# resize2fs dev/sdxY XG
# cfdisk /dev/sdxY
# resize2fs dev/sdxY
# e2fsck -f /dev/sdxY
~~~

### Vérification du FS

Lister les valeurs du superblock du système de fichier

~~~
# tune2fs -l /dev/sdxY
~~~

`fsck` sera exécuté au prochain reboot lorsque les valeurs `Maximum mount count` et `Next check after` seront atteintes. Pour modifier ces seuils on exécutera :

~~~
# tune2fs -c <maximum_mount_count> -i <interval_between_check> /dev/sdxY
~~~

### Avoir des infos sur une partition

~~~
# dumpe2fs -h /dev/sda1 | less
~~~

L'option `-h` permettant d'avoir rapidement les infos importantes.

### Ajuster la place réservée

Pour voir le nombre de blocs réservés :

~~~
# tune2fs -l /dev/sda1 | grep -E "(Reserved block count|Block count)"
~~~

Pour connaître le pourcentage de blocs réservés, il faut faire le calcul à la main :

~~~
mount_point="/home"
dev=$(findmnt --noheadings "$mount_point" | head -n1 | awk '{ print $2 }')
blocs=$(tune2fs -l "$dev" | grep "Block count" | cut -d':' -f2 | xargs)
reserved_blocs=$(tune2fs -l "$dev" | grep "Reserved block count" | cut -d':' -f2 | xargs)
LC_NUMERIC="en_US.UTF-8" printf "Reserved blocs: %.0f%%\n" $(echo "scale=2; 100 * $reserved_blocs / $blocs" | bc)
~~~

Réduire à 1% le nombre de blocs réservé sur une partition ext3/4 :

~~~
# tune2fs -m 1 /dev/sda1
Setting reserved blocks percentage to 1% (328560 blocks)
~~~



### FSCK au démarrage

Un éventuel fsck au démarrage s'active dans la dernière colonne du fstab (mettre `0` pour le désactiver).

Par défaut, un "check" est programmé tous les 6 mois ou à partir d'environ 30 montages sans vérification.
Ces paramètres peuvent bien sûr être changé via :

~~~
# tune2fs -c 140 -i 1000d /dev/sda1
tune2fs 1.42.12 (29-Aug-2014)
Setting maximal mount count to 140
Setting interval between checks to 86400000 seconds
~~~

Cela évite des reboots trop longs dès que l'on dépasse 6 mois d'uptime.
En complément, on pourra utiliser un script avertissant qu'un fsck est conseillé…

Si on veut désactiver le `fsck` de manière ponctuelle, il faut dans le menu de Grub, rajouter aux [paramétres du noyau](TipsKernel#argmuents-de-la-ligne-de-commande-du-noyau-linux) [`fsck.mode=skip`](https://manned.org/man/debian-bookworm/systemd-fsck@.service.8#head4).

Aux dernières nouvelles, la technique de désactiver le `fsck` en créant le fichier `/fastboot` fonctionne toujours :

~~~
# touch /fastboot
~~~

> Note 1 : le fichier /fastboot sera supprimé au prochain démarrage

> Note 2 : attention, cette technique est [dépréciée depuis 2010](https://github.com/systemd/systemd/commit/f276a41821fcd2b7e4968edef46ba3bd99706ecf) 

On a le même principe pour forcer un `fsck` :

~~~
# touch /forcefsck
~~~


### Copier le contenu d'un dossier à la création du système de fichier

Il est possible de copier directement le contenu d'un répertoire à la création du système de fichier (ce contenu devient alors le contenu à la racine du système de fichier créé) avec l'argument `-d` de `mke2fs` (`mkft -t ext4`).

~~~
# mkfs -t ext4 -d /home /dev/vdb
~~~

> Cette commande ne nécessite que les droits de lecture sur le contenu du dossier et les droits d'écriture sur le disque. Le "disque" peut être un simple fichier (il n'y a pas besoin de `losetup` dans ce cas).

## Benchmark

Se reporter à [HowtoBenchmarks](https://wiki.evolix.org/HowtoBenchmarks).

## FAQ

Erreur `too many links` ?

Cela signifie que vous avez plus de 2^15^ (~= 32k) répertoires dans un répertoire.
Il faut envisager de structurer le répertoire différemment pour obtenir une hiérarchie plus profonde, avec moins d'éléments dans chaque répertoire.

Chercher le répertoire contenant le plus de répertoires = + de inodes/inode : [TipsShell#lister-les-répertoires-ayant-le-plus-de-fichiers-max-inode]() 
