---
title: Howto Tailscale
categories: vpn
...

* Documentation : https://tailscale.com/kb

[Tailscale](https://tailscale.com/) est une solution de VPN baser sur WireGuard

## Installation

Pour l'installer sur Debian, il faut ajouter les dépôts Tailscale :

~~~
# curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.noarmor.gpg | tee /etc/apt/keyrings/tailscale-archive-keyring.gpg >/dev/null
# chmod 644 /etc/apt/keyrings/tailscale-archive-keyring.gpg
# curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list
~~~

Puis on installe le paquet `tailscale` comme ceci :

~~~
# apt update
# apt install tailscale
~~~

Il faut ensuite configuré Tailscale depuis sont interface d'admin, dans votre compte Tailscale, puis activé le démarrage sur la machine du deamon comme ceci :

~~~
# tailscale up
~~~

On doit voir l'interface réseau `tailscale0` UP sur la machine.

## Fonctionnement de Tailscale

Voici la [documentation](https://tailscale.com/blog/how-tailscale-works) sur le fonctionnement de Tailscale.
