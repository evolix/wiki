---
title: Howto Openfire
...


**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto openfire

Installer Java :

~~~
# aptitude install default-jre-headless
~~~

Télécharger le paquet Debian sur <http://www.igniterealtime.org/downloads/index.jsp>

~~~
# dpkg -i openfire_3.9.1_all.deb
~~~

On peut ensuite y accéder sur le port 9090 : <http://server.example.com:9090>

Attention, lors de la configuration, il faut bien mettre le FQDN sous peine d'avoir des erreurs java.lang.NullPointerException
