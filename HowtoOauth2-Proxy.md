---
categories: network web
title: Howto Oauth2-Proxy
...

* Documentation : <https://oauth2-proxy.github.io/oauth2-proxy/>

[Oauth2-Proxy](https://github.com/oauth2-proxy/oauth2-proxy) est un proxy open-source écrit en go, permettant l'authentification avec divers *providers* oauth2 (Google, Keycloak, GitHub, etc... ) pour protéger des services qui ne supporteraient pas ce standard.

Il est généralement utilisé avec un autre load-balancer / reverse-proxy en amont (Nginx, Traefik, Caddy ...)  mais il peut être utilisé seul aussi.

<img src="./oauth2-architecture.svg">

## Installation

On peut installer oauth2-proxy de plusieurs manières selon les besoins.
Ici, on montre comment faire en utilisant **directement le binaire** et un **utilisateur dédié** ici `jdoe`, mais on pourrait aisément construire le binaire avec go, utiliser docker, etc (voir la [doc officielle](https://oauth2-proxy.github.io/oauth2-proxy/installation/) ).
Oauth2-proxy sera ensuite piloté par une unité systemd.

* Récupérer le binaire de la version souhaitée ( [liste releases](https://github.com/oauth2-proxy/oauth2-proxy/releases) )

~~~{.bash}
$ version=v7.6.0
$ wget https://github.com/oauth2-proxy/oauth2-proxy/releases/download/${version}/oauth2-proxy-${version}.linux-amd64.tar.gz
$ tar -xvf oauth2-proxy-${version}.linux-amd64.tar.gz
$ mkdir -p bin
$ mv oauth2-proxy-${version}.linux-amd64/oauth2-proxy bin/
~~~

* Vérifier qu'on peut bien le lancer avec `$ oauth2-proxy --version`

* Configurer l'unité systemd

~~~{.bash}
# vim /etc/systemd/system/oauth2-proxy.service

[...]
# Systemd service file for oauth2-proxy daemon
#
# Date: Feb 9, 2016
# Author: Srdjan Grubor <sgnn7@sgnn7.org>

[Unit]
Description=oauth2-proxy daemon service
After=network.target

[Service]
# www-data group and user need to be created before using these lines
User=jdoe
Group=jdoe

ExecStart=/home/jdoe/bin/oauth2-proxy --config=/home/jdoe/oauth2-proxy.cfg
ExecReload=/bin/kill -HUP $MAINPID

KillMode=process
Restart=always

[Install]
WantedBy=multi-user.target
[...]

# systemctl enable --now oauth2-proxy.service
# systemctl status oauth2-proxy.service
~~~

## Configuration

On peut configurer oauth2-proxy de plusieurs manières, avec des argument au lancement, avec un fichier de configuration, ou des variables d'environnements. 

Ici on montre comment faire avec un fichier de configuration, mais on pourrait utiliser une autre maniere en ajustant les noms ( voir [configuration/overview](https://oauth2-proxy.github.io/oauth2-proxy/configuration/overview) )

On montre différentes configuration pour le fichier `oauth2-proxy.cfg` du home de **jdoe**

### Pour un provider oidc standard ( fonctionne avec keycloak )


~~~{.toml}
provider = "oidc"

provider_display_name = "fooservice"
skip_provider_button = "true"

set_xauthrequest = "true"
client_id = "bar"
client_secret = "IE7XN9gUIqZlEl2YXZ1Q8FTc3gP19lNf"
redirect_url = "https://fooservice.exemple.com/oauth2/callback"
oidc_issuer_url= "https://sso.exemple.com/realms/foocorp"
email_domains = [ "exemple.com" ]
insecure_oidc_allow_unverified_email = "true"
code_challenge_method = "S256"
cookie_secure = "false"
cookie_secret = "ED3fzHHloWnbEMGZbJx0vlFmeRspFRWq"

# Optionnel ( configurer la déconnection )
backend_logout_url = "https://sso.exemple.com/realms/foocorp/protocol/openid-connect/logout"
whitelist_domains = "sso.exemple.eu"
~~~

* `skip_provider_button` permet de ne pas avoir d'être redirigé directement vers le providers sans demande de confirmation via un bouton.
* `insecure_oidc_allow_unverified_email` permet de ne pas forcer à ce que les email du provider ait été vérifiés ( par défaut à **false** )
* `email_domains` les domaines pour lequels un email de connections sera accepté, ici `jdoe@exemple.com` serait accepté.
* `cookie_secret` utilisé pour le chiffrement des cookies notamment, vous pouvez en générer une avec `head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32`
* `set_xauthrequest` permet de passer des informations (via des header) relative à l'utilisateur connecté, comme l'email, au reverse-proxy et par conséquent, au service protégé.

On peut optionnelement configurer le logout, il faut que dans l'application on puisse rajouter un bouton de déconnection qui redirige vers `/oauth2/sign_out?rd=$(url)` avec `$(url)` le `backend_logout_url` encodé en Encodage-pourcent (URL encoding). 

Alternativement, on peut aussi indiquer `backend_logout_url` dans le header `X-Auth-Request-Redirect` de la réponse.

Il faut alors aussi configurer dans oauth2-proxy :

* `backend_logout_url` permet d'indiquer vers quel URL oauth2-proxy doit rediriger **aprés** avoir reçu une requête sur `/oauth2/sign_out` , pour permettre à l'utilisateur de se déconnecter du provider oidc.
* `whitelist_domains` contient les domaines (séparé par des virgules, wildcard `*.exemple.eu accepté`) vers lesquels une redirection est autorisées **pour un utilisateur authentifié**.

> Pour que le logout fonctionne, `whitelist_domains` doit contenir le domaine correspondant à celui dans l'url `backend_logout_url`.

### Configuration de NGinx

Pour utiliser oauth2-proxy avec Nginx, il faut utiliser le module `ngx_http_auth_request_module` et la directive `auth_request` qui permet d'autoriser l'accés à un client par rapport au code http d'un sous requête :

* <http://nginx.org/en/docs/http/ngx_http_auth_request_module.html>

exemple de configuration :

~~~{.bash}

location /oauth2/ {
          proxy_pass       http://127.0.0.1:4180;
          proxy_set_header Host                    $host;
          proxy_set_header X-Real-IP               $remote_addr;
          proxy_set_header X-Auth-Request-Redirect $request_uri;
          # or, if you are handling multiple domains:
          # proxy_set_header X-Auth-Request-Redirect $scheme://$host$request_uri;
        }
        location = /oauth2/auth {
          proxy_pass       http://127.0.0.1:4180;
          proxy_set_header Host             $host;
          proxy_set_header X-Real-IP        $remote_addr;
          proxy_set_header X-Forwarded-Uri  $request_uri;
          # nginx auth_request includes headers but not body
          proxy_set_header Content-Length   "";
          proxy_pass_request_body           off;
        }

        location / {
            auth_request /oauth2/auth;
            error_page 401 = /oauth2/sign_in;

            # pass information via X-User and X-Email headers to backend,
            # requires running with --set-xauthrequest flag
            auth_request_set $user   $upstream_http_x_auth_request_user;
            auth_request_set $email  $upstream_http_x_auth_request_email;
            proxy_set_header X-User  $user;
            proxy_set_header X-Email $email;
            proxy_set_header REMOTE_USER $email;
[...]
~~~

### Configuration de Traefik
