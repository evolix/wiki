**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Live-CD

Un Live-CD est un CD-ROM ou DVD-ROM contenant un système (souvent GNU/Linux) pouvant
être démarré sans toucher au(x) système(s) déjà installé(s) sur l'ordinateur.

## Graver un Live-CD

1. Il faut télécharger un fichier .iso

Par exemple :

DEBIAN : [<http://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-8.5.0-amd64-standard.iso>]

Kubuntu : [<http://cdimage.ubuntu.com/kubuntu/releases/16.04/release/kubuntu-16.04-desktop-amd64.iso>]

2. Il faut ensuite le graver :

ATTENTION, il ne faut pas graver un CD-ROM contenant le fichier .iso MAIS le fichier .iso est un format spécial
contenant des données préparées pour être gravée ! Sous Windows, il faut donc "Ouvrir" le fichier .iso avec un
logiciel de gravure (du type Nero Burning Room) et non pas créer un nouveau CD-ROM contenant le .iso.
Une fois gravé, on obtiendra un CD-ROM contenant plusieurs répertoires/fichiers, et non pas un CD-ROM
contenant le fichier .iso

Sous Windows, il faut utiliser votre logiciel de gravure. Il doit être compatible avec les fichiers .ISO (vérifier
sur votre moteur de recherche préféré). Faites ensuite un clic droit sur le fichier .iso puis "Ouvrir avec" et choisissez
votre logiciel de gravure. Puis gravez !

Sous Linux :

~~~
$ wodim debian-live-6.0.5-amd64-standard.iso
~~~

Voir aussi : [<http://trac.evolix.net/infogerance/wiki/HowtoDebian/Download>]

## Démarrer sur un Live-CD

Insérer le CD-ROM dans le lecteur de l'ordinateur, puis démarrer la machine.

Si cela ne démarre pas tout seul sur le Live-CD, vous devez soit :

* Presser une touche permettant de choisir le périphérique d'amorçage

* Configurer les _Boot Options_ dans le BIOS pour que le démarrage via CD-ROM se fasse avant le démarrage sur le disque interne.

## Et une fois démarré ?

Vous devez obtenir un accès root sans trop de difficultés (souvent un terminal avec une invite de commande se terminant par #) :

~~~
#
~~~

Actions typiques qui peuvent être utiles :

### Ouvrir un accès distant via SSH

Vous devez faire la configuration IP, positionner un mot de passe et démarrer le service SSH :

~~~
# ifconfig eth0 IP/masque
# passwd
# /etc/init.d/ssh start
~~~

### restaurer LILO ou GRUB

Vous devez vous "chrooter" sur le système de base (changer les noms de partitions selon votre ordianteur) :

~~~
# mkdir /chroot
# mount /dev/sda2 /chroot/
# mount /dev/sda1 /chroot/boot
# mount /dev/sda5 /chroot/usr
# mount /dev/sda3 /chroot/var
# mount -t proc /proc /chroot/proc
# mount -o bind /dev /chroot/dev
# mount -o bind /dev/pts /chroot/dev/pts
# mount -o bind /sys /chroot/sys
# chroot /chroot /bin/bash
~~~

Remarque : à noter la variante en cas de partition chiffrée ([<http://trac.evolix.net/infogerance/wiki/HowtoChiffrementData>])

~~~
# mount /dev/sda2 /chroot/
~~~

devient

~~~
# cryptsetup luksOpen /dev/sda2 sda2crypt
# mount /dev/mapper/sda2crypt /chroot/
~~~


Vous voilà donc plus ou moins comme si vous étiez sur votre Linux local. Vous pouvez ensuite lancer une restauration
de LILO ou GRUB qui va se réinstaller dans le MBR de votre disque dur :

~~~
# lilo -v
~~~

ou

~~~
# grub-install /dev/sda
# update-grub2
~~~

Il est aussi possible d'utiliser d'autre commande, notamment :

~~~
# update-initramfs -k all -u
# dpkg-reconfigure linux-image-6.1.0-26-amd64
~~~

MAIS attention, cela va regénérer l'initramfs et cela risque de casser des choses car il y a de l'auto-détection : par exemple `/etc/crypttab` qui risque de casser et donc le système ne bootera plus du tout si la partition ROOT est chiffrée...

## FAQ

### J'ai un multiboot UEFI et je veux restaurer GRUB car je n'arrive plus à démarrer sous Linux

Vous avez probablement écraser votre partition UEFI avec une mise-à-jour Windows.
Via Live-CD vous devez simplement monter en RW (il faut parfois le forcer) votre partition UEFI dans /boot/efi avant de faire votre grub-install.
Évidemment, vous devez avoir les paquets grub-efi installés.
