# Kexec

Kexec permet de charger un nouveau noyau en ne redémarrant pas la machine au sens bypass du POST BIOS et du bootloader.
Documentation : <https://manpages.debian.org/buster/kexec-tools/kexec.8.en.html> 

## Installation

~~~
# apt install kexec-tools
~~~

Une fois le paquet installé, kexec n'est pas utilisable de suite. Idéalement il faut redémarrer pour que celui-ci soit pris en charge automatiquement. Ensuite il suffira de taper la commande `reboot` pour que kexec prenne le relais et charge le nouveau noyau.

> Note : Cela ne fonctionne que si vous utilisez systemd-boot (et en mode UEFI).

Il existe une astuce pour le faire fonctionner autrement mais nous ne la conseillons pas, voir [cet article](https://patrakov.blogspot.com/2019/06/kexec-on-modern-distributions.html).