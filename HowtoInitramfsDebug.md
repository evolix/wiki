---
categories: kernel boot
title: Howto Initramfs debug
...

* Documentation : <https://wiki.debian.org/InitramfsDebug>

## UUID

Normalement la partition racine est montée dans `/root` et l'on peut modifier le *fstab* avec `vi /root/etc/fstab` et corriger les UUID ou commenter les lignes problématiques pour les corriger plus tard. 

## Disques LVM

Si les partitions LVM ne sont pas détectées, une solution peut être de forcer leur activation avec `vgchange` :

~~~
(initramfs) lvm
lvm> vgchange -ay
^D
^D
~~~

## Souci LUKS

<https://wiki.debian.org/CryptsetupDebug>
<https://wiki.evolix.org/HowtoLUKS>

On a pu constater lors de mise à jour noyau des soucis lors de la reconstruction de l'initramfs sur des portables Debian Jessie avec des partitions chiffrées.

~~~
Reading all physical volumes. This may take a while ...
No volume groups found
~~~

Il faut corriger `/etc/crypttab` (mettre à jour l'UUID) et `/etc/fstab`.


## Reconstruire l’initramfs

~~~
# update-initramfs -k VERSION -u
# update-initramfs -k all -u
~~~

## Réduire la taille de l’initramfs

Dans `/etc/initramfs-tools/initramfs.conf`, configurer

~~~
COMPRESS=xz
~~~

au lieu de la valeur `gzip` par défaut permet de gagner près de 30% de la taille.

Si ça ne suffit pas, utiliser

~~~
MODULES=dep
~~~

permet de gagner au moins 80% de taille supplémentaire (il faut bien sûr prendre garde lors d’un changement matériel).


## Regarder dans un initramfs

~~~
# lsinitramfs /boot/initrd.img-4.19.0-27-cloud-amd64

# mkdir /tmp/initramfs
# unmkinitramfs /boot/initrd.img-4.19.0-27-cloud-amd64 /tmp/initramfs/
~~~

## Créer un initramfs

~~~
# mkinitramfs
~~~

## Divers

~~~
Commandes disponibles :

ls, vi, cat...

Arboresence :

TODO 

/cryptroot/crypttab
etc.

Déchiffrer les partitions :

(initramfs) sh scripts/local-top/ORDER

Poursuivre le boot après la réparation :

(initramfs) quit
~~~


