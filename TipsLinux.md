## Comment vérifier l'umask d'un processus ?

~~~
$ gdb --pid=<PID du process>
(gdb) call/o umask(077)
$1 = 022
(gdb) call/o umask($1)
$2 = 077
~~~

On voit dans cet exemple que le umask de ce process est **022**

Note : attention, cela change l'umask du process à 077 pendant quelques secondes !

## Affinité CPU

« L'affinité » CPU permet d'isoler un processus (ou plusieurs) sur un choix de processeur. Par exemple, sur une machine à 8 cpu (ou cores), dire à un processus de pouvoir seulement utiliser le cpu 0 et 1. (C'est le noyau via son scheduler qui s'en chargera).  
On peut faire ceci avec taskset, par exemple :  

~~~
$ taskset -c 0,1 ls
~~~

Cela limitera le processus ls aux processeurs 0 et 1.

> **Note**: htop permet aussi de le faire à chaud avec la touche « a ».

## Ajouter un fichier de SWAP

Par exemple pour ajouter un fichier de SWAP nommé `swapfile3` de 4G dans /home. (Souvent la partition la plus grosse).

~~~
# cd /home
# dd if=/dev/zero of=./swapfile3 bs=1M count=4096
# OU # fallocate -l 4G ./swapfile3
# chown root: swapfile3
# chmod 0600 swapfile3
# mkswap -LSWAP3 ./swapfile3
# echo '/home/swapfile3 none swap sw 0 0' >> /etc/fstab
# swapon -a
### OU
# swapon /home/swapfile3
~~~
