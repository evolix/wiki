---
categories: network
title: Howto ARIN
...

En complément du [HowtoRIPE]() voici les démarches pour obtenir des adresses IP auprès de l'[ARIN (American Registry for Internet Numbers)](https://www.arin.net/).

## Comment obtenir un Org ID (Organization Identifier)

* Créer un *compte ARIN Online* sur https://www.arin.net/public/register_1.xhtml
* Créer un compte POC (Point of Contact) qui vous donnera un Nic Handle du type **JD-ARIN** (données accessibles via *whois JD-ARIN*)
* Créer un Org ID, vous devez avoir une structure officielle résidant en Amérique du Nord

## Demander une première Allocation

On peut demander une première allocation d’adresses IPv4 et IPv6. Une allocation est une sorte de réserve d’adresses, dans laquelle on pourra assigner des blocs à des clients ou pour son propre usage.

### IPv6

En IPv6, vous devrez répondre aux questions suivantes :

* Longueur de préfixe souhaité (/32 par défaut)
* Network Name
* Numéro d'AS
* Information sur la personne qui va [attester la demande](https://www.arin.net/resources/agreements/officer_attest.html)
* Description de *IPv6 products and services your organization expects to offer during the next 5 years* (moins de 4000 caractères)
* Date de déploiement d'IPv6
* Nombre de clients IPv4
* Description du plan d'assignement : *In the next 1, 2, and 5 years, estimate how much space you plan to assign to customers? (e.g. how many /64s, /56s, /48s, and other prefix sizes you will be assigning?)* (moins de 4000 caractères)
* Informations additionnelles pour appuyer votre demande (moins de 4000 caractères)

### IPv4

TODO