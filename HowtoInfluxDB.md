---
categories: monitoring
title: Howto InfluxDB
...

* Documentation : <https://docs.influxdata.com/influxdb>

[InfluxDB](https://www.influxdata.com/time-series-platform/influxdb/) est une base de données de séries temporelles. Nous l'utilisons en combinaison avec [collectd](HowtoCollectd) et [Grafana](HowtoGrafana).


## Installation

Nous utilisons les dépôts fournis par InfluxData pour avoir une version stable plus récente.

~~~
# wget -q https://repos.influxdata.com/influxdata-archive_compat.key
# echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor > /etc/apt/keyrings/influxdata-archive_compat.gpg
# rm influxdata-archive_compat.key
# chmod 644 /etc/apt/keyrings/influxdata-archive_compat.gpg
# echo 'deb [signed-by=/etc/apt/keyrings/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' > /etc/apt/sources.list.d/influxdb.list
# apt update
# apt install influxdb

# systemctl edit influxdb.service
[Service]
ExecStartPre=-/usr/bin/install --owner influxdb --directory /var/lib/influxdb/

# systemctl start influxdb.service

$ influx -version
InfluxDB shell version: 1.8.10

$ systemctl status influxdb
● influxdb.service - InfluxDB is an open-source, distributed, time series database
     Loaded: loaded (/lib/systemd/system/influxdb.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-18 15:41:32 CEST; 30s ago
       Docs: https://docs.influxdata.com/influxdb/
    Process: 78443 ExecStart=/usr/lib/influxdb/scripts/influxd-systemd-start.sh (code=exited, status=0/SUCCESS)
   Main PID: 78444 (influxd)
      Tasks: 10 (limit: 2340)
     Memory: 12.0M
        CPU: 119ms
     CGroup: /system.slice/influxdb.service
             └─78444 /usr/bin/influxd -config /etc/influxdb/influxdb.conf
~~~

## Configuration

Le fichier de configuration se trouve dans `/etc/influxdb/influxdb.conf`.

On peut désactiver la collection de statistiques internes à InfluxDB, et ne faire écouter le serveur HTTP qu'en local. Ce dernier permet d'afficher des graphes à des fins de tests uniquement.

~~~
[monitor]
  store-enabled = false
[http]
   bind-address = "127.0.0.1:8086"
~~~

### Authentification

* Documentation : <https://docs.influxdata.com/influxdb/v1.8/administration/authentication_and_authorization/>

L'authentification ici permettra à ce que la base de données ne soit pas accessible à n'importe qui, d'autant plus que n'importe quel utilisateur Unix peut s'y connecter.

On commence par créer un utilisateur avec les droits administrateur :

~~~
# influx
Connected to http://localhost:8086 version 1.8.10
InfluxDB shell version: 1.8.10
> CREATE USER <username> WITH PASSWORD '<password>' WITH ALL PRIVILEGES
> SHOW USERS
user       admin
----       -----
<username> true
~~~

On modifie la configuration dans `/etc/influxdb/influxdb.conf` pour activer l'authentification :

~~~
[http]
  auth-enabled = true
~~~

Puis on redémarre influxDB :

~~~
# systemctl restart influxdb
~~~

On s'authentifiera ensuite via la commande `auth` :

~~~
# influx
Connected to http://localhost:8086 version 1.8.10
InfluxDB shell version: 1.8.10
> auth
username: root
password: 
~~~

Ou, avec la commande `influx`, en laissant l'arguement `-password` vide mais bien présent pour qu'il soit demandé à l'utilisateur :

~~~
# influx -username root -password ''
password: 
Connected to http://localhost:8086 version 1.8.10
InfluxDB shell version: 1.8.10
> 
~~~

Attention, si le mot de passe donné n'est pas le bon, aucune erreur n'est affichée, mais les commandes ayant besoin d'être authentifiées seront en erreur.

Si on utilise [Grafana](HowtoGrafana#ajouter-une-source), il faut ensuite le configurer pour s'authentifier afin qu'il puisse lire les données.

### Collectd

Pour récupérer les métriques envoyées par [collectd](/HowtoCollectd), il suffit d'activer le listener :

~~~
[[collectd]]
  enabled = true
  bind-address = ":25826"
  database = "collectd"
  typesdb = "/usr/share/collectd"
~~~

Puis télécharger le fichier, si collectd n'est pas installé sur la machine :

~~~
# mount -o remount,rw /usr
# mkdir /usr/local/share/collectd/
# wget https://raw.githubusercontent.com/collectd/collectd/master/src/types.db -O /usr/local/share/collectd/
# chmod 755 /usr/local/share/collectd/
# chmod 644 /usr/local/share/collectd/types.db
# mount -o remount /usr
~~~

#### Authentification

Ici, l'authentification concerne l'écriture des données par le collecteur dans la base de données.

On modifie la configuration `/etc/influxdb/influxdb.conf` :

~~~
[[collectd]]
  […]
  security-level = "sign"
  auth-file = "</path/to/auth_file>"
~~~

Le `security-level` peut être `none` (aucune authentification), `sign` (données signées avec HMAC-SHA-256), ou `encrypt` (données chiffrées avec AES-256, et intégrité assurée avec SHA-1).

Le fichier `auth-file` doit contenir le ou les utilisateur(s) avec la syntaxe suivante :

~~~
user0: foo
user1: bar
~~~

Attention aux droits de ce fichier : il doit n'être lisible et n'appartenir qu'à l'utilisateur `influxdb`.

Il faut ensuite configurer [collectd](/HowtoCollectd#plugin-network) pour qu'il s'authentifie lorsqu'il envoie ses données.

## Administration

### Emplacement des données

Par défaut les données sont stockées dans `/var/lib/influxdb/`. Pour modifier l'emplacement dans `/opt/influxdb/`, on peut procéder ainsi :

Éditer `/etc/influxdb/influxdb.conf` :

~~~
[meta]
  # Where the metadata/raft database is stored
#  dir = "/var/lib/influxdb/meta"
  dir = "/opt/influxdb/meta"

[data]
  # The directory where the TSM storage engine stores TSM files.
#  dir = "/var/lib/influxdb/data"
  dir = "/opt/influxdb/data"

  # The directory where the TSM storage engine stores WAL files.
#  wal-dir = "/var/lib/influxdb/wal"
  wal-dir = "/opt/influxdb/wal"
~~~

Puis déplacer les anciennes données :

~~~
# systemctl stop influxdb.service
# cp -a  /var/lib/influxdb/ /opt/
# systemctl start influxdb.service
~~~

Si on veut faire un lien symbolique dans `/home/influxdb/`, on peut procéder ainsi :

~~~
# systemctl stop influxdb.service
# mkdir /home/influxdb
# chown influxdb: /home/influxdb
# chmod 755 /home/influxdb/
# mv /var/lib/influxdb/* /home/influxdb/
# rmdir /var/lib/influxdb/
# ln -s /home/influxdb/ /var/lib/
# systemctl start influxdb.service
~~~

### Structure de la base

Il peut être intéressant de naviguer dans la base de données afin de comprendre comment sont stockées les données, et ainsi afficher des graphes avec plus de facilité :

~~~
# influx
Connected to http://localhost:8086 version 1.7.10
InfluxDB shell version: 1.7.10
> SHOW DATABASES
name: databases
name
----
collectd
> USE collectd
Using database collectd
> SHOW MEASUREMENTS
name: measurements
name
----
cpu_value
disk_value
load_longterm
load_midterm
load_shortterm
snmp_rx
snmp_tx
tcpconns_value
> SELECT * FROM snmp_rx,snmp_tx WHERE time > now()-1h LIMIT 5
name: snmp_rx
time                host   type       type_instance value
----                ----   ----       ------------- -----
1527610980434511472 Switch if_packets machine A     4241766500
1527611039463879536 Switch if_octets  machine B     9414616763688
1527611039463903506 Switch if_octets  machine C     59780133577
1527611039463953500 Switch if_octets  machine D     109232798624
1527611039556165882 Switch if_packets machine B     7925165702

name: snmp_tx
time                host   type       type_instance value
----                ----   ----       ------------- -----
1527610980434511472 Switch if_packets machine A     11434830808
1527611039463879536 Switch if_octets  machine B     174654547558
1527611039463903506 Switch if_octets  machine C     135392659943
1527611039463953500 Switch if_octets  machine D     9179809568001
1527611039556165882 Switch if_packets machine B     396998647
~~~

### Supprimer des mesures

On peut vouloir supprimer une mesure particulière, par exemple dans le cas où une machine n'est plus sur le switch :

~~~
> SHOW SERIES FROM snmp_rx,snmp_tx WHERE type_instance='machine A'
key
---
snmp_rx,host=Switch,type=if_octets,type_instance=machine\ A
snmp_rx,host=Switch,type=if_packets,type_instance=machine\ A
snmp_tx,host=Switch,type=if_octets,type_instance=machine\ A
snmp_tx,host=Switch,type=if_packets,type_instance=machine\ A
> DROP SERIES FROM snmp_rx,snmp_tx WHERE type_instance='machine A'
~~~

### Rétention

Par défaut, les données sont gardées sur une durée illimitée, et l'intervalle de vérification de la rétention est de 30 minutes.

Si on ne souhaite pas utiliser la rétention, on peut la désactiver :

~~~
[retention]
   # Determines whether retention policy enforcement enabled.
   enabled = false
~~~

Si au contraire, on veut avoir une rétention d'un an, il faut modifier la politique :

~~~
# influx
> use collectd
> SHOW RETENTION POLICIES
name    duration shardGroupDuration replicaN default
----    -------- ------------------ -------- -------
autogen 0s       168h0m0s           1        true

> ALTER RETENTION POLICY "autogen" ON "collectd" DURATION 52w REPLICATION 1 DEFAULT
> SHOW RETENTION POLICIES
name     duration  shardGroupDuration replicaN default
----     --------  ------------------ -------- -------
autogen  8736h0m0s 168h0m0s           1        true
~~~

Et on peut augmenter l'intervalle de vérification de la rétention :

~~~
[retention]
   # Determines whether retention policy enforcement enabled.
   enabled = true

  # The interval of time when retention policy enforcement checks run.
  check-interval = "24h"
~~~

### Monitoring

Le [check NRPE](HowtoIcinga) suivant peut être mis en place :

~~~
command[check_influxdb]=/usr/lib/nagios/plugins/check_http -I 127.0.0.1 -u /health -p 8086 -r '"status":"pass"'
~~~

### Continuous Query

Les "continuous queries" (ou requêtes continues) sont des requêtes qui sont exécutées automatiquement et périodiquement sur les données en temps réel, pour ensuite en stocker le résultat.

Par exemple, pour créer des mesures "switch_bps_tx" et "switch_bps_rx" avec le champ "bitspersec" où est converti le nombre total d'octets transmis par une interface et récupéré toutes les minutes, en une mesure en bits par secondes, toutes les minutes également :

~~~
# influx
> CREATE CONTINUOUS QUERY "Interface-Traffic-Conversion_TX" ON collectd BEGIN SELECT non_negative_derivative(mean(value), 1s) * 8 AS bitspersec INTO collectd.autogen.switch_bps_tx FROM collectd.autogen.snmp_tx WHERE type = 'if_octets' GROUP BY host, type_instance, time(1m) END
> CREATE CONTINUOUS QUERY "Interface-Traffic-Conversion_RX" ON collectd BEGIN SELECT non_negative_derivative(mean(value), 1s) * 8 AS bitspersec INTO collectd.autogen.switch_bps_rx FROM collectd.autogen.snmp_rx WHERE type = 'if_octets' GROUP BY host, type_instance, time(1m) END

> SHOW CONTINUOUS QUERIES
name: collectd
name                            query
----                            -----
Interface-Traffic-Conversion_TX CREATE CONTINUOUS QUERY "Interface-Traffic-Conversion_TX" ON collectd BEGIN SELECT non_negative_derivative(mean(value), 1s) * 8 AS bitspersec INTO collectd.autogen.switch_bps_tx FROM collectd.autogen.snmp_tx WHERE type = 'if_octets' GROUP BY host, type_instance, time(1m) END
Interface-Traffic-Conversion_RX CREATE CONTINUOUS QUERY "Interface-Traffic-Conversion_RX" ON collectd BEGIN SELECT non_negative_derivative(mean(value), 1s) * 8 AS bitspersec INTO collectd.autogen.switch_bps_rx FROM collectd.autogen.snmp_rx WHERE type = 'if_octets' GROUP BY host, type_instance, time(1m) END
~~~

### Backup et restauration

#### Générer un backup

Attention : la documentation mentionne une backup intégral de toutes les bases de données si aucune plage temporelle n'est spécifiée, cependant nous avons pu constater l'absence de certaines dates après une telle procédure. C'est pourquoi nous recommandons de spécifier des plages temporelles de début **et** de fin spécifiques.

~~~
# influxd backup -portable <output_file>
~~~

Pour spécifier une date de début jusqu'à ce jour (données du jour incluses) : 

~~~
# influxd backup -portable -since <date> <output_file>
# influxd backup -portable -start <date> <output_file>
~~~

**N.B. :** la date est au format iso-8601 : `2021-01-16T23:09:44+02:00`

Pour spécifier une date de début et de fin :

~~~
# influxd backup -portable -start <date> -end <date> <output_file>
~~~

#### Restaurer un backup

~~~
# influxd restore -portable <input_file>
~~~
