---
categories: ruby
title: Howto Rbenv
...

* Documentation : <https://github.com/rbenv/rbenv>
* Rôle Ansible : <https://gitea.evolix.org/evolix/ansible-roles/src/branch/stable/rbenv>

Rbenv est un ensemble de scripts permettant de gérer de multiples environnements d'exécution de Ruby.

## Installation

Rbenv s'utilise le plus souvent au sein d'un compte utilisateur non administrateur et sans besoin de privilèges, mais quelques dépendances doivent être satisfaites pour pouvoir l'installer et l'utiliser.

~~~
# apt install build-essential git libcurl4-openssl-dev libffi-dev libreadline-dev libssl-dev libxml2-dev libxslt1-dev zlib1g-dev pkg-config
~~~

On bascule ensuite dans l'environnement de l'utilisateur.

### Minimum

On clone le dépôt Git de Rbenv :

~~~
$ git clone https://github.com/rbenv/rbenv.git ~/.rbenv
~~~

On configure notre shell (`~/.profile`) pour initialiser Rbenv, en ajoutant ces 2 lignes :

~~~{.bash}
export PATH="~/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
~~~

### Optionnel

On peut installer quelques plugins :

~~~
$ mkdir ~/.rbenv/plugins
$ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
$ git clone https://github.com/rbenv/rbenv-installer.git ~/.rbenv/plugins/rbenv-installer
$ git clone https://github.com/rkh/rbenv-update.git ~/.rbenv/plugins/rbenv-update
$ git clone https://github.com/rbenv/rbenv-vars.git ~/.rbenv/plugins/rbenv-vars
$ git clone https://github.com/rbenv/rbenv-default-gems.git ~/.rbenv/plugins/rbenv-default-gems
$ git clone https://github.com/rkh/rbenv-whatis.git ~/.rbenv/plugins/rbenv-whatis
$ git clone https://github.com/rkh/rbenv-use.git ~/.rbenv/plugins/rbenv-use
~~~

On peut configurer Rubygems dans le fichier `~/.gemrc` pour limiter la quantité de documentation installée pour les gems :

~~~
---
:sources:
- https://rubygems.org
install: --no-document
update: --no-document
gem: --no-document
~~~

## Utilisation

~~~
$ rbenv help
Usage: rbenv <command> [<args>]

Some useful rbenv commands are:
   commands    List all available rbenv commands
   local       Set or show the local application-specific Ruby version
   global      Set or show the global Ruby version
   shell       Set or show the shell-specific Ruby version
   install     Install a Ruby version using ruby-build
   uninstall   Uninstall a specific Ruby version
   rehash      Rehash rbenv shims (run this after installing executables)
   version     Show the current Ruby version and its origin
   versions    List all Ruby versions available to rbenv
   which       Display the full path to an executable
   whence      List all Ruby versions that contain the given executable

See `rbenv help <command>' for information on a specific command.
For full documentation, see: https://github.com/rbenv/rbenv#readme
~~~

### Installer une version de Ruby

La commande `rbenv install --list` vous indiquera toutes les versions de Ruby connues (par le plugin "ruby-build").

On installe la version voulue :

~~~
$ rbenv install <version>`
~~~

Notez que la partition sur laquelle se trouve Rbenv doit avoir les droits d'exécution. Ça n'est pas le cas par défaut pour `/home` sur les installations Evolix.

De même, si votre dossier temporaire par défaut n'est pas accessible en exécution, vous pouvez en créer un et le spécifier ainsi :

~~~
$ mkdir ~/tmp
$ TMPDIR=~/tmp rbenv install <version>`
~~~

Pour avoir la liste des versions installées :

~~~
rbenv versions
* system (set by /home/foo/.rbenv/version)
  2.6.4
  2.6.5
  2.7.0
~~~

### Changer de version de Ruby

* `rbenv global <version>` change la version par défaut pour votre utilisateur.
* `rbenv local <version>` change la version pour le dossier courant et ses descendants.
* `rbenv shell <version>` change pour la session courante.

Il est aussi possible de créer un fichier `.ruby-version` à n'importe quel emplacement et Rbenv en tiendra compte pour cet emplacement et ses descendants (c'est ce que fait `rbenv local <version>`).

### Shell non-interactif

L'installation par défaut de Rbenv ne l'initialise que pour les sessions "login" (via le fichier `~/.profile`).

Si on veut utiliser Rbenv dans des sessions "non-login" (et "non-interactives"), il faut forcer le PATH :

~~~
$ ssh user@my-server 'PATH=$HOME/.rbenv/shims:$PATH /path/to/ruby/script'
~~~

Le site de Rbenv dispose d'ailleurs d'une [page dédiée à l'explication de l'initialisation d'un shell](https://github.com/rbenv/rbenv/wiki/Unix-shell-initialization).

## Mise à jour de Rbenv

Tous les plugins et Rbenv lui-même peuvent être mis à jour par un simple `git pull` dans `~/rbenv/` et `~/.rbenv/plugins/`.
