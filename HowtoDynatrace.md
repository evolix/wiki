---
title: Howto Dynatrace
---

# Installation de l'agent OneAgent

Doc officielle : <https://help.dynatrace.com/get-started/installation/how-do-i-install-dynatrace-agent/>

Il faut d'abord accéder à l'espace [client](https://signin.dynatrace.com). Ensuite dans le menu à gauche, section « Deploy Dynatrace », « Start Installation ». Vous aurez des instructions du type :


```
# cd /tmp
# wget -O Dynatrace-OneAgent-Linux-1.115.176.sh https://terXXXX.live.dynatrace.com/installer/agent/unix/latest/XXXX
# wget https://ca.dynatrace.com/dt-root.cert.pem ; ( echo -e 'Content-Type: multipart/signed; protocol="application/x-pkcs7-signature"; micalg="sha-256"; boundary="--SIGNED-INSTALLER"\n\n----SIGNED-INSTALLER' ; cat Dynatrace-OneAgent-Linux-1.115.176.sh ) | openssl cms -verify -CAfile dt-root.cert.pem > /dev/null
# /bin/sh Dynatrace-OneAgent-Linux-1.115.176.sh APP_LOG_CONTENT_ACCESS=1
```

> **Note** : L'installation se fera dans /opt.

Une fois l'installation terminé, dans l'interface web vous allez voir apparaître la machine d'ici quelques minutes. Il vous sera suggérer de redémarrer certains services, comme Apache.