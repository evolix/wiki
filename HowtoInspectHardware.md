* Statut de cette page : test / bookworm

Cette page a pour but de rassembler les diverses commandes utiles pour obtenir des informations matérielles sur une machine.


## Outils généraux

### `dmidecode`

`dmidecode` est un utilitaire qui décode le contenu de la table DMI.

~~~
$ dmidecode --type <TYPE> [--quiet]
~~~

Où `<TYPE>` peut être :

* `baseboard` : informations sur la carte mère.
* `bios`
* `cache`
* `chassis`
* `connector` : informations sur les ports.
* `memory`
* `processor` 
* `slot` : information sur les slots PCI
* `system`

`<TYPE>` peut aussi être un numéro DMI :

~~~
Type   Information
────────────────────────────────────────────
   0   BIOS
   1   System
   2   Baseboard
   3   Chassis
   4   Processor
   5   Memory Controller
   6   Memory Module
   7   Cache
   8   Port Connector
   9   System Slots
  10   On Board Devices
  11   OEM Strings
  12   System Configuration Options
  13   BIOS Language
  14   Group Associations
  15   System Event Log
  16   Physical Memory Array
  17   Memory Device
  18   32-bit Memory Error
  19   Memory Array Mapped Address
  20   Memory Device Mapped Address
  21   Built-in Pointing Device
  22   Portable Battery
  23   System Reset
  24   Hardware Security
  25   System Power Controls
  26   Voltage Probe
  27   Cooling Device
  28   Temperature Probe
  29   Electrical Current Probe
  30   Out-of-band Remote Access
  31   Boot Integrity Services
  32   System Boot
  33   64-bit Memory Error
  34   Management Device
  35   Management Device Component
  36   Management Device Threshold Data
  37   Memory Channel
  38   IPMI Device
  39   Power Supply
  40   Additional Information
  41   Onboard Devices Extended Information
  42   Management Controller Host Interface 
~~~


### `lshw`

A documenter


## Mémoire

Lister les slots utilisés et libres :

~~~
$ lshw -class memory [-short]
~~~

Plus détaillé :

~~~
$ dmidecode --type memory [--quiet]
~~~


## Processeur(s)

Nombre de CPUs :

~~~
$ nproc
8
~~~

Modèle des processeur :

~~~
$ lspci | grep -i proc
~~~

Plus détaillé :

~~~
$ dmidecode --type processor --quiet
~~~


## Disques

Attention : Si vous avez du RAID matériel, `lsblk` indiquera seulement les volumes RAID virtuels.

Lister les périphériques blocs :

~~~
$ lsblk
~~~

Pour exclure les partitions de la sortie :

~~~
$ lsblk --nodeps
~~~

Pour exclure ou inclure des périphériques blocs par type de périphérique (major number) :

~~~
$ lsblk --include 9,11
$ lsblk --exclude 9,11
~~~

La liste des major numbers est variable selon les systèmes, mais elle peut être trouvée dans `/proc/devices`.


## Divers

Lister les périphériques connectés au bus USB :

~~~
$ lsusb
~~~

Lister les périphériques connectés au bus PCI :

~~~
$ lspci
~~~
