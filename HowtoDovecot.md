---
categories: email
title: Howto Dovecot
...

* Documentation : <https://doc.dovecot.org/2.3/>
* Options : <https://doc.dovecot.org/2.3/settings/core/>
* Statut de cette page : test / bookworm

[Dovecot](https://www.dovecot.org/) est un serveur permettant l'accès à des emails avec les protocoles [POP](https://fr.wikipedia.org/wiki/Post_Office_Protocol) et [IMAP](https://fr.wikipedia.org/wiki/Internet_Message_Access_Protocol). Il possède toutes les fonctionnalités indispensables pour nous : la couche [SSL/TLS](HowtoSSL) pour sécuriser les échanges réseau, l'intégration avec [Postfix](HowtoPostfix), la gestion de l'authentification avec [LDAP](HowtoLDAP), le support du format de stockage [Maildir](#format-de-boîte-mail-maildir) et les filtres [Sieve](https://fr.wikipedia.org/wiki/Sieve). Nous aimons sa légèreté et sa rapidité, nous l'avons définitivement adopté sur nos serveurs de messagerie en remplacement de [Courier POP/IMAP](http://www.courier-mta.org/).


## Installation

~~~
# apt install dovecot-pop3d dovecot-imapd dovecot-sieve dovecot-ldap dovecot-managesieved

# dovecot --version
2.3.19.1 (9b53102964)

# systemctl status dovecot
● dovecot.service - Dovecot IMAP/POP3 email server
     Loaded: loaded (/lib/systemd/system/dovecot.service; enabled; preset: enabled)
     Active: active (running) since Thu 2024-10-10 17:07:43 CEST; 5h 13min ago
       Docs: man:dovecot(1)
             https://doc.dovecot.org/
   Main PID: 876947 (dovecot)
     Status: "v2.3.19.1 (9b53102964) running"
      Tasks: 6 (limit: 4673)
     Memory: 5.4M
        CPU: 1.568s
     CGroup: /system.slice/dovecot.service
             ├─876947 /usr/sbin/dovecot -F
             ├─876950 dovecot/anvil
             ├─876951 dovecot/log
             ├─876952 dovecot/config
             ├─877190 dovecot/stats
             └─877191 dovecot/old-stats
~~~

> * Note* : le paquet `dovecot-ldap` est utile uniquement pour gérer l'authentification avec un annuaire [LDAP](HowtoLDAP). Il est également possible de gérer l'authentification avec une base de données SQL, il faudra alors installer le paquet `dovecot-mysql` ou `dovecot-pgsql`.


## Configuration

Fichiers de configuration :

~~~
/etc/dovecot/
├   dovecot.conf
├   conf.d/
│   ├── 10-auth.conf
│   ├── 10-director.conf
│   ├── 10-logging.conf
│   ├── 10-mail.conf
│   ├── 10-master.conf
│   ├── 10-ssl.conf
│   ├── 10-tcpwrapper.conf
│   ├── 15-lda.conf
│   ├── 15-mailboxes.conf
│   ├── 20-imap.conf
│   ├── 20-managesieve.conf
│   ├── 20-pop3.conf
│   ├── 90-acl.conf
│   ├── 90-plugin.conf
│   ├── 90-quota.conf
│   ├── 90-sieve.conf
│   ├── 90-sieve-extprograms.conf
│   ├── auth-checkpassword.conf.ext
│   ├── auth-deny.conf.ext
│   ├── auth-dict.conf.ext
│   ├── auth-ldap.conf.ext
│   ├── auth-master.conf.ext
│   ├── auth-passwdfile.conf.ext
│   ├── auth-sql.conf.ext
│   ├── auth-static.conf.ext
│   ├── auth-system.conf.ext
│   ├── auth-vpopmail.conf.ext
│   └── z-evolinux-defaults.conf
├── dovecot-dict-auth.conf.ext
├── dovecot-dict-sql.conf.ext
├── dovecot-ldap.conf.ext
├── dovecot-sql.conf.ext
└── private/
~~~

La configuration se trouve dans le fichier `/etc/dovecot/dovecot.conf` qui inclut de nombreux fichiers séparés dans `/etc/dovecot/conf.d/*.conf`.

Voici le principe général des fichiers de configuration : <https://doc.dovecot.org/2.3/configuration_manual/config_file/>
notamment comment fonctionne la surcharge ds directives.

Le résultat peut être observé avec `dovecot -a` (toutes les options) ou `dovecot -n` (uniquement les options qui ne sont pas par défaut).

Le fichier `/etc/dovecot/conf.d/z-evolinux-defaults.conf` contient notre configuration de base :

~~~
# Autorise les mécanismes PLAIN/LOGIN même sans SSL/TLS
#disable_plaintext_auth = no
auth_mechanisms = plain login
auth_verbose = yes
#auth_debug = yes
#auth_debug_passwords = yes

# Authentification LDAP + intégration avec Postfix pour l'auth SMTP
!include auth-ldap.conf.ext
service auth {
    unix_listener auth-userdb {
        mode = 0600
        user = vmail
        group = vmail
    }
    unix_listener /var/spool/postfix/private/auth-client {
        mode = 0666
        user = postfix
        group = postfix
    }
}

# Stockage des emails dans /home/mail avec UID/GID 5000/5000
mail_location = maildir:/home/vmail/%d/%n
mail_uid = 5000
mail_gid = 5000

# Activation Sieve
protocol lda {
    mail_plugins = sieve
}

# Optimisations
service login {
    process_limit = 256
}
mail_max_userip_connections = 42
~~~

Il est important de commenter la ligne suivante si on n'utilise pas du tout l'authentification via PAM car cela évite d'avoir plusieurs secondes de lag à chaque requête POP/IMAP ! Cela se fait en commentant la ligne suivante dans le fichier `/etc/dovecot/conf.d/10-auth.conf` :

~~~
#!include auth-system.conf.ext
~~~

Les paramètres de connexion à LDAP ainsi que la correspondance des champs doivent être indiqués dans le fichier `/etc/dovecot/dovecot-ldap.conf.ext` :

~~~
hosts = 127.0.0.1
auth_bind = yes
ldap_version = 3
base = dc=example,dc=com
user_attrs = homeDirectory=home
user_filter = (&(isActive=TRUE)(uid=%u))
pass_attrs = uid=user,userPassword=password
iterate_filter = (&(isActive=TRUE))
~~~

### Indexes

<https://doc.dovecot.org/developer_manual/design/indexes/index_file_format/>

On conseille d'utiliser les indexes Dovecot, cela permet d'optimiser le fonctionnement de Dovecot.

Par défaut, les indexes sont stockés à la racine de chaque Maildir, mais l'on peut aussi stocker les indexes ailleurs pour de meilleurs performances (par exemple sur un disque séparé, et même plus rapide). On spécifie alors à l'option `mail_location` :

~~~
mail_location = maildir:~/Maildir:INDEX=/srv/indexes-dovecot/%u
~~~

Ces indexes sont recréés à la volée, donc en cas de souci (corruption, etc.) on peut les effacer et redémarrer Dovecot sans perte de données.

> Attention : si vous gérez plus de 26 « IMAP tags », les tags supplémentaires seront stockés uniquement dans l'index et il faut donc éviter de les effacer

### Authentification SMTP sur Dovecot

On conseille de déléguer l'authentification SMTP à Dovecot, ce qui est pratique pour ne pas définir à deux endroits différents les paramètres du
backend d'authentification utilisé. Cela consiste à faire écouter Dovecot sur une socket Unix (comme utilisé dans notre notre configuration de base) :

~~~
service auth {
  […]

  # Postfix smtp-auth
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    user = postfix
    group = postfix
  }
}
~~~

puis indiquer le chemin vers la socket dans la configuration de Postfix :

~~~
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth-client
~~~


### Configuration SSL/TLS

* Documentation : <https://doc.dovecot.org/configuration_manual/dovecot_ssl_configuration/>

Il est fortement recommandé d'activer la couche [SSL/TLS](HowtoSSL).
Cela se fait ainsi :

~~~
ssl = yes
ssl_cert = </etc/ssl/certs/example.com.fullchain.crt
ssl_key = </etc/ssl/private/example.com.key
~~~

Attention, la directive `ssl_cert` doit inclure le certificat du domaine ET toute la chaîne de certification, que l'on appelle parfois « fullchain ».

La directive `ssl_ca` ne doit PAS être utilisée, elle sert à faire de la vérification de certificats SSL clients.

> Note : Il n’y plus besoin d’ajouter la ligne `ssl_dh_parameters_length`.

À noter que si l'on veut forcer toutes les connexions POP/IMAP à utiliser SSL/TLS (sauf les connexions depuis *localhost*) , on utilise :

~~~
ssl = required
~~~

Si l'on veut avoir des certificats différents pour le POPS et IMAPS on peut utiliser :

~~~
protocol imap {
  ssl_cert = </etc/ssl/certs/imap.pem
  ssl_key = </etc/ssl/private/imap.pem
}
protocol pop3 {
  ssl_cert = </etc/ssl/certs/pop3.pem
  ssl_key = </etc/ssl/private/pop3.pem
}
~~~

### Livraison avec Dovecot LDA

Dovecot fournit en local un agent de livraison (LDA) nommé `dovecot-lda`.
On recommande d'utiliser la commande `deliver`, qui est un lien symbolique, pour l'usage des filtres Sieve dont celui-ci peut être utile dans le cas d'extension d'adresse (subaddressing) tel que `foo+bar@domaine.net`.

On ajoutera dans postfix la ligne suivante à `/etc/postfix/master.cf` :

~~~
dovecot unix - n n - - pipe flags=DRhu user=vmail:vmail argv=/usr/lib/dovecot/deliver -f ${sender} -a ${original_recipient} -d ${user}@${nexthop}
~~~

> *Note* : Tous les comptes sont gérés par un unique compte Unix _vmail_.

et dans le fichier `/etc/postfix/main.cf` :

~~~
virtual_transport = dovecot
dovecot_destination_recipient_limit = 1
~~~

_Il également possible d'utiliser le démon *dovecot-lmtp* via le package `dovecot-lmtp`._

### Mailboxes Sent, Trash, Drafts, Junk

<https://doc.dovecot.org/2.3/configuration_manual/namespace/#mailbox-settings>

On peut créer automatiquement des sous-boîtes pour les clients IMAP.
Les noms sont à peu près normalisés dans la [RFC 6154](https://tools.ietf.org/html/rfc6154).

Voici comment créer automatiquement Sent, Trash, Drafts et Junk :

~~~
namespace inbox {
  location =
  mailbox Drafts {
    auto = no
    special_use = \\Drafts
  }
  mailbox Sent {
    auto = subscribe
    special_use = \\Sent
  }
  mailbox Spam {
    auto = subscribe
    special_use = \\Junk
  }
  mailbox Trash {
    auto = no
    special_use = \\Trash
  }
  prefix =
  separator = .
}
~~~

La paramètre `prefix` permet d'envoyer un [NAMESPACE IMAP](https://tools.ietf.org/html/rfc2342) par défaut au client.
Le client peut aussi régler son « NAMESPACE IMAP » : `Dossier sur le serveur IMAP` sous Thunderbird, `Préfixe du chemin IMAP` sous K-9 Mail.

On conseille de le laisser vide, sauf si vous migrez d'une configuration précédente : par exemple si vous migrez depuis Courier-IMAP vous pourriez vouloir mettre `prefix= INBOX.` pour éviter d'avoir l'impression que votre arboresence de dossiers IMAP soit « sous » le dossier de réception par défaut.

Le paramètre `separator` sert à définir un caractère pour simuler une arborescence de sous-dossiers. Par défaut c'est le caractère `.` pour des Maildirs.

### Compression IMAP

On peut activer une compression au niveau du protocole IMAP ([commande COMPRESS dans la RFC4978](https://tools.ietf.org/html/rfc4978#section-3)]).
Cela permet notamment de réduire la bande passante utilisée.


Il faut activer le plugin *imap_zlib* comme ceci :

~~~
protocol imap {
  mail_plugins = $mail_plugins imap_zlib
}
~~~

Pour vérifier que la compression est bien active, on peut se connecter en IMAP, et voir lors du login si la compression est bien en mode *deflate* : **COMPRESS=DEFLATE**

~~~
* OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE STARTTLS AUTH=PLAIN AUTH=LOGIN] Dovecot ready.

a1 LOGIN foo password

a1 OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE SORT SORT=DISPLAY THREAD=REFERENCES THREAD=REFS THREAD=ORDEREDSUBJECT MULTIAPPEND URL-PARTIAL CATENATE UNSELECT CHILDREN NAMESPACE UIDPLUS LIST-EXTENDED I18NLEVEL=1 CONDSTORE QRESYNC ESEARCH ESORT SEARCHRES WITHIN CONTEXT=SEARCH LIST-STATUS BINARY MOVE SPECIAL-USE COMPRESS=DEFLATE] Logged in
~~~

### Sieve rétrocompatible avec procmail

La configuration suivante est utile lorsque l'on veut migrer de procmail à Sieve de manière progressive, pour pouvoir utiliser des script Sieve tout en gardant ses anciens script procmail. Par contre elle n'est pas recommandé lors de la mise en place d'un nouveau service mail car procmail est déprécié depuis des années.

Configuré ainsi Dovecot utilisera procmail en solution de replis si un courriel n'a pas déjà été accepté par un script Sieve et qu'un utilisateur possède un fichier `~/.procmailrc`. Si utilisateur souhaite ne pas du tout utiliser Sieve, il peut alors mettre `|/local/bin/procmail -f-` dans son `~/.forward`, ce qui contournera la logique précédente.

On commence par créer un script qui execute procmail quand l'utilisateur possède un fichier de configuration dans son compte :

~~~ sh
install --mode 755 -D --no-target-directory /dev/null /usr/local/lib/dovecot/sieve-pipe/procmail
cat > /usr/local/lib/dovecot/sieve-pipe/procmail <<EOF
#!/bin/sh

if [ ! -f "\$HOME/.procmailrc" ]; then
    exit 1
fi

exec /usr/bin/procmail
EOF
~~~

Puis, on crée une règle Seive qui passe les courriels via une pipe UNIX au script procmail précedement créé :

~~~ sh
install --mode 644 -D --no-target-directory /dev/null /var/lib/dovecot/sieve/after.d/procmail
cat > /var/lib/dovecot/sieve/after.d/procmail <<EOF
# If a message hasn't been handled by the user sieve's config
# we fallback to its procmail if he has configured it.

require "vnd.dovecot.pipe";

pipe "procmail";
EOF
~~~

Finalement on configure Dovecot pour qu'il utilise notre règle Seive quand un courriel n'a pas déjà été accepté par une règle Sieve système ou utilisateur :

~~~
plugin {
  sieve_after = /var/lib/dovecot/sieve/after.d/procmail
  sieve_extensions = +vnd.dovecot.pipe
  sieve_plugins = sieve_extprograms

  sieve_pipe_bin_dir = /usr/local/lib/dovecot/sieve-pipe
}
~~~

Note : Il est très probable, lors de ce genre de migration, que votre ancienne configuration Postfix (et Dovecot) ai besoin d'être ajusté pour que Postfix utilise Dovecot comme MDA local plutot que procmail.


### Statistiques

Ce module permet notamment de faire fonctionner les plugins Munin.

En Dovecot 2.2, il s'appelle `stats`, et a été renommé en `old_stats` à partir de 2.3.


#### Dovecot 2.2 (Debian Stretch)

Documentation officielle : <https://doc.dovecot.org/configuration_manual/stats/old_statistics/>

Pour un serveur mail avec des comptes mail virtuels :

~~~
# Cette directive doit précéder celles à l'intérieur des filtres 
mail_plugins = $mail_plugins stats

protocol imap {
  mail_plugins = $mail_plugins imap_stats
}
plugin {
  stats_refresh = 30 secs
  stats_track_cmds = yes
}
service stats {
  fifo_listener stats-mail {
    user = vmail
    group = vmail
    mode = 0660
  }
}
~~~

Pour un serveur mail avec des comptes mail unix, dans le `service stats {`, autoriser tous les utilisateurs à écrire dans les stats (au lieu de l'utilisateur `vmail`) :

~~~
service stats {
  fifo_listener stats-mail {
    mode = 0666
  }
}
~~~

**Attention :** La directive `mail_plugins` globale (pas celle à l'intérieur des filtres `{}`) doit précéder toutes les directives `mail_plugins` se trouvant dans les filtres. Sinon, on aura un warning du genre :

~~~
# doveconf -n > /dev/null
doveconf: Warning: /etc/dovecot/conf.d/XX.conf line XX: Global setting mail_plugins won't change the setting inside an earlier filter at /etc/dovecot/conf.d/XX line XX (if this is intentional, avoid this warning by moving the global setting before /etc/dovecot/conf.d/XX line XX)
~~~


#### Dovecot 2.3 (>= Debian Buster)

Dovecot 2.3 a introduit un nouveau module `stats`, complètement différent de l'ancien.

Sa sortie et sa configuration ne sont pas rétrocompatible.

L'ancien module `stats` de la version 2.2 a été renommé `old_stats` en 2.3 (ainsi que toutes ses variables de configuration).

Documentation officielle pour continuer à utiliser le module `old_stats` : <https://doc.dovecot.org/installation_guide/upgrading/from-2.2-to-2.3/>

Pour un serveur mail avec des comptes mail virtuels :

~~~
# Cette directive doit précéder celles à l'intérieur des filtres 
mail_plugins = $mail_plugins old_stats

protocol imap {
  mail_plugins = $mail_plugins imap_old_stats
}
plugin {
  old_stats_refresh = 30 secs
  old_stats_track_cmds = yes
}
service stats {
  fifo_listener stats-mail {
    user = vmail
    group = vmail
    mode = 0660
  }
    unix_listener stats-reader {
        user = vmail
        group = vmail
        mode = 0660
    }

    unix_listener stats-writer {
        user = vmail
        group = vmail
        mode = 0660
    }
}
~~~

Pour un serveur mail avec des comptes mail Unix, il faut ajuster le groupe et mettre un groupe où il y a tous les utilisateurs Unix.
On peut aussi mettre `mode = 0666` mais c'est déconseillé.

**Attention :** La directive `mail_plugins` globale (pas celle à l'intérieur des filtres `{}`) doit précéder toutes les directives `mail_plugins` se trouvant dans les filtres. Sinon, on aura un warning du genre :

~~~
# doveconf -n > /dev/null
doveconf: Warning: /etc/dovecot/conf.d/XX.conf line XX: Global setting mail_plugins won't change the setting inside an earlier filter at /etc/dovecot/conf.d/XX line XX (if this is intentional, avoid this warning by moving the global setting before /etc/dovecot/conf.d/XX line XX)
~~~


## Sieve

Sieve est un langage permettant d'écrire des règles de filtrage d'emails, du même type que [procmail](https://fr.wikipedia.org/wiki/Procmail).
Sieve est normalisé via la [RFC 5228](https://tools.ietf.org/html/rfc5228) et plusieurs clients email supportent la gestion de règles Sieve (*Thunderbird*, *Roundcube, *Ingo*…).

Pour activer Sieve, il faut utiliser Dovecot LDA et ajuster la directive *mail_plugins* (comme utilisé dans notre notre configuration de base) :

~~~
protocol lda {
  mail_plugins = $mail_plugins sieve
}
~~~

Dovecot lit par défaut le fichier _.dovecot.sieve_ à la racine du compte de
l'utilisateur et le compile à la volée. Si une erreur est rencontrée, il va
simplement ignorer le fichier et délivrer les messages dans la boite
principale, de sorte qu'aucun message ne soit perdu. Les erreurs de compilation
sont journalisées dans le fichier *.dovecot.sieve.log* à la racine du compte.

Il est également possible de définir des scripts sieve globaux à tous les
comptes, voir le fichier `90-sieve.conf`.

### Syntaxe 

Un filtre Sieve a la forme suivante :

~~~
if CONDITION1 {
  ACTION1;
} elsif CONDITION2 {
  ACTION2;
} else {
  keep;
}
~~~

> *Note* : le else {keep;} est le comportement par défaut

Exemples de condition :

~~~
if header :contains "Subject" "Foo"
if header :contains "Subject" ["Foo", "Bar"]
if address :domain "From" "qq.com"
if anyof (header :contains "X-Foo" "Bar", header :contains "X-Baz" "Qux")
~~~

### Exemple de règles

Voici quelques liens pour écrire les règles : <http://support.tigertech.net/sieve>, <http://sieve.info/tutorials>

Si on créer un fichier de règle global, par exemple dans /etc/dovecot/sieve.global, il faut généré le fichier de règle pré-compilé avec la commande sievec :

~~~
# sievec /etc/dovecot/sieve.global
~~~

Sinon on peux avoir ce message d'erreur dans les logs mail : 

~~~
The LDA Sieve plugin does not have permission to save global Sieve script binaries; global Sieve scripts like `/etc/dovecot/sieve.global' need to be pre-compiled using the sievec tool
~~~

#### Ajouter un message d'absence

Admettons que le compte utilisateur ayant cette arboresence, il faudra alors ajouter le fichier .dovecot.sieve :

~~~
cat /home/$DOMAINE/$UTILISATEUR/.dovecot.sieve
require ["vacation"];

if true #false #Activation True/False
{
        vacation :days 1 :addresses "email@example.net" :subject "Message d'absence" text:
Bonjour,

Veuillez utiliser email@example.net à présent pour me contacter.

Cordialement,
.
;
}
~~~

Si l'on veut faire l'usage de variables :

~~~
require ["vacation", "variables"];

if header :matches "subject" "*" {
  vacation :days 3 :subject "Re: ${1}" "Bonjour,

Je suis indispo jusqu'au DD/MM/YYYY.

";
}
~~~

#### Tri sur le sujet

~~~
require ["fileinto", "copy"];

if header :contains ["Subject"] ["testsieve"] {
  fileinto "Test";
}

~~~

#### Déduplication des mails entrants

Pour réaliser un équivalent de la règle procmail suivante :

~~~
:0 Wh: msgid.lock
| formail -D 8192 $HOME/.msgid.lock
~~~

Testé avec Dovecot 2.2.13 en Jessie.

Ajouter le plugin sieve duplicate dans `/etc/dovecot/conf.d/90-sieve.conf` :

~~~
  sieve_extensions = +vnd.dovecot.duplicate
~~~

Puis dans les règles sieve :

~~~
require ["fileinto","vnd.dovecot.duplicate"];

if duplicate {
    fileinto "Trash";
}

# Si on est sûr de vouloir les supprimer
#if duplicate {
#     discard;
#}
~~~

> *Note* : la ligne require doit être unique et en début de fichier, si des modules sont déjà chargés il suffit d'ajouter "vnd.dovecot.duplicate".

En Jessie-backports, version 2.2.27, « duplicate » est intégré. Et il faut mettre dans une règle sieve :

```
require ["duplicate", "variables"];
if header :matches "message-id" "*" {
  if duplicate :uniqueid "${0}" {
    discard;
  }
}
```

#### Déplacer les emails avec un certain sujet dans une sous-boîte Junk

~~~
require ["fileinto", "mailbox"];

if header :contains "subject" ["[SPAM]"] {
  fileinto :create "Spam";
}
~~~

#### Redirection

~~~
if header :contains "Subject" "Foo Bar"
{
    redirect "jdoe@example.com";
}
~~~


#### managesieve

Si l'on a bien installé le package `dovecot-managesieved`, Dovecot supporte le protocole *managesieve* qui permet d'éditer les règles
depuis un [client compatible](http://sieve.info/clients) : *Mozilla Thundebird* via une [extension](http://sieve.mozdev.org/), [Roundcube](HowtoMail/Roundcube), Horde [Ingo](https://www.horde.org/apps/ingo) etc.

La configuration par défaut de Dovecot active le démon **managesieved** qui écoute par défaut sur le port réseau TCP/4190.

> *Note* : si on utilise Roundcube, il faut installer les packages suivants :
>
> ~~~
> # apt install roundcube-plugins php-net-sieve
> ~~~


## Mode debug

### Activer le raw logging

<http://wiki.dovecot.org/Debugging/Rawlog>

Le raw logging est très utile pour effectuer du debug poussé car il permet de voir toutes les commandes jouées par un client.
Par exemple en IMAP, il faut activer cela dans le _dovecot.conf_ :

~~~
protocol imap {
  mail_executable = /usr/lib/dovecot/rawlog /usr/lib/dovecot/imap
}
~~~

Ensuite, il suffit de créer un répertoire _dovecot.rawlog_ dans le $HOME de l'utilisateur (accessible en écriture évidemment), et toutes les commandes IMAP passées seront stockées dans des fichiers : `<annee><mois><jour>-.*.{in,out}`

### Debug pour l'authentification

Activer :

~~~
auth_debug = yes
#auth_debug_passwords = yes
~~~

### Se connecter en IMAP via telnet

Pour debug des situations, on peux se connecter en IMAP cia telnet en local sur le port 143, voici quelques commandes utiles :

~~~
~# telnet 127.0.0.1 143
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.
* OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE STARTTLS AUTH=PLAIN AUTH=LOGIN] Dovecot ready.
~~~

* Se connecter avec un utilisateur :

~~~
a1 LOGIN foo $PASSWORD
a1 OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE SORT SORT=DISPLAY THREAD=REFERENCES THREAD=REFS THREAD=ORDEREDSUBJECT MULTIAPPEND URL-PARTIAL CATENATE UNSELECT CHILDREN NAMESPACE UIDPLUS LIST-EXTENDED I18NLEVEL=1 CONDSTORE QRESYNC ESEARCH ESORT SEARCHRES WITHIN CONTEXT=SEARCH LIST-STATUS BINARY MOVE SPECIAL-USE] Logged in
~~~

* Pour lister les repertoires :

~~~
a2 LIST """*"
* LIST (\HasNoChildren \Sent) "." Sent
* LIST (\HasNoChildren) "." Envoy&AOk-s
* LIST (\HasNoChildren \Trash) "." Trash
* LIST (\HasNoChildren \Drafts) "." Drafts
a2 OK List completed (0.000 + 0.000 secs).
~~~

* Pour examiner le contenu d'un dossier :

~~~
a3 EXAMINE INBOX
* FLAGS (\Answered \Flagged \Deleted \Seen \Draft)
* OK [PERMANENTFLAGS ()] Read-only mailbox.
* 263 EXISTS
* 0 RECENT
* OK [UNSEEN 2] First unseen.
* OK [UIDVALIDITY 1571146202] UIDs valid
* OK [UIDNEXT 278] Predicted next UID
a3 OK [READ-ONLY] Examine completed (0.000 + 0.000 secs).
~~~

* Pour quitter :

~~~
a4 LOGOUT
* BYE Logging out
a4 OK Logout completed (0.000 + 0.000 secs).
Connection closed by foreign host.
~~~


## Optimisations

## Nombre max de processus de login

À chaque login, dovecot fork un processus. Afin d'éviter les fork-bomb s'il y a trop de connexions il y a une limite par défaut à 128. L'augmenter peut résoudre des problèmes si le serveur reçoit de nombreuses connexions.

~~~
login_max_processes_count = 256
~~~

Il faut en parallèle augmenter la limite sur le nombre de fichiers ouverts, dans _/etc/default/dovecot_ :

~~~
ulimit -n 5696
~~~

### Nombre maximum de connexions par IP

Dovecot limite également le nombre de connexions IMAP d'une même adresse IP avec un même compte.
Cette limite est de 10 par défaut, il est possible de l'augmenter en ajoutant dans la section IMAP :

~~~
mail_max_userip_connections = 42
~~~

Si la limite n'est pas assez haute, on aura cette erreur :

~~~
dovecot: imap-login: Maximum number of connections from user+IP exceeded (mail_max_userip_connections)
~~~


## Monitoring

### Nagios

On utilise les checks classiques pour POP(S)/IMAP(S) :

~~~
/usr/lib/nagios/plugins/check_imap -H localhost
/usr/lib/nagios/plugins/check_imap -H localhost -E -s "a login $login $password\n" -e "Logged in" -M crit
/usr/lib/nagios/plugins/check_imap -S -H localhost -p 993
/usr/lib/nagios/plugins/check_pop -H localhost
/usr/lib/nagios/plugins/check_pop -S -H localhost -p 995
~~~

### Munin

3 plugins Munin sont disponibles sur <https://github.com/munin-monitoring/contrib/tree/master/plugins/dovecot>.

Nous les copions dans `/usr/local/lib/munin/plugins/` et faisons des liens symboliques dans `/etc/munin/plugins/`.

Nous utilisons `dovecot1` et `dovecot_stats_` (ce dernier est un plugin Munin wildcard).

~~~
# cat /etc/munin/plugin-conf.d/z-custom
[dovecot1]
group adm

[dovecot_stats_*]
group adm
~~~

Le plugin `dovecot_stats_` fait des statistiques par domaine. Pour avoir les statistiques globales, il faut le patcher :

~~~
[...]
+if [ "${domain}" = "global" ]; then
+    args="global"
+else
+    args="domain domain=$domain"
+fi
 
# Fetch data
# Gawk script cadged from http://awk.info/?JanisP
-doveadm $stats_command dump domain domain=$domain | gawk -F\\t -v cols="user_cpu sys_cpu min_fa    ults maj_faults vol_cs invol_cs read_count write_count num_logins num_cmds mail    _lookup_path mail_lookup_attr mail_read_count mail_cache_hits " -v domain=${dom    ain//\./_} '
+doveadm $stats_command dump $args | gawk -F\\t -v cols="user_cpu sys_cpu min_fa    ults maj_faults vol_cs invol_cs read_count write_count num_logins num_cmds mail    _lookup_path mail_lookup_attr mail_read_count mail_cache_hits " -v domain=${dom    ain//\./_} '
[...]
~~~

On peut ainsi faire des statistiques par domaine et/ou globales :

~~~
ln -s /usr/local/lib/munin/plugins/dovecot_stats_ /etc/munin/plugins/dovecot_stats_global
ln -s /usr/local/lib/munin/plugins/dovecot_stats_ /etc/munin/plugins/dovecot_stats_example.org
~~~

### log2mail

Afin d'être alerté en cas de souci, il est conseillé d'ajouter la configuration suivante au logiciel `log2mail` :

~~~
file = /var/log/mail.log
pattern = "Maximum number of connections from user+IP exceeded"
mailto = monitoring@example.com
template = /etc/log2mail/mail

file = /var/log/mail.log
pattern = "All login processes are in use"
mailto = monitoring@example.com
template = /etc/log2mail/mail

file = /var/log/mail.log
pattern = "Fatal: block_alloc"
mailto = monitoring@example.com
template = /etc/log2mail/mail

file = /var/log/mail.log
pattern = "client connections are being dropped"
mailto = monitoring@example.com
template = /etc/log2mail/mail

file = /var/log/mail.log
pattern = process_limit
mailto = monitoring@example.com
template = /etc/log2mail/mail
~~~


## Format de boîte mail `Maildir`

Documentation externe :

* <https://fr.wikipedia.org/wiki/Maildir>
* <https://doc.dovecot.org/admin_manual/mailbox_formats/maildir/>

Maildir est un format qui permet de stocker des mails dans un répertoire du système de fichiers.

Il contient trois répertoires `new`, `cur`, `tmp`. Ces répertoires contiennent un fichier par mail.

Il peut aussi contenir récursivement des sous-répertoires, eux-même contenant trois répertoires `new`, `cur`, `tmp`.

Le nom des sous-répertoires doit impérativement commencer par un `.` (répertoires cachés sous unix).


### Créer un sous-répertoire `Maildir` manuellement

On peut créer un sous-répertoire `Maildir` manuellement en se plaçant dans le répertoire `Maildir` parent et en jouant la commande suivante :

~~~
$ maildirmake.dovecot .<NEW_DIR_NAME>
~~~

Notes :

- Le `.` au début du nom du répertoire est obligatoire.
- Ne pas oublier de vérifier que l'utilisateur Dovecot (généralement `mail`) a bien les droits pour y accéder.
- Pour pouvoir voir un dossier, certains clients mails nécessitent de s'y abonner.


### Date des mails

Le format Maildir s’appuye sur les dates du système de fichiers. On peut ainsi par exemple rechercher les emails entre le 18 mai 2023 et 11 octobre 2023 ainsi :

~~~
$ find $HOME/Maildir/.foo/ -type f -newermt 2023-05-18 ! -newermt 2023-09-11
~~~


## Migration depuis Courier

<https://wiki.dovecot.org/Migration/Courier>

Il est nécessaire de convertir les fichiers pour les abonnements IMAP et cache d'UID POP :

~~~
# wget https://raw.githubusercontent.com/dovecot/tools/master/courier-dovecot-migrate.pl
# perl courier-dovecot-migrate.pl --to-dovecot --recursive --convert /home

Total: 40620 mailboxes / 285 users
       0 errors
No actual conversion done, use --convert parameter

# perl courier-dovecot-migrate.pl --to-dovecot --recursive --convert /home
~~~

Et suivant les cas, il peut être nécessaire d'adapter la configuration. Le mieux est de faire des tests, notamment pour vérifier si les messages ne sont pas téléchargés en double en POP, si les dossiers IMAP s'affichent correctement (pas de doublons, accessible au même niveau). Voici un exemple de directive qui peuvent être adaptées :

~~~
namespace {
  prefix = INBOX.
  separator = .
  inbox = yes
}
pop3_uidl_format = %f
~~~

## FAQ

### Erreur "Out of memory"

Si vous avez des erreurs du type :

~~~
dovecot: imap(foo): Fatal: block_alloc(16777216): Out of memory
dovecot: imap(foo): Fatal: master: service(imap): child 666 returned error 83 (Out of memory (service imap { vsz_limit=256 MB }, you may need to increase it))
~~~

Il faut surcharger la limite par défaut de la mémoire `vsz_limit` dans la section `imap` de la configuration (`/etc/dovecot/conf.d/zzz-custom`) :

~~~
service imap {
  vsz_limit = 2048 M
}
~~~


### Vérifier la conf

Pour vérifier qu'il n'y a pas d'erreur dans la configuration

~~~
# doveconf -n > /dev/null
~~~

Il se peut que la configuration soit bonne mais qu'il y ait tout de même une erreur (un certificat SSL/TLS qui n'existe pas, par exemple), on peut alors lancer dovecot en mode debug :

~~~
# dovecot -F
~~~

### Mountpoints

<http://wiki2.dovecot.org/Mountpoints>

### Erreurs de LOCK

Si vous avez des chargements très longs à l'authentification ou des erreurs de type :

~~~
-ERR [IN-USE] Couldn't open INBOX: Timeout while waiting for lock
~~~

c'est peut-être que les locks sont activés pour le protocole POP3. Ainsi, dès qu'une connexion POP3 est en cours,
elle bloques les autres connexions POP3... et même IMAP. Pour résoudre cela, s'assurer d'avoir activé :

~~~
pop3_lock_session=no
~~~

### doveadm

La commande _doveadm_ permet d'avoir différentes interactions avec le serveur Dovecot. Par exemple :

~~~
# doveadm who
username                           # proto (pids)                                                  (ips)
jdoe@example.com                   1 imap  (4242)                                                  ()

# doveadm kick foo 192.0.2.0/24
kicked connections from the following users:
foo
~~~

Exemple, purger les corbeilles au bout de 30 jours :

~~~
# doveadm -Dv expunge -A mailbox INBOX.Trash savedbefore 30d
~~~

### Lenteur à chaque requête POP/IMAP

Si vous constatez une lenteur à chaque requête avec votre client POP/IMAP (Roundcube, Thunderbird etc.), vérifiez que vous avez désactivé l'authentification via PAM qui rajoute plusieurs secondes de lag si elle n'est pas utile ! La ligne suivante doit être commentée dans le fichier `/etc/dovecot/conf.d/10-auth.conf` :

~~~
#!include auth-system.conf.ext
~~~

### Erreur "Sieve script file path (...)/.dovecot.sieve is a directory"

~~~
Error: sieve: file script: Sieve script file path '/home/vmail/$domain/$user/.dovecot.sieve' is a directory
~~~

Supprimer le répertoire et faire un touch à la place, puis mettre le propriétaire et le groupe à `vmail` :

~~~
rm -r /home/vmail/$domain/$user/.dovecot.sieve
touch /home/vmail/$domain/$user/.dovecot.sieve
chown vmail: /home/vmail/$domain/$user/.dovecot.sieve
~~~

### Erreur "(...)/.dovecot.sieve/tmp failed: Not a directory"

~~~
Error: stat(/home/vmail/$domain/$user/.dovecot.sieve/tmp) failed: Not a directory
~~~

Par défaut, Dovecot considère que tout ce qui commence par un `.` est une Maildir. Si vous avez donc un fichier `.dovecot.sieve` accessible dans `mail_location`, il va considérer que c'est une Maildir et cela provoquera des erreurs du type :

~~~
dovecot: imap(jdoe@example.com): Error: stat(/home/vmail/example.com/jdoe/.dovecot.sieve/tmp) failed: Not a directory
~~~

La solution est que `.dovecot.sieve` ne soit pas accessible dans `mail_location` ou de modifier `sieve = ~/.dovecot.sieve` pour que cela ne commence pas par un `.`.

Un contournement est d'activer l'option `maildir_stat_dirs = yes` mais cela aura un petit impact sur les performances (sauf dans certains systèmes de fichiers comme `ext4`).


### Error: Corrupted record in index cache file

Si vous obtenez des erreurs du type :

~~~
Error: Corrupted record in index cache file sername/.INBOX/dovecot.index.cache: UID 1227: Broken virtual size in mailbox INBOX: ...
Error: Mailbox INBOX: UID=371203: read(...) failed: Cached  message size smaller than expected (3904 < 3914, box=INBOX, UID=371203) 
~~~

Vous pouvez essayer de supprimer les indexes et redémarrer Dovecot.

Si cela ne résoud pas le souci, cela peut être parce que vous avez une Maildir « corrompue » : fichiers de taille vide, permissions incorrectes... vous devrez résoudre ce problème avant de supprimer les indexes.


### Erreur /var/run/dovecot/stats-writer failed: Permission denied

Si vous trouvez l'erreur suivante dans les logs mail :

~~~
Jul  5 15:10:26 mail2 postfix/pipe[8907]: 92880605ED: to=<XXX>, relay=dovecot, delay=0.12, delays=0.05/0/0/0.07, dsn=2.0.0, status=sent (delivered via dovecot service (lda(XXX,)Error: net_connect_unix(/var/run/dovecot/stats-writer) failed: Permission de))
~~~

Ajouter/modifier dans `/etc/dovecot/conf.d/z-evolinux-defaults.conf` (attention, le bloc `service stats` peut déjà être présent ) :

~~~
service stats {
  unix_listener stats-reader {
    group = vmail
    mode = 0660
  }
  unix_listener stats-writer {
    group = vmail
    mode = 0660
  }
}
~~~

Puis redémarrer Dovecot.

### Erreur failed: Too many open files

Si vous trouvez l'ereur suivante dans les logs mail :

~~~
Jul  5 15:10:26 mail2 dovecot: config: Error: net_accept() failed: Too many open files
~~~

Il faut augmenter le nombre de fichiers que les processus du service `dovecot` peut ouvrir en éditant le service Systemd :

~~~
# systemctl edit dovecot.service
[Service]
LimitNOFILE=12288
# systemctl restart dovecot.service
~~~

### _Flags_ perdu lors du déplacement d’un e-mail

*WIP*

> Cas où les boites sont des comptes Unix (pas _vmail_).

Si on perd certains _flags_ / labels / _tags_ lorsqu’on déplace un e-mail d’un répertoire à un autre, c’est probablement un problème de permission.


### Incohérences après un déplacement de mail en CLI

Forcer la resynchronisation des indexes peut aider :

~~~
# doveadm force-resync -u vmail /home/vmail/<DOMAIN>/<ACCOUNT>/
ou
# doveadm force-resync -u plhotetemp /home/<USER>/Maildir/
~~~


### Warning process_limit (100) reached

`Warning: service(imap-login): process_limit (100) reached, client connections are being dropped`

Si besoin, on augmente les limites, par exemple :

~~~
default_process_limit = 8192
default_client_limit = 8192

service login {
    process_limit = 512
}
service imap {
    process_limit = 512
}
service imap-login {
    process_limit = 512
    service_count = 0
}
service imap {
    process_limit = 1024
}
service auth {
    client_limit = 8192
service auth {
}
service anvil {
    client_limit = 8192
}
~~~


### Erreur `User no longer exists`

Si Postfix s'adosse à un LDAP, il peut manquer dans `/etc/dovecot/dovecot-ldap.conf.ext` :

~~~
iterate_filter = (&(isActive=TRUE))
~~~

Cette option permet de ne garder que les comptes mails `isActive=TRUE` quand on utilise des commandes `doveadm` qui listent les utilisateurs avec un wildcard, par exemple :

~~~
$ doveadm user -u '*'
$ doveadm expunge -u '*' mailbox INBOX savedbefore 7d
~~~


### Erreur `Panic: file mail-index-util`

S'il y a pour symptôme d'afficher une page blanche sur le client mail avec cette erreur :
 
~~~
 Sep 2 19:19:07 machine  dovecot: imap(capso): Panic: file mail-index-util.c: line 37 (mail_index_uint32_to_offset): assertion failed: (offset < 0x40000000)
~~~

Alors, c'est probablement que le fichier de cache " dovecot.index.cache " est trop gros. Il peut être supprimé sans souci et sera recréé progressivement.


### Erreur `Mailbox INBOX … Cannot allocate memory`

Erreur `Mailbox INBOX: mmap(size=313486420) failed with file /home/vmail/../../Maildir/dovecot.index.cache: Cannot allocate memory`

C'est un problème de taille du fichier `dovecot.index.cache`, on peux supprimer le fichier, il sera recrée par Dovecot


### Logs

Par défaut, Dovecot envoie ses logs à syslog, mais on peut lui faire écrire directement ses logs par exemple :

~~~
log_path = /var/log/dovecot-core.log
info_log_path = /var/log/dovecot-info.log
log_timestamp = "%Y-%m-%d %H:%M:%S "
~~~

Ne pas oublier de rotater les logs.


### Quota

* <https://doc.dovecot.org/2.3/configuration_manual/quota_plugin/#quota>
* <https://doc.dovecot.org/2.3/settings/plugin/quota-plugin/>
* <https://doc.dovecot.org/2.3/configuration_manual/quota/>

~~~
mail_plugins = quota
plugin {
quota = maildir:User quota
quota_rule = *:storage=5G
quota_rule2 = INBOX/Trash:storage=+10%
quota_rule3 = INBOX/Spam:ignore
quota_exceeded_message = Over Quota.
quota_warning = storage=95%% quota-warning 95 %u
quota_warning2 = storage=80%% quota-warning 80 %u
quota_warning3 = storage=85%% quota-warning 85 %u
quota_warning4 = storage=90%% quota-warning 90 %u
}
service quota-warning {
  executable = script /usr/local/bin/dovecot-quota-warning.sh
  user = vmail
  unix_listener quota-warning {
 }
}
protocol imap {
  mail_plugins = $mail_plugins imap_quota
}
~~~

Un quota spécifique par utilisateur peut être défini dans LDAP via l'option `user_attrs` :
<https://doc.dovecot.org/2.3/configuration_manual/authentication/ldap_userdb/>

~~~
# doveadm quota get -u foo@example.com
Quota name Type    Value Limit   %
User quota STORAGE  3784  9766  38

# doveadm quota get -A

# doveadm quota recalc -A
~~~


### FTS : Full Text Search

* <https://doc.dovecot.org/2.3/configuration_manual/fts/>

Il est possible de faire des recherches en IMAP avec `SEARCH TEXT foo` ou `SEARCH BODY foo`.
Sauf que c'est rapidement très si vous avez pas mal de messages.
Dovecot peut indexer le contenu des messages avec un moteur externe.

Dovecot supporte désormais nativement SOLR, et il y a un [projet actif pour supporter Xapian](https://github.com/grosjo/fts-xapian).

Voici comment cela fonctionne avec Xapian :

~~~
# apt install dovecot-fts-xapian
~~~

On active dans la configuration :

~~~
mail_plugins = $mail_plugins fts fts_xapian

plugin {
    fts = xapian
    fts_xapian = partial=3 full=20
    fts_autoindex = yes
    fts_enforced = yes
    fts_autoindex_exclude = \Trash
    # Index attachements
    #fts_decoder = decode2text
}
service indexer-worker {
    vsz_limit = 2G
    process_limit = 0
}
~~~

Ainsi dans la Maildir vous aurez un réperoire d'indexes : `xapian-indexes/`

Le scan des nouveaux messages se fait à chaque recherche, mais l'on peut aussi
doveadm manuellement ou en cron pour relancer un scan ou une optimisation des indexes.
Par exemple :

~~~
# doveadm index -A -q \*
# doveadm fts optimize -A
~~~


### Email bombing mitigation

Entre autre, on peut mitiger (un peu) une attaque de type [Email bombing](https://fr.wikipedia.org/wiki/Email_bombing) via des filtres Sieve.

Les conseils de Proton sont intéressants : <https://proton.me/support/email-bomb-mitigation>


### Erreur Certificate failure for DOMAIN: unable to get local issuer certificate

Ou bien dans les logs mail : `ssl3_read_bytes:tlsv1 alert unknown ca: SSL alert number 48`.

C'est parce qu'il manque des certificats intermédiaires dans `ssl_cert`.

Vérifier qu'ils n'ont pas été mis dans `ssl_ca`.

