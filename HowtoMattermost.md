---
categories: chat
title: Howto Mattermost
...

* Documentation : <https://docs.mattermost.com>


[Mattermost](https://mattermost.com) est un logiciel libre de discussion instantanées en équipe. C'est une alternative open source à Slack qu'on peut héberger soi-même.

## Installation

### Pré-requis

Une machine avec Debian 11 (Bullseye) ou plus récent avec les logiciels suivants : 

* [Nginx](/HowtoNginx) - Servira de reverse proxy web
* [MySQL 8](/HowtoMySQL) - Base de donnée pour l'application -- **Obligatoirement** MySQL 8 car Mattermost n'est plus compatible avec MariaDB 

### Mise en place 

Tout d'abord, on a besoin :  
* D'une base MySQL avec un utilisateur - login/base : `mattermost` / pass : `PASSWORD`  
* D'un utilisateur unix : `mattermost`


On récupère donc la dernière version Team LTS (actuellement la 8.1.6) et on déroule : 

~~~
# su - mattermost
$ wget https://releases.mattermost.com/7.8.1/mattermost-team-8.1.6-linux-amd64.tar.gz
$ tar mattermost-team-9.1.6-linux-amd64.tar.gz
$ cd mattermost
$ mkdir data
~~~

Il faut ensuite ajuster la configuration à ses besoins. Elle se trouve dans le fichier `config/config.json`.
C'est là qu'il faut aussi paramétrer la connexion à la base de donnée MySQL. Celle-ci se règle via la valeur `DataSource` dans `SqlSettings`.
Dans notre exemple, on met la valeur suivante (à adapter) `mattermost:PASSWORD@tcp(localhost:3306)/mattermost?charset=utf8mb4,utf8`
Il faut aussi régler la valeur de `DriverName` sur `mysql` dans la même section `SqlSettings`.


Enfin, il ne reste plus qu'à mettre en place l'unité systemd :

~~~
[Unit]
Description=Mattermost
After=network.target
After=mariadb.service
Requires=mariadb.service

[Service]
Type=notify
Restart=always
WorkingDirectory=/home/mattermost/mattermost
ExecStart=/home/mattermost/mattermost/bin/mattermost
TimeoutStartSec=3600
LimitNOFILE=49152
RestartSec=10
User=mattermost
Group=mattermost

[Install]
WantedBy=multi-user.target
~~~


Par défaut, Mattermost écoute sur le port 8065. En s'y connectant, on peut créer le premier compte qui sera automatiquement administrateur

On peut aussi configurer nginx comme reverse proxy en s'inspirant de la configuration conseillée dans [la documentation officielle](https://docs.mattermost.com/install/install-debian.html#configuring-nginx-as-a-proxy-for-mattermost-server).

> Note: La configuration proposée par les développeurs s'appuie sur le module de cache de Nginx. Ce module permet de décharger de certaines requêtes le service Mattermost. Mais il peut compliquer les tests lors de soucis sur l'interface web. Dans cet exemple de configuration il n'est donc pas activé par défaut.

~~~
upstream backend {
   server 127.0.0.1:8065;
   keepalive 32;
}

#proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=mattermost_cache:10m max_size=3g inactive=120m use_temp_path=off;

server {
   listen 80;
   listen [::]:80;
   listen 443 ssl;
   listen [::]:443 ssl;

   server_name mattermost.example.com;

   ssl_certificate /etc/ssl/certs/mattermost.example.com.pem;
   ssl_certificate_key /etc/ssl/private/mattermost.example.com.pem;

   location ~ /api/v[0-9]+/(users/)?websocket$ {
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "upgrade";
       client_max_body_size 50M;
       proxy_set_header Host $http_host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
       proxy_set_header X-Frame-Options SAMEORIGIN;
       proxy_buffers 256 16k;
       proxy_buffer_size 16k;
       client_body_timeout 60;
       send_timeout 300;
       lingering_timeout 5;
       proxy_connect_timeout 90;
       proxy_send_timeout 300;
       proxy_read_timeout 90s;
       proxy_pass http://backend;
   }

   location / {
       client_max_body_size 50M;
       proxy_set_header Connection "";
       proxy_set_header Host $http_host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
       proxy_set_header X-Frame-Options SAMEORIGIN;
       proxy_buffers 256 16k;
       proxy_buffer_size 16k;
       proxy_read_timeout 600s;
       #proxy_cache mattermost_cache;
       #proxy_cache_revalidate on;
       #proxy_cache_min_uses 2;
       #proxy_cache_use_stale timeout;
       #proxy_cache_lock on;
       proxy_http_version 1.1;
       proxy_pass http://backend;
   }
}

~~~


Pour aller plus loin, il y a [la documentation](https://docs.mattermost.com/install/install-debian.html)

# Mise à jour

Voir [la documentation officielle](https://docs.mattermost.com/administration/upgrade.html#upgrading-to-the-latest-version) pour référence.

*TODO*

# Plugins

Mattermost est extensible avec de nombreux plugins.
La plupart du temps, il suffit de décompacter une archive dans le répertoire `plugins/` puis de l'activer dans la console système. Il est aussi possible d'uploader l'archive du plugin via la console système si cette option à été activée dans le `config.json`

# FAQ / Plomberie

## Configuration de l'outil CLI `mmctl`

Certaines opérations de maintenance se réalisent via l'outil en cli `mmctl`.
Celui-ci nécessite de se connecter avec un compte administrateur de l'instance Mattermost

~~~
$ mattermost/bin/mmctl auth login https://mattermost.example.com/
Connection name: default
Username: USERNAME
Password: PASSWORD

  credentials for "default": "USERNAME@https://mattermost.example.com" stored

~~~

## Recharger la configuration

Depuis quelques versions, Mattermost ne surveille plus le fichier de configuration pour y détecter les changements.
Il faut donc utiliser l'outil en CLI `mmctl` pour demander à Mattermost.

On le fait avec la commande `mattermost/bin/mmctl config reload` (après avoir configuré `mmctl`)

