# Howto Quota

Les quotas permettent de limiter l'usage disque (en taille et/ou inode) à des utilisateurs ou groupes.
Ils fonctionnent avec deux seuils, *soft* et *hard* (qu'on peut aussi appeller limite basse et hate).

* Le quota *hard* est une limite haute. L'utilisateur/groupe ne pourra pas dépasser cette limite.
* Le quota *soft* est une limite basse. L'utilisateur/groupe peut la dépasser. Néanmoins, il bénéficie d'un certain délai (période de grâce) pour déscendre en dessous de cette limite avant d'être bloqué (par défaut, 7 jours)

## Mise en place

~~~
# apt install quota
~~~

Pour l'activation des quotas, deux façons sont possibles.

A l'ancienne, avec les fichiers de quotas `aquota.user` et `aquota.group` ou dans les méta-données du système de fichier
si celui-ci le supporte.

Dans tous les cas, il faudra ajouter `usrquota` et/ou `grpquota` dans les options de montage partitions concernées dans le 
ficher `/etc/fstab` :

~~~
/dev/sdz /foo ext4 defaults,noexec,nosuid,nodev,usrquota,grpquota 0 2
~~~


### Activation directement dans ext4

Dans les méta-données du système de fichier, en ext4, ça nécessite une activation à la création du système de fichier.

~~~
# mkfs.ext4 -O quota -E quotatype=usrquota:grpquota /dev/sdz
~~~

C'est aussi activable après coup, mais il faudra démonter la partition.

~~~
# umount /foo
# tune2fs -O quota /dev/sdz
# tune2fs -Q usrquota,grpquota /dev/sdz
# mount /foo
~~~

### Avec les fichiers de quotas

Après avoir changé les options de montage pour rajouter les options `usrquota` et/ou `grpquota` : 

~~~
# mount -o remount /foo
# quotacheck -cufmv -F vfsv1 /foo  # /!\ écrase les seuils des quotas !!
# quotaon /foo
~~~




## Utilisation

Lister tous les quotas du serveur :

~~~
# repquota -auvg
~~~

Changer le quota d'un utilisateur *foo* sur *home* pour 1G (soft) et 2G (hard), sans limite d'inodes

~~~
## setquota --user <USER> <block-soft> <block-hard> <inode-soft> <inode-hard> <partition>
# setquota --user foo 1048576 2097152 0 0 /home
~~~

Changer le quota d'un groupe *bar* sur *home* pour 1G (soft) et 2G (hard), sans limite d'inodes

~~~
## setquota --user <USER> <block-soft> <block-hard> <inode-soft> <inode-hard> <partition>
# setquota --group bar 1048576 2097152 0 0 /home
~~~

Temps de grâce pour le quota de groupe :

~~~
# edquota -g -t
~~~


## (fichiers aquota.*) Restauration

Si vous avez une sauvegarde des fichiers `aquota.*` vous pouvez les restaurer en désactivant temporairement les quotas pour ce disque, puis enlever les attributs étendus de ces fichiers pour ensuite pour les remplacer par les fichiers sauvegardés.

Par exemple, pour restaurer les quotas utilisateur d'une partition /home, il faut faire :

```
quotaoff /home
chattr -iA /home/aquota.user
cp aquota.user.bak /home/aquota.user
chattr +iA /home/aquota.user
quotaon /home
```

## (fichiers aquota.*) Re-vérifier les quotas

Si vous avez restauré un backup du ficher de quotas, les valeurs d'occupation ne seront probablement pas à jour.

Pour lancer un check manuel :

~~~
quotaoff /home
quotacheck -m /home
quotaon /home
~~~

> Remarque : la commande `quotacheck` ne fonctionne que dans la situation avec des fichiers de quotas ()


### Forcer systemd-quotacheck a re-calculer les quotas

À partir de Debian 8 c'est systemd-quotacheck qui est utilisé au démarrage de la machine. Par défaut son exécution est très rapide, car il pense qu'il n'y a aucune différence entre l'utilisation disque et les quotas.  
Si on veut le forcer à re-calculer les quotas il faut indiquer le paramètre suivant lors du démarrage du noyau : `quotacheck.mode=force`

On peut le mettre via GRUB par exemple, via `/etc/default/grub`.


## Troubleshooting

### Valeurs d'utilisation plus élevées qu'attendu

Vérifier qu'il n'y a pas des fichiers appartenant à l'utilisateur en-dehors de son répertoire, dans tous le système de fichiers.

Par exemple avec find :

~~~
find /home -user USER|USERID -not -path '/home/USER/*'
~~~

Si `find` ne trouve rien, voir la section [Re-vérifier les quotas](#re-vérifier-les-quotas).


### Erreur : warnquota: Parse error at line 42. Cannot find end of group name

~~~
# warnquota -g
warnquota: Parse error at line 42. Cannot find end of group name.
~~~

Problème à la ligne 42 du fichier /etc/quotagrpadmins

