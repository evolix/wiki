---
categories: openbsd email
title: Howto OpenSMTPD
...

* Documentation : <https://www.opensmtpd.org/>
* Man pages : <https://man.openbsd.org/smtpd>

[OpenSMTPD](https://www.opensmtpd.org/) est un serveur mail intégré dans OpenBSD. Il est aussi disponible aussi sur Debian. Il comporte un nombre de fonctionnalités réduites qui ne le permettent pas de tout faire mais il convient dans la majorité des cas. Sa syntaxe se veut similaire à celle de [pf](HowtoOpenBSD/PacketFilter).


## Installation

Sous OpenBSD, OpenSMTPD est intégré au système de base.

Sous Debian :

~~~
# apt install opensmtpd opensmtpd-extras

# systemctl status opensmtpd
● opensmtpd.service - LSB: opensmtpd Mail Transport Agent
   Loaded: loaded (/etc/init.d/opensmtpd; generated; vendor preset: enabled)
     Docs: man:systemd-sysv-generator(8)
  Process: 24739 ExecStop=/etc/init.d/opensmtpd stop (code=exited, status=0/SUCCESS)
  Process: 24747 ExecStart=/etc/init.d/opensmtpd start (code=exited, status=0/SUCCESS)
    Tasks: 7 (limit: 4915)
   Memory: 9.1M
      CPU: 50ms
   CGroup: /system.slice/opensmtpd.service
           ├─24754 /usr/sbin/smtpd
           ├─24755 smtpd: klondike
           ├─24756 smtpd: control
           ├─24757 smtpd: lookup
           ├─24758 smtpd: pony express
           ├─24759 smtpd: queue
           └─24760 smtpd: scheduler
~~~

## Configuration de base

### OpenBSD

Sous OpenBSD, la configuration se trouve dans `/etc/mail/smtpd.conf` :

~~~
table aliases file:/etc/mail/aliases

# To accept external mail, replace with: listen on all
#
listen on lo0

action "mbox" mbox alias <aliases>
action "relay" relay

# Uncomment the following to accept external mail for domain "example.org"
#
# match from any for domain "example.org" action "mbox"
match for local action "mbox"
match from local for any action "relay"
~~~

Cette configuration permet de relayer juste les mails qui viennent de
la machine elle-même. Il n'y a donc pas de risque d'être un relai
ouvert.


### Debian

Sous Debian, la configuration se trouve dans `/etc/smtpd.conf` :

~~~
# To accept external mail, replace with: listen on all
listen on localhost

# If you edit the file, you have to run "smtpctl update table aliases"
table aliases file:/etc/aliases

action "mbox" mbox alias <aliases>
action "relay" relay

# Uncomment the following to accept external mail for domain "example.org"
#match from any for domain "example.org" action "mbox"

match for local action "mbox"
match for any action "relay"
~~~

ou pour Debian <11:

~~~
# To accept external mail, replace with: listen on all
listen on localhost

# If you edit the file, you have to run "smtpctl update table aliases"
table aliases file:/etc/aliases

# Uncomment the following to accept external mail for domain "example.org"
#accept from any for domain "example.org" alias <aliases> deliver to mbox

accept for local alias <aliases> deliver to mbox
accept for any relay
~~~

C'est une configuration similaire à OpenBSD.


## Configurations spécifiques

### Réécrire le From: pour tous les mails

On a ce cas quand on veut que les mails de root soient avec un autre
nom de domaine que le hostname.

~~~
action "relayAs" relay mail-from "root@example.com"
match from local for any action "relayAs"
~~~

Attention, dans ce cas cela ré-écrit même les messages envoyés avec un autre utilisateur.

### Utiliser un relai SMTP

On peut utiliser un relai en ajoutant le mot clé `host` dans la définition de l'action :

~~~
action "relayVia" relay host "192.0.2.25"
match from local for any action "relayVia"
~~~

On peut notamment utiliser cela pour contourner un blocage du port 25
et renvoyer sur un port différent (il faut évidemment que sur le relai
SMTP réponde sur ce port). Par exemple sur le port 2525 :

~~~
action "relayVia" relay host "192.0.2.25:2525"
match from local for any action "relayVia"
~~~


## Commandes smtpctl pratiques

### Obtenir des stats

~~~
# smtpctl show stats
control.session=1
uptime=297
uptime.human=4m57s
~~~ 

### Obtenir rapidement la taille de la queue

~~~
# smtpctl show stats | grep scheduler.envelope
~~~

### Voir la queue (équivalent de la commande `mailq`)

~~~
# smtpctl show queue
~~~

### Obtenir des détails sur un mail dans la queue 

~~~
# smtpctl show message 025a5c7b6cc771cb
~~~

### Forcer la redistribution de la queue

~~~
# smtpctl schedule all
~~~

### Supprimer tous les mails dans la queue

~~~
# smtpctl remove all
~~~
