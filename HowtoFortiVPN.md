---
title: How to FortiVPN
categories: network vpn security
---

# How to Fortinet SSLVPN sous linux

## Installation

~~~
# apt install network-manager-fortisslvpn network-manager-fortisslvpn-gnome network-manager-gnome
~~~

## Configuration

network-manager-fortisslvpn sert de frontend à [openfortivpn](https://github.com/adrienverge/openfortivpn) et a besoin de connaître le hash du certificat TLS utilisé par le serveur VPN, celui-ci peut être obtenu en utilisant directement openfortivpn pour se connecter au serveur:

~~~
# openfortivpn <ip_serveur_vpn>:<port> --username=<user>
VPN account password: <entrer son mot de passe>
ERROR:  Gateway certificate validation failed, and the certificate digest in not in the local whitelist. If you trust it, rerun with:
ERROR:      --trusted-cert [<hash>]
[...]
~~~

Une fois cette information obtenu il est possible de configuré la connexion VPN (uniquement en interface graphique):

- Lancer `nm-connection-editor`
- Cliquer sur le "+"
- Dans la liste déroulante sélectionner `Fortinet SSLVPN` et cliquer "Create"
- Dans General>Gateway mettre `<ip_serveur_vpn>:<port>`
- Dans Authentication>"User Name" mettre le nom d'utilisateur
- Pour Authentication>Password, soit:
  - Ne rien toucher, dans ce cas il faudra donner le mot de passe à chaque lancement du VPN (`nmcli connection up --ask <con-name>` pour la CLI)
  - Cliquer sur le symbole à droite de l'entrée de mot de passe et sélectionner "Store the password for all users" puis entré le mot de passe (ce plugin VPN ne semble pas supporté le stockage par utilisateur)
- Dans Advanced>Security>"Trusted Certificate" copié le hash obtenu précédemment
- Si souhaiter changer le nom de la connexion.
- Sauf si le VPN est censé être utilisé pour l'accès à internet aller dans l'onglet "IPv4 Settings" puis dans Routes cocher "Use this connection only for resources on its network"

## Démarrer le client VPN

Une fois la connexion définie dans NetworkManager il est possible de la démarré via la commande:

~~~
$ nmcli connection up <con-name>
~~~

Pour lancer le VPN sans utiliser NetworkManager il suffit de faire (en tant que root):

~~~
# openfortivpn <ip_serveur_vpn>:<port> --username=<user> --trusted-cert <hash>
VPN account password: <entrer son mot de passe>
~~~
