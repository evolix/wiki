---
toc: no
categories: sysadmin index
title: Base de connaissances Evolix
...

[Evolix](https://evolix.com/) est une entreprise spécialisée en Logiciels Libres basée à Marseille/Paris/Montréal.
Notre métier principal est l'hébergement et l'infogérance de serveurs, notamment l'**infogérance de serveurs Linux et BSD**.

Ce wiki a vocation à rassembler nos connaissances et conventions relatives à l'administration système et réseau et de les rendre libres && publiques.  
Notre ancien wiki (également public) se trouve sur <http://oldwiki.evolix.org/>

Les documentations sont rédigées pour fonctionner sous **Debian GNU/Linux version 12 _Bookworm_ (et compatible _Bullseye_ et _Buster_)** et sous **OpenBSD 7.3/7.4**.

**Attention**, les informations sont livrées sans garantie et peuvent être erronées ou incomplètes.  
Pour plus d'informations, consultez notre [offre d'Infogérance de serveurs](http://evolix.com/infogerance.html)


## Infogérance Linux

[BTRFS](HowtoBTRFS)  
[DRBD](HowtoDRBD)  
[ethtool](HowtoEthtool)  
[KVM](HowtoKVM)  
[LUKS](HowtoLUKS)  
[LVM](HowtoLVM)  
[Netfilter/iptables](HowtoNetfilter)  
[RAID logiciel](HowtoRAIDLogiciel)  
[Systemd](HowtoSystemd)  
[VRRP](HowtoVRRP)  


## Infogérance BSD

[CARP](HowtoOpenBSD/CARP)  
[Doas](HowtoDoas)  
[FlashInstall](HowtoOpenBSD/Flashinstall)  
[GestionDisques](HowtoOpenBSD/GestionDisques)  
[ISAKMPD](HowtoISAKMPD)  
[Netflow](HowtoOpenBSD/Netflow)  
[OpenBGPD](HowtoOpenBSD/OpenBGPD)  
[OSPF](HowtoOpenBSD/OSPF)  
[PacketFilter](HowtoOpenBSD/PacketFilter)  
[PFSYNC](HowtoOpenBSD/PFSYNC)  
[PFStat](HowtoOpenBSD/PFStat)  
[ProxyFTP](HowtoOpenBSD/ProxyFTP)  
[Reseau](HowtoOpenBSD/Reseau)  
[RAID1](HowtoOpenBSD/RAID1)  
[VLAN](HowtoOpenBSD/VLAN)  
[VRF-rtable-rdomain](HowtoOpenBSD/VRF-rtable-rdomain)  


## Infogérance services

###### A
[Amavis](HowtoAmavis)  
[Ansible](HowtoAnsible)  
[Apache](HowtoApache)  

###### B
[Bind](HowtoBind)

###### C
[Cron](HowtoCron)  
[CUPS](HowtoCUPS)

###### D
[Dovecot](HowtoDovecot)  
[DUC](HowtoDUC)

###### E
[Elasticsearch](HowtoElasticsearch)  
[Suite ELK](HowtoELK)  
[Exim](HowtoExim)  
[Extfs](TipsExtfs)

###### F
[Fail2Ban](HowtoFail2Ban)

###### G
[Gerrit](HowtoGerrit)  
[Git](HowtoGit)  
[Gitit](HowtoGitit)  
[Gitolite](HowtoGitolite)  
[GitLab](HowtoGitlab/11)

###### H
[HAProxy](HowtoHaproxy)  
[Htop](HowtoHtop)

###### I
[Icinga](HowtoIcinga)  
[Iperf](HowtoIperf)

###### J
[Jitsi](HowtoJitsi)

###### K
[KeePassX](HowtoKeePassX)  

###### L
[Let's Encrypt](HowtoLetsEncrypt)  
[Log2mail](HowtoLog2mail)  
[Logcheck](HowtoLogcheck)

###### M
[Mastodon](HowtoMastodon)  
[Memcached](HowtoMemcached)  
[Monit](HowtoMonit)  
[MTR](HowtoMTR)  
[MySQL](HowtoMySQL)

###### N
[New Relic](HowtoNewRelic)  
[Nfdump](Howtonfdump)  
[Nginx](HowtoNginx)  
[NFS](HowtoNFS)  
[NodeJS](HowtoNodeJS)  
[Nut](HowtoNut)

###### O
[OpenBGPD](HowtoOpenBSD/OpenBGPD)  
[OpenVPN](HowtoOpenVPN)  
[OpenSSH](HowtoOpenSSH)  
[OpenSMTPD](HowtoOpenSMTPD)  

###### P
[PHP](HowtoPHP)  
[Postfix](HowtoPostfix)  
[PostgreSQL](HowtoPostgreSQL)  
[ProFTPD](HowtoProFTPD)

###### R
[Redis](HowtoRedis)  
[Rsync](HowtoRsync)  
[Rsyslog](HowtoRsyslog)

###### S
[Screen](HowtoScreen)  
[Smart](HowtoSmart)  
[SSL](HowtoSSL)  
[SQLite](HowtoSQLite)  
[Squid](HowtoSquid)  
[Sudo](HowtoSudo)  
[SwitchCisco](SwitchCisco)  
[Systemd](HowtoSystemd)  

###### U
[Unbound](HowtoUnbound)  

###### V
[Varnish](HowtoVarnish)  

###### Z
[Zimbra](HowtoZimbra)  

## Best Practices

....

## Trucs & Astuces

[Checkconf](HowtoCheckconf)  
[cURL](HowtocURL)  
[Symfony](PracticeSymfony)  
[RIPE](HowtoRIPE)
