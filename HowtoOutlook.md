**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Outlook

## Application courrier sur Windows10

Pour modifié les pamamètres du serveur une fois que le compte a été ajouter à courrier, cela peux se faire dans :

* Clic droit sur le nom du compte -> Paramètre du compte 
  * Dans la fenêtre qui s'ouvre, cliquez sur **Modifié les paramètres de configuration de boites aux lettres**
  * Dans la fenêtre qui s'ouvre, cliquez sur **Paramètres avancés de boites aux lettres**

Paramètres de bases pour configurer un compte mail sécurisé en IMAP + SMTP :

Serveur de messagerie entrant :

* Nom du serveur, Port 993, cochez la case **Exigez le protocole SSL pour le courrier entrant**

Serveur de messagerie sortant :

* Nom du serveur, Port 25, cochez la case **Exigez le protocole SSL pour le courrier sortant**

## Code d'erreur 0x8007139f lors de l'ajout d'un compte mail dans courrier / outlook

Si vous avez le code d'erreur **0x8007139f** lors qu'un ajout d'un compte mail dans courrier / outlook, il faut vérifié dans les paramètres que Windows10 que les applications de mail sont bein autorisé dans le système :

* Paramètres -> Confidentialité -> Autorisé les applications a accèder à vos email -> cochez Activé
  * Vérifié dans **Choisir les applications qui peuvent acceder a vos emails** si l'application est bien activé. 

## Outlook 2013

Souci d'envoi d'emails ?

<http://www.msoutlook.info/question/736,> § Root your mailbox
