---
categories: web
title: Howto Collabora Code
...

* Documentation : <https://collaboraonline.github.io/docs/>
* Statut de cette page : test / bullseye

[Collabora Online Development Edition (CODE)](https://www.collaboraoffice.com/code/) est une suite bureautique utilisable dans son navigateur. Le logiciel est basé sur [Libre Office Online](https://www.libreoffice.org/download/libreoffice-online/)

## Installation

Collabora propose des paquets via leur dépôt pour installer le logiciel : 

~~~
# echo 'deb https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-debian11 ./' >> /etc/apt/sources.list.d/collabora-code.list
# cd /etc/apt/trusted.gpg.d
# wget https://collaboraoffice.com/downloads/gpg/collaboraonline-release-keyring.gpg
# chmod 644 collaboraonline-release-keyring.gpg
# apt update 

# apt install coolwsd code-brand collaboraofficebasis-fr collaboraoffice-dict-fr
~~~

Attention, l'installation se fait dans `/opt/collaboraonline` et `/opt/cool`, donc prévoyez de la place pour ça (par exemple créer un lien symbolique `/opt -> /srv/opt`)

Une fois installé, vous devrez d'abord configurer les "SSL settings" soit en générant un certificat, soit en désactivant la terminaison SSL. Vous pourrez ensuite démarrer le service :

~~~
# systemctl status coolwsd.service
● coolwsd.service - Collabora Online WebSocket Daemon
   Loaded: loaded (/lib/systemd/system/coolwsd.service; enabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/coolwsd.service.d
           └─override.conf
   Active: active (running) since Thu 2022-04-28 20:47:24 CEST; 2 weeks 4 days ago
 Main PID: 3985 (coolwsd)
    Tasks: 17 (limit: 4701)
   Memory: 437.9M
   CGroup: /system.slice/coolwsd.service
           ├─  465 /usr/bin/coolforkit --losubpath=lo --systemplate=/opt/cool/systemplate --lotemplate=/opt/collaboraoffice --childroot=/opt/cool/child-roots/ --clientport=9980 --masterport=coolwsd-9PINiUpT --rlimits=limit_virt_mem_mb:0;li
           ├─ 3985 /usr/bin/coolwsd --version --o:sys_template_path=/opt/cool/systemplate --o:child_root_path=/opt/cool/child-roots --o:file_server_root_path=/usr/share/coolwsd
           ├─ 4150 /usr/bin/coolforkit --losubpath=lo --systemplate=/opt/cool/systemplate --lotemplate=/opt/collaboraoffice --childroot=/opt/cool/child-roots/ --clientport=9980 --masterport=coolwsd-9PINiUpT --rlimits=limit_virt_mem_mb:0;li
           ├─13273 /usr/bin/coolforkit --losubpath=lo --systemplate=/opt/cool/systemplate --lotemplate=/opt/collaboraoffice --childroot=/opt/cool/child-roots/ --clientport=9980 --masterport=coolwsd-9PINiUpT --rlimits=limit_virt_mem_mb:0;li
           └─18537 /usr/bin/coolforkit --losubpath=lo --systemplate=/opt/cool/systemplate --lotemplate=/opt/collaboraoffice --childroot=/opt/cool/child-roots/ --clientport=9980 --masterport=coolwsd-9PINiUpT --rlimits=limit_virt_mem_mb:0;li
~~~

Pour les versions de Debian antérieures à Debian 10, tout est expliqué sur [leur site](https://www.collaboraoffice.com/code/linux-packages/)


## Configuration

Fichiers de configuration :

~~~
/etc/coolwsd/
├── coolkitconfig.xcu
└── coolwsd.xml
~~~

Le fichier de configuration principal se trouve être `/etc/coolwsd/coolwsd.xml`. Juste après installation, le service `coolwsd.service` ne fonctionne pas car la configuration du service écoute en HTTPS par défaut, mais aucun certificat n'est présent.

Il est plus simple d'utiliser un serveur web comme [Nginx](HowtoNginx) en reverse proxy en amont pour faire la terminaison SSL avant de transférer le trafic à collabora-code localement, sans HTTPS.

Pour cela, il faut passer changer les paramètres suivants de la section `<ssl>` : 

* `<enable>` : de *true* à **false**
* `<termination>` de *false* à **true**


~~~.xml

(...)
    <ssl desc="SSL settings">
        <enable type="bool" desc="Controls whether SSL encryption is enable (do not disable for production deployment). If default is false, must first be compiled with SSL support to enable." default="true">false</enable>
        <termination desc="Connection via proxy where loolwsd acts as working via https, but actually uses http." type="bool" default="true">true</termination>

(...)
~~~



Côté Nginx, on utilisera le vhost proposé par les [développeurs de collabora code](https://www.collaboraoffice.com/code/nginx-reverse-proxy/) : 

~~~
server {
    listen       443 ssl;
    server_name  collabora.example.org;

    ssl_certificate /etc/letsencrypt/live/collabora00.evolix.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/collabora00.evolix.org/privkey.pem;

    root /var/www/collabora00.evolix.org;

    access_log  /var/log/nginx/access.collabora00.evolix.org.log;
    error_log  /var/log/nginx/error.collabora00.evolix.org.log;


    # static files
    location ^~ /loleaflet {
        proxy_pass http://localhost:9980;
        proxy_set_header Host $http_host;
    }

    # WOPI discovery URL
    location ^~ /hosting/discovery {
        proxy_pass http://localhost:9980;
        proxy_set_header Host $http_host;
    }

    # Capabilities
    location ^~ /hosting/capabilities {
        proxy_pass http://localhost:9980;
        proxy_set_header Host $http_host;
    }

    # main websocket
    location ~ ^/lool/(.*)/ws$ {
        proxy_pass http://localhost:9980;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $http_host;
        proxy_read_timeout 36000s;
    }

    # download, presentation and image upload
    location ~ ^/lool {
        proxy_pass http://localhost:9980;
        proxy_set_header Host $http_host;
    }

    # Admin Console websocket
    location ^~ /lool/adminws {
        proxy_pass http://localhost:9980;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $http_host;
        proxy_read_timeout 36000s;
    }
}
~~~

Avec [Apache](HowtoApache) voici un exemple de VirtualHost :

~~~
<VirtualHost *:443>
    ServerName collabora.example.com:443
    ServerAlias collabora.example.com
    #ServerAlias office.france-palestine.org

    # SSL
    IncludeOptional /etc/apache2/ssl/collabora.con[f]

    # Encoded slashes need to be allowed
    AllowEncodedSlashes NoDecode

    ProxyPreserveHost On

    # static html, js, images, etc. served from loolwsd
    # loleaflet is the client part of LibreOffice Online
    ProxyPass           /loleaflet http://127.0.0.1:9980/loleaflet retry=0
    ProxyPassReverse    /loleaflet http://127.0.0.1:9980/loleaflet

    # WOPI discovery URL
    ProxyPass           /hosting/discovery http://127.0.0.1:9980/hosting/discovery retry=0
    ProxyPassReverse    /hosting/discovery http://127.0.0.1:9980/hosting/discovery

    # Main websocket
    ProxyPassMatch "/lool/(.*)/ws$" ws://127.0.0.1:9980/lool/$1/ws nocanon

    # Admin Console websocket
    ProxyPass   /lool/adminws ws://127.0.0.1:9980/lool/adminws

    # Download as, Fullscreen presentation and Image upload operations
    ProxyPass           /lool http://127.0.0.1:9980/lool
    ProxyPassReverse    /lool http://127.0.0.1:9980/lool

    # Endpoint with information about availability of various features
    ProxyPass           /hosting/capabilities http://127.0.0.1:9980/hosting/capabilities retry=0
    ProxyPassReverse    /hosting/capabilities http://127.0.0.1:9980/hosting/capabilities
</VirtualHost>
~~~

## Utilisation

#### Avec Nextcloud et le connecteur Collabora

Dans Nextcloud, il y a une application nommée ["Collabora Online"](https://apps.nextcloud.com/apps/richdocuments) permet de faire connecteur entre l'installation Nextcloud et le service Collabora Code fraîchement installé. Cela permettra de consulter et modifier des fichiers directement depuis l'interface de Nextcloud.

##### Configuration dans Nextcloud

##### Configuration dans Collabora Code

Dans le fichier de configuration `/etc/coolwsd/coolwsd.xml`, il faut autoriser l'adresse de l'instance Nextcloud à travailler avec Collabora.

Dans la sous-section `<wopi>` de la section `<storage>`, il faut rajouter un élément `<host></host>` qui contient une regex d'hôtes à autoriser. Par défaut, seul le premier élément `<host></host>` est pris en compte.

~~~.xml

(...)

 <storage desc="Backend storage">
        <wopi desc="Allow/deny wopi storage. Mutually exclusive with webdav." allow="true">
            <host desc="Regex pattern of hostname to allow or deny." allow="true">sharedcloud[0-9]{4}.example.org</host>

(...)
~~~

Pour autoriser plusieurs hôtes, il faut modifier `<alias_groups>` de `mode=first` à `mode=groups` et modifier son contenu :

~~~.xml
 <storage desc="Backend storage">
        <wopi desc="Allow/deny wopi storage. Mutually exclusive with webdav." allow="true">
            <alias_groups desc="default mode is 'first' it allows only the first host when groups are not defined. set mode to 'groups' and define group to allow multiple host and its aliases" mode="groups">
                <group>
                    <host desc="Regex pattern of hostname to allow or deny." allow="true">https://nextcloud.example.com:443</host>
                </group>

                <group>
                    <host desc="Regex pattern of hostname to allow or deny." allow="true">https://servername.nextcloud.example:443</host>
                    <alias desc="regex pattern of aliasname">https://alias[0-9].nextcloud.example:443</alias>
                </group>
            </alias_group>
~~~

## Monitoring

### Nagios

## FAQ

