---
categories: sysadmin
title: Howto Log2mail
...

**Log2mail** est un démon qui surveille en permanence une liste de journaux et qui envoie immédiatement un email si certains termes apparaissent dans ce fichier. C'est un complément idéal à [Logcheck](HowtoLogcheck) qui permet d'alimenter la liste des termes considérés comme anormaux. *Log2mail* était présent dans Debian pendant plusieurs années, il a été retiré depuis Debian 7 mais nous l'utilisons toujours car le [code source](https://github.com/fumiyas/log2mail) est simple et nous n'avons pas trouvé d'équivalent.


## Installation

Un paquet _.deb_ pour Debian 7, 8 et 9 est disponible sur <http://pub.evolix.org>.
Pour Debian 9 :

~~~
$ wget https://pub.evolix.org/evolix/pool/main/l/log2mail/log2mail_0.3.0-2_amd64.deb
# dpkg -i log2mail_0.3.0-2_amd64.deb
~~~

Ce paquet log2mail ne fournit pas encore d'unité systemd, on pourra créer l'unité suivante dans /etc/systemd/system/log2mail.service :

~~~
[Unit]
Description=Daemon watching logfiles and mailing lines matching patterns
After=network.target

[Service]
Type=forking
ExecStart=/usr/sbin/log2mail -- -f /etc/log2mail/config
KillMode=control-group
Restart=always
User=log2mail
Group=adm

[Install]
WantedBy=multi-user.target
~~~

## Configuration

Sa configuration principale se déroule dans le fichier `/etc/log2mail/config/default`.
On peut ajouter de la configuration dans ce fichier ou créer d'autres fichiers dans le dossier `/etc/log2mail/config/` (attention à bien ajuster les droits sur ces fichiers).

Par exemple, si vous voulez préciser que vous voulez recevoir un mail dès que le terme _fatal_
apparaît dans le fichier `/var/log/mail.log`, ajoutez ces lignes :

~~~
file = /var/log/mail.log
  pattern = "fatal"
  mailto = alert@example.com
~~~

De façon facultative vous pouvez ajouter l'option ` template = /etc/log2mail/template.mail-fatal` et créer un fichier template `/etc/log2mail/template.mail-fatal` pour le mail qui sera envoyé (par défaut, il y a un template dans `/etc/log2mail/mail`) :

~~~
From: %f
To: %t
Subject: [LOG2MAIL] Erreur fatale pour mail.log

Bonjour,

Nous avons reconnu le terme "%m" dans "%F" %n fois :
%l

Merci.
~~~

On peut préciser plusieurs patterns pour un même fichier ainsi :

~~~
file = /var/log/mail.log
  pattern = "fatal"
  mailto = alert@example.com

  pattern = "error"
  mailto = alert@example.com
~~~


Attention, *Log2mail* est assez basique… si il n'a pas les droits d'accès à
un fichier déclaré dans sa configuration, il plante !! On prendra donc bien
garde à bien vérifier les droits d'accès.

Pour des logs système, on devra ainsi souvent faire :

~~~
# adduser log2mail adm
~~~

Après toutes modifications, il ne faut pas oublier de redémarrer le daemon :

~~~
# systemctl restart log2mail
~~~

Et vérifier le process avec `ps aux` :

~~~
$ ps aux | grep log2mail
~~~

Pour avoir plusieurs destinataires, on définit la variable _mailto_ ainsi :

~~~
mailto = "destinataire1@mail.com destinataire2@mail.com"
~~~


## Configuration avancée

log2mail détecte les _pattern_ avec le code source suivant (ligne 25 de `data.cc`) :

~~~
int inPattern::matches(const string& aLine) {
  return (regexec(&rPattern, aLine.c_str(), 0, 0, 0) == 0);
}
~~~

D'après le man de _regexec(3)_ :

~~~
POSIX Regex Matching
    regexec() is used to match a null-terminated string against the precom?
    piled  pattern  buffer,  preg.   nmatch  and pmatch are used to provide
    information regarding the location of any matches.  eflags may  be  the
    bitwise-or  of  one  or  both  of REG_NOTBOL and REG_NOTEOL which cause
    changes in matching behavior described below.
~~~

On peut donc utiliser les [regex POSIX](http://en.wikipedia.org/wiki/Regular_expression#POSIX)

Voici quelques exemples testés :

~~~
# N'importe quelle ligne
pattern = ".*"
# Debut de ligne
pattern = "^ERROR"
~~~

Dans un mode plus avancé, on peut peut astucieusement combiner l'utilisation de *Log2mail* avec *Procmail*, une première détection sera faite avec *Log2mail* puis envoyé vers un email géré par *Procmail* pour un filtrage encore plus fin des alertes.


## FAQ

### Debug

L'option `-N` permet de forcer l'écriture des erreurs sur STDERR et que le process ne soit pas daemonizé (mis en tâche de fond) :

~~~
# /usr/sbin/log2mail -N -f /etc/log2mail/config/
~~~

### Log2mail VS Fail2Ban

Peut-on remplacer Log2mail par [Fail2Ban](HowtoFail2Ban) ? En effet, Fail2Ban permet de détecter des patterns, et de lancer des actions diverses, notamment envoyer un email. Malheureusement, Fail2Ban veut absolument détecter une adresse IP (variable _<HOST>_) ce qui le rend inutilisable dans un cas général.
