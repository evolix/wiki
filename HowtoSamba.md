---
title: Howto Samba
...

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Samba

(Description)
- Listen port 139 et requete sur port 445 over TCP

## Installation

~~~
# aptitude install samba
~~~

TODO...

## Configuration

Attention : la configuration est rechargée *automatiquement* toutes les 60s ...ce qui est pratique mais peut avoir des effets indésirables !

/etc/samba/smb.conf

## Installer un BDC (Backup Domain Controler)

<http://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/samba-bdc.html>

<http://contribs.martymac.org/samba2.2-Ldap/html/7/>

1. Configurer une réplication LDAP master -> slave (note: s'assurer que les attributs de mot de passe sont bien synchroniser)

2. Configurer *smb.conf* sur le BDC ainsi :

~~~
passdb backend = ldapsam:ldap://127.0.0.1
domain master = no
domain logons = yes
~~~

3. Sur le BDC, bien penser à également positionner le mot de passe LDAP : smbpasswd -W

4. Joindre le BDC au domaine via :

~~~
# net rpc join -S PDC -Uadmin
Enter admin's password:
Joined domain DOMAIN.
~~~

Attention, il peut être nécessaire de répéter l'opération 2 fois (bien s'assurer que la machine bdc$ a été créée entre temps).

5. Forcer le SID du BDC :

~~~
# net rpc getsid -S PDC
Storing SID S-1-5-21-123-456-789 for Domain DOMAIN in secrets.tdb
# net setlocalsid S-1-5-21-123-456-789
# /etc/init.d/samba stop && /etc/init.d/samba start
~~~

## Intégration des postes

Note importante : pour joindre un poste à un domaine Microsoft, il faut impérativement
qu'il ait une version Pro (pas de version Familiale !) : Windows XP Pro ou Windows Seven Pro.

### Windows XP Pro

<http://www.gcolpart.com/howto/samba.php4#clients>

### Windows Seven Pro

Procédure dispo ici : <https://wiki.samba.org/index.php/Windows7>

### Windows 10

ça marche, en changeant les mêmes clés de registre que sous Windows 7, cf <https://wiki.samba.org/index.php/Windows7>

## Tester la connexion

Les test vont se faire avec la commande smbclient :

~~~
# apt install smbclient
~~~

Lister les partages :

~~~
smbclient -L localhost -U username
~~~

Se connecter a un partage :

~~~
smbclient //localhost/partage -U username
~~~

## Vérifier le status du serveur

Pour voir les connexions et les fichiers ouverts par les utilisateurs, la commande `smbstatus` permet de retourner cela.

~~~
# smbstatus
~~~

## FAQ

### NT_STATUS_ACCESS_DENIED lors de la jonction à un domaine

Si vous avez quelque chose ressemblant à ceci : 

~~~
[2012/01/24 17:24:17.352908,  4] smbd/reply.c:786(reply_tcon_and_X) Client requested device type [?????] for share [IPC$]
[2012/01/24 17:24:17.352943,  3] lib/access.c:389(check_access) check_access: no hostnames in host allow/deny list.
[2012/01/24 17:24:17.353015,  2] lib/access.c:406(check_access) Allowed connection from  (192.168.0.111)
[2012/01/24 17:24:17.353510,  4] passdb/pdb_ldap.c:1601(ldapsam_getsampwnam) ldapsam_getsampwnam: Unable to locate user [root] count=0
[2012/01/24 17:24:17.353856,  2] smbd/service.c:598(create_connection_server_info) user 'myadmin' (from session setup) not permitted to access this share (ipc$)
create_connection_server_info failed: NT_STATUS_ACCESS_DENIED
[2012/01/24 17:24:17.353891,  3] smbd/error.c:80(error_packet_set)
error packet at smbd/reply.c(795) cmd=117 (SMBtconX) NT_STATUS_ACCESS_DENIED
~~~

Alors il est très probable que cela soit du à  :

~~~
invalid users           = root
~~~

### Expiration d'un compte

Expiration d'un compte (avec smbldap-tools) : smbldap-usermod -e YYYY-MM-DD <user>

Forcer le changement de mot de passe : _smbldap-usermod -B 1 <user>_

### Mise en place d'une « corbeille » sur un partage réseau

Il est possible de faire en sorte que les fichiers supprimés par les utilisateurs sur un partage soient déplacés dans un répertoire spécifique,
de sorte qu'ils puissent les récupérer en cas d'erreur. Cela se fait avec le module _recycle_ de Samba.

Exemple de configartion (à rajouter dans chacun des partages si besoin) :

~~~
vfs object = recycle
        recycle:repository = .trash/%U
        recycle:keeptree = Yes
        recycle:touch = Yes
        recycle:versions = Yes
        recycle:maxsixe = 0
        #recycle:exclude = *.tmp
        #recycle:exclude_dir = /tmp
~~~

Les fichiers supprimés seront déplacés dans le répertoire _.trash_ relatif au partage, et rangés dans un répertoire au nom de l'utilisateur qui l'a supprimé.
Si le fichier existe déjà, une copie est fait en _Copy #x of filename_ (grâce à l'option _recycle:versions_).

Pour plus d'informations sur les options possible : <http://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/VFS.html#id2651247>

Les fichiers conservés ne sont pas limités dans le temps. Pour cela on pourra mettre un cron qui nettoiera le répertoire concerné :

~~~
32 4 * * * find /home/samba/foo/.trash/ -type f -atime +7 -delete
~~~

Bien s'assurer dans ce cas que l'option _recycle:touch_ est active pour que la date d'accès soit mise à jour lors du déplacement du fichier.

### Synchronisation des mots de passe Unix vers Samba

Le but ici est de faire en sorte que lors d'un changement de mot de passe avec la commande `passwd`, le changement soit aussi appliqué sur le mot de passe Samba.

* Installation du module PAM nécessaire :

~~~
# aptitude install libpam-smbpass
~~~

* Ajout du module dans le fichier _/etc/pam.d/common-password_ :

~~~
password   required     pam_smbpass.so nullok use_authtok try_first_pass

~~~
Il faut aussi modifier dans le même fichier la ligne concernant le module pam_ldap (ou pam_unix si LDAP n'est pas utilisé) pour passer _required_ à _requisite_ (force à se terminer à la première erreur, pour éviter que les 2 mots de passe ne soient désynchronisés) :

~~~
#password   required     pam_ldap.so use_first_pass
password   requisite    pam_ldap.so use_first_pass
~~~

* Ajout du module dans le fichier _/etc/pam.d/common-auth_ :

~~~
auth    optional        pam_smbpass.so migrate
~~~

Note : pour que l'utilisateur puisse changer son mot de passe, il faut que les 2 mots de passe soient déjà synchronisés.

### Import des comptes d'un fichier smbpasswd vers la db interne à Samba (passdb.tdb)

~~~
# pdbedit -i smbpasswd
~~~

### Forcer un changement de mot de passe utilisateur

Pour qu'un utilisateur soit invité à changer son mot de passe à la prochaine connexion, il faut forcer l'attribut _sambaPwdMustChange_ à _0_. À partir de Samba 3.2, cela ne suffit plus, et il faut également définir _sambaPwdLastSet_ à _0_.

L'utilisateur serra averti par un message Windows comme quoi son mot de passe expire aujourd'hui, mais il aura toujours la possibilité de refuser de le changer et de reporter le changement indéfiniment.
Pour éviter cela, il faut retirer le flag _X_ du champs _sambaAcctFlags_, pour réellement faire expirer le mot de passe.

Au final, ça donne :

~~~
sambaPwdLastSet: 0
sambaPwdMustChange: 0
sambaAcctFlags: [U          ]
~~~


### Erreur Rejecting auth request from client CLIENT machine account CLIENT$

Windows gère des "credentials machine" c'est-à-dire une sorte de mot de passe associé à la machine.
Tous les 30 jours (au moins), il le renouvelle et doit donc transmettre son nouveau mot de passe à Samba (il est inscrit dans LDAP dans l'attribut "sambaNTPassword" de l'entrée machine CLIENT$).

Si il y a un souci de synchronisation (erreur réseau au démarrage, erreur temporaire, etc.) il peut être nécessaire de réinitialiser ces infos.
Deux possibilités :

- supprimer les attributs sambaNTPassword et sambaPwdLastSet associées à MACHINE$ et redémarrer la machine
- sortir la machine du domaine, effacer l'entrée LDAP associée à la machine, et rentrer à nouveau la machine dans le domaine

Note : si besoin (mais c'est moins sécurisé...), il est possible de désactiver ce renouvellement de mot de passe machine via une clé de registre :

~~~
Client-Registry: [HKLM\SYSTEM\CurrentControlSet\Services\Netlogon\Parameters] "DisablePasswordChange"=dword:00000001
~~~

### Tuer un client

Pour une raison ou une autre on peut vouloir tuer les connexions d'un client. On peut le faire avec smbcontrol.

~~~
# smbcontrol kill-client-ip 192.0.2.1
~~~

### NT_STATUS_IO_TIMEOUT

~~~
$ smbclient //localhost/partage -U username

smb: \> put filename
cli_push returned NT_STATUS_IO_TIMEOUT
putting file filename as \filename (1044.8 kb/s) (average 1044.8 kb/s)
~~~

Cette erreur arrive généralement lors d'un transfert avec un bas débit. Smbclient utilise un buffer, qui envoie un timeout s'il n'est pas rempli au bout de 20 secondes. On peut réduire la taille de ce buffer ponctuellement :

~~~
smb: \> iosize 
iosize <n> or iosize 0x<n>. Minimum is 0 (default), max is 16776960 (0xFFFF00)
smb: \> iosize 16384
~~~

### Lenteur à travers OpenVPN

Il arrive que samba soit plus lent, ou même que des fichiers volumineux ne réussissent pas à être transmis sur un partage samba à travers OpenVPN.
Pour résoudre le problème, il faut enlever un paramètre par défaut de la configuration /etc/samba/smb.conf :

~~~
#socket options = TCP_NODELAY SO_RCVBUF=8192 SO_SNDBUF=8192
socket options =  TCP_NODELAY
~~~

Il faut ensuite redémarrer le service samba pour prendre en compte cette modification réseau.

La [documentation de samba](https://www.samba.org/samba/docs/old/Samba3-HOWTO/speed.html) elle-même indique que cette option peut poser des problèmes de performances.

# Installation et Configuration de Samba sans LDAP


## Installation

~~~
# aptitude install samba
~~~

Sur Debian 6 Squeeze il est nécessaire d'installer aussi les paquets suivants:

~~~
# aptitude install samba-common samba-tools
~~~


## Configuration
La configuration du serveur se fait dans le fichier */etc/samba/smb.conf*:

~~~
[global]
        workgroup = Nom_du_groupe
        netbios name = Nom_du_serveur
        security = user
        interfaces = eth0
        syslog = 0
        log file = /var/log/samba/log.%m
        log level = 1
        max log size = 1000

[NOM_DU_PARTAGE]
        path = /home/chemin_du_dossier
        valid users = nom_du_user_samba
        create mask = 0777
        force create mode = 0777
        directory mask = 0777
        force directory mode = 0777
        browseable = yes
        comment = Commentaire_sur_le_partage
        writable = yes
        write list = liste_des_users_autorisé

~~~


## Ajout d'un utilisateur Samba
L'ajout d'un utilisateur Samba se fait avec la commande suivante:

~~~
smbpasswd -a nom_de_l'utilisateur
~~~

Un mot de passe pour cet utilisateur est demandé, l’utilisateur crée doit déjà exister en tant que utilisateur UNIX.


