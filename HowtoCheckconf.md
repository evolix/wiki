---
categories: system
title: Howto Checkconf
...

Avant de redémarrer un daemon, il vaut mieux s'assurer que sa
configuration est correcte. Cette page se veut être une cheat sheet
des différentes commandes existantes.

Le classement est fait par rapport au port réservé pour le logiciel
puis par ordre alphabétique.

## sudo

~~~
# visudo -cf /etc/sudoers
~~~


## FTP

### proftpd

~~~
# proftpd -t
Checking syntax of configuration file
Syntax check complete.
~~~

## SSH

### OpenSSH

Pour valider la configuration du serveur SSH :

~~~
# sshd -t -f /etc/ssh/sshd_config
~~~

Même chose, en affichant la configuration sur la sortie standard :

~~~
# sshd -T -f /etc/ssh/sshd_config
~~~

On peut aussi préciser des paramètres de connexion avec [l'option `-C`](https://manpages.debian.org/bookworm/openssh-server/sshd.8.en.html#C) :

~~~
# sshd -T -C addr=<mon-ip>,user=<mon-utilisateur>
~~~

Pour valider une clé publique :

~~~
# ssh-keygen -l -f .ssh/id_rsa.pub
~~~

## Mail

### OpenSMTPD


~~~
# smtpd -n
~~~

### Postfix

~~~
# postconf >/dev/null
~~~

### Spamassasin

~~~
spamassassin --lint
~~~

## DNS

### Bind

~~~
# named-checkzone domain /path/to/zone
# named-checkconf /etc/bind/named.conf
~~~

### NSD

~~~
# nsd-checkconf /var/nsd/etc/nsd.conf
~~~

### Unbound

~~~
# unbound-checkconf /var/unbound/etc/unbound.conf
~~~

## MySQL

~~~
# /usr/sbin/mysqld --help
~~~

## MaxScale

~~~
$ maxscale --config-check --config /path/to/config.cnf
~~~

Il peut être nécessaire de spécifier l'utilisateur qui doit jouer la commande, avec l'option `--user=<USER>`.

## DHCP

### dhcpd

~~~
# dhcpd -n
~~~

### isc-dhcp-server

~~~
# dhcpd -t
~~~

## HTTP

### Apache

~~~
# apachectl -t
# apachectl configtest
~~~

### Haproxy

~~~
# haproxy -c -f /etc/haproxy/haproxy.cfg
~~~

### httpd

~~~
# httpd -dnv
~~~

### Fail2ban

~~~
# fail2ban-regex /chemin/fichier.log /etc/fail2ban/filter.d/config-exclusion.conf
~~~

### DRBD

~~~
# drbdadm dump
~~~

### Logrotate

~~~
# logrotate /etc/logrotate.d/
# logrotate -vdf /etc/logrotate.d/config-file
~~~

### Nginx

~~~
# nginx -t
~~~

### Php

~~~
# php5-fpm -t
~~~

### Varnish

~~~
# sudo -u varnish varnishd -Cf /etc/varnish/default.vcl
~~~

Dans certain cas, il est possible d'obtenir l'erreur `cannot open shared object file: Permission denied`, essayez de relancer la commande après avoir mis votre umask à `0007`.

Ça peut aussi être car le dossier temporaire utilisé est monté en `noexec`. Dans ce cas on peut créer le dossier `/var/tmp-vcache` :

~~~
# mkdir /var/tmp-vcache
# chown vcache:varnish /var/tmp-vcache
# chmod 750 /var/tmp-vcache
~~~

… et l'utiliser dans la commande de test avec l'utilisateur qui fait tourner varnish

~~~
# sudo -u vcache TMPDIR=/var/tmp-vcache varnishd -Cf /etc/varnish/default.vcl > /dev/null
~~~

### Samba

~~~
# testparm
~~~

### Squid3

~~~
# squid -k parse
~~~

## Imap / pop

### Dovecot

~~~
# doveconf -n > /dev/null
~~~

## BGP

### OpenBGPD

~~~
# bgpd -nf /etc/bgpd.conf
~~~


## OSPF

### OpenOSPFD

~~~
# ospfd -nf /etc/ospfd.conf
# ospf6d -nf /etc/ospf6d.conf
~~~

## IRC

### Charybdis

~~~
$ charybdis-ircd -conftest
~~~

## Networking

~~~
# ifup --no-act eth0
~~~


## CUPS

~~~
# cupsd -t -c test.conf
~~~

## PHP-FPM

Changer la version en fonction.

~~~
# php-fpm7.3 -t
~~~

## /etc/fstab

~~~
mount --fake --all --verbose
~~~

## rsyslog

~~~
rsyslogd -N1
rsyslogd -N1 -f /etc/rsyslogd/foo.conf
~~~

## pf

~~~
pfctl -nf /etc/pf.conf
~~~
