---
title: Howto DroneCI
categories: sysadmin git ci
...

* Documentation : <https://readme.drone.io/>


[DroneCI](https://drone.io/) est un logiciel libre d'[intégration continue](https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue).

## Installation

Depuis la version 1.0, DroneCI est exclusivement fournit sous forme de conteneur [Docker](HowtoDocker).

~~~
apt install docker-compose
~~~

On créé le fichier **docker-compose.yml** :

~~~
version: '2'

services:
  drone-server:
    image: drone/drone
    ports:
      - 8080:80
    volumes:
      - /var/lib/drone:/data
    restart: always
    env_file:
      - /etc/drone/drone-server.env

  drone-agent:
    image: drone/agent
    depends_on:
      - drone-server
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    restart: always
    env_file:
      - /etc/drone/drone-agent.env
~~~

### Serveur

Éditer le fichier **/etc/drone/drone-server.env** :

~~~
DRONE_SERVER_HOST=drone.example.com
DRONE_SERVER_PROTO=https
DRONE_SERVER_PORT=:80
DRONE_TLS_AUTOCERT=false
DRONE_RPC_SECRET=XXXXXXXXXXXXXXXX
# Gitea config
DRONE_GITEA_SERVER=https://gitea.example.com
DRONE_GIT_ALWAYS_AUTH=false
# Logs config
DRONE_LOGS_DEBUG=true
DRONE_LOGS_TEXT=true
DRONE_LOGS_PRETTY=true
DRONE_LOGS_COLOR=true
# Admins
DRONE_USER_CREATE=username:admin,admin:true
~~~

### Agent

Éditer le fichier **/etc/drone/drone-agent.env** :

~~~
DRONE_RPC_SERVER=https://drone.example.com
DRONE_RPC_SECRET=XXXXXXXXXXXXXXXX
DRONE_RUNNER_CAPACITY=3
# Logs config
DRONE_LOGS_DEBUG=true
DRONE_LOGS_TEXT=true
DRONE_LOGS_PRETTY=true
DRONE_LOGS_COLOR=true
~~~

### Lancement

On peut maintenant lancer **DroneCI** via docker-compose :

~~~
docker-compose up
~~~

## Utilisation

DroneCI se configure via un fichier `.drone.yml` a la racine de vos projets **GIT**, voici un exemple simple qui lance l'utilitaire `shellcheck` sur un script shell :

~~~
kind: pipeline
name: default

steps:
- name: run shellcheck on script.sh
  image: evolix/shellcheck
  commands:
  - LC_ALL=C.UTF-8 shellcheck script.sh
~~~
