---
categories: mail test
title: Howto SpamAssassin
...

* Documentation : <http://spamassassin.apache.org/doc.html>
* Configuration : <https://spamassassin.apache.org/full/4.0.x/doc/Mail_SpamAssassin_Conf.html>
* Statut de cette page : test / bookworm

[SpamAssassin](http://spamassassin.apache.org/) est un filtre pour évaluer si des emails sont des spams ou pas.

## Installation

Son intégration demande seulement l'installation des paquets suivants :

~~~
# apt install spamassassin spamc libmail-dkim-perl libmail-spf-perl libmail-dmarc-perl
# chmod 755 /var/lib/spamassassin

$ spamassassin -V
SpamAssassin version 4.0.0
  running on Perl version 5.36.0

# systemctl status spamd
● spamd.service - Perl-based spam filter using text analysis
     Loaded: loaded (/lib/systemd/system/spamd.service; enabled; preset: enabled)
     Active: active (running) since Sun 2024-10-06 14:16:30 CEST; 2 days ago
   Main PID: 113753 (spamd)
      Tasks: 3 (limit: 4673)
     Memory: 159.1M
        CPU: 46.165s
     CGroup: /system.slice/spamd.service
             ├─113753 /usr/bin/perl "-T -w" /usr/sbin/spamd --pidfile=/run/spamd.pid --create-prefs --max-children 5 --helper-home-dir
             ├─113813 "spamd child"
             └─113814 "spamd child"
~~~

On met à jour les règles :

~~~
# sa-update
# chown -R debian-spamd: /var/lib/spamassassin/4.000000/
# chmod 755 /var/lib/spamassassin/
# chmod -R g+rX,o+rX /var/lib/spamassassin/4.000000/
~~~

### Avec Amavis

Le service systemd n'est plus nécessaire car SpamAssassin est maintenant intégré dans [Amavis](/HowtoAmavis).

### Avec Postfix

~~~
# cat /etc/postfix/master.cf

smtp      pass  -       -       y       -       -       smtpd
    -o content_filter=spamassassin

spamassassin  unix  -   n     n     -   -   pipe
    user=debian-spamd argv=/usr/bin/spamc -f -u vmail -e /usr/sbin/sendmail -oi -f ${sender} ${recipient}
~~~

Attention, cette technique réinjecte l'email via `sendmail` ce qui le fera passer par les `non_smtpd_milters`
et potentiellement ajouter une signature DKIM si vous utilisez OpenDKIM en mode signature (Mode s)
si l'email utilise un domaine local (pas mal de spams font cela).

Pour éviter cela, plusieurs solutions : désactiver la signature DKIM via `non_smtpd_milters` (mais probablement utile),
ou alors rejeter les emails « forgés » en amont en rejetant les emails invalides SPF.

### Avec procmail

~~~
$ cat ~/.procmail

:0fw: spamassassin.lock
* < 256000
| spamc
~~~

## Configuration

~~~
required_score   5
report_safe     0
rewrite_header Subject [SPAM]
add_header all Report _REPORT_

# filtre bayesien
# mkdir -p /var/spool/spam
use_bayes       1
bayes_auto_learn 1
#bayes_path /var/spool/spam/bayes
#bayes_file_mode 0777

# AWL : AutoWhitelist
# mkdir -p /var/spool/spam
loadplugin Mail::SpamAssassin::Plugin::AWL
use_auto_whitelist 1
#auto_whitelist_path /var/spool/spam/auto_whitelist
#auto_whitelist_file_mode 0666

# LANG TESTS
Mail::SpamAssassin::Plugin::TextCat
ok_languages en fr es it
ok_locales en fr es it

score BODY_8BITS 1.500
score CHARSET_FARAWAY 3.200
score CHARSET_FARAWAY_HEADER 3.200
score HTML_CHARSET_FARAWAY 0.500
score MIME_CHARSET_FARAWAY 2.450
score UNWANTED_LANGUAGE_BODY 2.800

# DCC
# use_dcc 1 => un plugin maintenant...
score DCC_CHECK 2.9

# RAZOR : <http://razor.sourceforge.net>
use_razor2 1
razor_timeout 10
score RAZOR2_CHECK 2.9
score RAZOR2_CF_RANGE_51_100 1.3

# pyzor : <http://pyzor.sourceforge.net/>
use_pyzor 0

# RBL (Realtime Blackhole List)
skip_rbl_checks 0
score RCVD_IN_BL_SPAMCOP_NET 3

# misc
score HELO_DYNAMIC_IPADDR 0.3
score BIZ_TLD 0.1
score PRIORITY_NO_NAME 0.2

# disable HTML tests

score HTML_MESSAGE 0
score HTML_00_10 0
score HTML_10_20 0
score HTML_20_30 0
score HTML_30_40 0
score HTML_40_50 0
score HTML_50_60 0
score HTML_60_70 0
score HTML_70_80 0
score HTML_80_90 0
score HTML_90_100 0
#score HTML_COMMENT_8BITS 0
score UPPERCASE_25_50 0
score UPPERCASE_50_75 0
score UPPERCASE_75_100 0
score MIME_HTML_ONLY 0.1
# From <http://maxime.ritter.eu.org/Spam/user_prefs>
# Trop de faux negatifs avec BAYES_(0|1|2|3|4)*
score BAYES_00                       0 0 -0.01 -0.01
score BAYES_01                       0 0 -0.01 -0.01
score BAYES_10                       0 0 -0.01 -0.01
score BAYES_20                       0 0 -0.01 -0.01
score BAYES_30                       0 0 -0.01 -0.01
score BAYES_40                       0 0 -0.01 -0.01
score BAYES_44                       0 0 -0.01 -0.01
score BAYES_50                       0 0 0.1 0.1
score BAYES_56                       0 0 0.5 0.5
score BAYES_60                       0 0 1.0 1.0
score BAYES_70                       0 0 2.5 2.5
score BAYES_80                       0 0 3.5 3.5
score BAYES_90                       0 0 4.5 4.5
score BAYES_99                       0 0 8.0 8.0

score RCVD_IN_SORBS_DUL 0.3
score SUBJ_ILLEGAL_CHARS      0
score RCVD_IN_NJABL_DUL 0.3

score ADDRESS_IN_SUBJECT 0.1

score HELO_LH_HOME 1.0

#internal_networks 192.168.XXX/24
trusted_networks 31.170.8.33 31.170.8.15 62.212.111.216 88.179.18.233 85.118.59.50
#score ALL_TRUSTED 0.3
score HELO_DYNAMIC_IPADDR 0.3

score FORGED_MUA_OUTLOOK 0.5

# Eudora sucks
score EXTRA_MPART_TYPE 0.1
score MIME_BOUND_EQ_REL 0.1
score MIME_QP_LONG_LINE 0.1

# SMTP senders *have* dynamic IP addresses
# A.B.C.D.dnsbl.sorbs.net -> 127.0.0.10
score RCVD_IN_DYNABLOCK 0
score HELO_DYNAMIC_IPADDR 0.3
score RCVD_IN_SORBS 0.1
score RCVD_IN_PBL 0.1
score RCVD_IN_SORBS_DUL 0

# old bug...
score FH_DATE_PAST_20XX 0.0
~~~

### AWL : AutoWhiteList

SpamAssassin a un puissant outil d’AWL si l’on définit bien une liste d’emails / expéditeurs avant de l’initialiser.
Néanmoins, il faut le maintenir.

Pour lister sa whitelist (couple email / 2 1er octets de l’IP)

~~~
# sa-awl /var/spool/spam/auto_whitelist

            AVG  (TOTSCORE/COUNT)  --  EMAIL|ip=IPBASE

AVG est le score moyen
TOTSCORE est le score total des mails
COUNT est le nombre de mails
~~~

Pour supprimer les entrées “vues” moins de 2 fois :

~~~
# sa-awl --clean --min 2 /var/spool/spam/auto_whitelist
~~~

Pour supprimer des entrées particulières :

~~~
# spamassassin --remove-addr-from-whitelist=foo@bar
SpamAssassin auto-whitelist: removing address: foo@bar
~~~


## Administration

### Whitelist / blacklist

Par utilisateur (`~/.spamassassin/user_prefs`) ou global :

~~~
whitelist_from jdoe@example.org
whitelist_from *@example.com
blacklist_from bad@example.net
~~~


