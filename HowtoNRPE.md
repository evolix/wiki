---
categories: monitoring
title: Howto NRPE
---

* Documentation : <https://assets.nagios.com/downloads/nagioscore/docs/nrpe/NRPE.pdf>
* Statut de cette page : test / bookworm

NRPE (Nagios Remote PluginExecutor) permet d'exécuter de façon légère des commandes prédéfinies sur un serveur distant.
Historiquement il est utilisé par NAGIOS pour des « checks » sur un serveur distant.
Il peut également être utilisé par [Icinga](HowtoIcinga).
Il supporte SSL/TLS ce qui permet d'avoir un minimum de sécurité.

## Installation

~~~
# apt install nagios-nrpe-server monitoring-plugins monitoring-plugins-basic monitoring-plugins-common monitoring-plugins-standard nagios-plugins-contrib

$ /usr/sbin/nrpe --version
NRPE - Nagios Remote Plugin Executor
Version: 4.1.0

# systemctl status nagios-nrpe-server
● nagios-nrpe-server.service - Nagios Remote Plugin Executor
     Loaded: loaded (/lib/systemd/system/nagios-nrpe-server.service; enabled; preset: enabled)
     Active: active (running) since Fri 2023-07-28 15:55:54 CEST; 3 days ago
       Docs: http://www.nagios.org/documentation
   Main PID: 549 (nrpe)
      Tasks: 1 (limit: 2356)
     Memory: 1.7M
        CPU: 15ms
     CGroup: /system.slice/nagios-nrpe-server.service
             └─549 /usr/sbin/nrpe -c /etc/nagios/nrpe.cfg -f
~~~


## Configuration

Nous utilisons le fichier `/etc/nagios/nrpe.d/evolix.cfg` pour surcharger la configuration par défaut.

`/etc/nagios/nrpe.d/evolix.cfg` :

~~~
# Allowed IPs
allowed_hosts=192.0.2.42,192.0.2.43

# System checks
command[check_load]=/usr/local/lib/monitoringctl/alerts_wrapper --name load /usr/lib/nagios/plugins/check_load --percpu --warning=0.7,0.6,0.5 --critical=0.9,0.8,0.7
command[check_swap]=/usr/local/lib/monitoringctl/alerts_wrapper --name swap /usr/lib/nagios/plugins/check_swap -a -w 30% -c 20%
command[check_disk1]=/usr/local/lib/monitoringctl/alerts_wrapper --name disk1 /usr/lib/nagios/plugins/check_disk -e -w 10% -c 3% -W 10% -K 3% -C -w 5% -c 2% -W 5% -K 2% -p /home -x /lib/init/rw -x /dev -x /dev/shm -x /run -I '^/run/' -I '^/sys/'
command[check_zombie_procs]=/usr/local/lib/monitoringctl/alerts_wrapper --name zombie_procs sudo /usr/lib/nagios/plugins/check_procs -w 5 -c 10 -s Z
command[check_total_procs]=/usr/local/lib/monitoringctl/alerts_wrapper --name total_procs sudo /usr/lib/nagios/plugins/check_procs -w 400 -c 600
command[check_users]=/usr/local/lib/monitoringctl/alerts_wrapper --name users /usr/lib/nagios/plugins/check_users -w 5 -c 10

# Generic services checks
command[check_smtp]=/usr/local/lib/monitoringctl/alerts_wrapper --name smtp /usr/lib/nagios/plugins/check_smtp -H localhost
command[check_dns]=/usr/local/lib/monitoringctl/alerts_wrapper --name dns /usr/lib/nagios/plugins/check_dns -H evolix.net
command[check_ntp]=/usr/local/lib/monitoringctl/alerts_wrapper --name ntp /usr/lib/nagios/plugins/check_ntp -H ntp.evolix.net
command[check_ssh]=/usr/local/lib/monitoringctl/alerts_wrapper --name ssh /usr/lib/nagios/plugins/check_ssh localhost
command[check_mailq]=/usr/local/lib/monitoringctl/alerts_wrapper --name mailq /usr/lib/nagios/plugins/check_mailq -M postfix -w 10 -c 20

# Specific services checks
command[check_pgsql]=/usr/local/lib/monitoringctl/alerts_wrapper --name pgsql /usr/lib/nagios/plugins/check_pgsql -H localhost -l nrpe -p 'PASSWORD'
command[check_mysql]=/usr/local/lib/monitoringctl/alerts_wrapper --name mysql /usr/lib/nagios/plugins/check_mysql -H localhost -f ~nagios/.my.cnf
...
~~~

**Notes :**

* Si [`alerts_wrapper`](#alerts_wrapper-et-monitoringctl) n'est pas installé, il faut retirer la première partie de la commande `/usr/local/lib/monitoringctl/alerts_wrapper --name NAME`.
* Dans des versions précédentes de Debian, il n'était pas vraiment possible de surcharger la configuration : des options en double provoquait une activation aléatoire d'une option ou d'une autre !


Les directives importantes & utiles sont : 

* `allowed_hosts` : Permet d'énumérer les adresses IP qui sont autorisées à parler avec le daemon NRPE
* `command[CHECK_NAME]=/usr/local/lib/monitoringctl/alerts_wrapper --name NAME  /usr/lib/nagios/plugins/check_XXX` : Définis une commande NRPE  `CHECK_NAME`


## Principe

Le principe est qu'un démon écoute sur le port TCP/5666 et qu'on peut ainsi exécuter une commande distante définie dans la configuration.

On utilise la commande check_nrpe, celle-ci est distribuée dans le paquet `nagios-nrpe-plugin` dans Debian

~~~
# apt install nagios-nrpe-plugin
$ /usr/lib/nagios/plugins/check_nrpe -V
NRPE Plugin for Nagios
Version: 4.1.0

## Exemple: /usr/lib/nagios/plugins/check_nrpe -H HOTE_NRPE -c CHECK_NAME
$ /usr/lib/nagios/plugins/check_nrpe -H 192.0.2.42 -c check_load
OK - load average: 0.01, 0.01, 0.01|load1=0.012;0.700;0.900;0; load5=0.010;0.600;0.800;0; load15=0.010;0.500;0.700;0; 
~~~

> *Note* : il faut savoir que le `check_load` lance simplement la commande définie dans la configuration.
Cela peut-être un check, mais aussi n'importe quelle commande ou script de son choix !


### `alerts_wrapper` et `monitoringctl`

TODO : ajouter des liens vers Gitea quand les scripts seront passé dans la branche `stable` de `ansible-roles`.

Comme indiqué dans la section [Configuration](#configuration), nous avons développé un script `alerts_wrapper` qui wrappe les checks, afin de pouvoir les désactiver à partir du serveur.

`alerts_wrapper` lance le check, mais vérifie s'il y a un fichier `/var/lib/monitoringctl/<CHECK_NAME>_alerts_disabled`, contient à un timestamp de réactivation.

Si le timestamp indique que l'alerte est encore désactivée, il renvoie à NRPE un code OK, en précisant quand même la sortie et l'état du check pour l'historique. Sinon, il est transparent.

Nous avons aussi développé `monitoringctl`, qui permet de désactiver localement, à partir du serveur, n'importe quel check wrappé avec `alerts_wrapper` :

~~~
# monitorinqctl disable load --during 1h30 --message 'Running heavy computation'
~~~

C'est utile si on veut permettre à un utilisateur d'un serveur de désactiver temporairement des alertes au cas par cas, sans lui donner accès au monitoring pour autant.

C'est aussi utilisable dans des scripts, ou des crons si on veut désactiver automatiquement une alerte à des heures particulières, par exemple lors de backups, de dumps, de pic d'activité régulier…


## checks

Même si ce n'est pas directement lié à NRPE, listons quelques checks intéressants.

### check_load

Vu l'utilisation massive de plusieurs CPUs, il est très intéressant d'utiliser l'option `--percpu` qui permet au check de s'adapter tout seul au nombre de CPUs :

~~~
$ /usr/lib/nagios/plugins/check_load --percpu --warning=0.7,0.6,0.5 --critical=0.9,0.8,0.7
~~~

### check_disk

Plusieurs options sont intéressantes :

* `-e` : check toutes les partitions mais en cas d'erreur, n'affiche que les partitions qui sont en erreur (WARN, CRIT ou UNKN)
* `-x /run` : ignore une partition
* `-I '^/run/'` : ignore les partitions selon un pattern
* `-C -w 5% -c 2% -W 5% -K 2% -p /home` : donne des seuils d'alertes différents pour une partition

### check_http

Plusieurs options sont intéressantes :

* `-S -H example.com -C 15,5` : vérifie que la validité d'un certificat SSL/TLS est inférieure à 15 jours (WARN) ou 5 jours (CRIT)


## Plomberie

### Erreur SSL avec le service NRPE sur une ancienne Debian depuis un client NRPE récent (Debian 10+)

Les versions récentes (Debian 10 et suivantes) ont une version plus récente d'OpenSSL, la bibliothèque qui gère le chiffrement TLS de la connexion NRPE. 
Celle-ci refusera d'établir la connexion chiffrée avec une ancienne version de NRPE, notamment celle présente dans Debian 8 (et plus ancien).

Pour le détail technique, c'est OpenSSL côté client qui refuse de faire un échange Diffie Hellman de 512 bits. 
Sauf que cette taille de clé est malheureusement codée en dur dans NRPE. 

Plusieurs alternatives sont possibles pour continuer de surveiller une ancienne machine : 
* A/ (Déconseillée) Désactiver SSL/TLS pour la communication entre le client et le serveur NRPE
* B/ Encapsuler l'utilisation de NRPE dans SSH. Le service de monitoring fait une connexion SSH vers l'hôte surveillé pour utiliser la commande `check_nrpe` localement
* C/ Patcher le serveur NRPE pour qu'il utilise à place une clé de 2048bits ([patch](https://gitea.evolix.org/evolix/nagios-nrpe-jessie-gbp/commit/b988357a7f58c2f28055f75bd3eec129ee1957bc)) - Cette version patchée est disponible sur notre dépôt [pub.evolix.org](/HowtoDebian/SourcesList)

### Désactiver SSL/TLS

Nous déconseillons cette configuration, mais c'est possible de désactiver le chiffrement.

Dans le fichier de configuration `/etc/default/nagios-nrpe-server`, il y a la variable `NRPE_OPTS`. Il faut la décommenter et rajouter `-n` pour désactiver SSL et redémarrer le servce.

> **Attention** :  ça implique que **tous** les clients devront désactiver SSL pour communiquer avec le service NRPE.

Côté client, on ajoutera aussi l'option `-n` pour désactiver l'utilisation de SSL/TLS lors de la connexion : 

Exemple : `/usr/lib/nagios/plugins/check_nrpe -n -H 192.0.2.42 -c check_load`


### Configurer un certificat SSL/TLS serveur

Dans la configuration de NRPE, il est possible de configurer un certificat serveur. Ça permet notamment au client de valider l'identité du serveur.


Côté serveur, ça se configure avec les directives suivantes pour donner les fichiers du certificat et la clé privée : 

```
ssl_cert_file=/etc/nagios/cert.pem
ssl_privatekey_file=/etc/nagios/privkey.key
```

Attention à ce que l'utilisateur `nagios` ait bien le droit de lecture sur ces fichiers.


> **Remarque importante** : Il semble que `check_nrpe` ne valide pas la correspondance du `CN` (CommonName) du certificat avec l'adresse de destination utilisée en argument (le `-H`) mais seulement la validité du certificat face à la CA utilisée avec l'argument.

Exemple : 

```
## Si la CA de notre PKI est dans /etc/ssl/ca-pki.pem
$ /usr/lib/nagios/plugins/check_nrpe -H 192.0.2.10 -A /etc/ssl/ca-pki.pem -c check_load
```

> *Note* : On peut utiliser `/etc/ssl/certs/ca-certificates.crt` si le certificat est issu d'une autorité de certification classique

## FAQ