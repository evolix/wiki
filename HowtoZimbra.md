---
categories: mail
title: Howto Zimbra
...

* Documentation : <https://www.zimbra.com/documentation/>

[Zimbra](https://www.zimbra.com) est un logiciel de messagerie et de Groupware (gestion des agendas, carnets d'adresses, etc.).
Il possède une interface web (versions AJAX, HTML ou mobile), une version riche (Zimbra Desktop), une synchronisation Activesync pour les mobiles (iPhone, Android, Windows Phone, Nokia) et un plugin pour Microsoft Outlook (>=2003).
Il existe en version *Network Edition* (version propriétaire nécessitant des licences payantes) et *Open Source Edition* (version gratuite mais avec des fonctionnalités en moins comme la synchronisation Activesync et le plugin pour Microsoft Outlook).


## Comprendre les licences Zimbra

Zimbra a été racheté successivement par Yahoo, VMware, Telligent puis Synacor.
C'est l'une des raisons pour laquelle le système de licences a souvent changé et qu'il est difficile à comprendre.

La licence conseillée est la PROFESSIONNAL car elle inclut les fonctionnalités intéressantes comme la synchronisation Activesync et le plugin pour Microsoft Outlook, voir <https://www.zimbra.com/email-server-software/product-edition-comparison/>
Cela inclut notamment un support STANDARD : par le portail <https://support.zimbra.com> et par téléphone en heures ouvrées, voir <https://www.zimbra.com/support/support-offerings/>

Pour acheter des licences en France, vous devez passer par un revendeur comme STARXPERT ou SCC, qui passe lui-même par COMMEO (revendeur officiel).

En terme de tarif, comptez environ 20 EUR par compte par an pour la licence PROFESSIONNAL (la quantité minimale étant de 25), voir <http://www.commeo.fr/com/tarifs-zimbra/>

Attention, méfiez-vous de la licence perpétuelle : elle n'inclut pas les mises à jour majeures de Zimbra !

## Installation

Prérequis :

* Avoir au moins 2 Go de RAM.
* Avoir un _/etc/hosts_ propre, contenant par exemple (avec zimbra.example.com résolvant vraiment vers 192.0.32.10) :

~~~
127.0.0.1 localhost.localdomain localhost
192.0.32.10 zimbra.example.com zimbra
~~~

Installation de Zimbra 8 sous Ubuntu 14.04 LTS 64bits :

~~~
# aptitude remove lsb-invalid-mta
# aptitude install libgmp10 libperl5.18 libaio1 unzip resolvconf
$ wget http://files2.zimbra.com/downloads/8.6.0_GA/zcs-NETWORK-8.6.0_GA_1153.UBUNTU14_64.20141215151218.tgz
$ tar xvf zcs-NETWORK-8.6.0_GA_1153.UBUNTU14_64.20141215151218.tgz
~~~

~~~
# cd zcs-NETWORK-8.6.0_GA_1153.UBUNTU14_64.20141215151218
# ./install.sh
Operations logged to /tmp/install.log.4467
Checking for existing installation...
    zimbra-ldap...NOT FOUND
    zimbra-logger...NOT FOUND
    zimbra-mta...NOT FOUND
    zimbra-snmp...NOT FOUND
    zimbra-store...NOT FOUND
    zimbra-apache...NOT FOUND
    zimbra-spell...NOT FOUND
    zimbra-convertd...NOT FOUND
    zimbra-memcached...NOT FOUND
    zimbra-proxy...NOT FOUND
    zimbra-archiving...NOT FOUND
    zimbra-cluster...NOT FOUND
    zimbra-core...NOT FOUND

VMWARE END USER LICENSE AGREEMENT

IMPORTANT-READ CAREFULLY: BY DOWNLOADING, INSTALLING, OR USING THE 
[...]
Do you agree with the terms of the software license agreement? [N] y

Checking for prerequisites...
     FOUND: NPTL
     FOUND: netcat-openbsd-1.89-4ubuntu1
     FOUND: sudo-1.8.3p1-1ubuntu3.4
     FOUND: libidn11-1.23-2
     FOUND: libpcre3-8.12-4
     FOUND: libgmp3c2-2:4.3.2+dfsg-2ubuntu1
     FOUND: libexpat1-2.0.1-7.2ubuntu1.1
     FOUND: libstdc++6-4.6.3-1ubuntu5
     FOUND: libperl5.14-5.14.2-6ubuntu2.3

Checking for suggested prerequisites...
     FOUND: pax
     FOUND: perl-5.14.2
     FOUND: sysstat
     FOUND: sqlite3
Prerequisite check complete.

Checking for installable packages

Found zimbra-core
Found zimbra-ldap
Found zimbra-logger
Found zimbra-mta
Found zimbra-snmp
Found zimbra-store
Found zimbra-apache
Found zimbra-spell
Found zimbra-convertd
Found zimbra-memcached
Found zimbra-proxy
Found zimbra-archiving


Select the packages to install

Install zimbra-ldap [Y]
Install zimbra-logger [Y]
Install zimbra-mta [Y]
Install zimbra-snmp [Y]
Install zimbra-store [Y]
Install zimbra-apache [Y]
Install zimbra-spell [Y]
Install zimbra-convertd [Y]
Install zimbra-memcached [N]
Install zimbra-proxy [N]
Install zimbra-archiving [N] 

Checking required space for zimbra-core
checking space for zimbra-store

Installing:
    zimbra-core
    zimbra-ldap
    zimbra-logger
    zimbra-mta
    zimbra-snmp
    zimbra-store
    zimbra-apache
    zimbra-spell
    zimbra-convertd

The system will be modified.  Continue? [N] y

Removing /opt/zimbra
Removing zimbra crontab entry...done.
Cleaning up zimbra init scripts...done.
Cleaning up /etc/ld.so.conf...done.
Cleaning up /etc/security/limits.conf...done.

Finished removing Zimbra Collaboration Suite.

Installing packages

    zimbra-core......zimbra-core_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-ldap......zimbra-ldap_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-logger......zimbra-logger_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-mta......zimbra-mta_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-snmp......zimbra-snmp_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-store......zimbra-store_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-apache......zimbra-apache_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-spell......zimbra-spell_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done
    zimbra-convertd......zimbra-convertd_7.0.0_GA_3077.UBUNTU10_64_amd64.deb...done

Operations logged to /tmp/zmsetup.03012011-104432.log
Installing LDAP configuration database...done.

DNS ERROR resolving MX for zimbra.example.com
It is suggested that the domain name have an MX record configured in DNS
Change domain name? [Yes] No
done.
Checking for port conflicts
Main menu

   1) Common Configuration:                                                  
   2) zimbra-ldap:                             Enabled                       
   3) zimbra-store:                            Enabled                       
        +Create Admin User:                    yes                           
        +Admin user to create:                 admin@zimbra.example.com
******* +Admin Password                        UNSET                         
        +Anti-virus quarantine user:           virus-quarantine.tqiruegf6wsr88vv@zimbra.example.com
        +Enable automated spam training:       yes                           
        +Spam training user:                   spam.qrs78szfr87wr@zimbra.example.com
        +Non-spam(Ham) training user:          ham.xagwfg7g37jkk@zimbra.example.com
        +SMTP host:                            zimbra.example.com                  
        +Web server HTTP port:                 80                            
        +Web server HTTPS port:                443                           
        +Web server mode:                      http                          
        +IMAP server port:                     143                           
        +IMAP server SSL port:                 993                           
        +POP server port:                      110                           
        +POP server SSL port:                  995                           
        +Use spell check server:               yes                           
        +Spell server URL:                     <http://zimbra.example.com:7780/aspell.php>
        +Configure for use with mail proxy:    FALSE                         
        +Configure for use with web proxy:     FALSE                         
        +Enable version update checks:         TRUE                          
        +Enable version update notifications:  TRUE                          
        +Version update notification email:    admin@zimbra.example.com
        +Version update source email:          admin@zimbra.example.com      
******* +License filename:                     UNSET                         

   4) zimbra-mta:                              Enabled                       
   5) zimbra-snmp:                             Enabled                       
   6) zimbra-logger:                           Enabled                       
   7) zimbra-spell:                            Enabled                       
   8) zimbra-convertd:                         Enabled                       
   9) Default Class of Service Configuration:                                
  10) Enable default backup schedule:          yes                           
   r) Start servers after configuration        yes                           
   s) Save config to file                                                    
   x) Expand menu                                                            
   q) Quit                                    

Address unconfigured (**) items  (? - help)
[...]
~~~

Il vous reste au minimum à configurer le mot de passe admin
et à contacter votre revendeur pour acheter des licences Zimbra.

~~~
*** CONFIGURATION COMPLETE - press 'a' to apply
Select from menu, or press 'a' to apply config (? - help) a
Save configuration data to a file? [Yes] 
Save config in file: [/opt/zimbra/config.11297] 
Saving config in /opt/zimbra/config.11297...done.
The system will be modified - continue? [No] yes
Operations logged to /tmp/zmsetup.03012011-104432.log
Setting local config values...done.
Setting up CA...done.
Deploying CA to /opt/zimbra/conf/ca ...done.
Creating SSL certificate...done.
Installing mailboxd SSL certificates...done.
Initializing ldap...done.
Setting replication password...done.
Setting Postfix password...done.
Setting amavis password...done.
Setting nginx password...done.
Creating server entry for zimbra.example.com...done.
Saving CA in ldap ...done.
Saving SSL Certificate in ldap ...done.
Setting spell check URL...done.
Setting service ports on zimbra.example.com...done.
Adding zimbra.example.com to zimbraMailHostPool in default COS...done.
Installing webclient skins...
        lake...done.
        lemongrass...done.
        carbon...done.
        tree...done.
        steel...done.
        waves...done.
        bare...done.
        pebble...done.
        hotrod...done.
        oasis...done.
        twilight...done.
        lavender...done.
        bones...done.
        sky...done.
        beach...done.
        smoke...done.
        sand...done.
Finished installing webclient skins.
Setting zimbraFeatureTasksEnabled=TRUE...done.
Setting zimbraFeatureBriefcasesEnabled=TRUE...done.
Setting convertd URL...done.
Setting MTA auth host...done.
Setting TimeZone Preference...done.
Initializing mta config...done.
Setting services on zimbra.example.com...done.
Creating domain zimbra.example.com...done.
Setting default domain name...done.
Setting up default domain admin UI components..done.
Granting group zimbraDomainAdmins@zimbra.example.com domain right +domainAdminConsoleRights on zimbra.example.com...done.
Granting group zimbraDomainAdmins@zimbra.example.com global right +domainAdminZimletRights...done.
Setting up global distribution list admin UI components..done.
Granting group zimbraDLAdmins@zimbra.example.com global right +adminConsoleDLRights...done.
Granting group zimbraDLAdmins@zimbra.example.com global right +listAccount...done.
Creating domain zimbra.example.com...already exists.
Creating admin account admin@zimbra.example.com...done.
Creating root alias...done.
Creating postmaster alias...done.
Creating user spam.r4nd0m@zimbra.example.com...
done.
Creating user ham.r4nd0m@zimbra.example.com...
done.
Creating user virus-quarantine.r4nd0m@zimbra.example.com...
done.
Setting spam training and Anti-virus quarantine accounts...done.
Initializing store sql database...done.
Setting zimbraSmtpHostname for zimbra.example.com...done.
Configuring SNMP...done.
Checking for default IM conference room...not present.
Initializing default IM conference room...done.
Setting up syslog.conf...done.
Setting default backup schedule...Done
Looking for valid license to install...license installed.
Starting servers...done.
Installing common zimlets...
        com_zimbra_srchhighlighter...done.
        com_zimbra_cert_manager...done.
        com_zimbra_attachmail...done.
        com_zimbra_social...done.
        com_zimbra_attachcontacts...done.
        com_zimbra_phone...done.
        com_zimbra_bulkprovision...done.
        com_zimbra_webex...done.
        com_zimbra_linkedin...done.
        com_zimbra_url...done.
        com_zimbra_email...done.
        com_zimbra_date...done.
        com_zimbra_dnd...done.
        com_zimbra_adminversioncheck...done.
Finished installing common zimlets.
Installing network zimlets...
        com_zimbra_backuprestore...done.
        com_zimbra_license...done.
        com_zimbra_mobilesync...done.
        com_zimbra_hsm...done.
        com_zimbra_delegatedadmin...done.
        com_zimbra_convertd...done.
Finished installing network zimlets.
Restarting mailboxd...done.
Setting up zimbra crontab...done.

Moving /tmp/zmsetup.03012011-104432.log to /opt/zimbra/log
~~~

## Configuration

Activer HTTPS en plus de HTTP :

~~~
# su zimbra
zimbra$ /opt/zimbra/bin/zmtlsctl redirect
Setting ldap config zimbraMailMode redirect for zimbra.example.com...done.
Rewriting config files for cyrus-sasl, webxml and mailboxd...done.
# /etc/init.d/zimbra restart
Host zimbra.example.com
        Stopping stats...Done.
        Stopping mta...Done.
        Stopping spell...Done.
        Stopping snmp...Done.
        Stopping cbpolicyd...Done.
        Stopping archiving...Done.
        Stopping antivirus...Done.
        Stopping antispam...Done.
        Stopping imapproxy...Done.
        Stopping memcached...Done.
        Stopping mailbox...Done.
        Stopping convertd...Done.
        Stopping logger...Done.
        Stopping zmconfigd...Done.
        Stopping ldap...Done.
Host zimbra.example.com
        Starting ldap...Done.
        Starting zmconfigd...Done.
        Starting logger...Done.
        Starting convertd...Done.
        Starting mailbox...Done.
        Starting antispam...Done.
        Starting antivirus...Done.
        Starting snmp...Done.
        Starting spell...Done.
        Starting mta...Done.
        Starting stats...Done.
~~~

Pour autoriser un certificat non reconnu par Sun/Java (par exemple pour accéder à un annuaire extérieur en LDAP pour la GAL),
on suivra les instructions décrites sur <http://blogs.sun.com/gc/entry/unable_to_find_valid_certification> :

~~~
$ wget http://blogs.sun.com/andreas/resource/InstallCert.java
$ /opt/zimbra/jdk1.6.0_22/bin/javac InstallCert.java
$ /opt/zimbra/jdk1.6.0_22/bin/java InstallCert secure.example.com
# cp jssecacerts /opt/zimbra/jdk1.6.0_22/jre/lib/security/
# /etc/init.d/zimbra restart
~~~

## Administration

### Via l'interface web

Accessible sur le port 7071 : <https://127.0.0.1:7071/zimbraAdmin/> + ssh -L 7071:127.0.0.1:7071 zimbra.example.com


### En ligne de commande avec ZMPROV

<http://wiki.zimbra.com/wiki/Zmprov>

~~~
# su zimbra
$ export PATH=$PATH:/opt/zimbra/bin

Get all conf / global settings
$ zmprov gacf
Get domain
$ zmprov gd <domaine>
Get account
$ zmprov ga <compte>
Get default COS
$ zmprov gc default
Get COS <cos>
$ zmprov gc <cos>
Modify global conf
$ zmprov mcf
Modify domain
$ zmprov md <domaine>
Modify account
$ zmprov ma <compte>

Search GAL
$ zmprov sg <domaine> <recherche>
~~~

Voici ainsi l'application de certaines modifications via la COS <cos> :

~~~
Commencer la semaine ouvrée le lundi
$ zmprov mc <cos> zimbraPrefCalendarFirstDayOfWeek 1
Définir les horaires ouvrés de 9h à 18h
$ zmprov mc <cos> zimbraPrefCalendarWorkingHours 1:N:0900:1800,2:Y:0900:1800,3:Y:0900:1800,4:Y:0900:1800,5:Y:0900:1800,6:Y:0900:1800,7:N:0900:1800
Afficher les numéros de semaine dans le mini-calendrier
$ zmprov mc <cos> zimbraPrefShowCalendarWeek TRUE
Ne PAS afficher les rappels pour les réunions dont la date est dépassée
$ zmprov mc <cos> zimbraPrefCalendarShowPastDueReminders FALSE
Autoriser la visualisation disponible/occupés uniquement pour les utilisateurs internes
zmprov mc <cos> zimbraACE "00000000-0000-0000-0000-000000000000 all viewFreeBusy"
Ne pas autoriser l'ajout automatique d'une invitation
zmprov mc <cos>zimbraPrefCalendarAutoAddInvites FALSE
~~~

D'autres modifications :

~~~
Renvoyer une notification lors d'un nouveau message à l'adresse <mail>
$ zmprov ma <login> zimbraPrefNewMailNotificationAddress <mail>
Envoyer les alertes pour les rendez-vous de calendrier à <mail>
$ zmprov ma <login> zimbraPrefCalendarReminderEmail <mail>
~~~

## GAL

<http://wiki.zimbra.com/wiki/GAL_Attribute_Mapping>

La GAL (Global Access List) est une liste de contacts globale. Elle est accessible a priori par tous les comptes d'un domaine,
elle comprend généralement les comptes eux-même et elle est utilisée pour les complétions. Sur les périphériques de type Mobile
(iPhone et Android), elle est accessible d'une façon différente des contacts classiques via "Annuaire d'entreprise".

La GAL pourra être interne (associé à un compte dédié...) ou externe (synchronisation avec un annuaire LDAP).

### GAL interne

Dans le cas d'une GAL interne, il faut absolument utiliser un compte dédié (oui, vous avez bien lu, cela comptera pour une licence), souvent _galsync@<domaine>_.
En configurant la GAL interne, cela crée d'ailleurs le compte automatiquement et l'associé à une _Datasource_... et il n'est pas possible de le changer après sauf
en passant en ligne de commande. Imaginons que vous changez le compte et la _Datasource_ associé, vous pouvez :

1. Supprimer le compte _galsync@example.com_ (attention, Zimbra va râler quand vous irez dans la configuration de la GAL.

2. Forcer le changement des paramètres de la GAL via :

~~~
$ zmgsautil createAccount -a galsync@example.com -n exampleGAL --domain example.com -t zimbra -p 1d
$ zmgsautil fullSync -a galsync@example.com -n exampleGAL
~~~

3. Dédoublonner le paramètre _zimbraGalAccountId_ :

~~~
$ zmprov gd example.com zimbraGalAccountId
$ zmprov md example.com zimbraGalAccountId 822bac1a-5e92-48a0-8bb0-xxxxxxx
~~~


### GAL externe

Dans le cas d'un GAL externe, il "suffit" de configurer la bonne connexion au serveur LDAP.
La plupart du temps, une GAL externe sera en lecture seule.

Note : à voir l'intérêt d'utiliser un compte dédié dans ce cas ??

## Configuration

### Postfix

Non seulement Postfix est inclus dans le "bundle Zimbra", mais sa configuration est directement géré par Zimbra. Ainsi vous nous pouvez *pas* modifier le _main.cf_
car il est regénéré en permanence. Imaginons que vous souhaitez rajouter une source _virtual_alias_maps_, la bonne façon pour faire une modification est d'utilisation
la commande suivante :

Sous Zimbra 7 :

~~~
$ zmlocalconfig -e postfix_virtual_alias_maps='hash:/etc/zimbra-aliases,proxy:ldap:${zimbra_home}/conf/ldap-vam.cf'
$ postfix reload
~~~

Sous Zimbra 8 :

~~~
# /opt/zimbra/postfix-2.11.1.2z/sbin/postmap lmdb:/etc/zimbra-alias
$ zmprov -r -m -l ms z.example.com zimbraMtaVirtualAliasMaps 'lmdb:/etc/zimbra-alias,proxy:ldap:/opt/zimbra/conf/ldap-vam.cf'
$ zmmtactl restart
~~~

Voyez également le fichier _/opt/zimbra/conf/zmmta.cf_ qui sert dans le processus de génération automatique de la configuration de Postfix.

ATTENTION, sous Zimbra 8.6, l'algorithme hash n'est plus supporté : _unsupported dictionary type: hash_
Vous devez utiliser 'lmdb' et utiliser /opt/zimbra/postfix-2.11.1.2z/sbin/postmap lmdb:<fichier> pour générer les .lmdb


## Administration

Tester le bon fonctionnement des services :

~~~
$ zmcontrol status
Host zimbra.example.com
        antispam                Running
        antivirus               Running
        convertd                Running
        ldap                    Running
        logger                  Running
        mailbox                 Running
        mta                     Running
        snmp                    Running
        spell                   Running
        stats                   Running
        zmconfigd               Running
~~~

### Stockage des mails

Zimbra ne stocke malheureusement pas les mails de façon standard : pas de Maildir, de mbox, etc. :(

Il indexe les messages dans MySQL et les stocke dans des répertoires sous _/opt/zimbra/store_ :

~~~
mysql> select * from zimbra.volume;
+----+------+----------+-------------------+-----------+-----------------+--------------+--------------------+----------------+-----------------------+
| id | type | name     | path              | file_bits | file_group_bits | mailbox_bits | mailbox_group_bits | compress_blobs | compression_threshold |
+----+------+----------+-------------------+-----------+-----------------+--------------+--------------------+----------------+-----------------------+
|  1 |    1 | message1 | /opt/zimbra/store |        12 |               8 |           12 |                  8 |              0 |                  4096 |
|  2 |   10 | index1   | /opt/zimbra/index |        12 |               8 |           12 |                  8 |              0 |                  4096 |
+----+------+----------+-------------------+-----------+-----------------+--------------+--------------------+----------------+-----------------------+
2 rows in set (0.00 sec)

mysql> select * from mboxgroup12.mail_item limit 1 offset 250\G
*************************** 1. row ***************************
  mailbox_id: 12
          id: 756
        type: 5
   parent_id: 753
   folder_id: 3
    index_id: 756
     imap_id: 756
        date: 1301526232
        size: 1760
   volume_id: 1
 blob_digest: Cv6LEbNCmYaEQXLTY4Pc05f37BB=
      unread: 0
       flags: 0
        tags: 0
      sender: John Doe
     subject: test
        name: NULL
    metadata: d1:f0:1:s37:jdoe@example.com (John Doe)1:ve31ff
mod_metadata: 1617
 change_date: 1301527020
 mod_content: 1610
1 row in set (0.00 sec)
~~~

Voir <http://wiki.zimbra.com/wiki/Account_mailbox_database_structure>

Gestion des volumes avec _zmvolume_, qui permet de lister les volumes
et leurs propriétés, les changer :

~~~
$ zmvolume -l
   Volume id: 1
        name: message1
        type: primaryMessage
        path: /opt/zimbra/store
  compressed: false
     current: true

   Volume id: 2
        name: index1
        type: index
        path: /opt/zimbra/index
  compressed: false
     current: true

$ zmvolume -e -id 1 -c true
$ zmvolume -e -id 2 -c true

$ zmvolume -l
   Volume id: 1
        name: message1
        type: primaryMessage
        path: /opt/zimbra/store
  compressed: true               threshold: 4096 bytes
     current: true

   Volume id: 2
        name: index1
        type: index
        path: /opt/zimbra/index
  compressed: true               threshold: 4096 bytes
     current: true
~~~


### Redolog


Zimbra gère des _redolog_ c'est-à-dire des journaux de transation où il conserve toutes les actions effectuées.
C'est très intéressant pour la sécurité, mais un peu moins côté consommation disque. Pour les désactiver :

~~~
$ zmprov gcf zimbraRedoLogEnabled      
zimbraRedoLogEnabled: TRUE
$ zmprov mcf zimbraRedoLogEnabled FALSE
$ zmcontrol shutdown
Host zimbra.example.com
        Stopping stats...Done.
        Stopping mta...Done.
        Stopping spell...Done.
        Stopping snmp...Done.
        Stopping cbpolicyd...Done.
        Stopping archiving...Done.
        Stopping antivirus...Done.
        Stopping antispam...Done.
        Stopping imapproxy...Done.
        Stopping memcached...Done.
        Stopping mailbox...Done.
        Stopping convertd...Done.
        Stopping logger...Done.
        Stopping zmconfigd...Done.
        Stopping ldap...Done.
$ zmcontrol startup 
Host zimbra.example.com
        Starting ldap...Done.
        Starting zmconfigd...Done.
        Starting logger...Done.
        Starting convertd...Done.
        Starting mailbox...Done.
        Starting antispam...Done.
        Starting antivirus...Done.
        Starting snmp...Done.
        Starting spell...Done.
        Starting mta...Done.
        Starting stats...Done.
$ rm -f /opt/zimbra/redolog/archive/*log /opt/zimbra/redolog/redo.log
~~~

### Sauvegardes

Note : les sauvegardes incrémentales nécessitent l'activation du Redolog (voir ci-dessus).

Les sauvegardes sont réalisées via des crons qui effectuent des sauvegardes complètes, incrémentales et 
des nettoyages. On peut gérer ces tâches via la commande _zmschedulebackup_ :

~~~
$ zmschedulebackup                    
Current Schedule:

        f 0 1 * * 6 -a all --mail-report
        i 0 1 * * 0-5 --mail-report
~~~

On peut remettre le planning par défaut ainsi :

~~~
$ zmschedulebackup -D
Default schedule set

Current Schedule:

        f 0 1 * * 6 -a all --mail-report
        i 0 1 * * 0-5 --mail-report
        d 1m 0 0 * * * --mail-report
~~~

On peut ajouter des sauvegardes ainsi :

~~~
$ zmschedulebackup -A f "0 1 * * 6" -a all --mail-report
$ zmschedulebackup -A i "0 1 * * 0-5" --mail-report
~~~

Pour modifier le planning des sauvegardes, par exemple pour effacer les sauvegardes au bout de 20 jours :

~~~
$ zmschedulebackup -R f "0 1 * * 6" -a all i "0 1 * * 0-5" --mail-report --mail-report d 20d "0 0 * * *" --mail-report
Schedule replaced

Current Schedule:

        f 0 1 * * 6 -a all --mail-report
        i 0 1 * * 0-5 --mail-report
        d 20d 0 0 * * * --mail-report
~~~

Pour supprimer immédiatement les sauvegardes datant de plus de 15 jours ou 1 jour :

~~~
$ zmbackup -del 15d
$ zmbackup -del 1d
~~~

Les sauvegardes sont stockées dans `/opt/zimbra/backup` :

On retrouvera notamment la liste des comptes dans `/opt/zimbra/backup/accounts.xml`.
Et les sauvegardes complètes dans `/opt/zimbra/backup/tmp/full-20XXXXXX.NNN`.

> ATTENTION : les sauvegardes ne sont utilisables qu'entre des versions identiques de Zimbra ! Cela n'est donc pas utilisable pour des migrations de données de versions différentes.

### SSH

Attention, Zimbra utilise l'accès SSH au compte zimbra pour divers paramètres.
Si l'on met des restrictions de type _AllowUsers_ on veillera à garder l'autorisation pour le compte zimbra.

### Gestion des licences

Le plus simple est de le gérer en ligne de commande.

Pour afficher la licence actuelle :

~~~
$ zmlicense -p
~~~

En théorie, la licence actuelle se trouve dans _/opt/zimbra/conf/ZCSLicense.xml_.

Pour installer une nouvelle licence (en cas de renouvellement par exemple) :

~~~
$ zmlicense -i ZCSLicense.xml
~~~

Puis dans l'interface web d'admin, aller dans _Global Settings_ puis _License_ et cliquer sur _Activate license_.

On peut aussi l'activer via _zmlicense_ (mais cela dysfonctionne souvent avec les anciennes versions de Zimbra).

~~~
$ zmlicense -a
~~~

Autre possibilité, générer un fichier d'activation activation.xml via <https://support.zimbra.com/> puis l'activer via l'interface web ou en ligne de commande :

~~~
$ zmlicense -A activation.xml
~~~

Autres possibilités :

~~~
# Voir le fingerprint de la license
$ zmlicence -f
~~~

### Gestion des certificats

Si vous obtenez des erreurs du type :

~~~
Unable to determine enabled services from ldap.
Enabled services read from cache. Service list may be inaccurate.
ERROR: service.FAILURE (system failure: ZimbraLdapContext) (cause: javax.net.ssl.SSLHandshakeException sun.security.validator.ValidatorException: PKIX path validation failed: java.security.cert.CertPathValidatorException: signature check failed)
~~~

C'est que le certificat interne de Zimbra a expiré !

Pour le renouveler (autosigné), lire <http://wiki.zimbra.com/wiki/Administration_Console_and_CLI_Certificate_Tools> et faire :

~~~
# zmcertmgr createca -new
# zmcertmgr createcrt -new -days 1000
# zmcertmgr deploycrt self
# zmcertmgr deployca
# zmcertmgr viewdeployedcrt
~~~

Reste un point important à gérer : les _Java KeyStore_ ... et attention car il peut en avoir plusieurs à gérer :
par exemple: cacerts et jssecacerts dans /opt/zimbra/java/jre/lib/security/ ET /opt/zimbra/jdk1.6.0_22/jre/lib/security/

On peut éventuellement les gérer bas niveau avec avec keytool :

~~~
KEYSTORE_PATH=/opt/zimbra/jdk1.6.0_22/jre/lib/security/jssecacerts (version 7)
KEYSTORE_PATH=/opt/zimbra/jdk-1.7.0_60/jre/lib/security/cacerts (version 8.6)
# /opt/zimbra/java/bin/keytool -list -keystore $KEYSTORE_PATH -storepass changeit
# /opt/zimbra/java/bin/keytool -delete -keystore $KEYSTORE_PATH -storepass changeit
# /opt/zimbra/java/bin/keytool -import -alias my_ca -keystore $KEYSTORE_PATH -storepass changeit -file /tmp/ca.pem
~~~

MAIS il est conseillé d'utiliser _zmcertmgr_. Par exemple, pour autoriser les connexions LDAPS vers un certificat SSL de chez StartSSL, on importe les certificats :

~~~
# zmcertmgr addcacert /opt/zimbra/ssl/zimbra/ca/ca.pem 
** Importing certificate /opt/zimbra/ssl/zimbra/ca/ca.pem to CACERTS as zcs-user-ca.pem...done.
** NOTE: mailboxd must be restarted in order to use the imported certificate.
# zmcertmgr addcacert startssl-ca.pem
** Importing certificate startssl-ca.pem to CACERTS as zcs-user-startssl-ca.pem...done.
** NOTE: mailboxd must be restarted in order to use the imported certificate.
# zmcertmgr addcacert sub.class1.server.ca.pem
** Importing certificate sub.class1.server.ca.pem to CACERTS as zcs-user-sub.class1.server.ca.pem...done.
** NOTE: mailboxd must be restarted in order to use the imported certificate.
~~~

Enfin, on doit redémarrer Zimbra.

Pour installer un certificat externe, vous devez :

* Générer un fichier CSR :

~~~
$ zmcertmgr createcsr comm -new
~~~

* Générer le certificat via ce CSR
* Installer le certificat obtenu (par exemple z.example.com.crt) + chaîne de certification :

~~~
$ /opt/zimbra/bin/zmcertmgr deploycrt comm /tmp/z.example.com.crt /tmp/ca.crt 
** Verifying /tmp/z.example.com.crt against /opt/zimbra/ssl/zimbra/commercial/commercial.key
Certificate (/tmp/z.example.com.crt) and private key (/opt/zimbra/ssl/zimbra/commercial/commercial.key) match.
Valid Certificate: /tmp/z.example.com.crt: OK
** Copying /tmp/z.example.com.crt to /opt/zimbra/ssl/zimbra/commercial/commercial.crt
** Appending ca chain /tmp/ca.crt to /opt/zimbra/ssl/zimbra/commercial/commercial.crt
** Importing certificate /opt/zimbra/ssl/zimbra/commercial/commercial_ca.crt to CACERTS as zcs-user-commercial_ca...done.
** NOTE: mailboxd must be restarted in order to use the imported certificate.
** Saving server config key zimbraSSLCertificate...done.
** Saving server config key zimbraSSLPrivateKey...done.
** Installing mta certificate and key...done.
** Installing slapd certificate and key...done.
** Installing proxy certificate and key...done.
** Creating pkcs12 file /opt/zimbra/ssl/zimbra/jetty.pkcs12...done.
** Creating keystore file /opt/zimbra/mailboxd/etc/keystore...done.
** Installing CA to /opt/zimbra/conf/ca...done.
~~~

Attention, le fichier ca.crt doit comprendre l'ensemble de la chaîne de certification, souvent certificat racine puis certificat intermédiaire.


### Commandes diverses

~~~
# Redémarrer Amavis
$ zmamavisdctl restart
# Redémarrer ClamAV
$ zmantivirusctl restart

# Dump LDAP
$ ./openldap/sbin/slapcat -F /opt/zimbra/data/ldap/config
$ /opt/zimbra/libexec/zmslapcat /tmp
$ /opt/zimbra/libexec/zmslapcat -c /tmp

# Gestion LDAP
$ ldap status
slapd running pid: 24709
$ ldap stop
$ ldap start

# Restaurer l'ensemble des paramètres via LDAP
$ ldap stop
$ rm -f data/ldap/hdb/db/__db.* data/ldap/hdb/db/*.bdb data/ldap/hdb/db/alock data/ldap/hdb/logs/*
$ ./openldap/sbin/slapadd -F /opt/zimbra/data/ldap/config -b "" -q -l /tmp/ldap.bak 
_#################### 100.00% eta   none elapsed            none fast!
Closing DB...
$ vim conf/localconfig.xml
$ ldap start
# /etc/init.d/zimbra restart

$ zmlocalconfig -s zimbra_ldap_userdn
$ zmlocalconfig -s zimbra_ldap_password

# Transfert des sauvegardes des données des comptes (uniquement pour des versions identiques)
$ scp -r /opt/zimbra/backup/sessions/full-20120428.NNN newserver.example.com:/opt/zimbra/backup/sessions/
$ scp -r /opt/zimbra/backup/accounts.xml newserver.example.com:opt/zimbra/backup/

# Corriger les permissions
# /opt/zimbra/libexec/zmfixperms

# Détails d'un compte
$ zmprov ga COMPTE

# Changer le mot de passe d'un compte (dont admin)
$ zmprov sp COMPTE PASSWORD
~~~

## Stockage des données

<https://wiki.zimbra.com/wiki/Account_mailbox_database_structure>

Des informations sont stockées dans MySQL (bases mboxgroup*) et dans les répertoires *store/* et *index/*.

Pour un utilisateur donné, on peut déduire la base utilisée :

~~~
$ zmprov getMailboxInfo jdoe@z.example.com
mailboxId: 42
~~~

Les données de cet utilisateur seront dans la mboxgroup* (il suffit de faire mailboxId % 100).

### Candendrier

~~~
MariaDB [(none)]> use mboxgroup42

MariaDB [mboxgroup42]> select * from appointment where uid='XXXX';

MariaDB [mboxgroup42]> select * from appointment where metadata LIKE ='XXXX';
MariaDB [mboxgroup42]> select * from mail_item where index_id=NNNN;
~~~

NNN étant l'item_id de la table appointment.

## Mises à jour / migration

Il faut mettre régulièrement à jour Zimbra, particulièrement lors des mises-à-jour critiques car des failles peuvent exister.

Attention, vous devez avoir au moins 5 Go d'espace libre.

## Changement de serveur (en gardant la même version de Zimbra)

Si vous utilisez strictement la même version d'OS (64 bits, etc.), vous pouvez :

* Installer votre OS puis un Zimbra vierge
* Couper Zimbra sur l'ancien et le nouveau serveur
* Transférer /opt/zimbra
* Mettre à jour l'IP dans /etc/hosts
* Mettre à jour l'UID/GID dans /opt/zimbra/conf/localconfig.xml
* Redémarrer Zimbra
* Mettre à jour l'IP dans l'admin Zimbra (restrictions MRA)
* Redémarrer Zimbra

### Mise-à-jour mineure de Zimbra (7.0.0 vers 7.2.6) sous Ubuntu 10.04 LTS 64bits

~~~
$ wget http://files2.zimbra.com/downloads/7.2.6_GA/zcs-NETWORK-7.2.6_GA_2926.UBUNTU10_64.20131203120019.tgz
$ tar xvf zcs-NETWORK-7.2.6_GA_2926.UBUNTU10_64.20131203120019.tgz
# cd zcs-NETWORK-7.2.6_GA_2926.UBUNTU10_64.20131203120019
# ./install.sh

Operations logged to /tmp/install.log.8252
Checking for existing installation...
    zimbra-ldap...FOUND zimbra-ldap-7.0.0_GA_3077.UBUNTU10_64
    zimbra-logger...FOUND zimbra-logger-7.0.0_GA_3077.UBUNTU10_64
    zimbra-mta...FOUND zimbra-mta-7.0.0_GA_3077.UBUNTU10_64
    zimbra-snmp...FOUND zimbra-snmp-7.0.0_GA_3077.UBUNTU10_64
    zimbra-store...FOUND zimbra-store-7.0.0_GA_3077.UBUNTU10_64
    zimbra-apache...FOUND zimbra-apache-7.0.0_GA_3077.UBUNTU10_64
    zimbra-spell...FOUND zimbra-spell-7.0.0_GA_3077.UBUNTU10_64
    zimbra-convertd...FOUND zimbra-convertd-7.0.0_GA_3077.UBUNTU10_64
    zimbra-memcached...NOT FOUND
    zimbra-proxy...NOT FOUND
    zimbra-archiving...NOT FOUND
    zimbra-cluster...NOT FOUND
    zimbra-core...FOUND zimbra-core-7.0.0_GA_3077.UBUNTU10_64
ZCS upgrade from 7.0.0 to 7.2.6 will be performed.
Checking for available license file...
Current Users=14 Licensed Users=15

Saving existing configuration file to /opt/zimbra/.saveconfig

ZIMBRA NE END USER LICENSE AGREEMENT
[...]
Do you agree with the terms of the software license agreement? [N] y

Checking for prerequisites...
     FOUND: NPTL
     FOUND: netcat-openbsd-1.89-3ubuntu2
     FOUND: sudo-1.7.2p1-1ubuntu5.6
     FOUND: libidn11-1.15-2
     FOUND: libpcre3-7.8-3build1
     FOUND: libgmp3c2-2:4.3.2+dfsg-1ubuntu1
     FOUND: libexpat1-2.0.1-7ubuntu1.1
     FOUND: libstdc++6-4.4.3-4ubuntu5.1
     FOUND: libperl5.10-5.10.1-8ubuntu2.3
Checking for suggested prerequisites...
     FOUND: perl-5.10.1
     FOUND: sysstat
     FOUND: sqlite3
Prerequisite check complete.
Checking current number of databases...
Checking for a recent backup

Do you want to verify message store database integrity? [Y]
Verifying integrity of message store databases.  This may take a while.
mysqld is alive
Database errors found.
/opt/zimbra/mysql/bin/mysqlcheck --defaults-file=/opt/zimbra/conf/my.cnf -S /opt/zimbra/db/mysql.sock -A -C -s -u root --auto-repair --password=XXXXXXX
mysql.general_log
 Error    : You can't use locks with log tables.
 mysql.slow_log
 Error    : You can't use locks with log tables.

Checking for installable packages

Found zimbra-core
Found zimbra-ldap
Found zimbra-logger
Found zimbra-mta
Found zimbra-snmp
Found zimbra-store
Found zimbra-apache
Found zimbra-spell
Found zimbra-convertd
Found zimbra-memcached
Found zimbra-proxy
Found zimbra-archiving


The Zimbra Collaboration Suite appears already to be installed.
It can be upgraded with no effect on existing accounts,
or the current installation can be completely removed prior
to installation for a clean install.

Do you wish to upgrade? [Y] 

Select the packages to install
    Upgrading zimbra-core
    Upgrading zimbra-ldap
    Upgrading zimbra-logger
    Upgrading zimbra-mta
    Upgrading zimbra-snmp
    Upgrading zimbra-store
    Upgrading zimbra-apache
    Upgrading zimbra-spell
    Upgrading zimbra-convertd

Install zimbra-memcached [N] 
Install zimbra-proxy [N] 
Install zimbra-archiving [N] 

Checking required space for zimbra-core
checking space for zimbra-store

Installing:
    zimbra-core
    zimbra-ldap
    zimbra-logger
    zimbra-mta
    zimbra-snmp
    zimbra-store
    zimbra-apache
    zimbra-spell
    zimbra-convertd

The system will be modified.  Continue? [N] y

Shutting down zimbra mail

Backing up the ldap database...done.

Removing existing packages

   zimbra-ldap...done
   zimbra-logger...done
   zimbra-mta...done
   zimbra-snmp...done
   zimbra-store...done
   zimbra-spell...done
   zimbra-convertd...done
   zimbra-apache...done

Removing deployed webapp directories
Installing packages

    zimbra-core......zimbra-core_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-ldap......zimbra-ldap_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-logger......zimbra-logger_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-mta......zimbra-mta_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-snmp......zimbra-snmp_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-store......zimbra-store_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-apache......zimbra-apache_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-spell......zimbra-spell_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done
    zimbra-convertd......zimbra-convertd_7.2.6_GA_2926.UBUNTU10_64_amd64.deb...done

Setting defaults from saved config in /opt/zimbra/.saveconfig/config.save
   HOSTNAME=z.example.com
   LDAPHOST=z.example.com
   LDAPPORT=389
   SNMPTRAPHOST=z.example.com
   SMTPSOURCE=admin@z.example.com
   SMTPDEST=admin@z.example.com
   SNMPNOTIFY=yes
   SMTPNOTIFY=yes
   LDAPROOTPW=XXX
   LDAPZIMBRAPW=XXX
   LDAPPOSTPW=XXX
   LDAPREPPW=XXX
   LDAPAMAVISPW=XXX
   LDAPNGINXPW=XXX
Restoring existing configuration file from /opt/zimbra/.saveconfig/localconfig.xml...done
Operations logged to /tmp/zmsetup.01092014-000043.log
Upgrading from 7.0.0_GA_3077 to 7.2.6_GA_2926
Stopping zimbra services...done.
Verifying /opt/zimbra/conf/my.cnf
Starting mysql...done.
This appears to be 7.0.0_GA
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...already running.
Redolog version update required.
Thu Jan  9 00:01:23 2014: Verified redolog version 1.30.
Thu Jan  9 00:01:23 2014: Updating Redolog schema version from 1.30 to 1.31.
Redolog version update finished.
Stopping mysql...done.
Updating from 7.0.1_GA
Updating from 7.1.0_GA
Updating from 7.1.1_GA
Updating from 7.1.2_GA
Updating from 7.1.3_GA
Updating from 7.1.4_GA
Updating from 7.2.0_GA
Updating from 7.2.1_GA
Updating from 7.2.2_GA
Updating from 7.2.3_GA
Updating from 7.2.4_GA
Updating from 7.2.5_GA
Updating from 7.2.6_GA
Updating global config and COS's with attributes introduced after 7.0.0_GA...done.
Stopping ldap...done.
Upgrade complete.

Running bdb db_recover...done.
Running zmldapapplyldif...done.
Checking ldap status....already running.
Setting defaults...done.
Setting defaults from existing config...done.
Checking for port conflicts
Setting defaults from ldap...done.
Saving config in /opt/zimbra/config.11327...done.
Operations logged to /tmp/zmsetup.01092014-000043.log
Setting local config values...done.
Setting up CA...done.
Deploying CA to /opt/zimbra/conf/ca ...done.
Warning: No valid SSL certificates were found.
New self-signed certificates will be generated and installed.
Creating SSL certificate...done.
Creating SSL certificate...done.
Creating SSL certificate...done.
Installing mailboxd SSL certificates...done.
Setting replication password...done.
Setting Postfix password...done.
Setting amavis password...done.
Setting nginx password...done.
Creating server entry for z.example.com...already exists.
Saving CA in ldap ...done.
Saving SSL Certificate in ldap ...done.
Setting spell check URL...done.
Setting service ports on z.example.com...done.
Adding z.example.com to zimbraMailHostPool in default COS...done.
Installing webclient skins...
        lake...done.
        lemongrass...done.
        carbon...done.
        tree...done.
        steel...done.
        waves...done.
        bare...done.
        pebble...done.
        hotrod...done.
        oasis...done.
        twilight...done.
        lavender...done.
        bones...done.
        sky...done.
        beach...done.
        smoke...done.
        sand...done.
Finished installing webclient skins.
Setting Keyboard Shortcut Preferences...done.
Setting zimbraFeatureTasksEnabled=TRUE...done.
Setting zimbraFeatureBriefcasesEnabled=TRUE...done.
Setting MTA auth host...done.
Setting TimeZone Preference...done.
prov> Initializing mta config...done.
Setting services on z.example.com...done.
Creating user spam.r4nd0m@z.example.com...already exists.
Creating user ham.r4nd0m@z.example.com...already exists.
Creating user virus-quarantine.r4nd0m@z.example.com...already exists.
Setting spam training and Anti-virus quarantine accounts...done.
Configuring SNMP...done.
Checking for default IM conference room...already initialized.
Setting up syslog.conf...Failed
Looking for valid license to install...license already installed.
Activating license...license activated.
Starting servers...done.
Checking for deprecated zimlets...done.
Installing common zimlets...
        com_zimbra_viewmail...done.
        com_zimbra_srchhighlighter...done.
        com_zimbra_cert_manager...done.
        com_zimbra_attachmail...done.
        com_zimbra_social...done.
        com_zimbra_attachcontacts...done.
        com_zimbra_phone...done.
        com_zimbra_bulkprovision...done.
        com_zimbra_webex...done.
        com_zimbra_linkedin...done.
        com_zimbra_url...done.
        com_zimbra_email...done.
        com_zimbra_date...done.
        com_zimbra_dnd...done.
        com_zimbra_adminversioncheck...done.
Finished installing common zimlets.
Installing network zimlets...
        com_zimbra_backuprestore...done.
        com_zimbra_license...done.
        com_zimbra_smime_cert_admin...done.
        com_zimbra_mobilesync...done.
        com_zimbra_hsm...done.
        com_zimbra_delegatedadmin...done.
        com_zimbra_smime...done.
        com_zimbra_convertd...done.
Finished installing network zimlets.
Getting list of all zimlets...done.
Updating non-standard zimlets...
        com_zimbra_webex...done.
Finished updating non-standard zimlets.
Restarting mailboxd...done.
Setting up zimbra crontab...done.

Moving /tmp/zmsetup.01092014-000043.log to /opt/zimbra/log

Configuration complete - press return to exit
~~~

### Mise à jour majeure de Zimbra (7.2 vers 8.6)

Attention : pour une mise à jour majeure, assurez-vous d'avoir une licence non perpétuelle (car les mises à jour majeures ne sont pas supportées !)

Les méthodes recommandées par le support Zimbra sont :
<https://wiki.zimbra.com/wiki/ZCS_to_ZCS_rsync_Migration> et <https://wiki.zimbra.com/wiki/Ubuntu_Upgrades>

En résumé, la mise à jour se fait en réinstallant Zimbra (install.sh).

Par exemple, pour passer de Ubuntu 10.04/ZCS 7.2 en Ubuntu 14.04/ZCS 8.6 il faut :

* Sous Ubuntu 10.04, passer de ZCS 7.2 à ZCS 8.0.9
* Passer de Ubuntu 10.04 en Ubuntu 12.04
* Sous Ubuntu 12.04, passer de ZCS 8.0.9 en 8.6
* Passer de Ubuntu 12.04 en Ubuntu 14.04
* Réinstaller ZCS 8.6 avec les packages pour Ubuntu 14.04

~~~
# cd zcs-NETWORK-8.0.9_GA_6191.UBUNTU10_64.20141103151643
# ./install.sh -a /root/activation.xml 

[...]
The Zimbra Collaboration Server appears already to be installed.
It can be upgraded with no effect on existing accounts,
or the current installation can be completely removed prior
to installation for a clean install.

Do you wish to upgrade? [Y] 
[...]

Upgrading from 7.2.6_GA_2926 to 8.0.9_GA_6191
Stopping zimbra services...done.
This appears to be 7.2.6_GA
Starting mysql...done.
Upgrading ldap data...done.
Upgrading LDAP configuration database...done.
Loading database...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Checking ldap status...already running.
Running mysql_upgrade...done.
Redolog version update required.
Thu May 19 12:20:18 2016: Verified redolog version 1.31.
Thu May 19 12:20:18 2016: Updating Redolog schema version from 1.31 to 1.42.
Redolog version update finished.
Schema upgrade required from version 65 to 92.
Running /opt/zimbra/libexec/scripts/migrate20120611_7to8_bundle.pl
Thu May 19 12:20:22 2016: Verified schema version 65.
Thu May 19 12:20:23 2016: Adding device information columns to ZIMBRA.MOBILE_DEVICES table...
Thu May 19 12:20:24 2016: Adding ZIMBRA.PENDING_ACL_PUSH table...
Thu May 19 12:20:24 2016: Adding TAG table...
............
Thu May 19 12:20:35 2016: Adding TAGGED_ITEM table...
............
Thu May 19 12:20:46 2016: Adding version and last_purge_at columns to ZIMBRA.MAILBOX table...
Thu May 19 12:20:47 2016: Adding/Modifying columns in mail_item, mail_item_dumpster, revision, revision_dumpster tables...
............
............
............
............
Thu May 19 12:22:09 2016: Dropping IM tables...
Thu May 19 12:22:11 2016: Verified schema version 65.
Thu May 19 12:22:11 2016: Updating DB schema version from 65 to 90.
Running /opt/zimbra/libexec/scripts/migrate20120410-BlobLocator.pl
Thu May 19 12:22:14 2016: Verified schema version 90.
Thu May 19 12:24:22 2016: Verified schema version 90.
Thu May 19 12:24:22 2016: Updating DB schema version from 90 to 91.
Running /opt/zimbra/libexec/scripts/migrate20121009-VolumeBlobs.pl
Thu May 19 12:24:26 2016: Verified schema version 91.
Thu May 19 12:24:28 2016: Verified schema version 91.
Thu May 19 12:24:28 2016: Updating DB schema version from 91 to 92.
Stopping mysql...done.
Updating from 7.2.7_GA
Updating from 7.2.8_GA
Updating from 8.0.0_BETA1
Adding dynamic group configuration
Stopping ldap...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Stopping ldap...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Updating from 8.0.0_BETA2
Starting mysql...done.
Stopping mysql...done.
Updating from 8.0.0_BETA3
Updating from 8.0.0_BETA4
Stopping ldap...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Stopping ldap...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Stopping ldap...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Stopping ldap...done.
Checking ldap status...not running.
Running zmldapapplyldif...done.
Checking ldap status...not running.
Starting ldap...done.
Updating from 8.0.0_BETA5
Updating from 8.0.0_GA
Updating from 8.0.1_GA
Updating from 8.0.2_GA
Updating from 8.0.3_GA
Updating from 8.0.4_GA
Updating from 8.0.5_GA
Updating from 8.0.6_GA
Updating from 8.0.7_GA
Updating from 8.0.8_GA
Updating from 8.0.9_GA
Updating global config and COS's with attributes introduced after 7.2.6_GA...done.
Stopping ldap...done.
Upgrade complete.

[...]

# do-release-upgrade

# apt-get install libperl5.14 unzip
# cd zcs-NETWORK-8.6.0_GA_1153.UBUNTU12_64.20141215195643
# ./install.sh -s --skip-activation-check

[...]
   Do you want to verify message store database integrity? [Y] No
[...]

Software Installation complete!

# ./install.sh

[...]
ZCS upgrade from 8.6.0 to 8.6.0 will be performed.
[...]

[...]
   Do you want to verify message store database integrity? [Y] Yes
[...]

Updating from 8.5.0_BETA1
Updating from 8.5.0_BETA2
Updating from 8.5.0_BETA3
Updating from 8.5.0_GA
Updating from 8.5.1_GA
Updating from 8.6.0_BETA1
Updating from 8.6.0_BETA2
Updating from 8.6.0_GA
Updating global config and COS's with attributes introduced after 8.0.9_GA...done.
[...]


# do-release-upgrade

# apt install libperl5.18
# cd zcs-NETWORK-8.6.0_GA_1153.UBUNTU14_64.20141215151218
# ./install.sh -s --skip-activation-check

[...]
   Do you want to verify message store database integrity? [Y] No
[...]

# ./install.sh

[...]
   Do you want to verify message store database integrity? [Y] Yes
[...]

# /opt/zimbra/libexec/zmfixperms
# reboot

~~~


### Migration de données

En dehors des sauvegardes (pour des versions de Zimbra identiques) et des mises à jour majeures, il existe des outils plus ou fonctionnels pour migrer des données :

* l'outil zmztozmig : attention, pas mal de choses ne sont pas conservées, comme les préférences utilisateur, les items partagés et parfois une partie des données :(
  voir <https://wiki.zimbra.com/wiki/Zimbra_to_Zimbra_Migration>

~~~
[INFO|main:1| 05/18/2016 03:29:05]: ****************SUMMARY************************** 
[INFO|main:1| 05/18/2016 03:29:05]: Total Accounts processed         :    12 
[INFO|main:1| 05/18/2016 03:29:05]: Successfull Accounts             :    12 
[INFO|main:1| 05/18/2016 03:29:05]: Failed accounts                  :    0 
[INFO|main:1| 05/18/2016 03:29:05]: Total Migration Time(seconds)    :    2791.538 
[INFO|main:1| 05/18/2016 03:29:05]: ************************************************* 
~~~

* l'outil _zmmailbox getRestURL/postRestURL_ : zmmailbox -z -m ACCOUNT getRestURL "/?fmt=tgz" > /tmp/ACCOUNT.tgz voir <http://linux-sys-adm.com/how-to-migrate-zimbra-678-and-next-version-with-script-from-one-server-to-another/>
* requête sur l'interface REST directement : voir <https://wiki.zimbra.com/wiki/Zimbra_to_Zimbra_Server_Migration>

Exemples :

~~~
$ wget --no-check-certificate https://USER:PASS@zimbra.example.com/service/home/USER/calendar?fmt=ics
$ wget --no-check-certificate https://USER:PASS@zimbra.example.com/service/home/USER/contacts?fmt=csv
$ wget --no-check-certificate https://USER:PASS@zimbra.example.com/service/home/USER/contacts?fmt=vcf
~~~

et l'on peut réinjecter les données de façon similaire.


## FAQ

### Il manque des menus dans l'interface d'admin

C'est probablement que les zimlets par défaut ne sont pas installés.
Vérifier avec la commande `zmzimletctl listZimlets`.
Si aucun zimlets n'est présent on les installera à la main (en théorie ça devrait être installé automatiquement…) :

~~~
$ cd /opt/zimbra/zimlets
$ for i in *.zip; do zmzimletctl deploy $i; done$
$ cd ../zimlets-network/
$ for i in *.zip; do zmzimletctl deploy $i; done
~~~

### Failles de sécurité

Il est important de mettre à jour régulièrement Zimbra. Il y a notamment _des remote exploits_ qui permettent
à n'importe qui de prendre la main sur votre Zimbra si vous n'êtes pas à jour !

Voir <http://www.zimbra.com/forums/announcements/>

Citons notamment la faille de décembre 2013 décrite sur <http://www.exploit-db.com/exploits/30472/>
qui permet d'avoir accès au localconfig.xml contenant plusieurs mots de passe critiques via :

<https://z.example.com/res/I18nMsg,AjxMsg,ZMsg,ZmMsg,AjxKeys,ZmKeys,ZdMsg,Ajx%20TemplateMsg.js.zgz?v=091214175450&skin=../../../../../../../../../opt/zimbra/conf/localconfig.xml%00>

### Erreurs Java SSL

Java est strict avec le SSL... difficile d'ignorer les erreurs.

En cas de certificat expiré, chaîne de certification invalide (_unable to find valid certification path to requested target_), Java va vous jeter.

Vous devez donc avoir un certificat valide, et une chaîne de certification correcte (en ajoutant manuellement les certificats CA si besoin, voir [HowtoZimbra#Gestiondescertificats]()

### GAL LDAP externe cassée

Si vous n'avez plus de GAL via LDAP sur votre smartphone et que vous obtenez "ldap search failure" lors d'une recherche dans la GAL via l'interface web, la solution (à confirmer) est d'importer votre certificat dans le keytool :

~~~
# openssl s_client -connect ldap.example.com:636 > EXTERNAL-LDAP-CERT-FILE
# /opt/zimbra/java/bin/keytool -import -alias EXTERNAL-LDAP -keystore /opt/zimbra/java/jre/lib/security/cacerts -storepass changeit -file /tmp/EXTERNAL-LDAP-CERT-FILE
Certificate was added to keystore
# /etc/init.d/zimbra stop
# /etc/init.d/zimbra start
~~~

### Purge d'une boîte mail

Par exemple de la boîte d'admin :

~~~
$ zmmailbox -z -m admin@z.example.com emptyFolder /Inbox
~~~

### Erreur ssl_error_weak_server_ephemeral_dh_key sous Firefox/Thunderbird

Il faut désactiver certains ciphers :

~~~
$ zmprov mcf +zimbraSSLExcludeCipherSuites TLS_DHE_RSA_WITH_AES_128_CBC_SHA
$ zmprov mcf +zimbraSSLExcludeCipherSuites TLS_DHE_RSA_WITH_AES_256_CBC_SHA
$ zmprov mcf +zimbraSSLExcludeCipherSuites SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA
$ zmmailboxdctl restart
Stopping mailboxd...done.
Starting mailboxd...done.
~~~

Voir <https://wiki.zimbra.com/wiki/Disabling_the_use_of_weak_DH_keys_in_Zimbra_Collaboration_mailboxd>

### Installation de nouveaux modules Zimbra

Pour ajouter de nouveaux modules (par exemple, certains modules sont nécessaires **avant** de faire une upgrade, notamment *zimbra-proxy* et *zimbra-memcached* doivent être installés avant d'upgrader en Zimbra 8.8),
il faut ré-exécuter le script `install.sh` original et choisir d'installer ces nouveaux modules.

### Quotas d'envois de courriels

Il faut de prime abord configurer cpolicyd : <https://wiki.zimbra.com/wiki/Cluebringer_Policy_Daemon>

Par la suite il faut définir un "policy" qui correspond aux comptes
à qui s’applique le quota, déclarer la nature du quota et puis
préciser les limites du quota.
