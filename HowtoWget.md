---
title: Howto wget
categories: tips wget
...

* Page de manuel : <https://www.gnu.org/software/wget/manual/wget.html>

# HowTo Wget

Wget est un client supportant les protocoles HTTP, HTTPS et FTP pour récupérer du contenu distant sur Internet et ainsi que le téléchargement au travers des proxies HTTP.

## Utilisation de base

Exemples d'utilisation :

Récupérer un fichier en limitant la bande passante et le sauvegardant sous un nom différent : 

~~~
$ wget --limit-rate=100K https://ftp.acc.umu.se/cdimage/release/current/amd64/iso-cd/debian-9.3.0-amd64-netinst.iso -O debian9.iso
~~~

Télécharger un fichier en mode silencieux avec l'affichage de son avancement :

~~~
$ wget -q --show-progress https://ftp.acc.umu.se/cdimage/release/current/amd64/iso-cd/debian-9.3.0-amd64-netinst.iso
~~~

Seulement pour télécharger les fichier dont l'extension est iso :

~~~
$ wget -r -A "*iso" https://ftp.acc.umu.se/cdimage/release/current/amd64/iso-cd/
~~~

Télécharger des fichiers en passant par un proxy :

~~~
$ wget --limit-rate=100K -A *.mpeg -r http://dc5video.debian.net/2005-07-09/
$ http_proxy=http://192.168.14.4:3128 wget -p -H www.thomascook.fr
~~~


Voici les différentes options possibles :

* `-c` : pour reprendre un téléchargement déjà commencé
* `-q` : Pour le mode silencieux
* `-O` : fichier de sortie
* `--show-progress` : Montre la progression du téléchargement
* `-4` : utilise la liaison ipv4
* `-6` : utilise la liaison ipv6
* `--limit-rate=100k` : pour limiter le téléchargement à 100 K**o**/s
* `--no-check-certificate` : Ne vérifie pas la validité des certificats
* `-r` : téléchargement récursif d'un site (mode « aspirateur »)
* `-l <niveau>` (défaut=5) : niveau de répertoires à explorer pour l'aspirateur (`-l 0` : aucun)
* `-a <regex>` : pour limiter à une expression régulière
* `-X /*/*/*/foo,/*/*/*/bar` : Exclu les sous-dossiers portant le nom foo et bar
* `--reject-regex <regex>` : Exclus l'accès aux urls qui match avec l'expression régulière
* `--accept-regex <regex>` : Inclus l'accès aux urls qui match avec l'expression régulière
* `-A` : filtre le contenu que l'on veut récupérer
* `-R "*-mac-*"` : filtre le contenu que l'on ne veut pas récupérer
* `-P <foo>`: pour spécifier le répertoire foo/ où mettre le contenu téléchargé
* `-p` : prendre tous les fichiers dont la page a besoin
* `-H` : active le teléchargement de « pages extérieures » au site demandé
* `-nc` : Ne télécharge pas le fichier s'il est déjà présent
* `-nd` : Ne représente pas l'arborescence de fichier
* `-i <fichier>` : Liste d'URL désignant les fichiers à télécharger
* `-e robots=off` : Ne respecte pas les directives incluse dans robots.txt
* `--no-parent` : Ne monte jamais dans le répertoire parent lors de la récupération récursive
* `--mirror` : Équivalent aux options `-r -N -l inf --no-remove-listing`

En général, on voudra utiliser l'option `--no-parent` avec les options `--mirror` ou `-r`.
