---
categories: mail
title: Howto Mailman
...

* Documentation : <http://docs.mailman3.org/>

[Mailman](http://www.list.org/) est un logiciel libre de gestion de listes de diffusion. Il est développé en Python depuis plus de 20 ans. Il est souvent utilisé dans projets libres comme Debian, Python, Wikimédia, etc. Il est plus simple que [Sympa](HowtoSympa) car il ne nécessite pas forcément de base de données SQL, mais ses fonctionnalités sont moins avancées.

> *Le saviez-vous ?* Le 1er jour de chaque mois est parfois appelé *Mailman day* car ce jour là Mailman envoie classiquement un email de rappel d'inscription à tous les inscrits à chaque liste de diffusion.

## Installation

~~~
# apt install mailman nginx fcgiwrap postfix

# systemctl status mailman
● mailman.service - LSB: Mailman Master Queue Runner
   Loaded: loaded (/etc/init.d/mailman)
  Process: 16017 ExecStop=/etc/init.d/mailman stop (code=exited, status=0/SUCCESS)
  Process: 16050 ExecStart=/etc/init.d/mailman start (code=exited, status=0/SUCCESS)
   CGroup: /system.slice/mailman.service
           ├─16062 /usr/bin/python /usr/lib/mailman/bin/mailmanctl -s -q start
           ├─16063 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=ArchRunner:0:1 -s
           ├─16064 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=BounceRunner:0:1 -s
           ├─16065 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=CommandRunner:0:1 -s
           ├─16066 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=IncomingRunner:0:1 -s
           ├─16067 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=NewsRunner:0:1 -s
           ├─16068 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=OutgoingRunner:0:1 -s
           ├─16069 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=VirginRunner:0:1 -s
           └─16070 /usr/bin/python /var/lib/mailman/bin/qrunner --runner=RetryRunner:0:1 -s
~~~

## Configuration

Configuration basique via le fichier `/etc/mailman/mm_cfg.py`:

~~~
DEFAULT_URL_PATTERN = 'http://%s/'
DEFAULT_SERVER_LANGUAGE = 'fr'
~~~

Il faut obligatoirement définir une liste nommée "mailman" avec listmaster / mot de passe :

~~~
# /usr/lib/mailman/bin/newlist mailman
~~~

On créera également un mot de passe global pour autoriser la création de listes :

~~~
# mmsitepass
~~~

### Configuration avec Nginx

Exemple de configuration pour [Nginx](HowtoNginx) :

~~~
server {

    listen 80;
    server_name lists.example.com
    root /usr/lib/cgi-bin/mailman;

    location = / {
     rewrite ^ /listinfo permanent;
    }

    location / {
      fastcgi_split_path_info ^(/[^/]*)(.*)$;
      fastcgi_pass unix:/var/run/fcgiwrap.socket;
      include fastcgi.conf;
      fastcgi_param PATH_INFO         $fastcgi_path_info;
      fastcgi_param PATH_TRANSLATED   $document_root$fastcgi_path_info;
    }

    location /mailman-icons {
      alias /usr/lib/mailman/icons;
    }

    location /archives {
      alias /var/lib/mailman/archives/public;
      autoindex on;
    }

}
~~~

## Configuration avec Postfix 

Ajout dans `/etc/postfix/main.cf` :

~~~
alias_maps = hash:/etc/aliases hash:/var/lib/mailman/data/aliases
alias_database = hash:/etc/aliases hash:/var/lib/mailman/data/aliases
mydestination = lists.example.com
transport_maps = hash:/etc/postfix/transport
mailman_destination_recipient_limit = 1
~~~

et dans `/etc/postfix/transport` :

~~~
lists.example.com mailman:
~~~

Puis dans `/etc/mailman/mm_cfg.py` :

~~~
MTA='Postfix'
~~~

Et enfin, exécuter ces commandes :

~~~
# /usr/lib/mailman/bin/genaliases
# /etc/init.d/mailman restart
# /etc/init.d/postfix restart
~~~

## Administration

### En ligne de commande

On peut administrer Mailman en ligne de commande !

Lister les listes de diffusions :

~~~
# list_lists 
2 matching mailing lists found:
                 Mailman - [no description available]
                    Test - Liste de diffusion pour tests
~~~

Lister les admins d'une liste :

~~~
# list_admins Test
List: test,     Owners: jdoe@example.com
~~~

Lister les membres d'une liste :

~~~
# list_members Test
jdoe@example.com
test@example.com
~~~

Et de nombreuses commandes : `add_members`, `remove_members`, `newlist`, etc.

### SPF / DKIM / DMARC

On conseille bien sûr de configurer SPF, DMARC, ainsi que DKIM via [OpenDKIM](/HowtoOpenDKIM).

Pour la configuration d'une liste, on choisira l'option :

~~~
Replace the From: header address with the list's posting address to mitigate issues stemming from the original From: domain's DMARC or similar policies.

MUNGE FROM
~~~

### Modifier DEFAULT_URL_PATTERN

<https://wiki.list.org/DOC/4.27%20Securing%20Mailman%27s%20web%20GUI%20by%20using%20Secure%20HTTP-SSL%20%28HTTPS%29>

Si l'on modifie *DEFAULT_URL_PATTERN* dans le fichier `/etc/mailman/mm_cfg.py`, cela sera pris en compte pour toutes les futures listes **mais** il faut relancer la commande suivante pour que cela soit pris en compte pour les anciennes listes déjà créées :

~~~
# /usr/sbin/withlist -l -a -r fix_url
Importing fix_url...
Running fix_url.fix_url()...
Loading list mailman (locked)
Saving list
Loading list test (locked)
Saving list
[...]
Finalizing
~~~
