**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

bnx2 et bnx2x sont des firmwares utilisés (entre autres ?) par les cartes Broadcom NetXtreme II :

~~~
0b:00.1 Ethernet controller: Broadcom Corporation NetXtreme II BCM5709 Gigabit Ethernet (rev 20)
~~~

Étant donné que ceux-ci ne sont pas libre, il ne sont pas intégré dans l'installateur Debian et la carte réseau ne sera pas fonctionnelle.
Il est nécessaire de télécharger le firmware sous forme de .deb (nonfree) et de le placer à la racine d'une clé USB (qui peut être aussi bien en ext3 qu'en vfat). L'installateur Debian détectera automatiquement le paquet est l'utilisera.

*Liens (suivant le modèle de la carte) :*

* <http://packages.debian.org/lenny/firmware-bnx2>
* <http://packages.debian.org/lenny/firmware-bnx2x>