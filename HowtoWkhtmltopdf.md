---
categories: web tool
title: Howto wkhtmltopdf
...

* Documentation : <https://wkhtmltopdf.org/usage/wkhtmltopdf.txt>

[wkhtmltopdf](https://wkhtmltopdf.org/) est un outil libre pour transformer un document HTML au format PDF. Il s'utilise en ligne de commande et ne nécessite pas de lancer un environnement graphique grâce à l'utilisation du moteur [Qt WebKit](https://wiki.qt.io/Qt_WebKit).

## Installation

wkhtmltopdf est disponible dans les dépôts officiels de Debian mais cela nécessite d'installer un serveur X.

Nous préférons l'installer via le package `.deb` (qui fonctionne bien avec Debian 10 malgré son nom) fourni avec des librairies statiques sur le [site officiel](https://wkhtmltopdf.org/downloads.html) :

~~~
# apt install ca-certificates fontconfig fontconfig-config fonts-dejavu-core libfontconfig1 libfontenc1 \
  libfreetype6 libjpeg62-turbo libpng16-16 libxfont2 libxrender1 openssl ucf x11-common xfonts-75dpi \
  xfonts-base xfonts-encodings xfonts-utils

# cd /tmp 
# wget --no-check-certificate https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb
# chmod 644 wkhtmltox_0.12.6-1.buster_amd64.deb
# apt install ./wkhtmltox_0.12.6-1.buster_amd64.deb
~~~

> *Note* : Pour Debian 9 :
>
> ~~~
> # apt install ca-certificates fontconfig fontconfig-config fonts-dejavu-core libfontconfig1 libfontenc1 \
>  libfreetype6 libjpeg62-turbo libpng16-16 libxfont1 libxrender1 openssl ucf x11-common xfonts-75dpi \
>  xfonts-base xfonts-encodings xfonts-utils
>  
> # cd /tmp 
> # wget --no-check-certificate https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.stretch_amd64.deb
> # chmod 644 wkhtmltox_0.12.6-1.stretch_amd64.deb
> # apt install wkhtmltox_0.12.6-1.stretch_amd64.deb
> ~~~
> 
> Pour Debian 8 :
>
> ~~~
> # apt install fontconfig fontconfig-config fonts-dejavu-core libexpat1 libfontconfig1 \
>   libfontenc1 libfreetype6 libjpeg62-turbo libpng12-0 libxfont1 libxrender1 ucf \
>   x11-common xfonts-75dpi xfonts-base xfonts-encodings xfonts-utils
> # wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.jessie_amd64.deb
> # dpkg -i wkhtmltox_0.12.5-1.jessie_amd64.deb
> ~~~



## Pixelization

Si le rendu devient pixelisé (en général sur la font par défaut), remplir le fichier de conf "no-bitmaps" correspondant à celui pointé par "/etc/fonts/conf.avail/70-no-bitmaps.conf".

~~~{.xml}
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
<!-- Reject bitmap fonts -->
 <selectfont>
   <rejectfont>
      <pattern>
           <patelt name="scalable"><bool>false</bool></patelt>
      </pattern>
   </rejectfont>
 </selectfont>
</fontconfig>
~~~

Ne pas oublier de rendre ce fichier en lecture pour tous.

voir <https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2193#issuecomment-136962264%29>

## wkhtmltopdf as a service

Utilisation avec [Node JS](HowtoNodeJS) inspirée de <https://github.com/traum-ferienwohnungen/docker-wkhtmltopdf-aas/blob/master/Dockerfile>

~~~
# su - foo
$ npm install yarn coffee-script forever bootprint bootprint-openapi
$ wget https://raw.githubusercontent.com/traum-ferienwohnungen/docker-wkhtmltopdf-aas/master/swagger.yaml
$ ./node_modules/.bin/bootprint openapi swagger.yaml documentation
$ wget https://raw.githubusercontent.com/traum-ferienwohnungen/docker-wkhtmltopdf-aas/master/package.json
$ wget https://raw.githubusercontent.com/traum-ferienwohnungen/docker-wkhtmltopdf-aas/master/app.coffee
$ ./node_modules/.bin/yarn install
$ ./node_modules/.bin/coffee --version
$ npm start
~~~

Script de lancement :

~~~
export USER='foo'
export PASS='PASSWORD'
export PAYLOAD_LIMIT='200mb'
npm start
~~~

Utilisation :

~~~
$ GET http://foo:PASSWORD@localhost:5555/metrics
$ GET http://foo:PASSWORD@localhost:5555/healthcheck
$ GET http://foo:PASSWORD@localhost:5555/status
~~~

### gestion via systemd

On définit un script `start.sh` :

~~~
#!/bin/sh

export USER='foo'
export PASS='PASSWORD'
export PAYLOAD_LIMIT='200mb'

umask 027

cd /foo/bar
npm start
~~~

Et une unité [systemd](HowtoSystemd) du type :

~~~
[Unit]
Description=wkhtmltopdf service
After=network.target

[Service]
Restart=always
User=foo
Group=foo
ExecStart=/foobar/start.sh

[Install]
WantedBy=multi-user.target
~~~
