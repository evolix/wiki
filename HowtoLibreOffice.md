---
title: Howto LibreOffice
---

Gestion des imprimantes :

~~~
$ /usr/lib/libreoffice/program/spadmin
~~~

## headless

Debian 8 :

~~~
# apt install libreoffice-writer openjdk-7-jre-headless
~~~

Pour avoir des jolies fonts :

~~~
# apt install t1-cyrillic
~~~

Pour faire des bidouilles avec les bases de données et LibreOffice :

~~~
# apt install libreoffice-sdbc-hsqldb libreoffice-report-builder-bin
~~~

## Manipulation de données

### Contrôle sur les données

Il est possible d'ajouter des contrôles sur les données numériques.
Une fois une cellule sélectionnée, on peut faire `clic droit > Validation des données`, ou bien passer par le menu `Données > Validité`. Dans l'onglet `Critère` on choisi alors la contrainte à appliquer. Il en existe plusieurs sortes.

### Menu de choix de valeurs

Parmi les contraintes il y en a sur les valeurs numériques, dates… Il y a aussi la possibilité de limité aux valeurs d'une plages de cellules. On choisit alors la contrainte `Autoriser: Plage de cellules`, puis on indique dans le champ `Source` la plage de cellules à utiliser.

### Valeur de cellule en fonction d'un choix

Si vous avez créé une série de clé/valeurs ainsi qu'une cellule qui indique le choix parmi cette série, il est possible de donner la valeur associée à une cellule.

Admettons qu'on ait comme clés/valeurs : `A1`:`Clé 1`, `A2`:`Valeur 1`, `B1`:`Clé 2`, `B2`:`Valeur 2`  
En `C3` on a indiqué une validité des données qui permet le choix entre `A1` et `B1`.  
En `C4` on veut la valeur associée à la clé choisie : `=INDEX(B1:B2;EQUIV(C3;A1:A2;0))`.  
En choisissant `Clé 2` dans le menu déroulant de `C3`, on aura `Valeur 2` qui s'affichera en `C4`.

## FAQ

### En tant qu'utilisateur, je lancer des commande "libreoffice --headless" mais je n'ai pas de résultat.

vérifier les droits de /var/spool/libreoffice (notamment le répertoire de cache)

### LibreOffice se blo

~~~
$ rm ~/.config/libreoffice/4/.lock
~~~
