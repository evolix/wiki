**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowtoRedmineGitHosting

[Redmine Git Hosting](http://redmine-git-hosting.io/) est un plugin permettant l'intégration de [Redmine](https://redmine.org/) avec [Gitolite](http://gitolite.com).

Il est edité par la société [Jbox Web](http://www.jbox-web.com/).

## Ajout de clé SSH

Pour ajouter une clé SSH à son compte il suffit de se rendre dans *Mon Compte* -> *Mes clés publiques* et de copier sa clé publique (.pub).

## Création d'un dépôt

Pour créer un dépôt, il faut se rendre dans la configuration d'un projet puis :

- Activer le module *Dépôt de sources* dans *Modules*

- Cliquer sur *Nouveau dépôt* dans *Dépôts*

- Choisir le *SCM* Gitolite

- Si l'identifiant est vide, le nom du projet sera utilisé par défaut

## Accès au dépôts

Les droits d'accès sont gérés au niveau des projets Redmine

Les membres du projet Redmine auront accès au dépôts du projet suivant leur rôles (Editable dans *Administration* -> *Rôles et permissions*)

Pour qu'un dépôt soit public, il faut que le projet parent soit aussi public

Attention un dépôt privé dans un projet public sera quand même public

## Notification Git

Les notifications Git se configure dans *Administration* -> *Redmine Git Hosting* -> *Notifications*

On peut changer le *Préfixe global des notifications* qui s'ajoute dans le sujet des mails, ex : *[REDMINE]*, *[GIT]*, *[FORGE EVOLIX]*

Choisir l' *Adresse de l'expéditeur des notifications*, ex: forge.evolix.org

## Mirroring Github

Pour créer un mirroir d'un de vos dépot sur Github il faut :

- Copier la clé publique de Redmine dans votre compte Github

- Créer un dépôt d'accueil sur Github

- Cliquer sur *Dépôt* -> *Configuration* -> *Mirroirs du dépôt* -> '''Ajouter un miroir de dépôt
Miroirs du dépôt'''