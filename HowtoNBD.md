**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# HowtoNBD

Network Block Device (NBD) est un protocole client/serveur qui émule un block device au dessus du réseau. Ça permet par exemple de partager un volume d'un disque via le réseau.

## Serveur

Le fichier de conf est /etc/nbd-server/config. Dans la section generic on mettra user/group à root pour accéder aux devices dans /dev.

À la manière de NFS on indiquera la liste des exports.


~~~
[pi01]
exportname = /dev/usb0/pi01
authfile = /etc/nbd-server/allow.pi01
~~~

/etc/nbd-server/allow.pi00

~~~
192.168.42.42
~~~

## Client

Monter un volume.


~~~
# nbd-client pi00 /dev/nbd0 -name pi01
~~~


