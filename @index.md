### Ne pas nommer les page HowtoFoo, mais Foo ?
→ [Sondage](https://framadate.org/fvyiusUOSOlNwY8O)  
Il a été convenu de garder « Howto » dans le nommage de la page pour le SEO. Mais sur la page d'accueil on ne le cite pas. Les liens sont donc de la forme `[Foo](HowtoFoo)`

### Organisation de l'index

→ [Sondage](https://framadate.org/LHoVrBD2ABSP5IJS)  
Il a été convenu de garder 1 lien par ligne.
