---
categories: mail
title: Howto Macromilter
...

* <https://github.com/sbidy/MacroMilter>

Macromilter un démon pour filter les emails avec des pièces jointes avec des macros Microsoft.

## Installation

Préparation :

~~~
# apt install python2.7 python2.7-dev libmilter-dev libmilter1.0.1 python-pip
# pip install oletools olefiles
# mkdir /etc/macromilter /var/log/macromilter
# pip install oletools pymilter configparser olefile (à confirmer)
~~~

Installation :

~~~
# cd /etc/macromilter
# wget https://raw.githubusercontent.com/sbidy/MacroMilter/master/macromilter/macromilter.py
# wget https://raw.githubusercontent.com/sbidy/MacroMilter/master/macromilter/config.ini
# chown postfix /etc/macromilter/*

# cd /etc/systemd/system/
# wget https://raw.githubusercontent.com/sbidy/MacroMilter/master/macromilter/macromilter.service
# systemctl daemon-reload

# cd /etc/logrotate.d/
# wget https://raw.githubusercontent.com/sbidy/MacroMilter/master/macromilter/macromilter.logrotate
# mv macromilter.logrotate macromilter
~~~

Il faut ajuster config.ini ainsi :

~~~
LOGFILE_DIR = /var/log/macromilter
LOGFILE_NAME = macromilter.log
~~~

Ajuster l'unité systemd en supprimant :

~~~
#Requires=var-run.mount
~~~

On peut ensuite :

~~~
# systemctl daemon-reload
# systemctl enable macromilter
# systemctl start macromilter
# systemctl status macromilter
~~~

Le démon écoute sur le port TCP/3690.

Pour l'activer, il faut modifier ` /etc/postfix/main.cf` :

~~~
smtpd_milters = inet:127.0.0.1:3690
non_smtpd_milters = inet:127.0.0.1:3690
#milter_default_action=accept
# Pour accélérer les signatures, pas d'attente pour contacter le milter
in_flow_delay = 0s
~~~

## Mise à jour

### Mise à jour des dépendances:

~~~
# pip install -U oletools pymilter configparser olefile
~~~

Dans le cas ou on obtient une erreur du style :

~~~
  Running setup.py bdist_wheel for oletools ... error
  Complete output from command /usr/bin/python -u -c "import setuptools, tokenize;__file__='/tmp/pip-build-QG_8yM/oletools/setup.py';f=getattr(tokenize, 'open', open)(__file__);code=f.read().replace('\r\n', '\n');f.close();exec(compile(code, __file__, 'exec'))" bdist_wheel -d /tmp/tmpzsWOtDpip-wheel- --python-tag cp27:
  usage: -c [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
     or: -c --help [cmd1 cmd2 ...]
     or: -c --help-commands
     or: -c cmd --help
  
  error: invalid command 'bdist_wheel'
  
  ----------------------------------------
  Failed building wheel for oletools
  Running setup.py clean for oletools
~~~

Cela signifie que pip a besoin de compilé au moins une partie du module mais que le module wheel n'est pas installé, cela peut être corrigé en faisant un `pip install wheel` (à priori cela ne devrait pas être nécessaire pour une installation des modules en revanche).

> /!\\ Il est possible que l'installation avec pip ne mette pas les bon droits au fichiers, dans ce cas un `chmod -R go+rX /usr/local/lib/python2.7/dist-packages` corrige le problème.

### Mise à jour de Macromilter lui-même

Cela ce fait à priori de la même manière que l'installation, en général, une mise à jour du fichier service systemd et du fichier de configuration ne devrait pas être nécessaire mais est préférable.
