---
categories: 
title: Howto RedShift
...

* Site officiel : <http://jonls.dk/redshift/>
* GitHub : <https://github.com/jonls/redshift>

Redshift est un outil qui permet de régler automatiquement les écrans pour réduire la fatigue visuelle en fonction du moment de la journée.


## Installation

~~~
apt install redshift-gtk
~~~


## Configuration

La configuration se fait dans `$HOME/.config/redshift.conf`.

On peut récupérer le fichier de configuration d'exemple :

~~~
wget -O "$HOME/.config/redshift.conf" https://github.com/jonls/redshift/blob/master/redshift.conf.sample
~~~


### Réglage des écrans

~~~
; Température des couleurs
temp-day=6000
temp-night=3500

; Luminosité
brightness-day=0.8
brightness-night=0.8

; Gamma (pour toutes les couleurs ou chaque canal de couleur individuellement)
gamma-day=0.8:0.7:0.8
gamma-night=0.6
~~~

### Heure de lever et du coucher du soleil

Si on ne change pas souvent d'endroit ou qu'on ne veut pas utiliser de services de géolocalisation, on peut configurer nos coordonnées à la main :

~~~
location-provider=manual
[manual]
; Marseille
lat=43.29
lon=5.39
~~~

Sinon, on peut utiliser `geoclue2` :

~~~
location-provider=geoclue2
~~~

Sinon, on peut aussi donner directement les heures de lever et du coucher du soleil (la transition se faisant progressivement dans la fourchette) :

~~~
dawn-time=6:00-7:45
dusk-time=18:35-20:15
~~~


