---
title: Howto XTerm
categories: x term xterm terminal
...

* Site officiel : <https://invisible-island.net/xterm/>
* Documentation : <https://manpages.debian.org/jump?q=xterm>

xterm est l'émulateur de terminal standard pour X.

## Installation

~~~
# apt install xterm
~~~

## Configuration

Sa configuration est, en général, dans le fichier `~/.Xresources`. Ce fichier est lu au démarrage d'une session X. Pour mettre à jour la configuration à chaud (sans redémarrer X) : `$ xrdb -merge ~/.Xresources`.

### Comportement de la touche `Alt`

La touche `Alt` n'a pas le même comportement que sur d'autres émulateurs de terminaux. Dans xterm, `Alt+<touche>` envoie un caractère spécial. Par exemple, `Alt+b` envoie « â » au lieu de `Échap+b` (qui permet de déplacer le curseur d'un mot en arrière dans [Bash](HowtoBash)). Pour que `Alt+<touche>` envoie `Échap+<touche>` :

~~~
XTerm.vt100.metaSendsEscape: true
~~~

### Copier-coller avec `Ctrl+c` et `Ctrl+v`

Par défaut, xterm ne permet d'utiliser que le presse-papier _primary_ : surligner pour copier et bouton 2 (clic du milieu) pour coller. Certaines applications comme [Firefox](TipsFirefox) permettent aussi de copier avec `Ctrl+c` et de coller avec `Ctrl+v` en utilisant un autre presse-papier nommé _clipboard_.

La configuration suivante permet à xterm de copier et coller dans le presse-papier _clipboard_ avec `Ctrl+Shift+c` et `Ctrl+Shift+v` :

~~~
XTerm.vt100.translations: #override\n\
    Ctrl Shift <Key>C: copy-selection(CLIPBOARD)\n\
    Ctrl Shift <Key>V: insert-selection(CLIPBOARD)
~~~

### Fonte

xterm gère deux types de fontes : les _bitmaps_ et les vectorielles.

Il y a 8 tailles de fontes configurable au total, de la plus petite à la plus grande : _Unreadable_, _Tiny_, _Small_, _Default_, _Medium_, _Large_, _Huge_ et _Enormous_. Voir la section [Utilisation/Taille du texte](#taille-du-texte) pour passer une fonte à l'autre.

Par exemple, si on souhaite utiliser la fonte [Terminus](https://packages.debian.org/search?exact=1&keywords=fonts-terminus) :

~~~
XTerm.vt100.font1: -misc-nil-medium-r-normal--2-20-75-75-c-10-misc-fontspecific
XTerm.vt100.font2: -xos4-terminus-medium-r-normal--12-120-72-72-c-60-iso10646-1
XTerm.vt100.font3: -xos4-terminus-medium-r-normal--16-160-72-72-c-80-iso10646-1
XTerm.vt100.font: -xos4-terminus-medium-r-normal--20-200-72-72-c-100-iso10646-1
XTerm.vt100.font4: -xos4-terminus-medium-r-normal--22-220-72-72-c-110-iso10646-1
XTerm.vt100.font5: -xos4-terminus-medium-r-normal--24-240-72-72-c-120-iso10646-1
XTerm.vt100.font6: -xos4-terminus-medium-r-normal--28-280-72-72-c-140-iso10646-1
XTerm.vt100.font7: -xos4-terminus-medium-r-normal--32-320-72-72-c-160-iso10646-1
~~~

> On peut obtenir le nom de la fonte avec la commande 'xfontsel`.

Pour utiliser une fonte vectorielle et définir sa taille :

~~~
XTerm.vt100.faceName: DejaVu Sans Mono
XTerm.vt100.faceSize: 12
~~~

> Par défaut, xterm utiliser la fonte vectorielle si la variable `XTerm.vt100.faceName` est définie. Si on souhaite définir une fonte vectorielle, mais utiliser la fonte _bitmap_ par défaut : `XTerm.vt100.renderFont: false`.

### Historique de défilement

xterm conserve 1024 lignes dans l'historique de défilement par défaut.

~~~
XTerm.vt100.saveLines: 10240
~~~

## Utilisation

### Défilement

Il est possible de faire défiler le texte avec la molette de la souris ou avec `Shift+PageBas` et `Shift+PageHaut`. Ces deux raccourcis claviers font défiler le texte demi-page par demi-page.

Avec la molette, le défilement se fait par cinq lignes. Avec `Ctrl` pressé, le défilement se fait par demi-page. Pratique pour défiler rapidement.

### Taille du texte

Il y a 8 tailles de fontes possibles, de la plus petite à la plus grande : _Unreadable_, _Tiny_, _Small_, _Default_, _Medium_, _Large_, _Huge_ et _Enormous_. Il y a deux façons de passer d'une taille à une autre.

1. Maintenir `Contrôle-clic droit` et relâcher le bouton de la souris sur la fonte souhaitée.
1. `Shift-plus` et `Shift-moins` du pavé numérique.
