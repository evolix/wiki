---
categories: sysadmin
title: Howto Rsyslog
...

* Documentation : <http://www.rsyslog.com/doc/v8-stable/>

[Rsyslog](http://www.rsyslog.com/) est le démon **Syslog** par défaut sous Debian. Le protocole [Syslog](https://fr.wikipedia.org/wiki/Syslog) permet de gérer la journalisation : les messages sont envoyés par diverses applications (messages kernel, user, daemon, mail, etc.) triés par _facility_ (LOG_KERN, LOG_CRON, LOG_DAEMON, LOG_MAIL, LOG_AUTH, etc.) et avec un niveau de _severity_ (LOG_EMERG, LOG_ALERT, LOG_ERR, LOG_WARNING, etc.) ; ils sont reçus par un démon qui les répartit dans différents fichiers situés dans le répertoire `/var/log/` ou par d'autres façons (serveur syslog distant, terminal, etc.)

## Installation

Le paquet **rsyslog** tourne normalement déjà :

~~~
# systemctl status rsyslog
● rsyslog.service - System Logging Service
   Loaded: loaded (/lib/systemd/system/rsyslog.service; enabled; vendor preset: enabled)
     Docs: man:rsyslogd(8)
           http://www.rsyslog.com/doc/
 Main PID: 2815 (rsyslogd)
    Tasks: 4
   Memory: 2.9M
      CPU: 1.604s
   CGroup: /system.slice/rsyslog.service
           └─2815 /usr/sbin/rsyslogd -n

# rsyslogd -v
rsyslogd 8.16.0, compiled with:
        PLATFORM:                               x86_64-pc-linux-gnu
        PLATFORM (lsb_release -d):
        FEATURE_REGEXP:                         Yes
        GSSAPI Kerberos 5 support:              Yes
        FEATURE_DEBUG (debug build, slow code): No
        32bit Atomic operations supported:      Yes
        64bit Atomic operations supported:      Yes
        memory allocator:                       system default
        Runtime Instrumentation (slow code):    No
        uuid support:                           Yes
        Number of Bits in RainerScript integers: 64
~~~

## Configuration

Le fichier de configuration principal est `/etc/rsyslog.conf` :

~~~
module(load="imuxsock")
module(load="imklog")
$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat
$FileOwner root
$FileGroup adm
$FileCreateMode 0640
$DirCreateMode 0755
$Umask 0022
$WorkDirectory /var/spool/rsyslog
$IncludeConfig /etc/rsyslog.d/*.conf

auth,authpriv.*         /var/log/auth.log
*.*;auth,authpriv.none      -/var/log/syslog
#cron.*             /var/log/cron.log
daemon.*            -/var/log/daemon.log
kern.*              -/var/log/kern.log
lpr.*               -/var/log/lpr.log
mail.*              -/var/log/mail.log
user.*              -/var/log/user.log

mail.info           -/var/log/mail.info
mail.warn           -/var/log/mail.warn
mail.err            /var/log/mail.err

*.=debug;\
    auth,authpriv.none;\
    news.none;mail.none -/var/log/debug
*.=info;*.=notice;*.=warn;\
    auth,authpriv.none;\
    cron,daemon.none;\
    mail,news.none      -/var/log/messages
*.emerg             :omusrmsg:*
~~~

> *Note* : Pour Debian 7 et 8, la syntaxe pour charger les modules était différente :
>
> ~~~
> $ModLoad imuxsock
> $ModLoad imklog
> ~~~

Les journaux principaux sont :

* auth.log : authentification système (login, su, getty)
* daemon.log : relatif aux daemons
* mail.* : relatifs aux mails
* kern.log : générés par le noyau
* user.log : générés par des programmes utilisateur
* debug : messages de debug
* messages : messages d'infos
* syslog : tous les messages (sauf exceptions définies)

Nous conseillons d'ajouter ces paramètres :

* activer cron.log
* supprimer mail.info, mail.warn et mail.err
* réduire les infos en double envoyées à /var/log/syslog
* créer des entrées pour les services standard

À la suite du fichier de configuration nous pouvons ajouter :

~~~
*.*;auth,authpriv.none;cron,mail,local4,local5,local7.none -/var/log/syslog
cron.*             /var/log/cron.log
#mail.info         -/var/log/mail.info
#mail.warn         -/var/log/mail.warn
#mail.err          /var/log/mail.err
local0.*           /var/log/postgresql.log
local1.*           /var/log/sympa.log
local4.*           -/var/log/openldap.log
local5.*           -/var/log/haproxy.log
local7.*           -/var/log/dhcp.log
~~~


## Configuration avancée

**Rsyslog** supporte une configuration plus avancée, notamment avec des notions d'input, filtre et output. 

### Input

<http://www.rsyslog.com/doc/v8-stable/configuration/modules/idx_input.html>

Exemple avec l'input [Text File](http://www.rsyslog.com/doc/v8-stable/configuration/modules/imfile.html) qui permet à Rsyslog d'avaler des logs à partir d'un fichier, et par exemple de les renvoyer vers un serveur Syslog distant (Rsyslog ou [Logstash](HowtoELK#logstash)) :

~~~
$InputFileName /home/log/foo.log
$InputFileTag foo:
$InputFileStateFile foo
$InputFileSeverity info
$InputFileFacility local0
$InputRunFileMonitor

local0.* @192.0.2.42
~~~

### Filtre

<http://www.rsyslog.com/doc/v8-stable/configuration/filters.html>

Voici un exemple de filtre sur _$programname_ (ce qui pratique car le protocole Syslog a un nombre limité de _facility_ personnalisables : local0, local1 ... jusqu'à local9) :

~~~
if $programname startswith 'haproxy' then /var/log/haproxy.log
~~~

### Ignorer certains messages

Créer un fichier de configuration dédié, par exemple `/etc/rsyslog.d/01-ignore-xyz.conf`, avec les permissions adaptées (644) et y ajouter :

Pour ignorer les logs émis par un programme, `bundle` dans cet exemple :

~~~
:programname, isequal, "bundle" ~
~~~

Le `~` ordonne à rsyslog de s'arrêter à ce point.

Pour ignorer une chaîne de caractères :

~~~
:msg, contains, "message_to_ignore" ~
~~~

Pour ignorer un message provenant de `pam_unix`, il vaut mieux configurer PAM en amont : [HowtoPAM](HowtoPAM).

Pour ignorer une chaîne de caractères dans un fichier de log spécifique :

~~~
:msg, contains, "message_to_ignore" ~
*.* /var/log/auth.log
~~~

Attention :

* Il y a un ordre dans les règles.
* Les règles mal définies peuvent passer inaperçues, testez bien si vos règles s'appliquent comme vous le souhaitez.


## Administration

### Rotation automatique des logs

On peut écrire des noms de fichier avec des variables comme la date par exemple.
Cela permet notamment de gérer une rotation automatique (sans compression).
En général on fait cela en définissant un template, que l'on utilise avec _?template_ :

~~~
$template DailyPerHostLogs,"/var/log/local2-%$YEAR%-%$MONTH%-%$DAY%.log"

local2.* ?DailyPerHostLogs
~~~

### Activation en réseau

Nous pouvons activer Rsyslog en écoute sur le réseau (port UDP/514) ainsi :

~~~
module(load="imudp")
input(type="imudp" port="514")
~~~

> *Note* Pour Debian 7 et 8, il fallait faire :
> 
> ~~~
> # provides UDP syslog reception
> $ModLoad imudp
> $UDPServerRun 514
> ~~~

Il est ainsi possible d'envoyer des logs à un serveur Rsyslog (ou [Logstash](HowtoELK#logstash) !) via le réseau en spécifiant :

~~~
*.* @192.0.2.42
local0.* @192.0.2.42
~~~

Si le serveur de logs distant écoute en TCP, il faut rajouter un @ pour indiquer qu'on veux établir la connexion en TCP, comme ceci :

~~~
*.* @@192.0.2.42
local0.* @@192.0.2.42
~~~

## FAQ

### Stopper rsyslog

~~~
# systemctl stop syslog.socket
~~~

### Mode debug

Pour lancer rsyslog en ligne de commande en mode debug :

~~~
# rsyslogd -d -n
~~~

### logger

Pour envoyer des informations à Syslog en ligne de commande on peut utiliser _logger_ :

~~~
$ logger test
$ logger -p local0.notice test
~~~

### format de configuration RainerScript

Rsyslog utilise le nouveau format _RainerScript_ pour sa configuration.
Pour rester avec le format de configuration « legacy » on utilise :

~~~
$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat
~~~

### Erreurs de connections à un serveur rsyslog

Les erreurs de connections suivantes peuvent indiquer un problème d'authentification d'un client syslog TCP TLS à un serveur rsyslog. Ça peut venir d'une absense de certificat présété par un client ou l'utilisation d'un certificat invalide par le client :

~~~
peer did not provide a certificate
not permitted to talk
~~~

