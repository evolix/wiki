**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# RAID LSI-SAS1068E

Cartes compatibles :

~~~
04:01.0 SCSI storage controller: LSI Logic / Symbios Logic 53c1030 PCI-X Fusion-MPT Dual Ultra320 SCSI (rev 08)
~~~

## Monitoring avec mpt-status

Le paquet mpt-status contient le binaire mpt-status qui permet de consulter l'état d'un contrôleur raid du type LSI-SAS1068E, mais aussi un daemon qui permet de vérifier le status toutes les x minutes et de prévenir par mail.

~~~
# aptitude install mpt-status
~~~

Charger le module noyau :

~~~
modprobe mptctl

~~~
Ajouter aussi cette ligne dans le fichier `/etc/rc.local`, de façon à le charger à chaque démarrage de la machine.
(Note de Romain : pas sûr que ce soit nécessaire.)

Et enfin créez le fichier `/etc/default/mpt-statusd` pour configurer le mail de destination, et le timing des check.

~~~
MAILTO=alice@bob.com  # Where to report problems
PERIOD=3600    # Seconds between each check    (default 10 minutes)
REMIND=3600   # Seconds between each reminder (default 2 hours)
RUN_DAEMON=yes
ID=0
~~~

