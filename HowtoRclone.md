---
categories: tools
title: Howto Rclone
...

* Site officiel / Documentation : <https://rclone.org/>

Rclone est un outil en ligne de commande de synchronisation de fichiers et dossiers compatible avec de nombreuses plateformes cloud (Amazon S3, Ceph, Ownloud....)

# Installation

~~~
# apt install rclone

$ rclone --version
2018/09/21 11:51:37 Config file "/home/foo/.rclone.conf" not found - using defaults
rclone v1.35-DEV
~~~

# Configuration

La configuration se situe dans `~/.rclone.conf` qui est un fichier au format INI. Celui-ci peut être géré via rclone directement avec `rclone config` qui est totalement interactif.

~~~
$ rclone config
2018/09/21 11:53:26 Config file "/home/foo/.rclone.conf" not found - using defaults
No remotes found - make a new one
n) New remote
s) Set configuration password
q) Quit config
n/s/q> 

[....]
~~~

Au final, on obtient une config toute belle :

~~~
$ cat `~/.rclone.conf`

[endpoint]
type = s3
provider = AWS
access_key_id = *****************
secret_access_key = **************************
region = eu-central-1
location_constraint = eu-central-1
~~~

Tous les détails pour la configuration de : 

* [S3](https://rclone.org/s3/)


# Utilisation

## Commandes basiques

Les termes entre crochets sont optionnels.

~~~
$ rclone lsd endpoint:[path]    # Lister les objets et leur taille en octet
$ rclone ls endpoint:[path]     # Lister RECURSIVEMENT les objets et leur taille en octet
$ rclone lsl endpoint:[path]    # Lister RECURSIVEMENT les objets, leur taille en octet et leur date de modification
$ rclone size endpoint:[path]   # Afficher le nombre d'objet et la taille totale
~~~

## Synchronisation 

Synchronisation à 1 sens (source vers destination)

~~~
$ rclone sync

Usage:
  rclone sync source:path dest:path [flags]
~~~


Exemple de flags pratiques :

* `-n`, `--dry-run` : Faire une synchronisation d'essai sans modifications de la destionation
* `-v`, `--verbose` : Rendre rclone un peu plus bavard
* `-P`, `--progress` : Affichage de l'état d'avancement de la synchronisation

* `--s3-acl=STRING` : (Spécifique S3) L'ACL des éléments à copier (Cf: Les [ACL AWS S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl)

Ainsi, pour synchroniser un dossier vers S3 : 

~~~
# Synchronisation de /home/foo vers le dossier "/foo/backups" du bucket S3 "mon_bucket"
$ rclone -v --s3-acl=private sync /home/foo/ S3:mon_bucket/foo/backups/
~~~
