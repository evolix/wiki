**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto Android

## Avoir un accès root

### HTC Hero

Il suffit d'installer UniversalAndroot.apk
Voir <http://asf-mobiles.com/2011/06/how-to-root-android-phone-using-universal-androot-in-5-minuts/>

### Google Nexus 5

1. Délocker le bootloader

Brancher son Nexus en USB à un ordinateur sous Debian :

~~~
# apt install android-tools-adb android-tools-fastboot
# adb devices
# adb reboot bootloader
# fastboot devices
# fastboot oem unlock
~~~

ATTENTION !! Cela efface toutes les données de votre Nexus !

2. Installer TWRP (image "recovery" alternative)

Télécharger la dernière version de TWRP via <https://dl.twrp.me/hammerhead/> puis :

~~~
# adb reboot bootloader

# fastboot flash recovery twrp-2.8.7.1-hammerhead.img
target reported max download size of 1073741824 bytes
sending 'recovery' (14694 KB)...
OKAY [  0.988s]
writing 'recovery'...
OKAY [  1.132s]
finished. total time: 2.120s
~~~

Déconnecter le câble USB puis :

- Reboot sur le bootloader
- Boot sur le recovery mode
- Swipe (à voir si absolument nécessaire)
- Reboot (cela peut être long, patience...)

Voir <https://twrp.me/devices/lgnexus5.html>

3. Installer SuperSU en mode recovery

Télécharger la dernière version stable deSuperSU via <https://download.chainfire.eu/supersu> puis passer en mode recovery (avec TWRP) :

~~~
# adb push UPDATE-SuperSU-v2.46.zip /sdcard/
3303 KB/s (4017098 bytes in 1.187s)

# adb reboot bootloader
~~~

Et via "Install" sur TWRP, sélectionner le fichier UPDATE-SuperSU-v2.46.zip et l'installer.

Puis reboot, l'application SuperSU est installée et vous permet de gérer l'accès root.

Voir <http://androiding.how/how-to-flash-supersu-using-twrp-recovery-and-root-any-android-device/>

## Configuration avec Free Mobile

<http://mobile.free.fr/faq-config-apn-android.html>

## Reset d'usine du téléphone

HTC Hero : Bouton "Home" et "Back", puis "On"

## Le Bootloader

HTC Hero : Bouton "Back", puis "On"

Google Nexus 5 : Bouton "Volume Down" + "On"

## Mode RECOVERY

HTC Hero : Bouton "Home", puis "On"

Le SYSTEM RECOVERY est géré avec une ROM spéciale... qui peut être flashée !

Pour le HTC HERO, la ROM la mieux semble être "RA-hero"

Pour Google Nexus 5, TWRP

<http://forum.xda-developers.com/showpost.php?p=4549551>
<http://github.com/packetlss>

Le SYSTEM RECOVERY permet notamment :

* d'avoir un accès root !
* de gérer des backups (cf ci-dessous)

## SAFE MODE

Avec le Google Nexus 5, il faut presser quelques secondes pour faire apparaître "Éteindre" sur l'écran.
Puis presser plusieurs secondes sur "Éteindre" ce qui fera apparaître la proposition de reboot en Safe Mode.

## NANDROID BACKUP

Dans le SYSTEM RECOVERY, on peut créer des backups.
Les backups sont stockés sur la SDCARD, dossier "nandroid".
On peut aussi restorer le dernier backup.
Pour un backup spécifique, il faut tricher avec les dates des backups :)

<http://wiki.cyanogenmod.com/index.php?title=Nandroid_backup>
<http://mdzlog.alcor.net/2010/05/29/extracting-files-from-a-nandroid-backup-using-unyaffs/>

## UPGRADE RECOVERY SYSTEM

Pour récupérer le SDK => <http://developer.android.com/sdk/>

~~~
# ./android-sdk-linux_86/tools/adb -d shell
/ # mount -a
mount: mounting /system/modules/modules.cramfs on /system/modules failed: No such file or directory
mount: mounting /system/xbin/xbin.cramfs on /system/xbin failed: No such file or directory
# ./android-sdk-linux_86/tools/adb  push recovery-RA-hero-v1.7.0.1.img /sdcard/recovery-RA-hero-v1.7.0.1.img
1591 KB/s (4067328 bytes in 2.496s)
# ./android-sdk-linux_86/tools/adb -d shell
/ # flash_image recovery /sdcard/recovery-RA-hero-v1.7.0.1.img
flashing recovery from /sdcard/recovery-RA-hero-v1.7.0.1.img
~~~

## RADIO ROM

Un téléphone contient une RADIO ROM qui est une sorte de liste des fréquences
radio possible. On peut donc mettre à jour cette radio ROM. Voir :
<http://www.villainrom.co.uk/forum/showthread.php?307-radio-roms-download-links>

On peut flasher via fastboot :

~~~
# fastboot flash radio radio-hammerhead-m8974a-2.0.50.2.26.img
~~~

## La ROM PRINCIPALE

Il existe de nombreuses ROMs, c'est dur de choisir, voir :
<http://theunlockr.com/downloads/android-downloads/android-roms>

Il faut :

* Vider tous les caches
* Reformater la SDCARD
* Placer la ROM (.zip) dans le répertoire principal de la SDCARD.
  par exemple VillainROM12.0.0-Full-signed.zip

~~~
# uname -a
Linux localhost 2.6.29-ninpo-freqtwk@titan-dirty-b7a296fa #39 PREEMPT Tue Jun 29 15:57:27 BST 2010 armv6l GNU/Linux
~~~

Voir :
<http://www.villainrom.co.uk/vBwiki/index.php?title=Installing_Custom_ROMs>
<http://wiki.smartphonefrance.info/hero-upgrade-modaco-rom.ashx>

## Dev Android

* Telecharger le SDK
* Installer "ant", "java-jdk" (javac)
* Créer l'application :

~~~
$ ./tools/android create project -t 1 -n test -p /tmp/test -a A9 -k com.example.myandroid
$ cd /tmp/test
~~~

Hack :

~~~
$ vim src/com/example/myandroid/A9.java
$ vim ./res/layout/main.xml
$ vim ./res/values/strings.xml
~~~

~~~
$ ant debug
$ ant release
~~~

~~~
$ upload bin/*.apk !
~~~

Codes :

<http://code.google.com/p/apps-for-android/>
<http://java.sun.com/docs/books/tutorial/java/TOC.html>
<http://developer.android.com/guide/appendix/faq/commontasks.html>


## Réinstaller de zéro

On peut réinstaller une factory image (ce qui va dans la partition /system).
Pour les Google Nexus, voir <https://developers.google.com/android/nexus/images>

Attention, il faut utiliser une machine avec plus de 2 Go de RAM libre, sous peine d'avoir
des "failed to allocate" ou "Processus arrêté" (OOM) ou "critical error: extent_create_backing: calloc: Cannot allocate memory"

~~~
# wget <https://dl.google.com/dl/android/aosp/hammerhead-lmy48m-factory-bf3c82fd.tgz>
# tar xvf hammerhead-lmy48m-factory-bf3c82fd.tgz
# cd hammerhead-lmy48m

# fastboot devices

# sh -x flash-all.sh

+fastboot flash bootloader bootloader-hammerhead-hhz12h.img
+fastboot reboot-bootloader
+sleep 5
+fastboot flash radio radio-hammerhead-m8974a-2.0.50.2.26.img
+fastboot reboot-bootloader
+sleep 5
+fastboot -w update image-hammerhead-lmy48m.zip
~~~