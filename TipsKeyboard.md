# Changer de disposition clavier (général)

# Personnalisation du keymap

[german umlauts](https://www.blunix.org/using-german-umlauts-on-us-layout-keyboards/)

[ISO latin1 table](http://cs.stanford.edu/people/miles/iso8859.html)

# Afficher les touches tapées

~~~
# apt install key-mon
$ key-mon --alt --larger
~~~

## TTY

~~~
# dumpkeys > key.kmap
~~~

~~~
# loadkeys key.kmap
~~~

## X11

### Xmodmap

Voir son keymap actuel :

~~~{.bash}
$ xmodmap -pk
~~~

Exporter son keymap actuel dans un format xmodmap :

~~~{.bash}
$ xmodmap -pke > ~/.Xmodmap
~~~

Un exemple du format xmodmap (dvorak keymap ici) :

~~~{.bash}
...
keycode  21 = bracketright braceright bracketright braceright guillemotright rightdoublequotemark
keycode  22 = NoSymbol
keycode  23 = Tab ISO_Left_Tab Tab ISO_Left_Tab
keycode  24 = BackSpace BackSpace BackSpace BackSpace Delete Delete
keycode  25 = apostrophe quotedbl apostrophe quotedbl equal plus
keycode  26 = period comma period comma Up Up
keycode  27 = p P p P oslash Oslash
keycode  28 = y Y y Y
keycode  29 = f F f F
keycode  30 = g G g G
keycode  31 = c C c C ccedilla Ccedilla
keycode  32 = r R r R ubreve Ubreve
...
~~~

Charger son fichier format xmodmap (à placer dans son ~/.xinitrc pour l'appliquer lors du lancement de la session X) :

~~~{.bash}
$ xmodmap ~/.Xmodmap
~~~

#### Connaître le keycode d'une touche

~~~{.bash}
$ showkeys
~~~

ou

~~~{.bash}
$ xev
~~~

Une fenêtre apparaîtra (fond blanc par défaut), où vous pourrez taper la touche (clavier ou autres périphériques) afin d'avoir en output du terminal le détail de ce dernier.

Exemple avec la touche **{Prior}** :

~~~
KeyRelease event, serial 36, synthetic NO, window 0x1200001,
    root 0xad, subw 0x0, time 1169588306, (2255,-3), root:(2256,553),
    state 0x0, keycode 112 (keysym 0xff55, Prior), same_screen YES,
    XLookupString gives 0 bytes: 
    XFilterEvent returns: False
~~~

Pour **xmodmap**, l'élément à regarder est le *keycode* (112 dans l'exemple) et le nom de cette dernière au niveau du *keysym* (Prior dans l'exemple).

### Transformer Caps_Lock en \<Ctrl\>

Très pratique pour tout WM au autres logiciel utilisant très fortement les raccourcis se basant sur **\<Ctrl\>**.

~~~{.bash}
$ setxkbmap -option ctrl:nocaps
~~~

L'appel de **setxkbmap** impliquera de nouveau à charger sa conf **Xmodmap** -> ajouter dans cette ordre là dans son *.xinitrc*.

### Changer de place les flèches directionnelles

Si les doigts de la mains sont positionnés dans le rang du milieu (qsdf-jklm : pour un azerty), on pourra apprécier avoir les flèches directionnelles à ce niveau là pour ne pas avoir besoin de retirer les doigts de la rangée.

L'idée peut-être d'ajouter les flèches avec la combinaison **\<AltGr\>**.

Pour l'exemple, on choisit (Azerty) :

~~~{.bash}
E -> Up
S -> Left
D -> Down
F -> Right
~~~

Modifier dans son *.Xmodmap* :

~~~{.bash}
keycode 26 = x x x x Up Up
keycode 39 = x x x x Left Left
keycode 40 = x x x x Down Down
keycode 41 = x x x x Right Right
keycode 42 = x x x x Prior Prior
keycode 43 = x x x x Next Next
~~~

Pour *42* et *43*, on peut mettre **{Prior}** et **{Next}** en combinaison **\<AltGr\>**. Ajouté aussi (facultativement) pour *26* et *40* voir *41* (car U dans une disposition *Dvorak*) pour déplacer les onglets, mais ne marchera pas pour la sélection (\<Shift\>), donc à éviter.

### Changer de place la touche \<Delete\>

> Pour la même raison que les flèches directionnelles

Pour l'exemple, utilisation de la touche A (Azerty) pour *Backspace* en usage normal et *Delete* combiné avec **\<AltGr\>** :

~~~{.bash}
keycode 24 = BackSpace BackSpace BackSpace BackSpace Delete Delete
~~~

# Se déplacer

Beaucoup de raccourcis se recoupent entre logiciels et sont accessible même si le moteur de l'interface est parfois complètement différent des uns des autres. Certains raccourcis sont indépendant du logiciel (comme se déplacer dans le corps d'un texte) mais d'autre sont à une volonté d'uniformisé certains raccourcis considéré comme acquis et standard.

 - texte / déplacements :

~~~{.bash}
{fleches_directionnelles} = "Se déplacer dans un champs texte"
<Ctrl> + {fleches_directionnelles} = "Se déplacer mot par mot dans un champs texte - blocs de lignes en bloc de lignes"
<Shift> + {fleches_directionnelles} = "Sélectionner du texte"
<Shift> + <Ctrl> + {fleches_directionneles} = "Sélectionner du texte mot par mot"
<Tab> = "Se positionner sur le champs/lien texte suivant"
<Shift> + <Tab> = "Se positionner su le champs/lien précédent"
<Ctrl> + j = "Valider <=> Entrée"
~~~

 - onglets :

~~~{.bash}
<Alt> + [0-9] = "Se positionner sur l'onglet numéro X"
<Ctrl> + {Prior}|<Shift>+<Tab> = "Se positionner sur l'onglet précédent"
<Ctrl> + {Next}|<Tab> = "Se positionner sur l'onglet suivant"
<Ctrl> + <Shift> + {Prior}|{Next} = "Déplacer l'onglet sur la place précédente ou suivante"
<Ctrl> + (<Shift> +) w = "Fermer l'onglet en cours"
~~~

# Copier/Coller

xsel

# Navigateurs web

 - navigation :

~~~{.bash}
<Ctrl> + u = "{Prior}"
<Ctrl> + d = "{Next}"
~~~

## Mode hint sur les navigateurs

# Shell

# S'entraîner sur sa nouvelle disposition clavier

~~~
# apt install gtypist
~~~