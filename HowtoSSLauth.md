---
title: Howto SSL authentification
categories: security
...

[SSL/TLS](HowtoSSL) est un protocole de sécurisation des échanges réseau.
Il est principalement connu pour le chiffrement de contenu entre client et serveur et l'authentificaton des dits serveurs.
Ce que l'on sait moins, c'est que SSL/TLS permet aussi l'authentification des clients et propose ainsi une alternatives aux mots de passe.

* <https://www.openssl.org/docs/>

## Installation

~~~
# apt install openssl

$ openssl version
OpenSSL 1.0.2h  3 May 2016
~~~

## Coté serveur

### Apache

~~~
SSLCACertificateFile /etc/ssl/certs/CA.pem
SSLVerifyClient optional
#SSLVerifyClient require
SSLOptions +FakeBasicAuth
~~~

### Nginx

/etc/nginx/sites-enabled/vhostname

~~~
ssl_client_certificate /etc/ssl/certs/CA.pem;
ssl_verify_client optional;
#ssl_verify_client on;
~~~

/etc/nginx/conf.d/ssl-client.conf

~~~
map $ssl_client_s_dn $ssl_client_s_cn
{
    default "";
    ~/CN=(?<CN>[^/]+) $CN;
}
~~~

Authentification via proxy :

~~~
proxy_set_header X-Authenticated-User $ssl_client_s_cn;
~~~

Authentification via fastcgi :

~~~
fastcgi_param REMOTE_USER $ssl_client_s_cn;
~~~

### Dovecot

/etc/dovecot/conf.d/10-ssl.conf

~~~
ssl = yes
ssl_ca = /etc/ssl/certs/CA.pem
ssl_cert_username_field = commonName
~~~

/etc/dovecot/conf.d/10-auth.conf

~~~
auth_ssl_require_client_cert = yes
auth_ssl_username_from_cert = yes
passdb {
        driver = passwd-file
        args = /etc/dovecot/passwd-file

        deny = no
        master = no
        pass = no
}
~~~

/etc/dovecot/passwd-file

~~~
jdoe:{plain}::::::nopassword
~~~

## Coté application web

### Gogs / Gitea

app.ini

~~~
[security]
REVERSE_PROXY_AUTHENTICATION_USER = X-Authenticated-User

[service]
ENABLE_REVERSE_PROXY_AUTHENTICATION = true
ENABLE_REVERSE_PROXY_AUTO_REGISTRATION = false
~~~

## Coté client

### Curl

~~~
curl --cert ./client.crt --key ./client.key -u "user:pass" "https://example.com"  
~~~