# Astuces IRC

## Commandes IRC

[Toutes les commandes IRC](https://en.wikipedia.org/wiki/List_of_Internet_Relay_Chat_commands)

Commandes au sein d'un canal :

~~~
/topic Nouveau topic
/nick <nouveau-pseudo>
~~~

Commandes « admin » au sein d'un canal :

~~~
/op <pseudo>
/deop <pseudo>
/voice <pseudo>
/devoice <pseudo>
/ban <pseudo>
/unban <pseudo>

# Permettre à tout le monde de changer le topic
/mode #test -t
~~~

Commandes sur un serveur :

~~~
/join #test
~~~

## Utilisation de NickServ et ChanServ

* [Documentation de NickServ](https://github.com/atheme/atheme/wiki/NickServ)
* [Documentation de ChanServ](https://github.com/atheme/atheme/wiki/ChanServ)

### NickServ

Enregistrer son pseudo (première chose à faire, à priori NickServ te demande de le faire à chaque connexion à un serveur l'utilisant):

~~~
/msg NickServ REGISTER <password> <email>
~~~

S'identifier avec son pseudo (enregistrer):

~~~
/msg NickServ IDENTIFY <password>
~~~

S'identifier pour un autre pseudo :

~~~
/msg NickServ IDENTIFY <pseudo> <password>
~~~

Aide:

~~~
/msg NickServ HELP [command]
~~~

### ChanServ

ChanServ permet d'enregister et géré des channels IRC.

**Être identifier avec NickServ est un pré-requis pour pouvoir utiliser ChanServ.**

**Toutes commandes permettant de faire survivre les droits d'un pseudo nécessitent que ce pseudo soit enregistrer avec NickServ.**

> Dans tous les cas <channel> inclut le dièse (`#`) au début du nom.

Enregistrer un channel (devient le `founder` du channel avec contrôle complet, nécessite d'être op sur le channel)

~~~
/msg ChanServ REGISTER <channel>
~~~

Ajouter quelqu'un comme successeur (prend contrôle du channel si le fondateur perds son enregistration NickServ pour absence longue):

~~~
/msg ChanServ ROLE <channel> ADD <pseudo> +successor
~~~

Fait en sorte que quelqu'un soit automatiquement mis en op sur le channel:

~~~
/msg ChanServ ROLE <channel> ADD <pseudo> +autoop
~~~

Fait en sorte que quelqu'un puisse se mettre en op/deop sur le channel:

~~~
/msg ChanServ ROLE <channel> ADD <pseudo> +op
~~~

Permet à quelqu'un de modifier le topic via ChanServ:

~~~
/msg ChanServ ROLE <channel> ADD <pseudo> +topic
~~~

Empêche le changement du topic sans passer par ChanServ:

~~~
/msg ChanServ SET <channel> TOPICLOCK ON
~~~

Permet la restauration du topic automatiquement après que le channel ai été vide:

~~~
/msg ChanServ SET <channel> KEEPTOPIC ON
~~~

Modifier le topic:

~~~
/msg ChanServ TOPIC <channel> <topic>
/msg ChanServ TOPICAPPEND <channel> <partie du topic à ajouter à la fin du topic>
/msg ChanServ TOPICPREPEND <channel> <partie du topic à ajouter au début du topic>
~~~

Aide:

~~~
/msg ChanServ HELP [command]
~~~

## Freenode

~~~
# Enregistrer son pseudo
/nickserv identify pseudo
# S'identifier
/msg nickserv identify PASSWORD
# Changer de passwd
/msg nickserv set password pass
# Virer qq'un qui a pris notre nick
/msg NickServ recover <nick> PASSWORD
/msg NickServ RELEASE <nick> PASSWORD
# Le ghoster (faire fuir le fantome <nick>)
/msg nickserv ghost <nick> PASSWORD
# Passer OP
/msg chanserv op #<chan> <pseudo>
# GESTION des acces OP a un channel :
/msg ChanServ ACCESS #test LIST
/msg ChanServ ACCESS #test ADD foo 5
/msg ChanServ ACCESS #test DEL foo
~~~

~~~
/chanserv set #test url http://irc.example.com/
~~~

## irssi

<https://irssi.org/documentation/>

~~~
/connect irc.example.com
~~~

avec mot de passe

~~~
/connect irc.example.com 6667 passwd nick
~~~

## xchat

Ajouter un serveur :

~~~
« + Ajouter » > Modifier le nom + [Entrée] > « Éditer… » > Éditer le serveur + [Entrée]
~~~

Fonctionnalités intéressantes :

* Information utilisateur : liste des pseudos dans « Liste des réseaux »
* Connexion automatique : cocher « Connexion automatique » dans « Éditer… » un serveur IRC
* Liste des canaux en _autojoin_ : dans « Liste de réseaux », éditer le serveur puis « Favorite channels » : `#test,#random`
* Journalisation : à activer dans « Paramètres » puis « Préférences »
* Liste des canaux existants : « Server » puis « List of channels »

## hexchat

F9 = faire disparaître/apparaître la barre de menu

## ircd

~~~
# apt install ircd-hybrid|ircd-ratbox|charybdis anope|atheme-services
~~~

### charybdis

https://www.stack.nl/~jilles/irc/charybdis-oper-guide/configlines.htm

operator "gcolpart" {
        user = "~gcolpart@*";
        user = "~reg@*";
        password = "etcnjl8juSU1E";
        flags = ~encrypted;

loadmodule "extensions/m_opme.so";


/oper
/quote opme #random

### anope

http://www.anope.org/

ircd.conf :

connect "services.int" {
        host = "127.0.0.1";
        send_password = "PASSWORD";
        accept_password = "PASSWORD";
        port = 6666
        hub_mask = "*";
        class = "server";
        flags = topicburst;
};      

service {
        name = "services.int";
};


/etc/default/anope :

START=yes

/etc/anope/services.conf :

uplink
{
        host = "127.0.0.1"
        port = 6666
        password = "PASSWORD"

serverinfo
{
        name = "services.int"

### atheme

zcat /usr/share/doc/atheme-services/examples/atheme.conf.example.gz  > /etc/atheme/atheme.conf

/etc/default/atheme-services :

ENABLED=1

/etc/atheme/atheme.conf :

uplink "hades.arpa"
{
        host = "127.0.0.1"
        port = 6666
        password = "PASSWORD"


/whois ChanServ

00:53 -!- ChanServ [ChanServ@services.int]
00:53 -!-  ircname  : Channel Services
00:53 -!-  server   : services.int [Atheme IRC Services]
00:53 -!-           : Network Service
00:53 -!- End of WHOIS

/msg NickServ REGISTER password youremail@example.com
/msg ChanServ REGISTER #random
/msg OperServ MODE #random +o foo
/msg OperServ MODE #random -t


/msg chanserv register #evolix
