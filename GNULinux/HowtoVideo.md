# Howto vidéo sous GNU/Linux

    # apt install ffmpeg mediainfo audacity handbrake

Infos sur une vidéo :

    $ mediainfo <vidéo>
    $ ffprobe <vidéo>

Convertir une vidéo .mp4 en .webm (basse qualité : crf de 5 et un bitrate de 1Mbps max) :

~~~
$ ffmpeg -i input.mp4 -c:v libvpx -qmin 0 -qmax 50 -crf 5 -b:v 1M -c:a libvorbis output.webm
~~~

pour une meilleure qualité pour convertir en .webm :

~~~
$ ffmpeg -i input.mp4 -c:v libvpx -qmin 0 -qmax 50 -crf 10 -b:v 3M -c:a libvorbis output.webm
~~~

Si la vidéo sature à 3Mbps (si elle est très bruitée par exemple), on peut augmenter le crf ou choisir un bitrate max plus important. Autre alternative, encoder en VP9 (très lent, mais compresse fortement)

Pour plus d'infos : <https://trac.ffmpeg.org/wiki/Encode/VP8>

Couper une vidéo, exemple en prenant les 23 premières secondes :

    $ ffmpeg -i input.mp4 -ss 0 -c copy -t 23 output.mp4

Extraire un son depuis la 10ème seconde jusqu'à 2h :

    $ ffmpeg -i input.wav -ss 00:00:10.0 -t 02:00:00.0 output.wav

Enregistrement d'un screencast (capture vidéo via x11grab et audio via pulse) en WEBM :

    $ ffmpeg -threads auto -f pulse -i <INPUT> -f x11grab -s 1024x768 -i :0.0+0,0 \
     -c:v libvpx -vpre 720p -vsync cfr -r 15 -c:a libvorbis -q:a 6 output.webm

Avec deux sources audios :

    $ ffmpeg -threads auto -f pulse -i <INPUT> -ac 1 -f pulse -i <OUPUT.monitor> -ac 2 \
     -f x11grab -s 1024x768 -i :0.0+0,0 -c:v libvpx -vpre 720p -vsync cfr -r 15 \
     -c:a libvorbis -q:a 6 -filter_complex amix=inputs=2 output.webm

On peut aussi utiliser *kazam* qui est plus convivial... mais il s'appuie sur gstreamer et a souvent des soucis de désynchro audio/vidéo !

    $ kazam

      SUPER-CTRL-R - Start Recording
      SUPER+CTRL+F - Stop Recording
      SUPER-CTRL-F - Finish Recording
      SUPER-CTRL-W - Show/Hide main window
      SUPER-CTRL-Q - Quit

Convertir un audio .wav en .mp3, exemples "classique" (pertes minimes) et compressé au maximum (et passé en mono) :

~~~
$ ffmpeg -i input.wav -codec:a libmp3lame -qscale:a 4 output.mp3
$ ffmpeg -i input.wav -codec:a libmp3lame -qscale:a 9 -ac 1 output.mp3
~~~

Pour un audio .m4a, cela devrait être auto-détecté, donc :

~~~
$ ffmpeg -i input.m4a -codec:a libmp3lame output.mp3
~~~

Convertir un audio .wav en .ogg :

~~~
$ ffmpeg -i input.wav -codec:a libvorbis output.ogg
~~~

Voir <https://trac.ffmpeg.org/wiki/Encode/MP3>

Extraire le son d'une vidéo MKV vers un MP3 mono basse qualité :

~~~
$ ffmpeg -i input.mkv -codec:a libmp3lame -ac 1 -ab 16k output.mp3
~~~

Convertir un .mkv (issu de OBS par exemple) en .webm, dans une assez bonne qualité :

~~~
$ ffmpeg -i input.mkv -c:v libvpx -qmin 0 -qmax 50 -crf 10 -b:v 2M -c:a libvorbis output.webm
~~~

Convertir un .flv (type de fichier OBS par défaut) en .webm :

~~~
$ ffmpeg -i filename.flv -vcodec libvpx -acodec libvorbis filename.webm
~~~
## Audacity

Travailler un fichier audio .wav pour avoir un fichier compressé au maximum et en mono :

* Sélectionner les zones à effacer (astuce : Ctrl+molette pour zoom/dézoom)
* Exporter en MP3 avec les paramètres suivants : Mode de débit binaire : Constant, Qualité : 16 kbps, Forcer à exporter en mono

