# Howto image sous GNU/Linux

Installer sxiv, une visionneuse d'image :

    # apt install sxiv shotwell 

## sxiv

Raccourcis utiles :

- < : pivote l'image
- n, p : image suivante, précédente

## Capture d'écran

    # apt install scrot

Les commandes peuvent se lancer depuis le terminal ou le lanceur d'application. Par défaut, les images se placent dans le répertoire courant, sous la forme « aaaa-mm-jj-000000_2560x1080_scrot.png ». 

Pour capturer tout l'écran :

    $ scrot

Pour capturer uniquement une partie de l'écran, (sélection manuelle) :

    $ scrot -s

Donner un nom à la capture d'écran :

    $ scrot -s nomDeLimage.png

Ajouter un délai de 2 secondes et afficher le chronomètre (si la commande est lancée depuis un terminal) ;

    $ scrot -d 2 -c

Plus d'infos : 

    $ man scrot


