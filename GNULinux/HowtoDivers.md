---
categories: divers
title: Howto Divers
...

Pour supprimer le "beep" système, désactiver temporairement/définitivement le module `pcspkr` :

~~~
# rmmod pcspkr
# echo "blacklist pcspkr" >> /etc/modprobe.d/blacklist.conf
~~~


