---
categories: java
title: Howto Java
...

* Documentation JAVA : <https://docs.oracle.com/javase/>
* Documentation OpenJDK : <http://openjdk.java.net/>

# Installation

Sous Debian 7 et 8, c'est la version 7 d'OpenJDK que l'on installe :

~~~
# apt install openjdk-7-jre-headless
~~~

Sous Debian 9, on installe la version 8 :

~~~
# apt install openjdk-8-jre-headless
~~~


## OpenJDK 8 sous Debian 8

On peut vouloir installer OpenJDK 8 sous Debian Jessie, on utilisera alors les backports :

~~~
# echo "deb http://archive.debian.org/debian jessie-backports main " >> /etc/apt/sources.list.d/backports.list
~~~

~~~
# apt install openjdk-8-jre-headless/jessie-backports
# update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
~~~


## Oracle Java 8 sous Debian 7 / 8 et 9

Le plus simple est de créer un package automatiquement en utilisant le tar.gz du site oracle.

1- Tout d'abord il faut ajouter le dépôt contrib dans */etc/apt/sources.list* :

~~~
deb http://mirror.evolix.org/debian stretch main contrib
~~~

2- Mettre à jour  les dépôts et installer le paquet *java-package*

~~~
apt update && apt install java-package
~~~

Sur Debian 7, il faut installer java-package depuis les backports, car il y a un bug avec la version de wheezy [ici](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=750092#15)

3- Il faut télécharger l'archive .tar.gz depuis le site d'[Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html), et utiliser la commande make-jpkg pour créer le paquet Debian :

Pour télécharger l'archive en acceptant les conditions d'Oracle, on peut utilisé ces options avec wget :

~~~
wget --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" 
~~~

<p style="text-align:center";>**<span style="color: 
#fb4141
">IMPORTANT:</span> A ne pas exécuter en root, mais avec son utilisateur.**</p>

~~~
$ make-jpkg jdk-8u51-linux-x64.tar.gz
~~~

Pour faire le build du paquet Debian, le TMPDIR par défaut est */tmp/*, il faut donc que la partition */tmp/* soit en exec ou s'il y a pas assez de place dans /tmp on peut spécifier un TMPDIR différent, par exemple dans son */home/* :

~~~
$ TMPDIR=/home/utilisateur/ make-jpkg jre-8u144-linux-x64.tar.gz
~~~

4- On peut ensuite installer le paquet Debian généré :

~~~
# dpkg -i oracle-java8-jdk_8u51_amd64.deb
~~~


# Sécurité

## Importer un certificat racine

Java stocke ces certificats dans un fichier, situé dans _/etc/ssl/certs/java/cacerts_ (sous Debian).

Pour lister les certificats :

~~~
$ keytool -list -v -keystore /etc/ssl/certs/java/cacerts
~~~

Pour importer un nouveau certificat, on peut utiliser la commande keytool :

~~~
$ keytool -import -v -file certificat.cer -alias <mykey> -keystore /etc/ssl/certs/java/cacerts
~~~

Il est ensuite demandé un mot de passe pour ouvrir la base de données des certificats. Par défaut, il s'agit de **changeit**.

Si vous ne précisez pas d'alias vous pourriez rencontrer l'erreur suivante :

~~~
keytool error: java.lang.Exception: Certificate not imported, alias <mykey> already exists
~~~

Basiquement, cela peut vouloir dire que le certificat a déjà été importé avec l'alias par défaut: mykey, il faudra donc trouver un autre nom pour importer le certificat.

Pour supprimer un certificat (parce qu'il est expiré par exemple) :

~~~
$ keytool -delete -keystore /etc/ssl/certs/java/cacerts -alias <mykey>
~~~


## Java Cryptography Extension (JCE)

<http://gcolpart.evolix.net/blog21/jce-non-limitees-sous-debian/>

Sous squeeze :

Télécharger le fichier ici : <http://download.oracle.com/otn-pub/java/jce_policy/6/jce_policy-6.zip>

~~~
# dpkg-divert --add --rename --divert /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/US_export_policy.jar.debianorig /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/US_export_policy.jar
# dpkg-divert --add --rename --divert /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/local_policy.jar.debianorig /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/local_policy.jar
- (Télécharger sur votre poste) sur le site d'oracle : <http://www.oracle.com/technetwork/java/javase/downloads/jce-6-download-429243.html> | scp sur le serveur
# cd /home/$USER
# unzip jce_policy-6.zip
# cp jce/US_export_policy.jar /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/
# cp jce/local_policy.jar /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/
# chmod 644 /usr/lib/jvm/java-6-sun-1.6.0.26/jre/lib/security/*.jar
~~~

Il faut aussi modifier `/etc/java-6-sun/security/java.security`, en mettant les lignes suivantes après `# List of providers and their preference orders (see above):`

~~~
security.provider.1=org.jasypt.hibernate.type.EncryptedStringType
security.provider.2=sun.security.rsa.SunRsaSign
security.provider.3=com.sun.net.ssl.internal.ssl.Provider
security.provider.4=com.sun.crypto.provider.SunJCE
security.provider.5=sun.security.jgss.SunProvider
security.provider.6=com.sun.security.sasl.Provider
security.provider.7=org.jcp.xml.dsig.internal.dom.XMLDSigRI
security.provider.8=sun.security.smartcardio.SunPCSC
security.provider.9=sun.security.provider.Sun
~~~

Sous Wheezy avec JRE Oracle (pas OpenJDK) :

Télécharger le fichier ici : <http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html>

~~~
# cd /home/$USER
# unzip UnlimitedJCEPolicyJDK7.zip
# cp UnlimitedJCEPolicy/US_export_policy.jar /usr/local/opt/jdk1.7.0_45/jre/lib/security/
# cp UnlimitedJCEPolicy/local_policy.jar /usr/local/opt/jdk1.7.0_45/jre/lib/security/
# chmod 644 /usr/local/opt/jdk1.7.0_45/jre/lib/security/*.jar
~~~

Il faut aussi modifier `/usr/local/opt/jdk1.7.0_45/jre/lib/security/java.security`, en mettant les lignes suivantes après `# List of providers and their preference orders (see above):`

~~~
security.provider.1=org.jasypt.hibernate.type.EncryptedStringType
security.provider.2=sun.security.rsa.SunRsaSign
security.provider.3=com.sun.net.ssl.internal.ssl.Provider
security.provider.4=com.sun.crypto.provider.SunJCE
security.provider.5=sun.security.jgss.SunProvider
security.provider.6=com.sun.security.sasl.Provider
security.provider.7=org.jcp.xml.dsig.internal.dom.XMLDSigRI
security.provider.8=sun.security.smartcardio.SunPCSC
security.provider.9=sun.security.provider.Sun
~~~

# Gestion de la mémoire

Le « tas » (heap), est l'emplacement mémoire où la machine virtuelle Java stocke les objets des applications.

Ce sont les options `-Xms` et `-Xmx` de la Machine Virtuelle Java (JVM) qui permettent de définir la taille du tas. Oracle recommande que les deux valeurs soient égales, afin de réduire le travail du ramasse-miettes ([source](https://docs.oracle.com/cd/E21764_01/web.1111/e13814/jvm_tuning.htm#PERFM161)).

Par exemple :

~~~
# java -Xms 512m -Xmx 512m
# java -Xms 1g -Xmx 1g
~~~

**Il est important de préciser ces options.** Sinon, la valeur par défaut est définie à 25% de la mémoire disponible, ou 50% s'il reste moins de 2 Go disponibles ([source](https://blog.openj9.org/2020/04/30/default-java-maximum-heap-size-is-changed-for-java-8/)).

Si la taille du tas n'est pas suffisante pour allouer un nouvel objet, la JVM lance alors l'exception `OutOfMemoryError`. Si les applications Java consomment toute la mémoire d'un serveur, c'est qu'il est sous-dimensionné par rapport au besoin.
 
Applications courantes :

* ElasticSearch : dans `/etc/elasticsearch/jvm.options`, un paramètre par ligne, `-Xms 1g` et `-Xmx 1g`.
* Tomcat : [HowtoTomcat#installation-et-configuration-dune-instance](), options `-Xms 1g -Xmx 1g` dans la variable `JAVA_OPTS`.

