Copier un mail dans deux maildir :

~~~
# --- Maildir 1 ---
:0c:  #carbon copy
* <Règles>
$HOME/Maildir/.Maildir1/

# --- Maildir 2 ---
:0
* <Règles>
$HOME/Maildir/.Maildir2/
~~~