---
title: Howto sudo
categories: system
...

* Documentation : <https://www.sudo.ws/readme.html>

Les systèmes de type Unix octroient à l'utilisateur root les pleins pouvoirs, tandis que les autres utilisateurs ont un droit d'accès minimal. [sudo](https://www.sudo.ws/) est un programme permettant d'accorder des droits à certains utilisateurs pour lancer des commandes en tant que _root_ ou un utilisateur différent. 


## Installation

Sous Debian :

~~~
# apt install sudo

$ sudo -V
Sudo version 1.8.10p3
Sudoers policy plugin version 1.8.10p3
Sudoers file grammar version 43
Sudoers I/O plugin version 1.8.10p3
~~~

Sous OpenBSD :

~~~
# pkg_add -z sudo-1

$ sudo -V
Sudo version 1.8.17p1
Sudoers policy plugin version 1.8.17p1
Sudoers file grammar version 45
Sudoers I/O plugin version 1.8.17p1
~~~


## Configuration

<https://manpages.debian.org/jessie/sudo/sudoers.5.en.html>

La configuration de _sudo_ se passe dans le fichier `/etc/sudoers`

Voici la configuration par défaut :

~~~
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
root    ALL=(ALL:ALL) ALL
%sudo   ALL=(ALL:ALL) ALL
~~~

On peut ainsi ajouter une autorisation complète à utilisateur en l'ajoutant au groupe _sudo_ :

~~~
# adduser foo sudo
~~~

Pour ajouter des autorisations spécifiques, on conseille d'utiliser la commande `visudo` qui édite `/etc/sudoers` et vérifie la syntaxe des autorisations afin d'éviter une erreur :

~~~
# visudo
~~~

Une autorisation a la forme suivante :

~~~
<user> <serveur> = (<runuser>:<rungroup>) <commande>
~~~

* `<user>` : utilisateur(s)/groupe(s) concernés par l'autorisation
* `<serveur>` : limite l'application de l'autorisation en fonction du hostname ou adresses IP locales (souvent ALL)
* `(<runuser>:<rungroup>)` : paramètre **facultatif** pour autoriser à une lancer une commande avec un utilisateur (`sudo -u`) et/ou un groupe (`sudo -g`)
* `<commande>` : commande(s) autorisée(s), éventuellement précédées par `NOPASSWD:` si l'on veut pas demander à l'utilisateur d'entrer son mot de passe

Voici des exemples d'autorisations :

~~~
jdoe ALL = ALL
jdoe ALL = NOPASSWD: ALL
jdoe ALL = /usr/sbin/tcpdump
jdoe ALL = (foo) /bin/kill
~~~

On peut également utiliser des alias :

~~~
Cmnd_Alias MAINT = /usr/share/scripts/evomaintenance.sh, /usr/share/scripts/listupgrade.sh, /usr/bin/apt, /bin/mount
User_Alias ADMINS = jdoe, foo
ADMINS ALL = NOPASSWD: MAINT
~~~

Il est important de noter que les autorisations sont lues du haut vers le bas et en cas de contradiction, c'est la dernière qui "a raison".
Dans l'exemple ci-dessous, la commande `evomaintenance.sh` ne demandera **pas de mot de passe** car elle vient **après** l'autorisation globale **avec mot de passe** :

~~~
Cmnd_Alias MAINT = /usr/share/scripts/evomaintenance.sh

%foo   ALL = (ALL:ALL) ALL
%foo   ALL = NOPASSWD: MAINT
~~~ 

Il est possible d'appliquer certains réglages seulement à certains utilisateurs, ou pour certaines commandes… Par exemple pour recevoir une notification mail pour chaque commande sudo de l'utilisateur "foo" sauf si c'est pour la commande "/usr/local/script.sh" :

~~~
Defaults:foo mail_always
Defaults!/usr/local/script.sh !mail_always
~~~ 

Plus d'info sur les "defaults" : <https://www.sudo.ws/docs/man/sudoers.man/#Defaults>

Il est possible de de désactiver entièrement les mails associés à une permission, en ajoutant le "tag" `NOMAIL`. Exemple :

~~~
%foo    ALL = NOMAIL: /usr/local/script.sh
~~~ 

Plus d'info sur les options : <https://www.sudo.ws/docs/man/sudoers.man/#SUDOERS_OPTIONS>

### sudoedit

Si l'on veut autoriser l'édition d'un fichier de manière sécurisée, il ne faut pas autoriser les éditeurs comme _vi_, _less_… car ils permettent l'exécution d'autres actions, ce qui peut revenir à donner une autorisation complète. Il faut plutôt utiliser la commande spéciale `sudoedit` (il ne faut pas spécifier son chemin) qui permet uniquement l'édition du fichier indiqué : toutes autres actions (exécution d'un shell, ouverture d'autres fichiers…) se feront avec les droits de l'utilisateur ayant exécuté sudo et non en tant que _root_.

Pour autoriser l'édition des fichiers d'un dossier (mais pas ses sous dossiers) sans se soucier des problèmes de chemins relatifs on peut spécifier cette autorisation comme ça : `sudoedit /etc/foo/*`

~~~
jdoe ALL = (ALL) sudoedit /etc/hosts
~~~


## Utilisation 

<https://manpages.debian.org/jessie/sudo/sudo.8.en.html>

On peut afficher les actions autorisées avec l'option `-l` :

~~~
$ sudo -l

Matching Defaults entries for foo on serveur:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin, umask=0077
User foo may run the following commands on serveur:
    (ALL : ALL) ALL
    (root) NOPASSWD: /usr/share/scripts/evomaintenance.sh, /usr/share/scripts/listupgrade.sh, /usr/bin/apt, /bin/mount
~~~


Pour passer en root, on recommande `sudo -i` plutôt que `sudo su` .

~~~
$ sudo -i
[sudo] password for foo: 
#
~~~


## FAQ

### À propos des erreurs de syntaxe

En cas d'erreur de syntaxe dans la configuration de Sudo, toutes les autorisations sont désactivées ! Cela peut donc être critique car vous pouvez par exemple perdre l'accès _root_. Afin d'éviter les erreurs de syntaxe, on conseille d'utiliser la commande `visudo` qui vérifie la syntaxe mais attention, cela vérifie uniquement la syntaxe du fichier invoqué : si l'on déporte une partie de la configuration dans un autre fichier `visudo -f /etc/sudoers.d/foo` ne vérifie que ce fichier... notamment si l'on introduit des alias en double (erreur `FOO already defined`), cela casse toute la configuration sans que l'on s'en aperçoive ! On conseille donc d'éviter d'éditer manuellement les fichiers dans `/etc/sudoers.d/` et de réserver ce répertoire uniquement aux configurations automatiques (de façon similaire à `/etc/cron.d/` pour _Cron_).

### Exécuter une commande depuis un certain utilisateur

~~~
$ sudo -u foo whoami
foo
~~~
