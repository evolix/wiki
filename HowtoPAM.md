PAM (Pluggable Authentication Modules) est un ensemble de librairies qui fournit aux programmes un moyen d'authentification unifié indépendant de la méthode d'authentification configurée sur le système : mots de passe locaux, LDAP...

La configuration de PAM pour les applications qui l'utilisent se situe dans `/etc/pam.d/*`. Chaque application a son fichier de configuration, par exemple `/etc/pam.d/sshd`.


# Syntaxe des fichiers de configuration

Documentation : <https://linux.die.net/man/5/pam.d>

La configuration est constituée de lignes qui ont la forme :

~~~
type control_flag module_path [module_arguments]
~~~

Commentaires :

* type : Module type/context/interface.
* control_flag : Indique à PAM le comportement à adopter en cas d'échec de l'authentification.
  * Syntaxe simple (historique) : un seul mot-clé.
  * Syntaxe avancée : liste de paires `value=action` entre crochets `[ ]` et séparée par des espaces.
* module_path : Chemin du module pam à utiliser (souvent un .so). Par défaut, PAM les cherche dans `/lib/security/` ou `/lib64/security/` selon l'architecture.
* module-arguments : Arguments passés au module.


# Utilisation concrète

## Vérifier si un programme est capable d'utiliser PAM

Par exemple pour `sshd` : 

~~~
$ ldd /usr/sbin/sshd | grep libpam.so
~~~

## Ne pas envoyer les messages de connexion SSH d'un utilisateur

Dans `/etc/pam.d/sshd`, juste avant la ligne :

~~~
# Standard Un*x session setup and teardown.
@include common-session
~~~

On ajoute :

~~~
session [success=ok default=ignore] pam_succeed_if.so quiet service in sshd user = my_user
~~~

Commentaires :

* `quiet` signale à PAM de ne pas logger les messages quand `sshd user = my_user`
