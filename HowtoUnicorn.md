**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

## Howto Unicorn

Unicorn est un serveur HTTP spécifiquement fait pour des application RoR. Il se place en règle générale derrière un serveur HTTP tel que Apache ou Nginx (notamment car il n'est pas optimisé pour gérer des requêtes de clients lents).

### Installation

Unicorn se présente comme une gem qui peut être installé classiquement :

~~~
# gem install unicorn
~~~

### Script d'init Debian

Voici un script d'init Debian permettant de lancer plusieurs instance de Unicorn dans /home :

~~~{.bash}
#!/bin/sh

### BEGIN INIT INFO
# Provides:          unicorn
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

set -e
. /lib/lsb/init-functions

. /etc/default/unicorn

case "$1" in
  start)    
        for instance in $INSTANCES; do
	    rails_env=`head -1 /home/$instance/www/current/config/database.yml |tr -d :`
	    PATH="/home/$instance/.rbenv/shims:/home/$instance/.rbenv/bin:/home/$instance/ruby-build/bin:$PATH"
	    WD="/home/$instance/www/current"
	    if [ -e /home/$instance/.rbenv/shims/unicorn_rails ]; then
	        log_daemon_msg "Starting unicorn server instance $instance..."
	        start-stop-daemon --start --quiet --name unicorn \
	          --pidfile $WD/tmp/pids/unicorn.pid \
	          --chdir $WD --chuid $instance \
	          --exec /home/$instance/.rbenv/shims/unicorn_rails -- \
	          -c $WD/config/unicorn.rb -D -E $rails_env
	        log_end_msg $?
	    fi
	done
	;;

  stop)
	for instance in $INSTANCES; do
	    WD="/home/$instance/www/current"
	    if [ -e /home/$instance/.rbenv/shims/unicorn_rails ]; then
	        log_daemon_msg "Stopping unicorn server instance $instance..."
	        start-stop-daemon --stop --quiet --name unicorn \
	          --pidfile $WD/tmp/pids/unicorn.pid
	        log_end_msg $?
	    fi
	done
	;;

  gracefull_stop)
	for instance in $INSTANCES; do
	    WD="/home/$instance/www/current"
	    if [ -e /home/$instance/.rbenv/shims/unicorn_rails ]; then
	        log_daemon_msg "Stopping gracefully unicorn server instance $instance..."
	        start-stop-daemon --stop --quiet --name unicorn --signal QUIT \
	          --pidfile $WD/tmp/pids/unicorn.pid
	        log_end_msg $?
	    fi
	done
	;;

  reload)
	for instance in $INSTANCES; do
	    WD="/home/$instance/www/current"
	    if [ -e /home/$instance/.rbenv/shims/unicorn_rails ]; then
	        log_daemon_msg "Reloading unicorn server instance $instance..."
	        kill -s USR2 `cat $WD/tmp/pids/unicorn.pid`
	        log_end_msg $?
	    fi
	done
	;;

  restart)
	for instance in $INSTANCES; do
	    rails_env=`head -1 /home/$instance/www/current/config/database.yml |tr -d :`
	    PATH="/home/$instance/.rbenv/shims:/home/$instance/.rbenv/bin:/home/$instance/ruby-build/bin:$PATH"
	    log_daemon_msg "Restarting unicorn server instance $instance..."
	    WD="/home/$instance/www/current"
	    if [ -e /home/$instance/.rbenv/shims/unicorn_rails ]; then
	        log_daemon_msg "Starting unicorn server instance $instance..."
	        start-stop-daemon --start --quiet --name unicorn \
	          --pidfile $WD/tmp/pids/unicorn.pid \
	          --chdir $WD --chuid $instance \
	          --exec bundle exec unicorn -- \
	          -c $WD/config/unicorn.rb -D -E $rails_env
	        start-stop-daemon --stop --quiet --name unicorn \
	          --pidfile $WD/tmp/pids/unicorn.pid
	        log_end_msg $?
	    fi
	done
	;;
*)
    echo "Usage: $0 {start|stop|gracefull_stop|restart|reload}"
    exit 1
    ;;
esac
~~~

Puis mettre la liste des instances à lancer dans le fichier _/etc/default/unicorn_.
