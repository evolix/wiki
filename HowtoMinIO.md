---
categories: sysadmin
title: Howto MinIO
...

- Documentation : <https://min.io/docs/minio/kubernetes/upstream/>
- Code : <https://github.com/minio/minio>
- Licence : [GNU AFFERO GENERAL PUBLIC LICENSE v3.0](https://github.com/minio/minio/blob/master/LICENSE)
- Language : Go

[MinIO](https://min.io/) est un système de stockage écrit en Go qui
fournit une API compatible avec [Amazon S3](/HowtoAmazon/Amazon-S3).
Comme Amazon S3, c’est de l’object storage : chaque fichier inclus est
un objet.
Les objets sont alors stockés dans un seau (*bucket*).

L’architecture est séparée en serveur, client (`mc`) et client SDK (qui
fournit l’API) disponible en divers langages (Java, Go, Node.js, Python,
.NET, Haskel).

## Requis

Les [requis](https://min.io/docs/minio/kubernetes/upstream/operations/checklists/hardware.html) du système selon la documentation officielle :

- Assez de processeurs pour le hachage ;
- Assez de mémoire vive pour le cache disque (au moins 128 Go par nœud pour haute performance) ;
- Au moins quatre nœuds pour le stockage d’objets ;
- Au moins huit disques par serveur pour les données ;

## Installation

Des paquets Debian sont
[disponibles au téléchargement](https://min.io/download#/linux)
(mais sans dépôt).
Comme c’est du Go, le paquet (serveur ou client) ne fournit qu’un
binaire, compatible a priori avec n’importe quelle version de Debian.
Cela permet de mettre en place rapidement le service dans un
environnement de développement.

* Serveur

~~~
# wget https://dl.min.io/server/minio/release/linux-amd64/minio_YYYYMMDDHHMMSS.0.0_amd64.deb
# apt install ./minio_YYYYMMDDHHMMSS.0.0_amd64.deb
~~~

* Client

~~~
# curl -o /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/mc
# chmod +x /usr/local/bin/mc
~~~

## Administration

> Cette documentation n’aborde que l’administration en ligne de commande.

> On suppose que ALIAS est un alias configuré avec un accès administrateur.

### Créer un utilisateur

~~~
$ mc admin user add ALIAS UTILISATEUR MOTDEPASSE
~~~

> Le mot de passe doit faire entre 8 et 40 caractères.
>
> ~~~
> $ apg -M CN -m 40 -n 1
> ~~~

## Utilisation

Un service systemd est fourni (`/etc/systemd/system/minio.service`),
mais pour un premier test, le serveur peut être lancé directement.

~~~
$ mkdir ~/minio
$ MINIO_ROOT_USER=admin MINIO_ROOT_PASSWORD=password minio server ~/minio --console-address ":9001"
~~~

Attention : le volume qui héberge le répertoire des données du serveur, ici `~/minio`, doit faire [au moins 1 Gio](https://github.com/minio/minio/issues/6795#issuecomment-437544010).

L’interface web est alors disponible en <http://localhost:9001>.

Le client en ligne de commande peut aussi être configuré.

~~~
$ mc alias set local http://localhost:9000 admin password
~~~

Il est maintenant possible de créer un espace de stockage (*bucket*) sur
le serveur, et d’y placer un fichier. Voir la section [#minio-client]()
pour plus d’information sur la commande `mc`.

~~~
$ mc mb local/bucket
Bucket created successfully `local/bucket`.
$ echo data >> /tmp/test
$ mc ls local 
[2022-11-08 16:54:07 CET]     0B bucket/
$ mc cp /tmp/test local/bucket
/tmp/test:       5 B / 5 B ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 78 B/s 0s
$ mc ls local/bucket
[2022-11-08 16:57:46 CET]     5B STANDARD test
~~~

## MinIO Client

Il s’agit un outil en ligne de commande pour interagir avec du stockage compatible Amazon S3. Le code est accessible sur [GitHub](https://github.com/minio/mc). Il y a une page dédiée à cet outil : [HowtoMinIO]().

On défini un _alias_, qui est un nom pour le stockage S3.

    $ mc alias set <alias> https://<hote> <access_key> <secret_key>

> On peut préciser un port :
>
>     $ mc alias set <alias> https://<hote>:<port> <access_key> <secret_key>

La configuration est enregistrée dans le fichier `~/.mc/config.json`.

Pour créer un _bucket_ :

    $ mc mb <alias>/<bucket>

Pour lister les objets dans un _bucket_ :

    $ mc ls <alias>/<bucket>

Pour copier un fichier :

    $ mc cp mon_fichier <alias>/<bucket>/

Pour supprimer un _bucket_ :

    $ mc rb <alias>/<bucket>

### Miroir

Pour faire une copie (synchro) des données dans `mon_alias` (serveur) vers le répertoires `/backup`.

    $ mc --json mirror --exclude '*/un_repertoire/*' --exclude '*/un_autre/repertoire/*' --overwrite --remove mon_alias/ /backup/

## FAQ

### Impossible d’ajouter un objet alors qu’il y a encore de la place

On a un petit volume d’un peu moins de 1 Gio pour essayer MinIO, mais impossible de copier un fichier de 15 Mio alors qu’il y a plus de 800 Mio de libres.

~~~
$ head -c 16M /dev/zero > zero.img
$ mc cp zero.img minio/mybucket/
mc: <ERROR> Failed to copy `/home/user/zero.img`. Storage backend has reached its minimum free drive threshold. Please delete a few objects to proceed.
~~~

Le volume qui héberge le répertoire des données du serveur est trop petit. Il doit faire au moins 1 Gio. Voir l’[_issue_ 6795 sur GitHub](https://github.com/minio/minio/issues/6795#issuecomment-437544010).
