---
categories: databases
title: Howto MySQL : mariabackup.
...

## Vue d'ensemble

Mariabackup est un outil qui permet de faire un backup physique d'une instance mysql / mariadb avec les moteurs InnoDB / Aria et MyISAM.
Pour InnoDB un backup "à chaud" est possible, qui a originellement été introduit dans Percona XtraBackup 2.3.8

MariaDB 10.1 a introduit des fonctionnalités exclusives à MariaDB telles que la compression des pages (InnoDB Page Compression) et le chiffrement des données (Data-at-Rest Encryption), ces fonctionnalités exclusives ne sont pas prise en charge par des autres solutions de backup, telles que Percona XtraBackup.

Mariabackup supporte toutes les fonctionnalités de Percona XtraBackup, et celles exclusives a MariaDB telles que :

* Sauvegarde et restauration des tables utilisant **Data-at-Rest Encryption**.
* Sauvegarde et restauration des tables utilisant **InnoDB Page Compression**.
* **mariabackup SST method** avec un cluster Galera.
* Sauvegarde et restauration des tables utilisant **MyRocks** comme moteur de stockage à partir de MariaDB 10.2.16 et MariaDB 10.3.8

## Différences avec Percona XtraBackup

* Percona XtraBackup copie ses fichiers de journalisation InnoDB dans le fichier xtrabackup_logfile, tandis que Mariabackup utilise le fichier ib_logfile0.
* Il n'y a pas de lien symbolique entre mariabackup et innobackupex, comme pour xtrabackup. À la place, mariabackup a l'option de ligne de commande --innobackupex pour activer les options compatibles avec innobackupex.
* Les options _--compact_ et _--rebuild_indexes_ de XtraBackup ne sont pas supportées.
* L'utilitaire _xbstream_ de XtraBackup a été renommé _mbstream._ Cependant, pour sélectionner ce format de sortie lors de la création d'une sauvegarde, l'option _--stream_ de Mariabackup attend toujours la valeur xbstream.
* Mariabackup ne supporte pas l'option _lockless binlog_ d'XtraBackup.

## Installer Mariabackup

L’exécutable mariabackup est inclus dans le paquet _mariadb-client_ sur Debian 9.

~~~
# apt install mariadb-client
~~~

Sur Debian 10 mariabackup est présent dans le paquet _mariadb-backup_ :

~~~
# apt install mariadb-backup
~~~

Un utilisateur spécifique a Mariabackup doit être créé, avec les droits RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT :

~~~
MariaDB > GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'mariabackup'@'localhost' IDENTIFIED BY 'foobarcorp';
~~~

Si on doit utilisé l'option --slave-info, il faut rajouter les droits `SUPER` et `SLAVE MONITOR` :

~~~
MariaDB > GRANT RELOAD, PROCESS, LOCK TABLES, SUPER, SLAVE MONITOR, BINLOG MONITOR ON *.* TO `mariabackup`@`localhost`IDENTIFIED BY 'foobarcorp';
~~~

Les informations du compte _mariabackup_ peuvent être spécifié dans un groupe d'option client dans le fichier _.my.cnf_ comme ceci :

~~~
[mariabackup]
user=mariabackup
password=foobarcorp
~~~

## Sauvegarde complète (Full Backup)

L'option --backup permet sauvegarder la base de données et l'option --target-dir pour lui indiquer où placer les fichiers de sauvegarde. 
Lors d'une sauvegarde complète, le répertoire cible doit être vide ou ne pas exister.

Pour créer un backup, exécuter la commande suivante :

~~~
# mariabackup --backup --target-dir=/home/mariabackup/ --user=mariabackup --password=foobarcorp
~~~

_Note : Les options --user et --password n'ont pas besoin d'être précisé si on les gères dans le .my.cnf_ 

La durée de la sauvegarde dépend de la taille des bases de données ou des tables que vous sauvegardez. Vous pouvez annuler la sauvegarde si nécessaire, car le processus de sauvegarde ne modifie pas la base de données.

### Sauvegarde compressé

On peux effectuer une sauvegarde complète et compressé en passant l'option _--stream=xbstream_ et en redirigeant la sortie vers un utilitaire de compression, comme gzip :

~~~
mariabackup --user=mariabackup --backup --stream=xbstream | gzip > backup.gz
~~~

Pour décompresser on utilisera la commande suivante :

~~~
gunzip -c backup.gz | mbstream -x
~~~

Cela décompresse l'archive dans le répertoire courant.

Il existe l'option --compress dans mariabackup, mais celle-ci va être déprécié.

## Restauration complète

### Préparation du backup

Les fichiers de données créés par Mariabackup dans le répertoire cible ne sont pas cohérents à un point dans le temps, étant donné que les fichiers de données sont copiés à différents moments de la sauvegarde.

Si vous essayez de restaurer à partir de ces fichiers, InnoDB remarque les incohérences et va se stopper pour protéger les bases de données contre la corruption.

Avant de pouvoir restaurer à partir d'une sauvegarde, vous devez d'abord la préparer pour rendre les fichiers de données cohérents. Vous pouvez le faire avec l'option _--prepare_ :

~~~
# mariabackup --prepare --target-dir=/home/mariabackup/
~~~

### Restauration du backup

Une fois la préparation de la sauvegarde terminé, vous pouvez la restaurer en utilisant les options _--copy-back_ ou _--move-back_.

L'option _--move-back_ déplace les fichiers de sauvegarde vers le datadir, ainsi les fichiers de la sauvegarde d'origine sont perdus.

Voici la provédure pour restaurer un backup :

* Tout d’abord, arrêtez le processus MariaDB Server.
* Ensuite, assurez-vous que le datadir soit vide.
* Ensuite, lancez Mariabackup avec l’une des options mentionnées ci-dessus:

~~~
# mariabackup --copy-back --target-dir=/home/mariabackup/
~~~
* Ensuite, vous devrez peut-être corriger les droits des fichiers :

~~~
# chown -R mysql:mysql /var/lib/mysql/
~~~

Lorsque Mariabackup restaure une base de données, il conserve les privilèges de fichier et de répertoire de la sauvegarde. 
Cependant, il écrit les fichiers sur le disque en tant qu'utilisateur et groupe restaurant la base de données.

En tant que tel, après la restauration d'une sauvegarde, vous devrez peut-être ajuster le propriétaire du répertoire de données pour qu'il corresponde à l'utilisateur et au groupe du serveur MariaDB, généralement mysql pour les deux.

## Sauvegarde incrémentielle avec Mariabackup

Lorsque vous utilisez Mariabackup, vous avez la possibilité d'effectuer une sauvegarde complète ou incrémentielle.
Les sauvegardes complètes créent une copie complète dans un répertoire vide tandis que les sauvegardes incrémentielles mettent à jour une sauvegarde précédente avec de nouvelles données.

Les pages InnoDB contiennent des numéros de séquence de journal, ou LSN. Chaque fois que vous modifiez une ligne sur une table InnoDB de la base de données, le moteur de stockage incrémente ce nombre. 

Lors de l'exécution d'une sauvegarde incrémentielle, Mariabackup vérifie la sauvegarde la plus récente du LSN par rapport aux LSN contenus dans la base de données.
Il met ensuite à jour tous les fichiers de sauvegarde en retard.

### Mise en place de la sauvegarde incrémentielle

Pour effectuer une sauvegarde incrémentielle, vous devez d’abord effectuer une sauvegarde complète, comme vu précédemment :

~~~
# mariabackup --backup --target-dir=/home/mariabackup/
~~~

Ceci sauvegarde toutes les bases de données dans le répertoire cible /home/mariabackup/
Si vous regardez dans ce répertoire dans le fichier _xtrabackup_checkpoints_, vous pouvez voir les données de LSN fournies par InnoDB :

~~~
# cat /home/mariabackup/xtrabackup_checkpoints

backup_type = full-prepared
from_lsn = 0
to_lsn = 6902092065
last_lsn = 6902092065
recover_binlog_info = 0
~~~

Une fois que vous avez créé une sauvegarde complète sur votre système, vous pouvez également sauvegarder les modifications incrémentielles aussi souvent que vous le souhaitez.

Pour effectuer une sauvegarde incrémentielle, vous devez exécuter Mariabackup avec l'option _--backup_ pour lui indiquer d'effectuer une sauvegarde et avec l'option _--target-dir_ pour lui indiquer où placer les modifications incrémentielles. 

Le répertoire cible doit être vide. Vous devez également l'exécuter avec l'option _--incremental-basedir_ pour lui indiquer le chemin d'accès à la sauvegarde complète effectuée ci-dessus :

~~~
mariabackup --backup --target-dir=/home/mariabackup/inc1/ --incremental-basedir=/home/mariabackup/
~~~

Cette commande crée une série de fichiers delta qui stockent les modifications incrémentielles dans _/home/mariabackup/inc1/_
Vous pouvez trouver un fichier _xtrabackup_checkpoints_ similaire dans ce répertoire, avec les valeurs LSN mises à jour :

~~~
# cat /home/mariabackup/inc1/xtrabackup_checkpoints 

backup_type = incremental
from_lsn = 6902092065
to_lsn = 6909766286
last_lsn = 6909766286
recover_binlog_info = 0
~~~

Pour effectuer des sauvegardes incrémentielles supplémentaires, vous pouvez ensuite utiliser le répertoire de base incrémentiel de la sauvegarde incrémentielle précédente comme répertoire cible pour la prochaine sauvegarde incrémentielle :

~~~
# mariabackup --backup --target-dir=/home/mariabackup/inc2/ --incremental-basedir=/home/mariabackup/inc1/
~~~

### Restauration d'un sauvegarde incrémentielle

En suivant les étapes ci-dessus, vous avez trois sauvegardes dans /home/mariabackup/ : la première est une sauvegarde complète, les autres sont incrémentées sur cette première sauvegarde.

Pour restaurer une sauvegarde dans la base de données, vous devez d’abord appliquer les sauvegardes incrémentielles à la sauvegarde complète de base.
Ceci est fait en utilisant l'option de commande _--prepare_ avec l'option _--apply-log-only_.

Premièrement, nous préparons le backup de base :

~~~
# mariabackup --prepare --apply-log-only --target-dir=/home/mariabackup/
~~~

Ensuite, appliquez les modifications incrémentielles à la sauvegarde complète de base :

~~~
# mariabackup --prepare --apply-log-only --target-dir=/home/mariabackup/ --incremental-dir=/home/mariabackup/inc1/
~~~

L'exécution de cette commande met la sauvegarde complète de base, c'est-à-dire _/home/mariabackup/_, en synchronisation avec les modifications contenues dans la première sauvegarde incrémentielle.
Répétez l'étape ci-dessus pour appliquer les modifications de la sauvegarde _/home/mariabackup/inc2/_ à la base :

~~~
# mariabackup --prepare --apply-log-only --target-dir=/home/mariabackup/ --incremental-dir=/home/mariabackup/inc2/
~~~

### Restauration du backup incrémentiel 

Nous pouvons désormais restaurer le backup de la même manière que cité plus haut :

* Arrêtez le processus MariaDB Server, assurez-vous que le datadir soit vide, lancez Mariabackup comme ceci :

~~~
# mariabackup --copy-back --target-dir=/home/mariabackup/
~~~

Assurez-vous que les droits de _/var/lib/mysql/_ soit correct.

## Mise en place d'une réplication esclave avec Mariabackup

Mariabackup facilite la configuration d'un esclave de réplication à l'aide d'une sauvegarde complète.

La première étape consiste simplement à prendre et à préparer une nouvelle sauvegarde complète d'un serveur de base de données dans la topologie de réplication.

1. Si le serveur de base de données source est le maître de réplication souhaité, il n'est pas nécessaire d'ajouter d'autres options lors de la sauvegarde complète.

~~~
# mariabackup --backup --target-dir=/home/mariabackup/
~~~

Si le serveur de base de données source est un esclave de réplication nous devrions ajouter l'option _--slave-info_ et éventuellement l'option _--safe-slave-backup_ :

~~~
# mariabackup --backup --slave-info --safe-slave-backup --target-dir=/home/mariabackup/
~~~

2. Et puis nous préparons la sauvegarde comme vous le feriez normalement :

~~~
# mariabackup --prepare --target-dir=/home/mariabackup/
~~~

### Copier le backup sur le nouveau serveur esclave

Une fois que le backup est terminé et préparé, nous pouvons le copier sur le nouveau serveur esclave :

~~~
# rsync -avP /home/mariabackup/ root@sql2:/home/mariabackup
~~~

### Restauration du backup sur le nouveau serveur esclave

Arrêter MySQL, supprimer le contenu du datadir puis restaurer la sauvegarde dans le datadir, comme vous le feriez normalement :

~~~
# mariabackup --copy-back --target-dir=/home/mariabackup/
~~~
_Note_ : il faut s'assurer que aucun processus mysql ne tourne sur le slave, et que le datadir soit vide_

Et ajuster les droits des fichiers, si nécessaire:

~~~
# chown -R mysql:mysql /var/lib/mysql/
~~~

### Configuration du nouveau serveur slave

Sur le master, il faut maintenant configurer notre serveur slave, comme nous ne faisons d'habitude, c'est à dire en créant l'utilisateur MySQL 'repl' et configuration du _server-id_ :

~~~
# mysql
MariaDB > CREATE USER 'repl'@'sql2' IDENTIFIED BY 'ReplPassWord';
MariaDB > GRANT REPLICATION SLAVE ON *.*  TO 'repl'@'sql2';
~~~

~~~
# vim /etc/mysql/mariadb.conf.d/zzz-evolinux-custom.cnf
server-id = 3
~~~

Attention aux autorisations réseaux ! Le serveur esclave doit pouvoir joindre le server maître sur le port 3306.

Démarrer le service MySQL sur le serveur esclave.

### Démarrer la réplication sur le nouveau serveur slave

À ce stade, nous devons obtenir les coordonnées de réplication du maître à partir du répertoire de sauvegarde d'origine.

Si nous prenons la sauvegarde sur le maître, les coordonnées seront dans le fichier _xtrabackup_binlog_info_.

Si nous prenons la sauvegarde sur un autre esclave et si nous fournissons l'option _--slave-info_, alors les coordonnées seront dans le fichier _xtrabackup_slave_info_.

Note : il est possible que les coordonnées GTID (0-1-2 par exemple) ne soient pas définies s'il n'y a pas d'activité sur le serveur source/maître. Ça ne posera pas de problème.

~~~
# cat /home/mariabackup/xtrabackup_binlog_info
[…]
mysql-bin.000002	327	0-1-2
[…]
~~~

Mariabackup vide les coordonnées de réplication sous deux formes: les coordonnées **GTID**, le fichier journal binaire et les coordonnées de position, comme celles que vous verriez normalement à partir de la sortie **SHOW MASTER STATUS**. 

Nous pouvons choisir le jeu de coordonnées à utiliser pour configurer la réplication.

Quelles que soient les coordonnées utilisées, nous devrons configurer la connexion principale à l'aide de **CHANGE MASTER TO**, puis démarrer les threads de réplication avec **START SLAVE** 

* Par GTID :
Si nous voulons utiliser des GTID, nous devons d'abord définir gtid_slave_pos sur les coordonnées GTID extraites de _xtrabackup_binlog_info_ dans le répertoire de sauvegarde, puis _MASTER_USE_GTID = slave_pos_ dans la commande _CHANGE MASTER TO_, comme ceci :

Note : si on a pas de coordonnées GTID, car il n'y a pas d'activité sur le serveur source/maître, il ne fait pas définir la variable `gtid_slave_pos`. Dans ce cas, il ne faut pas faire la commande `SET GLOBAL gtid_slave_pos`.

~~~
SET GLOBAL gtid_slave_pos = "0-1-2";
CHANGE MASTER TO 
   MASTER_HOST="sql1", 
   MASTER_PORT=3307, 
   MASTER_USER="repl",  
   MASTER_PASSWORD="ReplPassWord", 
   MASTER_USE_GTID=slave_pos;
START SLAVE;
~~~

* Par fichier et position :
Si nous voulons utiliser les binlogs et les coordonnées de position, nous définirions _MASTER_LOG_FILE_ et _MASTER_LOG_POS_ dans la commande _CHANGE MASTER TO_ avec les coordonnées du binlog extraite de _/home/mariabackup/xtrabackup_binlog_info_ ou de _xtrabackup_slave_info_ dans le répertoire de sauvegarde, selon que la la sauvegarde a été effectuée auprès du maître ou d'un esclave du maître :

~~~
CHANGE MASTER TO 
   MASTER_HOST="sql1", 
   MASTER_PORT=3307, 
   MASTER_USER="repl",  
   MASTER_PASSWORD="ReplPassWord", 
   MASTER_LOG_FILE='mysql-bin.000002',
   MASTER_LOG_POS=327;
START SLAVE;
~~~

## Faire un backup partiel avec mariabackup

Il est possible de faire un backup partiel avec mariabackup, soit en excluant des bases et / ou des tables.
Pour que le backup partiel soit fonctionnel il faut que l'option ` innodb_file_per_table` soit bien activée.

Voici les options possibles pour faire un backup partiel :

* `--databases` pour choisir quelles sont les bases à sauvegarder
* `--databases-exclude` pour choisir quelle sont les bases à exclure du mariabackup
* `--databases-file` pour vérifié les fichier dans le chemin de backup pour une base
* `--tables` pour choisir quelles sont les tables à sauvegarder
* `--tables-exclude` pour choisir quelles sont les tables à exclure du mariabackup
* `--tables-file` pour vérifié les fichier dans le chemin de backup pour une table

Toutes ces options supportent les wildcard, par exemple `--databases=test_*`

### Création d'un backup partiel

Pour ne sauvegarder que la base `evotest` on peut faire un mariabackup comme ceci :

~~~
# mariabackup --backup --target-dir=/home/mariabackup/ --databases='evotest' --user=mariabackup --password=foobar
~~~

Le temps de backup dépend de la taille de la base. On peut arrêter le mariabackup à tout moment, le processus de backup ne modifie pas la base de données.

*Attention* : Mariabackup ne peut actuellement pas sauvegarder un sous-ensemble de tables partitionnées, la sauvegarde d'une table partitionnée est une sélection de tout ou rien.

### Préparation du backup partiel

Comme avec les backups complets, à ce stade la sauvegarde faite par mariabackup n'est pas cohérente au moment où le backup a été fait (point-in-time consistent).
Si on essaye de restaurer à partir de ces fichiers, InnoDB remarque les incohérences, et arrête le processus de démarrage pour protéger la base de données de la corruption de données.

Pour les sauvegardes partielles, la sauvegarde n'est pas un répertoire de données MariaDB complètement fonctionnel, donc InnoDB générerait plus d'erreurs que pour les sauvegardes complètes. Ce point sera également très important à garder à l'esprit lors du processus de restauration.

Il faut donc préparer le backup, avant le processus de restauration, pour qu'il soit cohérent.
Les sauvegardes partielles reposent sur les tablespace InnoDB.

Pour que MariaDB importe des tablespaces, InnoDB recherche un fichier avec une extension .cfg, pour que Mariabackup crée ces fichiers, vous devez également ajouter l'option `--export` lors de l'étape de préparation.

~~~
# mariabackup --prepare --export --target-dir=/home/mariabackup/
~~~

Si cette commande de retourne pas d'erreur, le backup peut être prêt a être restauré.

### Restauration du backup partiel

Comme dit précédemment, un backup partiel repose sur les tablespace InnoDB, donc le processus de restauration et très différents que celui d'un backup complet.

Plutôt que d'utiliser les options `--copy-back` ou `--move-back`, chaque fichier individuel d'espace de table fichier par table InnoDB devra être importé manuellement dans le serveur cible.
Le processus utilisé pour importer le fichier dépendra de l'implication ou non du partitionnement.

#### Restauration de tables individuelle non partitionnées

Pour restaurer les tables individuelle non partitionnées à partir de la sauvegarde, recherchez les fichiers `.ibd` et `.cfg` de la table dans la sauvegarde, puis importez-les à l'aide du processus [Importation de tablespace pour les tables non partitionnées.](https://mariadb.com/kb/en/innodb-file-per-table-tablespaces/#importing-transportable-tablespaces-for-non-partitioned-tables)

#### Restauration de tables individuelle partitionnées

Pour restaurer les tables partitionnées, ont se basera également sur les fichier `.idb` et `.cfg` du backup partiel, mais avec l'aide du processus [Importation de tablespace pour les tables partitionnées](https://mariadb.com/kb/en/innodb-file-per-table-tablespaces/#importing-transportable-tablespaces-for-partitioned-tables)

## Fonctionnement de MariaBackup

Voici les différentes étapes du fonctionnement de MariaBackup lors d'un backup complet.

### Phase d'initialisation

* Connexion à l'instance mysqld, récupération des variables importantes comme datadir ,InnoDB pagesize, encryption keys, encryption plugin etc...
* Scan du répertoire de la base de donnée, du datadir, recherche des tablespaces InnoDB et chargement des tablespaces.
* Si _--lock-ddl-per-table_ est utilisé : Des verrous MDL (Metadata Locking) sont fait pour les tablespaces InnoDB. Cela permet de s’assurer qu’il n’ya pas de table ALTER, RENAME, TRUNCATE ou DROP sur les tables que nous voulons copier.
* Si _--lock-ddl-per-table_ n'est pas utilisé : Mariabackup devra connaître toutes les tables créées ou modifiées au cours de la sauvegarde. Pour plus d'information voir https://jira.mariadb.org/browse/MDEV-16791

### Manipulation des Redo Log.

Un thread dédié à mariabackup est démarré pour copier le journal de restauration InnoDB (ib_logfile *).

* Cela est nécessaire pour enregistrer toutes les modifications effectuées pendant l'exécution de la sauvegarde.
* Le journal est également utilisé pour détecter si des TRUNCATE ou ONLINE ALTER TABLE sont effectués.
* On présume que le processus de copie est capable de rattraper les informations du serveur, pendant la copie, si le Redo Log est assez gros.

### Phase de copie des Tablespaces InnoDB

* Copie des Tablespaces InnoDB sélectionné, fichier par fichier, dans les threads dédiés de Mariabackup, sans impliquer le serveur mysqld.
* Il s'agit d'une copie "sécurisé", elle vérifié la cohérence des données avec la somme de contrôle.
* Les fichiers ne sont pas cohérent dans un point dans le temps (point-in-time consistent), car les données peuvent être modifié pendant la copie. 
* L'idée est que la récupération d'InnoDB, notament avec les redo log, le rendrait cohérent a un moment précis dans le temps.

### Création d'un point de sauvegarde cohérent

* Exécution de FLUSH TABLE WITH READ LOCK. Ceci est effectué par défaut, si l'option --no-lock n'est pas passé.
  La raison pour laquelle FLUSH est nécessaire est de s'assurer que toutes les tables sont dans un état cohérent 
  exactement au même moment, indépendamment du moteur de stockage.

* Si _--lock-ddl-per-table_ est utilisé et qu'une requête utilisateur est en attente de MDL (Metadata Locking) la requête utilisateur sera supprimée pour résoudre un blocage. Notez que ce ne sont que des requêtes de type ALTER, DROP, TRUNCATE ou RENAME TABLE qui empêche le backup de se faire correctement.

### Dernière phase de copie

* Copie des fichiers .frm, MyISAM, Aria et d'autres moteurs de stockage.
* Copie des tables crées pendant la sauvegarde et renommage des fichiers modifiés pendant la sauvegarde
* Copie du reste du journal de restauration InnoDB, arrêt du thread de copie des Redo Log.
* Ecriture des métadonnées (position du binlog, par exemple)

### Déverrouillage

* Si FLUSH TABLE WITH READ LOCK est terminé, exécution de UNLOCK TABLES
* Si _--lock-ddl-per-table_ est terminé, exécution de COMMIT

Si FLUSH TABLE WITH READ LOCK n'est pas utilisé, seules les tables InnoDB sont cohérentes (pas les tables dans la base de donnée mysql, ni les binlogs)
Le point de sauvegarde dépends du contenu des Redo logs dans le backup lui-même.
