**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# OBS Orange Business Services FCE Flexible Computing Console

[[Image(FCE_accueil.png)]]

## Lexique

BVPN -> Business VPN[[BR]]

VDC -> Virtual Data Center

## Créer une VM

### Allouer plus de ressources

La première chose à faire est de se rendre sur "tableau de bord" à l'onglet ressources pour constater les ressources dipsonibles.

[[Image(FCE_ressources.png)]]

Dans la partie "serveurs" on peut voir le dimensionnement actuel de nos VMs.

[[Image(FCE_serveurs.png)]]

Pour modifier un paramètre (RAM, CPU) on se rend sur la VM concernée.

[[Image(FCE_ressources_VM.png)]]

Et on clique sur modifier.

[[Image(FCE_modifier.png)]]

Un redémarrage de la machine devra être effectué pour que les changements soient pris en compte

En ce qui concerne l'espace disque, on doit se rendre dans l'onglet "périphériques" et cliquer sur l'icone représentant un crayon (en face du disque virtuel que l'on souhaite modifier).

[[Image(FCE_machine_reseau.png)]]

[[Image(FCE_espacedisque.png)]]

## Le réseau

A leur création, chaque VM se voit attribuer deux adresses IP publiques mais non routées. Une interface est estampillée trafic et l'autre service.

[[Image(FCE_machine_reseau.png)]]

Afin que la machine soit joignable via internet, il faudra ajouter une règle de redirection d'une IP publique vers l'adresse (interface trafic) de la machine via la section "Sécurité et réseaux" / "Adresses IP publiques / NAT" (Voir capture plus bas) et ensuite ajouter des règles de filtrage via la matrice des flux.


Pour créer la règle dans la matrice des flux (le terme commercial pour "parefeu") il faut commencer par créer une règle dans "Le carnet d'adresses" : sous Firewall / Adresses, choisir la zone internet dans le menu déroulant, puis "Créer une nouvelle adresse" et l'appeler "ANY", choisir comme type "Network" et "0.0.0.0" comme adresse ainsi que comme netmask.


À partir de là on peut créer la règle à proprement parler : il faut retourner sur "Firewall" -> "Matrice des flux" -> "Créer une règle de sécurité" -> Choisir internet comme "Zone source" et  "Frontend Internet" comme zone de destination, ajouter "ANY" comme adresse source et l'adresse publique du serveur comme destination -> Et choisir "Permit" pour autoriser les communications depuis internet vers le serveur. 

Il faut répéter l'opération pour autoriser les communications depuis le serveur vers internet mais en choisissant "Frontend Internet" comme zone source et internet comme zone de destination.

## Gérer le pare-feu

## Bascule d'une adresse IP publique

Lors de la migration d'une VM existante vers une nouvelle VM FCE on peut vouloir basculer une IP publique vers une IP privée différente.

Voici la démarche à suivre :

Se rendre sur la console FCE dans la section "Sécurité et réseaux" puis dans "Adresses IP publiques / NAT". On peut voir ici les redirections actuellement en place.

[[Image(FCE_NAT.png)]]

Pour migrer une redirection IP publique vers IP privée, il faut cliquer sur la poubelle située en face de la redirection.

Une fois le redirection supprimée, l'adresse IP reste réservée pour un usage ultérieur pendant 3 jours. Vous avez donc la possibilité de la réattribuer.

[[Image(FCE_migration.png)]]

Si la poubelle est grisée cela signifie qu'une règle de firewall est liée à cette IP, il faudra donc préalablement supprimer cette règle pour pouvoir déplacer l'IP. Pour cela il faut se rendre dans la section "Firewall" sur la gauche, puis cliquer sur le bouton "Matrice des flux"

[[Image(FCE_firewall.png)]]

[[Image(FCE_matrice.png)]]

Vous pourrez donc ici supprimer la règle concernée

## Gérer le BVPN

Le BVPN, est une offre Orange qui permet de relier plusieurs sites distants et un VDC FCE.[[BR]]

Avant de configurer la partie FCE, il faut demander au client son plan d'adressage local de ses sites. Dans cet exemple on va partir sur 192.168.0.0/16 et configurer le nécessaire sur la console FCE.

### Mettre en place le routage

Dans sécurité & réseaux -> architecture sécurisée -> accès IPVPN. Créer une route du type 192.168.0.0/16.[[BR]]

Dans sécurité & réseaux -> architecture sécurisée -> firewall -> adresses -> zone intranet. Créer une adresse « network » du type 192.168.0.0/255.255.0.0.[[BR]]

Dans sécurité & réseaux -> architecture sécurisée -> firewall -> matrice des flux. Créer deux règles :[[BR]]

    Dans le sens intranet -> Front end Internet. L'adresse précédemment crée vers l'adresse du serveur.[[BR]]

    Dans le sens Front end Internet -> intranet. L'adresse du serveur vers l'adresse précédemment crée.

### Configuration sur la VM

Au niveau iptables il faut mettre une règle similaire à celle du pare-feu Orange :


~~~
/sbin/iptables -I INPUT -i eth1 -s 192.168.0.0/16 -j ACCEPT 
/sbin/iptables -I OUTPUT -o eth1 -d 192.168.0.0/16 -j ACCEPT
~~~

Pour s'assurer que le BVPN fonctionne, on peut faire un traceroute et constater que l'on fait plus que 1 saut. Exemple :


~~~
$ mtr -r 192.168.0.1                                               
HOST: hbb                         Loss%   Snt   Last   Avg  Best  Wrst StDev 
  1.|-- 213.56.106.92              0.0%    10    0.7   0.6   0.5   0.9   0.1 
  2.|-- 213.56.208.78              0.0%    10    1.0   1.2   0.7   3.3   0.8 
  3.|-- 213.56.208.17              0.0%    10    0.9   1.1   0.9   1.5   0.2 
  4.|-- 90.81.231.46               0.0%    10    1.4   3.5   0.9  18.2   5.4 
  5.|-- Xe-0-3-0.AVKJ1.Avignon.Ke  0.0%    10   15.4  15.5  15.2  16.0   0.3 
  6.|-- 172.16.165.14             90.0%    10   15.5  15.5  15.5  15.5   0.0 
  7.|-- ???                       100.0    10    0.0   0.0   0.0   0.0   0.0 
~~~

On peut aussi scanner un sous-réseau pour trouver rapidement des machines Windows par exemple :


~~~
# nmap 192.168.99.0/24 -p 22,80,443,445 --open  
                                                                
Starting Nmap 6.00 ( <http://nmap.org> ) at 2015-04-01 10:26 CEST 
Nmap scan report for 192.168.99.2                               
Host is up (0.0096s latency).                                   
Not shown: 2 filtered ports                                     
PORT    STATE SERVICE                                           
80/tcp  open  http                                              
445/tcp open  microsoft-ds     
[…]                                 
~~~
