---
title: Howto GeoIP
...

**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto GeoIP

GeoIP est un service de [maxmind](http://www.maxmind.com/app/ip-locate), ils proposent des base de données de localisation géographique des adresses IP, grauit ou payant, selon les besoins.

Il existe sous debian des paquets permettant de s'en servir _out-of-the-box_.
En installant les paquets `geoip-bin` et `geoip-database` on peut déjà se servir de GeopIP en terminal.

~~~
$ geoiplookup 85.31.205.43                                                           
GeoIP Country Edition: FR, France

~~~

En téléchargeant la base de données plus précise, on peut aller jusqu'à la ville, il faut pour cela télécharger la base [GeoLite City](http://www.maxmind.com/app/geolitecity) et l'extraire dans /usr/share/GeoIP/
Puis on peut intérogger la base de données ainsi :

~~~
$ geoiplookup -l -f /usr/share/GeoIP/GeoLiteCity.dat 209.85.227.103
GeoIP City Edition, Rev 1: US, CA, Mountain View, 94043, 37.419201, -122.057404, 807, 650
~~~

Notez que la version gratuite n'est pas précise en france :

~~~
geoiplookup -l -f /usr/share/GeoIP/GeoLiteCity.dat 85.31.205.43 
GeoIP City Edition, Rev 1: FR, (null), (null), (null), 46.000000, 2.000000, 0, 0
~~~

/!\ Ceci est en fait un bug de geoiplookup, en utilisant les scripts [PHP](http://geolite.maxmind.com/download/geoip/api/php/), on obtient bien une adresse précise.

À noter que la version gratuite n'est pas précise pour de nombreuses adresses IP de FAI, la version payante contient plus d'informations, exemple [ici](http://www.maxmind.com/app/lookup_city).

Il existe plusieurs APIs, vous pouvez donc faire des requêtes sur la base de données avec un module apache ou bien en PHP par exemple. Le site de maxmind liste toutes les [APIs disponibles](http://www.maxmind.com/app/api).
