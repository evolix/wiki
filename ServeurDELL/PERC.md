---
title: RAID Hardware sur serveur DELL avec contrôleur PERC
---

Documentation : <http://hwraid.le-vert.net/wiki/LSIMegaRAIDSAS>

## Infos générales

MegaCli est un outil propriétaire mal documenté mais qui s’avère capable de faire de très nombreuses opérations sur des cartes RAID Dell PERC.

On peut avoir l’ensemble des options avec la commande `megacli -help`.

Quelques termes et acronymes ou abréviations courantes :

* LD = logicial device
* PD = physical device
* Cfg = Configuration
* Prop = propriété
* aX (utilisé souvent comme `-a0`, `-aAll`…) désigne un adaptateur (une carte RAID)
* LX (utilisé souvent comme `-L0`, `-LAll`…) désigne un volume logique


## Remplacement d'un disque HS

Vous avez un disque HS, par exemple le disque 0, qui clignote.
Vous constatez ainsi le `Firmware state: Failed` :

~~~
# megacli -PDList -a0

Adapter #0

Enclosure Device ID: 32
Slot Number: 0
Device Id: 0
Sequence Number: 2
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 70007MB [0x88bb93a Sectors]
Non Coerced Size: 69495MB [0x87bb93a Sectors]
Coerced Size: 69376MB [0x8780000 Sectors]
Firmware state: Failed
SAS Address(0): 0x5000c5000b0b1c51
SAS Address(1): 0x0
Connected Port Number: 0(path0) 

Enclosure Device ID: 32
Slot Number: 1
Device Id: 1
Sequence Number: 2
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 70007MB [0x88bb93a Sectors]
Non Coerced Size: 69495MB [0x87bb93a Sectors]
Coerced Size: 69376MB [0x8780000 Sectors]
Firmware state: Online
SAS Address(0): 0x5000c5000b0b4355
SAS Address(1): 0x0
Connected Port Number: 1(path0) 
~~~

Vous le remplacez à chaud… et vous constatez qu'il ne se reconstruit pas tout seul.
Pas de panique. Notez déjà qu'il est possible que le nouveau disque apparaisse deux fois, une fois en `Firmware state: Unconfigured(bad)` et une autre en `Firmware state: Unconfigured(good)` :

~~~
Enclosure Device ID: 32
Slot Number: 0
Device Id: 0
Sequence Number: 2
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 0MB [0x0 Sectors]
Non Coerced Size: 9007199254740480MB [0xfffffffffff00000 Sectors]
Coerced Size: 0MB [0x0 Sectors]
Firmware state: Unconfigured(bad)
SAS Address(0): 0x5000c5002c5d18b5
SAS Address(1): 0x0
Connected Port Number: 0(path0) 

Enclosure Device ID: N/A
Slot Number: 33
Device Id: 33
Sequence Number: 1
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 70007MB [0x88bb93a Sectors]
Non Coerced Size: 69495MB [0x87bb93a Sectors]
Coerced Size: 69376MB [0x8780000 Sectors]
Firmware state: Unconfigured(good)
SAS Address(0): 0x5000c5002c5d18b5
SAS Address(1): 0x0
Connected Port Number: 0(path0) 
~~~

Après un petit reboot, seul `Firmware state: Unconfigured(good)` subsiste. Vous avez ainsi :

~~~
# megacli -PDList -a0

Adapter #0

Enclosure Device ID: 32
Slot Number: 0
Device Id: 0
Sequence Number: 1
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 70007MB [0x88bb93a Sectors]
Non Coerced Size: 69495MB [0x87bb93a Sectors]
Coerced Size: 69376MB [0x8780000 Sectors]
Firmware state: Unconfigured(good)
SAS Address(0): 0x5000c5002c5d18b5
SAS Address(1): 0x0
Connected Port Number: 0(path0) 

Enclosure Device ID: 32
Slot Number: 1
Device Id: 1
Sequence Number: 2
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 70007MB [0x88bb93a Sectors]
Non Coerced Size: 69495MB [0x87bb93a Sectors]
Coerced Size: 69376MB [0x8780000 Sectors]
Firmware state: Online
SAS Address(0): 0x5000c5000b0b4355
SAS Address(1): 0x0
Connected Port Number: 1(path0) 
~~~

On s'intéresse ensuite à notre volume RAID1 dans ce cas :

~~~
# megacli -CfgDsply -a0

==============================================================================
Adapter: 0
Product Name: PERC 6/i Integrated
Memory: 256MB
BBU: Present
==============================================================================
Number of DISK GROUPS: 1

DISK GROUPS: 0
Number of Spans: 1
SPAN: 0
Span Reference: 0x00
Number of PDs: 2
Number of VDs: 1
Number of dedicated Hotspares: 0
Virtual Disk Information:
Virtual Disk: 0 (target id: 0)
Name:
RAID Level: Primary-1, Secondary-0, RAID Level Qualifier-0
Size:69376MB
State: Degraded
Stripe Size: 64kB
Number Of Drives:2
Span Depth:1
Default Cache Policy: WriteBack, ReadAheadNone, Direct, No Write Cache if Bad BBU
Current Cache Policy: WriteBack, ReadAheadNone, Direct, No Write Cache if Bad BBU
Access Policy: Read/Write
Disk Cache Policy: Disk's Default
Physical Disk Information:
Physical Disk: 0
Physical Disk: 1
Enclosure Device ID: 32
Slot Number: 1
Device Id: 1
Sequence Number: 2
Media Error Count: 0
Other Error Count: 0
Predictive Failure Count: 0
Last Predictive Failure Event Seq Number: 0
Raw Size: 70007MB [0x88bb93a Sectors]
Non Coerced Size: 69495MB [0x87bb93a Sectors]
Coerced Size: 69376MB [0x8780000 Sectors]
Firmware state: Online
SAS Address(0): 0x5000c5000b0b4355
SAS Address(1): 0x0
Connected Port Number: 1(path0) 
~~~

On veut donc remplacer `Physical Disk: 0` (row = 0) dans le `target id: 0` (array = 0) par notre nouveau disque identifié comme étant `[32:0]` ce qui donne :

~~~
# megacli -PdReplaceMissing -PhysDrv [32:0] -array0 -row0 -a0                                  
Adapter: 0: Missing PD at Array 0, Row 0 is replaced.
~~~

Il devrait donc apparaître en `Firmware state: Offline`.
On lance enfin la reconstruction :

~~~
# megacli -PDRbld -Start -PhysDrv[32:0] -a0
Started rebuild progress on device(Encl-32 Slot-0)
~~~

Il passe donc en `Firmware state: Rebuild`.
On peut suivre sa reconstruction via :

~~~
# megacli -PDRbld -ShowProg -PhysDrv [32:0] -aALL                                  
Rebuild Progress on Device at Enclosure 32, Slot 0 Completed 28% in 3 Minutes.
~~~

Une fois terminé, il apparaît en `Firmware state: Online` et les opérations sont terminées.

**!!! ATTENTION !!! ATTENTION !!! ATTENTION !!!**

L'erreur fatale à ne pas commettre lorsque vous constatez un disque en état `Firmware state: Failed` est de le forcer à revenir `Online`.
En effet, cela force à repasser en mode RAID sans reconstruction et vous pouvez dire adieu à votre système de fichiers. NE FAITES DONC *PAS* :

~~~
# megacli_ATTENTION -PDOnline  -PhysDrv [32:0] -a0
EnclId-32 SlotId-0 state changed to OnLine.
~~~

Identifier un disque physiquement (faire clinoter la LED) :

~~~
megacli -PdLocate start -physdrv[<Enclosure Device ID>:<Slot Number>] -a0
~~~

## Remplacement à chaud d’un disque fonctionnel

Passer un disque en état `offline` :

~~~
megacli -PdOffline -physdrv [<Enclosure Device ID>:<Slot Number>] -a0
~~~

Passer un disque en état `missing` :

~~~
megacli -PdMarkMissing -physdrv [<Enclosure Device ID>:<Slot Number>] -a0
~~~

Préparer un disque pour le retirer :

~~~
megacli -PdPrpRmv -physdrv [<Enclosure Device ID>:<Slot Number>] -a0
~~~

Il est maintenant possible de remplacer le disque, le RAID commence à se reconstruire directement.

## Extension à chaud d’un disque virtuel

**Attention**, procédure expérimentale validée avec une carte H730, toujours s’assurer d’avoir des sauvegardes à jour avant de faire des opérations sur disque.

Après changement des disques physiques par des disques de plus grande capacité, il est possible d’augmenter la taille du disque virtuel (avec `N` le pourcentage d’augmentation, par exemple 100 — ou 99 pour une petite marge de sécurité — pour des disques de taille double).

~~~
megacli -LdExpansion -pN -Lall -aALL
~~~

Forcer le système d’exploitation à voir le changement de taille du disque.

~~~
echo 1 > /sys/block/sda/device/rescan
~~~

Et voilà, le disque accessible est plus grand, il est possible d’étendre ou ajouter des partitions.

## Ajout d'un disque dans un volume RAID5

Avoir un système avec la commande `omconfig`, par exemple, booter sur un Live-CD DELL OMSA (rappel, par défaut: login=root password=calvin) téléchargeable sur <http://linux.dell.com/files/openmanage-contributions/>

Pour connaître l'état des disques/volumes on utilisera `omreport` :

~~~
# omreport storage  vdisk controller=0 
# omreport storage pdisk controller=0
~~~

Et voilà un exemple qui ajoute un disque (ici `1:0:5`) sur un volume existant.

~~~
# omconfig storage vdisk action=reconfigure controller=0 vdisk=0 raid=r5 pdisk=0:0:0,0:0:1,0:0:2,0:0:3,1:0:5
Command successful!
~~~

Suite à cela, une loooongue phase de reconstruction du volume RAID.

## Performances

Si les performances lors d'un benchmark avec le cache FS désactivé sont ridicules (voir HowtoBenchmarks), il est possible que le cache du controlleur ne soit pas activé.
Dans ce cas, si le controlleur dispose d'une mémoire cache ET d'une batterie, on peut l'activer :

~~~
# megacli -AdpBbuCmd -GetBbuStatus -a0 | grep -e '^isSOHGood' -e '^Charger Status' -e '^Remaining Capacity'
Charger Status: Complete
Remaining Capacity: 1640 mAh
isSOHGood: Yes
~~~

La batterie est bonne, on peut l'activer :

~~~
# megacli -LDSetProp -Cached -LAll -aAll

Set Cache Policy to Cached on Adapter 0, VD 0 (target id: 0) success
Set Cache Policy to Cached on Adapter 0, VD 1 (target id: 1) success

Exit Code: 0x00

# megacli -LDSetProp EnDskCache -LAll -aAll

Set Disk Cache Policy to Enabled on Adapter 0, VD 0 (target id: 0) success
Set Disk Cache Policy to Enabled on Adapter 0, VD 1 (target id: 1) success

Exit Code: 0x00

# megacli -LDSetProp WB -LALL -aALL

Set Write Policy to WriteBack on Adapter 0, VD 0 (target id: 0) success
Set Write Policy to WriteBack on Adapter 0, VD 1 (target id: 1) success

Exit Code: 0x00
~~~

Et dans le cas ou la batterie subisse une defaillance, on demande à ce que le cache soit désactivé :

~~~
# megacli -LDSetProp NoCachedBadBBU -LALL -aALL

Set No Write Cache if bad BBU on Adapter 0, VD 0 (target id: 0) success
Set No Write Cache if bad BBU on Adapter 0, VD 1 (target id: 1) success

Exit Code: 0x00
~~~

Les débits en lecture/écriture devraient faire un bond ! 

## Ajout d'un nouveau volume RAID1

Exemple avec l'ajout d'un volume RAID1 sur 2 nouveaux disques,pour une carte RAID où 2 ensembles RAID sont déjà présents :

~~~
# megaclisas-status 
-- Controller information --
-- ID | H/W Model          | RAM    | Temp | BBU    | Firmware     
c0    | PERC H730P Adapter | 2048MB | 54C  | Good   | FW: 25.5.7.0005 

-- Array information --
-- ID | Type   |    Size |  Strpsz | Flags | DskCache |   Status |  OS Path | CacheCade |InProgress   
c0u0  | RAID-1 |    223G |   64 KB | RA,WB |  Enabled |  Optimal | /dev/sda | None      |None         
c0u1  | RAID-1 |    893G |   64 KB | RA,WB |  Enabled |  Optimal | /dev/sdb | None      |None         

-- Disk information --
-- ID   | Type | Drive Model                                | Size     | Status          | Speed    | Temp | Slot ID  | LSI ID  
c0u0p0  | SSD  | PHYG0240000L240AGN SSDSC2KG240G8R XCV1DL69 | 223.0 GB | Online, Spun Up | 6.0Gb/s  | 28C  | [32:0]   | 0       
c0u0p1  | SSD  | PHYG02440004240AGN SSDSC2KG240G8R XCV1DL69 | 223.0 GB | Online, Spun Up | 6.0Gb/s  | 24C  | [32:1]   | 1       
c0u1p0  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A115TP4F      | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 33C  | [32:2]   | 2       
c0u1p1  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A113TP4F      | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 34C  | [32:3]   | 3       
c0u1p2  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A114TP4F      | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 33C  | [32:4]   | 4       
c0u1p3  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A10KTP4F      | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 33C  | [32:5]   | 5       

-- Unconfigured Disk information --
-- ID   | Type | Drive Model                                        | Size     | Status                        | Speed    | Temp | Slot ID  | LSI ID | Path    
c0uXpY  | SSD  | S47NNA0N201026 SAMSUNG MZ7KH960HAJR-00005 HXM7404Q | 893. Gb  | Unconfigured(good), Spun Up   | 6.0Gb/s  | 33C  | [32:6]   | 6      | N/A     
c0uXpY  | SSD  | S47NNA0N105465 SAMSUNG MZ7KH960HAJR-00005 HXM7404Q | 893. Gb  | Unconfigured(good), Spun Up   | 6.0Gb/s  | 32C  | [32:7]   | 7      | N/A
~~~

Nos 2 nouveaux disques (`32:6` et `32:7`) sont dans l'état `Unconfigured`.

On tente de créer un nouveau volume logique (`-CfgLdAdd`) de type RAID1 (`-r1`) avec 2 disques sur notre carte (`-a0`).

Soit ça fonctionne directement (si les disques sont neufs), soit ce sont des disques réutilisés et il y aura probablement une erreur de ce type :

~~~
# megacli -CfgLdAdd -r1 [32:6,32:7] -a0

The specified physical disk does not have the appropriate attributes to complete
the requested command.

Exit Code: 0x26
~~~

Cela signifie qu'il y a déjà une configuration RAID trouvée sur ces disques.

Pour s'en assurer on cherche une configuration étrangère (`-CfgForeign -Scan`) :

~~~
# megacli -CfgForeign -Scan -a0

There are 1 foreign configuration(s) on controller 0.

Exit Code: 0x00
~~~

On demande alors à afficher la configuration (`-CfgForeign -Dsply 0` puisqu'il n'y en a qu'une), pour vérifier si elle concerne bien les disques ajoutés et pas des disques déjà présents :

~~~
# megacli -CfgForeign -Dsply 0 -a0
[…]
Enclosure Device ID: 32
Slot Number: 7
[…]
~~~

L'affichage est très long mais il faut chercher des lignes qui mentionnent nos disques.

Maintenant qu'on est sûr, on supprime cette configuration étrangère :

~~~
# megacli -CfgForeign -Clear -a0
Foreign configuration 0 is cleared on controller 0.
Exit Code: 0x00
~~~

On peut alors retenter de créer le volume logique :

~~~
# megacli -CfgLdAdd -r1 [32:6,32:7] -a0

Adapter 0: Created VD 1

Adapter 0: Configured the Adapter!!

Exit Code: 0x00
~~~

On peut ensuite ajouter au RAID créé les propriétés (`-LDSetProp`) courantes, en ciblant le volume logique `L2` de l'adapteur `a0` :

~~~
# megacli -LDSetProp -Cached -L2 -a0

Set Cache Policy to Cached on Adapter 0, VD 2 (target id: 2) success

Exit Code: 0x00

# megacli -LDSetProp EnDskCache -L2 -a0

Set Disk Cache Policy to Enabled on Adapter 0, VD 2 (target id: 2) success

Exit Code: 0x00

# megacli -LDSetProp RA -L2 -a0

Set Read Policy to ReadAhead on Adapter 0, VD 2 (target id: 2) success

Exit Code: 0x00
~~~

Notre carte RAID indique donc :

~~~
# megaclisas-status
-- Controller information --
-- ID | H/W Model          | RAM    | Temp | BBU    | Firmware
c0    | PERC H730P Adapter | 2048MB | 54C  | Good   | FW: 25.5.7.0005

-- Array information --
-- ID | Type   |    Size |  Strpsz | Flags | DskCache |   Status |  OS Path | CacheCade |InProgress
c0u0  | RAID-1 |    223G |   64 KB | RA,WB |  Enabled |  Optimal | /dev/sda | None      |None
c0u1  | RAID-1 |    893G |   64 KB | RA,WB |  Enabled |  Optimal | /dev/sdb | None      |None
c0u2  | RAID-1 |    894G |   64 KB | RA,WB |  Enabled |  Optimal | /dev/sdc | None      |Background Initialization : Completed 0%, Taken 0 min.

-- Disk information --
-- ID   | Type | Drive Model                                        | Size     | Status          | Speed    | Temp | Slot ID  | LSI ID
c0u0p0  | SSD  | PHYG0240000L240AGN SSDSC2KG240G8R XCV1DL69         | 223.0 GB | Online, Spun Up | 6.0Gb/s  | 28C  | [32:0]   | 0
c0u0p1  | SSD  | PHYG02440004240AGN SSDSC2KG240G8R XCV1DL69         | 223.0 GB | Online, Spun Up | 6.0Gb/s  | 24C  | [32:1]   | 1
c0u1p0  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A115TP4F              | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 33C  | [32:2]   | 2
c0u1p1  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A113TP4F              | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 33C  | [32:3]   | 3
c0u1p2  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A114TP4F              | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 32C  | [32:4]   | 4
c0u1p3  | SSD  | TOSHIBA KPM5XVUG480G B01C80D0A10KTP4F              | 446.6 Gb | Online, Spun Up | 12.0Gb/s | 33C  | [32:5]   | 5
c0u2p0  | SSD  | S47NNA0N201026 SAMSUNG MZ7KH960HAJR-00005 HXM7404Q | 893. Gb  | Online, Spun Up | 6.0Gb/s  | 33C  | [32:6]   | 6
c0u2p1  | SSD  | S47NNA0N105465 SAMSUNG MZ7KH960HAJR-00005 HXM7404Q | 893. Gb  | Online, Spun Up | 6.0Gb/s  | 32C  | [32:7]   | 7
~~~

On voit bien un 3ᵉ volume logique (`c0u2`) en RAID-1, en cours de reconstruction. Dans la liste des disques, on retrouve nos 2 disques associés.

## Ajout d'un nouveau volume RAID10

La méthode est sensiblement la même mais il faut créer un « span » au lieu d'un « logical drive ».

Point de départ :

~~~
# megaclisas-status 
-- Controller information --
-- ID | H/W Model          | RAM    | Temp | BBU    | Firmware     
c0    | PERC H730P Adapter | 2048MB | 57C  | Good   | FW: 25.5.8.0001 

-- Array information --
-- ID | Type   |    Size |  Strpsz | Flags | DskCache |   Status |  OS Path | CacheCade |InProgress   
c0u0  | RAID-1 |   3491G |   64 KB | RA,WB |  Enabled |  Optimal | /dev/sda | None      |None         

-- Disk information --
-- ID   | Type | Drive Model                       | Size     | Status          | Speed    | Temp | Slot ID  | LSI ID  
c0u0p0  | SSD  | 20422C2ABCB0MTFDDAK1T9TDT D3DJ004 | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 36C  | [32:0]   | 0       
c0u0p1  | SSD  | 20422C2AAE07MTFDDAK1T9TDT D3DJ004 | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 35C  | [32:1]   | 1       
c0u0p2  | SSD  | 20422C2AAAA2MTFDDAK1T9TDT D3DJ004 | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [32:2]   | 2       
c0u0p3  | SSD  | 20422C2AA0CAMTFDDAK1T9TDT D3DJ004 | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [32:3]   | 3       

-- Unconfigured Disk information --
-- ID   | Type | Drive Model                                        | Size     | Status                        | Speed    | Temp | Slot ID  | LSI ID | Path    
c0uXpY  | SSD  | S47PNA0T300141 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Unconfigured(good), Spun Up   | 6.0Gb/s  | 31C  | [32:4]   | 4      | N/A     
c0uXpY  | SSD  | S47PNA0T300145 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Unconfigured(good), Spun Up   | 6.0Gb/s  | 31C  | [32:5]   | 5      | N/A     
c0uXpY  | SSD  | S47PNA0T300146 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Unconfigured(good), Spun Up   | 6.0Gb/s  | 34C  | [32:6]   | 6      | N/A     
c0uXpY  | SSD  | S47PNA0T300142 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Unconfigured(good), Spun Up   | 6.0Gb/s  | 34C  | [32:7]   | 7      | N/A     
~~~

On crée l'ensmeble :

~~~
# megacli -CfgSpanAdd -r10 -Array0[32:4,32:5] -Array1[32:6,32:7] -a0
                                     
Adapter 0: Created VD 1

Adapter 0: Configured the Adapter!!

Exit Code: 0x00
~~~

Et voilà le résultat :

~~~
# megaclisas-status 
-- Controller information --
-- ID | H/W Model          | RAM    | Temp | BBU    | Firmware     
c0    | PERC H730P Adapter | 2048MB | 57C  | Good   | FW: 25.5.8.0001 

-- Array information --
-- ID | Type    |    Size |  Strpsz |   Flags | DskCache |   Status |  OS Path | CacheCade |InProgress   
c0u0  | RAID-1  |   3491G |   64 KB |   RA,WB |  Enabled |  Optimal | /dev/sda | None      |None         
c0u1  | RAID-10 |   3491G |   64 KB | ADRA,WB |  Default |  Optimal | /dev/sdb | None      |None         

-- Disk information --
-- ID     | Type | Drive Model                                        | Size     | Status          | Speed    | Temp | Slot ID  | LSI ID  
c0u0p0    | SSD  | 20422C2ABCB0MTFDDAK1T9TDT D3DJ004                  | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 36C  | [32:0]   | 0       
c0u0p1    | SSD  | 20422C2AAE07MTFDDAK1T9TDT D3DJ004                  | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 35C  | [32:1]   | 1       
c0u0p2    | SSD  | 20422C2AAAA2MTFDDAK1T9TDT D3DJ004                  | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [32:2]   | 2       
c0u0p3    | SSD  | 20422C2AA0CAMTFDDAK1T9TDT D3DJ004                  | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [32:3]   | 3       
c0u1s0p0  | SSD  | S47PNA0T300141 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [32:4]   | 4       
c0u1s0p1  | SSD  | S47PNA0T300145 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 31C  | [32:5]   | 5       
c0u1s1p0  | SSD  | S47PNA0T300146 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 34C  | [32:6]   | 6       
c0u1s1p1  | SSD  | S47PNA0T300142 SAMSUNG MZ7KH1T9HAJR-00005 HXM7904Q | 1.745 TB | Online, Spun Up | 6.0Gb/s  | 34C  | [32:7]   | 7       
~~~

Il faut ensuite ajuster les options du volume, comme pour le RAID1.

## Sauvegarde/restauration de la configuration

~~~
# megacli -CfgSave -f /home/backup/megacli_conf.dump -a0

# megacli -CfgRestore -f /home/backup/megacli_conf.dump -a0
~~~
