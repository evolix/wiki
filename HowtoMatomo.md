---
title: Howto Matomo
categories: web webapp
...

* Documentation : <https://matomo.org/faq/on-premise/matomo-requirements/>

Matomo est une application web affichant des statistiques de consultation de pages web tout en protégeant les données et la vie privée des internautes.


## Installer Matomo

### Pré-requis

Il y a besoin de ces pré-requis pour Debian 11 (Bullseye) :

~~~
# apt install php php-curl php-gd php-cli php-mysql php-xml php-mbstring
~~~

Nous préconisons d'utiliser Matomo avec [Apache-ITK](HowtoApache) et [MariaDB](HowtoMySQL).

Pour [Apache-ITK](HowtoApache) on désactive la gestion des droits pour faciliter les mises à jour :

~~~
AssignUserID example example
~~~


### Déployer

Se connecter avec le compte UNIX dédié, télécharger l'application et le dézipper en supposant que le DocumentRoot soit par exemple `/home/compte_unix/www/` :

~~~
# su - compte_unix
$ wget https://builds.matomo.org/matomo.zip && unzip matomo.zip
$ mv -i matomo www
~~~

Il reste à suivre les instructions de son installation via https://domaine-matomo-instance.com


## Configurer

### Forcer le SSL

Pour augmenter d'un cran le niveau de sécurité, on ajoute dans `/home/matomo/www/config/config.ini.php` ces directives :

~~~
[General]
force_ssl = 1
~~~


### Ajouter un utilisateur admin

Si on est bloqué, on peut insérer un nouvel utilisateur admin dans la base avec un mot de passe temporaire (à changer après la première connexion) :

~~~
$ php -r 'echo password_hash(md5(<TMP_PASS>), PASSWORD_DEFAULT) . "\n";'
$2y$10$v2HBGALslnw5ZeRaQDpon.xmQOZcRpxkYkrvlTs41j2F.THyLQhn6

$ mysql
MariaDB [(none)]> use matomo;
MariaDB [matomo_prod]> INSERT INTO `matomo_user` (login, password, email, superuser_access) VALUES ('<NEW_OGIN>', '<PASS_HASH>', '<MAIL>', 1);
~~~


### Activer l'archivage

Désactiver l'archivage via l'interface web dans : *administration (roue cranté)* -> *Système* -> *paramètres généraux* -> *Paramètres d'archivage* -> mettre sur *Non*

Puis ajouter cette tâche dans la crontab de l'utilisateur :

~~~
MAITO="foo@bar.com"
30 * * * * /usr/bin/php /home/matomo/www/console core:archive --quiet
~~~

### Configurer les dumps

Si vous utilisez `mysqldump` pour sauvegarder vos bases de données, il faut utiliser les options `--single-transaction --skip-lock-tables`, sans quoi vous risquez d'avoir un timeout de Matomo pendant la durée des dumps à cause du lock des tables par `mysqldump`.



## Mettre-à-jour

On peut facilement mettre à jour Matomo depuis l'interface d'administration en un seul click.
Il faut avant s'assurer que le répertoire racine de Matomo soit bien avec les droits en `755`

Ensuite il faut se connecter en tant que "super user" ou "admin" à Matomo et aller dans la section « Administration », si une nouvelle mise à jour est disponible, elle est proposée, donc cliquer sur "Update Automatically".

### Mise à jour sur les serveurs Matomo à fort trafic

Pour les serveurs à fort trafic, la mise à jour de Matomo peut prendre entre plusieurs minutes à plusieurs heures, il est donc recommandé de lancer la mise à jour en ligne de commande.

Vous pouvez mettre à jour Matomo en cli de cette façon :

~~~
$ php /home/matomo/www/console core:update
~~~ 

Il est recommandé également pour les serveurs a haut trafic, de désactiver le tracking des visiteurs et l'interface web de cette façon :

~~~
[Tracker]
record_statistics = 0

[General]
maintenance_mode = 1
~~~

Lorsque Matomo est en `maintenance_mode=1` le endpoint de l'API `index.php` retourne une `503`.
Si Matomo est derrière un load-balancer de type Haproxy, il faut rajouter la variable `multi_server_environment=1` pour que Matomo renvoie une `200`

Cela garantira que les mises à jour du schéma de base de données sont terminées dès que possible et sans erreurs de lecture/écriture simultanées.

Lorsque la mise à jour est terminé, **ne pas oublier de désactiver le mode maintenance et de réactiver le tracking.**

### Une fois la mise à jour terminée

Lorsque la mise à jour s'est terminé avec succès, il est recommandé d'aller dans `Admin > Diagnostics > System check` et d'examiner les recommandations faites par Matomo.

Par exemple, la vérification de l'intégrité des fichiers peut indiquer qu'il reste des fichiers de la version précédente qui peuvent désormais être supprimés.

## FAQ

### Performance

<https://fr.matomo.org/faq/on-premise/how-to-configure-matomo-for-speed/>