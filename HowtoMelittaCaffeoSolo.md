---
categories: coffee office
title: Howto Melitta Caffeo Solo
---

Au bureau de Marseille, nous utilisons une machine à café de la marque Melitta, modèle « Caffeo Solo » (achetée en 2015).

La notice est disponible sur le site de [Melitta](https://www.melitta.fr/conseils-de-nos-experts/assistance-service/modes-d-emploi/) ([lien direct PDF](https://www.melitta.de/media/pdf/3b/e4/4a/Solo_total_WEB.pdf)).

Il existe une [catégorie dédiée sur le site de iFixit](https://fr.ifixit.com/Device/Melitta_Caffeo_Solo).

