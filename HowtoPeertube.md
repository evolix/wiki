---
categories: web video
toc: no
title: Howto Peertube
...

* Documentation: [docs.joinpeertube.org/](https://docs.joinpeertube.org/)
* Dépôt: [github.com/Chocobozzz/PeerTube](https://github.com/Chocobozzz/PeerTube)

[Peertube](https://joinpeertube.org/) est une plateforme de diffusion en continu décentralisé. Il implémente le protocole de fédération ActivityPub et les protocoles pair-à-pair WebTorrents et HLS + P2P.

## Installation

PeerTube 5.2 (août 2023) dépend des paquets suivants sur un système Debian 11 (Bullseye):

~~~
# apt install curl python3-dev python-is-python3 certbot nginx ffmpeg postgresql postgresql-contrib openssl g++ make redis-server git unzip
~~~

Note : il faut également installer Node JS >= 16.x via le dépôt https://deb.nodesource.com et Redis >= 6.2.x via le dépôt bullseye backports.

### Utilisateur PeerTube

Créer un utilisateur peertube avec /var/www/peertube home:

~~~
$ sudo useradd -m -d /var/www/peertube -s /bin/bash -p peertube peertube
~~~

Définissez son MOT DE PASSE:

~~~
$ sudo passwd peertube
~~~

### Database

Créez la base de données de production et un utilisateur peertube à l'intérieur de PostgreSQL:

~~~
$ cd /var/www/peertube
$ sudo -u postgres createuser -P peertube
~~~

Ici, vous devez entrer un mot de passe pour l'utilisateur peertube PostgreSQL, qui doit être copié dans le fichier production.yaml. Ne vous contentez pas d'appuyer sur Entrée sinon il sera vide.

~~~
$ sudo -u postgres createdb -O peertube -E UTF8 -T template0 peertube_prod
~~~

Activez ensuite les extensions dont PeerTube a besoin:

~~~
$ sudo -u postgres psql -c "CREATE EXTENSION pg_trgm;" peertube_prod
$ sudo -u postgres psql -c "CREATE EXTENSION unaccent;" peertube_prod
~~~

### Préparer le répertoire PeerTube

Récupérer la dernière version taguée de Peertube:

~~~
VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) && echo "Latest Peertube version is $VERSION"
~~~

Ouvrez le répertoire peertube, créez quelques répertoires requis:

~~~
$ cd /var/www/peertube
$ sudo -u peertube mkdir config storage versions
$ sudo -u peertube chmod 750 config/
~~~

Téléchargez la dernière version du client Peertube, décompressez-la et supprimez le zip:

~~~
$ cd /var/www/peertube/versions
$ # Releases are also available on https://builds.joinpeertube.org/release
$ sudo -u peertube wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"
$ sudo -u peertube unzip -q peertube-${VERSION}.zip && sudo -u peertube rm peertube-${VERSION}.zip
~~~

### Installation fichiers web

~~~
$ cd /var/www/peertube
$ sudo -u peertube ln -s versions/peertube-${VERSION} ./peertube-latest
$ cd ./peertube-latest && sudo -H -u peertube yarn install --production --pure-lockfile
~~~

### Paramétrage de PeerTube

Copiez le fichier de configuration par défaut qui contient la configuration par défaut fournie par PeerTube. Vous ne devez pas mettre à jour ce fichier.

~~~
$ cd /var/www/peertube
$ sudo -u peertube cp peertube-latest/config/default.yaml config/default.yaml
~~~

Copiez maintenant l'exemple de configuration de production:

~~~
$ cd /var/www/peertube
$ sudo -u peertube cp peertube-latest/config/production.yaml.example config/production.yaml
~~~

puis éditez le fichier config/production.yaml en fonction de la configuration de votre serveur web et de votre base de données (sections webserver, database, redis, smtp et admin.email notamment). Les clés définies dans config/production.yaml remplaceront les clés définies dans config/default.yaml.

PeerTube ne prend pas en charge le changement d'hôte de serveur Web. Même si PeerTube CLI peut vous aider à changer de nom d'hôte, il n'y a pas de support officiel pour cela car il s'agit d'une opération risquée qui peut entraîner des erreurs imprévues.


### Webserver

Copiez le modèle de configuration nginx:

~~~
$ sudo cp /var/www/peertube/peertube-latest/support/nginx/peertube /etc/nginx/sites-available/peertube
~~~

Définissez ensuite le domaine pour le fichier de configuration du serveur Web. Remplacez [peertube-domain] par le domaine du serveur peertube.

~~~
$ sudo sed -i 's/${WEBSERVER_HOST}/[peertube-domain]/g' /etc/nginx/sites-available/peertube
$ sudo sed -i 's/${PEERTUBE_HOST}/127.0.0.1:9000/g' /etc/nginx/sites-available/peertube
~~~

Modifiez ensuite le fichier de configuration du serveur Web. Veuillez prêter attention aux clés d'alias des emplacements statiques. Il doit correspondre aux chemins de vos répertoires de stockage (définis dans le fichier de configuration à l'intérieur de la clé de stockage).

~~~
$ sudo vim /etc/nginx/sites-available/peertube
~~~

Activez le fichier de configuration:

~~~
$ sudo ln -s /etc/nginx/sites-available/peertube /etc/nginx/sites-enabled/peertube
~~~

Pour générer le certificat pour votre domaine comme requis pour faire fonctionner https, vous pouvez utiliser Let's Encrypt:

~~~
$ sudo systemctl stop nginx
$ sudo certbot certonly --standalone --post-hook "systemctl restart nginx"
$ sudo systemctl reload nginx
~~~

Maintenant que vous avez les certificats, vous pouvez recharger nginx :

~~~
$ sudo systemctl reload nginx
~~~

Certbot doit avoir installé un cron pour renouveler automatiquement votre certificat. Étant donné que notre modèle nginx prend en charge le renouvellement Webroot, nous vous suggérons de mettre à jour le fichier de configuration de renouvellement pour utiliser l'authentificateur Webroot.:

~~~
$ # Replace authenticator = standalone by authenticator = webroot
$ # Add webroot_path = /var/www/certbot
$ sudo vim /etc/letsencrypt/renewal/your-domain.com.conf
~~~

## Configuration

### Administrateur

Le nom d'utilisateur de l'administrateur est root et le mot de passe est généré automatiquement. Il se trouve dans les logs de PeerTube (chemin défini dans production.yaml). Vous pouvez également définir un autre mot de passe avec:
~~~
$ cd /var/www/peertube/peertube-latest && NODE_CONFIG_DIR=/var/www/peertube/config NODE_ENV=production npm run reset-password -- -u root
~~~

Note : il est aussi possible de définir une variable d'environnement PT_INITIAL_ROOT_PASSWORD qui doit contenir un minimum de six caractères.

## Optimisations TCP/IP 

~~~
$ sudo cp /var/www/peertube/peertube-latest/support/sysctl.d/30-peertube-tcp.conf /etc/sysctl.d/
$ sudo sysctl -p /etc/sysctl.d/30-peertube-tcp.conf
~~~


## systemd

Copier le modèle d'unité systemd fournit par les développeurs:
~~~
$ sudo cp /var/www/peertube/peertube-latest/support/systemd/peertube.service /etc/systemd/system/
~~~
Ajuste les chemins et options au besoin:
~~~
$ sudo vim /etc/systemd/system/peertube.service
~~~
Dites à systemd de recharger sa configuration:
~~~
$ sudo systemctl daemon-reload
~~~
Si vous souhaitez démarrer PeerTube en même temps que le système d'exploitation:
~~~
$ sudo systemctl enable peertube
~~~
Pour lancer PeerTube:
~~~
$ sudo systemctl start peertube
~~~


## Administration

Si vous avez modifié votre configuration nginx :

~~~
$ sudo systemctl reload nginx
~~~

Si vous avez modifié votre configuration systemd:

~~~
$ sudo systemctl daemon-reload
~~~

Redémarrez PeerTube et vérifiez les journaux:

~~~
$ sudo systemctl restart peertube && sudo journalctl -fu peertube
~~~

Pour réinitialiser un mot de passe utilisateur à partir de CLI

~~~
$ cd /var/www/peertube/peertube-latest
$ sudo -u peertube NODE_CONFIG_DIR=/var/www/peertube/config NODE_ENV=production npm run reset-password -- -u target_username
~~~

Les choses ont mal tourné ?

Remplacez peertube-latest destination par la version précédente et restaurez votre sauvegarde SQL:

~~~
$ OLD_VERSION="v0.42.42" && SQL_BACKUP_PATH="backup/sql-peertube_prod-2018-01-19T10:18+01:00.bak" && \
    cd /var/www/peertube && sudo -u peertube unlink ./peertube-latest && \
    sudo -u peertube ln -s "versions/peertube-$OLD_VERSION" peertube-latest && \
    sudo -u postgres pg_restore -c -C -d postgres "$SQL_BACKUP_PATH" && \
    sudo systemctl restart peertube
~~~

### Mise a jour

Lire attentivement les notes de version: https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md

#### Automatique

Le mot de passe demandé est le mot de passe de l'utilisateur de la base de données de PeerTube.

~~~
$ cd /var/www/peertube/peertube-latest/scripts && sudo -H -u peertube ./upgrade.sh
$ sudo systemctl restart peertube 
~~~

#### Manuelle

Faire une sauvegarde la BD PostgreSQL

~~~
$ SQL_BACKUP_PATH="backup/sql-peertube_prod-$(date -Im).bak" && \
    cd /var/www/peertube && sudo -u peertube mkdir -p backup && \
    sudo -u postgres pg_dump -F c peertube_prod | sudo -u peertube tee "$SQL_BACKUP_PATH" >/dev/null
~~~

Récupérez la dernière version taguée de Peertube :

~~~
$ VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) && echo "Latest Peertube version is $VERSION"
~~~

Téléchargez la nouvelle version et décompressez-la :

~~~
$ cd /var/www/peertube/versions && \
    sudo -u peertube wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip" && \
    sudo -u peertube unzip -o peertube-${VERSION}.zip && \
    sudo -u peertube rm peertube-${VERSION}.zip
~~~

Installer les dépendances de Node JS:

~~~
$ cd /var/www/peertube/versions/peertube-${VERSION} && \
    sudo -H -u peertube yarn install --production --pure-lockfile
~~~

Copiez les nouvelles valeurs de configuration par défaut et mettez à jour votre fichier de configuration :

~~~
$ sudo -u peertube cp /var/www/peertube/versions/peertube-${VERSION}/config/default.yaml /var/www/peertube/config/default.yaml
$ diff /var/www/peertube/versions/peertube-${VERSION}/config/production.yaml.example /var/www/peertube/config/production.yaml
~~~

Modifiez le lien symbolique pour qu'il pointe sur la dernière version :

~~~
$ cd /var/www/peertube && \
    sudo unlink ./peertube-latest && \
    sudo -u peertube ln -s versions/peertube-${VERSION} ./peertube-latest
~~~

Démarrer redis et postgresql :

~~~
sudo systemctl enable --now redis
sudo systemctl enable --now postgresql
~~~

Vérifiez les modifications apportées à la configuration nginx :

~~~
$ cd /var/www/peertube/versions
$ diff "$(ls --sort=t | head -2 | tail -1)/support/nginx/peertube" "$(ls --sort=t | head -1)/support/nginx/peertube"
~~~

Vérifiez les changements dans la configuration de systemd :

~~~
$ cd /var/www/peertube/versions
$ diff "$(ls --sort=t | head -2 | tail -1)/support/systemd/peertube.service" "$(ls --sort=t | head -1)/support/systemd/peertube.service"
~~~

## Optimisation

## Monitoring

### Nagios

### Munin

## Plomberie

## FAQ




