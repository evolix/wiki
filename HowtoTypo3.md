---
categories: web
title: Howto Typo3
...

* Documentation : <https://docs.typo3.org/>

[Typo3](https://typo3.org/) est un CMS Open Source adapté pour les sites nécessitant de très nombreuses fonctionnalités.

## Installation

~~~
# aptitude install imagemagick graphicsmagick php5-imagick
# aptitude install php-apc php5-curl
~~~

Paramètres PHP à modifier :

~~~
upload_max_filesize => 20 Mo
max_excution_time => 90s
Activer la fonction exec()
~~~

Modules Apache à activer et autoriser via `AllowOverride` :

~~~
mod_expires
mod_rewrite
mod_deflate
~~~

~~~
$ touch ENABLE_INSTALL_TOOL
$ chmod g+w ENABLE_INSTALL_TOOL
~~~

Droits à ajuster :

~~~
$ chmod g+w typo3temp/
$ chmod g+w typo3conf/localconf.php
$ chmod g+w typo3conf
$ chmod g+w chmod g+w fileadmin/ typo3conf/ext/ uploads/
$ chmod g+w typo3conf/l10n/ 
$ chmod g+w uploads/pics/
$ chmod g+w uploads/media/
$ chmod g+w uploads/tf/
$ chmod g+w fileadmin/_temp_/
$ chmod g+w typo3/ext/ 
~~~

## Troubleshooting

### Boucle avec flock

Certaines versions de Typo3 incluent un mécanisme de lock pour éviter les conflits et race conditions, cf <https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Lock%21LockBackendInterface.php/group/lock/8.6.x>

Cela peut parfois poser des problèmes (notamment avec Apache-ITK ?) avec des process qui vont boucler infiniment pour obtenir un lock :

~~~
[...]
chmod("/home/example/www/typo3temp/var/locks/flock_d7343161e87efb43c0324efc5cdf0784", 0664) = 0
flock(103, LOCK_EX|LOCK_NB)             = -1 EAGAIN (Resource temporarily unavailable)
flock(101, LOCK_UN)                     = 0
close(101)                              = 0
[...]
~~~

La solution est alors de killer le process et de supprimer le fichier de lock pour éviter que cela ne s'accumule.

On peut ainsi mettre un cron du type :

~~~
*/5 * * * * for i in `find /home/*/www/typo3temp/var/locks/flock_* -not -newermt '-600 seconds'`; do for j in `lsof -t $i`; do echo "Kill process `lsof $i`"; kill $j; done; rm $i; done
~~~