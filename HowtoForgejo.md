---
title: Howto Forgejo
categories: sysadmin git forge
...

* Documentation : <https://forgejo.org/docs/latest/>

[Forgejo](https://forgejo.org) est un Logiciel Libre de gestion de code source permettant de créer des projets avec un dépôt [Git](HowtoGit), des tickets, un wiki et une collaboration autour du code (fork, pull requests, etc.). Forgejo est un fork de [Gitea](HowtoGitea), qui lui même est un fork de [Gogs](https://gogs.io/) qui ressemble fortement au logiciel propriétaire *Github*. Forgejo est écrit en Go et nous le trouvons léger, simple à installer et à maintenir contrairement à Gitlab.


## Installation

Malheureusement, Forgejo n'est pas encore distribué sous forme de paquet Debian. Ni présent dans Debian car de nombreuses dépendances en Go sont manquantes. Une alternative est donc d'utiliser directement les binaires produits par les développeurs de Forgejo. Ils sont récupérables sur <https://forgejo.org/releases/>.

Le binaire peut simplement être déposé dans `/usr/local/bin` car il contient toutes les dépendances nécessaires à son bon fonctionement.

~~~
# cd /usr/local/bin
# VERSION=10.0.1
# wget https://codeberg.org/forgejo/forgejo/releases/download/v$VERSION/forgejo-$VERSION-linux-amd64
# chmod 755 forgejo-$VERSION-linux-amd64
# ln -s forgejo-$VERSION-linux-amd64 forgejo
<>
# mkdir /etc/forgejo/

# forgejo --version
Forgejo version 10.0.1+gitea-1.22.0 (release name 10.0.1) built with GNU Make 4.4.1, go1.23.6 : bindata, timetzdata, sqlite, sqlite_unlock_notify
~~~

> *Note* : Même si Forgejo peut fonctionner de manière autonome avec SQLite, à partir d'une certaine taille/utilisation, il est préférable de basculer vers un moteur de base de données tel que [HowtoMySQL]() ou [HowtoPostgreSQL]() et d'ajouter [HowtoRedis]() pour le cache/sessions.


## Mise à jour

Le processus de mise à jour est assez simple et similaire au processus d'installation. Après avoir bien regardé s'il n'y a pas d'actions à faire manuellement dans les notes de publications de la nouvelle version, on peut simplement télécharger le dernier binaire dans `/usr/local/bin/`. Lui appliquer les bon droits (*chmod 755*) et mettre à jour le lien symbolique `/usr/local/bin/forgejo` vers ce nouveau binaire.

Il ne reste plus qu'à redémarrer l'instance. C'est terminé !


## Configuration 

Dans la suite de cette documentation, on se place dans le cas d'une instance simple, fonctionnant avec l'utilisateur **git**. On part du principe que :

* [MySQL](HowtoMySQL) est déjà installé
* Le compte UNIX **git** existe sur la machine. 
* La configuration sera écrire dans `/etc/forgejo/git.ini` 
* Tous les fichiers de forgejo seront dans `/home/git/` **Attention, il ne doit pas y avoir de noexec**
* L'adresse de l'instance sera forgejo.example.org


Côté fichiers applicatifs, on va se retrouver avec la hiérarchie suivante : 

~~~
$ tree /home/git
├── internals 
│   ├── custom            << Fichiers de customisation de l'interface web de forgejo
│   │   ├── public        << Fichiers servis par le serveur web
│   │   └── templates     << Templates des pages
│   │       └── home.tmpl
│   │ 
│   └── data              << Toutes les données de forgejo (autres que les dépôts git)
│       ├── avatars
│       ├── indexers
│       ├── lfs
│       └── sessions
│
├── log                    << Journaux applicatifs
│   ├── gitea.log
│   ├── http.log
│   ├── serv.log
│   └── xorm.log
│
└── repositories            << Données des dépôts git
    └── foo
        └── hello_world.git
~~~

Ces dossiers seront créés au premier lancement de Forgejo. Il faut donc s'assurer que certaines variables d'environnement sont bien positionnées. 
 
Lors de la première exécution de Forgejo, une page d'installation va demander tous les paramètres nécessaires pour initialiser l'instance. Celle-ci va aussi permettre de créer le premier compte administrateur. 

### /etc/forgejo/git.ini

Avant de commencer, on doit initialiser le fichier de configuration de forgejo avec les commandes suivantes.

~~~
# touch /etc/forgejo/git.ini
# chown git:git /etc/forgejo/git.ini
~~~

On ajoute aussi quelques réglages initiaux, pour que l'interface de Forgejo soit directement fonctionnelle (avec des logs en cas de souci) :

~~~
# cat /etc/forgejo/git.ini
APP_NAME = Forgejo
RUN_USER = git
RUN_MODE = prod

[server]
PROTOCOL               = http+unix
DOMAIN                 = forgejo.example.org
HTTP_ADDR              = /run/forgejo/forgejo.sock
UNIX_SOCKET_PERMISSION = 666
OFFLINE_MODE           = true
SSH_DOMAIN             = forgejo.example.org
ROOT_URL               = http://forgejo.example.org/
ENABLE_GZIP            = true
LANDING_PAGE           = home
DISABLE_SSH            = false
SSH_PORT               = 22
LFS_START_SERVER       = true

[service]
REGISTER_EMAIL_CONFIRM                 = true
DISABLE_REGISTRATION                   = true
SHOW_REGISTRATION_BUTTON               = true
ENABLE_CACHE_AVATAR                    = false
ENABLE_NOTIFY_MAIL                     = true
ENABLE_REVERSE_PROXY_AUTHENTICATION    = false
ENABLE_REVERSE_PROXY_AUTO_REGISTRATION = false
ALLOW_ONLY_EXTERNAL_REGISTRATION       = false
ENABLE_CAPTCHA                         = true
REQUIRE_SIGNIN_VIEW                    = false
DEFAULT_USER_VISIBILITY                = limited
NO_REPLY_ADDRESS                       = forgejo.example.org

[repository]
ROOT                   = /home/git/repositories
MAX_CREATION_LIMIT     = 0

[log]
ROOT_PATH              = /home/git/log/
MODE                   = file
LEVEL                  = Info

[i18n]
LANGS = fr-FR, en-US
NAMES = Français,English
~~~

### systemd

On peut utiliser l'unité systemd suivante. Elle est inspirée de celle [proposée par les développeurs de Forgejo](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/contrib/systemd/forgejo.service). Nous l'avons ajustée pour correspondre au dossiers et réglages choisis plus haut.

~~~
# cat /etc/systemd/system/Forgejo.service

[Unit]
Description=Forgejo (Beyond coding. We forge.)
After=syslog.target
After=network.target
After=mysqld.service

[Service]
ExecStartPre=/bin/mkdir -m 0755 -p /run/forgejo
ExecStartPre=/bin/chown git: /run/forgejo
PermissionsStartOnly=yes

User=git
Group=git

Type=simple
RestartSec=2s
Restart=always

WorkingDirectory=/home/git
ExecStart=/usr/local/bin/forgejo web --config /etc/forgejo/git.ini
Environment=GITEA_WORK_DIR=/home/git/internals 


[Install]
WantedBy=multi-user.target
~~~

### MySQL

Par défaut, Forgejo utilise une base SQLite. Mais on peut utiliser MySQL (ou PostgreSQL), voir plus bas. 
Forgejo nécessite seulement une base MySQL avec un compte.

~~~
# mysqladmin create git
# mysql
mysql> GRANT ALL PRIVILEGES ON git.* TO 'git'@localhost IDENTIFIED BY 'PASSWORD';
~~~

Côté `forgejo.ini`, la base de donnée MySQL se configure avec la section ci-dessous. 

> *Note* : La page d'installation peut remplir cette section de la configuration automatiquement.

~~~
[database]
DB_TYPE  = mysql
HOST     = /run/mysqld/mysqld.sock
NAME     = git
USER     = git
PASSWD   = PASSWORD
~~~

### PostgreSQL

~~~
[database]
DB_TYPE = postgres
HOST = localhost
NAME = git
USER = git
PASSWD = PASSWORD
SCHEMA = 
SSL_MODE = disable
~~~

### Nginx

~~~
# cat /etc/nginx/sites-enabled/git
upstream forgejo_git {
    server unix:/run/forgejo/forgejo.sock;
}

server {
    listen 0.0.0.0:80;
    listen [::]:80;
    #listen 0.0.0.0:443 ssl http2;
    #listen [::]:443 ssl http2;

    server_name forgejo.example.org;

    #include /etc/nginx/letsencrypt.conf;
    #include /etc/nginx/ssl/git.conf;

    # Redirection HTTP vers HTTPS
    #if ( $scheme = http ) {
    #    return 301 https://$server_name$request_uri;
    #}

    location / {
        proxy_pass http://forgejo_git;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_read_timeout 10;
    }
}
~~~

### Redis

On peut utiliser Redis comme support pour les sessions et le cache de Forgejo.

#### Cache

~~~
# cat /etc/forgejo/git.ini
[cache]
ADAPTER = redis
HOST    = network=tcp,addr=127.0.0.1:6379,password=PASSWORD,db=1,pool_size=100,idle_timeout=180

~~~

#### Sessions

~~~
# cat /etc/forgejo/git.ini
[session]
PROVIDER         = redis
PROVIDER_CONFIG  = network=tcp,addr=127.0.0.1:6379,password=PASSWORD,db=0,pool_size=100,idle_timeout=180
~~~

### HTTPS

Après avoir activé le HTTPS sur le vhost nginx faisant office de reverse-proxy, il est utile d'ajuster quelques paramètres de configuration.

* Mettre à jour le `ROOT_URL` pour remplacer `http://forgejo.example.org` par `https://forgejo.example.org`
* Ajouter `COOKIE_SECURE    = true` dans la section `[session]`
* (Optionnel) Rajouter le préfixe `__Secure-` au nom des cookies. Il s'agit des paramètres `COOKIE_NAME` dans la section `[session]` ainsi que `COOKIE_USERNAME` et `COOKIE_REMEMBER_NAME` dans la section `[security]`


### Fail2Ban

`/etc/fail2ban/jail.d/forgejo.conf` :

~~~
[forgejo]
enabled   = true
port      = http,https,ssh
filter    = forgejo
logpath   = /home/git/log/gitea.log
maxretry  = 20
findtime  = 300
bantime   = 900
~~~

> *Remarque* : Ce n'est pas une erreur, à l'heure actuelle (version 10.0), le nom du log s'appelle toujours gitea.log par défaut

`/etc/fail2ban/filter.d/forgejo.conf` :

~~~
[Definition]
failregex =  .*(Failed authentication attempt|invalid credentials|Attempted access of unknown user).* from <HOST>
ignoreregex =
~~~

### SMTP

Configuration avec un Postfix local :

~~~
[mailer]
ENABLED = true
FROM = git@example.com
PROTOCOL = sendmail
SENDMAIL_PATH = /usr/sbin/sendmail
~~~

Il faudra configurer OpenDKIM afin de signer les emails via DKIM, ainsi que bien configurer SPF et DMARC pour le domaine utilisé.


## Avancé

### Multiples instances

La simplicité de Forgejo permet d'heberger plusieurs instances sur une même machine.

Comme pour la première instance, il a besoin de :

* D'un nouveau compte unix spécifique
* (Si MySQL) D'un nouveau compte avec une base associée.

On peut ainsi basculer sur une unité systemd avec paramètre comme celle donnée en exemple.
Ainsi, les instances seront manipulées avec `systemctl (start|restart|stop) forgejo@INSTANCE` où _INSTANCE_ se trouve être le nom de compte dédié à l'instance Forgejo


~~~
# cat /etc/systemd/system/forgejo@.service
[Unit]
Description=Forgejo (Beyond coding. We forge.)
After=syslog.target
After=network.target
After=mysqld.service

[Service]
User=%i
Group=%i

Type=simple
RestartSec=2s
Restart=always

WorkingDirectory=/home/%i
ExecStart=/usr/local/bin/Forgejo web --config /etc/Forgejo/%i.ini
Environment=GITEA_WORK_DIR=/home/%i/internals 

[Install]
WantedBy=multi-user.target
~~~

## Miroir

Les dépôts peuvent être configurés en miroir d’autres forges, ou pour mettre à jour des miroir sur d’autres forges.

### Gitlab (push)

Pour maintenir un [miroir sur une instance de Gitlab](https://docs.gitea.io/en-us/repo-mirror/#setting-up-a-push-mirror-from-gitea-to-gitlab), les quelques étapes suivantes suffisent.

1. Créer un [jeton d’accès personnel](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) **sur Gitlab** avec portée (« scope ») `write_repository`.

  * Sélectionner l’avatar en haut à droite.
  * Sélectionner « Modifier le profil » (« Edit profile »).
  * Dans la barre de gauche, sélectionner « Jetons d’accès » (« Access Tokens »).
  * Saisir un nom et éventuellement un date d’expiration pour le jeton.
  * Sélectionner la portée (« scope ») `write_repository`.
  * Cliquer sur « Créer jeton d’accès personnel » (« Create personal access token »).

2. Dans les réglages (« Settings » en haut à droite) du dépôt sur Gitea, entrer l’URL du dépôt distant (« Git Remote Repository URL ») dans les réglages du miroir (« Mirror Settings ») : `https://<hôte_de_destination>/<nom_ou_groupe_gitlab>/<projet_gitlab>.git`.

3. Remplir les champs d’autorisation « Authorization » avec `oauth2` comme nom d’utilisateur (« Username ») et le jeton d’accès personnel précédent comme mot de passe (« Password »).

4. Cliquer sur « Add Push Mirror » pour sauvegarder la configuration.


## FAQ / Plomberie

### La page d'accueil d'un projet ne se met pas à jour après le premier push

Il faut s'assurer que la partition qui contient le dossier des dépôts gits ne soit **pas** en `noexec`


### Impossible de visionner le diff d'un gros commit

> File diff suppressed because it is too large.

L'astuce consiste à mettre dans le fichier de configuration :

```
[git]
MAX_GIT_DIFF_LINES = 1000000
```

Augmenter à la valeur de votre choix.

### Chercher tous les accès d'un compte

```
# mysql
MariaDB [(none)]> use git
MariaDB [git]> select repository.name, user.name, access.mode from access  INNER JOIN user ON user.id = access.user_id INNER JOIN repository ON repository.id = access.repo_id WHERE user.name like '%USER%' \G
*************************** 1. row ***************************
name: REPO
name: USER
mode: 2
1 row in set (0.00 sec)
```

### Configurations spécifiques

Tous les paramètres configurables dans le fichier de configuration `.ini` sont disponibles sur <https://forgejo.org/docs/latest/admin/config-cheat-sheet/>

Par exemple pour changer la « taille » minimum des clés SSH RSA :

~~~
[ssh.minimum_key_sizes]
RSA = 2048
~~~



### Multi instance

Sur une machine, on peut très bien héberger plusieurs instances différences de forgejo.
Les instructions restent globalement les même, en adaptant les noms de compte, base sql etc.

Pour ne pas dupliquer l'unité systemd, il est possible d'en avoir une paramétrisée.

Elle part du principe que le nom de l'instance correspond au nom du compte unix ainsi qu'au nom de la configuration dans `/etc/forgejo/`

De plus, et pour pouvoir gérer les mises à jour des instances de manière indépendante, on utilise `/usr/local/bin/forgejo-<NOM_INSTANCE>` à place de `/usr/local/bin/forgejo`, comme lien symbolique vers la version de forgejo souhaitée.


~~~
# cat /etc/systemd/system/forgejo@.service 
[Unit]
Description=Forgejo (Beyond coding. We forge.) for %i
After=syslog.target
After=network.target
After=mysqld.service

[Service]
# Create /run/forgejo/INSTANCE folder for socket
ExecStartPre=/bin/mkdir -m 0755 -p /run/forgejo/%i
ExecStartPre=/bin/chown %i: /run/forgejo/%i
PermissionsStartOnly=yes

User=%i
Group=%i

Type=simple
RestartSec=2s
Restart=always

WorkingDirectory=/home/%i
ExecStart=/usr/local/bin/forgejo-%i web --config /etc/forgejo/%i.ini
Environment=GITEA_WORK_DIR=/home/%i/internals 

[Install]
WantedBy=multi-user.target
~~~

### Le dossier `repo-archive` prends énormément de place

Ce dossier sert à Forgejo pour stocker les générations d'archives zip (et autres) quand un usager clique sur "Télécharger comme archive ZIP" (ou tar.gz ou bundle) pour un dépot donné.
Il est normalement nettoyé régulièrement par une tâche cron interne à Forgejo.

Néanmoins, certains crawlers peuvent essayer d'aller sur un certain nombre de ces liens dans un laps de temps assez court, pour chaque commit, ce qui entraine la génération d'un grand nombre d'archives, et donc une forte occupation disque.
On peut manuellement forcer leur suppression via l'interface d'administration en cliquant sur le bouton `Delete all repositories' archives (ZIP, TAR.GZ, etc.)`