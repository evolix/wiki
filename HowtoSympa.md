---
categories: email
title: Howto Sympa
...

* Documentation : <https://sympa-community.github.io/manual/>
* Statut de cette page : prod / bullseye

[Sympa](http://www.sympa.org/) est un logiciel libre de gestion de listes de diffusion. Il est développé en Perl depuis plus de 20 ans par des développeurs français.
Il s'utilise avec une base de données [MySQL](HowtoMySQL) ou [PostgreSQL](HowtoPostgreSQL), et peut aller lire des données dans un annuaire [LDAP](HowtoOpenLDAP).

## Installation

Installer au préalable [Postfix](HowtoPostfix), [Apache](HowtoApache) et [MariaDB](HowtoMySQL).

~~~
# apt install sympa default-mysql-server libapache2-mod-fcgid opendkim-tools spawn-fcgi

# sympa -v
Sympa 6.2.60

# systemctl status sympa
● sympa.service - SYMPA mailing list manager
   Loaded: loaded (/lib/systemd/system/sympa.service; enabled; vendor preset: enabled)
     Docs: man:sympa_msg(8)
 Main PID: 18009 (sympa_msg.pl)
    Tasks: 1 (limit: 4915)
   CGroup: /system.slice/sympa.service
           └─18009 /usr/bin/perl /usr/lib/sympa/bin/sympa_msg.pl

Nov 05 20:34:34 systemd[1]: Starting SYMPA mailing list manager...
Nov 05 20:34:35 sympa_msg[17997]: info main::_load() Configuration file read, default log level 0
Nov 05 20:34:36 sympa_msg[17997]: notice Sympa::Process::daemonize() Starting sympa/msg daemon, PID 18009
Nov 05 20:34:36 sympa_msg[18009]: notice main:: Sympa/msg 6.2.16 Started
Nov 05 20:34:36 systemd[1]: Started SYMPA mailing list manager.
~~~

Nous conseillons d'utiliser une unité [systemd](HowtoSystemd) `wwsympa.service` pour la partie web.
D'ailleurs, à partir de Debian 11, le wrapper FastCGI n'est même plus inclus pour des raisons de sécurité.
Voici l'unité à créer :

~~~ {.ini}
[Unit]
Description=WWSympa - Web interface for Sympa mailing list manager
After=syslog.target sympa.service

[Service]
Type=forking
PIDFile=/var/run/sympa/wwsympa.pid
ExecStart=/usr/bin/spawn-fcgi -F $FCGI_CHILDREN \
    -P /var/run/sympa/wwsympa.pid \
    -s /var/run/sympa/wwsympa.socket \
    -u $FCGI_USER -g $FCGI_GROUP $FCGI_OPTS -- \
    /usr/lib/cgi-bin/sympa/wwsympa.fcgi
Environment="FCGI_CHILDREN=5"
Environment="FCGI_USER=sympa"
Environment="FCGI_GROUP=sympa"
Environment="FCGI_OPTS=-M 0600 -U www-data -G www-data"
#EnvironmentFile=-/etc/default/sympa
Restart=always
#RuntimeDirectory=sympa
#RuntimeDirectoryPreserve=yes

#ProtectHome=no
##ProtectHome=tmpfs
##BindPaths=/home/sympa-arc
#
#NoNewPrivileges=yes
#UMask=0027
##ProtectSystem=full
#PrivateTmp=yes
#PrivateDevices=yes
#ProtectKernelTunables=yes
#ProtectKernelModules=yes
#ProtectControlGroups=yes
#RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6
#RestrictNamespaces=yes
#LockPersonality=yes
#MemoryDenyWriteExecute=yes
#RestrictRealtime=yes
#
#SystemCallFilter=@system-service
#SystemCallErrorNumber=EPERM
#SystemCallArchitectures=native
#
#MemoryMax=1G

[Install]
WantedBy=multi-user.target
~~~

Attention, il ne faut pas mettre l'option `ProtectSystem=full` (paramètre par défaut sous Debian 10) car cela va empêcher la modification du fichier `/etc/mail/sympa/aliases`.


## Configuration

Fichiers de configuration :

~~~
/etc/sympa/
├── auth.conf
├── cookie
├── cookies.history
├── create_list_templates
├── custom_actions
├── custom_conditions
├── data_sources
├── data_structure.version
├── facility
├── families
├── global_task_models
├── list_task_models
├── mail_tt2
├── scenari
├── search_filters
├── sympa
│   ├── sympa.conf
│   └── sympa.conf.bin
├── sympa.conf-smime.in
├── topics.conf
└── web_tt2
~~~

La configuration principale se trouve dans le fichier `/etc/sympa/sympa/sympa.conf`.
Voici la configuration minimum à ajuster :

~~~
domain  lists.example.com
listmaster  postmaster@example.com
wwsympa_url http://lists.example.com/wws
lang    fr
~~~

> *Note 1* : pour Debian 10 ou inférieur, on indiquera `use_fast_cgi 1` pour utiliser le wrapper FastCGI avec Apache

> *Note 2* : en cas de changement de configuration de Sympa, il faut redémarrer Sympa **et** l'unité systemd `wwsympa` (ou Apache) à cause des process FastCGI.

### Apache

Vous devez créer un VirtualHost du type après avoir désactivé la configuration Sympa globale via `a2disconf sympa` et activé `a2enmod proxy_fcgi` :

~~~
<VirtualHost *:80 *:443>
    ServerName lists.example.com
    RedirectMatch ^/$ /wws/
    #Include /etc/apache2/ssl/sympa.conf

    Timeout 100

    Alias /static-sympa /usr/share/sympa/static_content
    <Directory /usr/share/sympa/static_content>
        Require all granted
    </Directory>

    Alias /css-sympa /var/lib/sympa/css
    <Directory /var/lib/sympa/css>
        Require all granted
    </Directory>

    Alias /pictures-sympa /var/lib/sympa/pictures
    <Directory /var/lib/sympa/pictures>
        Require all granted
    </Directory>

    <Location /wws>
        SetHandler "proxy:unix:/run/sympa/wwsympa.socket|fcgi://localhost/"
        Require all granted
    </Location>

    #RewriteEngine On
    #RewriteCond %{HTTPS} !=on
    #RewriteCond %{HTTP:X-Forwarded-Proto} !=https
    #RewriteRule ^/(.*) https://%{SERVER_NAME}/$1 [L,R=permanent]

</VirtualHost>
~~~

On conseille bien sûr de l'activer en [HTTPS](HowtoSSL), par exemple avec [Let's Encrypt](HowtoLetsEncrypt).

> *Note* : pour Debian 10 ou inférieur, si l'on utilise le wrapper FastCGI, on utilisera plutôt :
>
> ~~~
>    <IfModule mod_fcgid.c>
>        ScriptAlias /wws /usr/lib/cgi-bin/sympa/wwsympa-wrapper.fcgi
>        <Directory /usr/lib/cgi-bin/sympa>
>            Require all granted
>        </Directory>
>    </IfModule>
> ~~~


### Créer un compte administrateur (listmaster)

Ajouter l'adresse mail du futur admin dans `/etc/sympa/sympa/sympa.conf` dans la directive `listmaster` (les adresses doivent être séparées par des virgules).

Redémarrer le service `wwsympa`.

Puis, dans l'interface web, cliquer sur « Connexion » -> « Première connexion ? » et suivre les instructions.

Si ça a bien fonctionné, on doit voir le bouton « Administrateur de listes » dans la barre de menu du haut.


### DKIM

Génération des clés DKIM :

~~~
# opendkim-genkey -D /etc/ssl/private/ -d lists.example.com -s sympa2018 -v
# adduser sympa ssl-cert
# chown sympa:sympa /etc/ssl/private/sympa2018.private
~~~

On active ensuite dans `/etc/sympa/sympa/sympa.conf` :

~~~
dkim_feature on
dkim_private_key_path /etc/ssl/private/sympa2018.private
dkim_signer_domain lists.example.com
dkim_selector sympa2018
#dkim_signature_apply_on any
~~~

On peut ainsi configurer DKIM dans les paramètres de chaque liste de diffusion.

Sauf si votre liste est ouverte à n'importe quel expéditeur, on conseille de signer tous les messages via le paramètre `Les catégories de messages de liste qui seront signés avec DKIM. (dkim_signature_apply_on)` :

~~~
dkim_signature_apply_on any
~~~

On peut aussi mettre dans la configuration globale `dkim_signature_apply_on any` (par défaut cela va signer que les emails déjà signés).

### scenari

De nombreux scénarii (configurations de distribution des listes) sont proposés par défaut (cf. `/usr/share/sympa/default/scenari/`), mais il est possible de préparer un ou plusieurs scénarios pour des besoins spécifiques et récurrents en les plaçant dans `/etc/sympa/scenari/`. Voici un exemple qui supprime simplement le paramètre `quiet` afin de recevoir une notification en cas de rejet.

~~~
# cat /etc/sympa/scenari/send.confidential-not-quiet
title.gettext restricted to subscribers

is_subscriber([listname],[sender])             smtp,dkim,smime,md5    -> do_it
is_editor([listname],[sender])                 smtp,dkim,smime,md5    -> do_it
is_owner([listname],[sender])                  smtp,dkim,smime,md5    -> do_it
true()                                         smtp,dkim,md5,smime    -> reject
~~~

Plus de précisions sur la syntaxe et les options sont disponibles dans la [documentation officielle des scénarios](https://www.sympa.community/manual/customize/basics-scenarios.html).

### Configuration en mode multi-domaines

**Note :*** nous n'utilisons pas cette configuration en mode multi-domaines en production

Sympa permet d'utiliser plusieurs domaines différentes du type `list.example.com`, `list.example.org` etc.

Dans la configuration principale `/etc/sympa/sympa/sympa.conf` on va « désactiver » certaines directives :

~~~
domain  sympa
sendmail_aliases none
home /var/lib/sympa/expl
etc /etc/sympa
~~~

et l'on va configurer différents répertoires :

~~~
/etc/sympa/
├── list.example.com/
├── list.example.org/
~~~

avec `/etc/sympa/list.example.com/robot.conf` :

~~~
http_host list.example.com
wwsympa_url https://list.example.com/wws
title Listes example.com
lang fr
listmaster  postmaster@example.com
...
~~~

et l'arborescence suivante :

~~~
/var/lib/sympa/expl/
├── list.example.com/
├── list.example.org/
~~~


## Configuration de Postfix

Pour la réception d'emails, le principe de base est que les emails sont *pipés* vers le programme `/usr/lib/sympa/bin/queue`
sauf pour les emails de « rebond » qui sont *pipés* vers le programme `/usr/lib/sympa/bin/bouncequeue`.

### Configuration en mode non virtuel

Les emails d'administration (changement de mot de passe, modération, etc.) sont *pipés* vers `/usr/lib/sympa/bin/queue sympa` grâce à des alias :

~~~
sympa:          "|/usr/lib/sympa/bin/queue sympa"
#sympa-request:  "|/usr/lib/sympa/bin/queue sympa"
#sympa-owner:    "|/usr/lib/sympa/bin/bouncequeue sympa"
bounce:         "|/usr/lib/sympa/bin/bouncequeue sympa"
#listmaster:     "|/usr/lib/sympa/bin/queue sympa"
listmaster:     postmaster
~~~

Les emails pour une liste de diffusion *foo* sont envoyés à `foo@example.com`.
Les alias Postfix suivants redirigent vers les différents programmes :

~~~
foo: "| /usr/lib/sympa/bin/queue foo"
foo-request: "| /usr/lib/sympa/bin/queue foo-request"
foo-editor: "| /usr/lib/sympa/bin/queue foo-editor"
foo-subscribe: "| /usr/lib/sympa/bin/queue foo-subscribe"
foo-unsubscribe: "| /usr/lib/sympa/bin/queue foo-unsubscribe"
foo-owner: "| /usr/lib/sympa/bin/bouncequeue foo"
~~~

Le message passe ensuite par différentes files d'attente suivant la configuration (modération, etc.).

**Attention 1 :** Si ce domaine est déjà défini comme virtuel dans Postfix, cela cassera la réception de mails sur boîtes virtuelles associées à ce domaine.

**Attention 2:** vous ne devez pas avoir d'alias local pour `bounce` car il est utilisé par Sympa pour le [VERP](https://www.sympa.community/manual/customize/bounce-management.html#verp), vérifiez notamment qu'il n'est pas utilisé dans la directive Postfix `bounce_notice_recipient`.


### Configuration en mode virtuel avec un domaine dédié

C'est la configuration conseillée : vous avez un domaine du type `list.example.com` utilisé uniquement pour Sympa.

Il suffit alors de définir un transport via `/etc/postfix/transport` :

~~~
list.example.com sympa:
~~~

Et un process `sympa` dans `/etc/postfix/master.cf` :

~~~
    sympa unix  -       n       n       -       -       pipe
      flags=FR user=sympa
      argv=/var/lib/sympa/bin/postfix-to-sympa.pl ${nexthop} ${mailbox}
~~~

Où [postfix-to-sympa.pl](https://www.sympa.org/contribs/index#easy_configuration_for_postfix) est un script Perl (avec `$DEFAULT_DOMAIN` à ajuster) qui va notamment renvoyer vers les commandes Sympa `queue` et `bouncequeue`. Merci Bastien Roucaries pour ce script :)

Il faut aussi ajouter les directives suivantes dans `/etc/postfix/main.cf` :

~~~
relay_domains = list.example.com
sympa_destination_recipient_limit = 1
~~~

### Configuration en mode virtuel sans domaine dédié

Si l'on utilise un même domaine pour recevoir des emails et Sympa, il va falloir gérer des sortes d'alias virtuels pour ne renvoyer vers Sympa que ce qui est nécessaire.

Donc a priori le domaine virtuel est déjà configuré dans Postfix (par exemple via LDAP), sinon on peut l'ajouter via `virtual_mailbox_domains`.

On configure un template pour les « alias virtuels » :

~~~
# cat /etc/sympa/list_aliases.tt2
#--- [% list.name %]@[% list.domain %]: list transport map created at [% date %]
[% list.name %]@[% list.domain %] sympa:[% list.name %]@[% list.domain %]
[% list.name %]-request@[% list.domain %] sympa:[% list.name %]-request@[% list.domain %]
[% list.name %]-editor@[% list.domain %] sympa:[% list.name %]-editor@[% list.domain %]
#[% list.name %]-subscribe@[% list.domain %] sympa:[% list.name %]-subscribe@[%list.domain %]
[% list.name %]-unsubscribe@[% list.domain %] sympa:[% list.name %]-unsubscribe@[% list.domain %]
[% list.name %][% return_path_suffix %]@[% list.domain %] sympabounce:[% list.name %]@[% list.domain %]
~~~

On ajoute les directives suivantes dans `/etc/sympa/sympa/sympa.conf` :

~~~
sendmail_aliases /etc/postfix/sympa_transport
aliases_program postmap
aliases_db_type hash
~~~

On crée la base de données des boîtes virtuelles pour Postfix :

~~~
# touch /etc/sympa/sympa_transport
# chmod 644 /etc/sympa/sympa_transport
# chown sympa: /etc/sympa/sympa_transport
# /usr/lib/sympa/bin/sympa_newaliases.pl
~~~

On s'assure que `/etc/sympa/sympa_transport` contient :

~~~
sympa@example.org          sympa:sympa@example.org
listmaster@example.org     sympa:listmaster@example.org
bounce@example.org         sympabounce:sympa@example.org
abuse-feedback-report@example.org  sympabounce:sympa@example.org
~~~

On ajoute à `/etc/postfix/master.cf` :

~~~
sympa   unix    -       n       n       -       -       pipe
  flags=hqRu null_sender= user=sympa argv=/usr/lib/sympa/bin/queue ${nexthop}
sympabounce unix -      n       n       -       -       pipe
  flags=hqRu null_sender= user=sympa argv=$/usr/lib/sympa/bin/bouncequeue ${nexthop}
~~~

Et on ajoute les directives à `/etc/postfix/main.cf` :

~~~
virtual_mailbox_maps = (...) hash:/etc/sympa/sympa_transport
transport_maps = (...) hash:/etc/sympa/sympa_transport
~~~ 

**Attention :** vous ne devez pas avoir d'alias virtuel pour `bounce` car il est utilisé par Sympa pour le [VERP](https://www.sympa.community/manual/customize/bounce-management.html#verp), vérifiez notamment qu'il n'est pas utilisé dans la directive Postfix `bounce_notice_recipient`.


## Plomberie

Voici les répertoires importants pour Sympa :

* `/var/spool/sympa/` : files d'attente (modération etc.)
* `/var/lib/sympa/arc/` : archives des listes de diffusion générées avec [MHonArc](https://www.mhonarc.org/)
* `/var/lib/sympa/list_data/` : données de configuration des listes de diffusion
* `/usr/share/sympa/bin/` et `/usr/lib/sympa/bin/` : scripts Perl et binaires
* `/usr/share/sympa/lib/` et : bibliothèques Perl
* `/usr/share/sympa/default/` : templates

La configuration d'une liste se trouve à la fois :

* dans le répertoire `/var/lib/sympa/list_data/foo/` notamment le fichier `config`
* dans la base de données, notamment les tables `list_table` ou `user_table` (abonnés).


## FAQ

### List::load() No such robot

J'ai des messages de ce type alors que *example.com* n'est pas/plus du tout domaine :

~~~
Nov  9 18:35:18 serveur task_manager[7285]: List::load() No such robot (virtual domain) example.com
~~~

Cela pourrait venir de tâches à nettoyer dans `/var/spool/sympa/task/`.
Au passage, vérifier en base de données qu'il n'y a pas/plus rien relatif à *example.com* dans subscriber_table, session_table, logs_table.

### Installation sous Debian 6

Sous Debian Squeeze, voici quelques manipulations nécessaires suite à l'installation :

~~~
# chown sympa:sympa /usr/lib/sympa/lib/sympa/queue
# chmod u+s /usr/lib/sympa/lib/sympa/queue
# chown root /etc/postfix/sympa.aliases
~~~

### End of script output before headers

Si vous avez des erreurs Apache du type :

~~~
[fcgid:warn] [pid 21826] (104)Connection reset by peer: [client 192.0.2.42:37006] mod_fcgid: error reading data from FastCGI server
[core:error] [pid 21826] [client 192.0.2.42:37006] End of script output before headers: wwsympa-wrapper.fcgi
~~~

[Assurez-vous](https://bugs.debian.org/682562) d'avoir activé `use_fast_cgi`.

### Prise en compte des changements de configuration

En cas de changement de configuration de Sympa, il faut redémarrer Sympa **et** Apache (à cause des process FastCGI).

### Données dans LDAP

Après avoir créé une liste, on peut configurer un annuaire LDAP en allant dans `Configurer la liste` puis `Définition des sources de données`.
Il pourra être nécessaire de cliquer sur `Mise à jour` pour mettre à jour les données LDAP.

### Unable run newaliases / Failed to remove aliases

Lors de l'ajout / suppression de listes, si vous obtenez des erreurs du type :

~~~
wwsympa[19038]: [robot lists.example.com] [user listmaster@lists.example.com] [list test] Unable run newaliases
wwsympa[19027]: Failed to remove aliases ; status 6 : Ioctl() inappropré pour un périphérique
~~~

Un contournement peut être d'utiliser directement `/usr/lib/sympa/bin/alias_manager.pl` :

~~~
# /usr/lib/sympa/bin/alias_manager.pl -h
Usage:
    alias_manager.pl add | del listname domain
~~~

### sympa_newaliases exited with status 75

Sympa lance la commande `newaliases` mais n'a souvent pas les droits pour `/etc/aliases`
On conseille donc de modifier la configuration Postfix pour « réserver `newaliases` »
à Sympa en mettant uniquement

~~~
alias_database = hash:/etc/mail/sympa/aliases
~~~

Il faut aussi voir si il n'y a pas des restrictions Systemd.

### emails d'une liste vers une autre non archivée ?

C'est probablement à cause de la présence de l'entête `X-No-Archive: yes` !

### emails non reçus par une liste ?

Voir dans `sympa.pl`, certains filtres basiques sont effectués, notamment :

~~~
if ($sender =~ /^(mailer-daemon|sympa|listserv|mailman|majordomo|smartlist|$conf_email)(\@|$)/mio) {
~~~

### Sympa : User unknown in local recipient table

En cas d'erreur "foo@sympa.example.com: Recipient address rejected: User unknown in local recipient table" il faut probablement refaire un postmap sur le fichier d'alias de Sympa :

~~~
# postmap /etc/mail/sympa/aliases
~~~

(ou plus simplement un `newaliases` si Postfix est bien configuré).

### Forcer l'adresse email expéditrice


Pour différentes raisons - notamment de délivrabilité - on veut forcer l'expéditeur avec l'adresse email de la liste. Le plus simple est de positionner le paramètre `Auteur anonyme (anonymous_sender)` qui va simplement remplacer l'entête `From:` :

~~~
anonymous_sender
~~~

Nous conseillons plutôt de régler les paramètres via le menu `Configurer la liste > DKIM/DMARC/ARC` en spécifiant une protection DMARC pour `all` (ce qui va forcer l'adresse expéditrice avec l'adresse de la liste) et en ajustant `Nouveau format de nom From (phrase)` pour conserver le nom affiché par l'expéditeur :

~~~
dmarc_protection
mode all
phrase display_name
~~~

Sympa offre la possibilité que l'option `dmarc_protection` ne s'applique que quand les domaines expéditeurs ont une politique DMARC qui échoue, mais nous conseillons de ré-écrire le `From:` dans les cas (`mode all`).

Pour plus d'infos : <https://www.sympa.community/manual/customize/dmarc-protection.html>

### Read-only file system

Si vous avez des messages de ce type lors de l'ajout/suppression d'une liste :

~~~
wwsympa[19928]: err main::#1566 > main::do_create_list#9792 > Sympa::Spindle::spin#95 > Sympa::Request::Handler::create_list::_twist#223 > Sympa::Aliases::Template::add#119 Unable to append to /etc/mail/sympa/aliases: Read-only file system
wwsympa[20004]: err main::#1566 > main::do_close_list#11188 > Sympa::Spindle::spin#95 > Sympa::Request::Handler::close_list::_twist#74 > Sympa::Request::Handler::close_list::_close#155 > Sympa::Aliases::Template::del#228 Could not overwrite /etc/mail/sympa/aliases: Read-only file system
~~~

C'est peut-être une restriction Systemd du type `ProtectSystem=full` (par défaut dans Debian 10).

### Attention à la casse des adresses mails

Quand on demande l'index des listes :

~~~
DIED: Can't use an undefined value as an ARRAY reference at /usr/share/sympa/lib/Sympa/List.pm line 2868.
at /usr/share/sympa/lib/Sympa/List.pm line 2868.
Sympa::List::is_admin(Sympa::List <infos@frdomaine.org>, 'privileged_owner', 'n.s@fredomainee.fr') called at /usr/lib/cgi-bin/sympa/wwsympa.fcgi line 4258
main::do_lists() called at /usr/lib/cgi-bin/sympa/wwsympa.fcgi line 1557
~~~

Vérifier que les adresses mails dans les fichiers de configurations soient écrites en minuscules.

### Mise à jour de la base de données

En cas de migration de Sympa (ou autre), la structure de la base de données ne sera pas à jour.

Vous aurez par exemple des erreurs du type :

~~~
wwsympa[1827142]: err main::#1557 > main::do_lists#4258 > Sympa::List::is_admin#2868 [...] ORDER BY user_admin": (42S22) Unknown column 'date_epoch_admin' in 'field list'
wwsympa[1827142]: err main::#1557 > main::do_lists#4258 > Sympa::List::is_admin#2868 DIED: Can't use an undefined value as an ARRAY reference at /usr/share/sympa/lib/Sympa/List.pm line 2868

task_manager[3617077]: err main::#148 > Sympa::Spindle::spin#78 > Sympa::Spool::next#125 > Sympa::Spool::Task::_load#57 > Sympa::Spool::Task::_create_all_tasks#103 > Sympa::List::has_included_users#5796 > Sympa::Database::do_prepared_query#382 Unable to execute SQL statement "SELECT COUNT(*) FROM subscriber_table WHERE list_subscriber = ? AND robot_subscriber = ? AND inclusion_subscriber IS NOT NULL": (42S22) Unknown column 'inclusion_subscriber' in 'where clause'
~~~


Voici comment forcer la mise à jour de Sympa 6.2.16 à 6.2.60 par exemple :

~~~
# vim /etc/sympa/data_structure.version

6.2.16

# su -l sympa -s /bin/sh -c "/usr/lib/sympa/bin/sympa.pl --upgrade"

notice main:: Sympa 6.2.60 Started
notice main:: Upgrade process...
notice main:: Upgrading from 6.2.16 to 6.2.60...
notice Sympa::DatabaseManager::_check_indexes() Index session_prev_id_index on table session_table does not exist. Adding it
notice Sympa::DatabaseManager::_check_fields() Field "password_user" (table "user_table"; database "sympa") does NOT have awaited type (varchar(64)) where type in database seems to be (varchar(40)). Attempting to change it...
notice Sympa::DatabaseDriver::MySQL::update_field() ALTER TABLE user_table CHANGE password_user password_user varchar(64) 
...
~~~

### ARC

Ce thread est intéressant pour voir comment activer/gérer ARC : <https://github.com/sympa-community/sympa/issues/575>

### Le mot de passe devrait être reconverti

Si vous recevez un message du type « Le mot de passe devrait être reconverti » avec comme précision : « Les mots de passe dans la base de données semblent crypté. Exécutez upgrade_sympa_password.pl pour convertir les mots de passe à nouveau. »

Cela signifie que votre table `user_table` contient des mots de passe avec un chiffrement faible.
Vous pouvez utiliser le script `upgrade_sympa_password.pl` pour convertir ces mots de passe.

~~~
MariaDB [sympa]> select count(*) from user_table where password_user LIKE 'crypt%';
+----------+
| count(*) |
+----------+
|   48036 |
+----------+

# /usr/share/sympa/bin/upgrade_sympa_password.pl

Recoding password using md5 fingerprint.
...
Found in table user 41420 passwords stored using md5. Did you run Sympa before upgrading?
Updated 48036 user passwords in table user_table using md5 hashes.
Time required to calculate new md5 hashes: 0.29 seconds 0.00001 sec/hash

MariaDB [sympa]> select count(*) from user_table where password_user LIKE 'crypt%';
+----------+
| count(*) |
+----------+
|        0 |
+----------+
~~~

### footer

Pour chaque liste on peut créer des messages personnalisés, notamment un footer commun.

À partir de certaines versions, on peut aussi mettre un footer commun à toutes les listes (qui sera écrasé si un footer personnalisé est défini).

Pour cela, il suffit de créer un fichier `/etc/sympa/message_footer`.

Si l'on veut utiliser des [variables](https://www.sympa.community/manual/customize/message-personalization.html) de type `[% listname % ]` il faut ajouter dans la configuration globale :

~~~
personalization_feature on
personalization.mail_apply_on footer
~~~

Note : attention, le chemin est a changé depuis la version 6.2.43, c'est directement dans le répertoire `/etc/sympa/` et non plus dans un sous-répertoire `mail_tt2/` et certains noms de fichiers ont changé, comme proposé dans le [ticket #507](https://github.com/sympa-community/sympa/issues/507).

Il existe aussi la possibilité de mettre un footer global `/etc/sympa/message_global_footer` et `/etc/sympa/message_global_footer.mime` qui sera ajouté même si un footer personnalisé est déjà défini.

### Importer des archives à partir d'emails

Il faut partir d'un fichier "mailbox" (on pourra convertir une Maildir en mailbox via <https://github.com/jooooooon/maildir2mbox>)

On utilise ensuite "mbox2sympa.pl" téléchargeable sur <https://www.sympa.org/contribs/migration_and_archives/index> qui va générer des fichiers `log.ANNÉEMOIS`.
Ce vieux script est un peu capricieux, au pire on pourra aller supprimer la ligne 75 avec le `dir "Sorry, ..." if ( $month == NULL );` qui peut boguer.
On place ensuite ces fichiers `log.ANNÉEMOIS` dans le répertoire à créer `/var/lib/sympa/list_data/LISTE/archives/` et on ajuste les droits (`chown sympa:`)
Enfin, on peut lancer ce script qui va générer les archives :

~~~
# sudo -u sympa /usr/share/sympa/bin/arc2webarc.pl LISTE
...
Found 1066 messages
Rebuilding HTML
~~~

