**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto strongSwan

<http://www.strongswan.org/documentation.html>

StrongSwan est un fork de FreeS/WAN, permettant de gérer des VPNs IPsec sur des systèmes à base de noyau Linux. Il inclu 2 démons, Pluto et Charon, gérant respectivement les protocoles IKEv1 et IKEv2 d'IPsec.

## Installation

~~~
# aptitude install strongswan strongswan-starter
~~~

## Configuration

La configuration se fait dans le fichier _/etc/ipsec.conf_. Les clés privées ou passphrases sont à indiquer dans le fichier _/etc/ipsec.secrets_ (syntaxe différente du fichier de conf).

### Example de configuration

Fichier _/etc/ipsec.conf_ :

~~~
config setup
	interfaces=%defaultroute

conn myvpn
	type=tunnel
        # IP publique de la machine locale
	left=192.0.2.18
        # Réseau privé attribué à la machine locale dans le VPN
	leftsubnet=10.0.0.0/24
        # IP privée attribuée à la machine locale dans le VPN
	leftsourceip=10.0.0.1
        # IP publique de la machine distante
	right=198.51.100.56
        # Réseau privé attribué à la machine distante dans le VPN
	rightsubnet=10.0.1.0/24
	auto=route
	authby=secret
~~~

Fichier _/etc/ipsec.secrets_ :

~~~
192.0.2.18 198.51.100.56 : PSK "mypassphrase"
~~~

Pour pouvoir joindre le réseau privé distant, il faut ajouter une route comme suit :

~~~
route add -net 10.0.1.0/24 gw 10.0.0.1
~~~

## Administration

### État du VPN

/!\ Contrairement à OpenBSD, sur Linux la commande `route` ne retourne pas les routes IPsec. Pour gérer le réseau IPsec sous Linux, on peut utiliser les commandes `ip xfrm ...`.

~~~
# ipsec status
~~~

ou

~~~
# ip xfrm policy
~~~

### Gérer les démons pluto et charon

En cas de changement dans la conf :

~~~
ipsec start|stop|restart|reload
~~~

### Gérer les VPN

En cas de perte de connexion/problème de lifetime...

~~~
# ipsec down|up <connection name>
~~~