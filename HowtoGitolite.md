# Howto gitolite

<http://gitolite.com/gitolite/index.html#rtfm>

## Installation

On génère une clé SSH de setup :

    $ ssh-keygen -f ~/.ssh/gitolite_admin_id_rsa

Sur le serveur, on installe gitolite dans un compte Unix "git" :

    # apt install git gitolite3
    # adduser --disabled-password git
    # sudo -iu git
    $ gitolite setup -pk gitolite_admin_id_rsa.pub

## Gestion des utilisateurs

~/.ssh/config :

    ---
    Host git.example.com
      User git
      IdentityFile ~/.ssh/gitolite_admin_id_rsa

Manipulations avec l'utilisateur qui a la clé SSH de setup :

    git clone ssh://git@git.example.com/gitolite-admin
    cd gitolite-admin
    vim conf/gitolite.conf
    vim keydir/jdoe.pub
    git commit && git push

gitolite.conf :

    ---
    repo wiki
    RW = foo


