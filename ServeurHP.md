**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Serveur HP Proliant

## Mises-à-jour BIOS et firmwares

Télécharger et graver le DVD Service Pack for Proliant (HPP) via <https://www.hp.com/go/spp> (attention, il faut un compte HP, c'est fastidieux…).
Démarrer sur le DVD et l'on peut choisir de flasher manuellement ou tous les firmwares du serveur (attention, c'est long ~30 minutes)

Si ça ne fonctionne pas et qu'on dispose d'une license iLO suffisante, il est possible de pousser l'ISO directement via iLO et de faire la mise à jour par ce biais. Pour ça il est à priori nécessaire d'utiliser le client iLO Java ou .Net car le client HTML5 ne supporte pas toujours les ISO de grosse taille. Cette vidéo donne des explications utiles : <https://www.youtube.com/watch?v=ghA2B91my6s>


## Logs "Active Health System"

Pour récupérer les logs "Active Health System" (parfois demandé par le support HP), 
*prévoir une clé USB formatée en FAT32*, puis faire :

Démarrer via F10 (cela boote sur un Linux !)

Brancher la clé USB

Choisir "HP System Log Reader Utility"

On arrive sur un écran où l'on peut copier les logs sur la clé USB :

[[Image(intervention-HP-nov2015-ActiveHealthSystemLog.jpg)]]


## ILO

Par défaut l'interface ILO est en DHCP, pour y accéder il suffit de taper son adresse IP via un navigateur web.
Le login par défaut est « Administrator », est le mot de passe est indiqué sur une étiquette collée sur le serveur.