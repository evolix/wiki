---
title: Howto doas
categories: openbsd
...

* <http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man1/doas.1>
* <http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/doas.conf.5>

**doas** est un programme développé pour OpenBSD permettant d'accorder des droits à certains utilisateurs pour lancer des commandes en tant que _root_ ou un utilisateur différent. _doas_ a vocation à remplacer [sudo](HowtoSudo) car ce dernier, bien que plus complet, induit une complexité jugée inutile dans le système de base par les développeurs du projet OpenBSD.


## Configuration

On configure _doas_ en listant des autorisations dans le fichier `/etc/doas.conf`

Une autorisation a la forme suivante :

~~~
action option jdoe as foo cmd /path/commande
~~~

* `action` : permit / deny
* `option` : nopass / persist / keepenv / setenv { [variable…] }

### Exemples d'autorisations

* Tout permettre à "root" sans mot de passe :

~~~
permit nopass root
~~~

* Tous les membres du groupe `wheel` peuvent modifier ces variables d'environnement lors de l'exécution de `doas` :

~~~
permit setenv {ENV PS1 SSH_AUTH_SOCK SSH_TTY} :wheel
~~~

* L'utilisateur "_nrpe" peut exécuter "check_ipsecctl", sans mot de passe :

~~~
permit nopass _nrpe cmd /usr/local/libexec/nagios/check_ipsecctl.sh
~~~

* Tous les membres du groupe "wheel" peuvent exécuter "evomaintenance" en tant que root, sans mot de passe :

~~~
permit nopass :wheel as root cmd /usr/share/scripts/evomaintenance.sh
~~~


## Utilisation

~~~
$ doas su
$ doas <commande>
$ doas -u bar <commande>
~~~


## Liens utiles

* <http://www.tedunangst.com/flak/post/doas-mastery>
* <http://www.tedunangst.com/flak/post/doas>
