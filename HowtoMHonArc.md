**Cette page a été importée automatiquement de notre ancien wiki mais n'a pas encore été révisée.**

# Howto MHonArc

Pour générer des pages HTML à partir d'une Maildir :

~~~
$ mv MAILDIR/new MAILDIR/cur
$ mhonarc -mhpattern '^[^\.]' MAILDIR/cur
~~~

<http://www.mhonarc.org/MHonArc/doc/faq/envs.html#maildir>