---
categories: web
title: Howto Metabase
...

* Documentation : <https://www.metabase.com/docs/latest/>

[Metabase](https://www.metabase.com/) est un logiciel open source permettant l'exploration de données.

## Installation

Metabase nécessite OpenJDK 8, l'installer via [HowtoJava]().

Créer un utilisateur `metabase` et télécharger le fichier `.jar` :

~~~
useradd metabase
mkdir -p /home/metabase
cd /home/metabase
wget http://downloads.metabase.com/v0.31.0/metabase.jar
chown metabase: /home/metabase -R
~~~

Créer l'unité systemd suivante `/etc/systemd/system/metabase.service` :

~~~
[Unit]
Description=Metabase
After=network.target

[Service]
Type=simple
ExecStart=/usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar /home/metabase/metabase.jar
User=metabase
Group=metabase
WorkingDirectory=/home/metabase
EnvironmentFile=/home/metabase/metabase.conf
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=metabase
SuccessExitStatus=143
TimeoutStopSec=120
Restart=always

[Install]
WantedBy=multi-user.target
~~~

Il faut enlever la variable `EnvironmentFile` si on n'utilise que la configuration par défaut de metabase.

Puis lancer et activer le service au démarrage :

~~~
systemctl daemon-reload
systemctl start metabase
systemctl enable metabase
~~~

Si des fichiers db ont été fourni, il faut les placer dans `/home/metabase/data/` :

~~~
drwxr-xr-x 2 metabase metabase     4096 sept. 20 16:58 .
drwxr-x--- 4 metabase metabase     4096 sept. 20 16:57 ..
-rw-r--r-- 1 metabase metabase 11878400 sept. 20 17:08 metabase.db.mv.db
-rw-r--r-- 1 metabase metabase    14867 sept.  7 17:44 metabase.db.trace.db
~~~

Puis indiquer dans le fichier de configuration ceci :

~~~
MB_DB_FILE=/home/metabase/data/metabase.db
~~~

On donne à l'applicatif la DB nommée `metabase.db` et l'applicatif va chercher les données soit dans le fichier `metabase.db.mv.db` soit `metabase.db.trace.db` en fonction du besoin.

Si on veux faire écouter metabase sur un port alternatif, il faut configurer la variable `MB_JETTY_PORT` dans metabase.conf :

~~~
MB_JETTY_PORT=8094
~~~