---
title: Howto Datadog
---

Doc officielle : <https://docs.datadoghq.com/>

Tideways est un APM système et applicatif (PHP, …). Il permet d'analyser les métriques systèmes ainsi que fournir des traces applicatives pour repérer aisément d'où proviennent les lenteurs (requêtes SQL, appels externes, …).

# Installation

## Installer le paquet

~~~
# cd /etc/apt/trusted.gpg.d
# wget 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xa2923dff56eda6e76e55e492d3a80e30382e94de' -O datadoghq.asc
# chmod 644 datadoghq.asc
# echo 'deb https://apt.datadoghq.com/ stable 6' > /etc/apt/sources.list.d/datadog.list
# apt update
# apt install datadog-agent
# cp /etc/datadog-agent/datadog.yaml.example /etc/datadog-agent/datadog.yaml
# sed -i 's/api_key:.*/api_key: $apikey/' /etc/datadog-agent/datadog.yaml
# chown dd-agent: /etc/datadog-agent/datadog.yaml
# systemctl start datadog-agent
~~~

> **Note**: Vous devez spécifier votre clé d'API.

## Surveiller les applicatifs

Les modifications doivent être fait dans le code, par exemple pour PHP : <https://github.com/DataDog/php-datadogstatsd#php-datadog-statsd-client>

## Surveiller le système

### Apache

Mettre l'URL de server-status dans la configuration.

~~~
# cp /etc/datadog-agent/conf.d/apache.d/conf.yaml.example /etc/datadog-agent/conf.d/apache.d/conf.yaml
# vim /etc/datadog-agent/conf.d/apache.d/conf.yaml
# systemctl restart datadog-agent
~~~
